#include "stdafx.h"

#include "ScreenBatch.h"
#include "Paths.h"
#include "WindowConstants.h"
#include "SFML\Graphics.hpp"
#include "Core.h"

namespace mg {

	void ScreenBatch::Init(LuaPlus::LuaState* luaState)
	{
		// initialize framebuffer vbo

		vertices->Init(sizeof(FBOVertex) * 4, GL_ARRAY_BUFFER);

		vertices->Bind();
		vertices->Fill(
			vector<FBOVertex>{
				FBOVertex{ glm::vec3(0.f, 0.f, 0.f), glm::vec2(0.f, 0.f) },
				FBOVertex{ glm::vec3(0.f, float(height), 0.f), glm::vec2(0.f, 1.f) },
				FBOVertex{ glm::vec3(float(width), float(height), 0.f), glm::vec2(1.f, 1.f) },
				FBOVertex{ glm::vec3(float(width), 0.f, 0.f), glm::vec2(1.f, 0.f) }
			}
		);
		vertices->Unbind();

		indices->Init(sizeof(GLuint) * 4, GL_ELEMENT_ARRAY_BUFFER);

		indices->Bind();
		indices->Fill(vector<GLuint> { GLuint(0), GLuint(1), GLuint(2), GLuint(3)});
		indices->Unbind();

		glGenVertexArrays(1, &vao);

		glBindVertexArray(vao);
		vertices->Bind();
		indices->Bind();

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(FBOVertex), 0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(FBOVertex), BUFFER_OFFSET(12));

		glBindVertexArray(NULL);
		vertices->Unbind();
		indices->Unbind();

		CHECK_GL_ERROR
	}

	void ScreenBatch::Draw()
	{
		glDrawElements(GL_QUADS, indices->GetCurrentOffset(), GL_UNSIGNED_INT, BUFFER_OFFSET(0));

		CHECK_GL_ERROR
	}
}