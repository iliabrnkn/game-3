#pragma once

#include "RenderBatch.h"
#include "VertTypes.h"

namespace mg
{
	class ShadowBatch : public ConcreteRenderBatch<SimpleVertex>
	{
		friend class RenderingSystem3D;

		std::vector<unsigned int> indicesSource;
		std::vector<SimpleVertex> verticesSource;

	public:
		virtual void Init(LuaPlus::LuaState* luaState) override;
		virtual void Draw() override;
	};
}