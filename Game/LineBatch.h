#pragma once

#include <vector>

#include "RenderBatch.h"
#include "VertTypes.h"
namespace mg
{
	class LineBatch : public ConcreteRenderBatch<LineVertex>
	{
	public:
		LineBatch();
		virtual void Init(LuaPlus::LuaState* luaState) override;
		virtual void Draw() override;
	};
}