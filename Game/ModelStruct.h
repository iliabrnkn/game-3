#pragma once

#include "stdafx.h"

#include <vector>
#include <string>
#include <unordered_map>

#include <windows.h>
#include <GL/glew/glew.h>
#include <GL/GL.h>

#include <glm\glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "VertTypes.h"

using namespace std;

namespace mg
{
	const int MAX_MESHES_COUNT = 100;
	const int MAX_MESHES_TYPES = 20;
	const int MAX_BONES_PER_MODEL = 128;
	const int MAX_WEIGHTS_PER_VERTEX = 4;
	const int MAX_VERT_PER_MODEL = 4000;
	const int MAX_ADJUST_FACES_PER_VERT = 12;

	struct Bone
	{
		std::string name;

		glm::mat4x4 offsetMatrix;
		glm::mat4x4* localTransform;
		size_t queuePos;
	};

	struct AnimKey
	{
		double time;

		glm::vec3 pos;
		glm::quat rot;
		glm::vec3 scale;

		AnimKey() :
			pos(glm::vec3(0.0f)),
			rot(glm::quat()),
			scale(glm::vec3(0.0f))
		{
		}

		static AnimKey mix(const AnimKey& animKeyFirst, const AnimKey& animKeySec, float interpolant = 0.5f)
		{
			AnimKey result;

			result.pos = glm::mix(animKeyFirst.pos, animKeySec.pos, interpolant);
			result.rot = glm::slerp(animKeyFirst.rot, animKeySec.rot, interpolant);
			result.scale = glm::mix(animKeyFirst.scale, animKeySec.scale, interpolant);

			return result;
		}

		bool operator == (const AnimKey& animKey) const
		{
			if (animKey.pos == pos && animKey.rot == rot && animKey.scale == scale)
				return true;

			return false;
		}

		bool operator != (const AnimKey& animKey) const
		{
			if (!(*this == animKey))
				return true;

			return false;
		}
	};

	struct BoneNode
	{
		AnimKey currentAnimKey;
		std::string parentName;
		std::vector<string> childNames;

		string boneName;
		string name;

		bool transformedByChannel = false;

		glm::mat4x4 basicTransform;
		glm::mat4x4 ˝urrentTransform;
		glm::mat4x4 channelTransform;

		float additionalAngle;
	};

	struct AnimChannel
	{
		vector<AnimKey> keys;

		glm::vec3 minAngles;
	};
}