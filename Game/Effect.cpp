#include "stdafx.h"

#include <algorithm> 
#include <utility>
#include <string>

#include "Effect.h"
#include "ErrorLog.h"

using namespace std;

namespace mg {

	Effect::Effect()
		: programID(-1),
		isResourceOwner(true)
	{ }

	Effect::Effect(Effect& effect) :
		name(effect.name),
		programID(effect.programID),
		attribs(effect.attribs),
		uniforms(effect.uniforms),
		isResourceOwner(true)
	{
		effect.isResourceOwner = false;
	}

	Effect::Effect(Effect&& effect)
	{
		swap(name, effect.name);
		swap(programID, effect.programID);
		swap(attribs, effect.attribs);
		swap(uniforms, effect.uniforms);
		isResourceOwner = true;
	}

	Effect& Effect::operator = (Effect& effect)
	{
		swap(name, effect.name);
		swap(programID, effect.programID);
		swap(attribs, effect.attribs);
		swap(uniforms, effect.uniforms);
		isResourceOwner = true;

		effect.isResourceOwner = false;

		return *this;
	}

	Effect& Effect::operator = (Effect&& effect)
	{
		swap(name, effect.name);
		swap(programID, effect.programID);
		swap(attribs, effect.attribs);
		swap(uniforms, effect.uniforms);
		isResourceOwner = true;

		effect.isResourceOwner = false;

		return *this;
	}

	void Effect::LoadFromFile(const std::string& shaderFilename, LuaPlus::LuaState* luaState)
	{
		// ������ ���� lua � ��������
		// TODO: �����������

		luaState->DoFile(shaderFilename.c_str());
		
		LuaPlus::LuaObject mainTable = luaState->GetMetatable(0);

		name = LuaHelper::GetString(mainTable, "name");

		string vertShader = mainTable["vertexshader"].ToString();
		string fragShader = mainTable["fragmentshader"].ToString();

		// ������� � ����������� ��� ������

		vertShaderID = AddShader(vertShader.c_str(), GL_VERTEX_SHADER, "vertex");
		
		if (mainTable.GetByName("geometryshader").GetRef() != -1)
		{
			string geoShaderCode = mainTable["geometryshader"].ToString();
			geometryShaderExists = true;
			
			geoShaderID = AddShader(geoShaderCode.c_str(), GL_GEOMETRY_SHADER, "geometry");
		}

		fragShaderID = AddShader(fragShader.c_str(), GL_FRAGMENT_SHADER, "fragment");

		// ������ ��������� �� ����������� ����� ��� ���

		LuaPlus::LuaObject attribTable = mainTable["attriblocations"];

		for (int i = 1; ; i++)
		{
			LuaPlus::LuaObject currAttr = attribTable.GetByIndex(i);

			if (currAttr.GetRef() == -1) break;

			// �������� ����������� � �����������
			attribs[currAttr["name"].ToString()] = currAttr["location"].ToInteger();
		}


		CreateProgram();

		// �������� ��� �������-����������

		GLint size; // ������ ����������
		GLenum type; // ��� ���������� (float, vec3 or mat4, etc)

		GLchar name[GLSL_MAX_NAME_LENGTH];
		GLsizei length; // ����� ����� ����������

		GLint uniformCount;
		glGetProgramiv(programID, GL_ACTIVE_UNIFORMS, &uniformCount);

		for (GLuint i = 0; i < static_cast<GLuint>(uniformCount); i++)
		{
			// �������� �������-����������
			glGetActiveUniform(programID, i, GLSL_MAX_NAME_LENGTH, &length, &size, &type, name);
		}

		CHECK_GL_ERROR
	}

	Effect::~Effect(void)
	{
		if (isResourceOwner) // ���� ������� ������ �� ��������, ������� ��
		{
			glDeleteShader(vertShaderID);
			glDeleteShader(fragShaderID);
			
			if (geometryShaderExists)
				glDeleteShader(geoShaderID);
			
			glDeleteProgram(programID);
		}

		CHECK_GL_ERROR
	}

	const string& Effect::GetName() const
	{
		return name;
	}

	GLuint& Effect::AddShader(const char *chars, GLenum shaderType, const char* shaderLogName)
	{
		if (programID == -1) programID = glCreateProgram(); // ���������� �����
		GLuint index = glCreateShader(shaderType);

		glShaderSource(index, 1, &chars, 0);
		glCompileShader(index);

		//���������, ��������� �� ��������������� ������

		CheckShaderForErrors(index, GL_COMPILE_STATUS, shaderLogName);

		glAttachShader(programID, index);

		CHECK_GL_ERROR

		return index;

		CHECK_GL_ERROR
	}

	void Effect::CreateProgram()
	{
		for (const auto& attrib : attribs)
			glBindAttribLocation(programID, attrib.second, attrib.first.c_str());

		glLinkProgram(programID);
		CheckProgramForErrors(GL_LINK_STATUS);

		CHECK_GL_ERROR
	}

	void Effect::Use() const
	{
		glUseProgram(programID);

		CHECK_GL_ERROR
	}

	void Effect::CheckShaderForErrors(GLuint effect, GLenum param, const char* shaderLogName)
	{
		GLint result, length;

		char* log;

		// -- �������� ��� �������
		glGetShaderiv(effect, GL_INFO_LOG_LENGTH, &length);
		log = new char[length];
		glGetShaderInfoLog(effect, length, &result, log);

		ErrorLog::Log("--// " + name + " program, " + shaderLogName + " shader log //--");
		ErrorLog::Log(log);
		delete log;

		ErrorLog::Log("--// end of log //--");

		glGetShaderiv(effect, param, &result);

		if (result != GL_TRUE)
		{
			glDeleteShader(effect);
			ErrorLog::Log(std::string(shaderLogName) + " shader deleted in " + name + " program");
		}

		CHECK_GL_ERROR
	}

	void Effect::CheckProgramForErrors(GLenum param)
	{
		GLint result, length;

		char* log;

		// -- ������� ��� ��������� ���������
		glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &length);
		log = new char[length];
		glGetProgramInfoLog(programID, length, &result, log);

		// -- ����� ��������� �� ������ � ���������
		ErrorLog::Log("--// " + name + " shader program // --");
		ErrorLog::Log(log);
		ErrorLog::Log("--// end program log // --");

		glGetProgramiv(programID, param, &result);

		if (result != GL_TRUE)
		{
			glDeleteProgram(programID);
			ErrorLog::Log("--// program deleted // --\r\n");
		}

		CHECK_GL_ERROR

		delete log;
	}

	string Effect::GetInfoLog() const
	{
		GLint length;
		GLint trash;

		glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &length);

		char* logBuf = new char[length];
		glGetProgramInfoLog(programID, length, &trash, (GLchar*)logBuf);

		string res(logBuf);
		delete logBuf;

		CHECK_GL_ERROR

		return res;
	}

	GLint Effect::AllocateUniformVar(const string& varName) const
	{
		GLint location = glGetUniformLocation(programID, varName.c_str());
		
		if (location != -1)
		{
			uniforms.insert(uniforms.end(),
				pair<string, GLint>(string(varName), location));
		}
		else
		{
			if (errorUniforms.find(varName) == errorUniforms.end())
			{
				ErrorLog::Log("Effect error: can't find uniform var " + std::string(varName.c_str()) + " in " + std::string(name.c_str()));
				errorUniforms.insert(varName);
			}
		}

		CHECK_GL_ERROR

		return location;
	}

	GLint Effect::AllocateAttribVar(const string& attribName) const
	{
		GLint location = glGetAttribLocation(programID, attribName.c_str());

		if (location != -1)
		{
			attribs.insert(attribs.end(),
				pair<string, GLint>(string(attribName), location));
		}
		else
		{
			if (errorUniforms.find(attribName) == errorUniforms.end())
			{
				ErrorLog::Log("Error in " + std::string(attribName.c_str()) + ": can't find attrib " + name);
				errorUniforms.insert(attribName);
			}
		}

		CHECK_GL_ERROR

		return location;
	}

	GLint Effect::AllocateUniformBlock(const string& blockName) const
	{
		GLint location = glGetUniformBlockIndex(programID, blockName.c_str());

		if (location != -1)
		{
			uniformBlocks.insert(uniformBlocks.end(),
				pair<string, GLint>(string(blockName), location));
		}
		else
		{
			if (errorUniforms.find(blockName) == errorUniforms.end())
			{
				ErrorLog::Log("Error in " + name + ": can't find uniform block " + blockName + "\n\r");
				errorUniforms.insert(blockName);
			}
		}

		CHECK_GL_ERROR

		return location;
	}

	GLint Effect::GetAttribIndex(const string& varName) const
	{
		auto iter = attribs.find(varName);

		if (iter != attribs.end())
			return iter->second;
		else 
			return -1;
	}

	GLint Effect::GetUniformVar(const string& varName) const
	{
		auto iter = uniforms.find(varName);

		if (iter != uniforms.end())
			return iter->second;
		else 
			return -1;
	}

	void Effect::SetUniform1i(const string& varName, const GLint& v) const
	{
		auto iter = uniforms.find(varName);

		if (uniforms.size() == 0 || iter == uniforms.end())
			glUniform1i(AllocateUniformVar(varName), v);
		else
			glUniform1i(iter->second, v);

		CHECK_GL_ERROR
	}

	void Effect::SetUniform1iArray(const string& varName, unsigned int& num, const int* vs) const
	{
		auto iter = uniforms.find(varName);

		if (iter == uniforms.end())
			for (unsigned int i = 0; i < num; i++)
				glUniform1i(AllocateUniformVar(varName) + i, *(vs + i));
		else
			for (unsigned int i = 0; i < num; i++)
				glUniform1i(iter->second + i, *(vs + i));

		CHECK_GL_ERROR
	}

	void Effect::SetUniform1fArray(const string& varName, unsigned int& num, const float* vs) const
	{
		auto iter = uniforms.find(varName);

		if (iter == uniforms.end())
			for (unsigned int i = 0; i < num; i++)
				glUniform1f(AllocateUniformVar(varName) + i, *(vs + i));
		else
			for (unsigned int i = 0; i < num; i++)
				glUniform1f(iter->second + i, *(vs + i));

		CHECK_GL_ERROR
	}

	void Effect::SetUniformBlock(const std::string& blockName, const GLuint& bufferID, const GLuint& uniformBlockBinding, const GLsizei& size) const
	{
		glBindBufferRange(GL_UNIFORM_BUFFER, uniformBlockBinding, bufferID, 0, size);

		CHECK_GL_ERROR
		
		auto iter = uniformBlocks.find(blockName);

		if (iter == uniformBlocks.end())
		{
			auto loc = AllocateUniformBlock(blockName);

#ifdef _DEBUG
			if (loc == -1) return;
#endif

			glUniformBlockBinding(programID, loc, uniformBlockBinding);
			CHECK_GL_ERROR
		}
		else
		{
			glUniformBlockBinding(programID, iter->second, uniformBlockBinding);
			CHECK_GL_ERROR
		}
	}

	void Effect::SetUniform2f(const string& varName, const glm::vec2& v) const
	{
		auto iter = uniforms.find(varName);

		if (iter == uniforms.end())
			glUniform2fv(AllocateUniformVar(varName), 1, glm::value_ptr(v));
		else
			glUniform2fv(iter->second, 1, glm::value_ptr(v));

		CHECK_GL_ERROR
	}

	void Effect::SetUniform3f(const std::string& varName, const glm::vec3& v) const
	{
		auto iter = uniforms.find(varName);

		if (iter == uniforms.end())
			glUniform3fv(AllocateUniformVar(varName), 1, glm::value_ptr(v));
		else
			glUniform3fv(iter->second, 1, glm::value_ptr(v));

		CHECK_GL_ERROR
	}

	void Effect::SetUniform3fArray(const std::string& varName, const unsigned int& num, const glm::vec3* vs) const
	{
		auto iter = uniforms.find(varName);

		if (iter == uniforms.end())
			for (unsigned int i = 0; i < num; ++i)
				glUniform2fv(AllocateUniformVar(varName), 1, glm::value_ptr(*(vs + i)));
		else
			for (unsigned int i = 0; i < num; ++i)
				glUniform2fv(iter->second, 1, glm::value_ptr(*(vs + i)));

		CHECK_GL_ERROR
	}

	void Effect::SetUniform1f(const std::string& varName, const float& v) const
	{
		auto iter = uniforms.find(varName);

		if (iter == uniforms.end())
			glUniform1f(AllocateUniformVar(varName), v);
		else
			glUniform1f(iter->second, v);

		CHECK_GL_ERROR
	}

	void Effect::SetUniformMatrix4f(const string& varName, const float* buf) const
	{
		auto iter = uniforms.find(varName);

		if (iter == uniforms.end())
			glUniformMatrix4fv(AllocateUniformVar(varName), 1, GL_FALSE, (GLfloat*)buf);
		else
			glUniformMatrix4fv((GLint)(iter->second), 1, GL_FALSE, (GLfloat*)buf);

		CHECK_GL_ERROR
	}

	void Effect::InvokeDeferredUniformSetters()
	{
		for (auto& uniformSetterP : deferredSetters)
		{
			uniformSetterP->Invoke();
			
			CHECK_GL_ERROR
		}

		deferredSetters.clear();
	}
}