#include "stdafx.h"

#include "Skeleton.h"
#include "GameplayState.h"
#include "LuaHelper.h"
#include "Position.h"

namespace mg
{
	glm::mat4 Skeleton::GetJointTransformFromBoneBodyTransform(const BoneBody &boneBody)
	{
		btTransform bodyTransform = boneBody.body->getWorldTransform();

		// 1) ����� �� ��� ������� ����
		bodyTransform = bodyTransform * btTransform(btMatrix3x3::getIdentity(), boneBody.axisOffset).inverse();

		// 3) ���������� �������
		bodyTransform.getOrigin() /= MODEL_SCALE;

		// 4) ��������� �� �������� rootInverseTransform 
		bodyTransform = MatrixHelper::ToBTTransfom(rootNodeInverseTransform) * bodyTransform;

		return  MatrixHelper::ToGLMMatrix(bodyTransform);
	}

	void Skeleton::ResetJointTransforms()
	{
		for (auto &node : boneNodes)
		{
			if (node.second.transformedByChannel)
			{
				node.second.�urrentTransform = node.second.channelTransform;
			}
			else
			{
				node.second.�urrentTransform = node.second.basicTransform;
			}
		}
	}

	void Skeleton::UpdateNode(const std::string& nodeName, glm::mat4& parentTransform, const bool &alignToRigidBody)
	{
		auto& boneNode = boneNodes[nodeName];

		if (!alignToRigidBody)
		{
			boneNode.�urrentTransform = parentTransform * boneNode.�urrentTransform;
		}
		else
		{
			auto iter = boneBodies.find(nodeName);

			if (iter == boneBodies.end())
			{
				boneNode.�urrentTransform = parentTransform * boneNode.�urrentTransform;
			}
			else
			{
				boneNode.�urrentTransform = GetJointTransformFromBoneBodyTransform(iter->second);
			}
		}

		// ���� � �������� ���� ���� ����������
		for (auto& childName : boneNode.childNames)
		{
			UpdateNode(childName, boneNode.�urrentTransform, alignToRigidBody);
		}
	}

	//--- SkeletonComponentManager
	void SkeletonComponentManager::AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		if (!gps->HasElement<Skeleton>(table.GetByName("name").ToString()))
			return;

		auto skeleton = Skeleton(*gps->CreateElement<Skeleton>(table.GetByName("name").ToString()));

		// ����������� ���������� ������ (��������������� �-����)

		std::unordered_map<std::string, SceneVector> boneMainAxes;
		auto boneBindingPointsTable = LuaHelper::GetTable(table, "boneBindingPoints");

		for (LuaPlus::LuaTableIterator iter(boneBindingPointsTable); iter; iter.Next())
		{
			auto currTable = iter.GetValue();

			std::string jointName = iter.GetKey().ToString();
			SceneVector head(LuaHelper::TableToVec3(LuaHelper::GetTable(currTable, "head")));
			skeleton.boneHeadBindingPoints.insert(std::make_pair(jointName, head));

			SceneVector tail(LuaHelper::TableToVec3(LuaHelper::GetTable(currTable, "tail")));
				
			SceneVector newBasisY = (tail - head).Normalized();
			SceneVector newBasisX = newBasisY.Cross(SceneVector(0.f, 1.f, 0.f));

			boneMainAxes.insert(std::make_pair(jointName, newBasisY));
		}

		// ���� ������

		auto boneRigidBodiesTable = LuaHelper::GetTable(table, "boneRigidBodies");

		for (LuaPlus::LuaTableIterator iter(boneRigidBodiesTable); iter; iter.Next())
		{
			auto& currTable = iter.GetValue();

			btCollisionShape *shape = nullptr;

			SceneVector newAxisX, newAxisY, newAxisZ;

			std::string type = LuaHelper::GetString(currTable, "type");

			SceneVector origin;
			
			// ����� � ����������� ���� �����
			SceneVector axisOffset(0.f), centerOfMassOffset(0.f);
			float mass = LuaHelper::GetFloat(currTable, "mass");

			std::string firstJoint, secondJoint;

			// ����, ������� �������� �� ���� ��������
			if (type == "capsule" || type == "cylinder" ||
				type == "capsuleX" || type == "capsuleZ" ||
				type == "cylinderX" || type == "cylinderZ")
			{
				firstJoint = LuaHelper::GetString(currTable, "firstJoint");
				secondJoint = LuaHelper::GetString(currTable, "secondJoint");

				SceneVector boneMainAxis = skeleton.boneHeadBindingPoints[secondJoint] - skeleton.boneHeadBindingPoints[firstJoint];
				newAxisY = boneMainAxis.Normalized();

				if (newAxisY.Y() != 1.f)
				{
					newAxisX = newAxisY.Cross(SceneVector(0.0, 1.0, 0.0)).Normalized();
					newAxisZ = newAxisX.Cross(newAxisY).Normalized();
				}
				else
				{
					newAxisZ = SceneVector(0.f, 0.f, 1.f);
					newAxisX = SceneVector(1.f, 0.f, 0.f);
				}

				float radius = LuaHelper::GetFloat(currTable, "radius") / MODEL_SCALE;

				origin = skeleton.boneHeadBindingPoints.at(firstJoint);

				if (type == "capsule")
				{
					float capsuleHeight = boneMainAxis.Length() - radius * 2.f;
					assert(capsuleHeight > 0.f);
					shape = new btCapsuleShape(radius, capsuleHeight);
					axisOffset.Y() = boneMainAxis.Length() / 2.f;
				}
				else if (type == "capsuleZ")
				{
					float capsuleHeight = boneMainAxis.Length() - radius * 2.f;
					assert(capsuleHeight > 0.f);
					shape = new btCapsuleShapeZ(radius, capsuleHeight);
					std::swap(newAxisZ, newAxisY);
					axisOffset.Z() = boneMainAxis.Z() / 2.f;
				}
				else if (type == "capsuleX")
				{
					float capsuleHeight = boneMainAxis.Length() - radius * 2.f;
					assert(capsuleHeight > 0.f);
					shape = new btCapsuleShapeX(radius, capsuleHeight);
					std::swap(newAxisX, newAxisY);
					axisOffset.X() = boneMainAxis.Length() / 2.f;
				}
				else if (type == "cylinder")
				{
					shape = new btCylinderShape(btVector3(radius, boneMainAxis.Length() / 2.f, radius));
					axisOffset.Y() = boneMainAxis.Length() / 2.0;
				}
				else if (type == "cylinderX")
				{
					shape = new btCylinderShapeX(btVector3(radius, boneMainAxis.Length() / 2.f, radius));
					std::swap(newAxisX, newAxisY);
					axisOffset.X() = boneMainAxis.Length() / 2.f;
				}
				else if (type == "cylinderZ")
				{
					shape = new btCylinderShapeZ(btVector3(radius, boneMainAxis.Length() / 2.f, radius));
					std::swap(newAxisZ, newAxisY);
					axisOffset.Z() = boneMainAxis.Length() / 2.0;
				}
			}
			else if (type == "sphere") // ����, ���������� �� ������ �������
			{
				std::string joint = LuaHelper::GetString(currTable, "joint");
				// TODO: ������ ��� ����� ��������� / ������� �� MODEL_SCALE
				float radius = LuaHelper::GetFloat(currTable, "radius");

				assert(skeleton.boneHeadBindingPoints.find(joint) != skeleton.boneHeadBindingPoints.end());

				shape = new btSphereShape(radius);
				origin = skeleton.boneHeadBindingPoints.at(joint);
				firstJoint = secondJoint = joint;

				newAxisX = SceneVector(1.f, 0.f, 0.f);
				newAxisY = SceneVector(0.f, 1.f, 0.f);
				newAxisZ = SceneVector(0.f, 0.f, 1.f);

				auto axisOffsetTable = LuaHelper::GetTable(currTable, "axisOffset", false);

				if (!axisOffsetTable.IsNil())
				{
					axisOffset.X() = LuaHelper::GetFloat(axisOffsetTable, "x", false, 0.f);
					axisOffset.Y() = LuaHelper::GetFloat(axisOffsetTable, "y", false, 0.f);
					axisOffset.Z() = LuaHelper::GetFloat(axisOffsetTable, "z", false, 0.f);
				}
			}
			else if (type == "singleJointCylinder")
			{
				std::string joint = LuaHelper::GetString(currTable, "joint");
				// TODO: ������ ��� ����� ��������� / ������� �� MODEL_SCALE
				float radius = LuaHelper::GetFloat(currTable, "radius");
				float length = LuaHelper::GetFloat(currTable, "length");

				assert(skeleton.boneHeadBindingPoints.find(joint) != skeleton.boneHeadBindingPoints.end());

				shape = new btCylinderShape(btVector3(radius, length / 2.f, radius));

				origin = skeleton.boneHeadBindingPoints.at(joint);
				firstJoint = secondJoint = joint;

				newAxisX = SceneVector(1.f, 0.f, 0.f);
				newAxisY = SceneVector(0.f, 1.f, 0.f);
				newAxisZ = SceneVector(0.f, 0.f, 1.f);
			}
			else if (type == "singleJointCylinderCompound")
			{
				std::string joint = LuaHelper::GetString(currTable, "joint");
				// TODO: ������ ��� ����� ��������� / ������� �� MODEL_SCALE
				float radius = LuaHelper::GetFloat(currTable, "radius");
				float length = LuaHelper::GetFloat(currTable, "length");

				assert(skeleton.boneHeadBindingPoints.find(joint) != skeleton.boneHeadBindingPoints.end());

				auto *childShape = new btCylinderShape(btVector3(radius, length / 2.f, radius));
				
				origin = skeleton.boneHeadBindingPoints.at(joint);
				firstJoint = secondJoint = joint;

				auto localTransform = LuaHelper::TableToBTTransform(LuaHelper::GetTable(currTable, "localTransform"));

				shape = new btCompoundShape();
				reinterpret_cast<btCompoundShape*>(shape)->addChildShape(localTransform, childShape);

				centerOfMassOffset = LuaHelper::TableToVec3(LuaHelper::GetTable(currTable, "centerOfMassOffset", false));
			}

			shape->setLocalScaling(btVector3(MODEL_SCALE, MODEL_SCALE, MODEL_SCALE));

			bool invertedAxis = false;

			// TODO: ��������, ����� �� ����� ������ startTransform
			btTransform startTransformRotation;

			if (invertedAxis = LuaHelper::GetBoolean(currTable, "invertedAxis", false))
			{
				axisOffset = -axisOffset;
				startTransformRotation.setFromOpenGLMatrix(glm::value_ptr(
					glm::rotate<double>(glm::pi<double>(), glm::dvec3(1.0, 0.0, 0.0))));
			}
			else
			{
				startTransformRotation.setFromOpenGLMatrix(glm::value_ptr<double>(glm::dmat4(1.0)));
			}

			std::vector<std::string> ignoredCollisionBones;

			// ������������� ������������ �� ����� �������� ������ � �������
			if (LuaHelper::GetBoolean(currTable, "ignoreCollisionWithAllBones", false))
			{
				for (auto &pair : skeleton.boneHeadBindingPoints)
				{
					ignoredCollisionBones.push_back(pair.first);
				}
			}
			else
			{
				LuaPlus::LuaObject ignoredCollisionBonesTable;

				// ������������� ������������ ���� � �������������� ������
				if (!(ignoredCollisionBonesTable = LuaHelper::GetTable(currTable, "ignoredCollisionBones", false)).IsNil())
				{
					for (LuaPlus::LuaTableIterator bonesIter(ignoredCollisionBonesTable); bonesIter; bonesIter.Next())
					{
						ignoredCollisionBones.push_back(bonesIter.GetValue().GetString());
					}
				}
			}

			bool isKinematic = LuaHelper::GetBoolean(currTable, "isKinematic", false);
			bool isCharacter = LuaHelper::GetBoolean(currTable, "isCharacter", false);

			axisOffset *= MODEL_SCALE;

			btTransform startTransform;
			startTransform.setBasis(
				btMatrix3x3(
					newAxisX.X(), newAxisY.X(), newAxisZ.X(),
					newAxisX.Y(), newAxisY.Y(), newAxisZ.Y(),
					newAxisX.Z(), newAxisY.Z(), newAxisZ.Z()));

			startTransform.setOrigin(origin * MODEL_SCALE);
			startTransform = startTransform * startTransformRotation *
				btTransform(btQuaternion::getIdentity(), axisOffset + MODEL_SCALE);


			int additionalFlags = ~(btCollisionObject::CF_STATIC_OBJECT | btCollisionObject::CF_KINEMATIC_OBJECT);
			additionalFlags = isKinematic ? btCollisionObject::CF_NO_CONTACT_RESPONSE | btCollisionObject::CF_KINEMATIC_OBJECT : additionalFlags;
			additionalFlags = isCharacter ? btCollisionObject::CF_CHARACTER_OBJECT : additionalFlags;
			
			std::string collisionGroup = LuaHelper::GetString(currTable, "collisionGroup", false, "character_joints");
			std::string collisionMask = LuaHelper::GetString(currTable, "collisionMask", false, "character_joints");

			auto *body = gps->GetPhysics().CreateRigidBody(mass, startTransform, shape, SceneVector(0), 
				-1, COLLISION_GROUPS.at(collisionGroup), COLLISION_MASKS.at(collisionMask));

			bool collides = (COLLISION_GROUPS.at("level") & COLLISION_MASKS.at("character_boxes")) != 0;
			collides = collides && (COLLISION_GROUPS.at("character_boxes") & COLLISION_MASKS.at("level"));

			body->setCenterOfMassTransform(btTransform(btMatrix3x3::getIdentity(), centerOfMassOffset));
			
			body->setUserIndex(static_cast<int>(entity.id().index()));
			//body->setGravity(btVector3(0.0, 0.0, 0.0));
			body->setWorldTransform(startTransform);
			body->setContactStiffnessAndDamping(9999.f, 9999.f);
			// ��������� �������� ����
			LuaPlus::LuaObject dampingTable = LuaHelper::GetTable(currTable, "damping", false);

			if (!dampingTable.IsNil())
			{
				body->setDamping(LuaHelper::GetFloat(dampingTable, "linear", false, 0.05f), 
					LuaHelper::GetFloat(dampingTable, "angular", false, 20.05f));
			}
			else
			{
				body->setDamping(0.05f, 20.05f);
			}

			body->setDeactivationTime(LuaHelper::GetFloat(currTable, "deactivationTime", false, 1.8f));

			LuaPlus::LuaObject sleepingThresholdTable = LuaHelper::GetTable(currTable, "sleepingThreshold", false);

			if (!sleepingThresholdTable.IsNil())
			{
				body->setSleepingThresholds(LuaHelper::GetFloat(sleepingThresholdTable, "lin", false, 1.6f),
					LuaHelper::GetFloat(sleepingThresholdTable, "ang", false, 2.5f));
			}
			else
			{
				body->setSleepingThresholds(1.6f, 2.5f);
			}

			// ��������� �� ���� �� �����
			bool isCurrBodyAffectable = LuaHelper::GetBoolean(currTable, "affectable", false, false);

			// ���������� ���������� � ���� ����� ��� ���
			if (LuaHelper::GetBoolean(currTable, "debug", false))
			{
				body->setCustomDebugColor(btVector3(0.0, 0.0, 1.0));
			}
			else
			{
				body->setCustomDebugColor(btVector3(0.0, 0.0, 0.0));
			}

			skeleton.boneBodies.insert(
				std::make_pair(iter.GetKey().GetString(), 
				std::move(BoneBody(firstJoint, secondJoint, body, startTransform, axisOffset, ignoredCollisionBones, invertedAxis, 
					isCurrBodyAffectable)))
			);
		}

		// ����������� ������������ ������������
		for (auto &bb : skeleton.boneBodies)
		{
			for (auto &ignoredCollisionBoneName : bb.second.ignoredCollisionBones)
			{
				auto iter = skeleton.boneBodies.find(ignoredCollisionBoneName);
					
				if (iter != skeleton.boneBodies.end()) // ���� ����� 
				{
					if (&ignoredCollisionBoneName == &bb.first)
						continue;

					bb.second.body->setIgnoreCollisionCheck(iter->second.body, true);
					iter->second.body->setIgnoreCollisionCheck(bb.second.body, true);
				}
			}
		}

		// ��� ����������� - 6DOF
		LuaPlus::LuaObject constraintsTable = LuaHelper::GetTable(table, "constraints", false);

		if (!constraintsTable.IsNil())
		{
			for (LuaPlus::LuaTableIterator iter(constraintsTable); iter; iter.Next())
			{
				auto currTable = iter.GetValue();

				std::string firstBoneName = LuaHelper::GetString(currTable, "firstBody");
				std::string secondBodyName = LuaHelper::GetString(currTable, "secondBody");

				auto firstBodyIter = skeleton.boneBodies.find(firstBoneName);
				auto secondBodyIter = skeleton.boneBodies.find(secondBodyName);

				assert(firstBodyIter != skeleton.boneBodies.end() && secondBodyIter != skeleton.boneBodies.end());

				auto *firstBody = firstBodyIter->second.body;
				auto *secondBody = secondBodyIter->second.body;

				// ����������� ���� t-����

				glm::dmat4 firstT, secT;

				firstBodyIter->second.startTransform.getOpenGLMatrix(glm::value_ptr(firstT));
				secondBodyIter->second.startTransform.getOpenGLMatrix(glm::value_ptr(secT));

				glm::dmat3 firstBasis(firstT), secBasis(secT);
				glm::dvec3 secBasisInFirstBasis = glm::inverse(firstBasis) * secBasis * glm::dvec3(1.0);

				//----------------

				auto basisA = firstBodyIter->second.startTransform.getBasis().inverse();
				btMatrix3x3 frameABasis = firstBodyIter->second.startTransform.getBasis().inverse();

				// ���� ���, ��������, ����, �� ������ ������������ ����

				if (LuaHelper::GetBoolean(currTable, "axisShift", false))
				{
					frameABasis[0] = basisA.getRow(1);
					frameABasis[1] = basisA.getRow(2);
					frameABasis[2] = basisA.getRow(0);
				}

				auto basisB = secondBodyIter->second.startTransform.getBasis().inverse();
				btMatrix3x3 frameBBasis = basisB;

				if (LuaHelper::GetBoolean(currTable, "axisShift", false))
				{
					frameBBasis[0] = basisB.getRow(1);
					frameBBasis[1] = basisB.getRow(2);
					frameBBasis[2] = basisB.getRow(0);
				}

				btTransform frameA(frameABasis, firstBodyIter->second.axisOffset);
				btTransform frameB(frameBBasis, -secondBodyIter->second.axisOffset);

				btGeneric6DofConstraint *constraint = new btGeneric6DofConstraint(
					*firstBodyIter->second.body, *secondBodyIter->second.body, frameA, frameB, false);
				secondBodyIter->second.body->addConstraintRef(constraint);

				secondBodyIter->second.constraintDebug = LuaHelper::GetBoolean(currTable, "debug", false);

				// -- ��������� ����������� ����� �������

				auto angLo = LuaHelper::TableToVec3(currTable["angularLowLimit"]);
				constraint->setAngularLowerLimit(btVector3(angLo.x, angLo.y, angLo.z));
				auto angUp = LuaHelper::TableToVec3(currTable["angularUpperLimit"]);
				constraint->setAngularUpperLimit(btVector3(angUp.x, angUp.y, angUp.z));

				auto linLo = LuaHelper::TableToVec3(currTable["linearLowLimit"]);
				constraint->setLinearLowerLimit(btVector3(linLo.x, linLo.y, linLo.z));
				auto linUp = LuaHelper::TableToVec3(currTable["linearUpperLimit"]);
				constraint->setLinearUpperLimit(btVector3(linUp.x, linUp.y, linUp.z));

				constraint->setEnabled(true);

				gps->GetPhysics().dynamicsWorld->addConstraint(constraint, true);
			}
		}

		// ������� ���� �������� ��� ������
		auto orientAngleComponentsTable = LuaHelper::GetTable(table, "orientAngleComponents", false);

		skeleton.orientAngleComponents = std::unordered_map<std::string, float> {
			std::make_pair(SPINE_01_NODE_NAME, 0.5), std::make_pair(HEAD_ROTATION_NODE_NAME, 0.5)
		};

		if (!orientAngleComponentsTable.IsNil())
		{
			skeleton.orientAngleComponents.clear();

			for (LuaPlus::LuaTableIterator iter(orientAngleComponentsTable); iter.IsValid(); iter.Next())
			{
				assert(iter.GetValue().IsConvertibleToNumber() && iter.GetKey().IsString());

				std::string boneName = iter.GetKey().ToString();
				float angleComponent = iter.GetValue().ToNumber();

				skeleton.orientAngleComponents.insert(std::make_pair(boneName, angleComponent));
			}
		}

		entity.assign<Skeleton>(skeleton);
	}

	LuaPlus::LuaObject SkeletonComponentManager::GetComponentTable(entityx::Entity& entity) const
	{
		LuaPlus::LuaObject res;

		if (entity.has_component<Skeleton>())
		{
			auto componentHandler = entity.component<Skeleton>();
			res.AssignNewTable(gps->GetLuaState());

			res.SetString("name", componentHandler->name.c_str());
		}
		else
		{
			res.AssignNil();
		}

		return res;
	};

	LuaPlus::LuaObject SkeletonComponentManager::ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		if (!entity.has_component<Skeleton>() && !gps->HasElement<Skeleton>(table.GetByName("name").ToString()))
			return LuaPlus::LuaObject().AssignNil();

		entity.remove<Skeleton>();
		entity.assign<Skeleton>(*(gps->CreateElement<Skeleton>(table.GetByName("name").ToString())));
	}

	// ���������� ���������� ������ �������� ���� ����� ������� ��������s
	int SkeletonComponentManager::GetBoneBodyWorldArrayIndex(const LuaPlus::LuaObject &entityTable, const LuaPlus::LuaObject &boneName)
	{
		auto entity = gps->GetEntity(entityTable);

		int res = entity.component<Skeleton>()->boneBodies.at(boneName.GetString()).body->getWorldArrayIndex();

		return res;
	}

	// ������������ ���� ���, ��� ��� z � ������� ���� ���������� ���������� �� ����������� � ���������� �����
	void SkeletonComponentManager::OrientBoneBodyToPoint(const LuaPlus::LuaObject &entityTable, const char *boneBodyName,
		const LuaPlus::LuaObject &frameQuat, const LuaPlus::LuaObject &mapPoint)
	{
		auto &boneBodies = gps->GetEntity(entityTable).component<Skeleton>()->boneBodies;
		auto bbIter = boneBodies.find(boneBodyName);

		assert(bbIter != boneBodies.end());

		auto frameMx = MatrixHelper::ToGLMMatrix(bbIter->second.body->getWorldTransform() * 
			btTransform(LuaHelper::TableToBTQuat(frameQuat)));
		auto frameZAxis = SceneVector(frameMx[2][0], frameMx[2][1], frameMx[2][2]).Normalized();
		auto dirScene = (
			SceneVector::FromMapCoords(mapPoint) - SceneVector(bbIter->second.body->getWorldTransform().getOrigin())
		).Normalized();

		// �������� � ���������, ������������ ����
		btQuaternion res = btQuaternion(
			frameZAxis.XZ().Normalized().Cross(dirScene.XZ().Normalized()).Normalized(),
			glm::angle((glm::vec3)frameZAxis.XZ().Normalized(), (glm::vec3)dirScene.XZ().Normalized())
		);

		GUI::GetInstance().Watch("res quat axis x", res.getAxis().x());
		GUI::GetInstance().Watch("res quat axis y", res.getAxis().y());
		GUI::GetInstance().Watch("res quat axis z", res.getAxis().z());

		GUI::GetInstance().Watch("res quat angle", res.getAngle());

		//TODO: ��������, ����� ������� � bone body ���������� ��� �������� ������������� �������
		bbIter->second.additionalTransform = btTransform(res * bbIter->second.body->getWorldTransform().getRotation(), 
			bbIter->second.body->getWorldTransform().getOrigin());
	}

	void SkeletonComponentManager::SetBoneOrientationRatio(const LuaPlus::LuaObject &entityTable, const float &bodyComponent, const float &headComponent)
	{
		auto &angleComponents = gps->GetEntity(entityTable).component<Skeleton>()->orientAngleComponents;

		angleComponents[SPINE_01_NODE_NAME] = bodyComponent;
		angleComponents[HEAD_NODE_NAME] = headComponent;
	}
}