#pragma once

#include "lua\LuaPlus.h"

#include "ComponentManager.h"

namespace mg
{
	struct Inventory
	{
		Inventory(const int &width, const int &height, const LuaPlus::LuaObject &itemsTable) :
			width(width), height(height), itemsTable(itemsTable)
		{
		}

		~Inventory() {}

		int width, height;
		LuaPlus::LuaObject itemsTable; // ������� � �������� ���������
	};

	// Inventory component manager

	//class WeaponComponentManager : public ComponentManager
	//{
	//public:
	//	WeaponComponentManager() : ComponentManager("weapon") {};
	//	~WeaponComponentManager() = default;

	//	inline virtual void AssignComponent(entityx::Entity& entity, LuaPlus::LuaObject& table, GameplayState* gps) const override
	//	{
	//		if (!entity.has_component<Weapon>())
	//		{
	//			bool isOk = true;

	//			std::string meshName;

	//			if (!(table.GetByName("mesh").GetRef() != -1 && table.GetByName("mesh").IsString()))
	//			{
	//				isOk = false;
	//			}

	//			meshName = table.GetByName("mesh").ToString();

	//			if (!gps->HasElement<Mesh>(meshName))
	//				isOk = false;

	//			btCollisionShape* shape = nullptr;
	//			mgGhostObject* collisionObject = nullptr;

	//			if (table.GetByName("collisionObject").GetRef() != -1)
	//			{
	//				if (!table.GetByName("collisionObject").IsTable()
	//					|| !(table["collisionObject"].GetByName("shape").GetRef() != -1 && table["collisionObject"].GetByName("shape").IsString()))
	//					isOk = false;

	//				if (std::string(table["collisionObject"]["shape"].ToString()) == "capsule" && isOk)
	//				{
	//					if ((table["collisionObject"].GetByName("radius").GetRef() != -1 && table["collisionObject"].GetByName("radius").IsNumber())
	//						&& (table["collisionObject"].GetByName("height").GetRef() != -1 && table["collisionObject"].GetByName("height").IsNumber()))
	//					{
	//						// ���� ��� ������� � ��� ��������� �� �����
	//						shape = new btCapsuleShape(table["collisionObject"]["radius"].GetFloat(), table["collisionObject"]["height"].GetFloat());

	//						collisionObject = new mgGhostObject(entity.id().index(), true);
	//						collisionObject->setCollisionShape(shape);
	//						gps->GetPhysics().dynamicsWorld->addCollisionObject(collisionObject, 2, 1);
	//					}
	//					else isOk = false;
	//				}
	//				else isOk = false;
	//			}

	//			glm::mat4 offsetMat = LuaHelper::TableToMat4(table["offsetMx"]);

	//			int meshID = -1;
	//			if (entity.has_component<MeshBatch>())
	//			{
	//				meshID = entity.component<MeshBatch>()->meshes[0].ID;
	//			}
	//			else isOk = false;

	//			if (isOk)
	//			{
	//				entity.assign<Weapon>(table, collisionObject, shape, *gps->CreateElement<Mesh>(meshName,
	//					meshID + CHARS_MESHES_COUNT), offsetMat); // �������� �������� meshID + MAX_MESHES, ����� ����������, ��� ��� �������� �������
	//			}
	//		}
	//	}

	//	inline virtual LuaPlus::LuaObject GetComponentTable(entityx::Entity& entity, LuaPlus::LuaState* luaState) const override
	//	{
	//		if (entity.has_component<Weapon>())
	//		{
	//			auto *component = entity.component<Weapon>().get();
	//			return component->table;
	//		}
	//		else
	//		{
	//			LuaPlus::LuaObject res(luaState);
	//			res.AssignNil();

	//			return res;
	//		}
	//	}

	//	inline virtual LuaPlus::LuaObject ModifyComponent(entityx::Entity& entity, LuaPlus::LuaObject& table, GameplayState* gps) const override
	//	{
	//		LuaPlus::LuaObject res;

	//		return res;
	//	}

	//	inline virtual void RemoveComponent(entityx::Entity& entity, GameplayState* gps) const
	//	{
	//		if (entity.has_component<Weapon>())
	//		{
	//			entity.component<Weapon>()->removed = true;
	//		}
	//	}
	//};
}