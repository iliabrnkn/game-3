#pragma once

#include <vector>
#include <memory>

#include <GL\glew\glew.h>

#include "Texture.h"
#include "Effect.h"
#include "VBO.h"
#include "VertTypes.h"
#include "Camera.h"

namespace mg {

	class ApplicationState;

	class FBO
	{
	public:
		void Init();
		void Bind();
		void Unbind();
		void AttachTexture(const GLenum& attachment, const GLuint& textureID, const GLenum& target = GL_TEXTURE_2D);
		void CheckForErrors();

	protected:
		GLuint fboVAO, fbo, rb;
	};
}