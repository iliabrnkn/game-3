#pragma once

#include "stdafx.h"

#include <vector>

#include <GL\glew\glew.h>
#include "VertTypes.h"
#include "ErrorLog.h"

using namespace std;

namespace mg {

	template<typename T>
	class VBO
	{
		GLuint ID;
		GLenum target;
		size_t maxSize;
		int currentOffset; // current offset in T-size

	public:
		VBO();
		~VBO();

		void Bind();
		void Unbind();
		void Free();
		void Init(const GLsizei& initialSize, const GLenum& targetInit);
		void Fill(const T& data);
		void Fill(const vector<T>& vertices);
		void Clear();
		void ClearFromOffset(const GLintptr &offset);
		GLint GetCurrentOffset() const;
		GLuint GetID() const;
		size_t GetMaxSize() const;
	};

	template<typename T>
	VBO<T>::VBO() :
		currentOffset(0)
	{
	}

	template<typename T>
	VBO<T>::~VBO()
	{

	}

	template<typename T>
	size_t VBO<T>::GetMaxSize() const
	{
		return maxSize;
	}

	template<typename T>
	void VBO<T>::Init(const GLsizei& initialSize, const GLenum& targetInit)
	{
		target = targetInit;
		maxSize = initialSize;

		glGenBuffers(1, &ID);
		glBindBuffer(target, ID);
		glBufferData(target, initialSize, NULL, GL_DYNAMIC_DRAW);
		glBindBuffer(target, 0);

		CHECK_GL_ERROR
	}

	template<typename T>
	void VBO<T>::Bind()
	{
		glBindBuffer(target, ID);

		CHECK_GL_ERROR
	}

	template<typename T>
	void VBO<T>::Unbind()
	{
		glBindBuffer(target, 0);

		CHECK_GL_ERROR
	}

	template<typename T>
	void VBO<T>::Free()
	{
		glDeleteBuffers(1, &ID);

		CHECK_GL_ERROR
	}

	template<typename T>
	void VBO<T>::Clear()
	{
		glBufferData(target, maxSize, NULL, GL_DYNAMIC_DRAW);
		
		CHECK_GL_ERROR
		currentOffset = 0;
	}

	template<typename T>
	void VBO<T>::ClearFromOffset(const GLintptr &offset)
	{
		glBufferSubData(target, offset, maxSize - offset, NULL);

		CHECK_GL_ERROR
			currentOffset = offset / sizeof(T);
	}

	template<typename T>
	void VBO<T>::Fill(const vector<T>& data)
	{
		size_t sizeBeingReplaced = sizeof(T) * data.size();

		if (currentOffset * sizeof(T) + sizeBeingReplaced <= maxSize)
		{
			glBufferSubData(target, currentOffset * sizeof(T), data.size() * sizeof(T), data.data());
			currentOffset += data.size();
		}
		else
		{
			ErrorLog::Log("Buffer overflow");

#ifdef _DEBUG
			assert(false);
#endif
		}

		CHECK_GL_ERROR
	}

	template<typename T>
	void VBO<T>::Fill(const T& data)
	{
		if (currentOffset * sizeof(T) <= maxSize)
		{
			glBufferSubData(target, currentOffset * sizeof(T), sizeof(T), glm::value_ptr(data));
			currentOffset++;
		}
		else
		{
			ErrorLog::Log("Buffer overflow");
		}

		CHECK_GL_ERROR
	}

	template<typename T>
	GLint VBO<T>::GetCurrentOffset() const
	{
		return currentOffset;
	}

	template<typename T>
	GLuint VBO<T>::GetID() const
	{
		return ID;
	}
}
