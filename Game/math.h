#pragma once

//#include <gl/glew/glew.h>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace mg
{
	const double M_PI = 3.14159265358979323846;   // pi
	const double M_PI_2 = 1.57079632679489661923;   // pi/2
	const double M_PI_4 = 0.785398163397448309616;  // pi/4
	const double M_PI_3 = 1.0471975511965976;

	const glm::mat4 RAW_MATRIX = glm::scale(glm::vec3(1.0, 0.5, 1.0)) *
		glm::rotate((float)(M_PI_4), glm::vec3(0.0, 0.0, 1.0));

	inline float getDepth(const glm::vec2& point) {

		if (point != glm::vec2(0.0))
			return glm::length(point) * glm::dot(glm::normalize(point), glm::normalize(glm::vec2(1.0, 1.0)));
		else return 0.0;
	}
}