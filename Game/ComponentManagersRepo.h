#pragma once

#include <unordered_map>
#include <memory>
#include <string>

#include <lua\LuaPlus.h>
#include <entityx/entityx.h>

#include "bullet/btBulletDynamicsCommon.h"

#include "Moveable.h"
#include "Position.h"
#include "AIPiece.h"
#include "Direction.h"
#include "Selectable.h"
#include "LightPoint.h"
#include "Pathable.h"
#include "Animation.h"
#include "Model.h"
#include "Viewable.h"
#include "Collideable.h"
#include "ParticleEmitter.h"
#include "InverseKinematicsController.h"
#include "Skeleton.h"
#include "AutoWalkController.h"
#include "Strikeable.h"
#include "SpotLight.h"
#include "Tracer.h"
#include "DirectionalLight.h"

namespace mg {

	/////////////////////////////////////////////////

	class ComponentManagersRepo
	{
		friend class ScriptApiFacade;
		
		typedef std::unordered_map<std::string, std::unique_ptr<ComponentManager>> FactoryMap;

		FactoryMap componentFactories;

	public:
		ComponentManagersRepo(GameplayState *gps)
		{
			componentFactories["moveable"] = std::move(std::unique_ptr<ComponentManager>(new MoveableComponentManager(gps)));
			componentFactories["position"] = std::move(std::unique_ptr<ComponentManager>(new PositionComponentManager(gps)));
			componentFactories["direction"] = std::move(std::unique_ptr<ComponentManager>(new DirectionComponentManager(gps)));
			componentFactories["selectable"] = std::move(std::unique_ptr<ComponentManager>(new SelectableComponentManager(gps)));
			componentFactories["lightPoint"] = std::move(std::unique_ptr<ComponentManager>(new LightPointComponentManager(gps)));
			componentFactories["pathable"] = std::move(std::unique_ptr<ComponentManager>(new PathableComponentManager(gps)));
			componentFactories["animationBatch"] = std::move(std::unique_ptr<ComponentManager>(new AnimationBatchComponentManager(gps)));
			componentFactories["meshBatch"] = std::move(std::unique_ptr<ComponentManager>(new MeshBatchComponentManager(gps)));
			componentFactories["skeleton"] = std::move(std::unique_ptr<ComponentManager>(new SkeletonComponentManager(gps)));
			componentFactories["viewable"] = std::move(std::unique_ptr<ComponentManager>(new ViewableComponentManager(gps)));
			componentFactories["collideable"] = std::move(std::unique_ptr<ComponentManager>(new CollideableComponentManager(gps)));
			componentFactories["particleEmitter"] = std::move(std::unique_ptr<ComponentManager>(new ParticleEmitterComponentManager(gps)));
			componentFactories["inverseKinematicsController"] = std::move(std::unique_ptr<ComponentManager>(new InverseKinematicsControllerComponentManager(gps)));
			componentFactories["autoWalkController"] = std::move(std::unique_ptr<ComponentManager>(new AutoWalkControllerComponentManager(gps)));
			componentFactories["strikeable"] = std::move(std::unique_ptr<ComponentManager>(new StrikeableComponentManager(gps)));
			componentFactories["spotLight"] = std::move(std::unique_ptr<ComponentManager>(new SpotLightComponentManager(gps)));
			componentFactories["tracer"] = std::move(std::unique_ptr<ComponentManager>(new TracerComponentManager(gps)));
			componentFactories["directionalLight"] = std::move(std::unique_ptr<ComponentManager>(new DirectionalLightComponentManager(gps)));
		}

		inline ComponentManager* operator [] (const std::string& string)
		{
			auto iter = componentFactories.find(string);

			if (iter != componentFactories.end())
				return iter->second.get();
			else
				return nullptr;
		}

		inline ComponentManager* operator [] (const size_t& i)
		{
			auto iter = std::next(componentFactories.begin(), i);

			if (iter != componentFactories.end())
				return iter->second.get();
			else
				return nullptr;
		}

		inline ComponentManager* At(const std::string& string)
		{
			return this->operator[](string);
		}

		inline ComponentManager* At(const int &i)
		{
			return this->operator[](i);
		}

		inline size_t Count()
		{
			return componentFactories.size();
		}

		~ComponentManagersRepo() = default;
	};
}