#pragma once

#pragma once

#include <vector>
#include <memory>

#include <entityx\System.h>
#include "VBO.h"
#include "Texture.h"

namespace mg {

	class ViewSystem : public entityx::System<ViewSystem>
	{
		friend class GameplayState;
		friend class Core;

		GameplayState* gps;

	public:
		void configure(entityx::EventManager &events) override;
		void update(entityx::EntityManager &es, entityx::EventManager &em, entityx::TimeDelta dt) override;

		ViewSystem(GameplayState* gps);
	};

}