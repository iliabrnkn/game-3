#pragma once

#include "GUI.h"

namespace mg
{
	class Button : public Widget
	{
		friend class GUI;

	private:
		std::string textContent;

	public:
		Button(const WidgetParameters& params, const std::string& name) :
			Widget(params, name),
			textContent(params.text)
		{
			DrawBox(GUI_COLOR_MEDIUM, GUI_COLOR_DIM);
			SetBackgroundImg(background, sf::IntRect(0, 0, width, height));
		}

		virtual void Draw(sf::RenderWindow &renderWindow, sf::Shader &shader)
		{
			sf::Text text;
			text.setFont(GUI::GetInstance().GetFont());
			text.setString(textContent);
			text.setCharacterSize(10);
			text.setFillColor(GUI_COLOR_MEDIUM);
			text.setPosition(static_cast<sf::Vector2f>(origin + CONTENT_OFFSET));

			Widget::Draw(renderWindow, shader);

			shader.setUniform("textureHeight", static_cast<float>(text.getLocalBounds().height));
			shader.setUniform("textureWidth", static_cast<float>(text.getLocalBounds().width));

			renderWindow.draw(text, &shader);
		}

		virtual void OnClick(const sf::Vector2i& relativeCoords)
		{
		}

		virtual void OnMouseEnter() override
		{
			DrawBox(GUI_COLOR_BRIGHT, GUI_COLOR_MEDIUM);
		}

		virtual void OnMouseLeave() override
		{
			DrawBox(GUI_COLOR_MEDIUM, GUI_COLOR_DIM);
		}

		~Button() {}
	};
}