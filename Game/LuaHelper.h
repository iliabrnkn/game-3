#pragma once

#include <vector>
#include <string>

#include <glm\glm.hpp>
#include <lua\LuaPlus.h>

#include <bullet/btBulletDynamicsCommon.h>
#include <btBulletCollisionCommon.h>

namespace mg
{
	class LuaHelper
	{
	public:

		// ������� ��� ������� LuaPlus::LuaHelper
		static inline int GetInteger(const LuaPlus::LuaObject &obj, const std::string &key, const bool &require = true,
			const int &defaultValue = -1)
		{
			return LuaPlus::LuaHelper::GetInteger(obj, key.c_str(), require, defaultValue);
		}

		static inline float GetFloat(const LuaPlus::LuaObject &obj, const std::string &key, const bool &require = true,
			const float &defaultValue = -1.f)
		{
			return LuaPlus::LuaHelper::GetFloat(obj, key.c_str(), require, defaultValue);
		}

		static inline bool GetBoolean(const LuaPlus::LuaObject &obj, const std::string &key, const bool &require = true,
			const bool &defaultValue = false)
		{
			return LuaPlus::LuaHelper::GetBoolean(obj, key.c_str(), require, defaultValue);
		}

		static inline LuaPlus::LuaObject GetTable(const LuaPlus::LuaObject &obj, const std::string &key, const bool &require = true)
		{
			return LuaPlus::LuaHelper::GetTable(obj, key.c_str(), require);
		}

		static inline std::string GetString(const LuaPlus::LuaObject &obj, const std::string &key, const bool &require = true, 
			const std::string defaultValue = "")
		{
			return LuaPlus::LuaHelper::GetString(obj, key.c_str(), require, defaultValue.c_str());
		}

		// ������� ������� � glm-������� 4x4
		static inline glm::mat4 GetMat4(const LuaPlus::LuaObject &table)
		{
			int c = 0;
			glm::mat4 mat(1.f);

			for (LuaPlus::LuaTableIterator iter(table); iter.IsValid(); iter.Next())
			{
				if (c < 16)
				{
					mat[c % 4][c / 4] = iter.GetValue().ToNumber();
					++c;
				}
			}

			return mat;
		}

		// ������� ������� � ������������ ������

		static inline btVector3 GetBTVec3(const LuaPlus::LuaObject &table)
		{
			return btVector3(
				GetFloat(table, "x", false, 0.f),
				GetFloat(table, "y", false, 0.f), 
				GetFloat(table, "z", false, 0.f)
			);
		}

		static inline LuaPlus::LuaObject BTVec3ToTable(const btVector3 &vec, LuaPlus::LuaState *state)
		{
			LuaPlus::LuaObject res; res.AssignNewTable(state);

			res.SetNumber("x", vec.x());
			res.SetNumber("y", vec.y());
			res.SetNumber("z", vec.z());

			return res;
		}

		// ������� ������� � ������������ ����������
		static inline btQuaternion GetBTQuat(const LuaPlus::LuaObject &table)
		{
			if (table.GetByName("w").GetRef() != -1)
			{
				return btQuaternion(
					GetFloat(table, "x", false, 0.f), 
					GetFloat(table, "z", false, 0.f), 
					GetFloat(table, "y", false, 0.f), 
					GetFloat(table, "w")
				);
			}
			else if (!table.GetByName("axis").IsNil())
			{
				auto axisTable = GetTable(table, "axis");

				btVector3 axis = btVector3(
					GetFloat(axisTable, "x", false, 0.f),
					GetFloat(axisTable, "z", false, 0.f),
					GetFloat(axisTable, "y", false, 0.f)
				).normalized();

				return btQuaternion(axis, GetFloat(table, "angle"));
			}
			// ���� ������
			else if (!table.GetByName("angleZ").IsNil() || !table.GetByName("angleY").IsNil() || !table.GetByName("angleX").IsNil())
			{
				return btQuaternion(
					(btScalar)(GetFloat(table, "angleZ", false, 0.f)),
					(btScalar)(GetFloat(table, "angleY", false, 0.f)),
					(btScalar)(GetFloat(table, "angleX", false, 0.f))
				);
			}
		}

		// ������� ������� � ������������ ������� 3x3
		static inline btMatrix3x3 GetBTMat3(const LuaPlus::LuaObject &table)
		{
			btMatrix3x3 res;

			if (!table.GetByName("angleZ").IsNil() || !table.GetByName("angleY").IsNil() || !table.GetByName("angleX").IsNil())
			{
				btScalar yaw = GetFloat(table, "angleZ", false, 0.f);
				btScalar pitch = GetFloat(table, "angleY", false, 0.f);
				btScalar roll = GetFloat(table, "angleX", false, 0.f);

				res.setEulerZYX(roll, pitch, yaw);
			}
			else
			{
				LuaPlus::LuaTableIterator iter(table);

				for (int i = 0; i < 3; ++i)
				{
					for (int j = 0; j < 3; ++j)
					{
						assert(iter.IsValid());

						res[i][j] = iter.GetValue().ToNumber();

						iter.Next();
					}
				}

				//std::swap(res[1], res[2]);
			}

			return res;
		}

		// ������� ������� � �������� � btTransform
		static inline btTransform GetBTTransform(const LuaPlus::LuaObject &table)
		{
			btTransform res = btTransform::getIdentity();

			if (table.IsNil())
				return res;

			assert(!table.GetByName("rotation").IsNil() || !table.GetByName("basis").IsNil());

			if (!table.GetByName("origin").IsNil())
			{
				res.setOrigin(GetBTVec3(table["origin"]));
			}

			if (!table.GetByName("rotation").IsNil())
			{
				res.setRotation(GetBTQuat(table["rotation"]));
			}
			else if (!table.GetByName("basis").IsNil())
			{
				res.setBasis(GetBTMat3(table["basis"]));
			}

			return res;
		}

		static inline glm::vec2 GetVec2(const LuaPlus::LuaObject &table)
		{
			auto res = glm::vec2(
				GetFloat(table, "x", false, 0.f),
				GetFloat(table, "y", false, 0.f)
			);

			return res;
		}

		static inline glm::vec3 GetVec3(const LuaPlus::LuaObject &table)
		{
			auto res = glm::vec3(
				GetFloat(table, "x", false, 0.f),
				GetFloat(table, "y", false, 0.f),
				GetFloat(table, "z", false, 0.f)
			);

			res.x = GetFloat(table, "r", false, res.x);
			res.y = GetFloat(table, "g", false, res.y);
			res.z = GetFloat(table, "b", false, res.z);

			return res;
		}

		static inline glm::vec4 GetVec4(const LuaPlus::LuaObject &table)
		{
			auto res = glm::vec4(
				GetFloat(table, "x", false, 0.f),
				GetFloat(table, "y", false, 0.f),
				GetFloat(table, "z", false, 0.f),
				GetFloat(table, "w", false, 1.f)
			);

			res.x = GetFloat(table, "r", false, res.x);
			res.y = GetFloat(table, "g", false, res.y);
			res.z = GetFloat(table, "b", false, res.z);
			res.w = GetFloat(table, "a", false, res.w);

			return res;
		}

		static inline LuaPlus::LuaObject Vec3ToTable(const glm::vec3 &vec, LuaPlus::LuaState *state)
		{
			LuaPlus::LuaObject res; res.AssignNewTable(state);

			res.SetNumber("x", vec.x);
			res.SetNumber("y", vec.y);
			res.SetNumber("z", vec.z);

			res.SetMetatable(state->GetGlobal("vectorMetatable"));

			return res;
		}

		static inline std::vector<std::string> GetVectorOfStrings(const LuaPlus::LuaObject &table)
		{
			std::vector<std::string> res;

			for (LuaPlus::LuaTableIterator iter(table); iter; iter.Next())
			{
				if (iter.GetValue().IsString())
					res.push_back(iter.GetValue().GetString());
			}

			return res;
		}

		static btCollisionShape *GetCollisionShape(const LuaPlus::LuaObject &table);
	};
}