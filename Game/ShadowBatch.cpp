#include <vector>
#include <glm/glm.hpp>

#include "ShadowBatch.h"
#include "Paths.h"
#include "Core.h"

namespace mg
{
	void ShadowBatch::Init(LuaPlus::LuaState* luaState)
	{
		effect = make_shared<Effect>();
		// loading shaders

		effect->LoadFromFile(EFFECTS_FOLDER + "ShadowPlane.lua", luaState);

		auto videoMode = Core::GetVideoMode();

		std::swap(indicesSource, std::vector<unsigned int> {0, 1, 2, 3});
		std::swap(verticesSource, std::vector<SimpleVertex>{
			SimpleVertex(0.0, 0.0, 0.0),
			SimpleVertex(0.0, videoMode.height, 0.0),
			SimpleVertex(videoMode.width, videoMode.height, 0.0),
			SimpleVertex(videoMode.width, 0.0, 0.0)
		});

		// initialize buffers

		indices->Init(BUFFER_SIZE_4KB, GL_ELEMENT_ARRAY_BUFFER);
		vertices->Init(BUFFER_SIZE_4KB, GL_ARRAY_BUFFER);

		vertices->Bind();
		vertices->Fill(verticesSource);

		indices->Bind();
		indices->Fill(indicesSource);

		// initialize VAO

		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		vertices->Bind();
		indices->Bind();

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(SimpleVertex), 0);
		glBindVertexArray(0);

		vertices->Unbind();
		indices->Unbind();
	}

	void ShadowBatch::Draw()
	{
		glDrawElementsBaseVertex(GL_QUADS, 4, GL_UNSIGNED_INT, BUFFER_OFFSET(0), 0);
	}
}