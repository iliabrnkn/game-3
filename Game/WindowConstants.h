#pragma once

#include <SFML\Graphics.hpp>

#include <GL/glew/glew.h>
#include <GL/GL.h>

#define WINDOW_STYLE sf::Style::Fullscreen
#define WINDOW_WIDTH sf::VideoMode::getDesktopMode().width
#define WINDOW_HEIGHT sf::VideoMode::getDesktopMode().height
//
//#define WINDOW_STYLE sf::Style::Default
//#define WINDOW_WIDTH 800
//#define WINDOW_HEIGHT 9 * WINDOW_WIDTH / 16
//
//#define WINDOWED_STYLE sf::Style::Default
//#define WINDOWED_WIDTH 800
//#define WINDOWED_HEIGHT 9 * WINDOWED_WIDTH / 16
namespace mg
{
	/*const int WINDOW_WIDTH = 800;
	const int WINDOW_HEIGHT = 9 * WINDOW_WIDTH / 16;*/

	/*const int WINDOW_WIDTH_FS_BIG = 1920;
	const int WINDOW_HEIGHT_FS_BIG = 1080;*/

	/*const int WINDOW_WIDTH_FS = 1600;
	const int WINDOW_HEIGHT_FS = 900;*/

	/*const GLsizei RENDERBUFFER_WIDTH = 640;
	const GLsizei RENDERBUFFER_HEIGHT = 9 * RENDERBUFFER_WIDTH / 16;*/

	const GLsizei RENDERBUFFER_WIDTH = 640;
	const GLsizei RENDERBUFFER_HEIGHT = 9 * RENDERBUFFER_WIDTH / 16;

	const int STENCIL_BITS = 0;
	const int ANTIALIASING_LEVEL = 0;
	const int OGL_MAJOR_VERSION = 4;
	const int OGL_MINOR_VERSION = 0;
	const int ZNEAR_SCENE = -50;
	const int ZFAR_SCENE = 50;
}