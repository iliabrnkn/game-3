#pragma once

#include <vector>
#include <memory>
#include <unordered_map>

#include <GL\glew\glew.h>
#include <lua\LuaPlus.h>

#include "Texture.h"
#include "Effect.h"
#include "VBO.h"
#include "VertTypes.h"
#include "Camera.h"
#include "FBO.h"
#include "RenderBatch.h"
#include "ScreenBatch.h"
#include "RenderLightSource.h"
#include "Scene.h"

namespace mg {

	const int SHADOWMAP_SIZE = 256;
	const int DIRECTIONAL_SHADOWMAP_WIDTH = glm::sqrt(glm::pow(RENDERBUFFER_WIDTH, 2) + glm::pow(2 * RENDERBUFFER_HEIGHT, 2));
	const int DIRECTIONAL_SHADOWMAP_HEIGHT = DIRECTIONAL_SHADOWMAP_WIDTH;//DIRECTIONAL_SHADOWMAP_WIDTH;
	const int BLOOM_POWER = 5;
	const int CLOUD_TEXTURE_SIZE = 256;

	class Controls;

	class Renderer
	{
		friend class Core;

		FBO globalFBO, omniLightFBO, staticOmniLightFBO, directionalLightFBO, spotLightFBO, bloomPingFBO, bloomPongFBO, deferredRenderingFBO, cloudFBO;
		LuaPlus::LuaState* luaState;

		std::unordered_map<std::string, std::shared_ptr<Effect>> effects;
		int lightsCount, spotLightsCount;

	public:

		Renderer() :
			bloomBatch(RENDERBUFFER_WIDTH, RENDERBUFFER_HEIGHT), deferredRenderingBatch(RENDERBUFFER_WIDTH, RENDERBUFFER_HEIGHT)
		{}

		void InitFBO();
		void RenderScene(Scene& scene, Controls& controls);
		void RenderOmniLights(Scene& scene);
		void RenderSpotLights(Scene& scene);
		void RenderDirectionalLight(Scene &scene);
		void RenderClouds(Scene &scene);
		void BakeStaticOmniLights(Scene& scene);
		void DeferredRendering();
		void RenderToScreen();
		void RenderDebugData(Scene& scene);
		void SetLightCount(const int& lightsCount);
		void SetSpotLightCount(const int& dirLightsCountInitial);
		void ClearBuffers(const int &flags = GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		void Init(LuaPlus::LuaState* luaState);

		void RenderBloom(Scene& scene);

		glm::vec3 testLightOffset;

		Effect& GetDeferredRenderingEffect();

	private:
		Effect deferredRenderingEffect, bloomEffect, bloomBlendingEffect, renderToScreenEffect, directionalLightEffect;
		shared_ptr<Camera> camera;
		ScreenBatch deferredRenderingBatch;
		ScreenBatch screenBatch;
		ScreenBatch bloomBatch;
		std::vector<RenderLightSource> lightSources;
		glm::mat4 windowProjectionMatrix, renderbufferProjectionMatrix;

		Texture colorTex, posTex, normalTex, normalTex3D, deferredRenderingColorOut,
			depthTex, dynamicCubicShadowMap, staticCubicShadowMap, dynamic2DShadowMap, static2DShadowMap, directionalShadowMap,
			particleTex, blurredTex, bloomPingTex, bloomPongTex,
			cloudMaskTex;

		VBO<glm::mat4> shadowMxsBuffer;
	};
}