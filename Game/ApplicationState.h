#pragma once

#include <SFML\Graphics.hpp>

namespace mg {

	class Core;
	class Effect;

	class ApplicationState
	{
	public:
		virtual void Init() = 0;
		virtual void Update(float delta, sf::Window *window) = 0;
		virtual void PrepareDeferredRendering(Effect& fboEffect) {};
		virtual void HandleControls(sf::Window *window, const double& deltaTime, const sf::Event& event) {};

		ApplicationState();
		virtual ~ApplicationState() = 0;
	};

}