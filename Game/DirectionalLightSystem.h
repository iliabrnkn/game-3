#pragma once

#include <lua\LuaPlus.h>
#include <entityx\System.h>

#include "Camera.h"
#include "VBO.h"
#include "Texture.h"
#include "Effect.h"
#include "ScreenBatch.h"

namespace mg {

	struct Position;
	struct LightPoint;
	struct Direction;

	class DirectionalLightSystem : public entityx::System<DirectionalLightSystem>
	{
		friend class GameplayState;
		friend class Core;

		GameplayState* gps;
		
	public:
		void configure(entityx::EventManager &events) override;
		void update(entityx::EntityManager &es, entityx::EventManager &em, entityx::TimeDelta dt) override;

		DirectionalLightSystem(GameplayState* gameplayState);
	};
}