#include "stdafx.h"

#include "Selectable.h"
#include "GameplayState.h"

namespace mg
{
	
	void SelectableComponentManager::AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		entity.assign<Selectable>();
	}

	LuaPlus::LuaObject SelectableComponentManager::GetComponentTable(entityx::Entity& entity) const
	{
		LuaPlus::LuaObject res;

		if (entity.has_component<Selectable>())
		{
			auto componentHandler = entity.component<Selectable>();

			res.AssignNewTable(gps->GetLuaState());
		}
		else
		{
			res.AssignNil();
		}

		return res;
	}
}