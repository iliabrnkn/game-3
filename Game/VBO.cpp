﻿#include "stdafx.h"
#include <vector>
#include <memory>

#include "VBO.h"

namespace mg {

	template<typename T>
	VBO<T>::VBO():
		currentOffset(0)
	{
	}

	template<typename T>
	VBO<T>::~VBO()
	{
		glDeleteBuffers(1, &ID);
	}

	template<typename T>
	void VBO<T>::Init(const GLsizei& initialSize, const GLenum& targetInit)
	{
		glGenBuffers(1, &ID);
		glBindBuffer(target, ID);
		glBufferData(target, initialSize, NULL, GL_STATIC_DRAW);
		glBindBuffer(target, 0);

		target = targetInit;
		maxSize = initialSize;
	}

	template<typename T>
	void VBO<T>::Bind()
	{
		glBindBuffer(target, ID);
	}

	template<typename T>
	void VBO<T>::Unbind()
	{
		glBindBuffer(target, 0);
	}

	template<typename T>
	void VBO<T>::Free()
	{
		glDeleteBuffers(1, &ID);
	}

	template<typename T>
	void VBO<T>::Fill(const vector<T>& data)
	{
		size_t sizeBeingReplaced = sizeof(T) * data.size();

		currentOffset += data.size();

		if (currentOffset * sizeof(T) < maxSize)
		{
			glBindBuffer(target, ID);
			glBufferSubData(target, currentOffset*sizeof(T), data.size() * sizeof(SpriteVertex), data.data());
			glBindBuffer(target, 0);
		}
		else
		{
			printf("Buffer overflow");
		}
	}

	template<typename T>
	GLint VBO<T>::GetCurrentOffset() const
	{
		return currentOffset;
	}
}