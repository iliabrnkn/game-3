#pragma once

#include <entityx\System.h>
#include <unordered_map>
#include <string>
#include <memory>

#include "Camera.h"
#include "ParticleEmitter.h"
#include "ObjectProps.h"

namespace mg {

	const double PARTICLES_PERIOD = 4.0;
	const int MAX_PARTICLES_PER_FRAME = 4;
	const float FIRE_PARTICLES_VELOCITY = 10.0f;

	class GameplayState;
	struct Position;

	class ParticlesSystem : public entityx::System<ParticlesSystem>, public entityx::Receiver<entityx::ComponentRemovedEvent<ParticleEmitter>>
	{
		const int MAX_EMITTERS_COUNT = 100;

		friend class GameplayState;
		
		GameplayState* gps;

		VBO<EmitterProps> emitterPropsBuffer;

		inline float GetDepthVal(const SceneVector &particlePos) const
		{
			if (particlePos != SceneVector(0.f))
				return particlePos.Dot(
					SceneVector(sqrt(2.f) / 4.f, sqrt(2.f) / 4.f, sqrt(3.f) / 2.f)
				);
			else
				return 0.0;
		}

		inline size_t ParticlesSystem::FindLastUnusedParticle(ParticleEmitter &emitter)
		{
			auto iter = std::find_if(emitter.particles.begin() + emitter.lastUnusedParticleID, emitter.particles.end(),
				[&emitter](auto &x) { return x.lifeTime >= emitter.maxLifeTime || x.position == glm::vec3(-100.f); });

			if (iter != emitter.particles.end())
			{
				emitter.lastUnusedParticleID = iter - emitter.particles.begin();
				return emitter.lastUnusedParticleID;
			}
			else if (emitter.lastUnusedParticleID > 0)
			{
				auto iterBack = std::find_if(emitter.particles.begin(), emitter.particles.begin() + emitter.lastUnusedParticleID + 1,
					[&emitter](auto &x) { return x.lifeTime >= emitter.maxLifeTime || x.position == glm::vec3(-100.f);  });

				if (iterBack != emitter.particles.end())
				{
					emitter.lastUnusedParticleID = iterBack - emitter.particles.begin();
					return emitter.lastUnusedParticleID;
				}
			}

			return 0;
		}

	public:
		virtual void configure(entityx::EventManager &events) override;
		virtual void update(entityx::EntityManager &es, entityx::EventManager &em, entityx::TimeDelta dt) override;
		void receive(const entityx::ComponentRemovedEvent<ParticleEmitter>& ev);

		ParticlesSystem(GameplayState* gps);
	};
}
