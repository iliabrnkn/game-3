#include "stdafx.h"

#include <fstream>

#include "AnimationManager.h"
#include "Paths.h"

using namespace std;

namespace mg
{
	void AnimationManager::LoadAnimationFromFile(const string& filename)
	{
		ifstream file;
		file.open(ANIMATIONS_FOLDER + filename, ios::binary);

		if (file.fail())
		{
			printf("Can't open animation file.");
			file.close();
		}

		unsigned int animationNum;

		file.read((char*)&(animationNum), sizeof(unsigned int));

		for (int iNumAnim = 0; iNumAnim < animationNum; iNumAnim++)
		{
			Animation anim;

			// ��� ��������
			anim.activity = 0.0;
			anim.elapsedTicks = 0.0;

			//Helper::ReadStringFromFile(file, name);

			// ������������ �������� � �����
			file.read((char*)&(anim.durationInTicks), sizeof(double));
			// ���-�� ����� � �������
			file.read((char*)&(anim.ticksPerSec), sizeof(double));

			// ���-�� �������
			unsigned int channelNum = 0;
			file.read((char*)&channelNum, sizeof(unsigned int));

			for (int iNumChannels = 0; iNumChannels < channelNum; iNumChannels++)
			{
				AnimChannel currentChannel;

				// ��� ���������������� ������ ����
				string channelName;

				ReadStringFromFile(file, channelName);

				// ���-�� ������ ��������
				unsigned int numKeys;

				file.read((char*)&numKeys, sizeof(unsigned int));

				for (int iNumKeys = 0; iNumKeys < numKeys; iNumKeys++)
				{
					// ����
					AnimKey currentKey;

					file.read((char*)&(currentKey.time), sizeof(double));

					// �������� ����� �������, ���������� ������
					file.read((char*)&(currentKey.pos.x), sizeof(float));
					file.read((char*)&(currentKey.pos.y), sizeof(float));
					file.read((char*)&(currentKey.pos.z), sizeof(float));

					// �������� ����� ��������, ����������
					file.read((char*)&(currentKey.rot.x), sizeof(float));
					file.read((char*)&(currentKey.rot.y), sizeof(float));
					file.read((char*)&(currentKey.rot.z), sizeof(float));
					file.read((char*)&(currentKey.rot.w), sizeof(float));

					// �������� ����� ���������������, ���������� ������
					file.read((char*)&(currentKey.scale.x), sizeof(float));
					file.read((char*)&(currentKey.scale.y), sizeof(float));
					file.read((char*)&(currentKey.scale.z), sizeof(float));

					//currentChannel.keys.insert(pair<double, AnimKey>(currentKey.time, currentKey));

					currentChannel.keys.push_back(currentKey);
				}

				anim.channels.insert(make_pair(channelName, currentChannel));
			}

			animations.insert(make_pair(filename, anim));
		}

		file.close();
	}

	void AnimationManager::ReadStringFromFile(ifstream &file, string& stringToChange)
	{
		// ����� ������ ��� �����������
		unsigned int length;

		file.read((char*)&length, sizeof(unsigned int));

		char* cStr = new char[length + 1];
		file.read(cStr, sizeof(char) * (length + 1));
		stringToChange = cStr;

		delete cStr;
	}

	unordered_map<string, Animation> AnimationManager::GetAnimations()
	{
		return animations;
	}
}