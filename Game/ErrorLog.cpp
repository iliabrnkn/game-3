#include "stdafx.h"

#include <cassert>
#include <iostream>
#include <fstream>
#include <ctime>

#include <GL\glew\glew.h>

#include "ErrorLog.h"

namespace mg
{
	std::vector<std::string> ErrorLog::log;
	std::chrono::time_point<std::chrono::high_resolution_clock, std::chrono::duration<double>> ErrorLog::prevTime;

	const std::chrono::duration<double> ErrorLog::LAG_PERIOD = std::chrono::duration<double>(0.5);

	void ErrorLog::CheckGLError(const std::string& msg)
	{
		auto err = glGetError();

		if (err == 1280)
			Log("1280 - INVALID ENUM " + msg);
		if (err == 1281)
			Log("1281 - INVALID VALUE " + msg);
		if (err == 1282)
			Log("1282 - INVALID OPERATION " + msg);
		if (err == 1283)
			Log("1283 - STACK OVERFLOW " + msg);
		if (err == 1284)
			Log("1284 - STACK UNDERFLOW " + msg);
		if (err == 1285)
			Log("1285 - STACK OUT OF MEMORY " + msg);

		assert(err == GL_NO_ERROR);
	}

	void ErrorLog::Log(const std::string& entry)
	{
		printf("%s\r\n", entry.c_str());

		log.push_back(entry);
	}

	std::string ErrorLog::GetFullLog()
	{
		std::string res;

		for (auto entry : log)
		{
			res.append(entry);
			res.append("\r\n");
		}

		return res;
	}

	void ErrorLog::Dump(const std::string& logFileName)
	{
		std::ofstream logFile(logFileName);

		auto t = std::time(nullptr);
		logFile << (GetFullLog() + "\r\n------------ " + std::asctime(std::localtime(&t))) + " ------------\r\n";

		logFile.close();
	}

	void ErrorLog::CheckLag(const std::string &msg, const bool reset)
	{
		if (!reset)
		{
			auto currTime = std::chrono::high_resolution_clock::now();
			std::chrono::duration<double> diff = currTime - prevTime;

			if (diff >= std::chrono::duration<double>(LAG_PERIOD))
				ErrorLog::Log("LAG TIME = " + std::to_string(diff.count()) + " (" + msg + ")");
		}
		
		prevTime = std::chrono::high_resolution_clock::now();

	}

	void APIENTRY ErrorLog::DebugOutputCallback(GLenum source, GLenum type, GLuint id,
		GLenum severity, GLsizei length, const GLchar* message, const void* userParam)
	{
		std::string sourceName, typeName, severityName;

		switch (source)
		{
		case GL_DEBUG_SOURCE_API:
			sourceName = "GL_DEBUG_SOURCE_API";
		case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
			sourceName = "GL_DEBUG_SOURCE_WINDOW_SYSTEM";
		case GL_DEBUG_SOURCE_SHADER_COMPILER:
			sourceName = "GL_DEBUG_SOURCE_SHADER_COMPILER";
		case GL_DEBUG_SOURCE_THIRD_PARTY:
			sourceName = "GL_DEBUG_SOURCE_THIRD_PARTY";
		case GL_DEBUG_SOURCE_APPLICATION:
			sourceName = "GL_DEBUG_SOURCE_APPLICATION";
		case GL_DEBUG_SOURCE_OTHER:
			sourceName = "GL_DEBUG_SOURCE_OTHER";
		}

		switch (type)
		{
		case GL_DEBUG_TYPE_ERROR:
			typeName = "GL_DEBUG_TYPE_ERROR";
		case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
			typeName = "GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR";
		case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
			typeName = "GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR";
		case GL_DEBUG_TYPE_PORTABILITY:
			typeName = "GL_DEBUG_TYPE_PORTABILITY";
		case GL_DEBUG_TYPE_PERFORMANCE:
			typeName = "GL_DEBUG_TYPE_PERFORMANCE";
		case GL_DEBUG_TYPE_MARKER:
			typeName = "GL_DEBUG_TYPE_MARKER";
		case GL_DEBUG_TYPE_PUSH_GROUP:
			typeName = "GL_DEBUG_TYPE_PUSH_GROUP";
		case GL_DEBUG_TYPE_POP_GROUP:
			typeName = "GL_DEBUG_TYPE_POP_GROUP";
		case GL_DEBUG_TYPE_OTHER:
			typeName = "GL_DEBUG_TYPE_OTHER";
		}

		switch (severity)
		{
		case GL_DEBUG_SEVERITY_HIGH:
			severityName = "GL_DEBUG_SEVERITY_HIGH";
		case GL_DEBUG_SEVERITY_MEDIUM:
			severityName = "GL_DEBUG_SEVERITY_MEDIUM";
		case GL_DEBUG_SEVERITY_LOW:
			severityName = "GL_DEBUG_SEVERITY_LOW";
		case GL_DEBUG_SEVERITY_NOTIFICATION:
			severityName = "GL_DEBUG_SEVERITY_NOTIFICATION";
		}

		Log("---OPENGL DEBUG OUTPUT---\r\nsource:" +
			sourceName + "\r\n" +
			"type: " + typeName + "\r\n" +
			"severity: " + severityName + "\r\n" +
			"msg: " + std::string(message) + "\r\n"
			"--- --- ---\r\n"
		);
	}
}