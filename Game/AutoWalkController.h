#pragma once

#include "ComponentManager.h"
#include "Vector.h"

namespace mg {

	enum FootOnGround
	{
		None = 0,
		Left = 1,
		Right = 2
	};

	struct AutoWalkController
	{
		SceneVector walkDirection;
		bool isActive;
		FootOnGround currFootOnTheGround;
		bool needToChangeLegs;
		FootOnGround ChangeLegs();

		inline static std::string GetFootPostfix(const FootOnGround &footOnGround)
		{
			if (footOnGround == FootOnGround::Left)
				return "_l";
			else if (footOnGround == FootOnGround::Right)
				return "_r";
			else return "";
		}

		AutoWalkController() :
			isActive(false), walkDirection(0.0), currFootOnTheGround(FootOnGround::None)
		{}

		~AutoWalkController()
		{}
	};

	class AutoWalkControllerComponentManager : public ComponentManager
	{
	public:
		AutoWalkControllerComponentManager(GameplayState *gps) : ComponentManager("autoWalkController", gps) {}
		virtual void AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const override;
		virtual LuaPlus::LuaObject ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const override;
		virtual LuaPlus::LuaObject GetComponentTable(entityx::Entity& entity) const override;
		void AutoWalk(const LuaPlus::LuaObject &entityTable, const LuaPlus::LuaObject &direction) const;
		void SetAutoWalkActive(const LuaPlus::LuaObject &entityTable, const bool &active) const;
	};
}