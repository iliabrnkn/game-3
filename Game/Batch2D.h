#pragma once

#include "RenderBatch.h"

namespace mg
{
	const int MAX_OBJECTS = 16;

	class SpriteBatch : public ConcreteRenderBatch<SpriteVertex>
	{
		friend class RenderingSystem2D;

	public:
		virtual void Init(LuaPlus::LuaState* luaState) override;
		virtual void Draw() override;

		SpriteBatch();
		~SpriteBatch();
	};
}