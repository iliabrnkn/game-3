#pragma once

#include <entityx\System.h>
#include <SFML\Graphics.hpp>
#include <glm/glm.hpp>
#include <lua\LuaPlus.h>

namespace mg
{
	class Camera;

	class ControlSystem : public entityx::System<ControlSystem>
	{
		friend class GameplayState;

		std::shared_ptr<Camera> camera;
		sf::Window* window;

		glm::vec2 shiftOffset;

		double updatesPerSec;
		double keyUpdatesPerSec;
		double timeFromLastClick;
		double timeFromLastKeyPressed;

		// toggles

		bool toggleFightMode, canDisableFightMode;

		LuaPlus::LuaState* luaState;

	public:
		void configure(entityx::EventManager &events) override;
		void update(entityx::EntityManager &es, entityx::EventManager &em, entityx::TimeDelta dt) override;
	};
}