#pragma once

#include <lua\LuaPlus.h>
#include <glm\gtc\type_ptr.hpp>

#include "bullet/btBulletDynamicsCommon.h"

#include "Batch3D.h"
#include "Batch2D.h"
#include "SimpleBatch.h"
#include "ParticleEmitterBatch.h"
#include "TextureAtlas.h"
#include "LineBatch.h"
#include "Vector.h"
#include "TilesetStructs.h"

#include "Props.h"

namespace mg {
	/*
		����� ������ �������� ��� ��������� ���������� ��������� (������� ������) � ��������
	*/

	struct Physics;

	const float NEAR_POINTS_LEN = 0.3f;
	const int SEGMENT_TILE_SIZE = 30;

	// ���������� ������� ������, �������������� ��� ��������� ������ �� ������
	struct SceneSegment
	{
		GLint firstElement;
		GLint elementsCount;

		SceneVector minBoundingPoint, maxBoundingPoint;
	};

	// ���. ����������, �������������� ��� ��������� ������ �����
	struct SceneDrawAdditionalInfo
	{
		bool drawLevelGeometry;
		bool drawCharacters;

		// ����� ��� ������ ������ � ������������� �����������
		std::vector<int> sceneSegmentsToDraw;

		SceneDrawAdditionalInfo()
			: drawLevelGeometry(true), drawCharacters(true)
		{}

		SceneDrawAdditionalInfo(const bool &drawLevelGeometry, const bool &drawCharacters)
			: drawLevelGeometry(drawLevelGeometry), drawCharacters(drawCharacters)
		{}

		void AddSceneSegmentToDraw(const int &sceneSegmentID)
		{
			sceneSegmentsToDraw.push_back(sceneSegmentID);
		}
	};

	class Scene
	{
		Effect lightEffect;
		Effect spotLightEffect;
		Effect directionalLightEffect;
		Effect geometryEffect;
		Effect backgroundEffect;
		Effect simpleEffect;
		Effect particlesEffect;
		Effect linesEffect;
		Effect cloudEffect;

		GeometryBatch geometry;
		GeometryBatch charactersBatch;
		GeometryBatch cloudBatch;
		SimpleBatch simpleBatch;
		ParticlesBatch particlesBatch;
		LineBatch linesBatch;

		//std::vector<glm::vec3> vertices;

		std::vector<AnimVertex> vertsConverted;
		std::vector<unsigned int> alignedIndices;

		btTriangleMesh* trisMesh;

		int modelCount, objectCount;

		btBvhTriangleMeshShape* triangleShape;
		RigidBody* levelBody;

		size_t sceneGeometryVertsOffset;
		size_t sceneGeometryIndsOffset;

		std::map<int, SceneSegment> sceneSegments;

		SceneDrawAdditionalInfo mainSceneDrawAdditionalInfo;

	public:
		Scene();
		~Scene() = default;

		void Init(LuaPlus::LuaState* luaState);

		std::map<int, SceneDrawAdditionalInfo> lightsQueue, dirLightsQueue;

		// ������������ �������� ����� ��� ���������� ������� ��������� - ����������, ��� ��������� ����� ���������� ��� ��������� ��������� ���������
		inline void RegisterLightInQueue(const int &lightIndex, const SceneDrawAdditionalInfo &lightInfo)
		{
			lightsQueue[lightIndex] = lightInfo;
		}

		inline void RegisterDirLightInQueue(const int &lightIndex, const SceneDrawAdditionalInfo &lightInfo)
		{
			dirLightsQueue[lightIndex] = lightInfo;
		}

		inline const std::map<int, SceneSegment> &GetSceneSegments() const
		{
			return sceneSegments;
		}

		inline void AddSceneSegment(const SceneSegment &sceneSegment)
		{
			sceneSegments.insert(std::make_pair(sceneSegments.size(), sceneSegment));
		}

		inline const std::map<int, SceneDrawAdditionalInfo> &GetLightsQueue() const
		{
			return lightsQueue;
		}

		inline const std::map<int, SceneDrawAdditionalInfo> &GetDirLightsQueue() const
		{
			return dirLightsQueue;
		}

		inline SceneDrawAdditionalInfo &GetMainSceneDrawAdditionalInfo()
		{
			return mainSceneDrawAdditionalInfo;
		}

		inline void ClearLightsQueue()
		{
			lightsQueue.clear();
		}

		inline RigidBody *GetLevelBody()
		{
			return levelBody;
		}

		inline int Scene::CreateNewModelID()
		{
			return modelCount++;
		}

		void Scene::ResetModelCounter()
		{
			modelCount = 0;
		}

		int Scene::CreateNewObjectID()
		{
			return ++objectCount;
		}

		inline void ClearBuffers()
		{
			geometry.ClearBuffers();
		}

		inline GeometryBatch& GetGeometry()
		{
			return geometry;
		}

		inline GeometryBatch& GetCharactersBatch()
		{
			return charactersBatch;
		}

		inline GeometryBatch &GetCloudBatch()
		{
			return cloudBatch;
		}

		inline SimpleBatch& GetSimpleBatch()
		{
			return simpleBatch;
		}

		inline Effect& GetLightEffect()
		{
			return lightEffect;
		}

		inline Effect& GetSpotLightEffect()
		{
			return spotLightEffect;
		}

		inline Effect& GetDirectionalLightEffect()
		{
			return directionalLightEffect;
		}

		inline Effect& GetLinesEffect()
		{
			return linesEffect;
		}

		inline Effect& GetGeomertyEffect()
		{
			return geometryEffect;
		}

		inline Effect& GetSimpleEffect()
		{
			return simpleEffect;
		}

		inline Effect& GetBackgroundEffect()
		{
			return backgroundEffect;
		}

		inline Effect& GetParticlesEffect()
		{
			return particlesEffect;
		}

		inline Effect& GetCloudEffect()
		{
			return cloudEffect;
		}

		inline size_t GetCurrGeometryIndsCount() const
		{
			return alignedIndices.size();
		}

		inline ParticlesBatch& GetParticlesBatch() { return particlesBatch; }

		inline LineBatch& GetLineBatch() { return linesBatch; }

		inline size_t GetGeometryOffsetVerts() const
		{
			return sceneGeometryVertsOffset;
		}

		inline size_t GetGeometryOffsetInds() const
		{
			return sceneGeometryIndsOffset;
		}

		// ���������� ���������� �������� � ���������� ���������� ��� �������
		inline glm::vec2 GetVertSpriteTexCoords(const glm::vec3 &vert, const GidTex *gidTex, const glm::vec2 &texOffset = glm::vec2(0.f)) const
		{
			auto absScreenCoords = MatrixHelper::SceneToScreenAbsF(vert) +
				glm::vec2(32, gidTex->height - 32);

			return glm::vec2(
				(gidTex->stUnrelatedLeftTop.x + double(absScreenCoords.x) + texOffset.x) / gidTex->layerWidth,
				(gidTex->stUnrelatedLeftTop.y + double(absScreenCoords.y) + texOffset.y) / gidTex->layerHeight
			);
		}

		void AddQuadToSceneGeometry(const SceneVector &offset, const SceneVector &n, const GidTex *gidTex,
			SceneVector &maxBoundingPoint);
		void AddVertexDataToSceneGeometry(PropsHandler props, const glm::vec3 &offset, const GidTex *gidTex,
			SceneVector &maxBoundingPoint);

		void CreateTriangleMeshShape(Physics& physics);
		void FillBuffers();
	};
}