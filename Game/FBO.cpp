#include "stdafx.h"

#include <vector>
#include <glm/gtc/type_ptr.hpp>
#include <lua\LuaPlus.h>

#include "FBO.h"
#include "Paths.h"
#include "OGLConstants.h"
#include "WindowConstants.h"
#include "sizes.h"
#include "LightSystem.h"
#include "ApplicationState.h"

#include "Core.h"

using namespace std;

namespace mg
{	
	void FBO::Bind() 
	{
		glBindFramebuffer(GL_FRAMEBUFFER, fbo);

		CHECK_GL_ERROR
	}

	void FBO::Unbind() 
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		CHECK_GL_ERROR
	}

	void FBO::Init()
	{
		glGenFramebuffers(1, &fbo);

		CHECK_GL_ERROR
	}

	void FBO::AttachTexture(const GLenum& attachment, const GLuint& textureID, const GLenum& target)
	{
		if (target == GL_TEXTURE_2D || target == GL_TEXTURE_2D_ARRAY)
		{
			glFramebufferTexture(GL_FRAMEBUFFER, attachment, textureID, 0);
		}
		else
		{
			glFramebufferTextureARB(GL_FRAMEBUFFER, attachment, textureID, 0);
		}

		CheckForErrors();

		CHECK_GL_ERROR
	}

	void FBO::CheckForErrors()
	{
		GLenum error = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if (error != GL_FRAMEBUFFER_COMPLETE)
		{
			string errorMsg;

			switch (error)
			{
			case GL_FRAMEBUFFER_UNDEFINED:
				errorMsg = "GL_FRAMEBUFFER_UNDEFINED";
				break;
			case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
				errorMsg = "GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT";
				break;
			case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
				errorMsg = "GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT";
				break;
			case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
				errorMsg = "GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER";
				break;
			case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
				errorMsg = "GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER";
				break;
			case GL_FRAMEBUFFER_UNSUPPORTED:
				errorMsg = "GL_FRAMEBUFFER_UNSUPPORTED";
				break;
			case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
				errorMsg = "GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE";
				break;
			case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
				errorMsg = "GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS";
				break;
			}

			ErrorLog::Log("Incomplete FBO : " + errorMsg);
		}
	}
}