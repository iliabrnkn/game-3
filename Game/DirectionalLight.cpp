#include "stdafx.h"

#include "DirectionalLight.h"
#include "GameplayState.h"

namespace mg
{
	void DirectionalLightComponentManager::AssignComponent(entityx::Entity &entity, const LuaPlus::LuaObject &table) const
	{
		entity.assign<DirectionalLight>(
			LuaHelper::GetBoolean(table, "enabled", false, true),
			LuaHelper::GetVec3(LuaHelper::GetTable(table, "color")),
			LuaHelper::GetVec2(LuaHelper::GetTable(table, "windVelocity"))
		);
	}
	
	LuaPlus::LuaObject DirectionalLightComponentManager::GetComponentTable(entityx::Entity& entity) const
	{
		return LuaPlus::LuaObject();
	}

	LuaPlus::LuaObject DirectionalLightComponentManager::ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		return LuaPlus::LuaObject();
	}

	void DirectionalLightComponentManager::EnableDirectionalLight(const LuaPlus::LuaObject &entity, const bool &enabled) const
	{
		gps->GetEntity(entity).component<DirectionalLight>()->enabled = enabled;
	}

	bool DirectionalLightComponentManager::IsDirectionalLightEnabled(const LuaPlus::LuaObject &entity) const
	{
		return gps->GetEntity(entity).component<DirectionalLight>()->enabled;
	}

	void DirectionalLightComponentManager::SetDirectionalLightColor(const LuaPlus::LuaObject &entity, const LuaPlus::LuaObject &color) const
	{
		gps->GetEntity(entity).component<DirectionalLight>()->color = LuaHelper::GetVec3(color);
	}
}