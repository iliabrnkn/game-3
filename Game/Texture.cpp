﻿#include "stdafx.h"
#include <string>

#include "Texture.h"

#include "ErrorLog.h"

namespace mg
{
	int Texture::texUnitsCounter = 1;

	Texture::Texture(void):
		textureUnit(0),
		textureID(0)
	{
		static bool initedStatic(false);
		
		if (!initedStatic)
		{
			ResetTextureUnitCount();
			
			initedStatic = true;
		}
	}

	void Texture::ResetTextureUnitCount()
	{
		texUnitsCounter = 0;
	}

	Texture::~Texture(void)
	{
		Delete();
	}

	void Texture::Delete()
	{
		if (textureID != 0)
			glDeleteTextures(1, &textureID);
	}

	void Texture::Init(const sf::Image& image, const GLenum& pixelType)
	{	
		Init(image.getSize().x, image.getSize().y, GL_RGBA, GL_NEAREST, (void*)image.getPixelsPtr());
	}

	//TODO: рефакторинг

	void Texture::Init(const unsigned int& widthInitial, const unsigned int& heightInitial, const GLint& pixelFormatInitial,
		const GLint& filterInitial, const void* pixelsPtrInitial, const GLenum& pixelTypeInitial, const GLenum& pixelInternalFormatInitial, 
		const GLenum& targetInitial, const unsigned int& depthInitial)
	{
		glGenTextures(1, &textureID);

		CHECK_GL_ERROR

		target = targetInitial;
		Rebind();

		width = widthInitial;
		height = heightInitial;
		pixelFormat = pixelFormatInitial;

		if (target == GL_TEXTURE_2D)
		{
			glTexParameteri(target, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(target, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glTexParameteri(target, GL_TEXTURE_MIN_FILTER, filterInitial);
			glTexParameteri(target, GL_TEXTURE_MAG_FILTER, filterInitial);

			glTexImage2D(
				target, 0, pixelInternalFormatInitial,
				width, height, 0,
				pixelFormat, pixelTypeInitial, pixelsPtrInitial
			);

			CHECK_GL_ERROR
		}
		else if (target == GL_TEXTURE_CUBE_MAP)
		{
			glTexParameteri(target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(target, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameteri(target, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
			glTexParameteri(target, GL_TEXTURE_MIN_FILTER, filterInitial);
			glTexParameteri(target, GL_TEXTURE_MAG_FILTER, filterInitial);

			for (int i = 0; i < 6; ++i)
			{
				glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, pixelInternalFormatInitial,
					width, height, 0, pixelFormat, pixelTypeInitial, NULL);
			}

			CHECK_GL_ERROR
		}
		else if (target == GL_TEXTURE_CUBE_MAP_ARRAY || target == GL_TEXTURE_CUBE_MAP_ARRAY_ARB || target == GL_TEXTURE_2D_ARRAY)
		{
			depth = depthInitial;

			glTexParameteri(target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(target, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameteri(target, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
			glTexParameteri(target, GL_TEXTURE_MIN_FILTER, filterInitial);
			glTexParameteri(target, GL_TEXTURE_MAG_FILTER, filterInitial);

			glTexImage3D(target, 0, pixelInternalFormatInitial, width, height, target == GL_TEXTURE_2D_ARRAY ? depth : (depth * 6), 0, pixelFormat, pixelTypeInitial, NULL);

			CHECK_GL_ERROR
		}

		CHECK_GL_ERROR
	}

	void Texture::Init2DArray(const GLuint &widthInitial, const GLuint &heightInitial, const GLuint &depthInitial, const GLubyte *images)
	{
		glGenTextures(1, &textureID); CHECK_GL_ERROR

		target = GL_TEXTURE_2D_ARRAY;
		Rebind();

		width = widthInitial;
		height = heightInitial;
		pixelFormat = GL_RGBA;

		glTexParameteri(target, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(target, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		auto maxTexSize = GetMaxTextureSize();
		auto maxLayers = GetMaxArrayTextureLayers();
		auto currSize = width * height * depthInitial;

		assert(width * height * depthInitial < GetMaxTextureSize() && depthInitial < GetMaxArrayTextureLayers());
			
		glTexStorage3D(target, 1, GL_RGBA8, width, height, depthInitial); CHECK_GL_ERROR
	}

	void Texture::InitSubImage(const GLint &xOffset, const GLint &yOffset, const GLint &zOffset, 
		const GLsizei &width, const GLsizei &height, const GLsizei depth,
		const GLubyte *subImage)
	{
		Rebind();

		glTexSubImage3D(
			target,
			0,
			xOffset,
			yOffset,
			zOffset,
			width,
			height,
			depth,
			pixelFormat,
			GL_UNSIGNED_BYTE,
			subImage
		);

		CHECK_GL_ERROR
	}

	void Texture::SetComparisonMode(GLint compareFunction)
	{
		Rebind();
		glTexParameteri(target, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
		glTexParameteri(target, GL_TEXTURE_COMPARE_FUNC, compareFunction);

		CHECK_GL_ERROR
	}

	void Texture::InitBuffer()
	{
		glGenTextures(1, &textureID);
		target = GL_TEXTURE_BUFFER;
		Rebind();

		CHECK_GL_ERROR
	}

	void Texture::BindBuffer(const GLuint& vboID, const GLenum& internalFormat)
	{
		glBindBuffer(GL_TEXTURE_BUFFER, vboID);
		glBindTexture(GL_TEXTURE_BUFFER, textureID);
		glTexBuffer(GL_TEXTURE_BUFFER, internalFormat, vboID);
	}

	void Texture::Init1D(const unsigned int& widthInitial)
	{
		glGenTextures(1, &textureID);

		target = GL_TEXTURE_1D;
		Rebind();
		width = widthInitial;
		height = 1;
		glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glTexImage1D(GL_TEXTURE_1D, 0, GL_RED,
			width, 0, GL_RED, GL_UNSIGNED_BYTE, NULL);

		CHECK_GL_ERROR
	}

	void Texture::Rebind() const
	{
		if (textureUnit == 0)
		{
			textureUnit = texUnitsCounter++;

			glActiveTexture(GL_TEXTURE0 + textureUnit); CHECK_GL_ERROR
			glBindTexture(target, textureID); CHECK_GL_ERROR
		}
		else
		{
			glActiveTexture(GL_TEXTURE0 + textureUnit); CHECK_GL_ERROR
			glBindTexture(target, textureID); CHECK_GL_ERROR
		}
	}

	void Texture::Unbind() const
	{
		glActiveTexture(GL_TEXTURE0 + textureUnit);
		glBindTexture(target, NULL);

		textureUnit = 0;

		CHECK_GL_ERROR
	}

	GLuint Texture::GetTextureUnit() const
	{
		return textureUnit;
	}

	void Texture::Redraw(void* pixelsPtr = NULL)
	{
		glBindTexture(target, textureID);

		if (target == GL_TEXTURE_2D)
		{
			/*glTexImage2D(
				GL_TEXTURE_2D, 0, mPixelFormat,
				mWidth, mHeight, 0,
				mPixelFormat, GL_UNSIGNED_BYTE, pixelsPtr);*/

			glTexSubImage2D(GL_TEXTURE_2D,
				0,
				0,
				0,
				width,
				height,
				pixelFormat,
				GL_UNSIGNED_BYTE,
				pixelsPtr);
		}
		else if (target == GL_TEXTURE_1D)
		{
			glTexImage1D(GL_TEXTURE_1D, 0, GL_RED,
				width, 0, GL_RED, GL_UNSIGNED_BYTE, pixelsPtr);
		}

		CHECK_GL_ERROR
	}

	const int& Texture::GetWidth() const
	{
		return width;
	}

	const int& Texture::GetHeight() const
	{
		return height;
	}

	GLuint Texture::GetIndex() const
	{
		return textureID;
	}

	int Texture::GetTexUnitCounter()
	{
		return texUnitsCounter;
	}

}
