#pragma once

#include <glm\glm.hpp>

#include "ComponentManager.h"

namespace mg {

	struct LightPoint
	{
		float radius;
		glm::vec3 color;
		SceneVector positionOffset;
		bool dynamic;
		bool active;

		LightPoint(const float& radius, const glm::vec3& color, const bool &dynamic, const bool &active, const SceneVector &positionOffset) :
			radius(radius), color(glm::normalize(color)), dynamic(dynamic), active(active), positionOffset(positionOffset) {}
	};

	// LightPointComponentManager

	class LightPointComponentManager : public ComponentManager
	{
	public:
		LightPointComponentManager(GameplayState *gps) : ComponentManager("lightPoint", gps) {};
		~LightPointComponentManager() = default;

		virtual void AssignComponent(entityx::Entity &entity, const LuaPlus::LuaObject &table) const override;
		virtual LuaPlus::LuaObject GetComponentTable(entityx::Entity& entity) const override;
		virtual LuaPlus::LuaObject ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const override;

		void EnableLightPoint(const LuaPlus::LuaObject &entity, const bool &enabled) const;
		bool IsLightPointEnabled(const LuaPlus::LuaObject &entity) const;
		void SetLightPointOffset(const LuaPlus::LuaObject &entity, const LuaPlus::LuaObject &pos) const;
	};
}