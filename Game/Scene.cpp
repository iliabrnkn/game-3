#include "stdafx.h"
#include <algorithm>

#include "Scene.h"
#include "Paths.h"
#include "ErrorLog.h"
#include "Physics.h"
#include "Vector.h"

namespace mg{
	Scene::Scene():
		modelCount(0),
		sceneGeometryVertsOffset(0),
		sceneGeometryIndsOffset(0)
	{}

	void Scene::Init(LuaPlus::LuaState* luaState)
	{
		geometry.Init(luaState);
		charactersBatch.Init(luaState);
		simpleBatch.Init(luaState);
		particlesBatch.Init(luaState);
		linesBatch.Init(luaState);
		cloudBatch.Init(luaState);

		lightEffect.LoadFromFile(EFFECTS_FOLDER + "Light.lua", luaState);
		spotLightEffect.LoadFromFile(EFFECTS_FOLDER + "SpotLight.lua", luaState);
		directionalLightEffect.LoadFromFile(EFFECTS_FOLDER + "DirectionalLight.lua", luaState);
		geometryEffect.LoadFromFile(EFFECTS_FOLDER + "Default3D.lua", luaState);
		backgroundEffect.LoadFromFile(EFFECTS_FOLDER + "Default2D.lua", luaState);
		simpleEffect.LoadFromFile(EFFECTS_FOLDER + "Simple.lua", luaState);
		particlesEffect.LoadFromFile(EFFECTS_FOLDER + "Particles.lua", luaState);
		linesEffect.LoadFromFile(EFFECTS_FOLDER + "Lines.lua", luaState);
		cloudEffect.LoadFromFile(EFFECTS_FOLDER + "Clouds.lua", luaState);

		trisMesh = new btTriangleMesh();
	}

	// ��������� ������� � ��������� ������
	void Scene::AddQuadToSceneGeometry(const SceneVector &offset, const SceneVector &n, const GidTex *gidTex,
		SceneVector &maxBoundingPoint)
	{
		geometry.GetVertices().Bind();

		std::vector<AnimVertex> quadVertices;

		quadVertices.push_back(AnimVertex(offset.X(), offset.Y(), offset.Z()));
		quadVertices.push_back(AnimVertex(offset.X(), offset.Y(), offset.Z() + 1.f));
		quadVertices.push_back(AnimVertex(offset.X() + 1.f, offset.Y(), offset.Z() + 1.f));
		quadVertices.push_back(AnimVertex(offset.X() + 1.f, offset.Y(), offset.Z()));

		sceneGeometryVertsOffset += 4;
		quadVertices[0].normal = n; quadVertices[1].normal = n; quadVertices[2].normal = n; quadVertices[3].normal = n;

		for (auto &vert : quadVertices)
		{
			vert.objectID = -gidTex->gidNum;
			vert.texCoord = GetVertSpriteTexCoords(vert.vert - (glm::vec3)offset, gidTex);
			vert.texCoord.x = glm::max<float>(vert.texCoord.x - 1.f/(gidTex->layerWidth), 0.f);
		}

		auto indices = std::vector<unsigned int>{
			unsigned int(0),
			unsigned int(1),
			unsigned int(2),
			unsigned int(2),
			unsigned int(0),
			unsigned int(3)
		};

		sceneGeometryIndsOffset += 6;

		// �������� �������
		for (auto &ind : indices)
		{
			alignedIndices.push_back(vertsConverted.size() + ind);
		}

		// �������
		for (auto &vert : quadVertices)
		{
			vertsConverted.push_back(vert);

			maxBoundingPoint.X() = glm::max(maxBoundingPoint.X(), vert.vert.x);
			maxBoundingPoint.Y() = glm::max(maxBoundingPoint.Y(), vert.vert.y);
			maxBoundingPoint.Z() = glm::max(maxBoundingPoint.Z(), vert.vert.z);
		}

		// ��� btBvhTriangleMeshShape, �.�. ��� ������������ � ���������� ������
		trisMesh->addTriangle(SceneVector(quadVertices[0].vert), SceneVector(quadVertices[1].vert), SceneVector(quadVertices[2].vert));
		trisMesh->addTriangle(SceneVector(quadVertices[2].vert), SceneVector(quadVertices[3].vert), SceneVector(quadVertices[0].vert));

		GetSimpleBatch().AddTriangleToSceneGeometry(
			SceneVector(quadVertices[0].vert), SceneVector(quadVertices[1].vert), SceneVector(quadVertices[2].vert)
		);
		GetSimpleBatch().AddTriangleToSceneGeometry(
			SceneVector(quadVertices[2].vert), SceneVector(quadVertices[3].vert), SceneVector(quadVertices[0].vert)
		);

		CHECK_GL_ERROR
	}

	void Scene::AddVertexDataToSceneGeometry(PropsHandler props, const glm::vec3 &offset, const GidTex *gidTex,
		SceneVector &maxBoundingPoint)
	{
		for (auto animVert : props->vertices)
		{
			animVert.objectID = -gidTex->gidNum;
			animVert.texCoord = GetVertSpriteTexCoords(glm::vec3(animVert.vert.x, animVert.vert.y, animVert.vert.z), gidTex,
				props->texOffset);
			animVert.vert += offset;

			// ��������� � ������ ��� ����������� �������
			vertsConverted.push_back(animVert);

			maxBoundingPoint.X() = glm::max(maxBoundingPoint.X(), animVert.vert.x);
			maxBoundingPoint.Y() = glm::max(maxBoundingPoint.Y(), animVert.vert.y);
			maxBoundingPoint.Z() = glm::max(maxBoundingPoint.Z(), animVert.vert.z);
		}

		sceneGeometryVertsOffset += props->vertices.size();

		// �������� �������
		for (int i = 0; i < props->indices.size(); ++i)
		{
			auto alignedInd = vertsConverted.size() + props->indices[i] - props->vertices.size();
			alignedIndices.push_back(alignedInd);

			if (i > 0 && (i + 1) % 3 == 0)
			{
				size_t currFirstIndNum = alignedIndices.size() - 3;

				trisMesh->addTriangle(
					SceneVector(vertsConverted[alignedIndices[currFirstIndNum]].vert),
					SceneVector(vertsConverted[alignedIndices[currFirstIndNum + 1]].vert),
					SceneVector(vertsConverted[alignedIndices[currFirstIndNum + 2]].vert)
				);

				/*GetSimpleBatch().AddTriangleToSceneGeometry(
					SceneVector(vertsConverted[alignedIndices[currFirstIndNum]].vert),
					SceneVector(vertsConverted[alignedIndices[currFirstIndNum + 1]].vert),
					SceneVector(vertsConverted[alignedIndices[currFirstIndNum + 2]].vert)
				);*/
			}
		}

		sceneGeometryIndsOffset += props->indices.size();
	}

	void Scene::FillBuffers()
	{
		geometry.GetIndices().Bind();
		geometry.GetIndices().Fill(alignedIndices);
		geometry.GetIndices().Unbind();

		geometry.GetVertices().Bind();
		geometry.GetVertices().Fill(vertsConverted);
		geometry.GetVertices().Unbind();
	}

	void Scene::CreateTriangleMeshShape(Physics& physics)
	{
		// ������ ����� �� �������������
		btCollisionShape* trisColShape = new btBvhTriangleMeshShape(trisMesh, true);
		btTransform t0; t0.setBasis(btMatrix3x3(1, 0, 0, 0, 1, 0, 0, 0, 1)); t0.setOrigin(btVector3(0.f, 0.f, 0.f));
		trisColShape->setMargin(0.03f);

		levelBody = physics.CreateRigidBody(0.f, t0, trisColShape, SceneVector(0), -1, COL_WALLS, COL_ALL);
		levelBody->setContactStiffnessAndDamping(9999.f, 9999.f);
		levelBody->setActivationState(DISABLE_DEACTIVATION);
		levelBody->setCustomDebugColor(btVector3(1.f, 0.f, 0.f));
	}
}