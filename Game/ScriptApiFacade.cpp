#include "stdafx.h"

#include <chrono>

#include <glm/glm.hpp>

#include "ScriptApiFacade.h"
#include "Position.h"
#include "GameplayState.h"
#include "GUI.h"
#include "AIPiece.h"
#include "Paths.h"
#include "Animation.h"
#include "Collideable.h"
#include "PhysicsSystem.h"
#include "RenderingSystem3D.h"
#include "AutoWalkController.h"
#include "Core.h"
#include "Controls.h"
#include "Level.h"
#include "AnimationSystem.h"
#include "Pathable.h"

#include "LuaHelper.h"

using namespace std;

namespace mg {

	ScriptApiFacade& ScriptApiFacade::Get()
	{
		static auto *instance = (new ScriptApiFacade());

		return *instance;
	}

	// ��������� �����
	void ScriptApiFacade::WatchFloat(const char* name, const float& val) const
	{
		GUI::GetInstance().Watch(name, val);
	}

	// ������� �������� ���������� � ���� �������
	LuaPlus::LuaObject ScriptApiFacade::GetComponent(LuaPlus::LuaObject& table, const char* componentName)
	{
		auto entity = gps->GetEntity(table);

		auto componentManager = repo->At(componentName);

		if (componentManager == nullptr)
		{
			LuaPlus::LuaObject res;
			res.AssignNil();

			return res;
		}
		else
		{
			return componentManager->GetComponentTable(entity);
		}
	}

	// �������� ��������� ��������
	LuaPlus::LuaObject ScriptApiFacade::ModifyComponent(LuaPlus::LuaObject &entityTable, const char *componentName, LuaPlus::LuaObject &componentTable)
	{
		auto entity = gps->GetEntity(entityTable);
		auto componentManager = repo->At(componentName);

		if (componentManager != nullptr)
			return componentManager->ModifyComponent(entity, componentTable);
		else
			return LuaPlus::LuaObject().AssignNil();
	}

	// ������� ��������� �� ��������
	void ScriptApiFacade::RemoveComponent(LuaPlus::LuaObject &entityTable, const char *componentName)
	{
		auto entity = gps->GetEntity(entityTable);
		
		if (repo->At(componentName) != nullptr)
		{
			repo->At(componentName)->RemoveComponent(entity);
		}
	}

	// ��������� ������������ ��������� � ��������
	void ScriptApiFacade::AddComponent(LuaPlus::LuaObject &entityTable, const char *componentName, LuaPlus::LuaObject &componentTable)
	{
		auto entity = gps->GetEntity(entityTable);

		if (repo->At(componentName) != nullptr)
		{
			repo->At(componentName)->AssignComponent(gps->GetEntity(entityTable), componentTable);
		}
	}

	// ������� �������� �� ���������� �������, ��� ������ ���� ����������� �������������� �����������
	LuaPlus::LuaObject ScriptApiFacade::CreateEntity(LuaPlus::LuaObject& table)
	{
		auto entity = gps->entityManager.create();
		auto initialComponents = LuaPlus::LuaHelper::GetTable(table, "initialComponents", false);

		if (!initialComponents.IsNil())
			AssignComponentsToEntity(gps, entity, initialComponents);

		auto id = entity.id().index();

		table.SetInteger("id", id);

		entity.assign<AIPiece>(table); // ���� ��������� ������ ����������� � ���������, ��������� ��������
		entity.component<AIPiece>()->Emit("onInit");

		return table;
	}

	// ������� ��������
	void ScriptApiFacade::RemoveEntity(LuaPlus::LuaObject& table)
	{
		gps->entityManager.destroy(gps->GetEntity(table).id());

		table.AssignNil();
	}

	// ������� ������, ������� ��������� �������-������� ����� ���������� �����
	void ScriptApiFacade::SetTimer(LuaPlus::LuaObject& table, const float& time, const LuaPlus::LuaObject &callback)
	{
		if (!callback.IsFunction()) return;
		
		auto entity = gps->GetEntity(table);

		// ��������� � timerRequests, � �� � timers ��� ����, ����� �������� ����������� ��������� ��� ���������� ������ ������� � �������-��������
		entity.component<AIPiece>()->timerRequests.push_back(LuaTimer(time, callback));

	}

	// ������� ������, ������� ��������� �������-������� ����� ������ ���������� ������� ���� ����������� ��������
	void ScriptApiFacade::SetRepeater(const LuaPlus::LuaObject &table, const float& period, const LuaPlus::LuaObject &callback, 
		const LuaPlus::LuaObject &predicate, const LuaPlus::LuaObject &finalization, const char *name)
	{
		auto entity = gps->GetEntity(table);

		// ��������� � timerRequests, � �� � timers ��� ����, ����� �������� ����������� ��������� ��� ���������� ������ ������� � �������-��������
		entity.component<AIPiece>()->repeaterRequests.push_back(LuaRepeater(period, predicate, callback, finalization, name));
	}

	// ���������� ������� ���� � �������������
	double ScriptApiFacade::GetTimeMS()
	{
		return static_cast<double>(
			std::chrono::duration_cast<std::chrono::milliseconds>(
				std::chrono::system_clock::now().time_since_epoch()
			).count()
		);
	}

	void ScriptApiFacade::ConsoleLog(const char *msg)
	{
		GUI::GetInstance().ConsoleLog(msg);
	}

	// ��������� �������
	void ScriptApiFacade::LoadLevel(const LuaPlus::LuaObject &levelTable)
	{
		gps->LoadLevel(levelTable);
	}

	// ���������, ���������� �� ���� �������� � ����� � ����������
	bool ScriptApiFacade::CheckIfTilesetExists(const char* filename)
	{
		ifstream f(IMAGES_FOLDER + filename);
		auto res = f.good();
		
		return res;
	}

	// ���������� �������� ������� �� ��������� �����������
	LuaPlus::LuaObject ScriptApiFacade::GetEntityByID(const unsigned int& id)
	{
		auto entity = gps->GetEntity(id);

		return (entity.component<AIPiece>()->table);
	}

	// ���������� ������ �� ���������� ���������� �����
	void ScriptApiFacade::CenterCameraAt(const float& x, const float& y)
	{
		gps->GetCamera()->CenterAt(x, y);
	}

	// ������ ����������� ������
	LuaPlus::LuaObject ScriptApiFacade::NormalizeVec(const LuaPlus::LuaObject& table)
	{
		return SceneVector::FromMapCoords(table).Normalized().ToMapCoords(table.GetState());
	}

	// �������� ������������ ��� ���� ��������
	LuaPlus::LuaObject ScriptApiFacade::MixVec(const LuaPlus::LuaObject first, const LuaPlus::LuaObject second, const float& a)
	{
		auto res = SceneVector(
			SceneVector::Mix(
				SceneVector::FromMapCoords(first),
				SceneVector::FromMapCoords(second),
				a
			)
		).ToMapCoords(gps->GetLuaState());

		return res;
	}

	// ������������ �� ����
	LuaPlus::LuaObject ScriptApiFacade::SlerpVec(const LuaPlus::LuaObject first, const LuaPlus::LuaObject second, const float& a)
	{
		auto res = SceneVector(
			SceneVector::Slerp(
				SceneVector::FromMapCoords(first),
				SceneVector::FromMapCoords(second),
				a
			)
		).ToMapCoords(gps->GetLuaState());

		return res;
	}
	
	// ������� ���� ����� ����� ���������� ���������
	float ScriptApiFacade::FindAngle(const LuaPlus::LuaObject& vec1, const LuaPlus::LuaObject& vec2) const
	{
		return SceneVector::FromMapCoords(vec1).Angle(SceneVector::FromMapCoords(vec2), SceneVector(0.f, 1.f, 0.f));
	}

	// ��������/������ ������
	void ScriptApiFacade::ShowWidget(const char* widgetName, const bool& display)
	{
		auto* widget = GUI::GetInstance().GetWidget<Widget>(widgetName);

		if (widget)
			widget->SetDisplayability(display);
	}

	// �������� ������� � ���������
	// ���������� true, ���� ���������� �������� � false - ���� ���
	bool ScriptApiFacade::AddToInventory(const int &x, const int &y, const LuaPlus::LuaObject &entityTable)
	{
		gps->GetInventory()->AddItem(entityTable);

		return false;
	}

	// ���������, ����� �� ����-���� ��� (�������), ���� �� - ���������� ������� ���������� ��������, ���� ��� - nil
	LuaPlus::LuaObject ScriptApiFacade::RayTest(const float &xStart, const float &yStart, const float &zStart,
		const float &xDir, const float &yDir, const float &zDir) const
	{
		// TODO
		//auto start = btVector3(xStart, zStart, yStart);
		//auto end = btVector3(xStart + xDir, zStart + zDir, yStart + yDir);

		//btCollisionWorld::ClosestRayResultCallback rayCallback(start, end);

		//gps->GetPhysics().dynamicsWorld->rayTest(start, end, rayCallback);

		//bool hit = rayCallback.hasHit();

		//auto possibleGhostObj = dynamic_cast<const mgGhostObject*>(rayCallback.m_collisionObject);

		//if (possibleGhostObj) // ���� ������ �� ���-�� �����������
		//{
		//	auto res = gps->GetEntity(possibleGhostObj->GetEntityID()).component<AIPiece>()->table;

		//	return LuaPlus::LuaObject(res);
		//}

		//return LuaPlus::LuaObject();

		return LuaPlus::LuaObject();
	}

	// ��������� ��� ��������� � ������� �������������� ����������� � ��������
	void ScriptApiFacade::AssignComponentsToEntity(entityx::EntityX *entityx, entityx::Entity& entity, const LuaPlus::LuaObject& componentsTable)
	{
		// ���������� ��� ��������� ��������� �����������, � ���� ����� ���������� ��������� � �������, ������� ��� � ������������ � ��������
		for (auto i = 0; i < repo->Count(); ++i)
		{
			LuaPlus::LuaObject currCompTable = componentsTable.GetByName(repo->At(i)->GetComponentTableName().c_str());

			if (currCompTable.GetRef() != -1)
			{
				repo->At(i)->AssignComponent(entity, currCompTable);
			}
		}
	}

	void ScriptApiFacade::DebugShowLevelGeometry()
	{
		gps->GetScene().GetSimpleBatch().ShowLevelGeometry() = !gps->GetScene().GetSimpleBatch().ShowLevelGeometry();
	}
	
	void ScriptApiFacade::DebugShowAABBs()
	{
		gps->GetScene().GetSimpleBatch().ShowAABBs() = !gps->GetScene().GetSimpleBatch().ShowAABBs();
	}

	void ScriptApiFacade::DebugShowCoords()
	{
		gps->GetScene().GetSimpleBatch().ShowCoords() = !gps->GetScene().GetSimpleBatch().ShowCoords();
	}

	void ScriptApiFacade::DebugDrawMeshes()
	{
		gps->systems.system<RenderingSystem3D>()->SwitchDisplayMeshes();
	}

	void ScriptApiFacade::DebugEnableConstraints()
	{
		auto numConstraints = gps->GetPhysics().dynamicsWorld->getNumConstraints();

		for (int i = 0; i < numConstraints; ++i)
		{
			auto *constraint = gps->GetPhysics().dynamicsWorld->getConstraint(i);
			constraint->setEnabled(!constraint->isEnabled());
		}
	}

	void ScriptApiFacade::DebugEnableGravity()
	{
		// TODO: �����������
		if (gps->GetPhysics().dynamicsWorld->getGravity().length() > 0.0)
		{
			gps->GetPhysics().dynamicsWorld->setGravity(btVector3(0.0, 0.0, 0.0));
			GUI::GetInstance().ConsoleLog("gravity disabled");
		}
		else
		{
			GUI::GetInstance().ConsoleLog("gravity enabled");

			auto collisionObjectArray = gps->GetPhysics().dynamicsWorld->getCollisionObjectArray();
			auto numColObj = gps->GetPhysics().dynamicsWorld->getNumCollisionObjects();

			for (int i = 0; i < numColObj; ++i)
			{
				RigidBody *body;
				
				if (body = reinterpret_cast<RigidBody*>(collisionObjectArray[i]))
				{
					body->setGravity(btVector3(0.0, -10.0, 0.0));
				}

			}

			gps->GetPhysics().dynamicsWorld->setGravity(btVector3(0.0, -10.0, 0.0));
		}
	}

	// ������� ����� �������
	float ScriptApiFacade::GetLength(const LuaPlus::LuaObject &vec)
	{
		return SceneVector::FromMapCoords(vec).Length();
	}

	int ScriptApiFacade::Random(const int &maxNum)
	{
		return rand() % (maxNum + 1);
	}

	LuaPlus::LuaObject ScriptApiFacade::CrossProduct(const LuaPlus::LuaObject &vecA, const LuaPlus::LuaObject &vecB)
	{
		return LuaHelper::Vec3ToTable(
			glm::cross(LuaHelper::GetVec3(vecA), LuaHelper::GetVec3(vecB)), 
			gps->GetLuaState()
		);
	}

	float ScriptApiFacade::DotProduct(const LuaPlus::LuaObject &vecA, const LuaPlus::LuaObject &vecB)
	{
		return SceneVector::FromMapCoords(vecA).Dot(SceneVector::FromMapCoords(vecB));
	}

	// ������ ������
	void ScriptApiFacade::DebugDrawVector(const LuaPlus::LuaObject &begin, const LuaPlus::LuaObject &dir, const LuaPlus::LuaObject &color)
	{
		gps->GetScene().GetSimpleBatch().DrawVector(
			SceneVector::FromMapCoords(begin),
			SceneVector::FromMapCoords(dir),
			LuaHelper::GetVec3(color)
		);
	}

	// �������� �������� �������
	bool ScriptApiFacade::DebugLooseBodies()
	{
		return AnimationSystem::debugLooseBodies = !AnimationSystem::debugLooseBodies;
	}

	LuaPlus::LuaObject ScriptApiFacade::GetEntityByScreenCoords(const LuaPlus::LuaObject &screenCoords)
	{
		LuaPlus::LuaObject res;

		SceneVector rayEnd = MatrixHelper::ScreenToScene(
			glm::vec2(LuaHelper::GetVec3(screenCoords)), 
			gps->GetCamera().get()
		);

		auto rayStart = MatrixHelper::ViewerToScene(
			MatrixHelper::SceneToViewer(rayEnd) + SceneVector(0.f, 0.f, gps->GetPhysics().RAY_ENLARGEMENT)
		);
		auto entityID = gps->GetPhysics().GetEntityIDByRay(rayStart, rayEnd);
		
		if (entityID != -1)
			res = gps->GetEntity(entityID).component<AIPiece>()->table;

		return res;
	}

	LuaPlus::LuaObject ScriptApiFacade::GetEntityByMapCoords(const LuaPlus::LuaObject &mapCoords)
	{
		LuaPlus::LuaObject res;

		SceneVector rayEnd = SceneVector::FromMapCoords(mapCoords);

		auto rayStart = MatrixHelper::ViewerToScene(
			MatrixHelper::SceneToViewer(rayEnd) + SceneVector(0.f, 0.f, gps->GetPhysics().RAY_ENLARGEMENT)
		);
		auto entityID = gps->GetPhysics().GetEntityIDByRay(rayStart, rayEnd);

		if (entityID != -1)
			res = gps->GetEntity(entityID).component<AIPiece>()->table;

		return res;
	}

	// ���������� ���������� ����������� ����� � ����, ������������ �� ������ ����������� � �����
	LuaPlus::LuaObject ScriptApiFacade::GetSceneCoordsByScreenCoords(const LuaPlus::LuaObject &screenCoords)
	{
		SceneVector rayEnd = MatrixHelper::ScreenToScene(
			glm::vec2(LuaHelper::GetVec3(screenCoords)),
			gps->GetCamera().get()
		);

		auto rayStart = MatrixHelper::ViewerToScene(
			MatrixHelper::SceneToViewer(rayEnd) + SceneVector(0.f, 0.f, gps->GetPhysics().RAY_ENLARGEMENT)
		);

		SceneVector resVec = gps->GetPhysics().GetHitSceneCoordsByRay(rayStart, rayEnd);

		return resVec.ToMapCoords(gps->GetLuaState());
	}

	// ������������ ������ ������ ���
	LuaPlus::LuaObject ScriptApiFacade::RotateVec(const LuaPlus::LuaObject &vec, const LuaPlus::LuaObject &quat)
	{
		return SceneVector::FromMapCoords(vec).Rotate(LuaHelper::GetBTQuat(quat)).ToMapCoords(gps->GetLuaState());
	}

	const char* ScriptApiFacade::GetScriptsFolder()
	{
		return SCRIPTS_FOLDER.c_str();
	}

	const char* ScriptApiFacade::GetMapsFolder()
	{
		return MAPS_FOLDER.c_str();
	}

	void ScriptApiFacade::SetDefaultCursor(const bool &highlighted)
	{
		GUI::GetInstance().SetDefaultCursor(highlighted);
	}

	void ScriptApiFacade::SetAimCursor(const float &factor)
	{
		GUI::GetInstance().SetAimCursor(factor);
	}

	void ScriptApiFacade::SetCursorInvisible(const bool &invisible)
	{
		GUI::GetInstance().SetCursorVisibility(!invisible);
	}

	void ScriptApiFacade::LockAimCursor(const LuaPlus::LuaObject &coords)
	{
		auto screenCoords = SceneVector::FromMapCoords(coords).ToScreenCoords(gps->GetCamera());
		GUI::GetInstance().SetTargetLockAimCoords(sf::Vector2f(screenCoords.x, screenCoords.y));
		GUI::GetInstance().Watch("screenCoords.x", screenCoords.x);
		GUI::GetInstance().Watch("screenCoords.y", screenCoords.y);
	}

	// ������������ ������ �������
	void ScriptApiFacade::Init()
	{
		repo = std::move(std::unique_ptr<ComponentManagersRepo>(new ComponentManagersRepo(gps)));

		//---- �����
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_loadLevel", Get(), &ScriptApiFacade::LoadLevel);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_createEntity", Get(), &ScriptApiFacade::CreateEntity);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_removeEntity", Get(), &ScriptApiFacade::RemoveEntity);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_getComponent", Get(), &ScriptApiFacade::GetComponent);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_modifyComponent", Get(), &ScriptApiFacade::ModifyComponent);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_removeComponent", Get(), &ScriptApiFacade::RemoveComponent);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_addComponent", Get(), &ScriptApiFacade::AddComponent);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_setTimer", Get(), &ScriptApiFacade::SetTimer);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_setRepeater", Get(), &ScriptApiFacade::SetRepeater);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_centerCameraAt", Get(), &ScriptApiFacade::CenterCameraAt);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_getEntityByScreenCoords", Get(), &ScriptApiFacade::GetEntityByScreenCoords);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_getEntityByMapCoords", Get(), &ScriptApiFacade::GetEntityByMapCoords);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_getSceneCoordsByScreenCoords", Get(), &ScriptApiFacade::GetSceneCoordsByScreenCoords);

		gps->GetLuaState()->GetGlobals().RegisterDirect("c_associateGIDWProps", *gps, &GameplayState::AssociateGIDWProps);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_setDebugGid", *(gps->GetLevel()), &Level::SetDebugGid);

		//---- ����������

		// ���������� ���������� ����� ����� ����������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_getDistance",
			dynamic_cast<PositionComponentManager&>(*(repo->At("position"))),
			&PositionComponentManager::GetDistance);

		// ���������� ���������� ����� ����� ����������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_getPos",
			dynamic_cast<PositionComponentManager&>(*(repo->At("position"))),
			&PositionComponentManager::GetPos);

		// ���������� ���� ��������� ����������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_addIKPose", 
			dynamic_cast<InverseKinematicsControllerComponentManager&>(*(repo->At("inverseKinematicsController"))),
			&InverseKinematicsControllerComponentManager::AddPose);

		// ��������� ��������� �������� ���� �����
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_getBoneBodyWorldArrayIndex", 
			dynamic_cast<SkeletonComponentManager&>(*(repo->At("skeleton"))),
			&SkeletonComponentManager::GetBoneBodyWorldArrayIndex);

		// ��������� ��������� ���� �����
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_getBoneBodyPos",
			dynamic_cast<SkeletonComponentManager&>(*(repo->At("skeleton"))),
			&SkeletonComponentManager::GetBoneBodyPos);

		// ���������� �����
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_looseBoneBodies",
			dynamic_cast<SkeletonComponentManager&>(*(repo->At("skeleton"))),
			&SkeletonComponentManager::LooseBoneBodies);

		// ���� �� � �������� ������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_hasSkeleton",
			dynamic_cast<SkeletonComponentManager&>(*(repo->At("skeleton"))),
			&SkeletonComponentManager::HasSkeleton);

		// ����� ���������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_setDead",
			dynamic_cast<SkeletonComponentManager&>(*(repo->At("skeleton"))),
			&SkeletonComponentManager::SetDead);

		// ��������� ����������
		/*gps->GetLuaState()->GetGlobals().RegisterDirect("c_addConstraint",
			dynamic_cast<CollideableComponentManager&>(*(repo->At("collideable"))),
			&CollideableComponentManager::AddConstraint);*/

		gps->GetLuaState()->GetGlobals().RegisterDirect("c_setWeaponBody",
			dynamic_cast<CollideableComponentManager&>(*(repo->At("collideable"))),
			&CollideableComponentManager::SetWeaponBody);

		// �������/����� ���� ��� ������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_debugTransformEntity",
			dynamic_cast<CollideableComponentManager&>(*(repo->At("collideable"))),
			&CollideableComponentManager::DebugTransformEntity);

		// �������� id �������� ���� ���������� collideable
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_getWorldArrayIndex",
			dynamic_cast<CollideableComponentManager&>(*(repo->At("collideable"))),
			&CollideableComponentManager::GetWorldArrayIndex);

		// �������� �� �������� ��������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_orientBoneBodyToPoint",
			dynamic_cast<SkeletonComponentManager&>(*(repo->At("skeleton"))),
			&SkeletonComponentManager::OrientBoneBodyToPoint);

		// ���������� ���� �� �������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_orientBoneBodyToPoint",
			dynamic_cast<SkeletonComponentManager&>(*(repo->At("skeleton"))),
			&SkeletonComponentManager::OrientBoneBodyToPoint);

		// ��������� ����������� �������� ������ ��������/������ ��� ���������� ���������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_setBoneOrientationRatio",
			dynamic_cast<SkeletonComponentManager&>(*(repo->At("skeleton"))),
			&SkeletonComponentManager::SetBoneOrientationRatio);

		// ��������� ������������� ������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_enableBoneBodyAnimation",
			dynamic_cast<SkeletonComponentManager&>(*(repo->At("skeleton"))),
			&SkeletonComponentManager::EnableBoneBodyAnimation);

		// ��������� ������� ���� �����
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_setBoneBodyPos",
			dynamic_cast<SkeletonComponentManager&>(*(repo->At("skeleton"))),
			&SkeletonComponentManager::SetBoneBodyPos);

		// �������� ����������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_addBoneBodyConstraint",
			dynamic_cast<SkeletonComponentManager&>(*(repo->At("skeleton"))),
			&SkeletonComponentManager::AddBoneBodyConstraint);

		// ������� ����������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_removeBoneBodyConstraint",
			dynamic_cast<SkeletonComponentManager&>(*(repo->At("skeleton"))),
			&SkeletonComponentManager::RemoveBoneBodyConstraint);

		// ���������/���������� �����������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_enableBoneBodyConstraint",
			dynamic_cast<SkeletonComponentManager&>(*(repo->At("skeleton"))),
			&SkeletonComponentManager::EnableBoneBodyConstraint);

		// ����� ������������ � ����� ����
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_getNumConstraints",
			dynamic_cast<SkeletonComponentManager&>(*(repo->At("skeleton"))),
			&SkeletonComponentManager::GetNumConstraints);

		// ���������� ����������� ��������� �������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_setBodiesInertiaDamping",
			dynamic_cast<SkeletonComponentManager&>(*(repo->At("skeleton"))),
			&SkeletonComponentManager::SetBodiesInertiaDamping);

		// ������������ ��������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_playAnim", 
			dynamic_cast<AnimationBatchComponentManager&>(*(repo->At("animationBatch"))),
			&AnimationBatchComponentManager::PlayAnimation);

		// ���������� �� ����� ��������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_enableAnimationChanging",
			dynamic_cast<AnimationBatchComponentManager&>(*(repo->At("animationBatch"))),
			&AnimationBatchComponentManager::EnableAnimationChanging);

		// ��������� �������� ��������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_move",
			dynamic_cast<MoveableComponentManager&>(*(repo->At("moveable"))),
			&MoveableComponentManager::Move);

		// ��������� ��������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_stop",
			dynamic_cast<MoveableComponentManager&>(*(repo->At("moveable"))),
			&MoveableComponentManager::Stop);

		// ������� �������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_orientBodyTo",
			dynamic_cast<MoveableComponentManager&>(*(repo->At("moveable"))),
			&MoveableComponentManager::OrientBodyTo);

		// ���������� ����� ������� ��������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_getTargetVelocity",
			dynamic_cast<MoveableComponentManager&>(*(repo->At("moveable"))),
			&MoveableComponentManager::GetTargetVelocity);

		// �������� ��������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_setMaxLinVelocity",
			dynamic_cast<MoveableComponentManager&>(*(repo->At("moveable"))),
			&MoveableComponentManager::SetMaxLinVelocity);

		// ���������� �� �����
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_orientTo",
			dynamic_cast<DirectionComponentManager&>(*(repo->At("direction"))),
			&DirectionComponentManager::OrientTo);

		// ���������� �����������, � ������� ������� ��������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_getDir",
			dynamic_cast<DirectionComponentManager&>(*(repo->At("direction"))),
			&DirectionComponentManager::GetDir);

		// ���������� ��������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_applyCentralImpulse",
			dynamic_cast<CollideableComponentManager&>(*(repo->At("collideable"))),
			&CollideableComponentManager::ApplyCentralImpulse);

		gps->GetLuaState()->GetGlobals().RegisterDirect("c_applyImpulse",
			dynamic_cast<CollideableComponentManager&>(*(repo->At("collideable"))),
			&CollideableComponentManager::ApplyImpulse);

		// ���������� ��������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_applyTorqueImpulse",
			dynamic_cast<CollideableComponentManager&>(*(repo->At("collideable"))),
			&CollideableComponentManager::ApplyTorqueImpulse);

		gps->GetLuaState()->GetGlobals().RegisterDirect("c_applyCentralForce", 
			dynamic_cast<CollideableComponentManager&>(*(repo->At("collideable"))),
			&CollideableComponentManager::ApplyCentralForce);
		// ���������� ��������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_setAngularVelocity",
			dynamic_cast<CollideableComponentManager&>(*(repo->At("collideable"))),
			&CollideableComponentManager::SetAngularVelocity);

		// ���������� ����
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_getBodyPos",
			dynamic_cast<CollideableComponentManager&>(*(repo->At("collideable"))),
			&CollideableComponentManager::GetBodyPos);

		// ����������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_autoWalk",
			dynamic_cast<AutoWalkControllerComponentManager&>(*(repo->At("autoWalkController"))),
			&AutoWalkControllerComponentManager::AutoWalk);

		gps->GetLuaState()->GetGlobals().RegisterDirect("c_setAutoWalkActive",
			dynamic_cast<AutoWalkControllerComponentManager&>(*(repo->At("autoWalkController"))),
			&AutoWalkControllerComponentManager::SetAutoWalkActive);

		// ������� ��� ��������� ����������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_clearPoses",
			dynamic_cast<InverseKinematicsControllerComponentManager&>(*(repo->At("inverseKinematicsController"))),
			&InverseKinematicsControllerComponentManager::ClearPoses);

		// ��������� ���������� �� ��������� ������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_setActiveStrikeable",
			dynamic_cast<StrikeableComponentManager&>(*(repo->At("strikeable"))),
			&StrikeableComponentManager::SetActiveStrikeable);

		// �������� ���������� ��������� ������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_isActiveStrikeable",
			dynamic_cast<StrikeableComponentManager&>(*(repo->At("strikeable"))),
			&StrikeableComponentManager::IsActiveStrikeable);

		// �������� ������ ���/����
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_enableParticleEmitter",
			dynamic_cast<ParticleEmitterComponentManager&>(*(repo->At("particleEmitter"))),
			&ParticleEmitterComponentManager::EnableParticleEmitter);

		// �����
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_drawTrace",
			dynamic_cast<TracerComponentManager&>(*(repo->At("tracer"))),
			&TracerComponentManager::DrawTrace);

		// ���������/���������� �������� ������������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_enableLightPoint",
			dynamic_cast<LightPointComponentManager&>(*(repo->At("lightPoint"))),
			&LightPointComponentManager::EnableLightPoint);

		// ������� �� ��� ��� ����������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_isLightPointEnabled",
			dynamic_cast<LightPointComponentManager&>(*(repo->At("lightPoint"))),
			&LightPointComponentManager::IsLightPointEnabled);

		// �������� �����������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_setLightPointOffset",
			dynamic_cast<LightPointComponentManager&>(*(repo->At("lightPoint"))),
			&LightPointComponentManager::SetLightPointOffset);

		// ���������/���������� ������������� �����
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_enableDirectionalLight",
			dynamic_cast<DirectionalLightComponentManager&>(*(repo->At("directionalLight"))),
			&DirectionalLightComponentManager::EnableDirectionalLight);

		// ������� ��� ��� ������������ ����
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_isDirectionalLightEnabled",
			dynamic_cast<DirectionalLightComponentManager&>(*(repo->At("directionalLight"))),
			&DirectionalLightComponentManager::IsDirectionalLightEnabled);

		// ������� ����� ������������� ���������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_setDirectionalLightColor",
			dynamic_cast<DirectionalLightComponentManager&>(*(repo->At("directionalLight"))),
			&DirectionalLightComponentManager::SetDirectionalLightColor);

		// ������� ������� � ���������� ������ �������� � �������� �������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_findPath", 
			dynamic_cast<PathableComponentManager&>(*(repo->At("pathable"))), 
			&PathableComponentManager::FindPath);
		
		// ������� ������ �������� ��������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_clearWayPoints", 
			dynamic_cast<PathableComponentManager&>(*(repo->At("pathable"))), 
			&PathableComponentManager::ClearWayPoints);
		
		// �������� � ������ ��������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_goToLastWayPoint",
			dynamic_cast<PathableComponentManager&>(*(repo->At("pathable"))),
			&PathableComponentManager::GoToLastWayPoint);

		// ���� �� � �������� �������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_hasWayPoints",
			dynamic_cast<PathableComponentManager&>(*(repo->At("pathable"))),
			&PathableComponentManager::HasWayPoints);

		// �������� ��������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_loadResources",
			*gps, &GameplayState::LoadResources);

		// ���������� �������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_setTimeFactor",
			*gps, &GameplayState::SetTimeFactor);

		// ������� ���� �� ������
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_getMousePos", &Controls::GetMousePos);

		//---- gui
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_showWidget", Get(), &ScriptApiFacade::ShowWidget);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_addToInventory", Get(), &ScriptApiFacade::AddToInventory);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_setDefaultCursor", Get(), &ScriptApiFacade::SetDefaultCursor);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_setAimCursor", Get(), &ScriptApiFacade::SetAimCursor);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_setCursorInvisible", Get(), &ScriptApiFacade::SetCursorInvisible);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_lockAimCursor", Get(), &ScriptApiFacade::LockAimCursor);

		//---- help
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_getScriptsFolder", Get(), &ScriptApiFacade::GetScriptsFolder);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_getMapsFolder", Get(), &ScriptApiFacade::GetMapsFolder);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_watchFloat", Get(), &ScriptApiFacade::WatchFloat);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_getTimeMS", Get(), &ScriptApiFacade::GetTimeMS);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_consoleLog", Get(), &ScriptApiFacade::ConsoleLog);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_checkIfTilesetExists", Get(), &ScriptApiFacade::CheckIfTilesetExists);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_getEntityByID", Get(), &ScriptApiFacade::GetEntityByID);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_normalizeVec", Get(), &ScriptApiFacade::NormalizeVec);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_mixVec", Get(), &ScriptApiFacade::MixVec);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_slerpVec", Get(), &ScriptApiFacade::SlerpVec);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_findAngle", Get(), &ScriptApiFacade::FindAngle);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_rayTest", Get(), &ScriptApiFacade::RayTest);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_getLength", Get(), &ScriptApiFacade::GetLength);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_random", Get(), &ScriptApiFacade::Random);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_crossProduct", Get(), &ScriptApiFacade::CrossProduct);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_dotProduct", Get(), &ScriptApiFacade::DotProduct);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_rotateVec", Get(), &ScriptApiFacade::RotateVec);

		/*gps->GetLuaState()->GetGlobals().RegisterDirect("c_playAnimation", Get(), &ScriptApiFacade::PlayAnimationLooped);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_playAnimationOnce", Get(), &ScriptApiFacade::PlayAnimationOnce);*/

		//---- debug
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_debugShowAABBs", Get(), &ScriptApiFacade::DebugShowAABBs);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_debugShowLevelGeometry", Get(), &ScriptApiFacade::DebugShowLevelGeometry);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_debugShowCoords", Get(), &ScriptApiFacade::DebugShowCoords);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_debugEnableGravity", Get(), &ScriptApiFacade::DebugEnableGravity);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_debugDrawMeshes", Get(), &ScriptApiFacade::DebugDrawMeshes);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_debugEnableConstraints", Get(), &ScriptApiFacade::DebugEnableConstraints);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_debugDrawVector", Get(), &ScriptApiFacade::DebugDrawVector);
		gps->GetLuaState()->GetGlobals().RegisterDirect("c_debugLooseBodies", Get(), &ScriptApiFacade::DebugLooseBodies);
	}
}