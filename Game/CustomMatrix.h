#pragma once

#include <glm/glm.hpp>
#include <memory>

#include "Vector.h"

// ����� ��� ���������� �������� ���������� �������

namespace mg {

	template <typename T>
	struct CustomMatrix
	{
		int rows, cols;
		double **components; // column-major

		CustomMatrix(const int &rows, const int &cols) :
			rows(rows), cols(cols),
			components(new T*[rows])
		{
			for (int i = 0; i < rows; ++i)
			{
				components[i] = new T[cols];

				for (int j = 0; j < cols; ++j)
				{
					components[i][j] = T(0);
				}
			}
		}

		CustomMatrix(const CustomMatrix<T> &m) :
			rows(m.rows), cols(m.cols),
			components(new T*[rows])
		{
			for (int i = 0; i < rows; ++i)
			{
				components[i] = new T[cols];
				std::copy(m[i], m[i] + cols, components[i]);
			}
		}

		~CustomMatrix()
		{
			for (int i = 0; i < rows; ++i)
				delete[] components[i];

			delete[] components;
		}

		inline void SetRow(const int &colNum, const T* data)
		{
			std::copy(data, data + cols, components[colNum]);
		}

		inline CustomMatrix<T> operator= (const CustomMatrix<T> &m)
		{
			for (int i = 0; i < rows; ++i)
				delete[] components[i];

			delete[] components;

			// ---

			components = new T*[m.rows];

			for (int i = 0; i < m.rows; ++i)
			{
				components[i] = new T[m.cols];
				std::copy(m.components[i], m.components[i] + m.cols, components[i]);
			}

			cols = m.cols;
			rows = m.rows;

			return *this;
		}

		inline CustomMatrix<T> Transposed() const
		{
			auto newMx = CustomMatrix(cols, rows);

			for (int j = 0; j < cols; ++j)
			{
				newMx.components[j] = new double[rows];

				for (int i = 0; i < rows; ++i)
				{
					newMx.components[j][i] = components[i][j];
				}
			}

			return newMx;
		}

		inline void Transpose()
		{
			std::swap(cols, rows);
			std::swap(components, this->Transposed().components);
		}

		inline T* operator[] (const int &rowNum)
		{
			return components[rowNum];
		}

		inline const T* operator[] (const int &rowNum) const
		{
			return components[rowNum];
		}

		inline CustomMatrix<T> operator* (const T &arg) const
		{
			CustomMatrix<T> res(*this);

			for (int i = 0; i < rows; ++i)
			{
				for (int j = 0; j < cols; ++j)
				{
					res[i][j] *= arg;
				}
			}

			return res;
		}

		inline std::vector<double> operator* (const SceneVector &de)
		{
			assert(this->cols == 3);

			std::vector<double> res(rows);

			for (int i = 0; i < rows; ++i)
			{
				res[i] = 0.0;

				for (int j = 0; j < 3; ++j)
				{
					res[i] += components[i][j] * de[j];
				}
			}

			return res;
		}

		inline CustomMatrix<T> operator/ (const T &arg) const
		{
			CustomMatrix<T> res(*this);

			for (int i = 0; i < rows; ++i)
			{
				for (int j = 0; j < cols; ++j)
				{
					res[i][j] /= arg;
				}
			}

			return res;
		}

		inline CustomMatrix<T> operator* (const CustomMatrix<T> &m) const
		{
			assert(this->cols == m.rows);

			CustomMatrix<T> res(this->rows, m.cols);

			for (int i = 0; i < this->rows; ++i)
			{
				for (int j = 0; j < m.cols; ++j)
				{
					for (int k = 0; k < min(m.rows, this->cols); ++k)
						res[i][j] += components[i][k] * m[k][j];
				}
			}

			return res;
		}

		CustomMatrix<T> GetMinorMatrix(const int &excludedRowNum, const int &excludedColNum) const
		{
			CustomMatrix<T> res(rows - 1, cols - 1);

			for (int i = 0; i < rows; ++i)
			{
				if (i == excludedRowNum) continue;

				for (int j = 0; j < cols; ++j)
				{
					if (j == excludedColNum) continue;

					res[i < excludedRowNum ? i : i - 1][j < excludedColNum ? j : j - 1] = components[i][j];
				}
			}

			return res;
		}

		inline T Determinant() const
		{
			assert(cols == rows); // ������ ��� ���������� ������

			T res = T(0);

			if (rows == 2 && cols == 2)
			{
				res = components[0][0] * components[1][1] - components[0][1] * components[1][0];
			}
			else
			{
				for (int j = 0; j < cols; ++j)
				{
					res = res + components[0][j] * GetAlgebraicAugment(0, j);
				}
			}

			return res;
		}

		inline T GetAlgebraicAugment(const int &row, const int &col) const
		{
			return glm::pow(-1, row + col) * GetMinorMatrix(row, col).Determinant();
		}

		inline CustomMatrix<T> GetAdjugateMatrix() const
		{
			CustomMatrix<T> res(cols, rows);

			auto transposed = this->Transposed();

			for (int i = 0; i < rows; ++i)
			{
				for (int j = 0; j < cols; ++j)
				{
					res[i][j] = transposed.GetAlgebraicAugment(i, j);
				}
			}

			return res;
		}

		inline CustomMatrix<T> Inversed() const
		{
			return GetAdjugateMatrix() / this->Determinant();
		}

		inline void Inverse()
		{
			*this = this->Inversed();
		}

		inline void DebugPrint() const
		{
			for (int i = 0; i < rows; ++i)
			{
				for (int j = 0; j < cols; ++j)
				{
					printf("%f ", components[i][j]);
				}

				printf("\r\n");
			}

			printf("\r\n");
		}
	};

}