#pragma once

#include <string>
#include <unordered_map>
#include <deque>
#include <random>
#include <ctime>

#include <btBulletDynamicsCommon.h>

#include <lua\LuaPlus.h>

#include "ComponentManager.h"

#include "Vector.h"
#include "CustomMatrix.h"

namespace mg
{
	struct BoneBody;
	struct Skeleton;

	//������� �����
	struct Jacobian : public CustomMatrix<double>
	{
		Jacobian(const int &joints) :
			CustomMatrix<double>(3, joints)
		{
		}

		Jacobian(const CustomMatrix &m) :
			CustomMatrix<double>(m)
		{}

		Jacobian(const Jacobian &jacobian)
			: CustomMatrix(jacobian)
		{
		}

		Jacobian() :
			CustomMatrix(3, 3)
		{
		}

		void AddEntry(const int &effectorNum, SceneVector &de)
		{
			components[0][effectorNum] = de.X();
			components[1][effectorNum] = de.Y();
			components[2][effectorNum] = de.Z();
		}

		inline void GLMDebugPrint(const glm::mat3& m) const
		{
			auto mt = glm::transpose(m);

			for (int i = 0; i < 3; ++i)
			{
				for (int j = 0; j < 3; ++j)
				{
					printf("%f ", mt[i][j]);
				}

				printf("\r\n");
			}

			printf("\r\n");
		}

		inline CustomMatrix PseudoInversed() const
		{
			//auto res = (this->Transposed() * *this).Inversed() * this->Transposed();
			return Transposed();
		}
	};



	// �������� �������� ������ �����
	const double IK_DEFAULT_TOTAL_TIME = 0.0001;
	const double SOLVING_SPAN = 1.0;

	// ������������ ��������� ������ ���� ���������, �� �������� � ��������
	struct Pose
	{
		Jacobian jacobian;
		SceneVector incrementBefore;

		std::unordered_map<std::string, btTransform> res;
		double totalTime;
		float velocityScalar;
		int effectorNum;
		mutable bool staled;

		std::string firstEffector, lastEffector;

		SceneVector endEffectorFinalPos;
		SceneVector finalEndEffectorTargetPosRelative;
		SceneVector currentEndEffectorTargetPos;
		SceneVector revolutionAxis;

		// ������ ��� �� ����, ������ ������ �������� - ��������� �������� �� ����, ������ ������ �������� - �������� ��������
		std::deque<BoneBody*> effectorChain;

		Pose(Skeleton &skeleton, const std::string &startEffectorName, const std::string &endEffectorName,
			const SceneVector &endEffectorFinalPos, const double &totalTime = IK_DEFAULT_TOTAL_TIME);

		~Pose();

		// ���������� �������� ������������� de �� ����� dt
		SceneVector Pose::GetTargetPosIncrement(const double &dt) const;

		SceneVector Pose::GetStartEffectorCurrentPos() const;
		SceneVector Pose::GetEndEffectorCurrentPos() const;

		inline BoneBody* Pose::GetStartEffectorBody()
		{
			return *effectorChain.begin();
		}

		inline BoneBody* Pose::GetEndEffectorBody()
		{
			return *(--effectorChain.end());
		}

	 	inline const BoneBody* Pose::GetStartEffectorBody() const
		{
			return *effectorChain.begin();
		}

		inline const BoneBody* Pose::GetEndEffectorBody() const
		{
			return *(--effectorChain.end());
		}

		inline SceneVector Pose::GetFinalEndEffectorPosition() const
		{
			return GetStartEffectorCurrentPos() + finalEndEffectorTargetPosRelative;
		}

		// ���������� �������� �������� ������ ���������� ����� ������� �� ����� dt
		std::unordered_map<std::string, btTransform> Solve(GameplayState *gps, Skeleton &skeleton, const double &dt);
	};

	struct InverseKinematicsController
	{
		std::vector<Pose> poses;

		InverseKinematicsController()
		{
			poses.reserve(10);
		}

		inline void AddPose(const Pose &newPose)
		{
			auto iterToRemove = std::remove_if(poses.begin(), poses.end(), 
				[&newPose](auto &pose) { 
				return pose.firstEffector == newPose.firstEffector
					&& pose.lastEffector == newPose.lastEffector;
			});

			poses.erase(iterToRemove, poses.end());

			poses.push_back(newPose);
		}
	};

	// InverseKinematicsManager 

	class InverseKinematicsControllerComponentManager : public ComponentManager
	{
	public:
		InverseKinematicsControllerComponentManager(GameplayState *gps) : ComponentManager("inverseKinematicsController", gps) {}

		~InverseKinematicsControllerComponentManager() {};

		virtual void AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const override;
		virtual LuaPlus::LuaObject ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const override;
		virtual LuaPlus::LuaObject GetComponentTable(entityx::Entity& entity) const override;
		void AddPose(const LuaPlus::LuaObject &entityTable, const LuaPlus::LuaObject &poseTable);
		void ClearPoses(const LuaPlus::LuaObject &entityTable);
	};
}