#include "stdafx.h"

#include <unordered_map>
#include <algorithm>

#include "Model.h"
#include "Animation.h"
#include "GameplayState.h"
#include "LuaHelper.h"
#include "Level.h"

using namespace std;

namespace mg {

	unordered_map<string, AnimationHandler> AnimationBatch::animationHandlerPrototypes;

	//--- MeshBatchComponentManager
	void MeshBatchComponentManager::AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		MeshBatch meshBatch;

		auto meshes = LuaPlus::LuaHelper::GetTable(table, "meshes");

		auto skinGid = LuaPlus::LuaHelper::GetInteger(table, "skinGid", false, 1);
		auto texLayerNum = gps->GetLevel()->GetSkinAtlas().GetGidTex(skinGid)->texLayerNum;

		for (LuaPlus::LuaTableIterator iter(meshes); iter; iter.Next())
		{
			//if (iter.GetValue().IsString() && gps->HasElement<Mesh>(iter.GetValue().ToString()))
			//{
			//	// TODO: �����������
			//	auto mesh = gps->CreateElement<Mesh>(iter.GetValue().ToString());

			//	meshBatch.meshes.push_back(Mesh(*mesh));
			//}

			if (iter.GetValue().IsString() && gps->HasElement<Mesh>(iter.GetValue().ToString()))
				meshBatch.meshes.push_back(Mesh(*gps->CreateElement<Mesh>(iter.GetValue().ToString())));

		}

		meshBatch.scale = LuaPlus::LuaHelper::GetFloat(table, "scale", false, MODEL_SCALE);
		
		LuaPlus::LuaObject materialTable = LuaPlus::LuaHelper::GetTable(table, "material", false);
		
		if (!materialTable.IsNil())
		{
			meshBatch.material.r = LuaPlus::LuaHelper::GetFloat(materialTable, "r", false, 0.f);
			meshBatch.material.g = LuaPlus::LuaHelper::GetFloat(materialTable, "g", false, 0.f);
			meshBatch.material.b = LuaPlus::LuaHelper::GetFloat(materialTable, "b", false, 0.f);
			meshBatch.material.a = LuaPlus::LuaHelper::GetFloat(materialTable, "a", false, 1.f);
		}
		else
			meshBatch.material = glm::vec4(-1.f);

		// ����������� ����� ����, ���� �� �����
		btTransform initialTransform = btTransform::getIdentity();
		auto initialTransformTable = LuaPlus::LuaHelper::GetTable(table, "initialTransform", false);

		if (!initialTransformTable.IsNil())
		{
			initialTransform = LuaHelper::GetBTTransform(initialTransformTable);
		}

		meshBatch.initialTransform = MatrixHelper::ToGLMMatrix(initialTransform);
		
		entity.assign<MeshBatch>(meshBatch);
	}

	LuaPlus::LuaObject MeshBatchComponentManager::GetComponentTable(entityx::Entity& entity) const
	{
		LuaPlus::LuaObject res;

		if (entity.has_component<MeshBatch>())
		{
			auto componentHandler = entity.component<MeshBatch>();

			res.AssignNewTable(gps->GetLuaState());

			int i = 1;

			for (auto mesh : entity.component<MeshBatch>()->meshes)
			{
				res.SetString(i, mesh.name.c_str());
				++i;
			}
		}
		else
		{
			res.AssignNil();
		}

		return res;
	};

	LuaPlus::LuaObject MeshBatchComponentManager::ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		if (!entity.has_component<MeshBatch>())
			return LuaPlus::LuaObject().AssignNil();

		// ����� ������� ������ �� ������, ��������� �����������, ��� ���� - ��� ����, � �������� - 
		// ������� ����������, ������������, ������������ �� ��� ���� ��� � ����� ��� ���
		std::unordered_map<std::string, bool> newMeshNames;

		for (LuaPlus::LuaTableIterator iter(table); iter; iter.Next())
		{
			if (iter.GetValue().IsString() && gps->HasElement<Mesh>(iter.GetValue().ToString()))
				newMeshNames[iter.GetValue().ToString()] = false;
		}

		// ������� ��� ���� � �����, ������� ������ �� �����
		auto removeIter = std::remove_if(entity.component<MeshBatch>()->meshes.begin(), entity.component<MeshBatch>()->meshes.end(), 
			[&newMeshNames](auto& x) {
				auto foundIter = newMeshNames.find(x.name); 
				
				if (foundIter == newMeshNames.end())
				{
					foundIter->second = true;
					return true;
				}
				
				return false;
			}
		);

		entity.component<MeshBatch>()->meshes.erase(removeIter, entity.component<MeshBatch>()->meshes.end());

		// ��������� � ������� ����� ����
		for (auto pair : newMeshNames)
		{
			if (pair.second) // ���� ���� ��� �� ������������ � ��������
			{
				entity.component<MeshBatch>()->meshes.push_back(*(gps->CreateElement<Mesh>(pair.first)));
			}
		}

		return GetComponentTable(entity);
	}
}