#include "stdafx.h"

#include <algorithm>

#include "InverseKinematicsController.h"

#include "GameplayState.h"
#include "Skeleton.h"
#include "LuaHelper.h"

namespace mg
{
	Pose::Pose(Skeleton &skeleton, const std::string &startEffectorName, const std::string &endEffectorName,
		const SceneVector &endEffectorFinalPosInitial, const double &totalTime) :
		effectorNum(0),
		staled(false),
		firstEffector(startEffectorName), lastEffector(endEffectorName), endEffectorFinalPos(endEffectorFinalPosInitial)
	{
		auto *currBoneBody = skeleton.FindBoneBodyBySecondJoint(endEffectorName);
		double lenSum = 0.0;
		
		do // ���������� ����� ��� ������, �������� �� ��������� ��������� � ����������
		{
			lenSum += currBoneBody->axisOffset.Length() * 2.0;
			effectorChain.push_front(currBoneBody);
			++effectorNum;

			if (currBoneBody->firstJoint == startEffectorName || currBoneBody == nullptr)
				break;

			currBoneBody = skeleton.GetBoneBodyParent(*currBoneBody);

		} while (true); // ������������ ���������� ��������� ��������� �� ���������� - ����� ���� ���� ������

		auto &startEffectorBoneBody = *effectorChain.begin();

		finalEndEffectorTargetPosRelative = endEffectorFinalPosInitial - startEffectorBoneBody->GetFirstJointPos();
		finalEndEffectorTargetPosRelative.SetLength(lenSum);
		endEffectorFinalPos = startEffectorBoneBody->GetFirstJointPos() + finalEndEffectorTargetPosRelative;

		velocityScalar = (GetFinalEndEffectorPosition() - GetEndEffectorCurrentPos()).Length() / (float)totalTime;

		// ��� �������� ���� ��������
		revolutionAxis = (GetEndEffectorCurrentPos() - GetStartEffectorCurrentPos()).Cross(
			finalEndEffectorTargetPosRelative).Normalized();

		if (revolutionAxis.IsNan()) // ���� ������� �������������
		{
			revolutionAxis = SceneVector(1.f, 0.f, 0.f);
		}
	}

	SceneVector Pose::GetTargetPosIncrement(const double &dt) const
	{
		auto vecToFinalPos = GetFinalEndEffectorPosition() - GetEndEffectorCurrentPos();

		if (vecToFinalPos.Length() > velocityScalar * dt)
		{
			return vecToFinalPos.Normalized() * (velocityScalar * dt);
		}
		else
		{
			staled = true;
			return vecToFinalPos;
		}
	}

	SceneVector Pose::GetStartEffectorCurrentPos() const
	{
		return GetStartEffectorBody()->GetFirstJointPos();
	}

	SceneVector Pose::GetEndEffectorCurrentPos() const
	{
		return GetEndEffectorBody()->GetSecondJointPos();
	}

	Pose::~Pose() {}

	std::unordered_map<std::string, btTransform> Pose::Solve(GameplayState *gps, Skeleton &skeleton, const double &dt)
	{
		totalTime += dt;
		SceneVector de = GetTargetPosIncrement(dt);

		incrementBefore = de;

		jacobian = Jacobian(effectorNum);
		int effectorNum = 0;

		for (auto *effectorBody : effectorChain)
		{
			auto currentEntry = revolutionAxis.Cross(
				GetEndEffectorCurrentPos() - effectorBody->GetFirstJointPos()
			).Normalized();

			jacobian.AddEntry(effectorNum, currentEntry);

			++effectorNum;
		}

		auto pseudoInversed = jacobian.PseudoInversed();//jacobian.Transposed();//.PseudoInversed(test); pseudoInversed.DebugPrint();

		auto solution = pseudoInversed * de;
		auto *currBoneBody = GetEndEffectorBody();

		for (auto rIter = solution.rbegin(); rIter != solution.rend(); ++rIter)
		{
			// ����������� �������� ��������
			auto currBoneBodyFwd = currBoneBody;

			res[currBoneBody->firstJoint] = btTransform::getIdentity();
			
			if (rIter.base() != solution.end())
			{
				for (auto iter = rIter.base(); iter != solution.end(); ++iter)
				{
					currBoneBodyFwd = &(skeleton.boneBodies.at(currBoneBodyFwd->secondJoint));

					auto linearDiff = *rIter * revolutionAxis.Cross(
						currBoneBodyFwd->body->getWorldTransform().getOrigin() - currBoneBody->body->getWorldTransform().getOrigin()
					);

					res[currBoneBodyFwd->firstJoint].setOrigin(linearDiff);
				}

				res[currBoneBody->firstJoint].setRotation(btQuaternion(revolutionAxis, *rIter));
			}
			else
			{
				res[currBoneBody->firstJoint].setOrigin(de);

				//auto oz = finalEndEffectorTargetPosRelative.Normalized();
				//auto ox = finalEndEffectorTargetPosRelative

				//res[currBoneBody->firstJoint].setRotation(endEffectorFinalPos);
			}
			
			currBoneBody = skeleton.GetBoneBodyParent(*currBoneBody);
		}

		return res;
	}

	// InverseKinematicControllerComponentManager
	void InverseKinematicsControllerComponentManager::AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		entity.assign<InverseKinematicsController>();
	}

	LuaPlus::LuaObject InverseKinematicsControllerComponentManager::ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		return GetComponentTable(entity);
	}

	LuaPlus::LuaObject InverseKinematicsControllerComponentManager::GetComponentTable(entityx::Entity& entity) const
	{
		return LuaPlus::LuaObject();
	}

	void InverseKinematicsControllerComponentManager::AddPose(const LuaPlus::LuaObject &entityTable, const LuaPlus::LuaObject &poseTable)
	{
		auto entity = gps->GetEntity(entityTable);
		auto ikController = entity.component<InverseKinematicsController>();

		auto test = SceneVector::FromMapCoords(LuaPlus::LuaHelper::GetTable(poseTable, "finalEndEffectorPose"));

		Pose pose(
			*(entity.component<Skeleton>().get()),
			LuaPlus::LuaHelper::GetString(poseTable, "startEffector"),
			LuaPlus::LuaHelper::GetString(poseTable, "endEffector"),
			SceneVector::FromMapCoords(LuaPlus::LuaHelper::GetTable(poseTable, "finalEndEffectorPose"))
		);

		// TODO
		ikController->poses.clear();
		ikController->poses.push_back(pose);
	}

	void InverseKinematicsControllerComponentManager::ClearPoses(const LuaPlus::LuaObject &entityTable)
	{
		auto entity = gps->GetEntity(entityTable);
		entity.component<InverseKinematicsController>()->poses.clear();
	}
}