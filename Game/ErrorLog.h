#pragma once

#include <vector>
#include <string>
#include <chrono>

/*
#ifdef _DEBUG
#define CHECK_GL_ERROR ErrorLog::CheckGLError("at " __FUNCTION__);
#else
#define CHECK_GL_ERROR // --
#endif
*/

#ifndef OPENGL_DEBUG
#define CHECK_GL_ERROR ErrorLog::CheckGLError("at " __FUNCTION__);
#else
#define CHECK_GL_ERROR 
#endif

namespace mg
{
	class ErrorLog
	{
		static const std::chrono::duration<double> LAG_PERIOD;

		static std::vector<std::string> log;
		static std::chrono::time_point<std::chrono::high_resolution_clock, std::chrono::duration<double>> prevTime;
	
	public:
		// ��������� ����� ���������
		static void Log(const std::string& entry);
		
		// ���������� ���� ��� ��� ���� ������
		static std::string GetFullLog();

		static void CheckGLError(const std::string& msg = "");

		// ���� � ����
		static void Dump(const std::string& logFileName = "log.txt");

		static void CheckLag(const std::string &msg, const bool reset = false);

		static void APIENTRY DebugOutputCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam);
	};
}