#include "stdafx.h"
#include "SimpleBatch.h"
#include "Paths.h"
#include "OGLConstants.h"
#include "Model.h"
#include "Sizes.h"

namespace mg
{
	void SimpleBatch::Init(LuaPlus::LuaState* luaState)
	{
		// initialize buffers

		vertices->Init(BUFFER_SIZE_32MB, GL_ARRAY_BUFFER);
		indices->Init(BUFFER_SIZE_8MB, GL_ELEMENT_ARRAY_BUFFER);

		// initialize VAO

		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		
		vertices->Bind();
		indices->Bind();

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(SimpleVertex), 0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(SimpleVertex), BUFFER_OFFSET(sizeof(glm::vec3)));
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(SimpleVertex), BUFFER_OFFSET(sizeof(glm::vec3) * 2));

		glBindVertexArray(NULL);
		vertices->Unbind();
		indices->Unbind();

#ifdef _DEBUG
		showAABBs = true;
		showLevelGeometry = true;
		showCoords = true;
#else
		showAABBs = false;
		showLevelGeometry = false;
		showCoords = false;
#endif

		CHECK_GL_ERROR
	}

	void SimpleBatch::Draw()
	{
		//glDepthFunc(GL_ALWAYS);
		glLineWidth(1.f);
		glDrawElements(GL_LINES, indices->GetCurrentOffset(), GL_UNSIGNED_INT, BUFFER_OFFSET(0));
		//glDepthFunc(GL_LEQUAL);
		//glDrawArrays(GL_POINTS, 0, 8);

		CHECK_GL_ERROR
	}

	void SimpleBatch::VisualizeCoords(const btCollisionShape* shape, const btTransform &transform)
	{
		btVector3 eMax, eMin;

		btTransform identity; identity.setIdentity();

		// ������������ vbo � ������ aabb
		vertices->Bind();

		glm::dmat4 modelMat;
		transform.getOpenGLMatrix(glm::value_ptr<double>(modelMat));

		std::vector<SimpleVertex> vec;

		vec.push_back(SimpleVertex(glm::vec3(modelMat * glm::vec4(0.0, 0.0, 0.0, 1.0)),
			glm::vec3(0.f, 1.f, 0.f)));
		vec.push_back(SimpleVertex(glm::vec3(modelMat * glm::vec4(0.0, 1.0, 0.0, 1.0)),
			glm::vec3(0.f, 1.f, 0.f)));

		vec.push_back(SimpleVertex(glm::vec3(modelMat * glm::vec4(0.0, 0.0, 0.0, 1.0)),
			glm::vec3(1.f, 0.f, 0.f)));
		vec.push_back(SimpleVertex(glm::vec3(modelMat * glm::vec4(1.0, 0.0, 0.0, 1.0)),
			glm::vec3(1.f, 0.f, 0.f)));

		vec.push_back(SimpleVertex(glm::vec3(modelMat * glm::vec4(0.0, 0.0, 0.0, 1.0)),
			glm::vec3(0.f, 0.f, 1.f)));
		vec.push_back(SimpleVertex(glm::vec3(modelMat * glm::vec4(0.0, 0.0, 1.0, 1.0)),
			glm::vec3(0.f, 0.f, 1.f), 0));

		vertices->Fill(vec);
		vertices->Unbind();

		unsigned int offset = GetVertices().GetCurrentOffset() - 6;

		std::vector<unsigned int> inds{
			offset, offset + 1, offset + 2, offset + 3, offset + 4, offset + 5
		};

		indices->Bind();
		indices->Fill(inds);
		indices->Unbind();

	}

	void SimpleBatch::VisualizeAABB(const btCollisionShape* shape, const btTransform &transform, const glm::vec3 &color)
	{
		btVector3 eMax, eMin;

		btTransform identity; identity.setIdentity();
		shape->getAabb(identity, eMax, eMin);

		// ������������ vbo � ������ aabb
		vertices->Bind();

		glm::dmat4 modelMat;
		transform.getOpenGLMatrix(glm::value_ptr<double>(modelMat));

		std::vector<SimpleVertex> vec;
		vec.push_back(SimpleVertex(glm::vec3(modelMat * glm::vec4(eMin.x(), eMin.y(), eMin.z(), 1.0)), color, 0));
		vec.push_back(SimpleVertex(glm::vec3(modelMat * glm::vec4(eMin.x(), eMax.y(), eMin.z(), 1.0)), color, 0));
		vec.push_back(SimpleVertex(glm::vec3(modelMat * glm::vec4(eMax.x(), eMax.y(), eMin.z(), 1.0)), color, 0));
		vec.push_back(SimpleVertex(glm::vec3(modelMat * glm::vec4(eMax.x(), eMin.y(), eMin.z(), 1.0)), color, 0));

		vec.push_back(SimpleVertex(glm::vec3(modelMat * glm::vec4(eMin.x(), eMin.y(), eMax.z(), 1.0)), color, 0));
		vec.push_back(SimpleVertex(glm::vec3(modelMat * glm::vec4(eMin.x(), eMax.y(), eMax.z(), 1.0)), color, 0));
		vec.push_back(SimpleVertex(glm::vec3(modelMat * glm::vec4(eMax.x(), eMax.y(), eMax.z(), 1.0)), color, 0));
		vec.push_back(SimpleVertex(glm::vec3(modelMat * glm::vec4(eMax.x(), eMin.y(), eMax.z(), 1.0)), color, 0));

		vertices->Fill(vec);
		vertices->Unbind();

		unsigned int offset = GetVertices().GetCurrentOffset() - 8;

		std::vector<unsigned int> inds{
			offset, offset + 1, offset + 1, offset + 2, offset + 2, offset + 3, offset + 3, offset,
			offset + 4, offset + 5, offset + 5, offset + 6, offset + 6, offset + 7, offset + 7, offset + 4,
			offset, offset + 4, offset + 1, offset + 5, offset + 2, offset + 6, offset + 3, offset + 7
		};

		indices->Bind();
		indices->Fill(inds);
		indices->Unbind();

	}

	void SimpleBatch::VisualizeLevelGeometry()
	{
		unsigned int offset = GetVertices().GetCurrentOffset();

		vertices->Bind();
		vertices->Fill(levelGeometry);
		vertices->Unbind();

		std::vector<unsigned int> inds;

		for (int i = 0; i < levelGeometry.size(); ++i)
		{
			inds.push_back(offset + i);
		}

		indices->Bind();
		indices->Fill(inds);
		indices->Unbind();
	}

	void SimpleBatch::AddTriangleToSceneGeometry(const SceneVector &p0, const SceneVector &p1, const SceneVector &p2)
	{
		SimpleVertex sv0(p0, glm::vec3(0.f, 1.f, 0.f));
		SimpleVertex sv1(p1, glm::vec3(0.f, 1.f, 0.f));
		SimpleVertex sv2(p2, glm::vec3(0.f, 1.f, 0.f));

		levelGeometry.push_back(sv0); levelGeometry.push_back(sv1);
		levelGeometry.push_back(sv1); levelGeometry.push_back(sv2);
		levelGeometry.push_back(sv2); levelGeometry.push_back(sv0);
	}

	// ��������� ������� ��������� ������
	/*void SimpleBatch::DrawBTTrisMesh(const btBvhTriangleMeshShape *shape)
	{
		auto map = shape->getTriangleInfoMap();

		int index = 0;
		auto triangleInfo = map->getAtIndex(index);

		while(triangleInfo)
		{

			auto triangleInfo = map->getAtIndex(++index);
		}
	}*/

	void SimpleBatch::DrawVector(const btVector3 &begin, const btVector3 &dir, const glm::vec3 &color)
	{
		vertices->Bind();

		std::vector<SimpleVertex> vec;
		vec.push_back(SimpleVertex(glm::vec3(glm::vec4(begin.x(), begin.y(), begin.z(), 1.0)), color, 0));
		vec.push_back(SimpleVertex(glm::vec3(glm::vec4(begin.x() + dir.x(), begin.y() + dir.y(), begin.z() + dir.z(), 1.0)), color, 0));

		vertices->Fill(vec);
		vertices->Unbind();

		unsigned int offset = GetVertices().GetCurrentOffset() - 2;

		std::vector<unsigned int> inds{
			offset, offset + 1
		};

		indices->Bind();
		indices->Fill(inds);
		indices->Unbind();
	}
}