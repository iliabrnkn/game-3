#include "stdafx.h"

#include "LightSystem.h"
#include "Sizes.h"
#include "LightPoint.h"
#include "Position.h"
#include "Collideable.h"
#include "Core.h"
#include "Paths.h"
#include "GameplayState.h"
#include "Level.h"
#include "SpotLight.h"
#include "MathHelper.h"

namespace mg {

	LightSystem::LightSystem(GameplayState* gameplayState)
		:gps(gameplayState)
	{}

	void LightSystem::configure(entityx::EventManager &events)
	{
		// dynamic lights
		shadowMXsBuffer.Init((LIGHT_MAX_DYNAMIC_OMNILIGHTS + LIGHT_MAX_STATIC_OMNILIGHTS) * 6 * sizeof(glm::mat4), GL_UNIFORM_BUFFER);
		lightsTransformsBufferTex.InitBuffer();
		shadowMXsBuffer.Bind();
		lightsTransformsBufferTex.BindBuffer(shadowMXsBuffer.GetID(), GL_RGBA32F);

		// light props
		lightProps.Init((LIGHT_MAX_STATIC_OMNILIGHTS + LIGHT_MAX_DYNAMIC_OMNILIGHTS) * sizeof(LightProps), GL_UNIFORM_BUFFER);
	}

	void LightSystem::update(entityx::EntityManager &es, entityx::EventManager &em, entityx::TimeDelta dt)
	{
		std::vector<glm::mat4> shadowTransforms;
		std::vector<glm::mat4> projectionMXs;

		int staticLightsCount = 0, lightsCount = 0;

		std::vector<LightProps> lightProperties;
		lightProperties.resize(LIGHT_MAX_DYNAMIC_OMNILIGHTS + LIGHT_MAX_STATIC_OMNILIGHTS);
		staticShadowTransforms.clear();
		shadowTransforms.clear();

		// omnilights
		es.each<LightPoint, Position>([&dt, &es, this, &shadowTransforms, &staticLightsCount, &lightsCount, &lightProperties, &projectionMXs]
			(entityx::Entity& entity, LightPoint& lightPoint, Position& lightPosition) 
		{
			SceneVector finalLightPosition = lightPosition + lightPoint.positionOffset;

			if (lightPoint.active)
			{
				if (!entity.has_component<SpotLight>())
				{
					auto projMat = glm::perspective<float>(glm::radians(90.f), 1.f, 0.01f, lightPoint.radius);

					// TODO: ��������� ���������� ����, �������� �� ��������� �� �����
					if (gps->GetCamera()->ViewportIntersectsWithCircle(finalLightPosition, lightPoint.radius))
					{
						shadowTransforms.push_back(projMat *
							glm::lookAt((glm::vec3)finalLightPosition, (glm::vec3)(finalLightPosition + glm::vec3(1.f, 0.f, 0.f)), glm::vec3(0.f, -1.f, 0.f)));
						shadowTransforms.push_back(projMat *
							glm::lookAt((glm::vec3)finalLightPosition, (glm::vec3)(finalLightPosition + glm::vec3(-1.f, 0.f, 0.f)), glm::vec3(0.f, -1.f, 0.f)));
						shadowTransforms.push_back(projMat *
							glm::lookAt((glm::vec3)finalLightPosition, (glm::vec3)(finalLightPosition + glm::vec3(0.f, 1.f, 0.f)), glm::vec3(0.f, 0.f, 1.f)));
						shadowTransforms.push_back(projMat *
							glm::lookAt((glm::vec3)finalLightPosition, (glm::vec3)(finalLightPosition + glm::vec3(0.f, -1.f, 0.f)), glm::vec3(0.f, 0.f, -1.f)));
						shadowTransforms.push_back(projMat *
							glm::lookAt((glm::vec3)finalLightPosition, (glm::vec3)(finalLightPosition + glm::vec3(0.f, 0.f, 1.f)), glm::vec3(0.f, -1.f, 0.f)));
						shadowTransforms.push_back(projMat *
							glm::lookAt((glm::vec3)finalLightPosition, (glm::vec3)(finalLightPosition + glm::vec3(0.f, 0.f, -1.f)), glm::vec3(0.f, -1.f, 0.f)));

						auto &currLightProps = lightProperties[lightsCount];

						currLightProps.color = lightPoint.color;
						currLightProps.pos = finalLightPosition;
						currLightProps.radius = lightPoint.radius;

						// ������ �������� ����� - ������������, �� ��������� ���������� �� ������ ������� � �������� ����� ������������
						currLightProps.staticIndex = lightPoint.dynamic ? -1 : staticLightsCount++;
						currLightProps.dynamicIndex = lightsCount++;

						if (!gps->IsBakingPass())
						{
							if (lightPoint.dynamic)
							{
								SceneDrawAdditionalInfo lightAdditionalInfo(true, true);

								//auto sideHalfLen = lightPoint.radius / glm::tan(glm::quarter_pi<float>()) + 0.5f;
								//auto sideHalfLen = SceneVector(lightPoint.radius);
								SceneVector lightPointMinBoundingPoint = finalLightPosition - SceneVector(lightPoint.radius);
								SceneVector lightPointMaxBoundingPoint = finalLightPosition + SceneVector(lightPoint.radius);

								// ����������, ����� �������� ����� ����� �������������� ��� ������� ���������
								for (auto &sceneSegment : gps->GetScene().GetSceneSegments())
								{

									if (
										MathHelper::IntersectsBoxes(
											sceneSegment.second.minBoundingPoint,
											sceneSegment.second.maxBoundingPoint,
											lightPointMinBoundingPoint,
											lightPointMaxBoundingPoint
										) ||
										MathHelper::IntersectsBoxes(
											lightPointMinBoundingPoint,
											lightPointMaxBoundingPoint,
											sceneSegment.second.minBoundingPoint,
											sceneSegment.second.maxBoundingPoint
										)
									)
									{
										lightAdditionalInfo.sceneSegmentsToDraw.push_back(sceneSegment.first);
									}
								}

								gps->GetScene().RegisterLightInQueue(currLightProps.dynamicIndex, lightAdditionalInfo);
							}
							else
							{
								gps->GetScene().RegisterLightInQueue(currLightProps.dynamicIndex, SceneDrawAdditionalInfo(false, true));
							}
						}
						else
						{
							if (!lightPoint.dynamic)
							{
								gps->GetScene().RegisterLightInQueue(currLightProps.dynamicIndex, SceneDrawAdditionalInfo(true, false));
							}
						}
					}
				}
			}
		});

		lightProps.Bind();
		lightProps.Clear();
		lightProps.Fill(lightProperties);
		lightProps.Unbind();

		Core::GetInstance()->GetRenderer()->SetLightCount(lightsCount);
		
		shadowMXsBuffer.Bind();
		shadowMXsBuffer.Clear();
		shadowMXsBuffer.Fill(shadowTransforms);
		shadowMXsBuffer.Unbind();

		// ��������� ��� ��������� ������������ �����

		gps->GetScene().GetLightEffect().SetUniformBlockDeferred<LightProps>("lightsBlock", 0, lightProps);
		gps->GetScene().GetLightEffect().SetUniform1iDeferred("dynamicShadowMXs", lightsTransformsBufferTex.GetTextureUnit());
		
		char postfix = 'A';
		for (auto &atlas : gps->GetLevel()->GetSpriteAtlases())
		{
			string varname = "spriteAtlas";
			varname.push_back(postfix++);
			gps->GetScene().GetLightEffect().SetUniform1iDeferred(varname,
				atlas.GetTexture()->GetTextureUnit());
		}
		
		Core::GetInstance()->GetRenderer()->GetDeferredRenderingEffect().SetUniform1iDeferred("omniLightsCount", lightsCount);
		Core::GetInstance()->GetRenderer()->GetDeferredRenderingEffect().SetUniformBlockDeferred<LightProps>("omniLightBlock", 0, lightProps);
	}
}