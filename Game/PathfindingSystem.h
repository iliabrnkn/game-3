#pragma once

#include <entityx\entityx.h>
#include <glm\glm.hpp>

#include <unordered_map>
#include <vector>

#include <lua/LuaPlus.h>

namespace mg {

	class PathfindingSystem : public entityx::System<PathfindingSystem>
	{
		friend class GameplayState;

		const float LAST_STEP_LEN = 0.3f;

		GameplayState* gps;

	public:
		PathfindingSystem(GameplayState* gps);

		void configure(entityx::EventManager &events) override;
		void update(entityx::EntityManager &es, entityx::EventManager &em, entityx::TimeDelta dt) override;
	};
}