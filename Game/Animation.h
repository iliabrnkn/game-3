#pragma once

#include <unordered_map>
#include <functional>
#include <vector>
#include <memory>
#include <deque>

#include "ModelStruct.h"
#include "Cloneable.h"
#include "ComponentManager.h"

using namespace std;

#define ROOT_JOINT "Armature"

namespace mg {

	struct Skeleton;

	const float ANIMATION_FADE_SPEED = 5.f;

	struct Animation : public Cloneable
	{
		double durationInTicks;
		double ticksPerSec;

		unordered_map<string, AnimChannel> channels;
		vector<double> keyTimes;

		Animation() = default;

		bool GetCurrentKey(const string& channelName, AnimKey& key, int activeKeyNum, float interpolant);
	};

	// ����� ��� ����������� � ��������� - ���������� � �������������, ���������� ������� � ��

	struct AnimationHandler
	{
		std::shared_ptr<Animation> animation;

		bool looped;
		bool staled;
		double elapsedTicks;
		float interpolant;
		float intensity;
		float speed;
		int activeKeyNum;

		std::set<std::string> allowedBoneNodes;
		std::string animName;

		AnimationHandler(std::shared_ptr<Animation> animation, bool looped, const std::set<string> &allowedBoneNodesInitial = std::set<string>{ ROOT_JOINT }) :
			staled(false), elapsedTicks(0.0), looped(looped),
			animation(animation), interpolant(.0f), activeKeyNum(0), intensity(1.0f), speed(1.0f), 
			allowedBoneNodes(allowedBoneNodesInitial)
		{
		}

		AnimationHandler(const AnimationHandler&) = default;

		AnimationHandler(const AnimationHandler& prototype, const bool& looped, const std::set<string> &allowedBoneNodesInitial, 
			const float& speedCoefficient = 1.0, const double &startElapsedTicks = 0.0, const std::string &animName = "") :
			staled(false), looped(looped),
			animation(prototype.animation), interpolant(.0f), activeKeyNum(0), intensity(1.0f), speed(speedCoefficient),
			allowedBoneNodes(allowedBoneNodesInitial), elapsedTicks(startElapsedTicks), animName(animName)
		{
		}

		AnimationHandler() = default;
		~AnimationHandler() = default;

		void AnimationHandler::CountElapsed(const double& deltaTime);

		bool GetCurrentKey(const string& channelName, AnimKey& key);

		inline void Reset(const double &animPos = 0.0) 
		{ 
			elapsedTicks = animation->durationInTicks * animPos; 
		}

	};

	struct AnimationBatch
	{
		static std::unordered_map<string, AnimationHandler> animationHandlerPrototypes;
		std::string currentAnimationName;
		std::string idleAnimationName;
		bool animationChanging;

		std::deque<AnimationHandler> activeAnimations;

		void PushAnimation(AnimationHandler &animHandler, const bool &resetAnimationBatch);

		AnimationBatch()
		{
		}
	};

	class AnimationBatchComponentManager : public ComponentManager
	{
	public:
		AnimationBatchComponentManager(GameplayState *gps) : ComponentManager("animations", gps) {};
		~AnimationBatchComponentManager() = default;

		static std::vector<std::string> names;

		virtual void AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const override;
		virtual LuaPlus::LuaObject GetComponentTable(entityx::Entity& entity) const override;

		void PlayAnimation(LuaPlus::LuaObject &entity, LuaPlus::LuaObject &animationProps);
		
		// ���� ����� ����������� ���������, �� ���������� ��������, ��������������� false, ����� ������������ �� true
		void EnableAnimationChanging(LuaPlus::LuaObject& table, const bool &enable = true);
	};
}