#pragma once

#include <string>

#include "entityx\Entity.h"

namespace mg {
	struct TransformHitboxesToRagdoll
	{
		entityx::Entity entity;

		TransformHitboxesToRagdoll(entityx::Entity entity) :
			entity(entity)
		{}
	};
}