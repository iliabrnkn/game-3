#include "stdafx.h"

#include <fstream>
#include <vector>
#include <string>
#include <unordered_map>

#include "ModelStruct.h"
#include "Animation.h"
#include "ModelManagers.h"
#include "RenderingSystem3D.h"
#include "Paths.h"
#include "OGLConstants.h"
#include "ModelStruct.h"
#include "StringHelper.h"
#include "GameplayState.h"
#include "RenderingSystem3D.h"
#include "Skeleton.h"
#include "Batch3D.h"
#include "SimpleBatch.h"

using namespace std;

namespace mg
{
	std::unordered_map<std::string, size_t> queuePoses;
	std::unordered_map<std::string, glm::mat4> offsets;
	//-----------MeshFactory------------

	void MeshFactory::LoadFromFile(const string& filename)
	{
		ifstream file = ifstream(filename.c_str(), ios::in | ios::binary);

		if (!(bool)file)
		{
			ErrorLog::Log("Couldn't open mesh file " + filename);
			return;
		}

		unsigned int meshCount = 0;
		file.read((char*)(&meshCount), sizeof(unsigned int)); // mesh count, unsigned int

		shared_ptr<Cloneable> res = shared_ptr<Cloneable>(new Mesh());
		auto *resRawPtr = (Mesh*)res.get();
		MeshBuffers currMeshBuffers;

		//----------- reading vertices ----------------

		for (unsigned int meshI = 0; meshI < meshCount; meshI++)
		{
			unsigned int uniqueBonesCount = 0;
			unsigned int numVertices = 0;
			file.read((char*)(&numVertices), sizeof(unsigned int));

			for (unsigned int vertNum = 0; vertNum < numVertices; vertNum++)
			{
				AnimVertex animVertex;

				// vertex coords
				file.read((char*)(&animVertex.vert.x), sizeof(float));
				file.read((char*)(&animVertex.vert.y), sizeof(float));
				file.read((char*)(&animVertex.vert.z), sizeof(float));

				// normal
				file.read((char*)(&animVertex.normal.x), sizeof(float));
				file.read((char*)(&animVertex.normal.y), sizeof(float));
				file.read((char*)(&animVertex.normal.z), sizeof(float));

				// texture coords

				file.read((char*)(&animVertex.texCoord.x), sizeof(float));
				file.read((char*)(&animVertex.texCoord.y), sizeof(float));

				animVertex.texCoord.y *= -1.f;

				currMeshBuffers.vertices.push_back(animVertex);
			}

			// indices - triangles

			unsigned int numIndices;

			file.read((char*)(&numIndices), sizeof(unsigned int));

			for (unsigned int i = 0; i < numIndices; i++)
			{
				unsigned int index;
				file.read((char*)(&index), sizeof(unsigned int));
				currMeshBuffers.indices.push_back(index);
			}
		}

		// ------------- reading bones --------------

		unsigned int numBones;
		file.read((char*)(&numBones), sizeof(unsigned int));
		resRawPtr->boneValues.resize(MAX_BONES_PER_MODEL);

		for (unsigned int i = 0; i < numBones; i++)
		{
			Bone bone;
			bone.name = ReadStringFromFile(file);// bone name

			unsigned int numWeights = 0;
			file.read((char*)(&numWeights), sizeof(unsigned int)); // weights count

			for (unsigned int j = 0; j < numWeights; j++)
			{
				unsigned int vertexId = 0;
				file.read((char*)(&vertexId), sizeof(unsigned int));

				float currWeightVal = 0;
				file.read((char*)(&currWeightVal), sizeof(float));

				for (int k = 0; k < 4; k++)
				{
					if (currMeshBuffers.vertices[vertexId].weightVal[k] == 0.0)
					{
						currMeshBuffers.vertices[vertexId].boneId[k] = i;
						currMeshBuffers.vertices[vertexId].weightVal[k] = currWeightVal;

						break;
					}
				}
			}

			// offset matrix

			file.read((char*)(&bone.offsetMatrix[0][0]), sizeof(float));
			file.read((char*)(&bone.offsetMatrix[1][0]), sizeof(float));
			file.read((char*)(&bone.offsetMatrix[2][0]), sizeof(float));
			file.read((char*)(&bone.offsetMatrix[3][0]), sizeof(float));

			file.read((char*)(&bone.offsetMatrix[0][1]), sizeof(float));
			file.read((char*)(&bone.offsetMatrix[1][1]), sizeof(float));
			file.read((char*)(&bone.offsetMatrix[2][1]), sizeof(float));
			file.read((char*)(&bone.offsetMatrix[3][1]), sizeof(float));

			file.read((char*)(&bone.offsetMatrix[0][2]), sizeof(float));
			file.read((char*)(&bone.offsetMatrix[1][2]), sizeof(float));
			file.read((char*)(&bone.offsetMatrix[2][2]), sizeof(float));
			file.read((char*)(&bone.offsetMatrix[3][2]), sizeof(float));

			file.read((char*)(&bone.offsetMatrix[0][3]), sizeof(float));
			file.read((char*)(&bone.offsetMatrix[1][3]), sizeof(float));
			file.read((char*)(&bone.offsetMatrix[2][3]), sizeof(float));
			file.read((char*)(&bone.offsetMatrix[3][3]), sizeof(float));

			bone.queuePos = i;

			queuePoses.insert(std::make_pair(bone.name, i));
			offsets[bone.name] = bone.offsetMatrix;

			resRawPtr->bones.insert(make_pair(bone.name, bone));
		}

		auto filenameParts = StringHelper::GetFileNameParts(filename);

		resRawPtr->name = filenameParts[filenameParts.size() - 2];;

		elements.insert(make_pair(resRawPtr->name, res));
		meshBuffers.insert(make_pair(resRawPtr->name, currMeshBuffers));

		file.close();
	}

	std::shared_ptr<Cloneable> MeshFactory::CreateElement(const std::string& componentName, GameplayState *gps, const int &id)
	{
		CHECK_GL_ERROR
		auto prototypeIter = elements.find(componentName);

		if (prototypeIter == elements.end())
			return std::shared_ptr<Cloneable>(nullptr);

		auto res = std::shared_ptr<Cloneable>(prototypeIter->second);
		auto resRawPtr = (Mesh*)res.get();

		resRawPtr->name = componentName;
		resRawPtr->ID = id != - 1 ? id : gps->GetScene().CreateNewModelID();

		vector<AnimVertex> animVertices = meshBuffers[componentName].vertices;

		for (auto& vert : animVertices)
		{	
			vert.objectID = resRawPtr->ID;

			//TODO: ��������� ���������������

			//vert.texCoord.z = 0;
		}

		vector<unsigned int> indices = meshBuffers[componentName].indices;
		
		auto indsOffsetInitial = gps->GetScene().GetCharactersBatch().GetIndices().GetCurrentOffset();
		auto vertsOffsetInitial = gps->GetScene().GetCharactersBatch().GetVertices().GetCurrentOffset();

		for (auto& index : indices)
		{
			index += gps->GetScene().GetCharactersBatch().GetVertices().GetCurrentOffset();
		}

		resRawPtr->vertOffset = gps->GetScene().GetCharactersBatch().GetVertices().GetCurrentOffset();
		resRawPtr->vertsCount = animVertices.size();
		resRawPtr->indsOffset = gps->GetScene().GetCharactersBatch().GetIndices().GetCurrentOffset();
		resRawPtr->indsCount = indices.size();
		resRawPtr->removed = false;

		gps->GetScene().GetCharactersBatch().GetVertices().Bind();
		gps->GetScene().GetCharactersBatch().GetVertices().Fill(animVertices);
		gps->GetScene().GetCharactersBatch().GetVertices().Unbind();

		gps->GetScene().GetCharactersBatch().GetIndices().Bind();
		gps->GetScene().GetCharactersBatch().GetIndices().Fill(indices);
		gps->GetScene().GetCharactersBatch().GetIndices().Unbind();

		return res;
	}

	//-----------SkeletonFactory------------
	
	void SkeletonFactory::LoadFromFile(const string& filename)
	{
		ifstream file = ifstream(filename.c_str(), ios::in | ios::binary);

		if (!(bool)file)
		{
			ErrorLog::Log("Couldn't open skeleton file" + filename);
			return;
		}

		auto filenameParts = StringHelper::GetFileNameParts(filename);

		shared_ptr<Cloneable> res = shared_ptr<Cloneable>(new Skeleton());
		auto *resRawPtr = (Skeleton*)res.get();

		unsigned int nodesCount = 0;
		vector<string> nodeNames;
		
		file.read((char*)(&nodesCount), sizeof(unsigned int));
		resRawPtr->name = filenameParts[filenameParts.size() - 2];

		for (unsigned int i = 0; i < nodesCount; i++)
		{
			auto nodeName = ReadStringFromFile(file);
			nodeNames.push_back(nodeName);
		}

		for (unsigned int i = 0; i < nodeNames.size(); i++)
		{
			BoneNode node;
			string currNodeName = ReadStringFromFile(file);
			node.name = currNodeName;

			// transformation matrix
			
			file.read((char*)(&node.basicTransform[0][0]), sizeof(float));
			file.read((char*)(&node.basicTransform[1][0]), sizeof(float));
			file.read((char*)(&node.basicTransform[2][0]), sizeof(float));
			file.read((char*)(&node.basicTransform[3][0]), sizeof(float));

			file.read((char*)(&node.basicTransform[0][1]), sizeof(float));
			file.read((char*)(&node.basicTransform[1][1]), sizeof(float));
			file.read((char*)(&node.basicTransform[2][1]), sizeof(float));
			file.read((char*)(&node.basicTransform[3][1]), sizeof(float));

			file.read((char*)(&node.basicTransform[0][2]), sizeof(float));
			file.read((char*)(&node.basicTransform[1][2]), sizeof(float));
			file.read((char*)(&node.basicTransform[2][2]), sizeof(float));
			file.read((char*)(&node.basicTransform[3][2]), sizeof(float));

			file.read((char*)(&node.basicTransform[0][3]), sizeof(float));
			file.read((char*)(&node.basicTransform[1][3]), sizeof(float));
			file.read((char*)(&node.basicTransform[2][3]), sizeof(float));
			file.read((char*)(&node.basicTransform[3][3]), sizeof(float));

			// parent name
			node.parentName = ReadStringFromFile(file);

			// ancestors
			unsigned int numChildren;
			file.read((char*)(&numChildren), sizeof(unsigned int));

			for (unsigned int i = 0; i < numChildren; i++)
			{
				node.childNames.push_back(ReadStringFromFile(file));
			}

			node.additionalAngle = 0.f;

			resRawPtr->boneNodes.insert(make_pair(currNodeName, node));
		}

		//root node inverse transform

		file.read((char*)(&resRawPtr->rootNodeInverseTransform[0][0]), sizeof(float));
		file.read((char*)(&resRawPtr->rootNodeInverseTransform[1][0]), sizeof(float));
		file.read((char*)(&resRawPtr->rootNodeInverseTransform[2][0]), sizeof(float));
		file.read((char*)(&resRawPtr->rootNodeInverseTransform[3][0]), sizeof(float));

		file.read((char*)(&resRawPtr->rootNodeInverseTransform[0][1]), sizeof(float));
		file.read((char*)(&resRawPtr->rootNodeInverseTransform[1][1]), sizeof(float));
		file.read((char*)(&resRawPtr->rootNodeInverseTransform[2][1]), sizeof(float));
		file.read((char*)(&resRawPtr->rootNodeInverseTransform[3][1]), sizeof(float));

		file.read((char*)(&resRawPtr->rootNodeInverseTransform[0][2]), sizeof(float));
		file.read((char*)(&resRawPtr->rootNodeInverseTransform[1][2]), sizeof(float));
		file.read((char*)(&resRawPtr->rootNodeInverseTransform[2][2]), sizeof(float));
		file.read((char*)(&resRawPtr->rootNodeInverseTransform[3][2]), sizeof(float));

		file.read((char*)(&resRawPtr->rootNodeInverseTransform[0][3]), sizeof(float));
		file.read((char*)(&resRawPtr->rootNodeInverseTransform[1][3]), sizeof(float));
		file.read((char*)(&resRawPtr->rootNodeInverseTransform[2][3]), sizeof(float));
		file.read((char*)(&resRawPtr->rootNodeInverseTransform[3][3]), sizeof(float));

		file.close();

		elements.insert(make_pair(resRawPtr->name, res));
	}

	// ��������

	std::shared_ptr<Cloneable> SkeletonFactory::CreateElement(const std::string& componentName, GameplayState *gps, const int &id)
	{
		auto componentFoundIter = elements.find(componentName);

		//reinterpret_cast<Skeleton*>(componentFoundIter->second.get())->locomotorActivity = 1.f;

		if (componentFoundIter != elements.end())
			return componentFoundIter->second;
		else
			throw std::invalid_argument("Invalid component name.");
	}

	//-----------AnimationFactory------------

	void AnimationFactory::LoadFromFile(const string& filename)
	{
		ifstream file = ifstream(filename.c_str(), ios::in | ios::binary);

		if (!(bool)file)
		{
			ErrorLog::Log("Couldn't open animation file " + filename);
			return;
		}

		auto filenameParts = StringHelper::GetFileNameParts(filename);

		shared_ptr<Cloneable> res = shared_ptr<Cloneable>(new Animation());
		auto *resRawPtr = (Animation*)(res.get());

		resRawPtr->name = filenameParts[filenameParts.size() - 2];

		// animation duration in ticks
		
		file.read((char*)(&resRawPtr->durationInTicks), sizeof(double));
		file.read((char*)(&resRawPtr->ticksPerSec), sizeof(double));

		//channels number
		unsigned int channelNum = 0;
		file.read((char*)&channelNum, sizeof(unsigned int));

		// keys num
		unsigned int keyNum;
		file.read((char*)&keyNum, sizeof(unsigned int));

		for (unsigned int i = 0; i < channelNum; i++)
		{
			AnimChannel channel;

			// channel node name
			auto currChannelName = ReadStringFromFile(file);

			for (unsigned int j = 0; j < keyNum; j++)
			{
				AnimKey currKey;

				// key time
				file.read((char*)&currKey.time, sizeof(double));

				if (resRawPtr->keyTimes.size() >= j + 1)
					assert(resRawPtr->keyTimes[j] == currKey.time);
					//continue;
				else
					resRawPtr->keyTimes.push_back(currKey.time);

				// position key

				file.read((char*)&currKey.pos.x, sizeof(float));
				file.read((char*)&currKey.pos.y, sizeof(float));
				file.read((char*)&currKey.pos.z, sizeof(float));

				// rotation key

				file.read((char*)&currKey.rot.x, sizeof(float));
				file.read((char*)&currKey.rot.y, sizeof(float));
				file.read((char*)&currKey.rot.z, sizeof(float));
				file.read((char*)&currKey.rot.w, sizeof(float));

				// scaling key

				file.read((char*)&currKey.scale.x, sizeof(float));
				file.read((char*)&currKey.scale.y, sizeof(float));
				file.read((char*)&currKey.scale.z, sizeof(float));

				channel.keys.push_back(currKey);
			}

			resRawPtr->channels.insert(make_pair(currChannelName, channel));
		}

		file.close();

		elements.insert(make_pair(resRawPtr->name, res));
	}

	std::shared_ptr<Cloneable> AnimationFactory::CreateElement(const string& componentName, GameplayState *gps, const int &id)
	{
		auto componentFoundIter = elements.find(componentName);

		if (componentFoundIter != elements.end())
			return componentFoundIter->second;
		else
			throw std::invalid_argument("Invalid component name.");
	}
}