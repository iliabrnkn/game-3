#pragma once

#include <vector>

#include "Vector.h"
#include "ComponentManager.h"

namespace mg {

	struct Direction;
	struct AIPiece;

	struct Moveable
	{
		Moveable(const float &maxLinearVelocity, const float &maxAngularVelocity) :
			maxLinearVelocity(maxLinearVelocity), maxAngularVelocity(maxAngularVelocity),
			targetBodyOrientation(0.f, 0.f, 1.f), currentBodyOrientation(targetBodyOrientation)
			{}

		float maxLinearVelocity;
		float maxAngularVelocity;
		bool walkForward;

		SceneVector targetLinearVelocity;
		SceneVector targetBodyOrientation;
		SceneVector currentBodyOrientation;

		// возвращает currentBodyOrientation
		void CalculateOrientation(const Direction &dir, AIPiece &aiPiece, const double &dt);
	};

	class MoveableComponentManager : public ComponentManager
	{
	public:
		MoveableComponentManager(GameplayState *gps) : ComponentManager("moveable", gps) {};
		~MoveableComponentManager() = default;

		virtual void AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const override;
		virtual LuaPlus::LuaObject GetComponentTable(entityx::Entity& entity) const override;
		virtual LuaPlus::LuaObject ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const override;

		// простая установка скорости
		void Move(LuaPlus::LuaObject &entityTable, const LuaPlus::LuaObject &dir) const;
		void Stop(LuaPlus::LuaObject &entityTable) const;
		void OrientBodyTo(LuaPlus::LuaObject &entityTable, LuaPlus::LuaObject &mapPointTable) const;
		LuaPlus::LuaObject GetTargetVelocity(const LuaPlus::LuaObject &entityTable) const;
		void SetMaxLinVelocity(LuaPlus::LuaObject &entityTable, const float &maxLinVelocityLen) const;
	};
}