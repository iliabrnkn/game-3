#pragma once

#include <glm/glm.hpp>

namespace mg {

	struct Orientation
	{
		Orientation(const float &x, const float &y, const float &z) :
			vec(x, y, z) {}

		~Orientation() {};

		glm::vec3 vec;
		glm::vec3 vecBefore;
		bool aiming; // ����� true, ������� ����� ������ �������� ������� � ������ ����� 0
	};

	// Orientation component manager

	class OrientationComponentManager : public ComponentManager
	{
	public:
		OrientationComponentManager(GameplayState *gps) : ComponentManager("orientation", gps) {};
		~OrientationComponentManager() = default;

		inline virtual void AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const override
		{
			entity.assign<Orientation>(
				(table.GetByName("x").GetRef() != -1 && table.GetByName("x").IsNumber()) ? table.GetByName("x").ToNumber() : 1.f,
				(table.GetByName("y").GetRef() != -1 && table.GetByName("y").IsNumber()) ? table.GetByName("y").ToNumber() : 0.f,
				(table.GetByName("z").GetRef() != -1 && table.GetByName("z").IsNumber()) ? table.GetByName("z").ToNumber() : 1.1f);
		}

		inline virtual LuaPlus::LuaObject GetComponentTable(entityx::Entity& entity) const override
		{
			LuaPlus::LuaObject res;

			if (entity.has_component<Orientation>())
			{
				auto componentHandler = entity.component<Orientation>();

				res.AssignNewTable(gps->GetLuaState());

				res.SetNumber("x", componentHandler->vec.x);
				res.SetNumber("y", componentHandler->vec.y);
				res.SetNumber("z", componentHandler->vec.z);

				res.SetBoolean("aiming", componentHandler->aiming);
			}
			else
			{
				res.AssignNil();
			}

			return res;
		};

		virtual LuaPlus::LuaObject ModifyComponent(entityx::Entity& entity, LuaPlus::LuaObject& table, GameplayState* gps) const 
		{
			if (!entity.has_component<Orientation>())
				return LuaPlus::LuaObject().AssignNil();
			
			entity.component<Orientation>()->vec.x = table.GetByName("x").GetRef() != -1 ? table.GetByName("x").ToNumber() : entity.component<Orientation>()->vec.x;
			entity.component<Orientation>()->vec.y = table.GetByName("y").GetRef() != -1 ? table.GetByName("y").ToNumber() : entity.component<Orientation>()->vec.y;
			entity.component<Orientation>()->vec.z = table.GetByName("z").GetRef() != -1 ? table.GetByName("z").ToNumber() : entity.component<Orientation>()->vec.z;
			entity.component<Orientation>()->aiming = table.GetByName("aiming").GetRef() != -1 ? table.GetByName("aiming").GetBoolean() : entity.component<Orientation>()->aiming;

			return GetComponentTable(entity);
		};
	};

}