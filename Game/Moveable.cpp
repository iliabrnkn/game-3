#include "stdafx.h"

#include "LuaHelper.h"

#include "Moveable.h"
#include "GameplayState.h"
#include "AutoWalkController.h"
#include "InverseKinematicsController.h"
#include "Position.h"
#include "Direction.h"
#include "AIPiece.h"

namespace mg
{
	void Moveable::CalculateOrientation(const Direction &dir, AIPiece &aiPiece, const double &dt)
	{
		if (targetLinearVelocity.Length() > 0.f) // ���� �������� ����
		{
			// ���� ������� ����� �������� ���������� ��������� � �������� ����������� ������ ������ PI/2, ������������� ���� �� ����������� ������, 
			// ����� - � �������� �������, �.�. ���������� �������� ������ �����
			auto walkDirAngle = dir.Angle(targetLinearVelocity.Normalized(), SceneVector(0.f, 1.f, 0.f));
			bool isWalkForwardCurr = glm::abs(walkDirAngle) <= glm::half_pi<float>();

			// ���� ����������� ������ ����������
			if (walkForward != isWalkForwardCurr)
			{
				walkForward = isWalkForwardCurr;
				aiPiece.Emit("onStartMove");
			}

			targetBodyOrientation = walkForward ? targetLinearVelocity.Normalized() : -targetLinearVelocity.Normalized();
		}
		else // ���� �������� �����
		{
			auto stayDirAngle = glm::abs(dir.Angle(targetBodyOrientation.Normalized()));

			if (stayDirAngle > glm::half_pi<float>())
				targetBodyOrientation = dir.Normalized();
		}

		// ������������� ����
		if (glm::abs(currentBodyOrientation.Angle(targetBodyOrientation)) > FLT_EPSILON)
		{
			currentBodyOrientation = glm::slerp(
				static_cast<glm::vec3>(currentBodyOrientation),
				static_cast<glm::vec3>(targetBodyOrientation),
				static_cast<float>(dt * maxAngularVelocity)
			);
			currentBodyOrientation.Normalize();
		}
	}

	// -- MoveableComponentManager

	void MoveableComponentManager::AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		entity.assign<Moveable>(
			LuaPlus::LuaHelper::GetFloat(table, "maxLinearVelocity"), 
			LuaPlus::LuaHelper::GetFloat(table, "maxAngularVelocity")
		);
	}

	LuaPlus::LuaObject MoveableComponentManager::GetComponentTable(entityx::Entity& entity) const
	{
		LuaPlus::LuaObject res;

		if (entity.has_component<Moveable>())
		{
			auto componentHandler = entity.component<Moveable>();

			res.AssignNewTable(gps->GetLuaState());
			
			res.SetObject("targetLinearVelocity", componentHandler->targetLinearVelocity.ToMapCoords(gps->GetLuaState()));
			res.SetObject("targetBodyOrientation", componentHandler->targetBodyOrientation.ToMapCoords(gps->GetLuaState()));
			res.SetObject("targetBodyOrientation", componentHandler->currentBodyOrientation.ToMapCoords(gps->GetLuaState()));
			res.SetNumber("maxLinearVelocity", componentHandler->maxLinearVelocity);
			res.SetNumber("maxAngularVelocity", componentHandler->maxAngularVelocity);
			res.SetBoolean("walkForward", componentHandler->walkForward);
		}
		else
		{
			res.AssignNil();
		}

		return res;
	}

	LuaPlus::LuaObject MoveableComponentManager::ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		if (!entity.has_component<Moveable>())
			return LuaPlus::LuaObject().AssignNil();

		auto component = entity.component<Moveable>();

		component->currentBodyOrientation = SceneVector::FromMapCoords(LuaHelper::GetTable(table, "currentBodyOrientation", false), 
			false, component->currentBodyOrientation);
		component->targetBodyOrientation = SceneVector::FromMapCoords(LuaHelper::GetTable(table, "targetBodyOrientation", false),
			false, component->targetBodyOrientation);
		component->targetLinearVelocity = SceneVector::FromMapCoords(LuaHelper::GetTable(table, "targetLinearVelocity", false),
			false, component->targetLinearVelocity);
		component->maxLinearVelocity = LuaHelper::GetFloat(table, "maxLinearVelocity", false, component->maxLinearVelocity);
		component->maxAngularVelocity = LuaHelper::GetFloat(table, "maxAngularVelocity", false, component->maxAngularVelocity);

		return GetComponentTable(entity);
	};

	void MoveableComponentManager::Move(LuaPlus::LuaObject &entityTable, const LuaPlus::LuaObject &dirTable) const
	{
		auto entity = gps->GetEntity(entityTable);

		if (entity.has_component<Moveable>())
		{
			auto component = entity.component<Moveable>();

			// ����������� ��������
			SceneVector dir = SceneVector::FromMapCoords(dirTable).Normalize();
			auto newTargetLinVelocity = component->maxLinearVelocity * dir;

			if (entity.has_component<AutoWalkController>() && !entity.component<AutoWalkController>()->isActive
				&& entity.has_component<InverseKinematicsController>())
			{
				entity.component<InverseKinematicsController>()->poses.clear();
			}

			component->targetLinearVelocity = newTargetLinVelocity;
		}
	}

	void MoveableComponentManager::Stop(LuaPlus::LuaObject &entityTable) const
	{
		auto entity = gps->GetEntity(entityTable);

		if (entity.has_component<Moveable>())
		{
			auto component = entity.component<Moveable>();
			
			component->targetLinearVelocity = glm::vec3(0.f);
		}
	}

	void MoveableComponentManager::OrientBodyTo(LuaPlus::LuaObject &entityTable, LuaPlus::LuaObject &mapPointTable) const
	{
		auto entity = gps->GetEntity(entityTable);
		entity.component<Moveable>()->targetBodyOrientation = (SceneVector::FromMapCoords(mapPointTable) - *entity.component<Position>()).Normalized();
	}

	LuaPlus::LuaObject MoveableComponentManager::GetTargetVelocity(const LuaPlus::LuaObject &entityTable) const
	{
		return gps->GetEntity(entityTable).component<Moveable>()->targetLinearVelocity.ToMapCoords(gps->GetLuaState());
	}

	void MoveableComponentManager::SetMaxLinVelocity(LuaPlus::LuaObject &entityTable, const float &maxLinVelocityLen) const
	{
		gps->GetEntity(entityTable).component<Moveable>()->maxLinearVelocity = maxLinVelocityLen;
	}
}