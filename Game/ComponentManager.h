#pragma once

#include <string>

#include <lua\LuaPlus.h>
#include <entityx/entityx.h>

namespace mg
{
	class GameplayState;

	class ComponentManager
	{
		std::string componentTableName;

	protected:
		GameplayState *gps;

	public:
		ComponentManager(const std::string& componentTableNameInitial, GameplayState *gps) : 
			componentTableName(componentTableNameInitial), gps(gps) {}

		virtual ~ComponentManager() = 0 {}

		// ��������� ������ ����� ���������
		virtual void AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const {};

		// �������� ��������� � ������, ���������� ���������� ������� ����������, ���� nil, ���� ��������� ������
		virtual LuaPlus::LuaObject ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const {
			// �������� ��� ��� �������, ������� ��������� ������������, �������������� ������� ������ ��� ��� ������
			return GetComponentTable(entity);
		};

		// ���������� ������� ����������, ���� nil, ���� ��������� ����������� � ������
		virtual LuaPlus::LuaObject GetComponentTable(entityx::Entity &entity) const = 0;

		inline virtual void RemoveComponent(entityx::Entity &entity) const
		{
		};

		inline std::string GetComponentTableName()
		{
			return componentTableName;
		}
	};
}