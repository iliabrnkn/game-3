#pragma once

#include "Widget.h"

namespace mg
{
	const int CORNER_OFFSET = 2;
	const sf::Color CORNERS_COLOR = sf::Color(24, 67, 91);
	const sf::Color BACKGROUND_COLOR = sf::Color(23, 23, 23);

	class RegularBox : public Widget
	{
	private:

		void DrawCorner(const sf::Vector2i& startPoint, const int& dx, const int& dy, const int& lineWidth, sf::Image& img, const sf::Color& color)
		{
			for (int i = 0; i < lineWidth; ++i)
			{
				img.setPixel(static_cast<unsigned int>(startPoint.x + dx * i), static_cast<unsigned int>(startPoint.y), color);
				img.setPixel(static_cast<unsigned int>(startPoint.y), static_cast<unsigned int>(startPoint.y + dy * i), color);
			}
		}

	public:
		RegularBox(const int& width, const int& height, const std::string& name, const std::string& parentName = "") :
			Widget(name, parentName)
		{
			sf::Image background;
			background.create(width, height);

			// ������ ������

			DrawCorner(sf::Vector2i(CORNER_OFFSET, CORNER_OFFSET), 1, 1, 5, background, CORNERS_COLOR);
			DrawCorner(sf::Vector2i(CORNER_OFFSET, height - CORNER_OFFSET), 1, -1, 5, background, CORNERS_COLOR);
			DrawCorner(sf::Vector2i(width - CORNER_OFFSET, height - CORNER_OFFSET), -1, -1, 5, background, CORNERS_COLOR);
			DrawCorner(sf::Vector2i(width - CORNER_OFFSET, CORNER_OFFSET), -1, 1, 5, background, CORNERS_COLOR);

			LoadImg(background, sf::IntRect(0, 0, width, height));
		}

		~RegularBox() {}
	};
}