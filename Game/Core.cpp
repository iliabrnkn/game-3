﻿#include "stdafx.h"

#include <vector>
#include <thread>
#include <glm/gtc/type_ptr.hpp>

#include "Core.h"
#include "States.h"
#include "WindowConstants.h"
#include "Paths.h"
#include "GUI.h"
#include "InventoryWindow.h"
#include "ItemGrid.h"
#include "ErrorLog.h"

#include "LightSystem.h"
#include "TextBox.h"
#include "Button.h"
#include "ErrorLog.h"
#include "ParticleEmitterBatch.h"

using namespace std;

namespace mg {

	sf::VideoMode Core::videoMode;
	int Core::windowStyle;

	Core::Core()
		:window(sf::VideoMode(videoMode.width, videoMode.height, 32), "SFML OpenGL", windowStyle,
			sf::ContextSettings(24, 0, ANTIALIASING_LEVEL, OGL_MAJOR_VERSION, OGL_MINOR_VERSION, 
#ifdef OPENGL_DEBUG
				sf::ContextSettings::Attribute::Debug
#else
				sf::ContextSettings::Attribute::Default
#endif
			)),
		lastFrameDelta(.0),
		camera(make_shared<Camera>()),
		renderer(make_unique<Renderer>())
	{
	}

	Core* Core::GetInstance()
	{
		static Core core;

		return &core;
	}

	void Core::SetVideoMode(const sf::VideoMode& videoMode, const int& windowStyle)
	{
		Core::videoMode = videoMode;
		Core::windowStyle = windowStyle;
	}

	Core::~Core()
	{
		//luaState->Destroy(luaState);
	}

	sf::VideoMode Core::GetVideoMode()
	{
		return videoMode;
	}

	void Core::Init()
	{
		glm::mat4 transformMatTest = glm::transpose(glm::scale(glm::vec3(0.007)) * glm::rotate(glm::pi<float>() / 10.f, glm::vec3(0.f, 1.f, 0.f)) * glm::rotate(glm::pi<float>() / 70.f, glm::vec3(0.f, 0.f, 1.f)) * glm::rotate(-glm::pi<float>() / 20.f, glm::vec3(1.f, 0.f, 0.f)));
		int here = 0;

		if (glewInit() != GLEW_OK)
			ErrorLog::Log("Can't initialize glew");

		assert(GLEW_ARB_texture_cube_map_array);

		glDepthMask(GL_TRUE);
		glClearDepth(1.f);
		glClearColor(0.0f, 0.0f, 0.0f, 0.f);
		glClearStencil(0);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_ALPHA_TEST);
		glEnable(GL_BLEND);
		glEnable(GL_MULTISAMPLE);
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_TEXTURE_2D_ARRAY);
		glEnable(GL_TEXTURE_CUBE_MAP);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_FRAMEBUFFER_SRGB);
		glEnable(GL_POINT_SPRITE);
		glTexEnvi(GL_POINT_SPRITE, GL_COORD_REPLACE, GL_TRUE);
		glEnable(GL_DEBUG_OUTPUT);
		glEnable(GL_AMD_debug_output);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);

		glDebugMessageCallback(ErrorLog::DebugOutputCallback, nullptr);

		luaState = LuaPlus::LuaState::Create(true);
		
		camera->Init();

		window.setKeyRepeatEnabled(true);
		window.setView(sf::View(sf::FloatRect(0, 0, RENDERBUFFER_WIDTH, RENDERBUFFER_HEIGHT))); // ???

		// --- GUI Initialize
		GUI::GetInstance().Init(window, luaState);

		states[GAMEPLAY_STATE] = std::unique_ptr<ApplicationState>(new GameplayState(luaState, camera));

		for (auto state = states.begin(); state != states.end(); state++)
			state->second->Init();

		currentState = states[GAMEPLAY_STATE].get();

		window.setActive();
		renderer->camera = camera;
		renderer->Init(luaState);

		window.setActive();
		controls.Init(luaState);

		bool loadedTestSound = testBuffer.loadFromFile(SOUND_FOLDER + "pistol_shot.wav");

		sound.setBuffer(testBuffer);
	}

	void Core::TestSound()
	{
		sound.play();
	}

	void Core::Loop()
	{
		clock.restart();
		sf::Event event;

		window.setActive();
		window.setMouseCursorVisible(false);

		while (currentState != nullptr && window.isOpen())
		{
			bool closeWindow = false;
			auto delta = clock.restart().asSeconds();
			lastFrameDelta += delta;

			if (lastFrameDelta >= FRAME_DURATION)
			{
				while (window.pollEvent(event))
				{
					if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
					{
						closeWindow = true;
					}

					if (GUI::GetInstance().IsConsoleOn())
					{
						if (event.type == sf::Event::TextEntered && event.text.unicode < 128)
							GUI::GetInstance().ConsolePush(static_cast<char>(event.text.unicode));

						if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Key::Return)
							GUI::GetInstance().ConsoleFlush();
					}
				}

				if (!closeWindow)
				{
					// чистим дебажный VBO
					reinterpret_cast<GameplayState*>(currentState)->GetScene().GetSimpleBatch().GetVertices().Bind();
					reinterpret_cast<GameplayState*>(currentState)->GetScene().GetSimpleBatch().GetVertices().Clear();
					reinterpret_cast<GameplayState*>(currentState)->GetScene().GetSimpleBatch().GetVertices().Unbind();

					reinterpret_cast<GameplayState*>(currentState)->GetScene().GetSimpleBatch().GetIndices().Bind();
					reinterpret_cast<GameplayState*>(currentState)->GetScene().GetSimpleBatch().GetIndices().Clear();
					reinterpret_cast<GameplayState*>(currentState)->GetScene().GetSimpleBatch().GetIndices().Unbind();

					// TODO: убрать то что вверху

					controls.HandleKeys(luaState, lastFrameDelta);
					controls.HandleMouse(&window, luaState, camera.get(), lastFrameDelta);

					//------ обновляем сцену
					currentState->Update(lastFrameDelta, &window);

					//------ рендерим сцену в FBO
					if (static_cast<GameplayState*>(currentState)->isBakingPass)
					{
						renderer->BakeStaticOmniLights(static_cast<GameplayState*>(currentState)->GetScene());

						// первый проход используется для запекания всех статических данных
						static_cast<GameplayState*>(currentState)->isBakingPass = false;
					}
					else
					{
						renderer->RenderOmniLights(static_cast<GameplayState*>(currentState)->GetScene());
					}
					
					renderer->RenderSpotLights(static_cast<GameplayState*>(currentState)->GetScene());
					renderer->RenderDirectionalLight(static_cast<GameplayState*>(currentState)->GetScene());
					renderer->RenderScene(static_cast<GameplayState*>(currentState)->GetScene(), controls);
					renderer->RenderClouds(static_cast<GameplayState*>(currentState)->GetScene());
					renderer->RenderBloom(static_cast<GameplayState*>(currentState)->GetScene());

					//------ отложенный рендеринг
					currentState->PrepareDeferredRendering(renderer->deferredRenderingEffect);
					renderer->DeferredRendering();
					renderer->RenderToScreen();
					//------ вывод на экран

					glActiveTexture(GL_TEXTURE0); // для того, чтоб sfml не бузил

					GUI::GetInstance().ClearRemovedWidgets();
					GUI::GetInstance().Draw(window);
					window.display();

					CHECK_GL_ERROR
				}

				lastFrameDelta = 0.0;
			}

			if (closeWindow)
			{
				((GameplayState*)currentState)->Free();
				window.close();
				ErrorLog::Dump();
			}
		}
	}

	sf::RenderWindow& Core::GetWindow()
	{
		return window;
	}

	Renderer* Core::GetRenderer()
	{
		return renderer.get();
	}

	LuaPlus::LuaState* Core::GetLuaState()
	{
		return luaState;
	}
}