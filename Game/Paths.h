#pragma once

#include <string>
#include <direct.h>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

namespace mg
{
	const string DATA_FOLDER = string(_getcwd(NULL, 0)) + "\\Data\\";
	const string MODELS_FOLDER = DATA_FOLDER + "Models\\";
	const string ANIMATIONS_FOLDER = DATA_FOLDER + "Animations\\";
	const string IMAGES_FOLDER = DATA_FOLDER + "Images\\";
	const string TILESETS_FOLDER = IMAGES_FOLDER + "Tilesets\\";
	const string TILESETS_GEOMETRY_FOLDER = MODELS_FOLDER + "TileGeometry\\";
	const string EFFECTS_FOLDER = DATA_FOLDER + "Effects\\";
	const string FONTS_FOLDER = DATA_FOLDER + "Fonts\\";
	const string MAPS_FOLDER = DATA_FOLDER + "Maps\\";
	const string TEXTURES_FOLDER = IMAGES_FOLDER + "Textures\\";
	const string SCRIPTS_FOLDER = DATA_FOLDER + "Scripts\\";
	const string GUI_FOLDER = IMAGES_FOLDER + "GUI\\";
	const string SOUND_FOLDER = DATA_FOLDER + "Sound\\";
}