#include "stdafx.h"

#include "glm\glm.hpp"
#include <lua\LuaPlus.h>

#include "Widget.h"
#include "GUI.h"
#include "Controls.h"

namespace mg
{
	Widget::Widget(const WidgetParameters& params, const std::string& name, const int &zOrderInitial) :
		childrenNames(10),
		name(name),
		width(params.width), height(params.height), origin(params.origin),
		visibility(params.visible),
		displayability(params.displayable),
		zOrder(zOrderInitial),
		removed(false),
		dragged(false)
	{
		id = GUI::GetNextID();
		background.create(width, height, GUI_COLOR_TRANSPARENT);

		startParams = params;
	}

	Widget::~Widget()
	{
		for (auto& childName : childrenNames)
		{
			GUI::GetInstance().RemoveWidget(childName);
		}
	}

	void Widget::SetBackgroundImg(const std::string& backgroundImagePath, const sf::IntRect& rect)
	{
		background.loadFromFile(backgroundImagePath);

		SetBackgroundImg(background, rect);
	}

	void Widget::SetBackgroundImg(const sf::Image& img, const sf::IntRect& rect)
	{
		texture.loadFromImage(img);
		sprite.setTexture(texture);
		sprite.setPosition(sf::Vector2f(static_cast<float>(width - img.getSize().x) / 2.f, 
			static_cast<float>(height - img.getSize().y) / 2.f) + static_cast<sf::Vector2f>(origin));
	}

	sf::Sprite& Widget::GetSprite()
	{
		return sprite;
	}

	void Widget::SetOrigin(const sf::Vector2f& originInitial)
	{
		origin.x = originInitial.x;
		origin.y = originInitial.y;
	}

	void Widget::SetSize(const int& width, const int& height)
	{
		this->width = width;
		this->height = height;
	}

	void Widget::DrawCorner(const sf::Vector2i& startPoint, const int& dx, const int& dy, const int& lineWidth, sf::Image& img, const sf::Color& color)
	{
		for (int i = 0; i < lineWidth; ++i)
		{
			img.setPixel(static_cast<unsigned int>(startPoint.x + dx * i), static_cast<unsigned int>(startPoint.y), color);
			img.setPixel(static_cast<unsigned int>(startPoint.x), static_cast<unsigned int>(startPoint.y + dy * i), color);
		}
	}

	void Widget::Init()
	{

	}

	std::string textContent;

	void Widget::DrawBox(const sf::Color& bordersColor, const sf::Color& cornersColor, const int& cornerSideLength, int boxWidth, int boxHeight, const sf::Vector2i& origin)
	{
		boxWidth = boxWidth == -1 ? width : boxWidth;
		boxHeight = boxHeight == -1 ? height : boxHeight;

		// ������ ������

		DrawCorner(origin + sf::Vector2i(CORNER_OFFSET, CORNER_OFFSET), 1, 1, cornerSideLength, background, cornersColor);
		DrawCorner(origin + sf::Vector2i(CORNER_OFFSET, boxHeight - CORNER_OFFSET - 1), 1, -1, cornerSideLength, background, cornersColor);
		DrawCorner(origin + sf::Vector2i(boxWidth - CORNER_OFFSET - 1, boxHeight - CORNER_OFFSET - 1), -1, -1, cornerSideLength, background, cornersColor);
		DrawCorner(origin + sf::Vector2i(boxWidth - CORNER_OFFSET - 1, CORNER_OFFSET), -1, 1, cornerSideLength, background, cornersColor);

		// ������ �������

		for (int x = 0; x < boxWidth; ++x)
		{
			background.setPixel(origin.x + x, origin.y, bordersColor);
			background.setPixel(origin.x + x, origin.y + boxHeight - 1, bordersColor);
		}

		for (int y = 0; y < boxHeight; ++y)
		{
			background.setPixel(origin.x, origin.y + y, bordersColor);
			background.setPixel(origin.x + boxWidth - 1, origin.y + y, bordersColor);
		}
	}

	void Widget::ClearRemoved()
	{
		for (auto childIter = children.begin(); childIter != children.end();)
		{
			if ((*childIter)->IsRemoved())
			{
				childIter = this->RemoveChild((*childIter)->GetID());
			}
			else
			{
				(*childIter)->ClearRemoved();
				childIter++;
			}
		}
	}

	// ���� ���� ��� ���������, ���������� true, ����� false

	bool Widget::HandleMouse(const sf::Vector2i& absMouseCoords) // �������� ������ �����������
	{
		auto absPos = GetAbsPos();

		if (visibility)
		{
			if (absMouseCoords.x >= absPos.x && absMouseCoords.x <= absPos.x + width &&
				absMouseCoords.y >= absPos.y && absMouseCoords.y <= absPos.y + height)
			{
				if (!mouseHover)
				{
					mouseHover = true;
					OnMouseEnter();
				}

				OnMouseOver(absMouseCoords - static_cast<sf::Vector2i>(origin));

				bool buttons = sf::Mouse::isButtonPressed(sf::Mouse::Left) || sf::Mouse::isButtonPressed(sf::Mouse::Right);

				if (buttons != 0)
				{
					if (!(Controls::LastTimePressed(sf::Mouse::Button::Left)
						|| Controls::LastTimePressed(sf::Mouse::Button::Right)
						|| Controls::LastTimePressed(sf::Mouse::Button::Middle)))
					{
						OnClick(absMouseCoords - static_cast<sf::Vector2i>(origin));
						printf("WIDGET CLICKED\r\n");

						if (!dragged)
						{
							printf("WIDGET DRAGGED\r\n");
							dragged = true;
						}
					}
				}
				else
				{
					if (dragged)
					{
						OnDrop(absMouseCoords);
					}

					dragged = false;
				}

			}
			else
			{
				if (mouseHover)
				{
					mouseHover = false;
					OnMouseLeave();
				}
			}

			if (dragged) // ���� �������������
			{
				OnDrag(absMouseCoords);
			}
		}

		if (displayability)
		{
			/*for (auto child = children.begin(); child != children.end(); ++child)
			{
				(*child)->HandleMouse(absCoords);
			}*/
			for (auto i = 0; i < children.size(); ++i)
			{
				children[i]->HandleMouse(absMouseCoords);
			}
		}

		return mouseHover || dragged;
	}

	void Widget::Highlight()
	{
		auto size = background.getSize();

		for (int x = 0; x < size.x; ++x)
		{
			for (int y = 0; y < size.y; ++y)
			{
				auto pixColor = background.getPixel(x, y);

				if (pixColor == GUI_COLOR_DIM)
					background.setPixel(x, y, GUI_COLOR_MEDIUM);
				else if (pixColor == GUI_COLOR_MEDIUM)
					background.setPixel(x, y, GUI_COLOR_BRIGHT);
				else if (pixColor == GUI_COLOR_BRIGHT)
					background.setPixel(x, y, GUI_COLOR_BRIGHTEST);
			}
		}

		SetBackgroundImg(background, sf::IntRect(0, 0, width, height));
	}

	void Widget::Fade()
	{
		auto size = background.getSize();

		for (int x = 0; x < size.x; ++x)
		{
			for (int y = 0; y < size.y; ++y)
			{
				auto pixColor = background.getPixel(x, y);

				if (pixColor == GUI_COLOR_MEDIUM)
					background.setPixel(x, y, GUI_COLOR_DIM);
				else if (pixColor == GUI_COLOR_BRIGHT)
					background.setPixel(x, y, GUI_COLOR_MEDIUM);
				else if (pixColor == GUI_COLOR_BRIGHTEST)
					background.setPixel(x, y, GUI_COLOR_BRIGHT);
			}
		}

		SetBackgroundImg(background, sf::IntRect(0, 0, width, height));
	}

	void Widget::Draw(sf::RenderWindow& window, sf::Shader &shader, sf::Vector2i &offset)
	{
		std::string name = "WeaponCell0";
		if (name == this->GetName())
		{
			GUI::GetInstance().Watch("pistol - x", offset.x);
			GUI::GetInstance().Watch("pistol - y", offset.y);
		}

		if (doReSort)
		{
			std::sort(children.begin(), children.end(), [](auto& a, auto& b) { return a->zOrder < b->zOrder; });
			doReSort = false;
		}

		if (displayability)
		{
			if (visibility)
			{
				float width = 0.f;
				float height = 0.f;
				
				if (GetSprite().getTexture())
				{
					height = static_cast<float>(GetSprite().getTexture()->getSize().y);
					width = static_cast<float>(GetSprite().getTexture()->getSize().x);

					sprite.setPosition(static_cast<sf::Vector2f>(offset));
				}

				shader.setUniform("textureHeight", height);
				shader.setUniform("textureWidth", width);

				window.draw(sprite, &shader);
			}

			for (auto iter = children.begin(); iter != children.end(); iter++)
			{
				(*iter)->Draw(window, shader, offset + (*iter)->origin);
			}
		}
	}

	std::string Widget::GetName() const
	{
		return name;
	}

	// ���������� true, ����� ������ ������ � ������
	std::vector<std::unique_ptr<Widget>>::iterator Widget::RemoveChild(const int &widgetID)
	{
		auto iter = std::find_if(children.begin(), children.end(), [&widgetID](auto& x) { return x->id == widgetID; });
		bool res = iter != children.end();

		if (iter != children.end())
		{
			for (auto childIter = (*iter)->children.begin(); childIter != (*iter)->children.end();)
			{
				childIter = iter->get()->RemoveChild((*childIter)->id);
			}

			auto removeIter = std::remove_if(children.begin(), children.end(), [&widgetID](auto& x) { return x->id == widgetID; });

			return children.erase(removeIter, children.end());
		}
		else
			return children.end();
		//else // ���� �� ����� ��������� ������ ����� �����������, ���������� ����� � ������ ����������
		//{
		//	iter = children.begin();

		//	while (!res && iter != children.end())
		//	{
		//		res = (*iter)->RemoveChild((*iter)->id);
		//		iter++;
		//	}
		//}
	}

	sf::Vector2i Widget::GetOrigin() const
	{
		return origin;
	}

	int Widget::GetMaxZOrder() const
	{
		if (children.size() == 0)
			return -1;

		auto maxEl = std::max_element(children.begin(), children.end(), [](auto& a, auto& b) { return a->zOrder < b->zOrder; });

		return (*maxEl)->zOrder;
	}

	void Widget::SetOnTop()
	{
		SetZOrder(parent->GetMaxZOrder() + 1);
		parent->DoReSort();
	}

	void Widget::SetZOrder(const int& z)
	{
		zOrder = z;
	}

	void Widget::DoReSort()
	{
		doReSort = true;
	}

	void Widget::SetVisibility(const bool& visible)
	{
		this->visibility = visibility;
	}

	bool Widget::IsVisible() const
	{
		return visibility;
	}

	void Widget::SetDisplayability(const bool& display)
	{
		displayability = display;
	}

	bool Widget::IsDisplayable() const
	{
		return displayability;
	}

	sf::Vector2i Widget::GetSize()  const
	{
		return sf::Vector2i(width, height);
	}

	sf::Vector2i Widget::GetAbsPos()
	{
		auto parent = GetParent<Widget>();
		auto res = origin;
		
		if (parent != nullptr && this->name != std::string("root")) // ������ ������� ��-�� ��������� ��������� � release mode
		{
			res += parent->GetAbsPos();
		}

		return res;
	}

	void Widget::SetEventHandler(const std::string &eventName, const LuaPlus::LuaObject &eventHandler)
	{
		assert(eventHandler.IsFunction());
		eventHandlers.insert(std::make_pair(eventName, eventHandler));
	}

	void Widget::FireEvent(const std::string &eventHandlerName, LuaPlus::LuaObject &arg)
	{
		auto iter = eventHandlers.find(eventHandlerName);
		assert(iter != eventHandlers.end());

		if (iter != eventHandlers.end())
		{
			static_cast<LuaPlus::LuaFunctionVoid>(iter->second)(arg);
		}
	}
}