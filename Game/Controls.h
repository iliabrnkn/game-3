#pragma once

#include <unordered_map>
#include <string>

#include <SFML\Graphics.hpp>
#include <glm/glm.hpp>
#include <lua\LuaPlus.h>

#include "Vector.h"
#include "Camera.h"

namespace mg
{
	const double TIME_FROM_LAST_CLICK = 1.0 / 30.0;
	const double CONTROLS_UPDATE_PERIOD = 0.1;

	class Controls
	{
	private:
		int selectionColor;
		glm::vec2 lastMousePosScreen;
		float timeFromLastClick = 0.f;
		float timeFromLastKeyPressed = 0.f;
		std::unordered_map<int, std::string> keyNames;
		std::vector<std::string> pressedKeys, repeatKeys;
		// ���� �������� ���� ���������� - true, �� ������, ��� ������� ���� ������ ������ ���
		bool originalPass;
		bool mouseKeyPressed;

		LuaPlus::LuaFunctionVoid onMouseClick;
		LuaPlus::LuaFunctionVoid onMouseMove;
		LuaPlus::LuaFunctionVoid onKeyPressed;
		static LuaPlus::LuaObject lastMouseArg;

		SceneVector lastMousePos;

		glm::vec2 GetMouseScreenCoords(const sf::Window *window);
		SceneVector GetMouseSceneCoords(const sf::Window *window, const Camera *camera);

		// ������� ���������� ��������� ������� ������ ����, ����� ��� ����������� ���� � ����� ��������� � ���������
		
		static bool leftButtonPressedLastTime;
		static bool rightButtonPressedLastTime;
		static bool middleButtonPressedLastTime;

	public:

		Controls();
		~Controls();

		void HandleKeys(LuaPlus::LuaState* luaState, const double& elapsed/*, const sf::Event& event*/);
		void HandleMouse(sf::Window* window, LuaPlus::LuaState* luaState, const Camera* camera, const double& elapsed);
		void PickColor();
		void Init(LuaPlus::LuaState* luaState);

		inline SceneVector GetCurrentMouseSceneCoords() const
		{
			return lastMousePos;
		}

		inline glm::vec2 GetCurrentMouseScreenCoords() const
		{
			return lastMousePosScreen;
		}

		inline static LuaPlus::LuaObject GetMousePos()
		{
			return lastMouseArg;
		}

		// ���������� true, ���� �� ���������� ������� ������ ���� ��� ������
		static bool LastTimePressed(const sf::Mouse::Button& button);
	};

}