#pragma once

#include <vector>

#include "RenderBatch.h"

namespace mg
{
	/* �����, �������������� ����� ���� ��������������� 3�-������*/

	class GeometryBatch : public ConcreteRenderBatch<AnimVertex>
	{
		friend class RenderingSystem3D;
		friend class MeshFactory;
		
		VBO<glm::vec4> floatUniformBuffer;

	public:
		virtual void Init(LuaPlus::LuaState* luaState) override;
		virtual void Draw() override;
		void Draw(const GLint &startElement, const GLint &elementsCount);
		void DrawInstanced(const int& instanceCount);
		void DrawInstanced(const int& instanceCount, const GLint &startIndex, const GLint &elementsCount);
	};
}