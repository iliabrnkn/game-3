#pragma once

#include "GUI.h"

namespace mg
{
	class TextBox : public Widget
	{
	private:

		std::string textContent;

	public:
		TextBox(const WidgetParameters& params, const std::string& name) :
			Widget(params, name),
			textContent(params.text)
		{
			DrawBox(GUI_COLOR_MEDIUM, GUI_COLOR_DIM);
			SetBackgroundImg(background, sf::IntRect(0, 0, width, height));
		}

		virtual void Draw(sf::RenderWindow &renderWindow, sf::Shader &shader)
		{
			sf::Text text;
			text.setFont(GUI::GetInstance().GetFont());
			text.setString(textContent);
			text.setCharacterSize(10);
			text.setFillColor(GUI_COLOR_MEDIUM);
			text.setPosition(static_cast<sf::Vector2f>(origin + CONTENT_OFFSET));

			Widget::Draw(renderWindow, shader);

			shader.setUniform("textureHeight", static_cast<float>(text.getLocalBounds().height));
			shader.setUniform("textureWidth", static_cast<float>(text.getLocalBounds().width));
			renderWindow.draw(text, &shader);
		}

		~TextBox(){}

	};
}