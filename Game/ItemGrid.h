#pragma once

#include "Widget.h"
#include "InventoryCell.h"

namespace mg {

	class InventoryItem;

	class ItemGrid : public Widget
	{
	protected:
		int cellWidth, cellHeight;

	public:
		ItemGrid(const WidgetParameters &params, const std::string &name, const int &cellWidth, const int &cellHeight)
			: Widget(params, name),
			cellWidth(cellWidth),
			cellHeight(cellHeight)
		{ }

		virtual ~ItemGrid() = 0 {}

		virtual bool ReplaceItem(InventoryItem *item, const sf::Vector2i& pos) = 0;
		virtual void FreePlace(InventoryItem *item) = 0;
		//virtual ItemCell *GetCell(const sf::Vector2i& screenCoords) = 0;
	};

	class ActiveItemsWindow : public ItemGrid
	{
		friend class InventoryWindow;

		std::vector<ActiveItemCell*> activeItemCells;

	public:
		ActiveItemsWindow(const WidgetParameters& params, const std::string& name);
		/*void SetEventHandler(const LuaPlus::LuaObject &eventHandler, const std::string &itemCellName);*/

		virtual bool ReplaceItem(InventoryItem *item, const sf::Vector2i &itemScreenPos);
		virtual void FreePlace(InventoryItem *item);
		virtual ~ActiveItemsWindow() {}
	};

	class InventoryGrid : public ItemGrid
	{
		friend class InventoryWindow;

		std::vector<std::vector<ItemCell*>> cells;
	public:
		InventoryGrid(const mg::WidgetParameters& params, const std::string& name)
			: ItemGrid(params, name, INVENTORY_CELL_SIDE_LENGTH, INVENTORY_CELL_SIDE_LENGTH)
		{
			auto horCellCount = width / INVENTORY_CELL_SIDE_LENGTH;
			auto vertCellCount = height / INVENTORY_CELL_SIDE_LENGTH;

			sf::Vector2i offset;

			cells.resize(vertCellCount);

			for (int i = 0; i < vertCellCount; ++i)
			{
				cells[i] = std::vector<ItemCell*>(horCellCount);
				for (int j = 0; j < horCellCount; ++j)
				{
					auto name = std::to_string(j) + std::to_string(i) + "cell";

					// ��������� ������ ���������
					cells[i][j] = AddChild<ItemCell>(
						WidgetParameters(INVENTORY_CELL_SIDE_LENGTH, INVENTORY_CELL_SIDE_LENGTH,
						origin + sf::Vector2i(j * INVENTORY_CELL_SIDE_LENGTH - j, i * INVENTORY_CELL_SIDE_LENGTH - i)), name);

					offset.x -= j;
				}

				offset.y -= i;
			}

			DrawBox(GUI_COLOR_BRIGHT, GUI_COLOR_DIM, INVENTORY_CORNER_SIDE_LENGTH, width - horCellCount + 1, height - vertCellCount + 1);

			SetBackgroundImg(background, sf::IntRect(0, 0, width, height));
		}

		// �������� �������� ����� � ����, ���� �� ����������, ���������� false

		bool AddItem(const LuaPlus::LuaObject& itemTable)
		{
			int itemWidth = itemTable["inventoryItem"]["width"].ToNumber();
			int itemHeight = itemTable["inventoryItem"]["height"].ToNumber();

			sf::Vector2i startPoint(0,0); // �����, � ������� � ����� ������ �������
			bool placeFound = false;

			// ��������� ����� �� ������� ������������ ���������� �����

			for (int y = 0; y < height && !placeFound; ++y)
			{
				for (int x = 0; x < width && !placeFound; ++x)
				{
					startPoint.x = x;
					startPoint.y = y;
					placeFound = CheckPlace(startPoint, itemWidth, itemHeight);
				}
			}

			if (!placeFound)
				return false;

			// ���� ����� ���� �������, ��������� ���� �������� �������

			auto itemWidget = cells[startPoint.x][startPoint.y]->AddChild<InventoryItem>(
				WidgetParameters(INVENTORY_CELL_SIDE_LENGTH * itemWidth, INVENTORY_CELL_SIDE_LENGTH * itemHeight, origin + startPoint * INVENTORY_CELL_SIDE_LENGTH,
				std::string(itemTable["inventoryItem"]["imgPath"].ToString()), itemTable ),
				itemTable["inventoryItem"]["name"].ToString());

			for (int y = startPoint.y; y < startPoint.y + itemHeight; ++y)
			{
				for (int x = startPoint.x; x < startPoint.x + itemWidth; ++x)
				{
					cells[y][x]->free = false;
				}
			}

			return true;
		}

		virtual void FreePlace(InventoryItem *item);

		// ��������, ������ �� ������� � ��������� �����

		bool CheckPlace(const sf::Vector2i& startPoint, const int& itemWidth, const int& itemHeight);

		bool ReplaceItem(InventoryItem* item, const sf::Vector2i& coords);

		virtual ~InventoryGrid()
		{}
		
		//virtual ItemCell *GetCell(const sf::Vector2i& screenCoords);
		sf::Vector2i GetGridCoords(const sf::Vector2i& screenCoords);
	};
}