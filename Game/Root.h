#pragma once

#include "Widget.h"

namespace mg
{
	class Root : public Widget
	{
	public:
		Root(const WidgetParameters& params, const std::string& name)
			: Widget(params, name)
		{}

		~Root() {};

		virtual bool HandleMouse(const sf::Vector2i& absCoords) override
		{
			auto res = false;

			for (auto child = children.begin(); child != children.end(); ++child)
			{
				auto currRes = (*child)->HandleMouse(absCoords);
				res = !res ? currRes : res;
			}

			return res;
		}
	};
}