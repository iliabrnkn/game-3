#pragma once

#include "ComponentManager.h"

namespace mg {

	struct Selectable
	{

		long int colorID;

		long int Selectable::currColorID = 0;

		Selectable()
		{
			colorID = ++currColorID;
		}

		~Selectable() = default;
	};

	// Selectable ComponentManager

	class SelectableComponentManager : public ComponentManager
	{
	public:
		SelectableComponentManager(GameplayState *gps) : ComponentManager("selectable", gps) {};
		~SelectableComponentManager() = default;

		virtual void AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const override;
		virtual LuaPlus::LuaObject GetComponentTable(entityx::Entity& entity) const override;
	};

}