#pragma once

#include <glm\glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/vector_angle.hpp>

struct SpriteVertex
{
	glm::vec3 sceneVert;
	glm::vec3 screenVert;
	glm::vec2 screenTexCoord;
	glm::vec2 volumeTexCoord;
	glm::vec2 normalTexCoord;
	int objectID;
	float layerOffset;
};

#define MAX_SPRITE_INDICES 67108864 // 64 mb
#define MAX_SPRITE_BUFFER MAX_SPRITE_INDICES * sizeof(SpriteVertex);

#define REGULAR_TILE_OBJECT_ID -1
#define BILLBOARD_VERTEX_OBJECT_ID -2
#define FENCE_VERTEX_OBJECT_ID -3
#define FENCE_BACKWARDS_VERTEX_OBJECT_ID -4

struct AnimVertex
{
	AnimVertex() : boneId(-1.0, -1.0, -1.0, -1.0), weightVal(0.0), objectID(-1) {}

	AnimVertex(const float& x, const float& y, const float& z) :
		vert(x, y, z), boneId(-1.0, -1.0, -1.0, -1.0), weightVal(0.0) {}

	AnimVertex(const glm::vec3 &vert, const int &objectID = -1) :
		vert(vert), boneId(-1.0, -1.0, -1.0, -1.0), weightVal(0.0), objectID(objectID) {}

	AnimVertex(const glm::vec3 &vert, const glm::vec2 &texCoord) :
		vert(vert), boneId(-1.0, -1.0, -1.0, -1.0), weightVal(0.0), objectID(0), normal(glm::vec3(0.f)), texCoord(texCoord) {}

	AnimVertex(const glm::vec3 &vert, const glm::vec3 &offset, const glm::vec2 &texCoord, const int &objectID = REGULAR_TILE_OBJECT_ID) :
		vert(vert), boneId(-1.0, -1.0, -1.0, -1.0), weightVal(0.0), objectID(objectID), normal(offset), texCoord(texCoord) {}

	glm::vec3 vert;
	glm::vec3 normal;
	glm::vec2 texCoord;
	glm::vec4 boneId;
	glm::vec4 weightVal;

	int objectID; // -1 - ��������� ������, ��� ��������� ����� - ������
};

struct SimpleVertex
{
	SimpleVertex() {};

	SimpleVertex(const glm::vec3& vert, const glm::vec3 &normal, const int &boneID) :
		vert(vert), normal(normal), boneID(boneID){};

	SimpleVertex(const glm::vec3& vert, const glm::vec3 &color) :
		vert(vert), normal(color), boneID(-1) {};

	SimpleVertex(const glm::vec3& vert, const glm::vec3 normal, const glm::vec2 &screenPos, const int &boneID = -1) : 
		vert(vert), normal(normal), boneID(boneID), screenPos(screenPos) {};

	glm::vec3 vert;
	glm::vec3 normal;
	float boneID;
	glm::vec2 screenPos;
};

#define MAX_ANIM_INDICES 67108864 // 64 mb
#define MAX_ANIM_BUFFER MAX_ANIM_INDICES * sizeof(AnimVertex);

struct FBOVertex
{
	glm::vec3 vert;
	glm::vec2 texCoord;
};

#define FIRE_PARTICLE 0
#define FOG_PARTICLE 1
#define GUNSHOT_PARTICLE 2

#define FOG_PARTICLE_TYPES 4

struct Particle
{
	glm::vec3 position;
	float lifeTime;
	glm::vec2 startTexCoord; // (-1.f, -1.f) - ��� �������
	glm::vec3 startVelocity;
	glm::vec3 endVelocity;
	int emitterNum;

	Particle(const glm::vec2 &startTexCoordInitial, const int &emitterNumInitial):
		position(-100.f),
		lifeTime(0.f),
		startTexCoord(startTexCoordInitial),
		startVelocity(0.f), endVelocity(0.f),
		emitterNum(emitterNumInitial)
	{}

	Particle(const int &emitterNumInitial) :
		position(-100.f),
		lifeTime(0.f),
		startTexCoord(-1.f),
		startVelocity(0.f), endVelocity(0.f),
		emitterNum(emitterNumInitial)
	{}

	Particle():
		lifeTime(0.f),
		startTexCoord(-1.f),
		startVelocity(0.f), endVelocity(0.f)
	{}

	~Particle()
	{}
};

// ��� ����� (����� �� ���� � �������, ��������, ������)

struct LineVertex
{
	glm::vec4 point;
	glm::vec4 color;

	LineVertex(const glm::vec3 &pos, const glm::vec4 &color)
		: point(pos, 1.f), color(color)
	{}

	~LineVertex() {};
};