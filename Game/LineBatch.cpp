#include "stdafx.h"

#include "LineBatch.h"

namespace mg
{
	LineBatch::LineBatch() :
		ConcreteRenderBatch<LineVertex>()
	{}

	void LineBatch::Init(LuaPlus::LuaState* luaState)
	{
		// ���

		// initialize buffers

		indices->Init(BUFFER_SIZE_128KB, GL_ELEMENT_ARRAY_BUFFER);
		vertices->Init(BUFFER_SIZE_128KB, GL_ARRAY_BUFFER);

		// initialize VAO

		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		vertices->Bind();
		indices->Bind();

		glEnableVertexAttribArray(0); // point
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(LineVertex), 0);
		glEnableVertexAttribArray(1); // color
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(LineVertex), BUFFER_OFFSET(16));

		glBindVertexArray(NULL);
		vertices->Unbind();
		indices->Unbind();

		CHECK_GL_ERROR
	}

	void LineBatch::Draw()
	{
		glLineWidth(2.f);
		glDrawArrays(GL_LINES, 0, vertices->GetCurrentOffset());

		CHECK_GL_ERROR
	}
}