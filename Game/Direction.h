#pragma once

#include <string>
#include <unordered_map>

#include <glm/gtx/quaternion.hpp>
#include <bullet/btBulletDynamicsCommon.h>

#include "Vector.h"
#include "ComponentManager.h"

namespace mg {

	const SceneVector DIRECTION_INITIAL = SceneVector(0.f, 0.f, 1.f);

	struct Direction : public SceneVector
	{
		Direction(const float &x, const float &y, const float &z = 0.f)
			: SceneVector(x, y, z) {}

		Direction(const glm::vec3 &vec)
			: SceneVector(vec) {}

		Direction (const SceneVector &vec):
			SceneVector(vec)
		{}


		inline btMatrix3x3 GetBasis() const
		{
			auto newXAxis = Normalized();
			auto newZAxis = newXAxis.Cross(SceneVector::Y_UNIT);

			return btMatrix3x3(
				newXAxis.Dot(SceneVector::X_UNIT), 0.0, newZAxis.Dot(SceneVector::X_UNIT),
				newXAxis.Dot(SceneVector::Y_UNIT), 1.0, newZAxis.Dot(SceneVector::Y_UNIT),
				newXAxis.Dot(SceneVector::Z_UNIT), 0.0, newZAxis.Dot(SceneVector::Z_UNIT)
			).transpose();
		}

		inline glm::mat4 GetRotationMatrix() const
		{
			return MatrixHelper::BTQuatToMat(GetQuat());
		}

		inline btQuaternion GetQuat() const
		{
			return btQuaternion(
				SceneVector(0.f, 1.f, 0.f),
				DIRECTION_INITIAL.Angle(
					this->Normalized(),
					SceneVector(0.f, 1.f, 0.f)
				)
			);
		}

		inline glm::quat GetGLMQuat()
		{
			auto dot = Normalized().Dot(SceneVector(1.f, 0.f));
			auto cosHalfAngle = glm::sqrt((1.f + dot) / 2.f);
			auto sinHalfAngle = glm::sqrt((1.f - dot) / 2.f);

			SceneVector axis = sinHalfAngle * SceneVector(1.f, 0.f).Cross(*this).Normalized();

			return glm::quat(cosHalfAngle, axis.X(), axis.Y(), axis.Z());
		}
	};

	// DirectionComponentManager

	class DirectionComponentManager : public ComponentManager
	{
	public:
		DirectionComponentManager(GameplayState *gps) : ComponentManager("direction", gps) {};
		~DirectionComponentManager() = default;

		virtual void AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const override;
		virtual LuaPlus::LuaObject GetComponentTable(entityx::Entity& entity) const override;
		virtual LuaPlus::LuaObject ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const override;
		void OrientTo(const LuaPlus::LuaObject &entityTable, const LuaPlus::LuaObject &vector) const;
		LuaPlus::LuaObject GetDir(const LuaPlus::LuaObject &entity) const;
	};
}