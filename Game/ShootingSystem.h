#pragma once

#include <entityx\System.h>
#include "Camera.h"
#include <lua/LuaPlus.h>

namespace mg {

	class ShootingSystem : public entityx::System<ShootingSystem>
	{
		friend class GameplayState;
		friend class Level;

		GameplayState* gps;
	public:

		virtual void configure(entityx::EventManager &events) override;
		virtual void update(entityx::EntityManager &es, entityx::EventManager &em, entityx::TimeDelta dt) override;

		ShootingSystem(GameplayState* gps);
	};
}