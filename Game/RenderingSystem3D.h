#pragma once

#include <entityx\System.h>
#include <memory>

#include "Effect.h"
#include "VBO.h"
#include "Position.h"
#include "Texture.h"
#include "VertTypes.h"
#include "Model.h"
#include "Batch3D.h"
#include "ObjectProps.h"

namespace mg {

	class Camera;
	class GameplayState;

	class RenderingSystem3D : public entityx::System<RenderingSystem3D>
	{
		friend class GameplayState;

		GLuint skinAtlasTexUnit;

		float maxLayerOffset;
		LuaPlus::LuaState* luaState;

		Texture boneMxBufferTexture;
		VBO<glm::mat4> boneMxBuffer;
		VBO<MeshProps> meshPropsBuffer;
		VBO<GidProps> gidPropsBuffer;

		bool displayMeshes;

	public:
		RenderingSystem3D(GameplayState* gps);
		~RenderingSystem3D();

		void configure(entityx::EventManager &events) override;
		void update(entityx::EntityManager &es, entityx::EventManager &em, entityx::TimeDelta dt) override;

		GameplayState* gps;

		inline void SwitchDisplayMeshes()
		{
			displayMeshes = !displayMeshes;
		}
	};

}