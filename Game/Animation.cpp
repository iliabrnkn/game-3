#include "stdafx.h"

#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/transform.hpp>

#include "Animation.h"
#include "GameplayState.h"
#include "Skeleton.h"
#include "LuaHelper.h"

namespace mg {

	bool Animation::GetCurrentKey(const string& channelName, AnimKey& key, int activeKeyNum, float interpolant)
	{
		auto foundChannelIter = channels.find(channelName);

		if (foundChannelIter != channels.end())
		{
			if (foundChannelIter->second.keys.size() > 1)
			{
				AnimKey& prevKey = foundChannelIter->second.keys[activeKeyNum];
				AnimKey& nextKey = foundChannelIter->second.keys[activeKeyNum + 1];

				// �������������������� �������� ���� ��������
				key.pos = glm::mix(prevKey.pos, nextKey.pos, static_cast<float>(interpolant));
				key.rot = glm::slerp(prevKey.rot, nextKey.rot, static_cast<float>(interpolant));
				key.scale = glm::mix(prevKey.scale, nextKey.scale, static_cast<float>(interpolant));
			}
			else
			{
				// ���� � ������ ����� ���� ����
				key = foundChannelIter->second.keys[0];
			}
		}
		else
		{
			return false;
		}

		return true;
	}

	void AnimationHandler::CountElapsed(const double& deltaTime)
	{
		if (!staled) // ���� �������� ������������
		{
			elapsedTicks += deltaTime * animation->ticksPerSec * speed;

			if (animation->durationInTicks < elapsedTicks)
			{
				elapsedTicks = elapsedTicks - animation->durationInTicks;

				if (!looped)
				{
					staled = true;
				}
			}

			for (size_t i = 0; i < animation->keyTimes.size(); i++)
			{
				auto& keyAfter = i < animation->keyTimes.size() - 1 ? animation->keyTimes[i + 1] : animation->keyTimes[0];

				if (animation->keyTimes[i] <= elapsedTicks && elapsedTicks <= keyAfter)
				{
					interpolant = (elapsedTicks - animation->keyTimes[i]) / (keyAfter - animation->keyTimes[i]);
					activeKeyNum = i;

					break;
				}
			}
		}
		else
		{
			auto intensityNext = intensity - static_cast<float>(deltaTime) * ANIMATION_FADE_SPEED;
			intensity = intensityNext >= 0.f ? intensityNext : 0.f;
		}
	}

	bool AnimationHandler::GetCurrentKey(const string& channelName, AnimKey& key)
	{
		return animation->GetCurrentKey(channelName, key, activeKeyNum, interpolant);
	}

	void AnimationBatch::PushAnimation(AnimationHandler &animHandler, const bool &resetAnimationBatch)
	{
		auto foundIter = std::find_if(activeAnimations.begin(), activeAnimations.end(),
			[&animHandler](auto &activeAnimHandler) { return animHandler.animation->name == activeAnimHandler.animation->name; });

		// ���� � ���� ��� ���� ��� �������� 
		if (foundIter != activeAnimations.end())
		{
			/*if (animHandler.looped) */animHandler.elapsedTicks = foundIter->elapsedTicks;
			foundIter->intensity = 0.0;
		}

		// ������ ��� ��������� �������� � ���� ����������� (���� resetAnimationBatch == true)
		if (resetAnimationBatch)
		{
			for (auto iter = activeAnimations.begin(); iter != activeAnimations.end(); iter++)
			{
				iter->staled = true;
			}
		}

		// ��������� ����� �������� � ������ (���� ��� �������� ���������� �������� - � ������, ����� - � �����)
		if (resetAnimationBatch)
			activeAnimations.push_front(animHandler);
		else
			activeAnimations.push_back(animHandler);
	}

	//--- AnimationBatchComponentManager
	std::vector<std::string> AnimationBatchComponentManager::names;

	void AnimationBatchComponentManager::AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		AnimationBatch animationBatch;

		// ��� ����� ��������

		for (auto& name : names)
		{
			AnimationBatch::animationHandlerPrototypes.insert(make_pair(name, AnimationHandler(gps->CreateElement<Animation>(name), false)));
		}

		entity.assign<AnimationBatch>(animationBatch);
	}

	LuaPlus::LuaObject AnimationBatchComponentManager::GetComponentTable(entityx::Entity& entity) const
	{
		LuaPlus::LuaObject res;

		if (entity.has_component<AnimationBatch>())
		{
			auto componentHandler = entity.component<AnimationBatch>();

			res.AssignNewTable(gps->GetLuaState());

			int i = 1;

			for (auto anim : entity.component<AnimationBatch>()->activeAnimations)
			{
				res.SetString(i, anim.animation->name.c_str());
				++i;
			}
		}
		else
		{
			res.AssignNil();
		}

		return res;
	};

	// ��������� ��������
	void AnimationBatchComponentManager::PlayAnimation(LuaPlus::LuaObject &entity, LuaPlus::LuaObject &animationProps)
	{
		auto ent = gps->GetEntity(entity);
		auto skel = ent.component<Skeleton>();

		std::string animationName = LuaHelper::GetString(animationProps, "name");
		AnimationHandler animHandler = AnimationBatch::animationHandlerPrototypes.find(animationName)->second;

		// ��������� ��� ����������� �������
		std::vector<std::string> allowedRootNodes = LuaHelper::GetVectorOfStrings(LuaHelper::GetTable(animationProps, "allowedRootNodes"));

		for (auto &rootBoneName : allowedRootNodes)
		{
			skel->GetNodeNamesFrom(rootBoneName, animHandler.allowedBoneNodes);
		}

		// ���� �������� ����������� ������� �������
		if (!LuaHelper::GetTable(animationProps, "restrictedRootNodes", false).IsNil())
		{
			std::set<std::string> restrictedBones;

			for (auto &restrictedRootBoneName : LuaHelper::GetVectorOfStrings(LuaHelper::GetTable(animationProps, "restrictedRootNodes")))
			{
				skel->GetNodeNamesFrom(restrictedRootBoneName, restrictedBones);
			}

			for (auto &restrictedBone : restrictedBones)
			{
				animHandler.allowedBoneNodes.erase(restrictedBone);
			}
		}

		// �������� ��������
		animHandler.speed = LuaHelper::GetFloat(animationProps, "speed", false,
			(animHandler.animation->durationInTicks / animHandler.animation->ticksPerSec) / 
			LuaHelper::GetFloat(animationProps, "exactTime", false, animHandler.animation->durationInTicks / animHandler.animation->ticksPerSec) 
		); 

		// ���������� �� ��������
		animHandler.looped = LuaHelper::GetBoolean(animationProps, "looped", false, true);

		gps->GetEntity(entity).component<AnimationBatch>()->PushAnimation(
			animHandler,
			LuaHelper::GetBoolean(animationProps, "resetAnimationBatch", false, true)
		);
	}

	// ��������� �������� ���� ���
	/*void AnimationBatchComponentManager::PlayAnimationOnce(LuaPlus::LuaObject& table, const char* animationName, LuaPlus::LuaObject& animTime,
		const LuaPlus::LuaObject &rootNodeNamesTable)
	{
		auto entity = gps->GetEntity(table);

		if (!entity.component<AnimationBatch>()->animationChanging)
			return;

		float speedCoef = 1.f;

		// ���� ������ ������ ����� ��������
		if (!animTime.IsNil() && animTime.IsNumber() && animTime.GetNumber() != .0f)
		{
			auto iterFound = AnimationBatch::animationHandlerPrototypes.find(animationName);

			if (iterFound != AnimationBatch::animationHandlerPrototypes.end())
			{
				auto& anim = iterFound->second.animation;
				speedCoef = (anim->durationInTicks / anim->ticksPerSec) / static_cast<float>(animTime.GetNumber());
			}
		}

		std::vector<std::string> rootNodeNames = LuaHelper::GetVectorOfStrings(rootNodeNamesTable);
	}*/

	void AnimationBatchComponentManager::EnableAnimationChanging(LuaPlus::LuaObject& table, const bool &enabled)
	{
		gps->GetEntity(table).component<AnimationBatch>()->animationChanging = enabled;
	}

}