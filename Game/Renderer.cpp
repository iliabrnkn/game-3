#include "stdafx.h"

#include <vector>
#include <glm/gtc/type_ptr.hpp>
#include <lua\LuaPlus.h>

#include "FBO.h"
#include "Paths.h"
#include "OGLConstants.h"
#include "WindowConstants.h"
#include "sizes.h"
#include "LightSystem.h"
#include "ApplicationState.h"
#include "Core.h"

#include "Renderer.h"
#include "Controls.h"

namespace mg
{
	void Renderer::ClearBuffers(const int &flags)
	{
		glClear(flags);

		CHECK_GL_ERROR
	}

	void Renderer::RenderBloom(Scene& scene)
	{
		globalFBO.Bind();

		glViewport(0, 0, RENDERBUFFER_WIDTH, RENDERBUFFER_HEIGHT);

		// ��������� ������
		globalFBO.AttachTexture(GL_COLOR_ATTACHMENT0, particleTex.GetIndex());
		globalFBO.AttachTexture(GL_COLOR_ATTACHMENT1, blurredTex.GetIndex());
		GLenum buffersToDraw[]{ GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
		glDrawBuffers(2, buffersToDraw);
		ClearBuffers(GL_COLOR_BUFFER_BIT);
		
		scene.GetParticlesBatch().Bind();
		scene.GetParticlesEffect().Use();

		scene.GetParticlesEffect().InvokeDeferredUniformSetters();
		
		scene.GetParticlesBatch().Draw();
		scene.GetParticlesBatch().Unbind();

		// ��������� �����
		globalFBO.AttachTexture(GL_COLOR_ATTACHMENT0, blurredTex.GetIndex());

		GLenum buffersToDrawLines[1]{ GL_COLOR_ATTACHMENT0};
		glDrawBuffers(1, buffersToDrawLines);

		scene.GetLineBatch().Bind();
		scene.GetLinesEffect().Use();
		scene.GetLinesEffect().InvokeDeferredUniformSetters();

		scene.GetLineBatch().Draw();
		scene.GetLineBatch().Unbind();

		globalFBO.Unbind();

		screenBatch.Bind();
		bloomEffect.Use();
		bloomEffect.SetUniformMatrix4f("projectionMatrix", glm::value_ptr(windowProjectionMatrix));
		
		bool horizontal = 1;

		for (int i = 1; i <= BLOOM_POWER * 2; ++i)
		{
			if (horizontal)
			{
				bloomPingFBO.Bind();
				GLenum buffersToDraw[]{ BLOOM_PING_PONG_ATTACHMENT };
				glDrawBuffers(1, buffersToDraw);
			}
			else
			{
				bloomPongFBO.Bind();
				GLenum buffersToDraw[]{ BLOOM_PING_PONG_ATTACHMENT };
				glDrawBuffers(1, buffersToDraw);
			}

			glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

			//---
			ClearBuffers();

			bloomEffect.SetUniform1i("horizontal", static_cast<int>(horizontal));

			if (i > 1)
			{
				bloomEffect.SetUniform1i("bloomTex", horizontal ? bloomPongTex.GetTextureUnit() : bloomPingTex.GetTextureUnit());
			}
			else
			{
				bloomEffect.SetUniform1i("bloomTex", blurredTex.GetTextureUnit());
			}

			screenBatch.Draw();

			if (horizontal)
				bloomPingFBO.Unbind();
			else
				bloomPongFBO.Unbind();

			horizontal = !horizontal;
		}

		screenBatch.Unbind();

		CHECK_GL_ERROR
	}

	void Renderer::Init(LuaPlus::LuaState* luaState)
	{
		auto videoMode = Core::GetVideoMode();
		windowProjectionMatrix = glm::ortho<float>(0.0, videoMode.width, 0.f, videoMode.height, ZNEAR_SCENE, ZFAR_SCENE);
		renderbufferProjectionMatrix = glm::ortho<float>(0.0, RENDERBUFFER_WIDTH, 0.f, RENDERBUFFER_HEIGHT, ZNEAR_SCENE, ZFAR_SCENE);
		///---------------

		colorTex.Init(RENDERBUFFER_WIDTH, RENDERBUFFER_HEIGHT, GL_RGBA, GL_NEAREST);
		posTex.Init(RENDERBUFFER_WIDTH, RENDERBUFFER_HEIGHT, GL_RGBA, GL_NEAREST, NULL, GL_FLOAT, GL_RGBA32F);
		particleTex.Init(RENDERBUFFER_WIDTH, RENDERBUFFER_HEIGHT, GL_RGBA, GL_NEAREST);
		blurredTex.Init(RENDERBUFFER_WIDTH, RENDERBUFFER_HEIGHT, GL_RGBA, GL_NEAREST); // ���� ������������ �� ��� ������ ���� �������, ��� ���� ������������ ��������� �� ���������
		normalTex.Init(RENDERBUFFER_WIDTH, RENDERBUFFER_HEIGHT, GL_RGBA, GL_NEAREST, NULL, GL_HALF_FLOAT, GL_RGBA16F);
		normalTex3D.Init(RENDERBUFFER_WIDTH, RENDERBUFFER_HEIGHT, GL_RGBA, GL_NEAREST, NULL, GL_HALF_FLOAT, GL_RGBA16F);
		depthTex.Init(RENDERBUFFER_WIDTH, RENDERBUFFER_HEIGHT, GL_DEPTH_COMPONENT, GL_LINEAR, NULL, GL_FLOAT, GL_DEPTH_COMPONENT32);

		cloudMaskTex.Init(RENDERBUFFER_WIDTH, RENDERBUFFER_HEIGHT, GL_RGBA, GL_LINEAR);
		deferredRenderingColorOut.Init(RENDERBUFFER_WIDTH, RENDERBUFFER_HEIGHT, GL_RGBA, GL_NEAREST);
		
		// ����� �����
		dynamicCubicShadowMap.Init(SHADOWMAP_SIZE, SHADOWMAP_SIZE, GL_DEPTH_COMPONENT, GL_LINEAR, NULL, GL_HALF_FLOAT, GL_DEPTH_COMPONENT16, GL_TEXTURE_CUBE_MAP_ARRAY_ARB, LIGHT_MAX_DYNAMIC_OMNILIGHTS + LIGHT_MAX_STATIC_OMNILIGHTS);
		staticCubicShadowMap.Init(SHADOWMAP_SIZE, SHADOWMAP_SIZE, GL_DEPTH_COMPONENT, GL_LINEAR, NULL, GL_HALF_FLOAT, GL_DEPTH_COMPONENT16, GL_TEXTURE_CUBE_MAP_ARRAY_ARB, LIGHT_MAX_STATIC_OMNILIGHTS);
		dynamic2DShadowMap.Init(SHADOWMAP_SIZE, SHADOWMAP_SIZE, GL_DEPTH_COMPONENT, GL_LINEAR, NULL, GL_HALF_FLOAT, GL_DEPTH_COMPONENT16, GL_TEXTURE_2D_ARRAY, LIGHT_MAX_DYNAMIC_DIRECT_LIGHTS + LIGHT_MAX_STATIC_DIRECT_LIGHTS);
		static2DShadowMap.Init(SHADOWMAP_SIZE, SHADOWMAP_SIZE, GL_DEPTH_COMPONENT, GL_LINEAR, NULL, GL_HALF_FLOAT, GL_DEPTH_COMPONENT16, GL_TEXTURE_2D_ARRAY, LIGHT_MAX_STATIC_DIRECT_LIGHTS);
		directionalShadowMap.Init(DIRECTIONAL_SHADOWMAP_WIDTH, DIRECTIONAL_SHADOWMAP_HEIGHT, GL_DEPTH_COMPONENT, GL_LINEAR, NULL, GL_HALF_FLOAT, GL_DEPTH_COMPONENT16);

		dynamicCubicShadowMap.SetComparisonMode(GL_LEQUAL);
		staticCubicShadowMap.SetComparisonMode(GL_LEQUAL);
		dynamic2DShadowMap.SetComparisonMode(GL_LEQUAL);
		static2DShadowMap.SetComparisonMode(GL_LEQUAL);
		directionalShadowMap.SetComparisonMode(GL_LEQUAL);
		
		// ����
		bloomPingTex.Init(WINDOW_WIDTH, WINDOW_HEIGHT, GL_RGBA, GL_NEAREST);
		bloomPongTex.Init(WINDOW_WIDTH, WINDOW_HEIGHT, GL_RGBA, GL_NEAREST);

		///---------------
		
		InitFBO();

		this->luaState = luaState;

		screenBatch.Init(luaState);
		bloomBatch.Init(luaState);
		deferredRenderingBatch.Init(luaState);

		deferredRenderingEffect.LoadFromFile(EFFECTS_FOLDER + "Framebuffer.lua", luaState);
		renderToScreenEffect.LoadFromFile(EFFECTS_FOLDER + "Scaling.lua", luaState);
		bloomEffect.LoadFromFile(EFFECTS_FOLDER + "BloomPing.lua", luaState);

		CHECK_GL_ERROR
	}

	// ������������� fbo

	void Renderer::InitFBO()
	{
		//---- STATIC LIGHTS
		staticOmniLightFBO.Init();
		staticOmniLightFBO.Bind();
		glDrawBuffer(GL_NONE);
		glReadBuffer(GL_NONE);
		staticOmniLightFBO.AttachTexture(DEPTH_ATTACHMENT, staticCubicShadowMap.GetIndex(), GL_TEXTURE_CUBE_MAP_ARRAY_ARB);
		staticOmniLightFBO.Unbind();

		//---- DYNAMIC LIGHTS
		omniLightFBO.Init();
		omniLightFBO.Bind();
		glDrawBuffer(GL_NONE);
		glReadBuffer(GL_NONE);
		omniLightFBO.AttachTexture(DEPTH_ATTACHMENT, dynamicCubicShadowMap.GetIndex(), GL_TEXTURE_CUBE_MAP_ARRAY_ARB);
		omniLightFBO.Unbind();

		//---- SPOT LIGHTS
		spotLightFBO.Init();
		spotLightFBO.Bind();
		glDrawBuffer(GL_NONE);
		glReadBuffer(GL_NONE);
		spotLightFBO.AttachTexture(DEPTH_ATTACHMENT, dynamic2DShadowMap.GetIndex(), GL_TEXTURE_2D_ARRAY);
		spotLightFBO.Unbind();

		// DIRECTIONAL LIGHT
		directionalLightFBO.Init();
		directionalLightFBO.Bind();
		glDrawBuffer(GL_NONE);
		glReadBuffer(GL_NONE);
		directionalLightFBO.AttachTexture(DEPTH_ATTACHMENT, directionalShadowMap.GetIndex(), GL_TEXTURE_2D);
		directionalLightFBO.Unbind();

		// CLOUD FBO

		cloudFBO.Init();
		cloudFBO.Bind();
		cloudFBO.AttachTexture(COLOR_ATTACHMENT, cloudMaskTex.GetIndex());
		glDrawBuffer(COLOR_ATTACHMENT);
		glReadBuffer(GL_NONE);
		cloudFBO.CheckForErrors();
		cloudFBO.Unbind();

		//---- SCENE FBO
		globalFBO.Init();
		globalFBO.Bind();
		
		globalFBO.AttachTexture(COLOR_ATTACHMENT, colorTex.GetIndex());
		globalFBO.AttachTexture(POS_ATTACHMENT, posTex.GetIndex());
		globalFBO.AttachTexture(NORMAL_ATTACHMENT, normalTex.GetIndex());
		globalFBO.AttachTexture(NORMAL_3D_ATTACHMENT, normalTex3D.GetIndex());
		globalFBO.AttachTexture(PARTICLE_ATTACHMENT, particleTex.GetIndex());
		globalFBO.AttachTexture(BLOOM_ATTACHMENT, bloomPingTex.GetIndex());
		globalFBO.AttachTexture(DEPTH_ATTACHMENT, depthTex.GetIndex());

		GLenum buffersToDraw[] { COLOR_ATTACHMENT, POS_ATTACHMENT, NORMAL_ATTACHMENT, NORMAL_3D_ATTACHMENT};
		glDrawBuffers(4, buffersToDraw);

		globalFBO.Unbind();

		// ����
		bloomPingFBO.Init();
		bloomPingFBO.Bind();
		bloomPingFBO.AttachTexture(BLOOM_PING_PONG_ATTACHMENT, bloomPingTex.GetIndex());
		bloomPingFBO.Unbind();

		bloomPongFBO.Init();
		bloomPongFBO.Bind();
		bloomPongFBO.AttachTexture(BLOOM_PING_PONG_ATTACHMENT, bloomPongTex.GetIndex());
		bloomPongFBO.Unbind();

		// ���������� ���������
		deferredRenderingFBO.Init();
		deferredRenderingFBO.Bind();
		deferredRenderingFBO.AttachTexture(COLOR_ATTACHMENT, deferredRenderingColorOut.GetIndex());
		glDrawBuffer(COLOR_ATTACHMENT);
		glReadBuffer(GL_NONE);
		deferredRenderingFBO.CheckForErrors();
		deferredRenderingFBO.Unbind();

		CHECK_GL_ERROR
	}

	// ������������ ����� � fbo
	void Renderer::RenderScene(Scene& scene, Controls& controls)
	{
		// ��������� ����� �����
		globalFBO.Bind();

		globalFBO.AttachTexture(COLOR_ATTACHMENT, colorTex.GetIndex());
		globalFBO.AttachTexture(POS_ATTACHMENT, posTex.GetIndex());
		GLenum buffersToDraw[]{ COLOR_ATTACHMENT, POS_ATTACHMENT, NORMAL_ATTACHMENT, NORMAL_3D_ATTACHMENT};
		glDrawBuffers(4, buffersToDraw);

		ClearBuffers();

		glViewport(0, 0, RENDERBUFFER_WIDTH, RENDERBUFFER_HEIGHT);

		scene.GetGeomertyEffect().Use();
		scene.GetGeomertyEffect().InvokeDeferredUniformSetters();
		
		// �������������� �� ��������
		scene.GetGeometry().Bind();
		for (auto segmentID : scene.GetMainSceneDrawAdditionalInfo().sceneSegmentsToDraw)
		{
			const auto &segment = scene.GetSceneSegments().at(segmentID);
			scene.GetGeometry().Draw(segment.firstElement, segment.elementsCount);
		}
		scene.GetGeometry().Unbind();

		scene.GetCharactersBatch().Bind();
		scene.GetCharactersBatch().Draw();
		scene.GetCharactersBatch().Unbind();
		
		// debug
		RenderDebugData(scene);

		globalFBO.Unbind();

		CHECK_GL_ERROR
	}

	void Renderer::RenderDebugData(Scene& scene)
	{
		scene.GetSimpleBatch().Bind();
		scene.GetSimpleEffect().Use();
		scene.GetSimpleEffect().InvokeDeferredUniformSetters();
		scene.GetSimpleEffect().SetUniformMatrix4f("mvp", glm::value_ptr(Core::GetInstance()->camera->GetSceneMVP()));
		
		scene.GetSimpleBatch().Draw();
		scene.GetSimpleBatch().Unbind();
	}

	// ������ ���� �� ����������� ���������� �����, ������� �� �������� � ����������

	void Renderer::BakeStaticOmniLights(Scene& scene)
	{
		// ��������� ����� �� ����������� ���������� �����

		staticOmniLightFBO.Bind();

		glViewport(0, 0, SHADOWMAP_SIZE, SHADOWMAP_SIZE);

		scene.GetGeometry().Bind();

		scene.GetLightEffect().Use();
		scene.GetLightEffect().InvokeDeferredUniformSetters();
		
		ClearBuffers();
		
		// ��� ����������� ���������� ������ �� ��� ��� �����
		for (auto &lightPointPair : scene.GetLightsQueue())
		{
			scene.GetLightEffect().SetUniform1i("lightPointNum", lightPointPair.first);
			scene.GetGeometry().DrawInstanced(6);
		}
		
		scene.GetGeometry().Unbind();

		staticOmniLightFBO.Unbind();

		scene.ClearLightsQueue();

		CHECK_GL_ERROR
	}

	void Renderer::RenderOmniLights(Scene& scene)
	{
		glDepthFunc(GL_LEQUAL);

		// ��������� �����
		omniLightFBO.Bind();

		glViewport(0, 0, SHADOWMAP_SIZE, SHADOWMAP_SIZE);

		scene.GetLightEffect().Use();
		scene.GetLightEffect().InvokeDeferredUniformSetters();
		
		ClearBuffers();

		scene.GetGeometry().Bind();
		
		for (auto &lightPointPair : scene.GetLightsQueue())
		{
			scene.GetLightEffect().SetUniform1i("lightPointNum", lightPointPair.first);
			
			if (lightPointPair.second.drawLevelGeometry)
			{
				for (auto &segmentID : lightPointPair.second.sceneSegmentsToDraw)
				{
					const auto &segment = scene.GetSceneSegments().at(segmentID);
					scene.GetGeometry().DrawInstanced(6, segment.firstElement, segment.elementsCount);
				}
			}
		}
		
		scene.GetGeometry().Unbind();
		scene.GetCharactersBatch().Bind();

		for (auto &lightPointPair : scene.GetLightsQueue())
		{
			scene.GetLightEffect().SetUniform1i("lightPointNum", lightPointPair.first);

			if (lightPointPair.second.drawCharacters)
				scene.GetCharactersBatch().DrawInstanced(6);
		}
		
		scene.GetCharactersBatch().Unbind();

		omniLightFBO.Unbind();

		scene.ClearLightsQueue();

		CHECK_GL_ERROR
	}

	void Renderer::RenderSpotLights(Scene& scene)
	{
		glDepthFunc(GL_LEQUAL);

		// ��������� ������������ �����
		spotLightFBO.Bind();

		glViewport(0, 0, SHADOWMAP_SIZE, SHADOWMAP_SIZE);

		scene.GetSpotLightEffect().Use();
		scene.GetSpotLightEffect().InvokeDeferredUniformSetters();

		ClearBuffers();

		scene.GetGeometry().Bind();

		for (auto &lightPointPair : scene.GetDirLightsQueue())
		{
			scene.GetSpotLightEffect().SetUniform1i("lightPointNum", lightPointPair.first);

			/*if (lightPointPair.second.drawLevelGeometry)
			{
				for (auto &segmentID : lightPointPair.second.sceneSegmentsToDraw)
				{
					const auto &segment = scene.GetSceneSegments().at(segmentID);
					scene.GetGeometry().Draw(segment.firstElement, segment.elementsCount);
				}
			}*/
			scene.GetGeometry().Draw();
		}

		scene.GetGeometry().Unbind();

		scene.GetCharactersBatch().Bind();

		for (auto &lightPointPair : scene.GetDirLightsQueue())
		{
			scene.GetSpotLightEffect().SetUniform1i("lightPointNum", lightPointPair.first);

			//if (lightPointPair.second.drawCharacters)
				scene.GetCharactersBatch().Draw();
		}

		scene.GetCharactersBatch().Unbind();

		spotLightFBO.CheckForErrors();
		spotLightFBO.Unbind();

		CHECK_GL_ERROR
	}

	void Renderer::RenderDirectionalLight(Scene& scene)
	{
		glDepthFunc(GL_LEQUAL);

		// ��������� ����� �� ������������� �����
		directionalLightFBO.Bind();

		glViewport(0, 0, DIRECTIONAL_SHADOWMAP_WIDTH, DIRECTIONAL_SHADOWMAP_HEIGHT);

		scene.GetDirectionalLightEffect().Use();
		scene.GetDirectionalLightEffect().InvokeDeferredUniformSetters();

		ClearBuffers();

		scene.GetGeometry().Bind();

		// �������������� �� ��������
		for (auto segmentID : scene.GetMainSceneDrawAdditionalInfo().sceneSegmentsToDraw)
		{
			const auto &segment = scene.GetSceneSegments().at(segmentID);
			scene.GetGeometry().Draw(segment.firstElement, segment.elementsCount);
		}

		scene.GetGeometry().Draw();
		scene.GetGeometry().Unbind();
		
		scene.GetCharactersBatch().Bind();
		scene.GetCharactersBatch().Draw();
		scene.GetCharactersBatch().Unbind();

		directionalLightFBO.Unbind();

		CHECK_GL_ERROR
	}

	void Renderer::RenderClouds(Scene &scene)
	{
		glDepthFunc(GL_LEQUAL);
		cloudFBO.Bind();
		glViewport(0, 0, RENDERBUFFER_WIDTH, RENDERBUFFER_HEIGHT);

		scene.GetCloudEffect().Use();
		scene.GetCloudEffect().InvokeDeferredUniformSetters();

		ClearBuffers();
		scene.GetCloudBatch().Bind();
		scene.GetCloudBatch().Draw();
		scene.GetCloudBatch().Unbind();
		cloudFBO.Unbind();

		CHECK_GL_ERROR

	}
	
	// ������������ ���������� fbo �� �����
	void Renderer::DeferredRendering()
	{
		deferredRenderingFBO.Bind();
		glViewport(0, 0, RENDERBUFFER_WIDTH, RENDERBUFFER_HEIGHT);

		deferredRenderingBatch.Bind();

		deferredRenderingEffect.Use();
		deferredRenderingEffect.InvokeDeferredUniformSetters();

		deferredRenderingEffect.SetUniformMatrix4f("projectionMatrix", glm::value_ptr(renderbufferProjectionMatrix));
		deferredRenderingEffect.SetUniform1i("colorTex", colorTex.GetTextureUnit());
		deferredRenderingEffect.SetUniform1i("normalTex", normalTex.GetTextureUnit());
		deferredRenderingEffect.SetUniform1i("normalTex3D", normalTex3D.GetTextureUnit());
		deferredRenderingEffect.SetUniform1i("posTex", posTex.GetTextureUnit());

		deferredRenderingEffect.SetUniform1i("dynamicShadowMap", dynamicCubicShadowMap.GetTextureUnit());
		deferredRenderingEffect.SetUniform1i("staticShadowMap", staticCubicShadowMap.GetTextureUnit());
		deferredRenderingEffect.SetUniform1i("spotLightDynamicShadowMap", dynamic2DShadowMap.GetTextureUnit());
		deferredRenderingEffect.SetUniform1i("spotLightStaticShadowMap", static2DShadowMap.GetTextureUnit());
		deferredRenderingEffect.SetUniform1i("directionalShadowMap", directionalShadowMap.GetTextureUnit());
		
		deferredRenderingEffect.SetUniform1i("bloomTex", bloomPingTex.GetTextureUnit());
		deferredRenderingEffect.SetUniform1i("particleTex", particleTex.GetTextureUnit());
		deferredRenderingEffect.SetUniform1i("cloudMaskTex", cloudMaskTex.GetTextureUnit());

		ClearBuffers();

		deferredRenderingEffect.SetUniform1i("blooming", 0);
		deferredRenderingBatch.Draw();

		deferredRenderingBatch.Unbind();
		deferredRenderingFBO.Unbind();

		CHECK_GL_ERROR
	}

	// ����������� �������� �� ���� �����

	void Renderer::RenderToScreen()
	{
		auto videoMode = Core::GetInstance()->GetVideoMode();

		glViewport(0, 0, videoMode.width, videoMode.height);

		screenBatch.Bind();

		renderToScreenEffect.Use();
		renderToScreenEffect.InvokeDeferredUniformSetters();

		renderToScreenEffect.SetUniform1f("gamma", 1.7f);
		renderToScreenEffect.SetUniformMatrix4f("projectionMatrix", glm::value_ptr(windowProjectionMatrix));
		renderToScreenEffect.SetUniform1i("colorTex", deferredRenderingColorOut.GetTextureUnit());

		ClearBuffers();

		screenBatch.Draw();

		screenBatch.Unbind();

		CHECK_GL_ERROR
	}

	Effect& Renderer::GetDeferredRenderingEffect()
	{
		return deferredRenderingEffect;
	}

	/*Effect& Renderer::GetStaticLightEffect()
	{
		return staticLuminanceEffect;
	}*/

	void Renderer::SetLightCount(const int& lightsCountInitial)
	{
		lightsCount = lightsCountInitial;
		GUI::GetInstance().Watch("lightsCount", lightsCount);
	}

	void Renderer::SetSpotLightCount(const int& dirLightsCountInitial)
	{
		spotLightsCount = dirLightsCountInitial;
		GUI::GetInstance().Watch("dirLightsCount", spotLightsCount);
	}
}