#pragma once

#include <bullet/btBulletDynamicsCommon.h>

#include "ComponentManager.h"

namespace mg {

	// �������� ��� btRigidBody
	// ��������� ����� ���������� ����� ���� ��� ��������� �� ��� ������������
	
	struct Collideable
	{
		RigidBody* body;

		Collideable(RigidBody *body) : body(body)
		{
		}
	};

	// CollideableComponentManager

	class CollideableComponentManager : public ComponentManager
	{
	public:
		CollideableComponentManager(GameplayState *gps) : ComponentManager("collideable", gps) {};
		~CollideableComponentManager() = default;

		virtual void AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const override;
		virtual LuaPlus::LuaObject GetComponentTable(entityx::Entity& entity) const override;
		virtual LuaPlus::LuaObject ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const;
		void ApplyCentralImpulse(const int &bodyWorldArrayIndex, const LuaPlus::LuaObject &impulseTable) const;
		void ApplyImpulse(const int &bodyWorldArrayIndex, const LuaPlus::LuaObject &impulseTable, const LuaPlus::LuaObject &relPoint) const;
		void ApplyTorqueImpulse(const LuaPlus::LuaObject &entityTable, const LuaPlus::LuaObject &torqueTable) const;
		void SetAngularVelocity(const LuaPlus::LuaObject &entityTable, const LuaPlus::LuaObject &angularVelocityTable) const;
		void ApplyCentralForce(const int &bodyWorldArrayIndex, const LuaPlus::LuaObject &forceVec) const;
		/*void AddConstraint(const int &firstBodyIndex, const int &secBodyIndex, const LuaPlus::LuaObject &constraintTable,
			const bool &addReferenceA, const bool &addReferenceB);*/
		void SetWeaponBody(const LuaPlus::LuaObject &characterEntityTable, const LuaPlus::LuaObject &weaponTable, const char* weaponJointName);
		LuaPlus::LuaObject DebugTransformEntity(const LuaPlus::LuaObject &entityTable, const LuaPlus::LuaObject &eulerAnglesOffset,
			const LuaPlus::LuaObject &originOffset);
		LuaPlus::LuaObject GetBodyPos(const int &bodyArrayIndex);
		int GetWorldArrayIndex(const LuaPlus::LuaObject &entity);
	};
}