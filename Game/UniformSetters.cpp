#include "stdafx.h"

#include "UniformSetters.h"
#include "Effect.h"
#include "ErrorLog.h"

namespace mg {

	void UniformISetter::Invoke()
	{
		effect->SetUniform1i(varName, val);
	}

	void UniformFSetter::Invoke()
	{
		effect->SetUniform1f(varName, val);
	}

	void Uniform2FSetter ::Invoke()
	{
		effect->SetUniform2f(varName, val);
	}

	void Uniform3FSetter::Invoke()
	{
		effect->SetUniform3f(varName, val);
	}

	void UniformMx4Setter::Invoke()
	{
		effect->SetUniformMatrix4f(varName, glm::value_ptr(val));
	}

	void UniformBlockSetter::Invoke()
	{
		glBindBuffer(GL_UNIFORM_BUFFER, bufferID);
		CHECK_GL_ERROR

		effect->SetUniformBlock(varName, bufferID, index, size);
		
		glBindBuffer(GL_UNIFORM_BUFFER, 0);
		CHECK_GL_ERROR
	}
}