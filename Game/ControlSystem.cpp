#include "stdafx.h"

#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/vector_angle.hpp>

#include "ControlSystem.h"
#include "Camera.h"
#include "Model.h"
#include "Moveable.h"
#include "Controllable.h"
#include "Matrices.h"
#include "Pathfinding.h"
#include "WindowConstants.h"
#include "Objects.h"
#include "Direction.h"
#include "Helpers.h"
#include "GUI.h"
#include "Matrices.h"
#include "ChangeAnimation.h"
#include "AIPiece.h"
#include "Skeleton.h"
#include "Orientation.h"

namespace mg
{
	void ControlSystem::configure(entityx::EventManager &events)
	{
		keyUpdatesPerSec = 1.0 / 30.0;
		updatesPerSec = 1.0 / 50.0;
		timeFromLastClick = 0.0;
		timeFromLastKeyPressed = 0.0;
		toggleFightMode = false;
	}

	void ControlSystem::update(entityx::EntityManager &es, entityx::EventManager &em, entityx::TimeDelta dt)
	{
		timeFromLastClick += dt;
		timeFromLastKeyPressed += dt;

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
			if (timeFromLastClick >= updatesPerSec)
				camera->MoveBy(-0.3f, -0.3f);

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
			if (timeFromLastClick >= updatesPerSec)
				camera->MoveBy(0.3f, 0.3f);

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			if (timeFromLastClick >= updatesPerSec)
				camera->MoveBy(-0.3f, 0.3f);

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			if (timeFromLastClick >= updatesPerSec)
				camera->MoveBy(0.3f, -0.3f);

		// make camera follow the contollable character


		es.each<Position, Moveable, Controllable, Direction, Skeleton, AIPiece, Orientation>([dt, &em, this](entityx::Entity entity, Position& position, 
			Moveable& moveable, Controllable& controllable, Direction& direction, Skeleton& skeleton, AIPiece& aipiece, Orientation& orientation) {
			
			auto mouseScreenPos = sf::Mouse::getPosition(*window);
			bool upKeyPressed(false), downKeyPressed(false), leftKeyPressed(false), rightKeyPressed(false);
			

			// left mouse click handling

			// moving keys handling

			if (timeFromLastKeyPressed >= keyUpdatesPerSec)
			{
				shiftOffset = glm::vec2(0.f);
				glm::vec2 keyMovingDir(0.0);

				if ((upKeyPressed = sf::Keyboard::isKeyPressed(sf::Keyboard::W)) | // up
					(downKeyPressed = sf::Keyboard::isKeyPressed(sf::Keyboard::S)) | // down
					(leftKeyPressed = sf::Keyboard::isKeyPressed(sf::Keyboard::A)) | // left
					(rightKeyPressed = sf::Keyboard::isKeyPressed(sf::Keyboard::D))) // right
				{
					if (upKeyPressed)
					{
						keyMovingDir.x += -0.5f;
						keyMovingDir.y += -0.5f;
					}

					if (downKeyPressed)
					{
						keyMovingDir.x += 0.5f;
						keyMovingDir.y += 0.5f;
					}

					if (leftKeyPressed)
					{
						keyMovingDir.x += -0.5f;
						keyMovingDir.y += 0.5f;
					}

					if (rightKeyPressed)
					{
						keyMovingDir.x += 0.5f;
						keyMovingDir.y += -0.5f;
					}

					em.emit<CancelPathfinding>(entity);
					moveable.rawMovementDir = glm::normalize(keyMovingDir);
					moveable.speedScalarIncrement = moveable.speedScalarMax / SPEED_UP_COEFFICIENT;
				}
				else
				{
					moveable.speedScalarIncrement = -moveable.speedScalarMax / SPEED_UP_COEFFICIENT;
				}

				//Looking around

				auto mousePosWindow = sf::Mouse::getPosition(*window);

				if (mousePosWindow.x >= 0 && mousePosWindow.y >= 0 && mousePosWindow.x <= window->getSize().x && mousePosWindow.y <= window->getSize().y)
				{
					// color picking

					auto mousePos = sf::Vector2i(float(mousePosWindow.x) * float(RENDERBUFFER_WIDTH) / float(WINDOW_WIDTH),
						float(mousePosWindow.y) * float(RENDERBUFFER_HEIGHT) / float(WINDOW_HEIGHT)) 
						- sf::Vector2i(float(RENDERBUFFER_WIDTH) / (float(WINDOW_WIDTH) * 2.f), float(RENDERBUFFER_HEIGHT) / (float(WINDOW_HEIGHT) * 2.f));

					shiftOffset = 0.5f * (inversed2DMatrix * glm::vec2(float(mousePos.x), float(mousePos.y)) - MAX_AXIS_HALF);

					if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift))
					{
						shiftOffset *= 2.f;
					}
				}

				//Fight mode toggle

				if (toggleFightMode && !canDisableFightMode)
					if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Tab))
					{
						canDisableFightMode = true;
					}

				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Tab))
				{
					if (toggleFightMode && canDisableFightMode)
					{
						toggleFightMode = false;
						canDisableFightMode = false;
					}
					else
					{
						toggleFightMode = true;
						aipiece.Emit("onStartAiming");
					}
				}

				timeFromLastKeyPressed = 0.0;
			}

			camera->MoveTo(position.x + shiftOffset.x, position.y + shiftOffset.y);
		});
	}
}