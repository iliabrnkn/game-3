#pragma once

#include <string>

#include "entityx\Entity.h"
#include "Animation.h"

namespace mg {
	struct AnimationChanging
	{
		AnimationChanging(const std::string& animNameInit, entityx::Entity entity, const bool& looped = true, const float& speedCoefficient = 1.0,
			const std::vector<std::string> &allowedBones,
			const std::vector<std::string> restrictedBones, 
			const bool &resetAnimationBatch = true)
			: entity(entity),
			animName(animNameInit),
			looped(looped),
			speedCoefficient(speedCoefficient),
			allowedRootBones(allowedBones),
			resetAnimationBatch(resetAnimationBatch)
		{}

		std::string animName;
		entityx::Entity entity;
		bool looped;
		float speedCoefficient;
		std::vector<std::string> allowedRootBones;
		bool resetAnimationBatch;
	};
}
