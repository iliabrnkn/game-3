#include "stdafx.h"

#include "Position.h"
#include "Collideable.h"
#include "GameplayState.h"

namespace mg
{
	void PositionComponentManager::AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		entity.assign<Position>(SceneVector::FromMapCoords(table));
	}

	LuaPlus::LuaObject PositionComponentManager::GetComponentTable(entityx::Entity& entity) const
	{
		if (entity.has_component<Position>())
		{
			return entity.component<Position>()->ToMapCoords(gps->GetLuaState());
		}
		else
		{
			return LuaPlus::LuaObject().AssignNil();
		}
	};

	LuaPlus::LuaObject PositionComponentManager::ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		auto component = entity.component<Position>();

		if (!entity.has_component<Position>())
			return LuaPlus::LuaObject().AssignNil();

		//*component = LuaHelper::TableToVec3(table);

		SceneVector newPos = SceneVector::FromMapCoords(table);

		if (!table.GetByName("x").IsNil()) component->X() = newPos.X();
		if (!table.GetByName("y").IsNil()) component->Z() = newPos.Z();
		if (!table.GetByName("z").IsNil()) component->Y() = newPos.Y();

		// ���� � �������� ���� ��������� Collideable, �������� ���������� �������� ����
		if (entity.has_component<Collideable>())
		{
			RigidBody *body = entity.component<Collideable>()->body;
			btTransform currTransform; body->getMotionState()->getWorldTransform(currTransform);

			body->setWorldTransform(
				btTransform(currTransform.getBasis(), *component)
			);
		}

		return GetComponentTable(entity);
	};

	float PositionComponentManager::GetDistance(const LuaPlus::LuaObject &firstEntityTable, const LuaPlus::LuaObject &secondEntityTable) const
	{
		auto firstEntity = gps->GetEntity(firstEntityTable);
		auto secondEntity = gps->GetEntity(secondEntityTable);

		if (firstEntity.has_component<Position>() && secondEntity.has_component<Position>())
		{
			auto pos1 = firstEntity.component<Position>();
			auto pos2 = secondEntity.component<Position>();

			return (*pos2 - *pos1).Length();
		}
		else
			return -1.0;
	}

	LuaPlus::LuaObject PositionComponentManager::GetPos(const LuaPlus::LuaObject &entity) const
	{
		return gps->GetEntity(entity).component<Position>()->ToMapCoords(gps->GetLuaState());
	}
}