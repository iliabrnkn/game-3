#pragma once

#include "ComponentManager.h"

namespace mg {
	
	struct Strikeable
	{
		bool active;

		Strikeable(const bool &active = false)
			: active(active) {}
	};

	class StrikeableComponentManager : public ComponentManager
	{
		public:
		StrikeableComponentManager(GameplayState *gps) : ComponentManager("strikeable", gps) {};
		~StrikeableComponentManager() = default;

		virtual void AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const override;
		virtual LuaPlus::LuaObject GetComponentTable(entityx::Entity& entity) const override;
		virtual LuaPlus::LuaObject ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const override;
		void SetActiveStrikeable(const LuaPlus::LuaObject& entityTable, const bool &active);
		bool IsActiveStrikeable(const LuaPlus::LuaObject& entityTable);
	};
}