#pragma once

#include <string>
#include <vector>

namespace mg {

	class FileHelper
	{

	public:

		static struct Paths {
			std::string DATA_FOLDER = std::string(_getcwd(NULL, 0)) + "\\Data\\";
			std::string MODELS_FOLDER = DATA_FOLDER + "Models\\";
			std::string ANIMATIONS_FOLDER = DATA_FOLDER + "Animations\\";
			std::string IMAGES_FOLDER = DATA_FOLDER + "Images\\";
			std::string TILESETS_FOLDER = IMAGES_FOLDER + "Tilesets\\";
			std::string TILESETS_GEOMETRY_FOLDER = MODELS_FOLDER + "TileGeometry\\";
			std::string EFFECTS_FOLDER = DATA_FOLDER + "Effects\\";
			std::string FONTS_FOLDER = DATA_FOLDER + "Fonts\\";
			std::string MAPS_FOLDER = DATA_FOLDER + "Maps\\";
			std::string TEXTURES_FOLDER = IMAGES_FOLDER + "Textures\\";
			std::string SCRIPTS_FOLDER = DATA_FOLDER + "Scripts\\";
			std::string GUI_FOLDER = IMAGES_FOLDER + "GUI\\";
			std::string SOUND_FOLDER = DATA_FOLDER + "Sound\\";
		} PATHS;

		static std::vector<std::string> GetFileNameParts(const std::string& fullFilename);

		static std::string GetFileNameWithoutResolution(const std::string& filename);
	};
}