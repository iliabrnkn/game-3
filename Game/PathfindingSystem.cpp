#include "stdafx.h"

#include <algorithm>
#include <glm\glm.hpp>

#include "GameplayState.h"
#include "Pathable.h"
#include "PathfindingSystem.h"
#include "Position.h"

namespace mg {
	PathfindingSystem::PathfindingSystem(GameplayState* gps) :
		gps(gps)
	{}

	void PathfindingSystem::configure(entityx::EventManager &events)
	{
	}

	void PathfindingSystem::update(entityx::EntityManager &es, entityx::EventManager &em, entityx::TimeDelta dt)
	{
		es.each<Pathable, Position>([dt, this](entityx::Entity& entity, Pathable &path, Position &pos)
		{
			if (path.waypoints.size() > 0)
			{
				for (int i = 1; i < path.waypoints.size(); ++i)
				{
					gps->GetScene().GetSimpleBatch().DrawVector(path.waypoints[i - 1],
						path.waypoints[i] - path.waypoints[i - 1], glm::vec3(1.f, 0.f, 0.f));
				}
			}

			// ���� ���������� �� ��������� ����� ������ LAST_STEP_LEN, ������� ����� � ������ ��������
			if (path.waypoints.size() > 0 && (path.waypoints.back() - pos).Length() < LAST_STEP_LEN)
			{
				path.waypoints.pop_back();
			}
		});
	}
}