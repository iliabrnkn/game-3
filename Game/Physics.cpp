#include "stdafx.h"

#include "Physics.h"
//#include "bullet\Serialize\BulletWorldImporter\btBulletWorldImporter.h"

namespace mg {
	
	// Physics	

	Physics::Physics()
	{
		collisionConfiguration = new btDefaultCollisionConfiguration();
		dispatcher = new btCollisionDispatcher(collisionConfiguration);
		overlappingPairCache = new btDbvtBroadphase();
		solver = new btSequentialImpulseConstraintSolver();
		dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, overlappingPairCache, solver, collisionConfiguration);

		dynamicsWorld->setBroadphase(overlappingPairCache);
		dynamicsWorld->setGravity(btVector3(0, -10.f, 0));
	}

	Physics::~Physics()
	{
	}

	void Physics::Free()
	{
		for (int i = dynamicsWorld->getNumConstraints() - 1; i >= 0; --i)
		{
			dynamicsWorld->removeConstraint(dynamicsWorld->getConstraint(i));
		}

		for (int i = dynamicsWorld->getNumCollisionObjects() - 1; i >= 0 ; --i)
		{
			auto *obj = dynamicsWorld->getCollisionObjectArray()[i];
			auto *body = btRigidBody::upcast(obj);

			if (body && body->getMotionState())
			{
				delete body->getMotionState();
			}

			if (obj->getCollisionShape())
			{
				delete obj->getCollisionShape();
			}

			dynamicsWorld->removeCollisionObject(obj);
			delete obj;
		}

		/*delete collisionConfiguration;
		delete dispatcher;
		delete overlappingPairCache;
		delete solver;*/
		delete dynamicsWorld;
		delete solver;
		delete overlappingPairCache;
		delete dispatcher;
		delete collisionConfiguration;
	}

	void Physics::CollisionDetection()
	{
		int numManifolds = dynamicsWorld->getDispatcher()->getNumManifolds();

		for (int i = 0; i < numManifolds; i++)
		{
			btPersistentManifold* contactManifold = dynamicsWorld->getDispatcher()->getManifoldByIndexInternal(i);

			const btCollisionObject* obA = contactManifold->getBody0();
			const btCollisionObject* obB = contactManifold->getBody1();

			int numContacts = contactManifold->getNumContacts();

			for (int j = 0; j < numContacts; j++)
			{
				btManifoldPoint& pt = contactManifold->getContactPoint(j);

				if (pt.getDistance() < 0.f)
				{
					const btVector3& ptA = pt.getPositionWorldOnA();
					const btVector3& ptB = pt.getPositionWorldOnB();
					const btVector3& normalOnB = pt.m_normalWorldOnB;
				}
			}
		}
	}

	RigidBody* Physics::CreateRigidBody(const btScalar &mass, const btTransform& startTransform, btCollisionShape* shape,
		const btVector3 &customDebugColor, const int &collisionFlags,
		const int &group, const int &mask)
	{
		bool isDynamic = (mass != 0.f);

		btVector3 localInertia(0, 0, 0);
		if (isDynamic)
			shape->calculateLocalInertia(mass, localInertia);

		MotionState* myMotionState = new MotionState(startTransform);
		btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, shape, localInertia);
		rbInfo.m_additionalDamping = true;
		RigidBody* body = new RigidBody(rbInfo);
		body->setContactStiffnessAndDamping(3000.0, 3000.0);

		myMotionState->m_userPointer = body;

		if (customDebugColor != btVector3(0.0, 0.0, 0.0))
		{
			body->setCustomDebugColor(customDebugColor);
		}

		if (collisionFlags != -1)
		{
			body->setCollisionFlags(body->getCollisionFlags() & collisionFlags);
		}

		dynamicsWorld->addRigidBody(body, group, mask);

		return body;
	}

	btGeneric6DofConstraint* Physics::CreateConstraint(
		btRigidBody *firstBody, btRigidBody *secBody,
		const SceneVector &frameAOrigin, const SceneVector &frameBOrigin,
		const btVector3 &angularLowerLimits, const btVector3 &angularUpperLimits,
		const btVector3 &linearLowerLimits, const btVector3 &linearUpperLimits,
		const bool &axisShift, const bool &collision)
	{
		btMatrix3x3 frameABasis = firstBody->getWorldTransform().getBasis().inverse();
		btMatrix3x3 frameBBasis = secBody->getWorldTransform().getBasis().inverse();

		return CreateConstraint(
			firstBody, secBody,
			frameAOrigin, frameBOrigin, frameABasis, frameBBasis, 
			angularLowerLimits, angularUpperLimits,linearLowerLimits, linearUpperLimits, 
			axisShift, collision
		);
	}

	btGeneric6DofConstraint* Physics::CreateConstraint(
		btRigidBody *firstBody, btRigidBody *secBody,
		const SceneVector &frameAOrigin, const SceneVector &frameBOrigin,
		const btMatrix3x3 &frameABasisInitial, const btMatrix3x3 &frameBBasisInitial,
		const btVector3 &angularLowerLimits, const btVector3 &angularUpperLimits,
		const btVector3 &linearLowerLimits, const btVector3 &linearUpperLimits,
		const bool &axisShift, const bool &collision)
	{
		btMatrix3x3 frameABasis = frameABasisInitial, frameBBasis = frameBBasisInitial;

		if (axisShift)
		{
			std::swap(frameABasis[0], frameABasis[1]);
			std::swap(frameABasis[1], frameABasis[2]);
			std::swap(frameBBasis[0], frameBBasis[1]);
			std::swap(frameBBasis[1], frameBBasis[2]);
		}

		btTransform frameA(frameABasis, frameAOrigin);
		btTransform frameB(frameBBasis, frameBOrigin);
		btGeneric6DofConstraint *constraint = new btGeneric6DofConstraint(*firstBody, *secBody, frameA, frameB, false);
		
		// -- ��������� ����������� ����� �������
		constraint->setAngularLowerLimit(angularLowerLimits);
		constraint->setAngularUpperLimit(angularUpperLimits);
		
		constraint->setLinearLowerLimit(linearLowerLimits);
		constraint->setLinearUpperLimit(linearUpperLimits);

		constraint->setBreakingImpulseThreshold(10000000);
		constraint->setEnabled(true);
		dynamicsWorld->addConstraint(constraint, !collision);

		return constraint;
	}

	RigidBody* Physics::GetBodyByWorldArrayIndex(const int &worldArrayIndex)
	{
		return reinterpret_cast<RigidBody*>(dynamicsWorld->getCollisionObjectArray()[worldArrayIndex]);
	}

	// Motion state
	MotionState::MotionState(const btTransform& startTrans, const btTransform& centerOfMassOffset) :
			btDefaultMotionState(startTrans, centerOfMassOffset)
	{
	}

	void MotionState::getWorldTransform(btTransform& centerOfMassWorldTrans) const
	{
		centerOfMassWorldTrans = m_graphicsWorldTrans * m_centerOfMassOffset.inverse();
	}

	void MotionState::setWorldTransform(const btTransform& centerOfMassWorldTrans)
	{
		if (((RigidBody*)m_userPointer)->getLinearVelocity().length() > 0.1)
			prevIterationMoving = true;
		else
			prevIterationMoving = false;

		m_graphicsWorldTrans = centerOfMassWorldTrans * m_centerOfMassOffset;
	}

	int Physics::GetEntityIDByRay(const SceneVector &start, const SceneVector &end) const
	{
		btCollisionWorld::ClosestRayResultCallback rayCallback(start, end);
		dynamicsWorld->rayTest(start, end, rayCallback);

		if (!rayCallback.hasHit() || !rayCallback.m_collisionObject)
			return -1;

		return rayCallback.m_collisionObject->getUserIndex();
	}

	SceneVector Physics::GetHitSceneCoordsByRay(const SceneVector &rayStart, const SceneVector &rayEnd) const
	{
		btCollisionWorld::ClosestRayResultCallback rayCallback(rayStart, rayEnd);
		dynamicsWorld->rayTest(rayStart, rayEnd, rayCallback);
		
		if (rayCallback.hasHit())
		{
			//return SceneVector(rayStart + (rayEnd - rayStart) * rayCallback.m_closestHitFraction);
			return SceneVector(rayCallback.m_hitPointWorld);
		}
		else
		{
			return (rayEnd);
		}
	}

	int Physics::GetWorldArrayIndex(const RigidBody *body)
	{
		return body->getWorldArrayIndex();
	}

	//void Physics::WriteConstraintToFile(btGeneric6DofConstraint *cs, const std::string &filename)
	//{
	//	auto *serializer = new btDefaultSerializer(MAX_SERIALIZATION_BUFFER_SIZE);
	//	
	//	serializer->startSerialization();
	//	
	//	btChunk* chunk = serializer->allocate(
	//		cs->calculateSerializeBufferSize(), 
	//		1
	//	);

	//	serializer->finalizeChunk(
	//		chunk, 
	//		cs->serialize(chunk->m_oldPtr, serializer), 
	//		BT_CONSTRAINT_CODE, 
	//		cs
	//	);
	//	serializer->finishSerialization();
	//	
	//	auto* file = fopen(filename.c_str(), "wb");
	//	
	//	fwrite(serializer->getBufferPointer(), serializer->getCurrentBufferSize(), 1, file);
	//	fclose(file);
	//}

	//btGeneric6DofConstraint *LoadConstraintFromFile()
	//{
	//	btBulletWorldImporter import(0);//don't store info into the world
	//	import.getConstraintByName()
	//	if (import.loadFile("myShape.bullet"))
	//	{
	//		int numShape = import.getNumCollisionShapes();
	//		if (numShape)
	//		{
	//			trimeshShape = (btBvhTriangleMeshShape*)import.getCollisionShapeByIndex(0);
	//		}
	//	}
	//}
}