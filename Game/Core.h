#pragma once

#include <string>
#include <unordered_map>
#include <memory>
#include <vector>
#include <mutex>

#include <windows.h>
#include <GL/glew/glew.h>
#include <GL/GL.h>
#include <SFML/Window.hpp>
#include <SFML/Window/WindowStyle.hpp>
#include <SFML/Audio.hpp>
#include <sfml/Graphics.hpp>
#include <sfml/Graphics/RenderTexture.hpp>
#include <lua\LuaPlus.h>

#include "ApplicationState.h"
#include "GameplayState.h"
#include "Texture.h"
#include "VBO.h"
#include "VertTypes.h"
#include "Effect.h"
#include "Renderer.h"
#include "Camera.h"
#include "Texture.h"
#include "Controls.h"

namespace mg {

	class Core
	{
		static const int SCREEN_TEX = 0;
		static const int SELECTION_TEX = 1;


	public:
		~Core();

		void Init();
		void Loop();
		sf::RenderWindow& GetWindow();
		static Core* GetInstance();
		std::shared_ptr<Camera> camera;
		static sf::VideoMode GetVideoMode();
		static void SetVideoMode(const sf::VideoMode& videoMode, const int& windowStyle);
		Renderer* GetRenderer();
		LuaPlus::LuaState *GetLuaState();

		static bool fs;

		void TestSound();

		inline const Controls &GetControls() const
		{
			return controls;
		}

	private:
		Core();
		Core(Core&) = delete;
		sf::Sound sound;

		double lastFrameDelta;
		//sf::Vector2f mouseScaleFactor;

		sf::SoundBuffer testBuffer;

		std::unordered_map<std::string, std::unique_ptr<ApplicationState>> states;
		ApplicationState *currentState;
		sf::RenderWindow window;
		sf::Clock clock;
		std::unique_ptr<Renderer> renderer;
		std::vector<GLenum> colorAttachments;
		LuaPlus::LuaState* luaState;

		std::mutex guiMutex;

		static sf::VideoMode videoMode;
		static int windowStyle;
		Controls controls;
	};

}