#include "stdafx.h"

#include <map>
#include <algorithm>
#include <glm/gtx/quaternion.hpp>

#include "MeshBatchSystem.h"
#include "Paths.h"
#include "OGLConstants.h"
#include "math.h"
#include "Sizes.h"
#include "GUI.h"
#include "GameplayState.h"
#include "Core.h"
#include "Level.h"
#include "Direction.h"

namespace mg
{
	MeshBatchSystem::MeshBatchSystem(GameplayState* gps) : gps(gps), recreateGeometryBatch(false)
	{}

	MeshBatchSystem::~MeshBatchSystem()
	{}

	void MeshBatchSystem::configure(entityx::EventManager &events)
	{
	}

	void MeshBatchSystem::update(entityx::EntityManager &es, entityx::EventManager &em, entityx::TimeDelta dt)
	{
		/*es.each<Weapon>([&dt, this](entityx::Entity& entity, Weapon& weapon)
		{
			if (weapon.removed)
				recreateGeometryBatch = true;
		});*/

		es.each<Mesh>([&dt, this](entityx::Entity& entity, Mesh& mesh) {
			if (mesh.removed)
				recreateGeometryBatch = true;
		});

		if (recreateGeometryBatch)
		{
			gps->GetScene().ResetModelCounter();

			gps->GetScene().GetGeometry().GetVertices().Bind();
			gps->GetScene().GetGeometry().GetVertices().ClearFromOffset(gps->GetScene().GetGeometryOffsetVerts() * sizeof(AnimVertex));
			gps->GetScene().GetGeometry().GetVertices().Unbind();

			gps->GetScene().GetGeometry().GetIndices().Bind();
			gps->GetScene().GetGeometry().GetIndices().ClearFromOffset(gps->GetScene().GetGeometryOffsetInds() * sizeof(unsigned int));
			gps->GetScene().GetGeometry().GetIndices().Unbind();

			// ���� ��������� �����-�� ��� (��� ������), �� ��� ����������� ���� ����������������, 
			// �������� � ������ �� ���������� ���������/�������� ���������� ����

			es.each<MeshBatch>([&dt, this](entityx::Entity& entity, MeshBatch& meshBatch) {
				for (auto meshBatchIter = meshBatch.meshes.begin(); meshBatchIter != meshBatch.meshes.end(); )
				{
					if (!meshBatchIter->removed)
					{
						*meshBatchIter = *(gps->CreateElement<Mesh>(meshBatchIter->name, meshBatchIter->ID));
						meshBatchIter++;
					}
					else
						meshBatch.meshes.erase(meshBatchIter);
				}

				if (meshBatch.meshes.size() == 0)
					entity.component<MeshBatch>().remove();
			});

			/*es.each<Weapon>([&dt, this](entityx::Entity& entity, Weapon& weapon)
			{
				if (!weapon.removed)
				{
					gps->CreateElement<Mesh>(weapon.mesh.name, weapon.mesh.ID);
				}
				else
				{
					entity.component<Weapon>().remove();
				}
			});*/

			recreateGeometryBatch = false;
		}

	}
}