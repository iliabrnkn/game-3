#pragma once

#include <glm/glm.hpp>

#include "ComponentManager.h"

namespace mg
{
	struct SpotLight
	{
		float radius;
		glm::vec3 color;
		SceneVector positionOffset;
		bool dynamic;
		bool active;
		float angle;

		SpotLight(const float &angle, const float& radius, const glm::vec3& color, 
			const bool &dynamic, const bool &active, const SceneVector &positionOffset) :
			angle(angle), radius(radius), color(color), dynamic(dynamic), active(true), positionOffset(positionOffset)
		{}
	};

	class SpotLightComponentManager : public ComponentManager
	{
	public:
		SpotLightComponentManager(GameplayState *gps) : ComponentManager("spotLight", gps)
		{}

		~SpotLightComponentManager() {}

		virtual void AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const override;
		virtual LuaPlus::LuaObject ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const override;
		virtual LuaPlus::LuaObject GetComponentTable(entityx::Entity &entity) const override;
	};
}