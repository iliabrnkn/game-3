#include "stdafx.h"
#include <typeinfo>

#include "GUI.h"
#include "WindowConstants.h"
#include "Paths.h"
#include "Widget.h"
#include "Root.h"
#include "Core.h"

using namespace std;

namespace mg {

	const int MAX_CONSOLE_ENTRIES = 30;
	int GUI::nextID;

	GUI::GUI()
		:root(std::move(std::unique_ptr<Widget>(new Root(WidgetParameters(0, 0, sf::Vector2i(0,0), true, false), "root"))))
	{
		font.loadFromFile(FONTS_FOLDER + "font.bdf");
		((sf::Texture&)(font.getTexture(10))).setSmooth(false);

		cursorTex.loadFromFile(GUI_FOLDER + "cursor.png");
		cursorBrightTex.loadFromFile(GUI_FOLDER + "cursor_bright.png");
		sightTex.loadFromFile(GUI_FOLDER + "sight.png");
		targetLockSightTex.loadFromFile(GUI_FOLDER + "sight2.png");
		cursorSprite.setTexture(cursorTex);
		aimCursorSprite.setTexture(sightTex);
		aimCursorSprite.setOrigin(sf::Vector2f(13.f, 13.f));
		targetLockAimSprite.setTexture(targetLockSightTex);
		targetLockAimSprite.setOrigin(sf::Vector2f(13.f, 13.f));
		currentCursorSprite = cursorSprite;

		std::string vertShader =
			"void main()\r\n"
			"{\r\n"
			// transform the vertex position
			"	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;\r\n"

			// transform the texture coordinates
			"	gl_TexCoord[0] = gl_TextureMatrix[0] * gl_MultiTexCoord0;\r\n"

			// forward the vertex color
			"	gl_FrontColor = gl_Color;\r\n"
			"}";

		std::string fragShader =
			"uniform float textureHeight;"
			"uniform float textureWidth;"
			"uniform sampler2D texture;\r\n"
			"\r\n"
			"vec4 bloomGUI(vec4 color, vec2 texCoord)\r\n"
			"{\r\n"
			"	vec2 frameOffset = vec2(1.0 / textureWidth, 1.0 / textureHeight);\r\n"
			"	float gaussian[49] = float[] (0.000036, 0.000363, 0.001446, 0.002291, 0.001446, 0.000363, 0.000036,\r\n"
			"									0.000363, 0.003676, 0.014662, 0.023226, 0.014662, 0.003676, 0.000363,\r\n"
			"									0.001446, 0.014662, 0.058488, 0.092651, 0.058488, 0.014662, 0.001446,\r\n"
			"									0.002291, 0.023226, 0.092651, 0.146768, 0.092651, 0.023226, 0.002291,\r\n"
			"									0.001446, 0.014662, 0.058488, 0.092651, 0.058488, 0.014662, 0.001446,\r\n"
			"									0.000363, 0.003676, 0.014662, 0.023226, 0.014662, 0.003676, 0.000363,\r\n"
			"									0.000036, 0.000363, 0.001446, 0.002291, 0.001446, 0.000363,	0.000036);\r\n"
			"\r\n"
			"	vec4 colorSum = vec4(0.0);\r\n"
			"\r\n"
			"	for (float i = 0.0; i < 7.0; i += 1.0)\r\n"
			"		for (float j = 0.0; j < 7.0; j += 1.0)\r\n"
			"		{\r\n"
			"			vec4 nextColor = texture2D(texture, vec2(texCoord.x + (j - 3.0) * frameOffset.x, texCoord.y + ((i - 3.0) * frameOffset.y)));\r\n"
			"\r\n"
			"			colorSum.rgb += gaussian[int(i) * 7 + int(j)] * (nextColor.rgb);\r\n"
			"		}\r\n"
			"\r\n"
			"	vec4 result = length(color.rgb) > length(colorSum.rgb) ? vec4(color.rgb, 1.0) : vec4(colorSum.rgb, 1.0);\r\n"
			"\r\n"
			"	return result;\r\n"
			"}\r\n"
			"\r\n"
			"void main()\r\n"
			"{\r\n"
			// lookup the pixel in the texture
			"	//vec4 pixel = bloomGUI(texture2D(texture, gl_TexCoord[0].xy), gl_TexCoord[0].xy);\r\n"
			"	\r\n"
			// multiply it by the color
			"	gl_FragColor = gl_Color * texture2D(texture, gl_TexCoord[0].xy);\r\n"
			"}\r\n";

		shader.loadFromMemory(vertShader, fragShader);
	}

	sf::Shader& GUI::GetShader()
	{
		return shader;
	}
	
	void GUI::Watch(const std::string& name, const float& value)
	{
		watchFloatVals[name] = value;
	}

	void GUI::Watch(const std::string& name, const std::string& value)
	{
		watchStrVals[name] = value;
	}

	void GUI::RemoveWatch(const std::string& name)
	{
		watchFloatVals.erase(name);
	}

	GUI::~GUI()
	{
	}

	GUI& GUI::GetInstance()
	{
		static GUI gui;

		return gui;
	}

	bool GUI::IsConsoleOn()
	{
		return isConsoleOn;
	}

	void GUI::SetConsoleOn(const bool& on)
	{
		isConsoleOn = true;
	}

	// �������� ������ ������� � ���������� �����
	void GUI::ConsolePush(const char& symbol)
	{
		if (!(symbol == '`' || symbol == '~'))
		{
			if (symbol != 8 && symbol != '\r')
				consoleBuffer.push_back(symbol);
			else if (consoleBuffer.size() > 0)
				consoleBuffer.pop_back();
		}
		else
		{
			consoleBuffer.clear();
			isConsoleOn = false;
		}
	}

	// ��������� ��� � �������
	void GUI::ConsoleFlush()
	{
		ConsoleLog(consoleBuffer);

		// ��������� ������
		luaState->DoString(consoleBuffer.c_str());

		consoleBuffer.clear();

		isConsoleOn = false;
	}

	void GUI::ConsoleLog(const std::string& entry)
	{
		consoleEntries.push_back(entry);

		if (consoleEntries.size() > 10)
			consoleEntries.pop_front();
	}

	void GUI::Init(sf::RenderWindow& window, LuaPlus::LuaState* luaState)
	{
		this->luaState = luaState;
	}

	void GUI::ClearConsoleLog()
	{
		if (consoleEntries.size() > 100)
		{
			consoleEntries.swap(deque<std::string>());
		}
		else
			consoleEntries.clear();
	}

	void GUI::SetDefaultCursor(const bool& isHighlighted)
	{
		cursorSprite.setTexture(isHighlighted ? cursorBrightTex : cursorTex);
		currentCursorSprite = cursorSprite;
	}

	void GUI::SetAimCursor(const float& factor)
	{
		currentCursorSprite = aimCursorSprite;
	}

	void GUI::SetTargetLockAimCoords(const sf::Vector2f &screenCoords)
	{
		targetLockAimSprite.setPosition(screenCoords);
	}

	void GUI::SetCursorVisibility(const bool &visible)
	{
		isCursorVisible = visible;
	}

	void GUI::ClearRemovedWidgets()
	{
		root->ClearRemoved();
	}

	void GUI::Draw(sf::RenderWindow &window)
	{
		window.pushGLStates();

		DrawConsoleLog(window, shader);
		DrawWatchVals(window, shader);
		
		auto mousePosWindow = Core::GetInstance()->GetControls().GetCurrentMouseScreenCoords();

		// ������ �������
		root->Draw(window, shader);

		// ������ ������
		currentCursorSprite.setPosition(mousePosWindow.x, mousePosWindow.y);

		shader.setUniform("textureHeight", static_cast<float>(cursorSprite.getTexture()->getSize().y));
		shader.setUniform("textureWidth", static_cast<float>(cursorSprite.getTexture()->getSize().x));
		
		if (isCursorVisible)
			window.draw(currentCursorSprite, &shader);
		else
			window.draw(targetLockAimSprite, &shader);

		window.popGLStates();
	}

	void GUI::DrawConsoleLog(sf::RenderWindow &window, sf::Shader &shader)
	{
		// drawing console log

		string consoleLogFull;

		for (auto& entry : consoleEntries)
		{
			consoleLogFull += entry + "\n";
		}

		if (isConsoleOn)
		{
			consoleLogFull += ">" + consoleBuffer;
		}

		sf::Text text;
		text.setFont(font);
		text.setString(consoleLogFull);
		text.setCharacterSize(10);
		text.setFillColor(sf::Color(35, 99, 135, 255));

		shader.setUniform("textureHeight", static_cast<float>(text.getLocalBounds().height));
		shader.setUniform("textureWidth", static_cast<float>(text.getLocalBounds().width));

		window.draw(text, &shader);
	}

	void GUI::DrawWatchVals(sf::RenderWindow &window, sf::Shader &shader)
	{
		string watchFullMsg;

		// floats

		for (auto& entry : watchFloatVals)
		{
			watchFullMsg += entry.first + " : " + to_string(entry.second) + "\n";
		}

		// strings

		for (auto& entry : watchStrVals)
		{
			watchFullMsg += entry.first + " : " + entry.second + "\n";
		}

		sf::Text text;
		text.setFont(font);
		text.setString(watchFullMsg);
		text.setCharacterSize(10);
		text.setFillColor(sf::Color(35, 99, 135, 255));
		
		text.setPosition(RENDERBUFFER_WIDTH - text.getGlobalBounds().width, 0);

		shader.setUniform("textureHeight", static_cast<float>(text.getLocalBounds().height));
		shader.setUniform("textureWidth", static_cast<float>(text.getLocalBounds().width));

		window.draw(text, &shader);
	}

	const Texture& GUI::GetTexture() const
	{
		return texture;
	}

	// ������� ���������� false, ���� ���� �� ��� GUI
	bool GUI::HandleMouse(const sf::Vector2i& mousePosWindow)
	{
		bool res = false;

		root->HandleMouse(mousePosWindow);

		return res;
	}
}