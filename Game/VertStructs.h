#pragma once

#include <glm\glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/vector_angle.hpp>

struct VBOVert
{
};

struct ScreenPoint : public VBOVert
{
	glm::vec3 mPoint;
};

struct TileVertex : public VBOVert
{
	glm::vec3 mSceneVert;
	glm::vec3 mScreenVert;
	glm::vec3 mSceneNormal;
	glm::vec2 mScreenTexCoord;
};

struct WallVertex : public VBOVert
{
	glm::vec3 mSceneVert;
	glm::vec3 mScreenVert;
	glm::vec2 mScreenTexCoord;
	glm::vec2 mScreenVolTexCoord;
	glm::vec2 mScreenTangentNormalMapTexCoord;
};

struct SpriteVertex : public VBOVert
{
	glm::vec3 mScreenVert;
	glm::vec2 mScreenTexCoord;
};

struct AnimVertex : public VBOVert
{
	glm::vec3 mVert;
	glm::vec3 mNormal;
	glm::vec2 mTexCoord;

	glm::vec4 mBoneId;
	glm::vec4 mWeightVal;
};