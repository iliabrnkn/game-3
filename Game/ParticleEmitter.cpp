#include "stdafx.h"
#include <stdlib.h>

#include "ParticleEmitter.h"
#include "GameplayState.h"
#include "VertTypes.h"
#include "LuaHelper.h"
#include "Level.h"

namespace mg
{
	int ParticleEmitterComponentManager::emitterCount = 0;
	
	void ParticleEmitterComponentManager::AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		int maxParticlesCount = LuaHelper::GetFloat(table, "maxParticlesCount");
		float maxLifeTime = LuaHelper::GetFloat(table, "maxLifeTime");
		auto startColor = LuaHelper::GetVec4(LuaHelper::GetTable(table, "startColor"));
		auto endColor = LuaHelper::GetVec4(LuaHelper::GetTable(table, "endColor"));
		auto startParticleSize = LuaHelper::GetFloat(table, "startParticleSize");
		auto endParticleSize = LuaHelper::GetFloat(table, "endParticleSize");
		auto startVelocityFactor = LuaHelper::GetFloat(table, "startVelocityFactor", false, 1.0);
		auto endVelocityFactor = LuaHelper::GetFloat(table, "endVelocityFactor", false, 1.0);
		// ����������� � ������������ ��������� �������� �� ����������
		int minGid = LuaHelper::GetInteger(table, "minGid", false);
		int maxGid = LuaHelper::GetInteger(table, "maxGid", false);
		bool blur = LuaHelper::GetBoolean(table, "blur", false, true);
		bool original = LuaHelper::GetBoolean(table, "original", false, true);
		bool active = LuaHelper::GetBoolean(table, "active", false, true);
		
		//	������� respawnParticleFunc ��������� �� ���� ������� �������� � ������ ���������� ������������ ������
		//	� ���������� ������� ����:
		//{
		//	{ -- �������
		//		pos = {...},
		//		startVelocity = {...},
		//		endVelocity = {...}
		//	},
		//	{ ... }, ...
		//}
		LuaPlus::LuaObject particleRespawnFunc = table.GetByName("particleRespawnFunc");
		assert(particleRespawnFunc.GetRef() != -1 && particleRespawnFunc.IsFunction());

		std::vector<Particle> particles;
		auto particleTileCount = gps->GetLevel()->GetParticleAtlas().GetGidCount();
		glm::vec2 singleParticleTexRatio;

		if (minGid >= 1 && maxGid >= minGid)
		{
			singleParticleTexRatio = 
				gps->GetLevel()->GetParticleAtlas().GetGidTex(minGid)->stRightBottom - 
				gps->GetLevel()->GetParticleAtlas().GetGidTex(minGid)->stLeftTop;

			for (auto i = 0; i < maxParticlesCount; ++i)
			{
				auto gid = minGid;

				gid += std::rand() % (maxGid - minGid + 1);

				auto gidTex = gps->GetLevel()->GetParticleAtlas().GetGidTex(gid);

				particles.push_back(Particle(glm::vec2(gidTex->stLeftBottom.x, gidTex->stLeftBottom.y), emitterCount));
			}
		}
		else
		{
			for (auto i = 0; i < maxParticlesCount; ++i)
				particles.push_back(Particle(emitterCount));
		}

		auto emitterID = emitterCount++;

		entity.assign<ParticleEmitter>(
			particles, maxLifeTime,
			startParticleSize, endParticleSize,
			startVelocityFactor, endVelocityFactor,
			singleParticleTexRatio,
			particleRespawnFunc,
			startColor, endColor,
			emitterID,
			blur, original, active
		);
	}

	LuaPlus::LuaObject ParticleEmitterComponentManager::GetComponentTable(entityx::Entity& entity) const
	{
		// TODO
		return LuaPlus::LuaObject().AssignNil();
	}

	LuaPlus::LuaObject ParticleEmitterComponentManager::ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		// TODO
		return LuaPlus::LuaObject().AssignNil();
	}

	// �������� ������ ���/����
	void ParticleEmitterComponentManager::EnableParticleEmitter(const LuaPlus::LuaObject &entity, const bool &enabled)
	{
		gps->GetEntity(entity).component<ParticleEmitter>()->active = enabled;
	}
}