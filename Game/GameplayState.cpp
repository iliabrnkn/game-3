#include "stdafx.h"
#include "GameplayState.h"

#include <dirent_win.h>

#include <SFML\Graphics.hpp>

#include "Level.h"
#include "RenderingSystem3D.h"
#include "ControlSystem.h"
#include "PathfindingSystem.h"
#include "AnimationSystem.h"
#include "LightSystem.h"
#include "AISystem.h"
#include "Core.h"
#include "Texture.h"
#include "ScriptApiFacade.h"
#include "Paths.h"
#include "Effect.h"
#include "ViewSystem.h"
#include "InventoryWindow.h"
#include "StringHelper.h"
#include "Animation.h"
#include "PhysicsSystem.h"
#include "MeshBatchSystem.h"
#include "ParticlesSystem.h"
#include "ShootingSystem.h"
#include "SpotLightSystem.h"
#include "PropsFactory.h"
#include "DirectionalLightSystem.h"

using namespace std;

namespace mg {

	GameplayState::GameplayState(LuaPlus::LuaState* luaState, std::shared_ptr<Camera> cameraInit)
		:
		camera(cameraInit),
		luaState(luaState),
		OnSceneUpdated(LuaPlus::LuaObject()),
		OnSceneUpdate(LuaPlus::LuaObject()),
		isBakingPass(true),
		timeFactor(1.0)
	{
	}

	GameplayState::~GameplayState()
	{
	}

	void GameplayState::LoadResources(const char *cPath)
	{
		DIR *dir;
		struct dirent *ent;
		std::string path(cPath);

		if ((dir = opendir(path.c_str())) != NULL)
		{
			while ((ent = readdir(dir)) != NULL)
			{
				auto filenameParts = StringHelper::GetFileNameParts(ent->d_name);

				if (filenameParts.back() == "anim")
				{
					auto factoryIter = GetComponentFactory<Animation>();
					factoryIter->LoadFromFile(path + std::string(ent->d_name));
					AnimationBatchComponentManager::names.push_back(filenameParts.front());
				}
				else if (filenameParts.back() == "sklt")
				{
					GetComponentFactory<Skeleton>()->LoadFromFile(path + std::string(ent->d_name));
				}
				else if (filenameParts.back() == "msh")
				{
					GetComponentFactory<Mesh>()->LoadFromFile(path + std::string(ent->d_name));
				}
				else if (filenameParts.back() == "tsv")
				{
					GetComponentFactory<Props>()->LoadFromFile(path + std::string(ent->d_name));
				}
				else continue;

				ErrorLog::Log(std::string(ent->d_name) + " file loaded");
			}
		}
		else
		{
			ErrorLog::Log("Can't open directory: " + path);
		}
	}

	void GameplayState::Init()
	{	
		ScriptApiFacade::Get().gps = this;
		ScriptApiFacade::Get().Init();

		// element factories
		AddComponentFactory<Skeleton, SkeletonFactory>();
		AddComponentFactory<Props, PropsFactory>();
		AddComponentFactory<Animation, AnimationFactory>();
		AddComponentFactory<Mesh, MeshFactory>();

		luaState->DoFile("Data/Scripts/resources.lua");

		// -- SYSTEMS

		// 1 - inner logic
		systems.add<AISystem>(this);
		systems.add<PathfindingSystem>(this);
		systems.add<PhysicsSystem>(this);
		systems.add<AnimationSystem>(this);
		systems.add<LightSystem>(this);
		systems.add<SpotLightSystem>(this);
		systems.add<DirectionalLightSystem>(this);
		systems.add<ViewSystem>(this);

		// 2 - drawing
		systems.add<MeshBatchSystem>(this);
		systems.add<RenderingSystem3D>(this);
		systems.add<ParticlesSystem>(this);
		systems.add<ShootingSystem>(this);

		// -- configure
		systems.configure();

		scene.Init(luaState);

		// -- GUI

		/*���� ��������� TODO: ������*/

		luaState->DoFile("Data/Scripts/inventory.lua");

		inventory = GUI::GetInstance().AddWidget<InventoryWindow>(WidgetParameters(640, 400, sf::Vector2i(40.0, 40.0), false), 
			WIDGET_INVENTORY_NAME);

		// -- main script file
		if (luaState->DoFile((SCRIPTS_FOLDER + "main.lua").c_str()))
		{
			ErrorLog::Log(lua_tostring(LuaState_to_lua_State(luaState), -1));
			assert(0);
		}

		auto ref = luaState->GetGlobal("state").GetRef();

		OnSceneUpdated = luaState->GetGlobal("onSceneUpdated");
		OnSceneUpdate = luaState->GetGlobal("onSceneUpdate");
	}

	LuaPlus::LuaState* GameplayState::GetLuaState() const
	{
		return luaState;
	}

	void GameplayState::Update(float delta, sf::Window *window)
	{
		OnSceneUpdate(delta * timeFactor);

		systems.update_all(delta * timeFactor);

		systems.system<AISystem>()->update(entityManager, eventManager,  delta * timeFactor);
		systems.system<PathfindingSystem>()->update(entityManager, eventManager, delta * timeFactor);
		systems.system<PhysicsSystem>()->update(entityManager, eventManager, delta * timeFactor);
		systems.system<AnimationSystem>()->update(entityManager, eventManager, delta * timeFactor);
		systems.system<LightSystem>()->update(entityManager, eventManager, delta * timeFactor);
		systems.system<SpotLightSystem>()->update(entityManager, eventManager, delta * timeFactor);
		systems.system<DirectionalLightSystem>()->update(entityManager, eventManager, delta * timeFactor);
		systems.system<ViewSystem>()->update(entityManager, eventManager, delta * timeFactor);

		// 2 - drawing
		systems.system<MeshBatchSystem>()->update(entityManager, eventManager, delta * timeFactor);
		systems.system<RenderingSystem3D>()->update(entityManager, eventManager, delta * timeFactor);
		systems.system<ParticlesSystem>()->update(entityManager, eventManager, delta * timeFactor);
		systems.system<ShootingSystem>()->update(entityManager, eventManager, delta * timeFactor);
		
		OnSceneUpdated(delta * timeFactor);
	}

	const std::shared_ptr<Camera> GameplayState::GetCamera() const
	{
		return camera;
	}

	std::shared_ptr<Camera> GameplayState::GetCamera()
	{
		return camera;
	}

	void GameplayState::PrepareDeferredRendering(Effect& fboEffect)
	{
		fboEffect.SetUniform3fDeferred("ambienceLight", level->ambienceLight);
	}

	entityx::Entity GameplayState::GetEntity(const entityx::Entity::Id& id)
	{
		return entityManager.get(id);
	}

	entityx::Entity GameplayState::GetEntity(const LuaPlus::LuaObject &table)
	{
		return GetEntity(table["id"].GetInteger());
	}

	entityx::Entity GameplayState::GetEntity(const unsigned int &id)
	{
		auto res = entityManager.get(entityx::Entity::Id(id, currentVersion));

		return res;
	}

	entityx::Entity GameplayState::GetPlayerEntity()
	{
		return entityManager.get(playerID);
	}

	void GameplayState::SetCurrentVersion(const unsigned int& versionInit)
	{
		currentVersion = versionInit;
	}

	void GameplayState::AssociateGIDWProps(const int &gid, const char *cModelName, const LuaPlus::LuaObject &texOffset,
		const LuaPlus::LuaObject &normalTexOffsetFactor)
	{
		std::string modelName(cModelName);

		if (!level)
		{
			level = make_unique<Level>();
		}

		if (!HasElement<Props>(modelName))
		{
			GetComponentFactory<Props>()->LoadFromFile(TILESETS_GEOMETRY_FOLDER + modelName + ".tsv");
			
			if (!HasElement<Props>(modelName))
			{
				ErrorLog::Log("Can't load " + modelName + ".tsv file");
				assert(false); return;
			}
		}

		auto propsHandler = CreateElement<Props>(modelName);
		propsHandler->texOffset = glm::vec2(LuaHelper::GetVec3(texOffset));
		propsHandler->textureOffsetFactor = glm::vec2(LuaHelper::GetVec3(normalTexOffsetFactor));

		level->AssociateGidWGeometry(gid, propsHandler, this);
	}

	void GameplayState::LoadLevel(const LuaPlus::LuaObject& luaObject)
	{	
		// �������� ������� ���������� ������
		//Texture::ResetTextureUnitCount();
		
		// ��������� ���������, ���� ��� ��� ���� ����������������
		/*if (level)
		{
			level->tilesetAtlas.GetTexture()->Delete();
			level->volumeAtlas.GetTexture()->Delete();
			level->normalAtlas.GetTexture()->Delete();
			level->materialAtlas.GetTexture()->Delete();

			level = make_unique<Level>();
		}

		level = make_unique<Level>();*/

		if (!level)
		{
			level = make_unique<Level>();
		}

		level->Load(luaObject, this);
		Core::GetInstance()->GetWindow().setActive(true);
		level->skinAtlas.InitTexture();

		for (auto &spriteAtlas : level->GetSpriteAtlases())
		{
			if (spriteAtlas.GetTilesetImagesCount() > 0)
				spriteAtlas.InitTexture();
		}

		level->particleAtlas.InitTexture();
		level->cloudAtlas.InitTexture();
		CHECK_GL_ERROR


	}

	Level* GameplayState::GetLevel()
	{
		return level.get();
	}

	bool GameplayState::IsBakingPass() const
	{
		return isBakingPass;
	}

	Physics& GameplayState::GetPhysics()
	{
		return physics;
	}

	void GameplayState::Free()
	{
		entityManager.each<AIPiece>([this](entityx::Entity &entity, AIPiece& aiPiece) {
			entity.destroy();
		});

		physics.dynamicsWorld->removeCollisionObject(scene.GetLevelBody());
		physics.Free();
	}

	void GameplayState::SetTimeFactor(const double &newTimeFactor)
	{
		timeFactor = newTimeFactor;
	}
}