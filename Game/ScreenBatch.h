#pragma once

#include <vector>
#include <string>

#include "RenderBatch.h"
#include "WindowConstants.h"

namespace mg
{
	class ScreenBatch : public ConcreteRenderBatch<FBOVertex>
	{
		friend class RenderingSystem3D;
		friend class MeshFactory;

		int width, height;

		VBO<glm::mat4> mxBuffer;
		VBO<glm::vec4> floatUniformBuffer;
		VBO<glm::vec4> lightPosesBuffer;

		std::string effectFileName;

	public:
		ScreenBatch(const int &widthInitial = WINDOW_WIDTH, const int &heightInitial = WINDOW_HEIGHT) :
			ConcreteRenderBatch<FBOVertex>(),
			width(widthInitial), 
			height(heightInitial) {};

		virtual void Init(LuaPlus::LuaState* luaState) override;
		virtual void Draw() override;
		unsigned int fakeIndex;
	};
}