#include "stdafx.h"

#include <glm\glm.hpp>

#include "ItemGrid.h"
#include "InventoryItem.h"

namespace mg
{
	void InventoryGrid::FreePlace(InventoryItem *item)
	{
		// ������� �������� � �������

		int itemWidth = item->itemTable["width"].ToInteger();
		int itemHeight = item->itemTable["height"].ToInteger();

		// ����������� ������� ��������� ������

		sf::Vector2i lastGridCoords = item->GetLastGridPos();

		for (int i = lastGridCoords.y; i < lastGridCoords.y + itemHeight; ++i)
		{
			for (int j = lastGridCoords.x; j < lastGridCoords.x + itemWidth; ++j)
			{
				cells[i][j]->free = true;
			}
		}

		item->Remove();
	}

	// coords - ���������� ����� ������� ����� �������������� �������� ������������ ������

	bool InventoryGrid::ReplaceItem(InventoryItem* item, const sf::Vector2i& itemScreenPos)
	{
		bool result = true;
		auto absPos = this->GetAbsPos();

		// ���� ������� �� ������� �����

		if (!(itemScreenPos.x >= absPos.x - item->width && itemScreenPos.x < (absPos.x + width + item->width)
			&& itemScreenPos.y >= absPos.y - item->height && itemScreenPos.y < (absPos.y + height + item->height)))
		{
			result = false;
		}
		else
		{
			int itemWidth = item->itemTable["width"].ToInteger();
			int itemHeight = item->itemTable["height"].ToInteger();

			sf::Vector2i gridCoords;

			gridCoords.x = static_cast<int>(max(
				min(glm::round(static_cast<float>(itemScreenPos.x - absPos.x) / static_cast<float>(INVENTORY_CELL_SIDE_LENGTH)),
				static_cast<float>(width)/ static_cast<float>(INVENTORY_CELL_SIDE_LENGTH - static_cast<float>(itemWidth))), 
				0.f));
			gridCoords.y = static_cast<int>(max(
				min(glm::round(static_cast<float>(itemScreenPos.y - absPos.y) / static_cast<float>(INVENTORY_CELL_SIDE_LENGTH)),
				static_cast<float>(height) / static_cast<float>(INVENTORY_CELL_SIDE_LENGTH - static_cast<float>(itemHeight))), 
				0.f));

			// ���������, �������� �� ����������� ���������� ������ �� ����� �����

			for (int i = gridCoords.y; i < gridCoords.y + itemHeight; ++i)
			{
				for (int j = gridCoords.x; j < gridCoords.x + itemWidth; ++j)
				{
					if (!cells[i][j]->free)
					{
						result = false;
						break;
					}
				}
			}

			if(result) // ���������� ������� �� ����� �����, ���� �� ��
			{
				// ��������� ������� � ������, ������ y � x ���� �������� ������� ���� ���������� � ������ ������
				auto newItemWidget = cells[gridCoords.y][gridCoords.x]->AddChild<InventoryItem>(item->Clone<InventoryItem>(item->name));

				for (int i = gridCoords.y; i < gridCoords.y + itemHeight; ++i)
				{
					for (int j = gridCoords.x; j < gridCoords.x + itemWidth; ++j)
					{
						cells[i][j]->free = false;
					}
				}

				newItemWidget->lastScreenPos = cells[gridCoords.y][gridCoords.x]->GetAbsPos();

				newItemWidget->itemTable["gridPos"].SetInteger("x", gridCoords.x);
				newItemWidget->itemTable["gridPos"].SetInteger("y", gridCoords.y);

				cells[gridCoords.y][gridCoords.x]->SetOnTop();
			}
		}

		return result;
	}

	bool ActiveItemsWindow::ReplaceItem(InventoryItem *item, const sf::Vector2i &itemScreenPos)
	{
		bool result = false;

		// ���� ������� � ���� �� ������ ��� ������
		for (auto &activeCellPtr : this->children)
		{
			ActiveItemCell* activeCell;
			
			if (activeCell = dynamic_cast<ActiveItemCell*>(activeCellPtr.get()))
			{
				auto absPos = activeCell->GetAbsPos();

				if (!(itemScreenPos.x >= absPos.x - item->GetSize().x && itemScreenPos.x < (absPos.x + width + item->GetSize().x)
					&& itemScreenPos.y >= absPos.y - item->GetSize().y && itemScreenPos.y < (absPos.y + height + item->GetSize().y)))
				{
					// ��������� ������� � ������ ��� ������
					auto clonedItem = activeCell->AddChild<InventoryItem>(item->Clone<InventoryItem>(item->GetName()));
					clonedItem->lastScreenPos = activeCell->GetAbsPos();
					activeCell->FireEvent("onWeaponChanged", clonedItem->GetTable());

					result = true;

					break;
				}
			}
		}

		return result;
	}

	sf::Vector2i InventoryGrid::GetGridCoords(const sf::Vector2i& screenCoords)
	{
		auto result = sf::Vector2i(
			static_cast<int>(glm::round(static_cast<float>(screenCoords.x - origin.x) / static_cast<float>(INVENTORY_CELL_SIDE_LENGTH))),
			static_cast<int>(glm::round(static_cast<float>(screenCoords.y - origin.y) / static_cast<float>(INVENTORY_CELL_SIDE_LENGTH)))
		);

		return result;
	}

	/*ItemCell *InventoryGrid::GetCell(const sf::Vector2i& screenCoords)
	{
		auto coords = GetGridCoords(screenCoords);

		if (coords.x >= 0 && coords.y >= 0 && cells[coords.x][coords.y])
			return cells[coords.x][coords.y];
		
		return nullptr;
	}*/

	bool InventoryGrid::CheckPlace(const sf::Vector2i& startPoint, const int& itemWidth, const int& itemHeight)
	{
		for (int y = startPoint.y; y < startPoint.y + itemHeight; ++y)
		{
			for (int x = startPoint.x; x < startPoint.x + itemWidth; ++x)
			{
				if (!cells[y][x]->free)
				{
					return false;
				}
			}
		}

		return true;
	}

	void ActiveItemsWindow::FreePlace(InventoryItem *item)
	{
		DrawBox(GUI_COLOR_BRIGHT, GUI_COLOR_DIM, INVENTORY_CORNER_SIDE_LENGTH, 3, 3, GetAbsPos());
	}

	ActiveItemsWindow::ActiveItemsWindow(const WidgetParameters& params, const std::string& name)
		: ItemGrid(params, name, INVENTORY_CELL_SIDE_LENGTH * 3, INVENTORY_CELL_SIDE_LENGTH * 3)
	{
		AddChild<ActiveItemCell>(
			WidgetParameters(INVENTORY_CELL_SIDE_LENGTH * 3, INVENTORY_CELL_SIDE_LENGTH * 3,
			sf::Vector2i(0, 0)), "WeaponCell0");

		AddChild<ActiveItemCell>(
			WidgetParameters(INVENTORY_CELL_SIDE_LENGTH * 3, INVENTORY_CELL_SIDE_LENGTH * 3,
			sf::Vector2i(INVENTORY_CELL_SIDE_LENGTH * 4, 0)), "WeaponCell1");
	}

	/*void ActiveItemsWindow::SetEventHandler(const LuaPlus::LuaObject &eventHandler, const std::string &itemCellName)
	{
		auto activeItemCell = GetChild<ActiveItemCell>(itemCellName);
		assert(activeItemCell);

		activeItemCell->SetEventHandler(eventHandler);
	}*/
}