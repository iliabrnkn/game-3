#pragma once

#include <memory>

#include <glm/glm.hpp>
#include <glm/gtx/vector_angle.hpp>

#include <lua\LuaPlus.h>

#include <btBulletDynamicsCommon.h>

#include "MatrixHelper.h"
#include "LuaHelper.h"

namespace mg {

	class GameplayState;
	class Camera;

	// ������ �����, �������� ��� ������� glm
	class SceneVector
	{
	protected:
		glm::vec3 vec;

	public:

		static SceneVector X_UNIT;
		static SceneVector Y_UNIT;
		static SceneVector Z_UNIT;

		SceneVector() :
			vec(0.f) {}

		~SceneVector() {};

		SceneVector(const float &x, const float &y, const float &z) :
			vec(x, y, z) {}

		SceneVector(const float &val) :
			vec(val) {}

		SceneVector(const float &x, const float &y) :
			vec(x, y, 0.f) {}

		SceneVector(const glm::vec2 &vec) :
			vec(vec.x, vec.y, 0.f) {}

		SceneVector (const glm::vec3 &vec) :
			vec(vec.x, vec.y, vec.z) {}

		SceneVector(const glm::vec4 &vec) :
			vec(vec.x, vec.y, vec.z) {}

		SceneVector(const btVector3 &vec) :
			vec(vec.x(), vec.y(), vec.z()) {}

		inline float operator[] (const int &index) const
		{
			return vec[index];
		}

		inline float& operator[] (const int &index)
		{
			return vec[index];
		}

		inline operator glm::vec2() const
		{
			return glm::vec2(vec);
		}

		inline operator glm::vec3() const
		{
			return vec;
		}

		inline operator glm::vec4() const
		{
			return glm::vec4(vec, 1.f);
		}

		inline operator btVector3() const
		{
			return btVector3(vec.x, vec.y, vec.z);
		}

		inline bool operator== (const SceneVector &vector) const
		{
			return vector.vec == vec;
		}

		inline bool operator!= (const SceneVector &vector) const
		{
			return vector.vec != vec;
		}

		inline SceneVector operator+ (const SceneVector &vector) const
		{
			return vec + vector.vec;
		}

		inline SceneVector operator+= (const SceneVector &vector)
		{
			return vec += vector.vec;
		}

		inline SceneVector operator- (const SceneVector &vector) const
		{
			return vec - vector.vec;
		}

		inline SceneVector operator- () const
		{
			return -vec;
		}

		inline SceneVector operator-= (const SceneVector &vector)
		{
			return vec -= vector.vec;
		}

		inline SceneVector operator* (const float &f) const
		{
			return f * vec;
		}

		inline SceneVector operator*= (const float &f)
		{
			return vec *= f;
		}

		inline SceneVector operator* (const double &d) const
		{
			return float(d) * vec;
		}

		inline SceneVector operator*= (const double &f)
		{
			return vec *= float(f);
		}

		inline SceneVector operator/ (const float &f) const
		{
			return vec / f;
		}

		inline SceneVector operator/ (const double &f) const
		{
			return vec / float(f);
		}

		inline SceneVector Cross(const SceneVector &vector) const
		{
			return glm::cross(vec, vector.vec);
		}

		inline float Dot(const SceneVector &vector) const
		{
			return glm::dot(vec, vector.vec);
		}

		inline SceneVector Normalized() const
		{
			return glm::normalize(vec);
		}

		inline SceneVector Normalize()
		{
			return vec = glm::normalize(vec);
		}

		inline float X() const { return vec.x; }

		inline float &X() { return vec.x; }

		inline float Y() const { return vec.y; }

		inline float &Y() { return vec.y; }

		inline float Z() const { return vec.z; }

		inline float &Z() { return vec.z; }

		inline SceneVector XZ() const { return SceneVector(vec.x, 0.f, vec.z); }
		inline SceneVector XY() const { return SceneVector(vec.x, vec.y, 0.f); }
		inline SceneVector YZ() const { return SceneVector(0.f, vec.y, vec.z); }

		// �����
		inline float Length() { return glm::length(vec); } const

		inline float &SetLength(const float &length) 
		{
			vec = glm::normalize(vec) * length;

			return this->Length();
		}

		// ���� � ������ ��������
		// ����������� �������� - ������.���� (��� �������� ����� ����.������)
		inline float Angle(const SceneVector &vector, const SceneVector &up = SceneVector(0.f, 1.f, 0.f)) const { 
			auto proj = up.Cross(vec).Dot(vector);

			if (proj != 0.f)
				return glm::angle(vec, (glm::vec3)vector) * glm::sign(proj);
			else
				return glm::angle(vec, (glm::vec3)vector);
		}

		// � ����������� ������
		inline glm::vec2 ToScreenCoords(const Camera *camera) const
		{
			return MatrixHelper::SceneToScreen(vec, camera);
		}

		inline glm::vec2 ToScreenCoords(const std::shared_ptr<Camera> camera) const
		{
			return ToScreenCoords(camera.get());
		}

		inline glm::vec2 ToScreenCoords() const
		{
			return MatrixHelper::SceneToScreenAbs(vec);
		}

		// � ����������� �����
		LuaPlus::LuaObject ToMapCoords(LuaPlus::LuaState *luaState) const;

		// �� ��������� �����
		static SceneVector FromMapCoords(const LuaPlus::LuaObject &table, const bool &require = true, 
			const SceneVector &defaultVal = SceneVector(0.f))
		{
			if (table.IsNil())
			{
				assert(!require);
				return defaultVal;
			}

			return MatrixHelper::MapSceneSwap(LuaHelper::GetVec3(table));
		}

		static SceneVector FromMapCoords(const float &x, const float &y, const float &z)
		{
			return MatrixHelper::MapSceneSwap(SceneVector(x, y, z));
		}

		static SceneVector FromMapCoords(const float &x, const float &y)
		{
			return MatrixHelper::MapSceneSwap(SceneVector(x, y));
		}

		static SceneVector FromScreenCoords(const glm::vec2 &screenCoords, const std::shared_ptr<Camera> camera);
		static SceneVector FromScreenCoords(const glm::vec2 &screenCoords, const Camera *camera);
		
		inline static SceneVector FromScreenCoords(const glm::vec2 &screenCoords)
		{
			return MatrixHelper::ScreenToSceneAbs(screenCoords);
		}

		inline static SceneVector Mix(const SceneVector &x, const SceneVector &y, const float &a)
		{
			glm::vec3 result = glm::mix(x.vec, y.vec, a);

			return SceneVector(result);
		}

		inline static SceneVector Slerp(const SceneVector &x, const SceneVector &y, const float &a)
		{
			glm::vec3 result = glm::slerp(x.vec, y.vec, a);

			return SceneVector(result);
		}

		inline bool IsValid()
		{
			return !IsInf() && !IsNan();
		}

		inline bool IsInf()
		{
			return glm::isinf<float>(vec.x) || glm::isinf<float>(vec.y) || glm::isinf<float>(vec.z);
		}

		inline bool IsNan()
		{
			return glm::isnan<float>(vec.x) || glm::isnan<float>(vec.y) || glm::isnan<float>(vec.z);
		}

		inline SceneVector Rotate(const btQuaternion &quat)
		{
			return MatrixHelper::BTQuatToMat(quat) * glm::vec4(vec.x, vec.y, vec.z, 0.f);
		}
	};
}