#include "stdafx.h"
#include "Batch3D.h"
#include "Paths.h"
#include "OGLConstants.h"
#include "Model.h"
#include "Sizes.h"

namespace mg
{
	void GeometryBatch::Init(LuaPlus::LuaState* luaState)
	{
		glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);

		// initialize buffers

		indices->Init(BUFFER_SIZE_8MB * 4, GL_ELEMENT_ARRAY_BUFFER);
		vertices->Init(BUFFER_SIZE_32MB * 4, GL_ARRAY_BUFFER);

		floatUniformBuffer.Init(MAX_MESHES_COUNT * sizeof(glm::vec4), // depth
			GL_UNIFORM_BUFFER
		);

		// initialize VAO

		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		vertices->Bind();
		indices->Bind();

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(AnimVertex), 0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(AnimVertex), BUFFER_OFFSET(12));
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(AnimVertex), BUFFER_OFFSET(24));
		glEnableVertexAttribArray(3);
		glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(AnimVertex), BUFFER_OFFSET(32));
		glEnableVertexAttribArray(4);
		glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(AnimVertex), BUFFER_OFFSET(48));
		glEnableVertexAttribArray(5);
		glVertexAttribIPointer(5, 1, GL_INT, sizeof(AnimVertex), BUFFER_OFFSET(64));

		glBindVertexArray(NULL);
		vertices->Unbind();
		indices->Unbind();

		CHECK_GL_ERROR
	}

	void GeometryBatch::Draw()
	{
		glDrawElementsBaseVertex(GL_TRIANGLES, indices->GetCurrentOffset(), GL_UNSIGNED_INT, BUFFER_OFFSET(0), 0);

		CHECK_GL_ERROR
	}

	void GeometryBatch::Draw(const GLint &startElement, const GLint &elementsCount)
	{
		glDrawElementsBaseVertex(GL_TRIANGLES, elementsCount, GL_UNSIGNED_INT, BUFFER_OFFSET(sizeof(GLint) * startElement), 0);

		CHECK_GL_ERROR
	}

	void GeometryBatch::DrawInstanced(const int& instanceCount)
	{
		glDrawElementsInstanced(GL_TRIANGLES, indices->GetCurrentOffset(), GL_UNSIGNED_INT, BUFFER_OFFSET(0), instanceCount);

		CHECK_GL_ERROR
	}

	void GeometryBatch::DrawInstanced(const int& instanceCount, const GLint &startElement, const GLint &elementsCount)
	{
		glDrawElementsInstanced(GL_TRIANGLES, elementsCount, GL_UNSIGNED_INT, BUFFER_OFFSET(sizeof(GLint) * startElement), instanceCount);

		CHECK_GL_ERROR
	}
}