#pragma once

#include "stdafx.h"

#include <windows.h>
#include <GL/glew/glew.h>
#include <GL/GL.h>

#include <SFML\Graphics.hpp>

namespace mg
{
	using namespace std;

	class Texture
	{
	public:
		Texture(void);
		~Texture(void);
		void Init(const unsigned int& widthInitial, const unsigned int& heightInitial, const GLint& pixelFormatInitial,
			const GLint& filterInitial, const void* pixelsPtrInitial = NULL, const GLenum& pixelTypeInitial = GL_UNSIGNED_BYTE, const GLenum& pixelInternalFormatInitial = GL_RGBA,
			const GLenum& targetInitial = GL_TEXTURE_2D, const unsigned int& depthInitial = 0);
		void Init(const sf::Image& image, const GLenum& pixelType = GL_UNSIGNED_BYTE);
		void Init1D(const unsigned int& widthInitial);
		void Init2DArray(const GLuint &widthInitial, const GLuint &heightInitial, const GLuint &depthInitial, const GLubyte *images = nullptr);
		void InitSubImage(const GLint &xOffset, const GLint &yOffset, const GLint &zOffset,
			const GLsizei &width, const GLsizei &height, const GLsizei depth,
			const GLubyte *subImage);

		void Rebind() const;
		void Unbind() const;
		void Redraw(void* pixelsPtr);

		const int& GetWidth() const;
		const int& GetHeight() const;

		GLuint GetIndex() const;
		GLuint GetTextureUnit() const;
		void InitBuffer();
		void BindBuffer(const GLuint& vboID, const GLenum& internalFormat);

		void SetComparisonMode(GLint compareFunction);

		static void ResetTextureUnitCount();
		static int GetTexUnitCounter();
 		void Delete(); // освобождает память

		inline static size_t GetMaxTextureSize()
		{
			GLint size;
			glGetIntegerv(GL_MAX_TEXTURE_SIZE, &size);

			return glm::pow(size, 2);
		}

		inline static size_t GetMaxArrayTextureLayers()
		{
			GLint size;
			glGetIntegerv(GL_MAX_ARRAY_TEXTURE_LAYERS, &size);

			return size;
		}

	private:
		GLenum target;
		GLuint textureID;
		GLint pixelFormat;

		unsigned int width;
		unsigned int height;
		unsigned int depth;

		static int texUnitsCounter;
		mutable int textureUnit;
	};
}

