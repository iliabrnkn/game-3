#include "stdafx.h"

#include "AutoWalkController.h"
#include "GameplayState.h"

namespace mg
{
	FootOnGround AutoWalkController::ChangeLegs()
	{
		if (currFootOnTheGround == FootOnGround::Left)
			return currFootOnTheGround = FootOnGround::Right;
		else if (currFootOnTheGround == FootOnGround::Right)
			return currFootOnTheGround = FootOnGround::Left;
		else
			return currFootOnTheGround = FootOnGround::None;
	}

	// ---- AutoWalkControllerComponentManager

	void AutoWalkControllerComponentManager::AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		entity.assign<AutoWalkController>();
	}
	
	LuaPlus::LuaObject AutoWalkControllerComponentManager::ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		return LuaPlus::LuaObject();
	}
	
	LuaPlus::LuaObject AutoWalkControllerComponentManager::GetComponentTable(entityx::Entity& entity) const
	{
		return LuaPlus::LuaObject();
	}

	void AutoWalkControllerComponentManager::AutoWalk(const LuaPlus::LuaObject &entityTable, const LuaPlus::LuaObject &direction) const
	{
		auto entity = gps->GetEntity(entityTable);
		auto autoWalkController = entity.component<AutoWalkController>();
		
		autoWalkController->isActive = true;
		autoWalkController->walkDirection = SceneVector::FromMapCoords(direction);
		autoWalkController->currFootOnTheGround = FootOnGround::None;
	}

	void AutoWalkControllerComponentManager::SetAutoWalkActive(const LuaPlus::LuaObject &entityTable, const bool &active) const
	{
		auto entity = gps->GetEntity(entityTable);
		auto autoWalkController = entity.component<AutoWalkController>();

		autoWalkController->isActive = active;
	}
}