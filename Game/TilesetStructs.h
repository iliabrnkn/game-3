#pragma once

#include "stdafx.h"

#include <glm/glm.hpp>

#include <SFML\Graphics.hpp>

#include <memory>

using namespace std;

namespace mg {

	enum class GidOrient {
		neither = 0,
		alongX = 1,
		alongY = 2
	};

	struct GidTex
	{
		int gidNum;
		glm::vec2 stLeftTop;
		glm::vec2 stLeftBottom;
		glm::vec2 stRightBottom;
		glm::vec2 stRightTop;
		glm::vec2 stUnrelatedLeftTop;
		glm::vec2 stUnrelatedLeftBottom;
		glm::vec2 stUnrelatedRightBottom;
		glm::vec2 stUnrelatedRightTop;
		glm::vec2 drawingOffset;

		int width;
		int height;

		int layerWidth;
		int layerHeight;

		int texLayerNum;

		GidTex(void)
		{
		}
	};

	struct TilesetImage
	{
		int firstGid;
		int tileWidth;
		int tileHeight;
		int tileCount;

		int singleTileYOffset;

		unsigned int width;
		unsigned int height;
		std::vector<GidTex*> gids;

		sf::Image image;

		TilesetImage()
		{

		}

		TilesetImage(const string& filename)
		{
			image.loadFromFile(filename);

			width = image.getSize().x;
			height = image.getSize().y;
		}
	};

}