#pragma once

#include <string>
#include <map>

namespace mg
{
	const int COL_NONE =		0;
	const int COL_ALL =			-1;
	const char COL_WALLS =		0B00000001;
	const char COL_CHARACTERS = 0B00000010;
	const char COL_WEAPONS =	0B00000100;
	const char COL_CHAR_BOXES = 0B00001000;

	const std::map<std::string, int> COLLISION_GROUPS = {
		std::make_pair("none", COL_NONE),
		std::make_pair("everything", COL_ALL),
		std::make_pair("level", COL_WALLS),
		std::make_pair("character_boxes", COL_CHAR_BOXES),
		std::make_pair("weapons", COL_WEAPONS),
		std::make_pair("character_joints", COL_CHARACTERS)
	};

	const std::map<std::string, int> COLLISION_MASKS = {
		std::make_pair("none", COL_NONE),
		std::make_pair("everything", COL_ALL),
		std::make_pair("level", COL_ALL),
		std::make_pair("character_boxes", COL_WALLS),
		std::make_pair("weapons", /*COL_CHARACTERS |*/ COL_WEAPONS | COL_WALLS),
		std::make_pair("character_joints", COL_CHARACTERS | COL_WEAPONS | COL_WALLS)
	};
}