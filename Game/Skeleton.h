#pragma once

#include <map>
#include <vector>
#include <deque>
#include <string>
#include <unordered_map>
#include <algorithm>
#include <glm/gtx/quaternion.hpp>

#include "bullet/btBulletDynamicsCommon.h"

#include "Animation.h"
#include "ModelStruct.h"
#include "Cloneable.h"
#include "GUI.h"
#include "ComponentManager.h"
#include "Vector.h"

namespace mg {

	const double AFFECTION_SPAN = 0.0;
	const double AFFECTION_RECOVERY_SPAN = 5.0;

	const std::string WEAPON_NODE_NAME = "weapon_r";
	const std::string PELVIS_NODE_NAME = "pelvis";
	const std::string SPINE_01_NODE_NAME = "spine_01";
	const std::string SPINE_02_NODE_NAME = "spine_02";
	const std::string HEAD_NODE_NAME = "head";
	const std::string HEAD_ROTATION_NODE_NAME = "head_$AssimpFbx$_Rotation";

	struct BoneBody
	{
		std::string firstJoint;
		std::string secondJoint;
		btTransform startTransform;
		SceneVector axisOffset;
		RigidBody *body;
		std::vector<std::string> ignoredCollisionBones;
		bool invertedAxis;
		bool constraintDebug;
		
		// -------
		bool animatedAbsolute; // ���� ���� ������������� � �����-�� ��� ��������� ������ ��������, ������������� true
		float inertiaDamping;
		
		// ------

		//TODO: delete
		btTransform animationTransformCurr;
		btTransform animationTransformPrev;
		btQuaternion additionalRotation; // �������������� ���� �������� ������ ����������� ���� ����
		SceneVector straightPosition;
		bool affectable;
		bool isConstraintBusy; // ����� ��� ��� ��������� ����������, ����� ��� ���������� �� ��������

		BoneBody(const std::string &firstBone, const std::string &secondBone, RigidBody *body, const btTransform &startTransform,
			SceneVector axisOffset, const std::vector<std::string> &ignoredCollisionBodies, const bool &invertedAxis, const bool &affectable = false) :
			firstJoint(firstBone), secondJoint(secondBone), body(body), startTransform(startTransform), axisOffset(axisOffset), 
			ignoredCollisionBones(ignoredCollisionBodies), invertedAxis(invertedAxis), constraintDebug(false), animatedAbsolute(true),
			additionalRotation(btQuaternion::getIdentity()), affectable(affectable), isConstraintBusy(false), straightPosition(0.f),
			inertiaDamping(1.f), animationTransformPrev(btTransform::getIdentity()), animationTransformCurr(btTransform::getIdentity())
		{
			body->setUserPointer(reinterpret_cast<void*>(this));
		}

		~BoneBody()
		{}

		inline SceneVector GetFirstJointPos() const
		{
			return body->getWorldTransform().getOrigin();
		}

		inline SceneVector GetMiddlePos() const
		{
			return body->getWorldTransform().getOrigin() +
				body->getWorldTransform().getBasis() * axisOffset;
		}

		inline SceneVector GetSecondJointPos() const
		{
			return body->getWorldTransform().getOrigin() +
				body->getWorldTransform().getBasis() * 2.0 * axisOffset;
		}

		inline btMatrix3x3 GetBasis() const
		{
			return body->getWorldTransform().getBasis();
		}
	};

	struct Skeleton : public Cloneable
	{
	public:

		// �� ������� ��������� ����� �������������� ���� ��� ���������� ���������
		std::unordered_map<std::string, float> orientAngleComponents;
		float locomotorActivity;
		std::vector<btTypedConstraint*> constraints;
		bool dead;

		std::unordered_map<string, BoneNode> boneNodes;
		glm::mat4x4 rootNodeInverseTransform;

		std::unordered_map<std::string, SceneVector> boneHeadBindingPoints;
		std::unordered_map<std::string, BoneBody> boneBodies;
		btTransform animationTransformBefore;
		float recoveryFactor;

		void ResetJointTransforms();
		
		Skeleton() : locomotorActivity(1.f) {}

		inline void ResetNodes()
		{
			for (auto& pair : boneNodes)
			{
				pair.second.currentAnimKey = AnimKey();
				pair.second.�urrentTransform = pair.second.basicTransform;
				pair.second.transformedByChannel = false;
			}
		}

		void Update(const bool &alignToRigidBodies = false)
		{
			ResetJointTransforms();
			UpdateNode(ROOT_JOINT, glm::mat4(1.f), alignToRigidBodies);
		}

		// ���������� ��� ����� ������ �� ��������, ������� � ���������� �����
		void GetNodeNamesFrom(const std::string &boneName, std::set<std::string>& result) const
		{
			auto iter = boneNodes.find(boneName);

			if (iter != boneNodes.end())
			{
				result.insert(boneName);

				for (auto &childName : iter->second.childNames)
				{
					GetNodeNamesFrom(childName, result);
				}
			}
		}

		glm::mat4 GetJointTransformFromBoneBodyTransform(const BoneBody &boneBody);

		inline BoneBody *FindBoneBodyByRigidBody(const btRigidBody *rigidBody)
		{
			auto iter = std::find_if(boneBodies.begin(), boneBodies.end(), [rigidBody](auto &p) { 
				return rigidBody == p.second.body; 
			});

			if (iter != boneBodies.end())
				return &(iter->second);
			else
				return nullptr;
		}

		inline BoneBody *FindBoneBodyBySecondJoint(const std::string &secondJoint)
		{
			auto iter = std::find_if(boneBodies.begin(), boneBodies.end(), [&secondJoint](auto &pair) {
				return secondJoint == pair.second.secondJoint;
			});

			if (iter == boneBodies.end())
				return nullptr;
			else
				return &(iter->second);
		}

		inline BoneBody *FindBoneBodyByFirstJoint(const std::string &firstJoint)
		{
			auto iter = std::find_if(boneBodies.begin(), boneBodies.end(), [&firstJoint](auto &pair) {
				return firstJoint == pair.second.firstJoint;
			});

			if (iter == boneBodies.end())
				return nullptr;
			else
				return &(iter->second);
		}

		inline BoneBody *GetBoneBodyParent(const BoneBody &boneBody)
		{
			return FindBoneBodyByRigidBody(&(boneBody.body->getConstraintRef(0)->getRigidBodyA()));
		}

		std::vector<BoneBody*> GetBoneBodyAncestors(const BoneBody &boneBody);

		/*inline void isConstraintsActive(const bool &active)
		{
			for (auto &bb : boneBodies)
			{
				if (bb.second.body->getNumConstraintRefs() > 0 && !bb.second.isConstraintBusy)
				{
					bb.second.body->getConstraintRef(0)->setEnabled(active);
				}
			}
		}*/

	private:

		void UpdateNode(const std::string& nodeName, glm::mat4& parentTransform, const bool &alignToRigidBody = false);
	};

	class SkeletonComponentManager : public ComponentManager
	{
	public:
		SkeletonComponentManager(GameplayState *gps) : ComponentManager("skeleton", gps) {};
		~SkeletonComponentManager() = default;
		
		virtual void AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const override;
		virtual LuaPlus::LuaObject ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const override;
		virtual LuaPlus::LuaObject GetComponentTable(entityx::Entity& entity) const override;
		
		// ���������� ���������� ������ �������� ���� ����� ������� ��������
		int SkeletonComponentManager::GetBoneBodyWorldArrayIndex(const LuaPlus::LuaObject &entityTable, const LuaPlus::LuaObject &boneName);

		// ����������� ���� ���, ����� ��� z ��������� �� �����
		void OrientBoneBodyToPoint(const LuaPlus::LuaObject &entityTable, const char *boneBodyName, const float &modifier,
			const LuaPlus::LuaObject &vector);

		// �������������� ������������� �����
		void SetBoneBodyPos(const LuaPlus::LuaObject &entity, const char *boneBodyName, const LuaPlus::LuaObject &offset);

		// ��������� ����������� ���������� ������ ������
		void EnableBoneBodyAnimation(const LuaPlus::LuaObject &entity, const LuaPlus::LuaObject &boneBodiesList, const bool &enable);

		// ���������� ��������� �������� �������� / ������� ��� ���������� ����
		void SetBoneOrientationRatio(const LuaPlus::LuaObject &entityTable, const float &bodyComponent, const float &headComponent);

		// ���������� �����
		LuaPlus::LuaObject GetBoneBodyPos(const LuaPlus::LuaObject &entity, const char* boneBodyName);

		// ��������� ���������� ����� ����� ������� ����
		void AddBoneBodyConstraint(const LuaPlus::LuaObject &entity, const char* boneBodyNameA, const char* boneBodyNameB,
			const LuaPlus::LuaObject &constraintInfo);

		// ������� ����������
		void RemoveBoneBodyConstraint(const LuaPlus::LuaObject &entity, const char* boneBodyNameB, const int& constraintNum);

		// ���������/���������� ����������� ����� ����
		void EnableBoneBodyConstraint(const LuaPlus::LuaObject &entity, const char *boneBodyName, const int &constraintNum, const bool &enabled);

		// "�����������" ����� �������
		void LooseBoneBodies(const LuaPlus::LuaObject &entity, const float &recoveryTime);

		void SetBodiesInertiaDamping(const LuaPlus::LuaObject &entity,
			const float &inertiaDamping, const LuaPlus::LuaObject &bodyList);

		// ���������� ����� ������������ � ����� ����
		int GetNumConstraints(const LuaPlus::LuaObject &entity, const char* boneBody);

		// ���� �� � �������� ������
		bool HasSkeleton(const LuaPlus::LuaObject &entity);

		// �����������
		void SetDead(const LuaPlus::LuaObject &entity, const bool &isDead);
	};
}