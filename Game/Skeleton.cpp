#include "stdafx.h"

#include <fstream>

#include "Skeleton.h"
#include "GameplayState.h"
#include "LuaHelper.h"
#include "Position.h"
#include "Direction.h"
#include "Moveable.h"

namespace mg
{
	glm::mat4 Skeleton::GetJointTransformFromBoneBodyTransform(const BoneBody &boneBody)
	{
		btTransform bodyTransform = boneBody.body->getWorldTransform();

		// 3) ���������� �������
		bodyTransform.getOrigin() /= MODEL_SCALE;

		// 4) ��������� �� �������� rootInverseTransform 
		bodyTransform = MatrixHelper::ToBTTransfom(rootNodeInverseTransform) * bodyTransform;

		return  MatrixHelper::ToGLMMatrix(bodyTransform);
	}

	void Skeleton::ResetJointTransforms()
	{
		for (auto &node : boneNodes)
		{
			if (node.second.transformedByChannel)
			{
				node.second.�urrentTransform = node.second.channelTransform;
			}
			else
			{
				node.second.�urrentTransform = node.second.basicTransform;
			}
		}
	}

	void Skeleton::UpdateNode(const std::string& nodeName, glm::mat4& parentTransform, const bool &alignToRigidBody)
	{
		auto& boneNode = boneNodes[nodeName];

		if (!alignToRigidBody)
		{
			boneNode.�urrentTransform = parentTransform * boneNode.�urrentTransform;
		}
		else
		{
			auto iter = boneBodies.find(nodeName);

			if (iter == boneBodies.end())
			{
				boneNode.�urrentTransform = parentTransform * boneNode.�urrentTransform;
			}
			else
			{
				boneNode.�urrentTransform = GetJointTransformFromBoneBodyTransform(iter->second);
			}
		}

		// ���� � �������� ���� ���� ����������
		for (auto& childName : boneNode.childNames)
		{
			UpdateNode(childName, boneNode.�urrentTransform, alignToRigidBody);
		}
	}

	std::vector<BoneBody*> Skeleton::GetBoneBodyAncestors(const BoneBody &boneBody)
	{
		std::vector<BoneBody*> res;


		for (auto &pair : boneBodies)
		{
			if (pair.second.body->getNumConstraintRefs() > 0 &&
				pair.second.body->getConstraintRef(0)->getRigidBodyA().getWorldArrayIndex() == boneBody.body->getWorldArrayIndex())
			{
				GUI::GetInstance().ConsoleLog(pair.first);
				res.push_back(&(pair.second));
			}
		}

		return res;
	}

	//--- SkeletonComponentManager
	void SkeletonComponentManager::AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		if (!gps->HasElement<Skeleton>(table.GetByName("name").ToString()))
			return;

		auto skeleton = Skeleton(*gps->CreateElement<Skeleton>(table.GetByName("name").ToString()));
		skeleton.locomotorActivity = 1.f;
		skeleton.dead = false;

		// ����������� ���������� ������ (��������������� �-����)

		std::unordered_map<std::string, SceneVector> boneMainAxes;
		auto boneBindingPointsTable = LuaHelper::GetTable(table, "boneBindingPoints");

		for (LuaPlus::LuaTableIterator iter(boneBindingPointsTable); iter; iter.Next())
		{
			auto currTable = iter.GetValue();

			std::string jointName = iter.GetKey().ToString();
			SceneVector head(LuaHelper::GetVec3(LuaHelper::GetTable(currTable, "head")));
			skeleton.boneHeadBindingPoints.insert(std::make_pair(jointName, head));

			SceneVector tail(LuaHelper::GetVec3(LuaHelper::GetTable(currTable, "tail")));
				
			SceneVector newBasisY = (tail - head).Normalized();
			SceneVector newBasisX = newBasisY.Cross(SceneVector(0.f, 1.f, 0.f));

			boneMainAxes.insert(std::make_pair(jointName, newBasisY));
		}

		// ���� ������

		auto boneRigidBodiesTable = LuaHelper::GetTable(table, "boneRigidBodies");

		for (LuaPlus::LuaTableIterator iter(boneRigidBodiesTable); iter; iter.Next())
		{
			auto& currTable = iter.GetValue();
			auto firstJoint = LuaHelper::GetString(currTable, "firstJoint");
			auto secondJoint = LuaHelper::GetString(currTable, "secondJoint", false, "");
			btCollisionShape *shape = LuaHelper::GetCollisionShape(LuaHelper::GetTable(currTable, "shape"));
			SceneVector newAxisX, newAxisY, newAxisZ;

			if (secondJoint != "foot_tip_l" && secondJoint != "foot_tip_r")
				shape->setMargin(0.06f);

			if (secondJoint != "")
				newAxisY = (skeleton.boneHeadBindingPoints[secondJoint] - skeleton.boneHeadBindingPoints[firstJoint]).Normalized();
			else
				newAxisY = SceneVector(0.f, 1.f, 0.f);

			if (newAxisY.Y() != 1.f)
			{
				newAxisX = newAxisY.Cross(SceneVector(0.0, 1.0, 0.0)).Normalized();
				newAxisZ = newAxisX.Cross(newAxisY).Normalized();
			}
			else
			{
				newAxisZ = SceneVector(0.f, 0.f, 1.f);
				newAxisX = SceneVector(1.f, 0.f, 0.f);
			}
			
			// ����� � ����������� ���� �����
			SceneVector axisOffset(0.f);
			float mass = LuaHelper::GetFloat(currTable, "mass");

			bool invertedAxis = LuaHelper::GetBoolean(currTable, "invertedAxis", false);
			auto axisOffsetTable = LuaHelper::GetTable(currTable, "axisOffset", false);
			if (!axisOffsetTable.IsNil())
			{
				axisOffset = LuaHelper::GetBTVec3(axisOffsetTable);
			}
			auto origin = skeleton.boneHeadBindingPoints.at(firstJoint);

			btTransform startTransformRotation = btTransform::getIdentity();

			if (invertedAxis)
			{
				axisOffset = -axisOffset;
				startTransformRotation.setFromOpenGLMatrix(glm::value_ptr(
					glm::rotate<double>(glm::pi<double>(), glm::dvec3(1.0, 0.0, 0.0))));
			}

			auto *compoundShape = new btCompoundShape();
			compoundShape->addChildShape(btTransform(btMatrix3x3::getIdentity(), axisOffset), shape);
			compoundShape->setLocalScaling(btVector3(MODEL_SCALE, MODEL_SCALE, MODEL_SCALE));

			std::vector<std::string> ignoredCollisionBones;

			// ������������� ������������ �� ����� �������� ������ � �������
			if (LuaHelper::GetBoolean(currTable, "ignoreCollisionWithAllBones", false))
			{
				for (auto &pair : skeleton.boneHeadBindingPoints)
				{
					ignoredCollisionBones.push_back(pair.first);
				}
			}
			else
			{
				LuaPlus::LuaObject ignoredCollisionBonesTable;

				// ������������� ������������ ���� � �������������� ������
				if (!(ignoredCollisionBonesTable = LuaHelper::GetTable(currTable, "ignoredCollisionBones", false)).IsNil())
				{
					for (LuaPlus::LuaTableIterator bonesIter(ignoredCollisionBonesTable); bonesIter; bonesIter.Next())
					{
						ignoredCollisionBones.push_back(bonesIter.GetValue().GetString());
					}
				}
			}

			axisOffset *= MODEL_SCALE;

			btTransform startTransform;
			startTransform.setBasis(
				btMatrix3x3(
					newAxisX.X(), newAxisY.X(), newAxisZ.X(),
					newAxisX.Y(), newAxisY.Y(), newAxisZ.Y(),
					newAxisX.Z(), newAxisY.Z(), newAxisZ.Z()));

			startTransform.setOrigin(origin * MODEL_SCALE);
			startTransform = startTransform * startTransformRotation *
				btTransform(btQuaternion::getIdentity());


			int additionalFlags = ~(btCollisionObject::CF_STATIC_OBJECT | btCollisionObject::CF_KINEMATIC_OBJECT);
			
			std::string collisionGroup = LuaHelper::GetString(currTable, "collisionGroup", false, "character_joints");
			std::string collisionMask = LuaHelper::GetString(currTable, "collisionMask", false, "character_joints");

			auto *body = gps->GetPhysics().CreateRigidBody(mass, startTransform, compoundShape, SceneVector(0), 
				-1, COLLISION_GROUPS.at(collisionGroup), COLLISION_MASKS.at(collisionMask));

			body->setCenterOfMassTransform(btTransform(btMatrix3x3::getIdentity(), axisOffset));
			body->setUserIndex(static_cast<int>(entity.id().index()));
			body->setWorldTransform(startTransform);
			// ��������� �������� ����
			LuaPlus::LuaObject dampingTable = LuaHelper::GetTable(currTable, "damping", false);

			if (!dampingTable.IsNil())
			{
				body->setDamping(LuaHelper::GetFloat(dampingTable, "linear", false, 0.05f), 
					LuaHelper::GetFloat(dampingTable, "angular", false, 1.05f));
			}
			else
			{
				body->setDamping(0.05f, 1.05f);
			}

			body->setDeactivationTime(LuaHelper::GetFloat(currTable, "deactivationTime", false, 1.8f));

			LuaPlus::LuaObject sleepingThresholdTable = LuaHelper::GetTable(currTable, "sleepingThreshold", false);

			if (!sleepingThresholdTable.IsNil())
			{
				body->setSleepingThresholds(LuaHelper::GetFloat(sleepingThresholdTable, "lin", false, 1.6f),
					LuaHelper::GetFloat(sleepingThresholdTable, "ang", false, 2.5f));
			}
			else
			{
				body->setSleepingThresholds(1.6f, 2.5f);
				//body->setSleepingThresholds(100.6f, 2.5f);
			}

			//body->setContactStiffnessAndDamping(1000.0, 1000.0);

			// ��������� �� ���� �� �����
			bool isCurrBodyAffectable = LuaHelper::GetBoolean(currTable, "affectable", false, false);

			// ���������� ���������� � ���� ����� ��� ���
			if (LuaHelper::GetBoolean(currTable, "debug", false))
			{
				body->setCustomDebugColor(btVector3(0.0, 0.0, 1.0));
			}
			else
			{
				body->setCustomDebugColor(btVector3(0.0, 0.0, 0.0));
			}

			body->setGravity(SceneVector(0.f));

			skeleton.boneBodies.insert(
				std::make_pair(iter.GetKey().GetString(), 
				std::move(BoneBody(firstJoint, secondJoint, body, startTransform, axisOffset, ignoredCollisionBones, invertedAxis, 
					isCurrBodyAffectable)))
			);
		}

		// ����������� ������������ ������������
		for (auto &bb : skeleton.boneBodies)
		{
			for (auto &ignoredCollisionBoneName : bb.second.ignoredCollisionBones)
			{
				auto iter = skeleton.boneBodies.find(ignoredCollisionBoneName);
					
				if (iter != skeleton.boneBodies.end()) // ���� ����� 
				{
					if (&ignoredCollisionBoneName == &bb.first)
						continue;

					bb.second.body->setIgnoreCollisionCheck(iter->second.body, true);
					iter->second.body->setIgnoreCollisionCheck(bb.second.body, true);
				}
			}
		}

		// ��� ����������� - 6DOF
		LuaPlus::LuaObject constraintsTable = LuaHelper::GetTable(table, "constraints", false);

		if (!constraintsTable.IsNil())
		{
			for (LuaPlus::LuaTableIterator iter(constraintsTable); iter; iter.Next())
			{
				auto currTable = iter.GetValue();

				std::string firstBoneName = LuaHelper::GetString(currTable, "firstBody");
				std::string secondBodyName = LuaHelper::GetString(currTable, "secondBody");

				auto firstBodyIter = skeleton.boneBodies.find(firstBoneName);
				auto secondBodyIter = skeleton.boneBodies.find(secondBodyName);

				assert(firstBodyIter != skeleton.boneBodies.end() && secondBodyIter != skeleton.boneBodies.end());

				/*
				// ����������� ���� t-����

				glm::dmat4 firstT, secT;

				firstBodyIter->second.startTransform.getOpenGLMatrix(glm::value_ptr(firstT));
				secondBodyIter->second.startTransform.getOpenGLMatrix(glm::value_ptr(secT));

				glm::dmat3 firstBasis(firstT), secBasis(secT);

				//----------------
				
				// todo - ������� ��������� �������
				auto basisA = firstBodyIter->second.startTransform.getBasis().inverse();
				btMatrix3x3 frameABasis = firstBodyIter->second.startTransform.getBasis().inverse();

				// ���� ���, ��������, ����, �� ������ ������������ ����

				if (LuaHelper::GetBoolean(currTable, "axisShift", false))
				{
					frameABasis[0] = basisA.getRow(1);
					frameABasis[1] = basisA.getRow(2);
					frameABasis[2] = basisA.getRow(0);
				}

				auto basisB = secondBodyIter->second.startTransform.getBasis().inverse();
				btMatrix3x3 frameBBasis = basisB;

				if (LuaHelper::GetBoolean(currTable, "axisShift", false))
				{
					frameBBasis[0] = basisB.getRow(1);
					frameBBasis[1] = basisB.getRow(2);
					frameBBasis[2] = basisB.getRow(0);
				}

				btTransform frameA(frameABasis, 2* firstBodyIter->second.axisOffset);
				btTransform frameB(frameBBasis, SceneVector());

				btGeneric6DofConstraint *constraint = new btGeneric6DofConstraint(
					*firstBodyIter->second.body, *secondBodyIter->second.body, frameA, frameB, false);
				secondBodyIter->second.body->addConstraintRef(constraint);

				secondBodyIter->second.constraintDebug = LuaHelper::GetBoolean(currTable, "debug", false);

				// -- ��������� ����������� ����� �������

				auto angLo = LuaHelper::GetVec3(currTable["angularLowLimit"]);
				constraint->setAngularLowerLimit(btVector3(angLo.x, angLo.y, angLo.z));
				auto angUp = LuaHelper::GetVec3(currTable["angularUpperLimit"]);
				constraint->setAngularUpperLimit(btVector3(angUp.x, angUp.y, angUp.z));

				auto linLo = LuaHelper::GetVec3(currTable["linearLowLimit"]);
				constraint->setLinearLowerLimit(btVector3(linLo.x, linLo.y, linLo.z));
				auto linUp = LuaHelper::GetVec3(currTable["linearUpperLimit"]);
				constraint->setLinearUpperLimit(btVector3(linUp.x, linUp.y, linUp.z));

				constraint->setEnabled(true);

				constraint->setBreakingImpulseThreshold(10000000);

				gps->GetPhysics().dynamicsWorld->addConstraint(constraint, true);*/

				secondBodyIter->second.body->addConstraintRef(
					gps->GetPhysics().CreateConstraint(
						firstBodyIter->second.body, secondBodyIter->second.body, firstBodyIter->second.axisOffset * 2.f, SceneVector(0.f),
						LuaHelper::GetBTVec3(currTable["angularLowLimit"]), LuaHelper::GetBTVec3(currTable["angularUpperLimit"]),
						LuaHelper::GetBTVec3(currTable["linearLowLimit"]), LuaHelper::GetBTVec3(currTable["linearUpperLimit"])
					)
				);
			}
		}

		// ������� ���� �������� ��� ������
		auto orientAngleComponentsTable = LuaHelper::GetTable(table, "orientAngleComponents", false);

		skeleton.orientAngleComponents = std::unordered_map<std::string, float> {
			/*std::make_pair(SPINE_01_NODE_NAME, 0.5), std::make_pair(HEAD_ROTATION_NODE_NAME, 0.5)*/
		};

		if (!orientAngleComponentsTable.IsNil())
		{
			skeleton.orientAngleComponents.clear();

			for (LuaPlus::LuaTableIterator iter(orientAngleComponentsTable); iter.IsValid(); iter.Next())
			{
				assert(iter.GetValue().IsConvertibleToNumber() && iter.GetKey().IsString());

				std::string boneName = iter.GetKey().ToString();
				float angleComponent = iter.GetValue().ToNumber();

				skeleton.orientAngleComponents.insert(std::make_pair(boneName, angleComponent));
			}
		}

		entity.assign<Skeleton>(skeleton);
	}

	LuaPlus::LuaObject SkeletonComponentManager::GetComponentTable(entityx::Entity& entity) const
	{
		LuaPlus::LuaObject res;

		if (entity.has_component<Skeleton>())
		{
			auto componentHandler = entity.component<Skeleton>();
			res.AssignNewTable(gps->GetLuaState());

			res.SetString("name", componentHandler->name.c_str());
		}
		else
		{
			res.AssignNil();
		}

		return res;
	};

	LuaPlus::LuaObject SkeletonComponentManager::ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		if (!entity.has_component<Skeleton>() && !gps->HasElement<Skeleton>(table.GetByName("name").ToString()))
			return LuaPlus::LuaObject().AssignNil();

		entity.remove<Skeleton>();
		entity.assign<Skeleton>(*(gps->CreateElement<Skeleton>(table.GetByName("name").ToString())));
	}

	// ���������� ���������� ������ �������� ���� ����� ������� ��������s
	int SkeletonComponentManager::GetBoneBodyWorldArrayIndex(const LuaPlus::LuaObject &entityTable, const LuaPlus::LuaObject &boneName)
	{
		auto entity = gps->GetEntity(entityTable);

		int res = entity.component<Skeleton>()->boneBodies.at(boneName.GetString()).body->getWorldArrayIndex();

		return res;
	}

	LuaPlus::LuaObject SkeletonComponentManager::GetBoneBodyPos(const LuaPlus::LuaObject &entity, const char* boneBodyName)
	{
		return gps->GetEntity(entity).component<Skeleton>()->boneBodies.at(boneBodyName).GetFirstJointPos().ToMapCoords(gps->GetLuaState());
	}

	// ������������ ���� ���, ��� ��� z � ������� ���� ���������� ���������� �� ����������� � ���������� �����
	void SkeletonComponentManager::OrientBoneBodyToPoint(const LuaPlus::LuaObject &entityTable, const char *boneBodyName, 
		const float &modifier, const LuaPlus::LuaObject &mapPoint)
	{
		auto movDir = gps->GetEntity(entityTable).component<Moveable>()->targetBodyOrientation.Normalize();
		auto rootRot = btQuaternion(SceneVector(0.f, 1.f, 0.f), SceneVector(0.f, 0.f, 1.f).Angle(movDir, SceneVector(0.f, 1.f, 0.f)));
		auto &bb = gps->GetEntity(entityTable).component<Skeleton>()->boneBodies.at(boneBodyName);

		// ���� ���������� ��������, ���������� false ������ ���������
		if (mapPoint.IsBoolean() && !mapPoint.GetBoolean())
		{
			bb.additionalRotation = btQuaternion::getIdentity();
			return;
		}

		SceneVector modifiedOZ = modifier * (SceneVector::FromMapCoords(mapPoint) - bb.GetFirstJointPos()).Normalized();
		SceneVector originalOZ = SceneVector(0.f, 0.f, 1.f).Rotate(rootRot * bb.startTransform.getRotation());
		SceneVector originalOX = SceneVector(1.f, 0.f, 0.f).Rotate(rootRot * bb.startTransform.getRotation());
	
		btQuaternion revOY = btQuaternion(
			originalOZ.XZ().Normalized().Cross(modifiedOZ.XZ().Normalized()),
			originalOZ.XZ().Normalized().Angle(
				modifiedOZ.XZ().Normalized(), 
				originalOZ.XZ().Normalized().Cross(modifiedOZ.XZ().Normalized())
			)
		);

		auto modifiedOX = originalOX.Rotate(revOY);

		btQuaternion revOX = btQuaternion(
			modifiedOX,
			originalOZ.XZ().Normalized().Rotate(revOY).Angle(
				modifiedOZ,
				modifiedOX
			)
		);
		
		bb.additionalRotation = revOX * revOY;
	}

	void SkeletonComponentManager::SetBoneOrientationRatio(const LuaPlus::LuaObject &entityTable, const float &bodyComponent, const float &headComponent)
	{
		auto &angleComponents = gps->GetEntity(entityTable).component<Skeleton>()->orientAngleComponents;

		angleComponents[SPINE_01_NODE_NAME] = bodyComponent;
		//angleComponents[HEAD_NODE_NAME] = headComponent;
	}

	void SkeletonComponentManager::EnableBoneBodyAnimation(const LuaPlus::LuaObject &entity, const LuaPlus::LuaObject &boneBodiesList, 
		const bool &enable)
	{
		auto skel = gps->GetEntity(entity).component<Skeleton>();

		for (LuaPlus::LuaTableIterator iter(boneBodiesList); iter.IsValid(); iter.Next())
		{
			std::string boneName = iter.GetValue().GetString();
			skel->boneBodies.at(boneName).animatedAbsolute = enable;
		}
	}

	void SkeletonComponentManager::SetBoneBodyPos(const LuaPlus::LuaObject &entity, const char *boneBodyName, 
		const LuaPlus::LuaObject &pos)
	{
		auto skel = gps->GetEntity(entity).component<Skeleton>();
		skel->boneBodies.at(boneBodyName).straightPosition = SceneVector::FromMapCoords(pos);
	}

	void SkeletonComponentManager::AddBoneBodyConstraint(const LuaPlus::LuaObject &entity, const char* boneBodyNameA, const char* boneBodyNameB,
		const LuaPlus::LuaObject &constraintInfo)
	{
		auto skel = gps->GetEntity(entity).component<Skeleton>();

		auto frameA = btTransform::getIdentity();
		auto frameB = skel->boneBodies.at(boneBodyNameB).body->getWorldTransform().inverse() * skel->boneBodies.at(boneBodyNameA).body->getWorldTransform();

		if (constraintInfo.GetByName("frameABasis").GetRef() != -1)
		{
			frameA.setBasis(LuaHelper::GetBTMat3(LuaHelper::GetTable(constraintInfo, "frameABasis")));
		}
		
		if (constraintInfo.GetByName("frameBBasis").GetRef() != -1)
		{
			frameB.setBasis(LuaHelper::GetBTMat3(LuaHelper::GetTable(constraintInfo, "frameBBasis")));
		}

		// �������� �������

		auto linLoLim = btVector3(0.f, 0.f, 0.f);
		auto linUpLim = btVector3(0.f, 0.f, 0.f);

		if (!constraintInfo.GetByName("linearLowerLimits").IsNil())
			linLoLim = LuaHelper::GetBTVec3(LuaHelper::GetTable(constraintInfo, "linearLowerLimits"));

		if (!constraintInfo.GetByName("linearUpperLimits").IsNil())
			linUpLim = LuaHelper::GetBTVec3(LuaHelper::GetTable(constraintInfo, "linearUpperLimits"));

		// �������� ������� �������� � ����������� �����, ��� ��� ��������� �� � ���������� ������ � � �������, ��������� � ������� �
		if (linLoLim.length() > 0.f)
			linLoLim = (frameA.getBasis().inverse() * frameB.getBasis()) * linLoLim;
		if (linUpLim.length() > 0.f)
			linUpLim = (frameA.getBasis().inverse() * frameB.getBasis()) * linUpLim;

		// ������� �������

		auto angLoLim = btVector3(0.f, 0.f, 0.f);
		auto angUpLim = btVector3(0.f, 0.f, 0.f);

		if (!constraintInfo.GetByName("angularLowerLimits").IsNil())
			angLoLim = LuaHelper::GetBTVec3(LuaHelper::GetTable(constraintInfo, "angularLowerLimits"));

		if (!constraintInfo.GetByName("angularUpperLimits").IsNil())
			angUpLim = LuaHelper::GetBTVec3(LuaHelper::GetTable(constraintInfo, "angularUpperLimits"));

		// ����� ������ ������ � � ����
		if (LuaHelper::GetBoolean(constraintInfo, "writeToFile", false))
		{
			auto str = std::ofstream("DebugOutput/constraint_" + std::string(boneBodyNameA) + "_" + std::string(boneBodyNameB) + ".txt");
			str.clear();
			str << std::to_string(frameB.getBasis()[0][0]) + ", " + std::to_string(frameB.getBasis()[0][1]) + ", " + 
				std::to_string(frameB.getBasis()[0][2]) + ", ";
			str << "\r\n";
			str << std::to_string(frameB.getBasis()[1][0]) + ", " + std::to_string(frameB.getBasis()[1][1]) + ", " +
				std::to_string(frameB.getBasis()[1][2]) + ", ";
			str << "\r\n";
			str << std::to_string(frameB.getBasis()[2][0]) + ", " + std::to_string(frameB.getBasis()[2][1]) + ", " +
				std::to_string(frameB.getBasis()[2][2]);

			str.close();
		}

		skel->boneBodies.at(boneBodyNameB).body->addConstraintRef(
			gps->GetPhysics().CreateConstraint(
				skel->boneBodies.at(boneBodyNameA).body, skel->boneBodies.at(boneBodyNameB).body,
				MODEL_SCALE * (skel->boneBodies.at(boneBodyNameA).startTransform.getBasis().inverse() * 
					(skel->boneHeadBindingPoints.at(boneBodyNameB) - skel->boneHeadBindingPoints.at(boneBodyNameA))),
				SceneVector(0.f),
				frameA.getBasis(), frameB.getBasis(),
				angLoLim, angUpLim, linLoLim, linUpLim,
				LuaHelper::GetBoolean(constraintInfo, "axisShift", false)
			)
		);
	}

	void SkeletonComponentManager::RemoveBoneBodyConstraint(const LuaPlus::LuaObject &entity, const char* boneBodyNameB, const int& constraintNum)
	{
		auto *body = gps->GetEntity(entity).component<Skeleton>()->boneBodies.at(boneBodyNameB).body;

		if (body->getNumConstraintRefs() > constraintNum)
		{
			body->removeConstraintRef(body->getConstraintRef(constraintNum));
			gps->GetPhysics().dynamicsWorld->removeConstraint(
				body->getConstraintRef(constraintNum)
			);
		}
	}

	void SkeletonComponentManager::EnableBoneBodyConstraint(const LuaPlus::LuaObject &entity, const char *boneBodyName, 
		const int &constraintNum, const bool &enabled)
	{
		gps->GetEntity(entity).component<Skeleton>()->boneBodies.at(boneBodyName).body->getConstraintRef(constraintNum)->setEnabled(enabled);
	}

	void SkeletonComponentManager::LooseBoneBodies(const LuaPlus::LuaObject &entity, const float &recoveryTime)
	{
		auto skel = gps->GetEntity(entity).component<Skeleton>();

		skel->locomotorActivity = 0.f;
		skel->recoveryFactor = 1.0 / recoveryTime;
	}

	void SkeletonComponentManager::SetDead(const LuaPlus::LuaObject &entity, const bool &isDead)
	{
		gps->GetEntity(entity).component<Skeleton>()->dead = isDead;
		
		if (isDead)
		{
			for (auto &bb : gps->GetEntity(entity).component<Skeleton>()->boneBodies)
			{
				bb.second.body->setGravity(SceneVector(0.f, -10.f, 0.f));
			}
		}
		else
		{
			for (auto &bb : gps->GetEntity(entity).component<Skeleton>()->boneBodies)
			{
				bb.second.body->setGravity(SceneVector(0.f, 0.f, 0.f));
			}
		}
	}

	void SkeletonComponentManager::SetBodiesInertiaDamping(const LuaPlus::LuaObject &entity,
		const float &inertiaDamping, const LuaPlus::LuaObject &bodyList)
	{
		auto skel = gps->GetEntity(entity).component<Skeleton>();

		for (LuaPlus::LuaTableIterator iter(bodyList); iter; iter.Next())
		{
			auto &bb = skel->boneBodies.at(iter.GetValue().GetString());
			bb.inertiaDamping = inertiaDamping;
		}
	}

	int SkeletonComponentManager::GetNumConstraints(const LuaPlus::LuaObject &entity, const char* boneBody)
	{
		return gps->GetEntity(entity).component<Skeleton>()->boneBodies.at(boneBody).body->getNumConstraintRefs();
	}

	bool SkeletonComponentManager::HasSkeleton(const LuaPlus::LuaObject &entity)
	{
		return gps->GetEntity(entity).has_component<Skeleton>();
	}
}