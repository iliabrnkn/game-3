#pragma once

#include <vector>
#include <string>

#include "RenderBatch.h"

namespace mg
{
	class GeometryBatch : public ConcreteRenderBatch<AnimVertex>
	{
		friend class Renderer;

	public:
		virtual void Init(LuaPlus::LuaState* luaState) override;
		virtual void Draw() override;
	};
}
