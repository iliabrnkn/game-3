#pragma once

#include "stdafx.h"
#include <gl/glew/glew.h>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Vector.h"
#include "Sizes.h"

namespace mg
{
	class Camera
	{
		const float RENDERBUFFER_SCENE_WIDTH = RENDERBUFFER_WIDTH / TILE_WIDTH_SCREEN;
		const float RENDERBUFFER_SCENE_HEIGHT = RENDERBUFFER_HEIGHT / TILE_HEIGHT_SCREEN;
		const float BINDING_BOX_SIZE = (RENDERBUFFER_SCENE_HEIGHT / 2.f + glm::sqrt(2.f) / 2.f * RENDERBUFFER_SCENE_WIDTH) * 2.f / glm::sqrt(2.f);

		SceneVector viewportCenterPos;
		SceneVector basicOffset;

		const float CAMERA_BOUNDING_BOX_HEIGHT = 20.f;

	public:
		Camera(void);
		~Camera(void);

		void Init();
		void CenterAt(float newPosX, float newPosY);
		void MoveBy(float dPosX, float dPosY);
		glm::mat4 GetProjectionMatrix() const;
		glm::mat4 GetWorldMatrix() const;
		glm::mat4 GetWorldViewMatrix() const;
		//SceneVector GetWorldOffset() const;
		//glm::ivec2 GetScreenOffset() const;
		SceneVector GetCameraCenterScenePos() const;
		SceneVector GetCameraTopLeftScenePos() const;

		bool ViewportIntersectsWithCircle(const SceneVector &circleCenterPos, const float &circleRadius) const;
		
		glm::mat4 GetScreenWorldMatrix() const;

		glm::mat4 GetSceneMVP() const;
		glm::mat4 GetScreenMVP() const;

		inline SceneVector Camera::GetViewportBoundingBoxMin() const
		{
			SceneVector res = viewportCenterPos - SceneVector(BINDING_BOX_SIZE, 0.f, BINDING_BOX_SIZE);
			res.Y() = -CAMERA_BOUNDING_BOX_HEIGHT;

			return res;
		}

		inline SceneVector Camera::GetViewportBoundingBoxMax() const
		{
			SceneVector res = viewportCenterPos + SceneVector(BINDING_BOX_SIZE, 0.f, BINDING_BOX_SIZE);
			res.Y() = CAMERA_BOUNDING_BOX_HEIGHT;

			return res;
		}
	};
}