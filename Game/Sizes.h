#pragma once

#include <glm\glm.hpp>
#include "WindowConstants.h"
#include "math.h"
//#include "Matrices.h"

namespace mg
{
	const float TILE_HEIGHT_SCREEN = 32.0f;
	const float MAP_TILE_STEP = 32.0f;
	const float TILE_WIDTH_SCENE = 45.254833995939f;
	const float HEIGHT_UNIT_SCREEN = 39.1918411f;
	const float TILE_WIDTH_SCREEN = 64.0f;
	const float ZUNITPROJLEN = 39.1918411f; // ������ ������ 45.254833995939, ������������ �����, ����� �������� ������ ����� �������� � ����� ������ ������� �������� �� ������
	
	/*const float MAX_AXIS_HALF = (glm::sqrt(float(RENDERBUFFER_HEIGHT) * float(RENDERBUFFER_HEIGHT) + float(RENDERBUFFER_WIDTH) * float(RENDERBUFFER_WIDTH)) /
		glm::sqrt((TILESTEP / 2.0 * TILESTEP / 2.0) + (TILEHYPOTENUSE / 2.0 * TILEHYPOTENUSE / 2.0))) / 2.0;*/
	
	const float COLOR_VOLUME_STEP = 25.5f;
	const float Z_COLOR_COEF = 255.0f / COLOR_VOLUME_STEP;
	const int LIGHT_MAX_DYNAMIC_OMNILIGHTS = 32;
	const int LIGHT_MAX_STATIC_OMNILIGHTS = 32;
	const int LIGHT_MAX_DYNAMIC_DIRECT_LIGHTS = 32;
	const int LIGHT_MAX_STATIC_DIRECT_LIGHTS = 32;
	const int TEX_WIDTH_PER_LIGHT_POINT = 1024;
	const float SHADOW_GRADIENT_ANGLE = M_PI / 45.f;

	const float MIN_POSITION_GAP = 0.001f;
	const float MIN_DIRECTION_ANGLE_GAP = 0.0001f * glm::pi<float>();
}