#pragma once

#include <lua\LuaPlus.h>

#include "Widget.h"
#include "GameplayState.h"
#include "ItemGrid.h"

namespace mg
{
	class InventoryItem : public Widget
	{
		friend class InventoryGrid;

		LuaPlus::LuaObject itemTable;

		sf::Vector2i mouseDragOffset;
	
	public:
		InventoryItem(const WidgetParameters& params, const std::string& name);

		virtual void OnMouseEnter() override;

		virtual void OnMouseLeave() override;

		LuaPlus::LuaObject &GetTable();

		virtual void OnClick(const sf::Vector2i& relativeCoords) override;

		virtual void OnDrag(const sf::Vector2i& mouseAbsCoords) override;

		virtual void OnDrop(const sf::Vector2i& mouseAbsCoords) override;

		sf::Vector2i lastScreenPos;

		sf::Vector2i GetLastScreenPos() const;

		sf::Vector2i GetLastGridPos() const;

		~InventoryItem();
	};
}