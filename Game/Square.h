#pragma once

#include <glm\glm.hpp>

#include "PathFinder/AStar.h"

class Square : public AStarNode
{
public:

	Square();

	~Square();

	void setType(const bool typeInit);

	bool getType() const;

	// A diagonal move has a sqrt(2) cost, 1 otherwise
	float localDistanceTo(AStarNode* node) const;

	virtual float distanceTo(AStarNode* node) const override;

private:
	// To tell wether a pixel is "walkable" or not
	bool m_type;
};