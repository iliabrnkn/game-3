#pragma once

#include <entityx\System.h>
#include <entityx\Entity.h>

#include "Camera.h"
#include "AISystem.h"
#include <lua/LuaPlus.h>
#include "TransformHitboxesToRagdoll.h"

namespace mg
{
	const float FORGIVING_ANGLE = 0.000314;
	const double INSIGNIFICANT_VAL = 0.1;

	class PhysicsSystem : public entityx::System<PhysicsSystem>, public entityx::Receiver<entityx::EntityDestroyedEvent>
	{
		friend class GameplayState;

		GameplayState* gps;

		double deltaTime;
		std::vector<btPersistentManifold*> contactManifolds;
	public:

		virtual void configure(entityx::EventManager &events) override;
		virtual void update(entityx::EntityManager &es, entityx::EventManager &em, entityx::TimeDelta dt) override;
		void receive(const entityx::EntityDestroyedEvent& ev);
		void HandleCollision(RigidBody *firstBody, RigidBody *secBody, const btManifoldPoint &point, const double &dt);

		PhysicsSystem(GameplayState* gps);
	};
}