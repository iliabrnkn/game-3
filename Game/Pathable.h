#pragma once

#include <vector>
#include <glm/glm.hpp>

#include "ComponentManager.h"
#include "Vector.h"

namespace mg {

	struct Pathable
	{
		Pathable() = default;
		~Pathable() = default;

		std::vector<SceneVector> waypoints;
	};

	class PathableComponentManager : public ComponentManager
	{
	public:
		PathableComponentManager(GameplayState *gps) : ComponentManager("pathable", gps) {};
		~PathableComponentManager() = default;

		virtual void AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const override;
		virtual LuaPlus::LuaObject GetComponentTable(entityx::Entity& entity) const override;
		LuaPlus::LuaObject FindPath(LuaPlus::LuaObject& beginPoint, LuaPlus::LuaObject& endPoint);
		LuaPlus::LuaObject GetCurrentPath(LuaPlus::LuaObject &entity);
		void ClearWayPoints(LuaPlus::LuaObject &entity) const;
		void GoToLastWayPoint(LuaPlus::LuaObject &entity) const;
		bool HasWayPoints(LuaPlus::LuaObject &entity) const;
	};
}