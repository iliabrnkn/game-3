#pragma once

#include "stdafx.h"

#include <vector>
#include <utility>
#include <unordered_map>
#include <windows.h>
#include <set>
#include <memory>
#include <string>
#include <GL/glew/glew.h>
#include <GL/GL.h>

#include <glm\glm.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <fstream>
#include <lua/LuaPlus.h>
//#include "VertStructs.h"
//#include "Defines.h"
#include "UniformSetters.h"
#include "VBO.h"

#define GLSL_MAX_NAME_LENGTH 64

namespace mg {

	class Effect
	{
		std::vector<std::shared_ptr<UniformSetter>> deferredSetters;
		mutable std::set<std::string> errorUniforms;

	public:
		~Effect(void);

		Effect();
		void LoadFromFile(const std::string& shaderFilename, LuaPlus::LuaState* luaState);

		Effect(Effect& effect);
		Effect(Effect&& effect);
		Effect(const Effect& effect);
		/*Effect& operator = (const Effect& effect);*/
		Effect& operator = (Effect& effect);
		Effect& operator = (Effect&& effect);

		void CreateProgram();
		void Use() const;

		GLint AllocateUniformVar(const std::string& varName) const;
		GLint AllocateAttribVar(const std::string& varName) const;
		GLint AllocateUniformBlock(const std::string& varName) const;

		void SetUniformMatrix4f(const std::string& varName, const float* buf) const;
		void SetUniform1f(const std::string& varName, const float& v) const;
		void SetUniform2f(const std::string& varName, const glm::vec2& v) const;
		void SetUniform3f(const std::string& varName, const glm::vec3& v) const;
		void SetUniform3fArray(const std::string& varName, const unsigned int& num, const glm::vec3* vs) const;
		void SetUniform1i(const std::string& varName, const GLint& v) const;
		void SetUniform1iArray(const std::string& varName, unsigned int& num, const int* vs) const;
		void SetUniform1fArray(const std::string& varName, unsigned int& num, const float* vs) const;
		void SetUniformBlock(const std::string& blockName, const GLuint& bufferID, const GLuint& uniformBlockBinding, const GLsizei& size) const;

		void SetUniformMatrix4fDeferred(const std::string& varName, glm::mat4& v)
		{
			std::shared_ptr<UniformSetter> el = std::shared_ptr<UniformSetter>(new UniformMx4Setter(static_cast<const Effect*>(this), varName, v));
			
			deferredSetters.push_back(el);
		}

		void SetUniform1fDeferred(const std::string& varName, float& v)
		{
			std::shared_ptr<UniformSetter> el = std::shared_ptr<UniformSetter>(new UniformFSetter(static_cast<const Effect*>(this), varName, v));
			
			deferredSetters.push_back(el);
		}

		void SetUniform2fDeferred(const std::string& varName, glm::vec2 v)
		{
			std::shared_ptr<UniformSetter> el = std::shared_ptr<UniformSetter>(new Uniform2FSetter(static_cast<const Effect*>(this), varName, v));
			
			deferredSetters.push_back(el);
		}

		void SetUniform3fDeferred(const std::string& varName, glm::vec3 v)
		{
			std::shared_ptr<UniformSetter> el = std::shared_ptr<UniformSetter>(new Uniform3FSetter(static_cast<const Effect*>(this), varName, v));
			
			deferredSetters.push_back(el);
		}

		void SetUniform1iDeferred(const std::string& varName, int v)
		{
			std::shared_ptr<UniformSetter> el = std::shared_ptr<UniformSetter>(new UniformISetter(static_cast<const Effect*>(this), varName, v));
			
			deferredSetters.push_back(el);
		}

		template<typename T>
		void SetUniformBlockDeferred(const std::string& blockName, const GLuint& uniformBlockBinding, VBO<T> buffer)
		{
			std::shared_ptr<UniformSetter> el = std::shared_ptr<UniformSetter>(
				new UniformBlockSetter(static_cast<const Effect*>(this), blockName, uniformBlockBinding, buffer.GetID(), buffer.GetMaxSize()));

			deferredSetters.push_back(el);
		}

		void SetUniformBlockDeferred(const std::string& blockName, const GLuint& uniformBlockBinding, const GLuint& vboID, const size_t& size)
		{
			std::shared_ptr<UniformSetter> el = std::shared_ptr<UniformSetter>(
				new UniformBlockSetter(static_cast<const Effect*>(this), blockName, uniformBlockBinding, vboID, size));
			
			deferredSetters.push_back(el);
		}

		GLint GetAttribIndex(const std::string& varName) const;
		GLint GetUniformVar(const std::string& varName) const;
		std::string GetInfoLog() const;
		const std::string& GetName() const;
		void InvokeDeferredUniformSetters();

	private:
		std::string name;
		GLuint programID;
		GLuint vertShaderID, fragShaderID, geoShaderID;
		mutable std::unordered_map<std::string, GLint> attribs;
		mutable std::unordered_map<std::string, GLint> uniforms;
		mutable std::unordered_map<std::string, GLint> uniformBlocks;

		GLuint& AddShader(const char *chars, GLenum shaderType, const char* shaderLogName);

		void CheckShaderForErrors(GLuint effect, GLenum param, const char* shaderLogName);
		void CheckProgramForErrors(GLenum param);

		bool isResourceOwner;
		bool geometryShaderExists;

	};
}