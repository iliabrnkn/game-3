#pragma once

#include <vector>

#include <lua\LuaPlus.h>

#include "ComponentManager.h"

namespace mg
{
	struct Viewable
	{
		Viewable(const float& angle, const float& radius) :
			angle(angle),
			radius(radius)
		{}
		
		~Viewable() = default;

		float angle;
		std::vector<LuaPlus::LuaObject> entitiesInSight;
		float radius;
	};

	class ViewableComponentManager : public ComponentManager
	{
	public:
		ViewableComponentManager(GameplayState *gps) : ComponentManager("viewable", gps) {};
		~ViewableComponentManager() = default;

		virtual void AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const override;
		virtual LuaPlus::LuaObject GetComponentTable(entityx::Entity& entity) const override;
		virtual LuaPlus::LuaObject ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const override;
	};

}