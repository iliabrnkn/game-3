#include "stdafx.h"

#include "AISystem.h"
#include "AIPiece.h"
#include "Position.h"
#include "GUI.h"
#include "ScriptApiFacade.h"

#include "Moveable.h"

namespace mg {
	AISystem::AISystem(GameplayState* gps) :
		gps(gps)
	{}

	void AISystem::configure(entityx::EventManager &events)
	{
		deltaTime = .0;
	}

	void AISystem::update(entityx::EntityManager &es, entityx::EventManager &em, entityx::TimeDelta dt)
	{
		es.each<AIPiece>([dt, &es, this](entityx::Entity& entity, AIPiece& aiPiece)
		{	
			if (aiPiece.timerRequests.size() > 0)
			{
				// ���� ����� �������������� ����� �������, ��������� ��
				aiPiece.timers.reserve(aiPiece.timers.size() + aiPiece.timerRequests.size());
				std::copy(aiPiece.timerRequests.begin(), aiPiece.timerRequests.end(), std::back_inserter(aiPiece.timers));
				aiPiece.timerRequests.clear();
			}

			// ���������� �� �������
			for (auto& timer : aiPiece.timers)
			{
				if (!timer.staled)
				{
					timer.currentTime += dt;

					if (timer.currentTime >= timer.endTime)
					{
						timer.Fire(aiPiece.table);
						timer.staled = true;
					}
				}
			}

			// �����������

			if (aiPiece.repeaterRequests.size() > 0)
			{
				// ���� ����� �������������� ����� �������, ��������� ��
				
				for (auto& repeaterRequest : aiPiece.repeaterRequests)
				{
					auto iter = aiPiece.repeaters.find(repeaterRequest.name);

					if (iter != aiPiece.repeaters.end())
						iter->second = std::move(repeaterRequest);
					else
						aiPiece.repeaters.insert(make_pair(repeaterRequest.name, repeaterRequest));
				}

				aiPiece.repeaterRequests.clear();
			}

			// ���������� �� �������
			for (auto& pair : aiPiece.repeaters)
			{
				pair.second.Check(dt, aiPiece.table);
			}

			// ������� ����������� �������
			auto removeIter = std::remove_if(aiPiece.timers.begin(), aiPiece.timers.end(),
				[](auto& x) { return x.staled; });

			aiPiece.timers.erase(removeIter, aiPiece.timers.end());

			// ������� ����������� ��������

			for (auto iter = aiPiece.repeaters.begin(); iter != aiPiece.repeaters.end();)
			{
				if (static_cast<LuaPlus::LuaFunction<bool>>(iter->second.predicate)(aiPiece.table, iter->second.elapsedTime))
				{
					if (iter->second.finalization.IsFunction())
						static_cast<LuaPlus::LuaFunction<bool>>(iter->second.finalization)(aiPiece.table);

					iter = aiPiece.repeaters.erase(iter);
				}
				else
					iter++;
			}

			// �������� ���������� ����������, ���� �� ����
			aiPiece.Emit("onUpdate");
		});
	}
}