#pragma once

#include <entityx\System.h>
#include "Camera.h"=
#include <lua/LuaPlus.h>

namespace mg {

	class AISystem : public entityx::System<AISystem>
	{
		const double timeDelimiter = 0.5;

		friend class GameplayState;
		friend class Level;

		GameplayState* gps;
		entityx::Entity::Id playerID;

		double deltaTime;
	public:

		virtual void configure(entityx::EventManager &events) override;
		virtual void update(entityx::EntityManager &es, entityx::EventManager &em, entityx::TimeDelta dt) override;

		AISystem(GameplayState* gps);
	};
}