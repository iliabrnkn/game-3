#include "stdafx.h"

Square::Square()
{}

Square::~Square()
{}

void Square::setType(const bool typeInit)
{
	m_type = typeInit;
}

bool Square::getType() const
{
	return m_type;
}

// A diagonal move has a sqrt(2) cost, 1 otherwise
float Square::localDistanceTo(AStarNode* node) const
{
	if (node->getX() != m_x && node->getY() != m_y)
		return 1.41421356237f;
	else
		return 1.0f;
}

float Square::distanceTo(AStarNode* node) const
{
	int newX = m_x - node->getX(), newY = m_y - node->getY();
	return sqrtf(static_cast<float>(newX*newX + newY*newY));
}