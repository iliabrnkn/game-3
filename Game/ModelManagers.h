#pragma once

#include <string>
#include <unordered_map>
#include <memory>
#include <fstream>

#include "ModelStruct.h"
#include "Model.h"
#include "Factory.h"
#include "Skeleton.h"

namespace mg
{
	const unsigned int INDICES_OFFSET_DUMMY = 2; // ������ ��� ����� � ������ ������ �������� ��� ��������

	struct MeshBuffers
	{
		std::vector<unsigned int> indices;
		std::vector<AnimVertex> vertices;
	};

	//-----------MeshManager------------

	class MeshFactory : public ComponentFactory<Mesh>
	{
		friend class GameplayState;

	private:
		std::unordered_map<std::string, MeshBuffers> meshBuffers;
		
	public:
		MeshFactory() = default;
		~MeshFactory() = default;

		virtual void LoadFromFile(const string& filename) override;
		virtual std::shared_ptr<Cloneable> CreateElement(const std::string& componentName, GameplayState *gps, const int &id = -1) override;
	};

	//-----------SkeletonManager------------

	class SkeletonFactory : public ComponentFactory<Skeleton>
	{
	public:
		SkeletonFactory() = default;
		~SkeletonFactory() = default;

		virtual void LoadFromFile(const string& filename) override;
		virtual std::shared_ptr<Cloneable> CreateElement(const std::string& componentName, GameplayState *gps, const int &id = -1) override;
	};

	//-----------AnimationManager------------

	class AnimationFactory : public ComponentFactory<Animation>
	{
	public:
		AnimationFactory() = default;
		~AnimationFactory() = default;

		virtual void LoadFromFile(const string& filename) override;
		virtual std::shared_ptr<Cloneable> CreateElement(const string& componentName, GameplayState *gps, const int &id = -1) override;

	};
}