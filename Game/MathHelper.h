#pragma once

#include "Vector.h"

namespace mg
{
	struct LinearSegment
	{
		float minCoord, maxCoord;

		LinearSegment(const float &minCoord, const float &maxCoord) :
			minCoord(minCoord), maxCoord(maxCoord) {}
	};

	class MathHelper
	{
		public:
		static inline bool IntersectsBoxes(const SceneVector &firstBoxMinBoundingPoint, const SceneVector &firstBoxMaxBoundingPoint,
			const SceneVector &secBoxMinBoundingPoint, const SceneVector &secBoxMaxBoundingPoint)
		{
			if (
					(
						(firstBoxMinBoundingPoint.X() <= secBoxMinBoundingPoint.X() && secBoxMinBoundingPoint.X() <= firstBoxMaxBoundingPoint.X())
						|| (firstBoxMinBoundingPoint.X() <= secBoxMaxBoundingPoint.X() && secBoxMaxBoundingPoint.X() <= firstBoxMaxBoundingPoint.X())
					) && (
						(firstBoxMinBoundingPoint.Y() <= secBoxMinBoundingPoint.Y() && secBoxMinBoundingPoint.Y() <= firstBoxMaxBoundingPoint.Y())
						|| (firstBoxMinBoundingPoint.Y() <= secBoxMaxBoundingPoint.Y() && secBoxMaxBoundingPoint.Y() <= firstBoxMaxBoundingPoint.Y())
					) && (
						(firstBoxMinBoundingPoint.Z() <= secBoxMinBoundingPoint.Z() && secBoxMinBoundingPoint.Z() <= firstBoxMaxBoundingPoint.Z())
						|| (firstBoxMinBoundingPoint.Z() <= secBoxMaxBoundingPoint.Z() && secBoxMaxBoundingPoint.Z() <= firstBoxMaxBoundingPoint.Z())
					)
				)
				return true;
			else
				return false;
		}

		// ������� ����������, ������������ �� ��� �������� ������� � ���
		static inline bool IntersectsLinearSegments(const LinearSegment &firstSegment, const LinearSegment &secSegment,
			float &intersectionMiddlePoint, float &intersectionLeftPoint, float &intersectionRightPoint)
		{
			const LinearSegment &leftSegment = firstSegment.minCoord <= secSegment.minCoord ? firstSegment : secSegment;
			const LinearSegment &rightSegment = &leftSegment == &firstSegment ? secSegment : firstSegment;

			// ���� ����������� ���������� ������� ������� ����� ����� ����������� � ������������ ������������� ������
			if (leftSegment.minCoord <= rightSegment.minCoord && rightSegment.minCoord <= leftSegment.maxCoord)
			{
				// ���� ������������ ���������� ������� ������� ����� ����� ������������ ���������� ������
				if (rightSegment.maxCoord <= leftSegment.maxCoord)
				{
					intersectionMiddlePoint = rightSegment.minCoord + (rightSegment.maxCoord - rightSegment.minCoord) / 2.f;
					intersectionLeftPoint = rightSegment.minCoord;
					intersectionRightPoint = rightSegment.maxCoord;
				}
				else // ���� ������
				{
					intersectionMiddlePoint = rightSegment.minCoord + (leftSegment.maxCoord - rightSegment.minCoord) / 2.f;
					intersectionLeftPoint = rightSegment.minCoord;
					intersectionRightPoint = leftSegment.maxCoord;
				}

				return true;
			}
			else
				return false;
		}

		static inline bool IntersectsLinearSegments(const LinearSegment &firstSegment, const LinearSegment &secSegment,
			float &intersectionMiddlePoint)
		{
			float leftPoint, rightPoint;
			return IntersectsLinearSegments(firstSegment, secSegment, intersectionMiddlePoint, leftPoint, rightPoint);
		}

		static inline bool IntersectsLinearSegments(const LinearSegment &firstSegment, const LinearSegment &secSegment)
		{
			float leftPoint, rightPoint, intersectionMiddlePoint;
			return IntersectsLinearSegments(firstSegment, secSegment, intersectionMiddlePoint, leftPoint, rightPoint);
		}

		// ������� ����������, �������� �� ������������� �����
		static inline bool RectContainsPoint(const SceneVector &firstBoundingPoint, const SceneVector &secBoundingPoint, 
			const SceneVector &point)
		{
			return IntersectsLinearSegments(
				LinearSegment(firstBoundingPoint.X(), secBoundingPoint.X()),
				LinearSegment(point.X(), point.X())
			) && IntersectsLinearSegments(
				LinearSegment(firstBoundingPoint.Z(), secBoundingPoint.Z()),
				LinearSegment(point.Z(), point.Z())
			);
		}
	};
}