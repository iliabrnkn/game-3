#include "stdafx.h"

#include "Camera.h"
#include <SFML\Graphics.hpp>

#include "WindowConstants.h"
#include "math.h"
#include "Sizes.h"
#include "MatrixHelper.h"
#include "Core.h"

namespace mg
{
	Camera::Camera(void)
		: viewportCenterPos(glm::vec3(0.0)),
		basicOffset(SceneVector::FromScreenCoords(glm::vec2(RENDERBUFFER_WIDTH / 2.f, RENDERBUFFER_HEIGHT / 2.f)))
	{
	}

	Camera::~Camera(void)
	{
	}

	void Camera::Init()
	{
	}

	void Camera::CenterAt(float newPosX, float newPosY)
	{
		viewportCenterPos = SceneVector::FromMapCoords(newPosX, newPosY);
	}

	void Camera::MoveBy(float dPosX, float dPosY)
	{
		viewportCenterPos += SceneVector::FromMapCoords(dPosX, dPosY, 0.f);
	}

	glm::mat4 Camera::GetWorldMatrix() const
	{
		auto screenOffset = GetCameraTopLeftScenePos().ToScreenCoords();
		auto cameraOffset = MatrixHelper::ScreenToSceneAbs(screenOffset);

		return glm::translate((glm::vec3)(-cameraOffset));
	}

	glm::mat4 Camera::GetWorldViewMatrix() const
	{
		return MatrixHelper::viewMx * GetWorldMatrix();
	}

	glm::mat4 Camera::GetScreenWorldMatrix() const
	{
		auto screenOffset = GetCameraTopLeftScenePos().ToScreenCoords();

		return glm::translate(glm::vec3(-screenOffset.x, -screenOffset.y, 0.f));
	}

	glm::mat4 Camera::GetSceneMVP() const
	{
		/*auto tv0 = MatrixHelper::viewMx * glm::vec4(1.f, 0.f, 0.f, 1.f);
		auto tv1 = MatrixHelper::viewMx * glm::vec4(0.f, 1.f, 0.f, 1.f);
		auto tv2 = MatrixHelper::viewMx * glm::vec4(0.f, 0.f, 1.f, 1.f);

		auto depthTest1 = (MatrixHelper::projectionMx * MatrixHelper::viewMx * glm::vec4(5.f, 0.f, 5.f, 1.f)).z;

		auto depthTest2 = (MatrixHelper::projectionMx * MatrixHelper::viewMx * glm::vec4(10.f, 0.f, 10.f, 1.f)).z;*/

		return MatrixHelper::projectionMx * GetWorldViewMatrix();
	}

	SceneVector Camera::GetCameraCenterScenePos() const
	{
		return viewportCenterPos;
	}

	SceneVector Camera::GetCameraTopLeftScenePos() const
	{
		auto res = viewportCenterPos - basicOffset;

		return res;
	}

	glm::mat4 Camera::GetScreenMVP() const
	{
		auto res = MatrixHelper::screenToNDMx * GetScreenWorldMatrix();

		return res;
	}

	glm::mat4 Camera::GetProjectionMatrix() const
	{
		return MatrixHelper::projectionMx;
	}

	bool Camera::ViewportIntersectsWithCircle(const SceneVector &circleCenterPos, const float &circleRadius) const
	{
		// ������� �� ������ ���������� �� ������ ������ � ����������� ������
		glm::vec2 fromCircleToScreenCenter = (circleCenterPos - viewportCenterPos).ToScreenCoords();

		// ������������� �� ������ �������������� � ��� �������
		glm::vec2 perp = fromCircleToScreenCenter.y <= 0.f ?
			glm::vec2(0.f, float(RENDERBUFFER_HEIGHT) / 2.f) : glm::vec2(0.f, -float(RENDERBUFFER_HEIGHT) / 2.f);
		auto angle = glm::angle(glm::normalize(fromCircleToScreenCenter), glm::normalize(fromCircleToScreenCenter));

		// ���������� �� ������ ������ �� ������� ������ � ����������� ������ ����������
		auto fromCenterToScreenBorder = SceneVector::FromScreenCoords(1.f / glm::tan(angle) * perp);

		// ���� ���������� �� ������� ������ �� ���������� ������ �������, ���������� true 
		if ((circleCenterPos - viewportCenterPos).Length() - fromCenterToScreenBorder.Length() <= circleRadius)
			return true;
	}
}