#include "stdafx.h"

#include "PropsFactory.h"
#include "ErrorLog.h"
#include "StringHelper.h"

namespace mg
{
	void PropsFactory::LoadFromFile(const std::string& filename)
	{
		std::ifstream file = std::ifstream(filename.c_str(), std::ios::in | std::ios::binary);

		if (!(bool)file)
		{
			ErrorLog::Log("Couldn't open props file " + filename);
			return;
		}

		auto filenameParts = StringHelper::GetFileNameParts(filename);

		std::shared_ptr<Cloneable> res = std::shared_ptr<Cloneable>(new Props());
		auto *resRawPtr = (Props*)res.get();

		resRawPtr->name = filenameParts[filenameParts.size() - 2];

		// --- �������� �����
		size_t versCount = 0;
		file.read((char*)(&versCount), sizeof(unsigned int));
		
		for (int i = 0; i < versCount; ++i)
		{
			AnimVertex animVert;

			// �������
			file.read((char*)(&animVert.vert.x), sizeof(float));
			file.read((char*)(&animVert.vert.y), sizeof(float));
			file.read((char*)(&animVert.vert.z), sizeof(float));

			// �������
			file.read((char*)(&animVert.normal.x), sizeof(float));
			file.read((char*)(&animVert.normal.y), sizeof(float));
			file.read((char*)(&animVert.normal.z), sizeof(float));

			resRawPtr->vertices.push_back(animVert);
		}

		// �������
		size_t idxCount;
		file.read((char*)(&idxCount), sizeof(unsigned int)); // ���-�� ��������

		for (int i = 0; i < idxCount; ++i)
		{
			unsigned int currIndex;
			file.read((char*)(&currIndex), sizeof(unsigned int));

			resRawPtr->indices.push_back(currIndex);
		}

		file.close();

		elements.insert(std::make_pair(resRawPtr->name, res));
	}

	std::shared_ptr<Cloneable> PropsFactory::CreateElement(const std::string& componentName, GameplayState *gps, const int &id)
	{
		auto componentFoundIter = elements.find(componentName);

		if (componentFoundIter != elements.end())
			return componentFoundIter->second;
		else
			throw std::invalid_argument("Invalid component name.");
	}
}