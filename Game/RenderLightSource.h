#pragma once

#include "glm/glm.hpp"

namespace mg {
	struct RenderLightSource
	{
		RenderLightSource(glm::vec3 pos, float radius, glm::vec3 color = glm::vec3(1.0))
			: pos(pos), radius(radius), color(color)
		{}

		glm::vec3 pos;
		glm::vec3 color;
		float radius;
	};
}