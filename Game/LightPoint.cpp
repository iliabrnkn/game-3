#include "stdafx.h"

#include "LightPoint.h"
#include "GameplayState.h"
#include "Sizes.h"

namespace mg
{
	void LightPointComponentManager::AssignComponent(entityx::Entity &entity, const LuaPlus::LuaObject &table) const
	{
		auto colorTable = LuaPlus::LuaHelper::GetTable(table, "color");
		auto positionOffsetTable = LuaPlus::LuaHelper::GetTable(table, "positionOffset", false);
		
		SceneVector positionOffset(0.f);
		if (!positionOffsetTable.IsNil())
			positionOffset = SceneVector::FromMapCoords(positionOffsetTable);

		entity.assign<LightPoint>(
			LuaPlus::LuaHelper::GetFloat(table, "radius"),
			glm::vec3(
				LuaPlus::LuaHelper::GetFloat(colorTable, "r"),
				LuaPlus::LuaHelper::GetFloat(colorTable, "g"),
				LuaPlus::LuaHelper::GetFloat(colorTable, "b")
			),
			LuaPlus::LuaHelper::GetBoolean(table, "dynamic"),
			LuaPlus::LuaHelper::GetBoolean(table, "active", false, true),
			positionOffset
		);
	}

	LuaPlus::LuaObject LightPointComponentManager::GetComponentTable(entityx::Entity& entity) const
	{
		LuaPlus::LuaObject res;

		if (entity.has_component<LightPoint>())
		{
			auto componentHandler = entity.component<LightPoint>();

			res.AssignNewTable(gps->GetLuaState());

			res.SetNumber("radius", componentHandler->radius);

			LuaPlus::LuaObject colorTable;
			colorTable.AssignNewTable(gps->GetLuaState());

			colorTable.SetNumber("r", componentHandler->color.x);
			colorTable.SetNumber("g", componentHandler->color.y);
			colorTable.SetNumber("b", componentHandler->color.z);

			res.SetBoolean("dynamic", componentHandler->dynamic);
			res.SetObject("color", colorTable);
		}
		else
		{
			res.AssignNil();
		}

		return res;
	};

	LuaPlus::LuaObject LightPointComponentManager::ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		// dynamic �������� ������ ���� ��� � ����� �� ��������

		if (!entity.has_component<LightPoint>())
			return LuaPlus::LuaObject().AssignNil();

		entity.component<LightPoint>()->radius = 
			LuaPlus::LuaHelper::GetFloat(table, "radius", false, entity.component<LightPoint>()->radius);

		auto colorTable = LuaPlus::LuaHelper::GetTable(table, "color", false);

		if (!colorTable.IsNil())
		{
			entity.component<LightPoint>()->color.x = LuaPlus::LuaHelper::GetFloat(colorTable, "r");
			entity.component<LightPoint>()->color.y = LuaPlus::LuaHelper::GetFloat(colorTable, "g");
			entity.component<LightPoint>()->color.z = LuaPlus::LuaHelper::GetFloat(colorTable, "b");
		}

		return GetComponentTable(entity);
	}

	void LightPointComponentManager::EnableLightPoint(const LuaPlus::LuaObject &entity, const bool &enabled) const
	{
		gps->GetEntity(entity).component<LightPoint>()->active = enabled;
	}

	bool LightPointComponentManager::IsLightPointEnabled(const LuaPlus::LuaObject &entity) const
	{
		return gps->GetEntity(entity).component<LightPoint>()->active;
	}

	void LightPointComponentManager::SetLightPointOffset(const LuaPlus::LuaObject &entity, const LuaPlus::LuaObject &pos) const
	{
		gps->GetEntity(entity).component<LightPoint>()->positionOffset = SceneVector::FromMapCoords(pos);
	}
}