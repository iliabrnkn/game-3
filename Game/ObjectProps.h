#pragma once

#include <glm\glm.hpp>

#include "Props.h"

namespace mg {

	struct ObjectProps {
		int entityID;
		glm::mat4 translateMatScreen;
		glm::mat4 translateMatScene;
		glm::vec4 mixColors;
	};

	struct LightProps {
		glm::vec4 pos;
		glm::vec3 color;
		float radius;
		int dynamicIndex;
		int staticIndex;
	};

	struct MeshProps
	{
		glm::mat4 transformMx;
		glm::vec4 material;
		int boneMxOffset;
		float padding1;
		float padding2;
		float padding3;

		MeshProps() : 
			transformMx(glm::mat4(1.f)), boneMxOffset(-1)
		{}
	};

	struct GidProps
	{
		float layerNum;
		float atlasNum;

		// �����������, ��������� �� �������������� ������� �������, �� ������� ���������� ������ �������� � ������� ������� ��������� �������
		glm::vec2 spriteSizeTextureOffsetFactor;

		GidProps(const int &layerNum, const int atlasNum = 0) :
			layerNum(layerNum), spriteSizeTextureOffsetFactor(0.f), atlasNum(atlasNum)
		{
		};

		GidProps(const int &layerNum, const PropsHandler &propsHandler, glm::vec2 spriteSize, const int atlasNum = 0) :
			layerNum(layerNum), spriteSizeTextureOffsetFactor(
				spriteSize.x * propsHandler->textureOffsetFactor.x,
				spriteSize.y * propsHandler->textureOffsetFactor.y), 
			atlasNum(atlasNum)
		{
		}

		GidProps() :
			layerNum(0)
		{
		}
	};

	struct EmitterProps {
		glm::vec4 startColor;
		glm::vec4 endColor;
		glm::vec2 singleParticleTexRatio; // ������ ����� �������� ������� ��������������� ������� ��������
		float startParticleSize;
		float endParticleSize;
		float maxLifeTime;
		int blurred;
		int original;
		float padding;
	};
}