#pragma once

#include <entityx\System.h>
#include "lua\lua.h"
namespace mg {

	class RenderingSystem2D : public entityx::System<RenderingSystem2D>
	{
		friend class GameplayState;
		
		GameplayState* gps;

		/*GLuint tilesetAtlasTexUnit;
		GLuint volumeAtlasTexUnit;
		GLuint normalAtlasTexUnit;*/

		unsigned int nextObjectID; // ��������� ��������, 0 �������������� ��� ����, 1 - ��� ������, ��� ��� ������ ���������� � 2
		glm::vec2 wallPiercingPointScreen;
		LuaPlus::LuaState* luaState;

	public:
		RenderingSystem2D(GameplayState* gps);

		virtual void configure(entityx::EventManager &events) override;
		virtual void update(entityx::EntityManager &es, entityx::EventManager &em, entityx::TimeDelta dt) override;
	};
}