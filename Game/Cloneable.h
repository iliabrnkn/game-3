#pragma once

namespace mg {
	
	struct Cloneable
	{
		std::string name;

		virtual ~Cloneable() = 0 {};
	};
}