#include "stdafx.h"

#include "Strikeable.h"
#include "Collideable.h"
#include "GameplayState.h"

namespace mg
{
	void StrikeableComponentManager::AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		entity.assign<Strikeable>(
			LuaPlus::LuaHelper::GetBoolean(table, "active", false));
	}

	LuaPlus::LuaObject StrikeableComponentManager::GetComponentTable(entityx::Entity& entity) const
	{
		LuaPlus::LuaObject res;

		if (entity.has_component<Strikeable>())
		{
			auto strikeable = entity.component<Strikeable>();
			
			res.AssignNewTable(gps->GetLuaState());
			res.SetNumber("active", strikeable->active);

		}

		return res;
	}

	LuaPlus::LuaObject StrikeableComponentManager::ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		return GetComponentTable(entity);
	}

	void StrikeableComponentManager::SetActiveStrikeable(const LuaPlus::LuaObject& entityTable, const bool &active)
	{
		auto entity = gps->GetEntity(entityTable);
		entity.component<Strikeable>()->active = active;
		//entity.component<Collideable>()->body->activate(active);
	}

	bool StrikeableComponentManager::IsActiveStrikeable(const LuaPlus::LuaObject& entityTable)
	{
		auto entity = gps->GetEntity(entityTable);
		return entity.component<Strikeable>()->active;
	}
}