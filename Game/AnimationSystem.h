#pragma once

#include <glm/glm.hpp>
#include <entityx\System.h>
#include <unordered_map>
#include <string>
#include <memory>

#include "Camera.h"
#include "Skeleton.h"
#include "Animation.h"
#include "Collideable.h"
#include "CustomMatrix.h"
#include "InverseKinematicsController.h"
#include "AIPiece.h"

namespace mg {

	struct Position;
	struct Direction;
	struct Moveable;

	class AnimationSystem : public entityx::System<AnimationSystem>/*, public entityx::Receiver<AnimationSystem>*/
	{
		friend class GameplayState;

		GameplayState* gps;
		void ApplyAnimationToSkeleton(const double &dt, Skeleton &skeleton, AnimationHandler& animationHandler, Moveable &mov, Position &pos, Direction &dir, AIPiece &aiPiece) const;
		std::string GetTrueParentName(const Skeleton &skeleton, const std::string &nodeName) const;
		std::string GetBoneBodyParentName(const Skeleton &skeleton, const std::string &nodeName) const;

	public:
		static bool debugLooseBodies;

		virtual void configure(entityx::EventManager &events) override;
		virtual void update(entityx::EntityManager &es, entityx::EventManager &em, entityx::TimeDelta dt) override;

		/*void receive(const AnimationChanging& changeRoutineAnimation);*/
		AnimationSystem(GameplayState* gps);

		btTransform GetBoneBodyTransformByJoint(const Skeleton &skeleton, const Position &pos, const Direction &dir, const std::string &jointName);
	};
}