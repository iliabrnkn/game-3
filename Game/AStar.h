#pragma once

#include <vector>
#include <algorithm>
#include <queue>
#include <memory>

#include <glm/glm.hpp>

#include "Vector.h"

namespace mg {

	struct AStarNode;

	typedef std::shared_ptr<AStarNode> AStarNodeHandler;

	struct AStarNode
	{
	public:
		float g;
		SceneVector pos;
		bool open, closed, type;

		AStarNodeHandler parent;

		// �������� ����, ������ � ����������� �� ���
		std::vector<AStarNodeHandler> neighbours;

		float DistanceTo(const AStarNodeHandler node) const
		{
			return (node->pos - pos).Length();
		}

		void Release()
		{
			open = closed = false;
			g = 0.0f;
			parent = nullptr;
		}

		//��������� ��������, ������� ��������� ��� ����������� ���� � �����
		inline void SetParent(AStarNodeHandler parent)
		{
			this->parent = parent;
		}

		// ���������� ��������� �� ������������ ���
		inline AStarNodeHandler GetParent() const
		{
			return parent;
		}

		// ���������� �������� ����
		inline std::vector<AStarNodeHandler>& GetNeighbours()
		{
			return neighbours;
		}

		~AStarNode()
		{}

		static inline AStarNodeHandler CreateAStarNode(const SceneVector &pos)
		{
			return std::shared_ptr<AStarNode>(new AStarNode(pos));
		}

	private:
		AStarNode(const SceneVector &pos) :
			g(0.f), pos(pos),
			closed(false), open(false), type(false)
		{}
	};

	/*class AStar
	{
	public:

		static AStar& getInstance()
		{
			static AStar instance;
			return instance;
		}

		bool GetPath(AStarNodeHandler start, AStarNodeHandler goal, std::vector<AStarNodeHandler> &path);

	private:

		AStar();
		~AStar();

		std::map<float, AStarNodeHandler> open;
	};*/
}