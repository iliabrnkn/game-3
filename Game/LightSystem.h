#pragma once

#include <vector>
#include <memory>

#include <entityx\System.h>
#include "Camera.h"
#include "VBO.h"
#include "Texture.h"
#include "Effect.h"
#include "lua\LuaPlus.h"
#include "ScreenBatch.h"
#include "ObjectProps.h"

namespace mg {

	struct Position;
	struct LightPoint;

	class LightSystem : public entityx::System<LightSystem>
	{
		friend class GameplayState;
		friend class Core;

		GameplayState* gps;
		VBO<glm::mat4> shadowMXsBuffer, projectionMXsBuffer;
		VBO<LightProps> lightProps;

		std::vector<glm::mat4> staticShadowTransforms;

		Texture lightsTransformsBufferTex, projectionMXsBufferTex;

	public:
		void configure(entityx::EventManager &events) override;
		void update(entityx::EntityManager &es, entityx::EventManager &em, entityx::TimeDelta dt) override;

		LightSystem(GameplayState* gameplayState);
	};

}