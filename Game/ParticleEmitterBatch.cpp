#include "stdafx.h"

#include <stdlib.h>
#include <time.h>

#include "ParticleEmitterBatch.h"
#include "Paths.h"
#include "OGLConstants.h"
#include "Model.h"
#include "Sizes.h"

namespace mg
{
	ParticlesBatch::ParticlesBatch() :
		ConcreteRenderBatch<Particle>()
	{}

	void ParticlesBatch::Init(LuaPlus::LuaState* luaState)
	{
		// ���
		srand(time(NULL));

		// initialize buffers
		vertices->Init(BUFFER_SIZE_512KB, GL_ARRAY_BUFFER);
		indices->Init(BUFFER_SIZE_512KB, GL_ELEMENT_ARRAY_BUFFER);

		// initialize VAO
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		vertices->Bind();
		indices->Bind();
		
		glEnableVertexAttribArray(0); // position
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), 0);
		glEnableVertexAttribArray(1); // lifeTime
		glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), BUFFER_OFFSET(12));
		glEnableVertexAttribArray(2); // startTexCoord
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Particle), BUFFER_OFFSET(16));
		glEnableVertexAttribArray(3); // startVelocity
		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), BUFFER_OFFSET(24));
		glEnableVertexAttribArray(4); // endVelocity
		glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), BUFFER_OFFSET(36));
		glEnableVertexAttribArray(5); // emitterNum
		glVertexAttribIPointer(5, 1, GL_INT, sizeof(Particle), BUFFER_OFFSET(48));

		glBindVertexArray(NULL);
		vertices->Unbind();
		indices->Unbind();

		CHECK_GL_ERROR
	}

	void ParticlesBatch::Draw()
	{
		glDrawArrays(GL_POINTS, 0, vertices->GetCurrentOffset());

		CHECK_GL_ERROR
	}
}