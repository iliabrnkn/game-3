#pragma once

#include <vector>
#include <memory>

#include <entityx\System.h>
#include <lua\LuaPlus.h>

#include "Camera.h"
#include "VBO.h"
#include "Texture.h"
#include "Effect.h"
#include "ScreenBatch.h"
#include "ObjectProps.h"

namespace mg {

	struct Position;
	struct LightPoint;
	struct Direction;

	class SpotLightSystem : public entityx::System<SpotLightSystem>
	{
		friend class GameplayState;
		friend class Core;

		GameplayState* gps;
		VBO<glm::mat4> dynamicShadowMXsBuffer, staticShadowMXsBuffer;
		VBO<LightProps> dirLightProps;

		std::vector<glm::mat4> staticShadowTransforms;

		Texture staticLightsTransformsBufferTex, dynamicShadowMXsBufferTex;

	public:
		void configure(entityx::EventManager &events) override;
		void update(entityx::EntityManager &es, entityx::EventManager &em, entityx::TimeDelta dt) override;

		SpotLightSystem(GameplayState* gameplayState);
	};
}