#pragma once

#include <string>
#include <vector>
#include <deque>
#include <unordered_map>
#include <memory>
#include <algorithm>

#include <lua\LuaPlus.h>

#include <SFML\Graphics\RenderTexture.hpp>
#include <SFML\Graphics\Text.hpp>

#include "Texture.h"
#include "Widget.h"

namespace mg {

	class GUI
	{
		friend class Core;
		LuaPlus::LuaState* luaState;

		// ����
		sf::Texture cursorTex, cursorBrightTex, sightTex, targetLockSightTex;
		sf::Sprite cursorSprite, aimCursorSprite, currentCursorSprite, targetLockAimSprite;
		sf::Shader shader;
		static int nextID;
		bool isCursorVisible;

	protected:
		GUI();
		~GUI();

	public:
		static GUI& GetInstance();
		void Init(sf::RenderWindow& window, LuaPlus::LuaState* luaState);
		void ConsoleLog(const std::string& entry);
		void GUI::ClearConsoleLog();
		const Texture& GetTexture() const;
		void Draw(sf::RenderWindow &window);
		void SetDefaultCursor(const bool &isHighlighted = false);
		sf::Shader &GetShader();
		void SetTargetLockAimCoords(const sf::Vector2f &screenCoords);
		void SetCursorVisibility(const bool &visible);

		// monitoring

		void Watch(const std::string& name, const float& value);
		void Watch(const std::string& name, const std::string& value);

		void RemoveWatch(const std::string& name);

		template <typename T>
		inline T* AddWidget(const WidgetParameters& params, const std::string& name = typeid(T).name())
		{
			auto* res = root->AddChild<T>(params, name);

			return res;
		}

		template <typename T>
		inline T* GetWidget(const std::string& name)
		{
			return root->GetChild<T>(name);
		}

		// ���������� ������ ��������� ������ �������� ���� �� �����������
		template <typename T>
		inline T* GetWidget(const sf::Vector2i &coords)
		{
			return root->GetChildByCoords<T>(coords, sf::Vector2i(0, 0));
		}

		inline void RemoveWidget(const std::string& name)
		{
			auto iter = std::remove_if(root->children.begin(), root->children.end(), [&name](auto& x) { return x->name == name; });

			if (iter != root->children.end())
			{
				root->children.erase(iter, iter + 1);
			}
		}

		inline const sf::Font& GetFont() 
		{
			return font;
		}

		void ClearRemovedWidgets();

		static inline int GetNextID()
		{
			return nextID++;
		}

		inline LuaPlus::LuaState* GetLuaState()
		{
			return luaState;
		}

		void ConsolePush(const char& symbol);
		void ConsoleFlush();

		bool HandleMouse(const sf::Vector2i& mousePosWindow);
		bool IsConsoleOn();
		void SetConsoleOn(const bool& on);
		void SetAimCursor(const float& factor = 0.f);
	
	private:
		void DrawConsoleLog(sf::RenderWindow &window, sf::Shader &shader);
		void DrawWatchVals(sf::RenderWindow &window, sf::Shader &shader);

		std::deque<std::string> consoleEntries;
		sf::Font font;
		Texture texture;
		std::string consoleBuffer;

		std::unordered_map<std::string, float> watchFloatVals;
		std::unordered_map<std::string, std::string> watchStrVals;
		/*std::vector<std::shared_ptr<Widget>> widgets;*/
		std::unique_ptr<Widget> root;
		bool isConsoleOn;
	};

}