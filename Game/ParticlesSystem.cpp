#include "stdafx.h"

#include <algorithm>

#include "GameplayState.h"
#include "ParticlesSystem.h"
#include "Level.h"
#include "ParticleEmitter.h"
#include "AIPiece.h"

namespace mg {
	void ParticlesSystem::configure(entityx::EventManager &events)
	{
		emitterPropsBuffer.Init(MAX_EMITTERS_COUNT * sizeof(EmitterProps), GL_UNIFORM_BUFFER);

		gps->eventManager.subscribe<entityx::ComponentRemovedEvent<ParticleEmitter>>(*this);
	}

	void ParticlesSystem::update(entityx::EntityManager &es, entityx::EventManager &em, entityx::TimeDelta dt)
	{
		std::vector<EmitterProps> emitterProps;
		std::vector<Particle> particles;

		es.each<ParticleEmitter, Position>([dt, this, &emitterProps, &particles](entityx::Entity& entity, ParticleEmitter& emitter, Position& pos)
		{
			EmitterProps currEmitterProps;

			// ��������� �������
			if (emitter.active)
			{
				auto respawnedParticles =
					static_cast<LuaPlus::LuaFunction<LuaPlus::LuaObject>>(emitter.particleRespawnFunc)(
						entity.component<AIPiece>()->table, glm::round(dt * emitter.particlesPerSec));

				for (LuaPlus::LuaTableIterator iter(respawnedParticles); iter.IsValid(); iter.Next())
				{
					auto lastUnusedParticleID = FindLastUnusedParticle(emitter);
					Particle &currParticle = emitter.particles[lastUnusedParticleID];
					currParticle.position = SceneVector::FromMapCoords(LuaHelper::GetTable(iter.GetValue(), "position"));
					currParticle.startVelocity = SceneVector::FromMapCoords(LuaHelper::GetTable(iter.GetValue(), "startVelocity"));
					currParticle.endVelocity = SceneVector::FromMapCoords(LuaHelper::GetTable(iter.GetValue(), "endVelocity"));
					currParticle.lifeTime = 0.f;
				}

				emitter.depleted = false;
			}

			// ���� �������� ������� ��� �������� � ��������, ������������ ��
			if (!emitter.depleted)
			{
				emitter.depleted = true;

				for (int i = 0; i < emitter.particles.size(); ++i)
				{
					auto &particle = emitter.particles[i];
					particle.emitterNum = emitter.emitterID;

					if (particle.position != glm::vec3(-100.f) && particle.lifeTime < emitter.maxLifeTime)
					{
						auto velocity = glm::mix(
							emitter.particles[i].startVelocity, 
							emitter.particles[i].endVelocity, 
							particle.lifeTime / emitter.maxLifeTime
						);
						particle.lifeTime += dt;
						particle.position += static_cast<float>(dt) * velocity;

						if (emitter.depleted) emitter.depleted = false;
					}
				}

				// ���� ������� ��� ��� �������, �� ������������� ��� �������, ����� �� �� ���������� �� ������
				if (emitter.depleted)
				{
					for (auto &particle : emitter.particles)
					{
						particle.position = SceneVector(-100.f);
						particle.lifeTime = 0.f;
						particle.startVelocity = SceneVector(0.f);
						particle.endVelocity = SceneVector(0.f);
					}
				}
				else
				{
					particles.reserve(particles.size() + emitter.particles.size());
					std::copy(emitter.particles.begin(), emitter.particles.end(), std::back_inserter(particles));
				}
			}

			currEmitterProps.startColor = emitter.startParticleColor;
			currEmitterProps.endColor = emitter.endParticleColor;
			currEmitterProps.startParticleSize = emitter.startParticleSize;
			currEmitterProps.endParticleSize = emitter.endParticleSize;
			currEmitterProps.singleParticleTexRatio = emitter.singleParticleTexRatio;
			currEmitterProps.maxLifeTime = emitter.maxLifeTime;
			currEmitterProps.blurred = (int)emitter.blur;
			currEmitterProps.original = (int)emitter.original;

			emitterProps.push_back(currEmitterProps);
		});

		// ��������� � ������� �� ������� � �������
		std::sort(particles.begin(), particles.end(), [this](Particle &first, Particle &sec) {
			return this->GetDepthVal(first.position) < this->GetDepthVal(sec.position);
		});

		gps->GetScene().GetParticlesBatch().Bind();
		gps->GetScene().GetParticlesBatch().ClearBuffers();
		gps->GetScene().GetParticlesBatch().GetVertices().Fill(particles);
		gps->GetScene().GetParticlesBatch().Unbind();

		emitterPropsBuffer.Bind();
		emitterPropsBuffer.Clear();
		emitterPropsBuffer.Fill(emitterProps);
		emitterPropsBuffer.Unbind();

		gps->GetScene().GetParticlesEffect().SetUniformMatrix4fDeferred(
			"mvp", 
			gps->GetCamera()->GetSceneMVP()
		);

		gps->GetScene().GetParticlesEffect().SetUniform1iDeferred(
			"particleTex", 
			gps->GetLevel()->GetParticleAtlas().GetTexture()->GetTextureUnit()
		);

		gps->GetScene().GetParticlesEffect().SetUniformBlockDeferred(
			"emitterBlock",
			0,
			emitterPropsBuffer
		);
	}

	/*void ParticlesSystem::RespawnParticle(ParticleEmitter &emitter, const Position &pos, const size_t &id)
	{
		if (emitter.type == FIRE_PARTICLE)
		{
			auto randX = (rand() % 2 == 1 ? -1.f : 1.f) * emitter.coreRadius * 1.f / float(max(1, rand() % 100));
			auto randZ = (rand() % 2 == 1 ? -1.f : 1.f) * emitter.coreRadius * 1.f / float(max(1, rand() % 100));
			auto randY = (rand() % 2 == 1 ? -1.f : 1.f) * emitter.coreRadius * 1.f / float(max(1, rand() % 100));

			emitter.particles[id].pos = SceneVector(randX, randY, randZ) * emitter.coreRadius + pos;
			emitter.particles[id].initialVelocity = SceneVector(randX, randY, randZ).Normalized();
		}
		else if (emitter.type == FOG_PARTICLE)
		{
			auto randX = (rand() % 2 == 1 ? -1.f : 1.f) * emitter.coreRadius * 1.f / float(max(1, rand() % 100));
			auto randZ = (rand() % 2 == 1 ? -1.f : 1.f) * emitter.coreRadius * 1.f / float(max(1, rand() % 100));
			auto randY = (0.f);

			emitter.particles[id].pos = SceneVector(randX, randY, randZ) * emitter.coreRadius + pos;
			emitter.particles[id].initialVelocity = SceneVector(randX, randY, randZ).Normalized();
			emitter.particles[id].type = 1 + rand() % FOG_PARTICLE_TYPES;
		}
		else if (emitter.type == GUNSHOT_PARTICLE)
		{
			auto randX = -emitter.coreRadius * 1.f / float(max(1, rand() % 100));
			auto randZ = (rand() % 2 == 1 ? -1.f : 1.f) * emitter.coreRadius * 1.f / (10.f * float(max(1, rand() % 100)));
			auto randY = (rand() % 2 == 1 ? -1.f : 1.f) * emitter.coreRadius * 1.f / (10.f * float(max(1, rand() % 100)));

			emitter.particles[id].pos = SceneVector(randX, randY, randZ) * emitter.coreRadius + pos;
			emitter.particles[id].initialVelocity = SceneVector(randX, randY, randZ).Normalized();
		}

		emitter.particles[id].lifeTime = 0.f;
	}*/

	ParticlesSystem::ParticlesSystem(GameplayState* gps) :
		gps(gps)
	{}

	void ParticlesSystem::receive(const entityx::ComponentRemovedEvent<ParticleEmitter>& ev)
	{
		/*if (ev.entity.has_component<ParticleEmitter>())
		{*/
			//GUI::GetInstance().ConsoleLog("emitter removed");
			ParticleEmitterComponentManager::EmitterCountDecrement();

			int emitterCount = 0;

			gps->entityManager.each<ParticleEmitter>([this, &emitterCount, &ev](entityx::Entity& entity, ParticleEmitter& emitter)
			{
				if (ev.component->emitterID != emitter.emitterID)
				{
					emitter.emitterID = emitterCount++;
					//GUI::GetInstance().ConsoleLog("emitter.emitterID = " + std::to_string(emitter.emitterID));
				}
			});
		//}
	}
}