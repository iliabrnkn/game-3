#pragma once

#include <GL\glew\glew.h>

struct GraphicsComponent
{
	GLuint bufferOffset;
	GLuint bufferLength;
};