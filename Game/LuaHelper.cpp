#include "stdafx.h"

#include "LuaHelper.h"

namespace mg {
	btCollisionShape* LuaHelper::GetCollisionShape(const LuaPlus::LuaObject &table)
	{
		std::string type = LuaHelper::GetString(table, "type");
		btCollisionShape *shape;

		if (type == "capsule" || type == "capsuleX" || type == "capsuleZ") // �������
		{
			double radius = LuaHelper::GetFloat(table, "radius");
			double capsuleHeight = LuaHelper::GetFloat(table, "height");

			if (type == "capsule")
				shape = new btCapsuleShape(radius, capsuleHeight);
			else if (type == "capsuleZ")
				shape = new btCapsuleShapeZ(radius, capsuleHeight);
			else if (type == "capsuleX")
				shape = new btCapsuleShapeX(radius, capsuleHeight);
		}
		else if (type == "cylinder" || type == "cylinderX" || type == "cylinderZ") // �������
		{
			double radius = table["radius"].GetNumber();
			double height = table["height"].GetNumber();

			if (type == "cylinder")
				shape = new btCylinderShape(btVector3(radius, height / 2.0, radius));
			else if (type == "cylinderZ")
				shape = new btCylinderShape(btVector3(radius, radius, height / 2.0));
			else if (type == "cylinderX")
				shape = new btCylinderShape(btVector3(height / 2.0, radius, radius));
		}
		else if (type == "sphere") // �����
		{
			double radius = table["radius"].GetNumber();

			shape = new btSphereShape(radius);
		}
		else if (type == "box") // ��������������
		{
			double length = table["length"].GetNumber(); // x
			double height = table["height"].GetNumber(); // y
			double width = table["width"].GetNumber(); // z

			shape = new btBoxShape(btVector3(length / 2.0, height / 2.0, width / 2.0));
		}
		else if (type == "singleJointCylinder")
		{
			// TODO: ������ ��� ����� ��������� / ������� �� MODEL_SCALE
			float radius = LuaHelper::GetFloat(table, "radius");
			float length = LuaHelper::GetFloat(table, "length");

			shape = new btCylinderShape(btVector3(radius, length / 2.f, radius));
		}
		else if (type == "shiftedCylinder")
		{
			// TODO: ������ ��� ����� ��������� / ������� �� MODEL_SCALE
			float radius = LuaHelper::GetFloat(table, "radius");
			float length = LuaHelper::GetFloat(table, "length");

			//assert(skeleton.boneHeadBindingPoints.find(joint) != skeleton.boneHeadBindingPoints.end());

			auto *childShape = new btCylinderShape(btVector3(radius, length / 2.f, radius));

			auto localTransform = LuaHelper::GetBTTransform(LuaHelper::GetTable(table, "localTransform"));

			shape = new btCompoundShape();
			reinterpret_cast<btCompoundShape*>(shape)->addChildShape(localTransform, childShape);
		}

		return shape;
	}
}