#include "stdafx.h"

#include "Vector.h"
#include "Camera.h"
#include "GameplayState.h"

namespace mg
{
	SceneVector SceneVector::X_UNIT(1.f, 0.f, 0.f);
	SceneVector SceneVector::Y_UNIT(0.f, 1.f, 0.f);
	SceneVector SceneVector::Z_UNIT(0.f, 0.f, 1.f);

	// �� ��������� ������
	SceneVector SceneVector::FromScreenCoords(const glm::vec2 &screenCoords, const Camera *camera)
	{
		return MatrixHelper::ScreenToScene(screenCoords, camera);
	}

	// �� ��������� ������
	SceneVector SceneVector::FromScreenCoords(const glm::vec2 &screenCoords, const std::shared_ptr<Camera> camera)
	{
		return MatrixHelper::ScreenToScene(screenCoords, camera.get());
	}

	LuaPlus::LuaObject SceneVector::ToMapCoords(LuaPlus::LuaState *luaState) const
	{
		return LuaHelper::Vec3ToTable(MatrixHelper::MapSceneSwap(vec), luaState);
	}
}