#pragma once

#include <string>
#include <vector>

namespace mg
{
	class StringHelper
	{
		StringHelper() {}
		~StringHelper() {}

	public:

		inline static std::vector<std::string> GetFileNameParts(const std::string& fullFilename)
		{
			std::vector<std::string> res;
			res.reserve(30);
			auto lastDelimiter = fullFilename.begin();

			for (auto iter = fullFilename.begin(); iter != fullFilename.end(); iter++)
			{
				if (*iter == '/' || *iter == '\\' || *iter == '.')
				{
					std::string part = fullFilename.substr(lastDelimiter - fullFilename.begin(), iter - lastDelimiter);

					res.push_back(part);

					lastDelimiter = iter + 1;
				}
			}

			res.push_back(fullFilename.substr(lastDelimiter - fullFilename.begin(), fullFilename.end() - lastDelimiter));
		
			return res;
		}
	};
}