#include "stdafx.h"

#include "NavigationMesh.h"
#include "Sizes.h"
#include "MathHelper.h"

namespace mg
{
	void NavigationMesh::Init(GameplayState* gps, const LuaPlus::LuaObject &table)
	{
		auto objects = LuaHelper::GetTable(table, "objects");

		for (LuaPlus::LuaTableIterator iter(objects); iter; iter.Next())
		{
			auto object = iter.GetValue();

			if (LuaHelper::GetString(object, "shape") == "rectangle")
			{
				float x = LuaHelper::GetFloat(object, "x");
				float y = LuaHelper::GetFloat(object, "y");
				float width = LuaHelper::GetFloat(object, "width");
				float height = LuaHelper::GetFloat(object, "height");

				navRects.push_back(
					NavigationRect(
						SceneVector::FromMapCoords(
							x / MAP_TILE_STEP,
							y / MAP_TILE_STEP
						),
						SceneVector::FromMapCoords((width + x) / MAP_TILE_STEP, (height + y) / MAP_TILE_STEP)
					)
				);

				navRects.back().AddNode(AStarNode::CreateAStarNode(
					SceneVector::FromMapCoords(
						(x + width / 2.f) / MAP_TILE_STEP, 
						(y + height / 2.f) / MAP_TILE_STEP
					)
				));
			}

			// ���������� �� �������� ���������������
			for (int i = 0; i < navRects.size(); ++i)
			{
				auto &currRect = navRects[i];

				for (int j = i + 1; j < navRects.size(); ++j)
				{
					auto &otherRect = navRects[j];

					// ���� �������������� ������������� �� �����, ������������ OX
					if (currRect.boundingMin.Z() == otherRect.boundingMax.Z()
						|| currRect.boundingMax.Z() == otherRect.boundingMin.Z())
					{
						float zCoord = currRect.boundingMin.Z() == otherRect.boundingMax.Z() ?
							currRect.boundingMin.Z() : currRect.boundingMax.Z();
						float middleXCoord, minIntersectXCoord, maxIntersectXCoord;

						if (MathHelper::IntersectsLinearSegments(
							LinearSegment(currRect.boundingMin.X(), currRect.boundingMax.X()),
							LinearSegment(otherRect.boundingMin.X(), otherRect.boundingMax.X()),
							middleXCoord, minIntersectXCoord, maxIntersectXCoord))
						{
							auto minNode = AStarNode::CreateAStarNode(SceneVector(minIntersectXCoord, 0.f, zCoord));
							auto middleNode = AStarNode::CreateAStarNode(SceneVector(middleXCoord, 0.f, zCoord));
							auto maxNode = AStarNode::CreateAStarNode(SceneVector(maxIntersectXCoord, 0.f, zCoord));

							currRect.AddNode(minNode);
							currRect.AddNode(middleNode);
							currRect.AddNode(maxNode);

							otherRect.AddNode(minNode);
							otherRect.AddNode(middleNode);
							otherRect.AddNode(maxNode);
						}
					}
					// ���� �� �����, ������������ OZ
					else if (
						currRect.boundingMin.X() == otherRect.boundingMax.X()
						|| currRect.boundingMax.X() == otherRect.boundingMin.X())
					{
						float xCoord = currRect.boundingMin.X() == otherRect.boundingMax.X() ?
							currRect.boundingMin.X() : currRect.boundingMax.X();
						float middleZCoord, minIntersectZCoord, maxIntersectZCoord;;

						if (MathHelper::IntersectsLinearSegments(
							LinearSegment(currRect.boundingMin.Z(), currRect.boundingMax.Z()),
							LinearSegment(otherRect.boundingMin.Z(), otherRect.boundingMax.Z()),
							middleZCoord, minIntersectZCoord, maxIntersectZCoord)
							)
						{
							auto node = AStarNode::CreateAStarNode(SceneVector::FromMapCoords(xCoord, middleZCoord));

							auto minNode = AStarNode::CreateAStarNode(SceneVector(xCoord, 0.f, minIntersectZCoord));
							auto middleNode = AStarNode::CreateAStarNode(SceneVector(xCoord, 0.f, middleZCoord));
							auto maxNode = AStarNode::CreateAStarNode(SceneVector(xCoord, 0.f, maxIntersectZCoord));

							currRect.AddNode(minNode);
							currRect.AddNode(middleNode);
							currRect.AddNode(maxNode);

							otherRect.AddNode(minNode);
							otherRect.AddNode(middleNode);
							otherRect.AddNode(maxNode);
						}
					}
				}
			}
		}
	}

	AStarNodeHandler NavigationMesh::FindClosestNode(const SceneVector &point)
	{
		AStarNodeHandler res;
		std::set<AStarNodeHandler> usedNodes;
		float minLen = 1000.f;

		for (auto &rect : navRects)
		{
			for (auto node : rect.nodes)
			{
				if (usedNodes.find(node) != usedNodes.end())
					continue;

				usedNodes.insert(node);

				auto currLen = (node->pos - point).Length();

				if (currLen < minLen)
				{
					minLen = currLen;
					res = node;
				}
			}
		}

		return res;
	}

	std::vector<SceneVector> NavigationMesh::FindPath(const SceneVector& startPoint, const SceneVector& endPoint)
	{
		// ������� ����� ��������������� ����������� ������ � �����
		auto* startRect = GetNavRectContainingPoint(startPoint);
		auto* endRect = GetNavRectContainingPoint(endPoint);

		// ���� �������� ����� �� ����������� ����, ���������� ������� ���������
		/*if (!endRect)
			return std::vector<SceneVector>();*/

		// ���� ��������� � �������� ����� ����� � ����� ��������������
		if (startRect == endRect)
		{
			return std::vector<SceneVector> { endPoint, startPoint };
		}

		AStarNodeHandler start, goal;

		// ����� ����������� ����� ���� � ��������������� � ��������� �������� A*
		if (startRect)
		{
			start = AStarNode::CreateAStarNode(startPoint);
			startRect->AddNode(start);
		}
		else
		{
			start = FindClosestNode(startPoint);
		}

		if (endRect)
		{
			goal = AStarNode::CreateAStarNode(endPoint);
			endRect->AddNode(goal);
		}
		else
		{
			goal = FindClosestNode(endPoint);
		}

		// ��������
		std::map<float, AStarNodeHandler> open;

		open.insert(std::make_pair(start->DistanceTo(goal), start));
		start->open = true;

		std::vector<SceneVector> path;

		while (!open.empty())
		{
			auto topOpenPair = open.begin();
			auto currentNode = topOpenPair->second; // ����� ������� ��� �� ������ ��������

			currentNode->open = false;
			open.erase(topOpenPair->first);
			currentNode->closed = true;

			if (currentNode == goal)
			{
				path = ReconstructPath(currentNode);
				break;
			}

			// ��� ������� ��������� ����
			for (auto neighbourNode : currentNode->GetNeighbours())
			{
				if (neighbourNode->closed)
					continue; // ��������� � ���������� ����

				// ������ ����� ���������� �� ������ �� �������� ����
				neighbourNode->g = glm::sqrt(currentNode->g) + currentNode->DistanceTo(neighbourNode);
				neighbourNode->SetParent(currentNode);

				auto h = neighbourNode->DistanceTo(goal);
				auto f = neighbourNode->g + h;

				if (!neighbourNode->open)
				{
					open.insert(std::make_pair(f, neighbourNode));
				}
			}
		}

		if (startRect) 
			startRect->RemoveNode(start);
 
		if (endRect)
			endRect->RemoveNode(goal);

		for (auto &rect : navRects)
		{
			for (auto &node : rect.nodes)
			{
				node->g = 0;
				node->open = node->closed = false;
				node->parent = nullptr;
			}
		}

		return path;
	}

	NavigationRect *NavigationMesh::GetNavRectContainingPoint(const SceneVector &point)
	{
		for (auto &navRect : navRects)
		{
			if (MathHelper::RectContainsPoint(navRect.boundingMin, navRect.boundingMax, point))
			{
				return &navRect;
			}
		}

		return nullptr;
	}

	std::vector<SceneVector> NavigationMesh::ReconstructPath(AStarNodeHandler node)
	{
		std::vector<SceneVector> path;

		auto parent = node->GetParent();
		path.push_back(node->pos);

		while (parent != nullptr)
		{
			path.push_back(parent->pos);
			parent = parent->GetParent();
		}

		/*std::vector<SceneVector> res;

		for (auto rIter = path.rbegin(); rIter != path.rend(); rIter++)
			res.push_back(*rIter);*/

		return path;
	}
}