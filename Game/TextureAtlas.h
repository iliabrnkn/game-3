#pragma once

#include "stdafx.h"

#include <limits>
#include <memory>

#include <glm/glm.hpp>

#include <SFML\Graphics.hpp>
#include "TilesetStructs.h"
#include "Texture.h"

using namespace std;

namespace mg{

	class TextureAtlas
	{
	public:

		// ���� tileWidth ��� tileHeight ����� 0, �� ��������������� ��������� ����� ����� ���������� ��������
		void AddTexture(
			const int& firstGidInit,
			const int& singleYOffsetInit,
			const int& tileCountInit,
			const string& tileset,
			const unsigned int& tileWidth = 0,
			const unsigned int& tileHeight = 0
		);

		TextureAtlas(void);
		~TextureAtlas(void);

		void InitTexture();
		void Init();

		GidTex* GetGidTex(int gidNum);
		void SetGidDrawingOffset(int gidNum, glm::vec2 offset);
		Texture* GetTexture();
		inline int GetGidCount() {
			return gidTexes.size();
		}

		inline bool HasGid(const int &gid) const
		{
			if (gidTexes.find(gid) != gidTexes.end())
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		inline size_t GetTilesetImagesCount() const
		{
			return tilesetImages.size();
		}

	private:
		unsigned int FindNearestPot(unsigned int val);
		int layerWidth, layerHeight, layerNum;
		
		// ���������� �������� � ��������� � ������
		size_t currentAtlasSize;

	protected:
		void DrawTilesetImage(sf::RenderTexture& atlasRenderTexture, TilesetImage& tilesetImage);

		vector<TilesetImage> tilesetImages;
		map<int, GidTex> gidTexes;
		map<int, glm::vec2> drawingOffsetsByGidNums;
		std::vector<sf::Image> images;
		Texture texture;
	};

}