#pragma once

#include <gl/glew/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <lua\LuaPlus.h>

#include <btBulletDynamicsCommon.h>

namespace mg {

	class Camera;
	class SceneVector;

	class MatrixHelper
	{
	public:
		// TODO: ��� ����

		static glm::mat4 mapSceneSwapMx;
		static glm::mat4 viewMx;
		static glm::mat4 screenToNDMx; // ������� �������� �� ��������� ������ � ��������������� ����������� ����������
		static glm::mat4 projectionMx, dirLightProjectionMx;

		static glm::mat4 screenToSceneMx;
		static glm::mat4 sceneToScreenMx;
		static glm::mat4 coreViewMx;
		static glm::mat4 dirLightViewMX;

		// �� ��������� ����� Tiled � ����������� ����� � �������
		static glm::vec3 MapSceneSwap(const glm::vec3 &pos);

		// �� ��������� ������ � ����������� �����
		static glm::ivec2 SceneToScreen(const glm::vec3 &scenePos, const Camera *camera);

		static glm::ivec2 SceneToScreenAbs(const glm::vec3 &scenePos);
		static glm::vec2 SceneToScreenAbsF(const glm::vec3 &scenePos);
		
		// � �������
		static glm::vec3 ScreenToScene(const glm::vec2 &coords, const Camera *camera);
		static glm::vec3 ScreenToSceneAbs(const glm::vec2 &coords);

		static inline glm::dmat4 ToGLMMatrix(const btTransform &t)
		{
			glm::dmat4 res;
			t.getOpenGLMatrix(glm::value_ptr<double>(res));

			return res;
		}

		static inline btTransform ToBTTransfom(const glm::dmat4 &m)
		{
			btTransform res;
			res.setFromOpenGLMatrix(glm::value_ptr(m));

			return res;
		}

		// ������� ��������� ����� � ���������� �����������
		static SceneVector SceneToViewer(const SceneVector &sceneCoords);

		// �������
		static SceneVector ViewerToScene(const SceneVector &viewerCoords);

		static inline btTransform ToBTTransfom(const glm::mat4 &m)
		{
			return ToBTTransfom(static_cast<glm::dmat4>(m));
		}

		static bool IsValid(const glm::mat3& m);

		static inline glm::mat4 BTQuatToMat(const btQuaternion &q)
		{
			return glm::toMat4(glm::quat(q.w(), q.x(), q.y(), q.z()));
		}

		static inline btMatrix3x3 BTQuatToBTBasis(const btQuaternion &q)
		{
			return ToBTTransfom(glm::toMat4(glm::quat(q.w(), q.x(), q.y(), q.z()))).getBasis();
		}
	};
}