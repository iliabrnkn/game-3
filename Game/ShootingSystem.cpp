#include "stdafx.h"
#include "ParticleEmitter.h"
#include "ShootingSystem.h"
#include "Position.h"
#include "Tracer.h"
#include "AIPiece.h"
#include "GameplayState.h"
#include "Collideable.h"

namespace mg {
	ShootingSystem::ShootingSystem(GameplayState* gps) :
		gps(gps)
	{}

	void ShootingSystem::configure(entityx::EventManager &events)
	{
	}

	void ShootingSystem::update(entityx::EntityManager &es, entityx::EventManager &em, entityx::TimeDelta dt)
	{
		std::vector<LineVertex> vertices;

		es.each<Tracer, AIPiece, Collideable>([dt, &es, this, &vertices](entityx::Entity& entity, 
			Tracer &tracer, AIPiece &aiPiece, Collideable &collideable)
		{
			for (auto &iter = tracer.traces.begin(); iter != tracer.traces.end();)
			{
				// �������, ��� ��������� �����

				btCollisionWorld::ClosestRayResultCallback rayResultCallback(iter->startPoint, iter->endPoint);
				rayResultCallback.m_collisionFilterGroup = COLLISION_GROUPS.at("character_joints");
				rayResultCallback.m_collisionFilterMask = COLLISION_MASKS.at("character_joints");
				gps->GetPhysics().dynamicsWorld->rayTest(iter->startPoint, iter->endPoint, rayResultCallback);

				if (rayResultCallback.hasHit())
				{
					if (rayResultCallback.m_collisionObject)
					{
						SceneVector hitCoords = rayResultCallback.m_closestHitFraction * (iter->endPoint - iter->startPoint) +
							iter->startPoint;
						iter->endPoint = hitCoords;

						LuaPlus::LuaObject args;
						args.AssignNewTable(gps->GetLuaState());
						args.SetObject("atacker", aiPiece.table);
						args.SetObject("hitNormalWorld",
							SceneVector(rayResultCallback.m_hitNormalWorld).ToMapCoords(gps->GetLuaState())
						);
						args.SetObject("hitCoords", hitCoords.ToMapCoords(gps->GetLuaState()));
						args.SetObject("traceDir", (iter->endPoint - iter->startPoint).Normalized().ToMapCoords(gps->GetLuaState()));

						if (rayResultCallback.m_collisionObject->getUserIndex() != -1 &&
							rayResultCallback.m_collisionObject->getUserIndex() != entity.id().index())
						{
							auto ent = gps->GetEntity(rayResultCallback.m_collisionObject->getUserIndex());

							/*if (ent.has_component<Skeleton>())
							{
								auto *body = const_cast<btRigidBody*>(
									reinterpret_cast<const btRigidBody*>(
										rayResultCallback.m_collisionObject
									)
								);

								body->applyImpulse(
									(iter->endPoint - iter->startPoint).Normalized() * 100.f,
									body->getWorldTransform().inverse() * hitCoords
								);

								ent.component<Skeleton>()->affected = true;
							}*/
							args.SetObject("targetEntity", ent.component<AIPiece>()->table);
							args.SetInteger("targetBodyWorldIndex", rayResultCallback.m_collisionObject->getWorldArrayIndex());

							ent.component<AIPiece>()->Emit(
								"onGetHit", args
							);

							aiPiece.Emit("onHit", args);
						}
						else // ���� ���������
						{
							aiPiece.Emit("onMiss", args);
						}
					};
				}

				// �������� ������ ������ �����
				vertices.push_back(
					LineVertex(iter->startPoint, glm::vec4(0.f))
				);

				vertices.push_back(
					LineVertex(iter->endPoint, glm::vec4(1.f))
				);

				iter = tracer.traces.erase(iter);
			}
		});

		// ������������ VBO
		gps->GetScene().GetLineBatch().Bind();
		gps->GetScene().GetLineBatch().GetVertices().Clear();
		gps->GetScene().GetLineBatch().GetVertices().Fill(vertices);
		gps->GetScene().GetLineBatch().Unbind();

		vertices.clear();

		gps->GetScene().GetLinesEffect().SetUniformMatrix4fDeferred("mvp", gps->GetCamera()->GetSceneMVP());
	}
}