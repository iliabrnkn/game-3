#pragma once

#include <sfml/Graphics.hpp>
#include <glm/glm.hpp>

#include "Core.h"

namespace mg {

	std::vector<std::string> GetFileNameParts(const std::string& fullFilename)
	{
		std::vector<std::string> res;
		res.reserve(30);
		auto lastDelimiter = fullFilename.begin();

		for (auto iter = fullFilename.begin(); iter != fullFilename.end(); iter++)
		{
			if (*iter == '/' || *iter == '\\' || *iter == '.')
			{
				std::string part = fullFilename.substr(lastDelimiter - fullFilename.begin(), iter - lastDelimiter);

				res.push_back(part);

				lastDelimiter = iter + 1;
			}
		}

		res.push_back(fullFilename.substr(lastDelimiter - fullFilename.begin(), fullFilename.end() - lastDelimiter));

		return res;
	}

	std::string GetFileNameWithoutResolution(const std::string& filename)
	{
		auto fileNameParts = GetFileNameParts(filename);
		std::string res;

		if (fileNameParts.size() > 1)
		{
			for (int i = 0; i < fileNameParts.size() - 1; ++i)
			{
				res = res.size() > 0 ? res + "/" + fileNameParts[i] : fileNameParts[i];
			}
		}

		return res;
	}
}