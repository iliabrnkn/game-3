#pragma once
#pragma once

#include <glm/glm.hpp>

#include <bullet/BulletCollision/CollisionDispatch/btGhostObject.h>
#include <bullet/btBulletDynamicsCommon.h>
#include <lua\LuaPlus.h>

#include "Model.h"
#include "ModelManagers.h"
#include "ComponentManager.h"
#include "GameplayState.h"
#include "Collideable.h"
#include "LuaHelper.h"

namespace mg {

	const glm::mat4 WEAPON_BONE_BINGIND_MXS = glm::mat4(); // ��������

	struct Weapon
	{
		Weapon(LuaPlus::LuaObject table, mgGhostObject* collisionObject, btCollisionShape* collisionShape, Mesh& mesh,
			const glm::mat4 &offsetMat, const float &gunshotOffsetTranslateCoefficient = 1.f, const glm::mat4 &gunshotOffsetMx = glm::mat4(1.f)) :
			table(table), collisionObject(collisionObject), collisionShape(collisionShape), 
			mesh(mesh), offsetMatrix(offsetMat), removed(false), gunshotOffsetTranslateCoefficient(gunshotOffsetTranslateCoefficient), gunshotOffsetMx(gunshotOffsetMx),
			shotCoords(std::vector<glm::vec3>())
		{
		}

		~Weapon() {}

		void Free()
		{
			delete collisionShape;
			delete collisionObject;
		}

		Mesh mesh;
		LuaPlus::LuaObject table;
		bool removed;
		
		mgGhostObject* collisionObject;
		btCollisionShape* collisionShape;
		glm::mat4 gunshotOffsetMx;

		// ������� ������������� � ������� ��������� ���������
		glm::mat4 localTransform;
		glm::mat4 offsetMatrix;
		float gunshotOffsetTranslateCoefficient;
		glm::vec3 absPos;
		std::vector<glm::vec3> shotCoords;
	};

	// Position component manager

	class WeaponComponentManager : public ComponentManager
	{
	public:
		WeaponComponentManager() : ComponentManager("weapon") {};
		~WeaponComponentManager() = default;

		inline virtual void AssignComponent(entityx::Entity& entity, LuaPlus::LuaObject& table, GameplayState* gps) const override
		{
			if (!entity.has_component<Weapon>())
			{
				bool isOk = true;

				std::string meshName;

				if (!(table.GetByName("mesh").GetRef() != -1 && table.GetByName("mesh").IsString()))
				{
					isOk = false;
				}

				meshName = table.GetByName("mesh").ToString();

				if (!gps->HasElement<Mesh>(meshName))
					isOk = false;

				btCollisionShape* shape = nullptr;
				mgGhostObject* collisionObject = nullptr;

				if (table.GetByName("collisionObject").GetRef() != -1)
				{
					if (!table.GetByName("collisionObject").IsTable()
						|| !(table["collisionObject"].GetByName("shape").GetRef() != -1 && table["collisionObject"].GetByName("shape").IsString()))
						isOk = false;

					if (std::string(table["collisionObject"]["shape"].ToString()) == "capsule" && isOk)
					{
						if ((table["collisionObject"].GetByName("radius").GetRef() != -1 && table["collisionObject"].GetByName("radius").IsNumber())
							&& (table["collisionObject"].GetByName("height").GetRef() != -1 && table["collisionObject"].GetByName("height").IsNumber()))
						{
							// ���� ��� ������� � ��� ��������� �� �����
							shape = new btCapsuleShape(table["collisionObject"]["radius"].GetFloat(), table["collisionObject"]["height"].GetFloat());

							collisionObject = new mgGhostObject(entity.id().index(), true);
							collisionObject->setCollisionShape(shape);
							gps->GetPhysics().dynamicsWorld->addCollisionObject(collisionObject, 2, 1);
						}
						else isOk = false;
					}
					else isOk = false;
				}

				glm::mat4 offsetMat = LuaHelper::TableToMat4(table["offsetMx"]);
				
				float gunshotOffsetTranslateCoefficient = 0.f;
				
				if (table.GetByName("gunshotOffsetTranslateCoefficient").GetRef() != -1)
				{
					gunshotOffsetTranslateCoefficient = table["gunshotOffsetTranslateCoefficient"].ToNumber();
				}

				glm::mat4 gunshotOffsetMx(1.f);

				if (table.GetByName("gunshotOffsetMx").GetRef() != -1)
				{
					gunshotOffsetMx = LuaHelper::TableToMat4(table["gunshotOffsetMx"]);
				}

				int meshID = -1;
				if (entity.has_component<MeshBatch>())
				{
					meshID = entity.component<MeshBatch>()->meshes[0].ID;
				}
				else isOk = false;

				if (isOk)
				{
					entity.assign<Weapon>(table, collisionObject, shape, *gps->CreateElement<Mesh>(meshName,
						meshID + CHARS_MESHES_COUNT), offsetMat, gunshotOffsetTranslateCoefficient, gunshotOffsetMx); // �������� �������� meshID + MAX_MESHES, ����� ����������, ��� ��� �������� �������
				}
			}
		}

		inline virtual LuaPlus::LuaObject GetComponentTable(entityx::Entity& entity, LuaPlus::LuaState* luaState) const override
		{
			if (entity.has_component<Weapon>())
			{
				auto *component = entity.component<Weapon>().get();
				return component->table;
			}
			else
			{
				LuaPlus::LuaObject res(luaState);
				res.AssignNil();

				return res;
			}
		}

		inline virtual LuaPlus::LuaObject ModifyComponent(entityx::Entity& entity, LuaPlus::LuaObject& table, GameplayState* gps) const override
		{
			LuaPlus::LuaObject res;

			// ���������, ���������� �� �������
			if (table.GetByName("shot").GetRef() != -1 && entity.has_component<Weapon>())
			{
				entity.component<Weapon>()->shotCoords.push_back(LuaHelper::TableToVec3(table["shot"])); // ���� 10.f
			}

			return res;
		}

		inline virtual void RemoveComponent(entityx::Entity& entity, GameplayState* gps) const
		{
			if (entity.has_component<Weapon>())
			{
				entity.component<Weapon>()->removed = true;
			}
		}
	};

}