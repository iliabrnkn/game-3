#pragma once

#include <vector>

#include "RenderBatch.h"
#include "Texture.h"

namespace mg
{
	class ParticlesBatch : public ConcreteRenderBatch<Particle>
	{
		friend class RenderingSystem3D;

	public:
		ParticlesBatch();
		virtual void Init(LuaPlus::LuaState* luaState) override;
		virtual void Draw() override;
		unsigned int fakeIndex;

		std::vector<Particle> vrts;
	};
}