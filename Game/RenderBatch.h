#pragma once

#include <memory>

#include <lua\LuaPlus.h>

#include "VBO.h"
#include "Effect.h"
#include "OGLConstants.h"

namespace mg
{
	class RenderBatch
	{
	public:
		RenderBatch() {};
		virtual ~RenderBatch() = 0 {};
		virtual void Draw() = 0;

		virtual void Bind() = 0;
		virtual void Unbind() = 0;
	};

	/*
	* �����, ����� ������� �������������� ������ � VAO
	* ������������ ����� ������ ��������������� ������
	* ������� �������� VBO ������ � ��������, ������������ ������ (����� ��������)
	*/

	template <typename T>
	class ConcreteRenderBatch : public RenderBatch
	{
	protected:
		GLuint vao;
		std::shared_ptr<VBO<T>> vertices;
		std::shared_ptr<VBO<GLuint>> indices;

	public:

		ConcreteRenderBatch() :
			vertices(std::make_shared<VBO<T>>()),
			indices(std::make_shared<VBO<GLuint>>())
		{
		}

		virtual ~ConcreteRenderBatch() = 0 
		{
			glDeleteVertexArrays(1, &vao);
		}

		ConcreteRenderBatch(const ConcreteRenderBatch<T>& concreteRenderBatch) :
			vertices(concreteRenderBatch.vertices),
			indices(concreteRenderBatch.indices)
		{}

		virtual void Init(LuaPlus::LuaState* luaState) = 0;
		virtual void ClearBuffers()
		{
			vertices->Clear();
			indices->Clear();
		}

		virtual void Draw() = 0;

		VBO<T>& GetVertices()
		{
			return *vertices;
		}

		VBO<unsigned int>& GetIndices()
		{
			return *indices;
		}

		virtual void Bind() override
		{
			glBindVertexArray(vao); CHECK_GL_ERROR
			vertices->Bind();
			indices->Bind();

			CHECK_GL_ERROR
		}

		virtual void Unbind() override
		{
			glBindVertexArray(0); CHECK_GL_ERROR

			vertices->Unbind();
			indices->Unbind();

			CHECK_GL_ERROR
		}
	};
}