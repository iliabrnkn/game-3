#include "stdafx.h"

#include "ViewSystem.h"
#include "AIPiece.h"
#include "Position.h"
#include "Viewable.h"
#include "GUI.h"
#include "GameplayState.h"
#include "Level.h"

namespace mg
{
	ViewSystem::ViewSystem(GameplayState* gps)
		: gps(gps)
	{}

	void ViewSystem::configure(entityx::EventManager &events)
	{
	}

	void ViewSystem::update(entityx::EntityManager &es, entityx::EventManager &em, entityx::TimeDelta dt)
	{
		//es.each<Viewable, Orientation, Position, AIPiece>([&dt, &es, this](entityx::Entity& observer, Viewable& view, Orientation& orient, Position& observerPos, AIPiece& aiPiece) {

		//	view.entitiesInSight.clear();

		//	es.each<AIPiece, Position>([&dt, &es, this, &view, &observerPos, &orient, &observer](entityx::Entity& entity, AIPiece& aiPiece, Position& pos) {

		//		if (observer.id() != entity.id())
		//		{
		//			// ���� ���������� �� ����� ������ ��� ������ ������
		//			if (glm::length(pos.AsVec2() - observerPos.AsVec2()) <= view.radius)
		//			{
		//				float angle = glm::angle(glm::normalize(glm::vec2(orient.vec)), glm::normalize(pos.AsVec2() - observerPos.AsVec2()));

		//				// ���� ����������� ��������� � ���� ������ �����������
		//				if (angle <= view.angle / 2.f)
		//				{
		//					// �������� �� ��, ���� �� ����� ������������ � ����������� ������
		//					bool intersect = false;

		//					for (auto& border : gps->GetLevel()->GetStaticBorders())
		//					{
		//						float tan = (pos.AsVec2().y - observerPos.AsVec2().y) / (pos.AsVec2().x - observerPos.AsVec2().x);

		//						if (border.alongX)
		//						{
		//							float& borderY = border.point0.y;

		//							//borderY == observerPos.AsVec2().y + tan * (x - observerPos.AsVec2().x);
		//							float x = (borderY - observerPos.AsVec2().y) / tan + observerPos.AsVec2().x;

		//							// ���� x ����� ����� ���������� � ��������� x �����, � ����� ������� �� ����������� �� y ������ �����, ������ ����� ������������ � ����������� ������ ������
		//							if (x >= min(border.point0.x, border.point1.x) && x <= max(border.point0.x, border.point1.x)
		//								&& glm::abs(pos.vec.y - observerPos.vec.y) > glm::abs(borderY - observerPos.vec.y))
		//							{
		//								intersect = true;
		//								break;
		//							}
		//						}
		//						else if (border.alongY)
		//						{
		//							float& borderX = border.point0.x;

		//							//borderY == observerPos.AsVec2().y + tan * (x - observerPos.AsVec2().x);
		//							float y = observerPos.AsVec2().y + tan * (borderX - observerPos.AsVec2().x);

		//							if (y >= min(border.point0.y, border.point1.y) && y <= max(border.point0.y, border.point1.y)
		//								&& glm::abs(pos.vec.x - observerPos.vec.x) > glm::abs(borderX - observerPos.vec.x))
		//							{
		//								intersect = true;
		//								break;
		//							}
		//						}
		//					}

		//					// ���� ����� ������������ � ����������� ��� ������, ������ ����������� ����� ������
		//					if (!intersect)
		//						view.entitiesInSight.push_back(aiPiece.table);
		//				}
		//			}
		//		}


		//	});

		//	if (orient.vecBefore != glm::vec3(0.f) && orient.vecBefore != orient.vec)
		//	{
		//		/*auto currVec = LuaPlus::LuaObject();
		//		auto vecBefore = LuaPlus::LuaObject();

		//		currVec.AssignNewTable();
		//		vecBefore.AssignNewTable();

		//		currVec.SetNumber("x", orient.vec.x);
		//		currVec.SetNumber("y", orient.vec.y);

		//		vecBefore.SetNumber("x", orient.vecBefore.x);
		//		vecBefore.SetNumber("y", orient.vecBefore.y);

		//		auto args = LuaPlus::LuaObject();
		//		args.AssignNewTable();
		//		args.SetObject("current", currVec);
		//		args.SetObject("before", currVec);*/

		//		aiPiece.Emit("onChangeOrientation", aiPiece.table);
		//	}

		//	orient.vecBefore = std::move(orient.vec);
		//});
	}
}