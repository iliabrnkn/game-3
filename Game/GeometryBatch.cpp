#include "stdafx.h"

#include "Paths.h"
#include "SFML\Graphics.hpp"
#include "Core.h"
#include "OGLConstants.h"

namespace mg {

	void GeometryBatch::Init(LuaPlus::LuaState* luaState)
	{
		// initialize framebuffer vbo

		auto videoMode = Core::GetVideoMode();

		vertices->Init(BUFFER_SIZE_32KB, GL_ARRAY_BUFFER);
		indices->Init(BUFFER_SIZE_32KB, GL_ELEMENT_ARRAY_BUFFER);

		glGenVertexArrays(1, &vao);

		glBindVertexArray(vao);
		vertices->Bind();
		indices->Bind();

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(SimpleVertex), 0);

	}

	void GeometryBatch::Draw()
	{
		glDrawElements(GL_TRIANGLES, indices->GetCurrentOffset(), GL_UNSIGNED_INT, BUFFER_OFFSET(0));
	}
}