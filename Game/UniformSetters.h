#pragma once

#include <string>
#include <glm\glm.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// ������� �������-����������

namespace mg {

	class Effect;

	class UniformSetter
	{

	protected:
		const Effect* effect;
		std::string varName;
	public:
		UniformSetter(const Effect* effect, const std::string& varName) : effect(effect), varName(varName) {};
		virtual ~UniformSetter() = 0 {}

		virtual void Invoke() = 0;
	};

	class UniformISetter : public UniformSetter
	{
		int val;

	public:

		UniformISetter(const Effect* effect, const std::string& varName, const int& val) :
			UniformSetter(effect, varName), val(val) {};

		virtual void Invoke() override;
	};

	class UniformFSetter : public UniformSetter
	{
		float val;

	public:

		UniformFSetter(const Effect* effect, const std::string& varName, const float& val) :
			UniformSetter(effect, varName), val(val) {};

		virtual void Invoke() override;
	};

	class Uniform2FSetter : public UniformSetter
	{
		glm::vec2 val;

	public:

		Uniform2FSetter(const Effect* effect, const std::string& varName, const glm::vec2& val) :
			UniformSetter(effect, varName), val(val) {};

		virtual void Invoke() override;
	};

	class Uniform3FSetter : public UniformSetter
	{
		glm::vec3 val;

	public:

		Uniform3FSetter(const Effect* effect, const std::string& varName, const glm::vec3& val) :
			UniformSetter(effect, varName), val(val) {};

		virtual void Invoke() override;
	};

	class UniformMx4Setter : public UniformSetter
	{
		glm::mat4 val;

	public:

		UniformMx4Setter(const Effect* effect, const std::string& varName, const glm::mat4& val) :
			UniformSetter(effect, varName), val(val) {};

		virtual void Invoke() override;
	};

	class UniformBlockSetter : public UniformSetter
	{
		size_t size;
		unsigned int index;
		unsigned int bufferID;

	public:

		UniformBlockSetter(const Effect* effect, const std::string& varName, const unsigned int& index,
			const unsigned int& bufferID, const size_t& size) :
			UniformSetter(effect, varName), index(index), bufferID(bufferID), size(size) {};

		virtual void Invoke() override;

	};
}