#include "stdafx.h"

#include <gl/glew/glew.h>

#include "Controls.h"
#include "Sizes.h"
#include "GUI.h"
#include "Core.h"
#include "Vector.h"

#include "ErrorLog.h";

using namespace std;

namespace mg
{
	bool Controls::leftButtonPressedLastTime = false;
	bool Controls::rightButtonPressedLastTime = false;
	bool Controls::middleButtonPressedLastTime = false;
	LuaPlus::LuaObject Controls::lastMouseArg;

	Controls::Controls() : 
		selectionColor(0),
		onMouseMove(LuaPlus::LuaObject()),
		onMouseClick(LuaPlus::LuaObject()),
		onKeyPressed(LuaPlus::LuaObject()),
		keyNames({ // ������ ������ ������� �� �� �����, ������ �������, �� ����� � �� ��������
			make_pair(static_cast<int>(sf::Keyboard::A), "A"),
			make_pair(static_cast<int>(sf::Keyboard::B), "B"),
			make_pair(static_cast<int>(sf::Keyboard::C), "C"),
			make_pair(static_cast<int>(sf::Keyboard::D), "D"),
			make_pair(static_cast<int>(sf::Keyboard::E), "E"),
			make_pair(static_cast<int>(sf::Keyboard::F), "F"),
			make_pair(static_cast<int>(sf::Keyboard::G), "G"),
			make_pair(static_cast<int>(sf::Keyboard::H), "H"),
			make_pair(static_cast<int>(sf::Keyboard::I), "I"),
			make_pair(static_cast<int>(sf::Keyboard::J), "J"),
			make_pair(static_cast<int>(sf::Keyboard::K), "K"),
			make_pair(static_cast<int>(sf::Keyboard::L), "L"),
			make_pair(static_cast<int>(sf::Keyboard::M), "M"),
			make_pair(static_cast<int>(sf::Keyboard::N), "N"),
			make_pair(static_cast<int>(sf::Keyboard::O), "O"),
			make_pair(static_cast<int>(sf::Keyboard::P), "P"),
			make_pair(static_cast<int>(sf::Keyboard::Q), "Q"),
			make_pair(static_cast<int>(sf::Keyboard::R), "R"),
			make_pair(static_cast<int>(sf::Keyboard::S), "S"),
			make_pair(static_cast<int>(sf::Keyboard::T), "T"),
			make_pair(static_cast<int>(sf::Keyboard::U), "U"),
			make_pair(static_cast<int>(sf::Keyboard::V), "V"),
			make_pair(static_cast<int>(sf::Keyboard::W), "W"),
			make_pair(static_cast<int>(sf::Keyboard::X), "X"),
			make_pair(static_cast<int>(sf::Keyboard::Y), "Y"),
			make_pair(static_cast<int>(sf::Keyboard::Z), "Z"),
			make_pair(static_cast<int>(sf::Keyboard::Num0), "Num0"),
			make_pair(static_cast<int>(sf::Keyboard::Num1), "Num1"),
			make_pair(static_cast<int>(sf::Keyboard::Num2), "Num2"),
			make_pair(static_cast<int>(sf::Keyboard::Num3), "Num3"),
			make_pair(static_cast<int>(sf::Keyboard::Num4), "Num4"),
			make_pair(static_cast<int>(sf::Keyboard::Num5), "Num5"),
			make_pair(static_cast<int>(sf::Keyboard::Num6), "Num6"),
			make_pair(static_cast<int>(sf::Keyboard::Num7), "Num7"),
			make_pair(static_cast<int>(sf::Keyboard::Num8), "Num8"),
			make_pair(static_cast<int>(sf::Keyboard::Num9), "Num9"),
			make_pair(static_cast<int>(sf::Keyboard::Escape), "Escape"),
			make_pair(static_cast<int>(sf::Keyboard::LControl), "LControl"),
			make_pair(static_cast<int>(sf::Keyboard::LShift), "LShift"),
			make_pair(static_cast<int>(sf::Keyboard::LAlt), "LAlt"),
			make_pair(static_cast<int>(sf::Keyboard::LSystem), "LSystem"),
			make_pair(static_cast<int>(sf::Keyboard::RControl), "RControl"),
			make_pair(static_cast<int>(sf::Keyboard::RShift), "RShift"),
			make_pair(static_cast<int>(sf::Keyboard::RAlt), "RAlt"),
			make_pair(static_cast<int>(sf::Keyboard::RSystem), "RSystem"),
			make_pair(static_cast<int>(sf::Keyboard::Menu), "Menu"),
			make_pair(static_cast<int>(sf::Keyboard::LBracket), "LBracket"),
			make_pair(static_cast<int>(sf::Keyboard::RBracket), "RBracket"),
			make_pair(static_cast<int>(sf::Keyboard::SemiColon), "SemiColon"),
			make_pair(static_cast<int>(sf::Keyboard::Comma), "Comma"),
			make_pair(static_cast<int>(sf::Keyboard::Period), "Period"),
			make_pair(static_cast<int>(sf::Keyboard::Quote), "Quote"),
			make_pair(static_cast<int>(sf::Keyboard::Slash), "Slash"),
			make_pair(static_cast<int>(sf::Keyboard::BackSlash), "BackSlash"),
			make_pair(static_cast<int>(sf::Keyboard::Tilde), "Tilde"),
			make_pair(static_cast<int>(sf::Keyboard::Equal), "Equal"),
			make_pair(static_cast<int>(sf::Keyboard::Dash), "Dash"),
			make_pair(static_cast<int>(sf::Keyboard::Space), "Space"),
			make_pair(static_cast<int>(sf::Keyboard::Return), "Return"),
			make_pair(static_cast<int>(sf::Keyboard::BackSpace), "BackSpace"),
			make_pair(static_cast<int>(sf::Keyboard::Tab), "Tab"),
			make_pair(static_cast<int>(sf::Keyboard::PageUp), "PageUp"),
			make_pair(static_cast<int>(sf::Keyboard::PageDown), "PageDown"),
			make_pair(static_cast<int>(sf::Keyboard::End), "End"),
			make_pair(static_cast<int>(sf::Keyboard::Home), "Home"),
			make_pair(static_cast<int>(sf::Keyboard::Insert), "Insert"),
			make_pair(static_cast<int>(sf::Keyboard::Delete), "Delete"),
			make_pair(static_cast<int>(sf::Keyboard::Add), "Add"),
			make_pair(static_cast<int>(sf::Keyboard::Subtract), "Subtract"),
			make_pair(static_cast<int>(sf::Keyboard::Multiply), "Multiply"),
			make_pair(static_cast<int>(sf::Keyboard::Divide), "Divide"),
			make_pair(static_cast<int>(sf::Keyboard::Left), "Left"),
			make_pair(static_cast<int>(sf::Keyboard::Right), "Right"),
			make_pair(static_cast<int>(sf::Keyboard::Up), "Up"),
			make_pair(static_cast<int>(sf::Keyboard::Down), "Down"),
			make_pair(static_cast<int>(sf::Keyboard::Numpad0), "Numpad0"),
			make_pair(static_cast<int>(sf::Keyboard::Numpad1), "Numpad1"),
			make_pair(static_cast<int>(sf::Keyboard::Numpad2), "Numpad2"),
			make_pair(static_cast<int>(sf::Keyboard::Numpad3), "Numpad3"),
			make_pair(static_cast<int>(sf::Keyboard::Numpad4), "Numpad4"),
			make_pair(static_cast<int>(sf::Keyboard::Numpad5), "Numpad5"),
			make_pair(static_cast<int>(sf::Keyboard::Numpad6), "Numpad6"),
			make_pair(static_cast<int>(sf::Keyboard::Numpad7), "Numpad7"),
			make_pair(static_cast<int>(sf::Keyboard::Numpad8), "Numpad8"),
			make_pair(static_cast<int>(sf::Keyboard::Numpad9), "Numpad9"),
			make_pair(static_cast<int>(sf::Keyboard::F1), "F1"),
			make_pair(static_cast<int>(sf::Keyboard::F2), "F2"),
			make_pair(static_cast<int>(sf::Keyboard::F3), "F3"),
			make_pair(static_cast<int>(sf::Keyboard::F4), "F4"),
			make_pair(static_cast<int>(sf::Keyboard::F5), "F5"),
			make_pair(static_cast<int>(sf::Keyboard::F6), "F6"),
			make_pair(static_cast<int>(sf::Keyboard::F7), "F7"),
			make_pair(static_cast<int>(sf::Keyboard::F8), "F8"),
			make_pair(static_cast<int>(sf::Keyboard::F9), "F9"),
			make_pair(static_cast<int>(sf::Keyboard::F10), "F10"),
			make_pair(static_cast<int>(sf::Keyboard::F11), "F11"),
			make_pair(static_cast<int>(sf::Keyboard::F12), "F12"),
			make_pair(static_cast<int>(sf::Keyboard::F13), "F13"),
			make_pair(static_cast<int>(sf::Keyboard::F14), "F14"),
			make_pair(static_cast<int>(sf::Keyboard::F15), "F15"),
			make_pair(static_cast<int>(sf::Keyboard::Pause), "Pause")
		})
	{
	}

	Controls::~Controls()
	{
	}

	void Controls::Init(LuaPlus::LuaState* luaState)
	{
		onMouseMove = luaState->GetGlobal("onMouseMove"); // ���������� ������ �����
		onMouseClick = luaState->GetGlobal("onMouseClick"); // ���������� �������� �����
		onKeyPressed = luaState->GetGlobal("onKeyPressed"); // ���������� ������� ������

		mouseKeyPressed = false;
	}

	void Controls::HandleKeys(LuaPlus::LuaState* luaState, const double& elapsed/*, const sf::Event& event*/)
	{
		// ���� �� ������� ���� � �������, ������������ ������� ���� ����������� �������

		if (!GUI::GetInstance().IsConsoleOn())
		{
			originalPass = false;

			// �������� �� ���� ��������
			if (timeFromLastKeyPressed >= CONTROLS_UPDATE_PERIOD)
			{
				repeatKeys.clear();
				std::swap(pressedKeys, repeatKeys);

				for (int i = static_cast<int>(sf::Keyboard::A); i <= static_cast<int>(sf::Keyboard::Pause); ++i)
				{
					if (sf::Keyboard::isKeyPressed(static_cast<sf::Keyboard::Key>(i)))
					{
						pressedKeys.push_back(keyNames[i]);
						timeFromLastKeyPressed = 0.0;
					}
				}

				originalPass = true;

				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Tilde))
				{
					GUI::GetInstance().SetConsoleOn(!GUI::GetInstance().IsConsoleOn());
				}
			}

			// ������� �������
			for (auto& keyName : pressedKeys)
			{
				// ���� ������� ���� ������ �� �����
				if (std::find(repeatKeys.begin(), repeatKeys.end(), keyName) != repeatKeys.end())
					onKeyPressed(keyName.c_str(), true, originalPass);
				else
					onKeyPressed(keyName.c_str(), false, originalPass);
			}

			timeFromLastKeyPressed += elapsed;
		}
		else
		{
			/*if (timeFromLastKeyPressed >= CONTROLS_UPDATE_PERIOD)
			{
				if (event.type == sf::Event::TextEntered && event.text.unicode < 128)
					GUI::GetInstance().ConsolePush(static_cast<char>(event.text.unicode));

				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
					GUI::GetInstance().ConsoleFlush();

				timeFromLastKeyPressed = 0.0;
			}
			
			timeFromLastKeyPressed += elapsed;*/
		}
	}

	// ������� ���������� ���� (�������� ������) ��� ����� � ��������� ���������
	// ��� ������� ����� �������� ����� ���� ��� ����� ���������� �����
	void Controls::PickColor()
	{
		glReadPixels(lastMousePosScreen.x, RENDERBUFFER_HEIGHT - lastMousePosScreen.y, 1, 1, GL_RED_INTEGER, GL_INT, &selectionColor);

		CHECK_GL_ERROR
	}

	glm::vec2 Controls::GetMouseScreenCoords(const sf::Window *window)
	{
		auto res = glm::vec2(-1.f);

		auto mouseWindowPosition = sf::Mouse::getPosition(*window);

		if (mouseWindowPosition.x > 0 && mouseWindowPosition.y > 0 &&
			mouseWindowPosition.x < window->getSize().x && mouseWindowPosition.y < window->getSize().y)
		{
			res = glm::vec2(
				mouseWindowPosition.x * float(RENDERBUFFER_WIDTH) / float(WINDOW_WIDTH),
				mouseWindowPosition.y * float(RENDERBUFFER_HEIGHT) / float(WINDOW_HEIGHT)
			);
		}

		return res;
	}

	SceneVector Controls::GetMouseSceneCoords(const sf::Window *window, const Camera *camera)
	{
		return SceneVector::FromScreenCoords(GetMouseScreenCoords(window), camera);
	}

	// ��������� �����
	void Controls::HandleMouse(sf::Window* window, LuaPlus::LuaState* luaState, const Camera* camera, const double& elapsed)
	{
		auto videoMode = Core::GetVideoMode();
		bool currLeftButtonPressed = false, currRightButtonPressed = false, currMiddleButtonPressed = false;

		auto mouseScreenCoords = GetMouseScreenCoords(window);

		if (mouseScreenCoords != glm::vec2(-1.f))
		{
			//lastMousePosScreen.x = static_cast<int>(mouseXResized); // ����������� � lastMousePosScreen ��������, �� �������� ����� ����� ����� ���� (����� ��������� �����)
			//lastMousePosScreen.y = static_cast<int>(mouseYResized);

			auto mouseScenePos = GetMouseSceneCoords(window, camera);
			int buttons = 0;

			// ��������� ������� ������ ����

			if (timeFromLastClick >= CONTROLS_UPDATE_PERIOD)
			{
				if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left))
					currLeftButtonPressed = true;
				else
					currLeftButtonPressed = false;

				if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Middle))
					currMiddleButtonPressed = true;
				else
					currMiddleButtonPressed = false;

				if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Right))
					currRightButtonPressed = true;
				else
					currRightButtonPressed = false;

				if (currRightButtonPressed || currMiddleButtonPressed || currLeftButtonPressed)
				{
					// ������ ������ � ������ ���
					if (!mouseKeyPressed)
					{
						LuaPlus::LuaObject arg;
						arg.AssignNewTable(*luaState);
						arg.SetNumber("xScene", mouseScenePos.X());
						arg.SetNumber("zScene", mouseScenePos.Z());
						arg.SetObject("mapCoords", mouseScenePos.ToMapCoords(luaState));
						arg.SetInteger("xScreen", mouseScreenCoords.x);
						arg.SetInteger("yScreen", mouseScreenCoords.y);
						arg.SetInteger("selection", selectionColor);
						arg.SetBoolean("leftButtonPressed", currLeftButtonPressed);
						arg.SetBoolean("rightButtonPressed", currRightButtonPressed);
						arg.SetBoolean("middleButtonPressed", currMiddleButtonPressed);
						arg.SetBoolean("originalPass", true);

						onMouseClick(arg);

						mouseKeyPressed = true;
					}
					else // ������ ������ �������
					{
						LuaPlus::LuaObject arg;
						arg.AssignNewTable(*luaState);
						arg.SetNumber("xScene", mouseScenePos.X());
						arg.SetNumber("zScene", mouseScenePos.Z());
						arg.SetObject("mapCoords", mouseScenePos.ToMapCoords(luaState));
						arg.SetInteger("xScreen", mouseScreenCoords.x);
						arg.SetInteger("yScreen", mouseScreenCoords.y);
						arg.SetInteger("selection", selectionColor);
						arg.SetBoolean("leftButtonPressed", currLeftButtonPressed);
						arg.SetBoolean("rightButtonPressed", currRightButtonPressed);
						arg.SetBoolean("middleButtonPressed", currMiddleButtonPressed);
						arg.SetBoolean("originalPass", false);

						onMouseClick(arg);
					}
				}
				else
					mouseKeyPressed = false;
			}

			// ���������, ������ �� ����� �� ����������
			if (!GUI::GetInstance().HandleMouse(sf::Vector2i(int(glm::round(mouseScreenCoords.x)), int(glm::round(mouseScreenCoords.y)))) && buttons)
			{
				// ���� ���, ������ �� �����
				LuaPlus::LuaObject arg;
				arg.AssignNewTable(*luaState);
				arg.SetNumber("xScene", mouseScenePos.X());
				arg.SetNumber("zScene", mouseScenePos.Z());
				arg.SetObject("mapCoords", mouseScenePos.ToMapCoords(luaState));
				arg.SetInteger("xScreen", mouseScreenCoords.x);
				arg.SetInteger("yScreen", mouseScreenCoords.y);
				arg.SetObject("mapCoords", mouseScenePos.ToMapCoords(luaState));
				arg.SetInteger("selection", selectionColor);
				arg.SetBoolean("leftButtonPressed", true/*leftButtonPressed*/);
				arg.SetBoolean("rightButtonPressed", true/*rightButtonPressed*/);
				arg.SetBoolean("middleButtonPressed", true/*middleButtonPressed*/);

				onMouseClick(arg);
			}

			if (buttons)
				timeFromLastClick = 0.0;

			// ��������� �������� �����

			if (lastMousePos != mouseScenePos)
			{
				LuaPlus::LuaObject arg;
				arg.AssignNewTable(*luaState);
				arg.SetNumber("xScene", mouseScenePos.X());
				arg.SetNumber("zScene", mouseScenePos.Z());
				arg.SetObject("mapCoords", mouseScenePos.ToMapCoords(luaState));
				arg.SetInteger("xScreen", mouseScreenCoords.x);
				arg.SetInteger("yScreen", mouseScreenCoords.y);
				arg.SetInteger("selection", selectionColor);

				lastMouseArg = arg;

				onMouseMove(arg);
			}

			lastMousePos = mouseScenePos;

			timeFromLastClick += elapsed;
		}

		lastMousePosScreen = mouseScreenCoords;
		leftButtonPressedLastTime = currLeftButtonPressed;
		rightButtonPressedLastTime = currMiddleButtonPressed;
		middleButtonPressedLastTime = currMiddleButtonPressed;
	}

	bool Controls::LastTimePressed(const sf::Mouse::Button& button)
	{
		if (button == sf::Mouse::Left)
			return leftButtonPressedLastTime;
		else if (button == sf::Mouse::Right)
			return rightButtonPressedLastTime;
		else if (button == sf::Mouse::Middle)
			return middleButtonPressedLastTime;
	}
}