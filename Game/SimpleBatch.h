#pragma once

#include <vector>

#include "RenderBatch.h"


#include <bullet/BulletCollision/CollisionDispatch/btGhostObject.h>
#include <bullet/btBulletDynamicsCommon.h>

#include "Vector.h"

namespace mg
{
	class SimpleBatch : public ConcreteRenderBatch<SimpleVertex>
	{

		friend class RenderingSystem3D;
		friend class SkeletonFactory;

		// ����� ��� ������ �������� ��������� ������ ������������ ��� �����
		std::vector<SimpleVertex> levelGeometry; 

		bool showAABBs;
		bool showLevelGeometry;
		bool showCoords;

	public:
		int vertCount;
		virtual void Init(LuaPlus::LuaState* luaState) override;
		virtual void Draw() override;
		unsigned int fakeIndex;
		void VisualizeCoords(const btCollisionShape* shape, const btTransform &transform);
		void VisualizeAABB(const btCollisionShape* shape, const btTransform &transform, const glm::vec3 &color = glm::vec3(1.f));
		void DrawVector(const btVector3 &begin, const btVector3 &dir, const glm::vec3& color);
		void AddTriangleToSceneGeometry(const SceneVector &p0, const SceneVector &p1, const SceneVector &p2);
		void VisualizeLevelGeometry();

		inline bool &ShowAABBs() { return showAABBs; }
		inline bool &ShowLevelGeometry() { return showLevelGeometry; }
		inline bool &ShowCoords() { return showCoords; }
	};
}