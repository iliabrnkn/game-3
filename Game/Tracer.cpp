#include "stdafx.h"

#include "Tracer.h"
#include "GameplayState.h"
#include "Position.h"
#include "Collideable.h"

namespace mg
{
	void TracerComponentManager::AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		entity.assign<Tracer>();
	}
	
	LuaPlus::LuaObject TracerComponentManager::GetComponentTable(entityx::Entity& entity) const
	{
		return LuaPlus::LuaObject();
	}

	LuaPlus::LuaObject TracerComponentManager::ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		return LuaPlus::LuaObject();
	}

	void TracerComponentManager::DrawTrace(const LuaPlus::LuaObject &entity, const LuaPlus::LuaObject &dir) const
	{
		auto ent = gps->GetEntity(entity);

		SceneVector startPoint = static_cast<SceneVector>(*(gps->GetEntity(entity).component<Position>()));
		SceneVector traceDir = SceneVector::FromMapCoords(dir);
		SceneVector endPoint = startPoint + traceDir;

		SceneVector offset(0.f);

		// �������� ����� � ����� �����, �������������� ����-��������
		if (ent.has_component<Collideable>())
		{
			btVector3 aabbMin; btVector3 aabbMax;

			ent.component<Collideable>()->body->getAabb(aabbMin, aabbMax);

			float radiusLen = ((SceneVector(aabbMax) - SceneVector(aabbMin)) / 2.f).Length() + 0.01;
			offset = traceDir.Normalized() * radiusLen;
		}

		ent.component<Tracer>()->traces.push_back(Trace(
			startPoint + offset,
			endPoint + offset
		));
	}
}