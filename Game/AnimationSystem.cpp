#include "stdafx.h"

#include <algorithm>
#include <deque>

#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/vector_angle.hpp>

#include "AnimationSystem.h"
#include "Model.h"
#include "Position.h"
#include "GUI.h"
#include "AIPiece.h"
#include "Direction.h"
#include "GameplayState.h"
#include "ParticleEmitter.h"
#include "Controls.h"
#include "InverseKinematicsController.h"
#include "Collideable.h"
#include "Moveable.h"
#include "AutoWalkController.h"

namespace mg {
	bool AnimationSystem::debugLooseBodies = false;

	AnimationSystem::AnimationSystem(GameplayState* gps)
		:gps(gps)
	{}

	void AnimationSystem::configure(entityx::EventManager &events)
	{
	}

	// ���������� ��������� �������� ������, ��� �������� ����� ����� �������� ��� ��������
	std::string AnimationSystem::GetTrueParentName(const Skeleton &skeleton, const std::string &nodeName) const
	{
		auto currParentName = skeleton.boneNodes.at(nodeName).parentName;
		
		while (currParentName.find('$') != std::string::npos && currParentName != "NOPARENT")
		{
			currParentName = skeleton.boneNodes.at(currParentName).parentName;
		}

		return currParentName;
	}

	// ���������� ��� ������, ��� �������� ���� ������� ����
	std::string AnimationSystem::GetBoneBodyParentName(const Skeleton &skeleton, const std::string &nodeName) const
	{
		auto currParentName = skeleton.boneNodes.at(nodeName).parentName;

		while (skeleton.boneBodies.find(currParentName) == skeleton.boneBodies.end() && currParentName != "NOPARENT")
		{
			currParentName = skeleton.boneNodes.at(currParentName).parentName;
		}

		return currParentName;
	}

	void AnimationSystem::update(entityx::EntityManager &es, entityx::EventManager &em, entityx::TimeDelta dt)
	{
		es.each<MeshBatch, Skeleton>([dt, this](entityx::Entity& entity, MeshBatch &meshBatch, Skeleton &skeleton)
		{
			// ��������� ������� �������� ������� � ������������ � ��������� ������� ��� ������
			skeleton.Update(true);

			// ���������� ������ �� ����� �� ������
			for (auto& mesh : meshBatch.meshes)
			{
				for (auto& bone : mesh.bones)
				{
					auto& skeletonBoneNode = skeleton.boneNodes.find(bone.first);

					if (skeletonBoneNode != skeleton.boneNodes.end())
					{
						mesh.boneValues[bone.second.queuePos] =
							skeleton.rootNodeInverseTransform *
							skeletonBoneNode->second.�urrentTransform *
							bone.second.offsetMatrix;
					}
				}
			}
		});

		// ���� ��������
		es.each<Skeleton, AnimationBatch, Position, Collideable, Moveable, Direction, InverseKinematicsController, AutoWalkController, AIPiece>([dt, this](
			entityx::Entity& entity, Skeleton& skeleton, AnimationBatch& animationBatch, Position& pos, Collideable &collideable, Moveable &mov, Direction &dir,
			InverseKinematicsController &ikController, AutoWalkController &awController, AIPiece &aiPiece)
		{
			if (!skeleton.dead) // ���� �������� ����, �� ������� ���
			{
				skeleton.ResetNodes();

				for (auto iter = animationBatch.activeAnimations.begin(); iter != animationBatch.activeAnimations.end(); )
				{
					iter->CountElapsed(dt);

					// ���� ��������� = 0, ������� ��, ����� - ��������� � �������
					if (iter->intensity <= 0.f)
					{
						iter = animationBatch.activeAnimations.erase(iter);
						continue;
					}

					iter++;
				}

				mov.CalculateOrientation(dir, aiPiece, dt);

				for (auto &animation : animationBatch.activeAnimations)
				{
					ApplyAnimationToSkeleton(dt, skeleton, animation, mov, pos, dir, aiPiece);
				}

				// ������������ ������� � ������������ � ���������
				skeleton.Update();

				for (auto &pair : skeleton.boneBodies)
				{
					if (skeleton.boneNodes.find(pair.first) == skeleton.boneNodes.end())
						continue;

					pair.second.animationTransformCurr = GetBoneBodyTransformByJoint(skeleton, SceneVector(0.f), dir, pair.first);

					if (pair.second.animationTransformPrev == btTransform::getIdentity())
					{
						pair.second.animationTransformPrev = pair.second.body->getWorldTransform();
						pair.second.animationTransformPrev.getOrigin() = SceneVector(0.f);
					}
				}

				// ������ �������� ������� ����� ������ � ������������ � ��������� ��������
				for (auto &pair : skeleton.boneBodies)
				{
					if (pair.first == "Armature" || skeleton.boneNodes.find(pair.first) == skeleton.boneNodes.end())
						continue;

					auto& boneBody = pair.second;

					// �������� �������� � ��������
					btTransform boneBodyTransformCurr = boneBody.body->getWorldTransform();
					btTransform boneBodyTransformNext = btTransform::getIdentity();

					boneBody.body->clearForces();

					if (boneBody.animatedAbsolute && (!boneBody.affectable || skeleton.locomotorActivity == 1.f))
					{
						boneBodyTransformNext = GetBoneBodyTransformByJoint(skeleton, pos, dir, pair.first);
					}
					else if (skeleton.boneNodes.at(pair.first).�urrentTransform != skeleton.boneNodes.at(pair.first).basicTransform)
					{
						boneBodyTransformNext =
							btTransform(
								boneBody.body->getWorldTransform().getRotation() *
								boneBody.animationTransformPrev.getRotation().inverse() * boneBody.animationTransformCurr.getRotation(),
								boneBody.body->getWorldTransform().getOrigin() +
								boneBody.animationTransformCurr.getOrigin() - boneBody.animationTransformPrev.getOrigin()
							);
					}

					auto linearVelocity = (boneBodyTransformNext.getOrigin() - boneBodyTransformCurr.getOrigin()) / dt;
					reinterpret_cast<RigidBody*>(boneBody.body)->applyCentralImpulse(
						skeleton.locomotorActivity * linearVelocity / boneBody.body->getInvMass()
					);

					boneBody.body->getWorldTransform().setRotation(boneBodyTransformNext.getRotation());
					boneBody.animationTransformPrev = boneBody.animationTransformCurr;
				}

				// ���� �������
				if (skeleton.locomotorActivity < 1.f)
				{
					skeleton.locomotorActivity = glm::min(
						skeleton.locomotorActivity + skeleton.recoveryFactor * static_cast<float>(dt), 1.f
					);
				}
			}
		});
	}

	// ���������� �������� � �������
	void AnimationSystem::ApplyAnimationToSkeleton(const double &dt, Skeleton &skeleton, AnimationHandler& animationHandler, Moveable &mov, Position &pos, Direction &dir,
		AIPiece &aiPiece) const
	{
		for (auto& boneNodePair : skeleton.boneNodes)
		{
			// � ������, ���� ������� ����� ��� � ����������� ��� ���� ��������, ���������� ��� �����
			if (animationHandler.allowedBoneNodes.find(boneNodePair.first) == animationHandler.allowedBoneNodes.end())
				continue;

			AnimKey currentKey;

			if (animationHandler.GetCurrentKey(boneNodePair.first, currentKey))
			{
				boneNodePair.second.currentAnimKey = AnimKey::mix(boneNodePair.second.currentAnimKey, currentKey, animationHandler.intensity);

				// ��������� ������� ������� �������������
				glm::mat4 translateMat = glm::translate(boneNodePair.second.currentAnimKey.pos);
				glm::mat4 rotationMat = glm::toMat4(boneNodePair.second.currentAnimKey.rot);
				glm::mat4 scaleMat = glm::scale(boneNodePair.second.currentAnimKey.scale);
				
				boneNodePair.second.channelTransform = translateMat * rotationMat * scaleMat;
				boneNodePair.second.transformedByChannel = true;
			}

			SceneVector localY = boneNodePair.second.channelTransform * glm::vec4(0.f, 1.f, 0.f, 1.f);
			double targetAdditionalAngle = 0.0;
			auto iter = skeleton.orientAngleComponents.end();

			if (boneNodePair.first == PELVIS_NODE_NAME)
			{
				localY = SceneVector(0.f, 1.f, 0.f);
				targetAdditionalAngle = SceneVector(0.f, 0.f, 1.f).Angle(mov.currentBodyOrientation);
				boneNodePair.second.transformedByChannel = true;
			}
			else if ((iter = skeleton.orientAngleComponents.find(boneNodePair.first)) != skeleton.orientAngleComponents.end())
			{
				// �������� ������ / �������
				targetAdditionalAngle = mov.currentBodyOrientation.Angle(dir) * iter->second;
				boneNodePair.second.transformedByChannel = true;
			}

			boneNodePair.second.channelTransform = 
				MatrixHelper::BTQuatToMat(btQuaternion(localY.Normalized(), targetAdditionalAngle)) *
				boneNodePair.second.channelTransform;
		}
	}

	// ��������� ������������� �������� ���� �� ������� ���������������� ��� ������� � �������
	btTransform AnimationSystem::GetBoneBodyTransformByJoint(const Skeleton &skeleton, const Position &pos, const Direction &dir, const std::string &jointName)
	{
		auto &boneNode = skeleton.boneNodes.at(jointName);
		auto &boneBody = skeleton.boneBodies.at(jointName);

		//1) ���� ������������� �����
		auto bodyTransform = MatrixHelper::ToBTTransfom(skeleton.rootNodeInverseTransform * boneNode.�urrentTransform);
		
		//2) ���������� �������
		bodyTransform.getOrigin() *= MODEL_SCALE;
		
		//3) ����� � ��������� ���������
		bodyTransform = btTransform(bodyTransform.getRotation(), bodyTransform.getOrigin() + pos);

		return bodyTransform;
	}
}