#include "stdafx.h"

#include <fstream>
#include <algorithm>
#include <vector>
#include <thread>
#include <mutex>

#include "Level.h"
#include "Paths.h"
#include "sizes.h"

#include "GameplayState.h"
#include "Paths.h"
#include "Position.h"
#include "Moveable.h"
#include "Direction.h"
#include "Core.h"
#include "Selectable.h"
#include "RenderingSystem3D.h"
#include "AIPiece.h"
#include "AISystem.h"
#include "LightPoint.h"
#include "GUI.h"
#include "ScriptApiFacade.h"
#include "Pathable.h"
#include "Skeleton.h"
#include "LightSystem.h"
#include "ViewSystem.h"
#include "Collideable.h"
#include "Objects.h"
#include "Core.h"
#include "Collideable.h"
#include "ObjectProps.h"

#include "Helpers.h"
#include "MathHelper.h"

using namespace std;

namespace mg {

	const float WALL_HEIGHT = 4.f;

	Level::Level() :
		spriteAtlases(SPRITE_ATLASES_COUNT)
	{
	}

	int Level::FindTilesetNumByGid(const LuaPlus::LuaObject &tilesets, const int &gid)
	{
		int tilesetNum = 0;
		for (LuaPlus::LuaTableIterator iter(tilesets); iter.IsValid(); iter.Next())
		{
			// ���������, ������ �� ���� ��� � �������
			if (iter.GetValue()["firstgid"].ToInteger() <= gid && iter.GetValue()["firstgid"].ToInteger() + iter.GetValue()["firstgid"].ToInteger() - 1)
				return tilesetNum;

			++tilesetNum;
		}
	}

	void Level::CreateSceneSegment(GameplayState* gps, const std::vector<std::vector<int>> &layerTiles,
		const int &startX, const int &startY, const float &layerHeight)
	{
		SceneSegment segment;
		segment.minBoundingPoint = SceneVector::FromMapCoords(startX, startY, layerHeight);
		segment.firstElement = gps->GetScene().GetCurrGeometryIndsCount();

		SceneVector maxBoundingPoint;

		for (int y = startY; y < startY + SEGMENT_TILE_SIZE; ++y)
		{ 
			for (int x = startX; x < startX + SEGMENT_TILE_SIZE; ++x)
			{
				auto &gidNum = layerTiles[y][x];

				if (gidNum != 0)
				{
					// ���� ���� ������������� �����-�� ������
					if (tileProps.find(gidNum) != tileProps.end())
					{
						gps->GetScene().AddVertexDataToSceneGeometry(
							tileProps.at(gidNum),
							SceneVector::FromMapCoords(x, y, layerHeight),
							GetGidTex(gidNum),
							maxBoundingPoint
						);
					}
					else { // ����� ������ ���� ������� (������� ���)
						gps->GetScene().AddQuadToSceneGeometry(
							SceneVector::FromMapCoords(x, y, layerHeight),
							SceneVector::FromMapCoords(0.f, 0.f, 1.f),
							GetGidTex(gidNum),
							maxBoundingPoint
						);
					}
				}
			}
		}

		segment.elementsCount = gps->GetScene().GetCurrGeometryIndsCount() - segment.firstElement;
		segment.maxBoundingPoint = maxBoundingPoint;
		gps->GetScene().AddSceneSegment(segment);
	}

	entityx::Entity Level::CreateTileLayer(GameplayState* gps, const LuaPlus::LuaObject &table)
	{
		auto name = LuaPlus::LuaHelper::GetString(table, "name");
		int levelScreenHeight = LuaPlus::LuaHelper::GetInteger(table, "offsety");
		auto yMax = LuaPlus::LuaHelper::GetInteger(table, "height");
		auto xMax = LuaPlus::LuaHelper::GetInteger(table, "width");
		auto data = LuaPlus::LuaHelper::GetTable(table, "data");
		float layerHeight = -static_cast<float>(levelScreenHeight) / HEIGHT_UNIT_SCREEN;
		std::vector<std::vector<int>> layerTiles;

		for (int y = 0; y < (height / SEGMENT_TILE_SIZE + (height % SEGMENT_TILE_SIZE > 0 ? 1 : 0)) * SEGMENT_TILE_SIZE; y++)
		{
			layerTiles.push_back(std::vector<int>((width / SEGMENT_TILE_SIZE + (width % SEGMENT_TILE_SIZE > 0 ? 1 : 0)) * SEGMENT_TILE_SIZE));
			
			for (int x = 0; x < width; x++)
			{
				// ���� � ����� ����� �����
				auto gid = data.GetByIndex(1 + y * width + x);

				if (!gid.IsNil())
					layerTiles[y][x] = gid.ToInteger();
			}
		}

		// ������ ���� ��������
		for (int startY = 0; startY < height; startY += SEGMENT_TILE_SIZE)
		{
			for (int startX = 0; startX < width; startX += SEGMENT_TILE_SIZE)
			{
				CreateSceneSegment(gps, layerTiles, startX, startY, layerHeight);
			}
		}

		auto entity = gps->entityManager.create();
		entity.assign<Position>(table["x"].ToInteger(), table["y"].ToInteger(), .0);

		return entity;
	}

	void Level::AssociateGidWGeometry(const int &gid, PropsHandler props, GameplayState *gps)
	{
		tileProps.insert(std::make_pair(gid, props));
	}

	void Level::ParseLayers(LuaPlus::LuaObject& layersInitial, LuaPlus::LuaObject& result) // ���������� ������ ������ �����
	{
		for (LuaPlus::LuaTableIterator iter(layersInitial); iter; iter.Next())
		{
			auto lvlgname = iter.GetValue().GetByName("name").ToString();

			if (string(iter.GetValue().GetByName("type").ToString()) == string("group") && 
				iter.GetValue().GetByName("visible").GetBoolean()) // ���� ��� ������ ����� � ��� ������, ��������� �� ��������� ����� ��������
			{
				ParseLayers(iter.GetValue().GetByName("layers"), result);
			}
			else if (string(iter.GetValue().GetByName("type").ToString()) == string("tilelayer") 
				|| string(iter.GetValue().GetByName("type").ToString()) == string("objectgroup")
				)
			{
				LuaPlus::LuaObject layer;
				layer.AssignObject(iter.GetValue());
				layer.SetString("parent", layersInitial.GetByName("name").ToString());
				result.SetObject(result.GetCount() + 1, layer);
			}
		}
	}

	void Level::InitAtlases(GameplayState* gps, const LuaPlus::LuaObject &mapTable)
	{
		// ������������� �������
		LuaPlus::LuaObject tilesetsTable = LuaPlus::LuaHelper::GetTable(mapTable, "tilesets");
		
		// ��������������� �� �������� �������� ��������
		auto tsSorted = GetTilesetsSortedBySize(tilesetsTable);

		for (int i = 0; i < tsSorted.size(); ++i)
		{
			auto tsTable = tsSorted[i];
			int spriteTilesetNum = float(i) / (float(tsSorted.size()) / float(SPRITE_ATLASES_COUNT));

			auto firstGid = LuaHelper::GetInteger(tsTable, "firstgid");
			auto tileCount = LuaHelper::GetInteger(tsTable, "tilecount");

			// ������� ������� � ��������
			auto spriteRelSize = glm::vec2(
				LuaHelper::GetFloat(tsTable, "tilewidth") / LuaHelper::GetFloat(tsTable, "imagewidth"),
				LuaHelper::GetFloat(tsTable, "tileheight") / LuaHelper::GetFloat(tsTable, "imageheight")
			);

			spriteAtlases[spriteTilesetNum].AddTexture(
				firstGid,
				0,
				tileCount,
				TILESETS_FOLDER + LuaHelper::GetString(tsTable, "image"),
				LuaHelper::GetInteger(tsTable, "tilewidth"),
				LuaHelper::GetInteger(tsTable, "tileheight")
			);

			auto layer = spriteAtlases[spriteTilesetNum].GetTilesetImagesCount() - 1;

			for (int gid = firstGid; gid <= firstGid - 1 + tileCount; ++gid)
			{
				if (tileProps.find(gid) != tileProps.end()) // ���� ������ �������� ������
					gps->SetGidProperties(gid, GidProps(layer, tileProps.at(gid), spriteRelSize, spriteTilesetNum));
				else
					gps->SetGidProperties(gid, GidProps(layer, spriteTilesetNum));
			}
		}

		// TODO: ������
		skinAtlas.AddTexture(1, 0, 1, TEXTURES_FOLDER + "guy.tga");
		skinAtlas.AddTexture(2, 0, 1, TEXTURES_FOLDER + "guy.tga");

		// TODO: ������
		particleAtlas.AddTexture(1, 0, 4, TILESETS_FOLDER + "sp_fog1.png", 80, 80);
		cloudAtlas.AddTexture(1, 0, 1, TEXTURES_FOLDER + "cloud.png", 256, 256);

		for (auto &spriteAtlas : spriteAtlases)
		{
			spriteAtlas.Init();
		}

		skinAtlas.Init();
		particleAtlas.Init();
		cloudAtlas.Init();
	}

	void Level::Load(const LuaPlus::LuaObject &mapTable, GameplayState *gps)
	{
		LuaPlus::LuaObject h = mapTable.GetByName("height");
		auto ref = h.GetRef();

		height = LuaPlus::LuaHelper::GetInteger(mapTable, "height");
		width = LuaPlus::LuaHelper::GetInteger(mapTable, "width");

		LuaPlus::LuaObject layersTable;
		layersTable.AssignNewTable(gps->GetLuaState());
		ParseLayers(LuaPlus::LuaHelper::GetTable(mapTable, "layers"), layersTable);
		InitAtlases(gps, mapTable);

		Core::GetInstance()->GetWindow().setActive(true);

		// layers parsing
		unsigned int layersMaxNum = layersTable.GetCount();

		for (LuaPlus::LuaTableIterator iter(layersTable); iter; iter.Next())
		{
			LuaPlus::LuaObject layerTable = iter.GetValue();
			if (layerTable.GetRef() == -1) break;

			if (!LuaHelper::GetBoolean(layerTable, "visible")) continue;

			auto type = LuaHelper::GetString(layerTable, "type");

			if (LuaHelper::GetString(layerTable, "type") == "tilelayer")
			{
				CreateTileLayer(gps, layerTable);
			}
			else if (LuaHelper::GetString(layerTable, "type") == "objectgroup")
			{
				if (LuaHelper::GetString(layerTable, "name").substr(0, 7) == "navmesh")
				{
					navigationMesh.Init(gps, layerTable);
				}
			}
		}

		gps->SetCurrentVersion(1);
		gps->GetScene().CreateTriangleMeshShape(gps->GetPhysics());

		// ������������� ���������� ��������� ��� �������� ��������� �����
		if (mapTable.GetByName("ambienceLight").GetRef() != -1 && mapTable["ambienceLight"].IsTable())
		{
			ambienceLight.x = mapTable["ambienceLight"].GetByName("r").GetRef() != -1 ? mapTable["ambienceLight"]["r"].ToNumber() : 1.0;
			ambienceLight.y = mapTable["ambienceLight"].GetByName("g").GetRef() != -1 ? mapTable["ambienceLight"]["g"].ToNumber() : 1.0;
			ambienceLight.z = mapTable["ambienceLight"].GetByName("b").GetRef() != -1 ? mapTable["ambienceLight"]["b"].ToNumber() : 1.0;
		}

		gps->GetScene().FillBuffers();

		// ������

		cloudTexCoordSize = glm::vec2(float(width) / TILES_PER_CLOUD_UNIT, float(height) / TILES_PER_CLOUD_UNIT);

		gps->GetScene().GetCloudBatch().Bind();
		gps->GetScene().GetCloudBatch().GetVertices().Fill(
			std::vector<AnimVertex>{
				AnimVertex(glm::vec3(0.f, 0.f, 0.f), glm::vec2(0.f, 0.f)),
				AnimVertex(glm::vec3(0.f, 0.f, height), glm::vec2(0.f, cloudTexCoordSize.y)),
				AnimVertex(glm::vec3(width, 0.f, height), glm::vec2(cloudTexCoordSize.x, cloudTexCoordSize.y)),
				AnimVertex(glm::vec3(width, 0.f, 0.f), glm::vec2(cloudTexCoordSize.x, 0.f))
			}
		);

		gps->GetScene().GetCloudBatch().GetIndices().Fill(
			std::vector<unsigned int>{0, 1, 2, 2, 3, 0}
		);
		gps->GetScene().GetCloudBatch().Unbind();
	}

	int Level::GetWidth() const
	{
		return width;
	}

	int Level::GetHeight() const
	{
		return height;
	}

	std::vector<TextureAtlas>& Level::GetSpriteAtlases()
	{
		return spriteAtlases;
	}

	TextureAtlas& Level::GetSkinAtlas()
	{
		return skinAtlas;
	}

	TextureAtlas& Level::GetParticleAtlas()
	{
		return particleAtlas;
	}

	TextureAtlas& Level::GetCloudAtlas()
	{
		return cloudAtlas;
	}

	void Level::TestPathFinding(GameplayState *gps)
	{
		//for (auto &rect : navigationMesh.navRects)
		//{
		//	gps->GetScene().GetSimpleBatch().DrawVector(
		//		rect.boundingMin, SceneVector(rect.boundingMax.X(), 0.f, rect.boundingMin.Z()) - rect.boundingMin,
		//		glm::vec3(0.f, 1.f, 0.f)
		//	);

		//	gps->GetScene().GetSimpleBatch().DrawVector(
		//		rect.boundingMin, SceneVector(rect.boundingMin.X(), 0.f, rect.boundingMax.Z()) - rect.boundingMin,
		//		glm::vec3(0.f, 1.f, 0.f)
		//	);

		//	gps->GetScene().GetSimpleBatch().DrawVector(
		//		rect.boundingMax, SceneVector(rect.boundingMin.X(), 0.f, rect.boundingMax.Z()) - rect.boundingMax,
		//		glm::vec3(0.f, 1.f, 0.f)
		//	);

		//	gps->GetScene().GetSimpleBatch().DrawVector(
		//		rect.boundingMax, SceneVector(rect.boundingMax.X(), 0.f, rect.boundingMin.Z()) - rect.boundingMax,
		//		glm::vec3(0.f, 1.f, 0.f)
		//	);

		//	// ����

		//	for (auto node : rect.nodes)
		//	{
		//		for (auto neighbour : node->neighbours)
		//		{
		//			gps->GetScene().GetSimpleBatch().DrawVector(node->pos, neighbour->pos - node->pos,
		//				glm::vec3(0.f, 0.f, 1.f));
		//		}
		//	}

		//	// ���� ������ ����

		//	/*auto path = navigationMesh.FindPath(
		//		SceneVector::FromMapCoords(1, 1),
		//		SceneVector::FromMapCoords(11, 19)
		//	);

		//	for (int i = 1; i < path.size(); ++i)
		//	{
		//		gps->GetScene().GetSimpleBatch().DrawVector(
		//			path[i - 1], path[i] - path[i - 1],
		//			glm::vec3(1.f, 0.f, 1.f));
		//	}*/
		//}
	}
}