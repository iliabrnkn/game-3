#include "stdafx.h"

#include "Batch2D.h"
#include "OGLConstants.h"
#include "Paths.h"

namespace mg
{
	void SpriteBatch::Init(LuaPlus::LuaState* luaState)
	{
		// initialize buffers

		indices->Init(BUFFER_SIZE_8MB, GL_ELEMENT_ARRAY_BUFFER);
		vertices->Init(BUFFER_SIZE_32MB, GL_ARRAY_BUFFER);

		// initialize VAO

		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		vertices->Bind();
		indices->Bind();

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(SpriteVertex), 0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(SpriteVertex), BUFFER_OFFSET(12));
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(SpriteVertex), BUFFER_OFFSET(24));
		glEnableVertexAttribArray(3);
		glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(SpriteVertex), BUFFER_OFFSET(32));
		glEnableVertexAttribArray(4);
		glVertexAttribPointer(4, 2, GL_FLOAT, GL_FALSE, sizeof(SpriteVertex), BUFFER_OFFSET(40));
		glEnableVertexAttribArray(5);
		glVertexAttribIPointer(5, 1, GL_INT, sizeof(SpriteVertex), BUFFER_OFFSET(48));
		glEnableVertexAttribArray(6);
		glVertexAttribPointer(6, 1, GL_FLOAT, GL_FALSE, sizeof(SpriteVertex), BUFFER_OFFSET(52));
		glBindVertexArray(0);

		glBindVertexArray(NULL);
		vertices->Unbind();
		indices->Unbind();

		CHECK_GL_ERROR
	}

	void SpriteBatch::Draw()
	{
		glDrawElementsBaseVertex(GL_QUADS, indices->GetCurrentOffset(), GL_UNSIGNED_INT, BUFFER_OFFSET(0), 0);

		CHECK_GL_ERROR
	}

	SpriteBatch::SpriteBatch()
		: ConcreteRenderBatch<SpriteVertex>()
	{}

	SpriteBatch::~SpriteBatch()
	{}
}