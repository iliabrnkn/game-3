-- инициализация уровня

function loadLevel(filename, args)

	print("attempt to load a map file: " .. filename)

	local level = dofile(filename)
	local tiles = {};

	--грузим тайлсеты
	for k, v in pairs(level.tilesets) do

		local imageFilename = string.sub(string.match(v.image, "/[^/]*$"), 2)


		local nameWoExtension = string.sub(imageFilename, 1, -5)

		v.image = nameWoExtension .. ".png";

		--[[local volumeFileName = nameWoExtension .. "_volume.png" -- объем
		if c_checkIfTilesetExists(volumeFileName) then
			v.volumemap = volumeFileName
		end

		local normalFileName = nameWoExtension .. "_normal.png" -- нормали
		if c_checkIfTilesetExists(normalFileName) then
			v.normalmap = normalFileName
		end]]

		if (v.tiles ~= nil) then
			for kt, vt in pairs(v.tiles) do
				tiles[v.firstgid + vt.id] = vt
			end
		end

		-- ищем в tillesetProperties нужные модельки
		--for i = 0, v.tilecount, 1 do
		for tcp, tcv in pairs(tilesetPropertyChunks) do
			for tpk, tpv in pairs(tcv) do

				local texOffset = {x = 0, y = 0}

				if (tpv.texOffset ~= nil) then
					texOffset = tpv.texOffset
				end

				local normalTexOffsetFactor = createVector{0.0, 0.0, 0.0}

				if (tpv.normalTexOffsetFactor ~= nil) then
					normalTexOffsetFactor = createVector(tpv.normalTexOffsetFactor)
				end


				for tsk, tsv in pairs(tpv.tilesets) do

					if (tsv.texOffset ~= nil) then
						texOffset = tsv.texOffset
					end

					if (tsv.normalTexOffsetFactor ~= nil) then
						normalTexOffsetFactor = createVector(tsv.normalTexOffsetFactor)
					end
					
					if (tsv.name == nameWoExtension) then -- нашли в tilesetProperties имя тайлсета
						if (tsv.tiles ~= nil) then -- для некоторых тайлов
							for tk, tv in pairs(tsv.tiles) do
								c_associateGIDWProps(
									v.firstgid + tv - 1, 
									tpv.modelName, 
									texOffset,
									normalTexOffsetFactor
								)
							end
						else -- для всех тайлов
							for i = 1,v.tilecount,1 do
								c_associateGIDWProps(
									v.firstgid + i - 1, 
									tpv.modelName, 
									texOffset,
									normalTexOffsetFactor
								)
							end
						end

						--[[if (tsv.debugGid ~= nil) then
							c_setDebugGid(tsv.debugGid)
						end]]
					end
				end
			end
		end

	end

	-- диффузный свет
	if args ~= nil then
		if args.ambienceLight ~= nil then
			level.ambienceLight = {
				r = args.ambienceLight.r, 
				g = args.ambienceLight.g, 
				b = args.ambienceLight.b
			}
		end
	else 
		level.ambienceLight = {
			r = 0.3, 
			g = 0.3, 
			b = 0.3
		}
	end

	-- сама загрузка уровня

	print("loading level")
	c_loadLevel(level)
	print("level loaded")

	-- инициализируем статические объекты

	local staticObjects = {}

	getStaticObjects(level, staticObjects)

	--[[for k,v in pairs(staticObjects) do
		print("static object processing")
		local objectComponents = {
			position = {x = v.x / const.TILESTEP, y = v.y / const.TILESTEP},
			sprite = {gid = v.gid, layernum = 2}
		}

		c_createEntity(prototypes["object"]:derive{
			name = v.name,
			initialComponents = objectComponents,
			onInit = function(self) end
		})
	end--]]

	print("static objects processed")

end

--[[function move(entity, rawMovDir)
	local moveable = c_getComponent(entity, "moveable")

	c_modifyComponent(entity, "moveable", {speedScalarIncrement = moveable.speedScalarMax / const.SPEED_UP_COEFFICIENT,
		rawMovementDir = {x = rawMovDir.x + moveable.rawMovementDir.x, y = rawMovDir.y + moveable.rawMovementDir.y}})
end

function stayStill(entity)
	local moveable = c_getComponent(entity, "moveable")

	c_modifyComponent(entity, "moveable", {speedScalarIncrement = -moveable.speedScalarMax / const.SPEED_UP_COEFFICIENT})
end--]]