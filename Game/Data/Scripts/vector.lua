-- трехмерный вектор

function createVector(x, y, z)
  
  if(
		(type(x) == "number" or x == nil) and 
		(type(y) == "number" or y == nil) and 
		(type(z) == "number" or z == nil)
  	) then
    local res = {
      x = x ~= nil and x or 0, 
      y = y ~= nil and y or 0, 
      z = z ~= nil and z or 0,
      toString = function(self)
        return "{"
        .. tostring(self.x) .. "; "
        .. tostring(self.y) .. "; "
        .. tostring(self.z) .. "}"
      end,
      xy = function(self)
      	return createVector(self.x, self.y, nil)
      end
    }

    setmetatable(res, vectorMetatable)
    return res
  elseif (type(x) == "table" and y == nil and z == nil) then -- если передали таблицу
    --local arg = {}

    --[[for k, v in pairs(x) do
    	if (k == "x")
	    	arg.x = v
	    elseif (k == "y")
	    	arg.y = v
	    elseif (k == "z")
	    	arg.z = v
	    end
	end]]

	return createVector(
		x.x ~= nil and x.x or 0.0, 
		x.y ~= nil and x.y or 0.0, 
		x.z ~= nil and x.z or 0.0
	)
  end
end

vectorMetatable = {
  __add = function(lhs, rhs)
    return
      createVector(
        lhs.x + rhs.x,
        lhs.y + rhs.y,
        lhs.z + rhs.z
      )
  end,
   __eq = function(lhs, rhs)
    return lhs.x == rhs.x and lhs.y == rhs.y and lhs.z == rhs.z
  end,
  __sub = function(lhs, rhs)
    return createVector(
      lhs.x - rhs.x,
      lhs.y - rhs.y,
      lhs.z - rhs.z
    )
  end,
  __mul = function(lhs, rhs) 
    if (type(lhs) == "number") then
      return createVector(rhs.x * lhs, rhs.y * lhs, rhs.z * lhs)
    elseif (type(rhs) == "number") then
      return createVector(lhs.x * rhs, lhs.y * rhs, lhs.z * rhs)
    else
      return nil
    end
  end,
  __div = function(lhs, rhs) 
    if (type(rhs) == "number") then
      return createVector(lhs.x / rhs, lhs.y / rhs, lhs.z / rhs)
    else
      return nil
    end
  end
} 

function crossProduct(lhs, rhs)
	return createVector(c_crossProduct(lhs, rhs))
end

function angle(lhs, rhs)
	return c_findAngle(lhs, rhs)
end

function dotProduct(lhs, rhs)
	return createVector(c_dotProduct(lhs, rhs))
end

function normalizeVec(lhs)
	return createVector(c_normalizeVec(lhs))
end

function mixVec(lhs, rhs, a)
	return createVector(c_mixVec(lhs, rhs, a))
end

function slerpVec(lhs, rhs, a)
	return createVector(c_slerpVec(lhs, rhs, a))
end

function lengthVec(lhs)
	return c_getLength(createVector(lhs))
end