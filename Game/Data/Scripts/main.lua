--dofile("C:/repos/gamereal/game/Game/Data/Scripts/c-functions-stub.lua")
--dofile("C:/repos/gamereal/game/Game/Data/Scripts/postures.lua")

-- инициализация уровня

dofile(c_getScriptsFolder() .. "inventory.lua")
dofile(c_getScriptsFolder() ..  "debug.lua")
dofile(c_getScriptsFolder() .. "vector.lua")
dofile(c_getScriptsFolder() .. "animations.lua")

-- сущности
dofile(c_getScriptsFolder() .. "prototypes.lua")
dofile(c_getScriptsFolder() .. "actor.lua")
dofile(c_getScriptsFolder() .. "enemy.lua")
dofile(c_getScriptsFolder() .. "physics.lua")
dofile(c_getScriptsFolder() .. "particles.lua")
dofile(c_getScriptsFolder() .. "weapons.lua")

dofile(c_getScriptsFolder() .. "functions.lua")
dofile(c_getScriptsFolder() .. "tilesets.lua")
dofile(c_getScriptsFolder() .. "level.lua")
dofile(c_getScriptsFolder() .. "critters-functions.lua")
loadLevel(c_getMapsFolder() .. "fight_area.lua", { ambienceLight = {r = 1.0, g = 1.0, b = 2.0}})--{ r = 22.0 / 32.0, g = 40.0 / 32.0, b = 111.0 / 32.0 } })
--loadLevel(c_getMapsFolder() .. "gas_station_location1.lua", { ambienceLight = { r = 1.0, g = 1.0, b = 2.5 } })
--loadLevel(c_getMapsFolder() .. "map_suburb_zone1_3.lua", { ambienceLight = {r = 1.0, g = 1.0, b = 2.0} })

c_setTimeFactor(0.5)

state = {
	ui = {
		lastHighlightedEntityID = nil
	},
	controls = {
		lastKeyPressed = nil,
		moveDir = {
			x = 0.0,
			y = 0.0,
			add = function(self, x, y)
				self.x = self.x + x
				self.y = self.y + y
			end
		}
	},
	inventoryShown = false,
	running = false
}

c_centerCameraAt(0, 0)
--player = createPlayer(92, 162, 0)
player = createPlayer(0, 0, 0)

sun = c_createEntity(prototypes["base"]:derive{
	name = "sun",
	initialComponents = {
		direction = {
			x = 1.0, y = 3, z = 2
		},
		directionalLight = {
			color = {r = 0.32, g = 0.31, b = 0.5},
			enabled = true,
			windVelocity = {x = 1.0 / 30.0, y = 1.0 / 30.0}
		}
	}
})

dummy = createDummy(7, 6, 0)

--baseballBat = debugCreateBaseballBat()
--player.currentWeapon = baseballBat

--pistol = debugCreatePistol()
--player.currentWeapon = pistol

--[[shotty = c_createEntity(prototypes["shotgun"]:derive{})
shotty:assign(player)]]

pistol = c_createEntity(prototypes["pistol"]:derive{})
shotty = c_createEntity(prototypes["shotgun"]:derive{})
bat = c_createEntity(prototypes["baseballBat"]:derive{})

-- тест атаки
--[[dummy:assignWeapon(bat)
dummy:enableAiming(true)
dummy:enableRun(true)

dummy.onUpdate = function(self)
	if (c_hasWayPoints(self)) then
		c_goToLastWayPoint(self)
	else
		c_stop(self)
	end

	local goal = c_getPos(player) - normalizeVec(c_getPos(player) - c_getPos(self)) * 0.5
	
	if (lengthVec(goal - c_getPos(self)) > 1.0) then
		c_findPath(self, goal)
	else
		self.currentWeapon:attack(player)
	end

	c_orientTo(self, c_getPos(player))
end]]

--[[c_createEntity(prototypes["fire"]:derive{
	name = "fire1",
	initialComponents = 
	{
		position = {x = 14.7, y = 10.7, z = 1.0},
		particleEmitter = {
			startColor = { r = 1.0, g = 0.8, b = 0.5},
			endColor = { r = 1.0, g = 0.0, b = 0.0, a = 0.0},
			maxLifeTime = 0.6,
			coreRadius = 0.2
		}
	}
})

c_createEntity(prototypes["base"]:derive{
	name = "fire_light",
	initialComponents = 
	{
		position = {x = 14.6, y = 10.6, z = 1.3},
		lightPoint = {
			color = {r = 1.0, g = 0.4, b = 0.4}, 
			radius = 30.0, 
			dynamic = true
		}
	}
})]]

--pistol:assign(player)


--[[testGunshot = c_createEntity(prototypes["gunshot"]:derive{
	gunshotProperties = {
		mainAxis = { x = 0.0, y = 0.0, z = 1.0 },
		upAxis = { x = 1.0, y = 0.0, z = 0.0 }
	},
	initialComponents = {
		position = {x = 1.0, y = 1.0, z = 3.0},
		particleEmitter = {active = false}
	}
})]]

--[[testFogEmitter = c_createEntity(prototypes["base"]:derive{
	name = "testFogEmitter",
	initialComponents = {
			position = {x = 5.0, y = 5.0, z = 0.5},
			particleEmitter = {
				maxParticlesCount = 800, 
				maxLifeTime = 2.5, 
				coreRadius = 1.0, 
				particlesPerFrame = 15,
				startParticleSize = 80.0,
				zSpeed = 200.0,
				velocity = 2.0,
				startColor = { x = 1.0, y = 0.0, z = 1.0},
				endColor = { x = 0.5, y = 0.0, z = 0.5},
				type = 1
			}
		},
		onInit = function(self) end
	})

testFogEmitter = c_createEntity(prototypes["base"]:derive{
	name = "testFogEmitter",
	initialComponents = {
			position = {x = 10.0, y = 10.0, z = 0.5},
			particleEmitter = {
				maxParticlesCount = 800, 
				maxLifeTime = 2.5, 
				coreRadius = 1.0, 
				particlesPerFrame = 15,
				startParticleSize = 80.0,
				zSpeed = 200.0,
				velocity = 2.0,
				startColor = { x = 0.0, y = 0.2, z = 1.0},
				endColor = { x = 0.5, y = 0.0, z = 0.5},
				type = 1
			}
		},
		onInit = function(self) end
	})]]

--[[fire = c_createEntity(prototypes["base"]:derive{
	name = "fire",
	initialComponents = {
			position = {x = 7.0, y = 4.0, z = 0.0},
			particleEmitter = {
				maxParticlesCount = 400,
				maxLifeTime = 0.8,
				coreRadius = 1.0,
				particlesPerFrame = 100,
				startPointSize = 3.0,
				zSpeed = 100.0,
				velocity = 2.0,
				colorStart = { x = 1.0, y = 1.0, z = 0.5},
				colorEnd = { x = 0.1, y = 0.0, z = 0.0},
				pointSize = 3.0,
				type = 0
			}
		},
		onInit = function(self) end
	})]]

--alignableEntity = player.currentWeapon

--[[pistol = debugCreatePistol()
player.currentWeapon = pistol

alignableEntity = pistol]]

--createBox(16, 17, 3, 1, 1, 1, 1)

--box = createBox(9, 9, 3, 1, 1, 1, 10)

--dummy = createDummy(15, 15)


--c_removeComponent(player, "weapon")

--[[enemy = c_createEntity(prototypes["enemy"]:derive{
		name = "player",
		initialComponents = {
			position = {x = 40.0, y = 44.0, z = 0.0},
			viewable = {angle = const.PI, radius = 100.0}
		},
		weapon = {
			animations = {
				hit = "bat_hit3",
				aiming = "bat_stand"
			}
		}
	})]]--

--playerPosG = c_getComponent(player, "position")

--c_centerCameraAt(playerPosG.x, playerPosG.y) -- передвигаем камеру в соответствии с координатами мыши
c_centerCameraAt(13, 13)
c_debugEnableGravity()

mouseTorch = c_createEntity(prototypes["base"]:derive{
	name = "torch",
	initialComponents = {
			position = { x = 33.0, y = 12.0, z = 2.5 },
			lightPoint = { color = {r = 1.0, g = 1.0, b = 1.0}, radius = 30.0, dynamic = true },
			--spotLight = { 
			--angle = math.pi / 2.0 ,
			--color = {r = 1.0, g = 1.0, b = 1.0}, 
			--radius = 20.0, 
			--dynamic = true
			--},
			direction = { z = -1.0, x = 0.0, y = 0.0 }
		},
		onInit = function(self) end
	}
)

staticLight = c_createEntity(prototypes["base"]:derive{
	name = "torch",
	initialComponents = {
			position = { x = 82.0, y = 162.0, z = 3.0 },
			lightPoint = {color = {r = 1.0, g = 1.0, b = 1.0}, radius = 30.0, dynamic = false}
		},
		onInit = function(self) end
	}
)
staticLight2 = c_createEntity(prototypes["base"]:derive{
	name = "torch2",
	initialComponents = {
			position = { x = 98.0, y = 167.0, z = 2.0 },
			lightPoint = {color = {r = 1.0, g = 1.0, b = 1.0}, radius = 20.0, dynamic = false}
		},
		onInit = function(self) end
	}
)

--[[c_createEntity(prototypes["base"]:derive{
				name = "wqwww",
				initialComponents = {
						position = {x = 5.0, y = 5.0, z = 1.5},
						lightPoint = {color = {r = 1.0, g = 1.0, b = 1.0}, radius = 25.0, dynamic = true}
					},
					onInit = function(self) end
				})]]

 --[[c_createEntity(prototypes["base"]:derive{
				name = "torch1",
				initialComponents = {
						position = {x = 20.0, y = 10.0, z = 2.5},
						lightPoint = {color = {r = 1.0, g = 1.0, b = 0.0}, radius = 20.0, dynamic = true}
					},
					onInit = function(self) end
				})]]

--[[mouseSpotlight = c_createEntity(prototypes["base"]:derive{
				name = "spot",
				initialComponents = {
						position = {x = 14.0, y = 14.0, z = 6.0},
						lightPoint = {color = {r = 1.0, g = 1.0, b = 1.0}, radius = 30.0, dynamic = true},
						directionalLight = {angle = math.pi / 4.0},
						direction = c_normalizeVec({ x = 1.0, y = 1.0, z = -3.0})
					},
					onInit = function(self) end
				})]]

dofile(c_getScriptsFolder() .. "controls.lua")
--[[
fire = c_createEntity(prototypes["base"]:derive{
		name = "fire",
		initialComponents = {
				position = {x = 15.0, y = 20.0, z = 1.0},
				particleEmitter = {
					maxParticlesCount = 400,
					maxLifeTime = 0.8,
					coreRadius = 5.0,
					particlesPerFrame = 100,
					startPointSize = 3.0,
					zSpeed = 200.0,
					velocity = 2.0,
					colorStart = { x = 1.0, y = 1.0, z = 0.5},
					colorEnd = { x = 1.0, y = 0.1, z = 0.1},
					pointSize = 3.0,
					type = 0
				}
			},
			onInit = function(self) end
		})

testFogEmitter = c_createEntity(prototypes["base"]:derive{
		name = "testFogEmitter",
		initialComponents = {
				position = {x = 15.0, y = 20.0, z = 1.0},
				particleEmitter = {
					maxParticlesCount = 100,
					maxLifeTime = 0.8,
					coreRadius = 0.6,
					particlesPerFrame = 100,
					startPointSize = 3.0,
					zSpeed = 200.0,
					colorStart = { x = 1.0, y = 1.0, z = 0.5},
					colorEnd = { x = 1.0, y = 0.1, z = 0.1},
					velocity = 2.0,
					type = 0
				}
			},
			onInit = function(self) end
		}]]--

--c_addToInventory(0, 0, weaponPrototypes.pistol)
