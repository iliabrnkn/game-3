return 
{
	{
		modelName = "road",
		tilesets = {
			{
				name = "road1"
			},
			{
				name = "road2"
			},
			{
				name = "road_and_grass1"
			},
			{
				name = "road_and_grass2"
			},
			{
				name = "road_and_grass3"
			},
			{
				name = "road3(parking)"
			}
		}
	}
}