return
{
	{
		modelName = "suburb_garage_door_1",
		tilesets = {
			{
				name = "suburb_garage_door1",
				tiles = {
					1, 2, 3,
					7, 8, 9,
					13, 14, 15
				}
			}
		}
	},
	{
		modelName = "suburb_garage_door_2",
		tilesets = {
			{
				name = "suburb_garage_door1",
				tiles = {
					4, 5, 6,
					10, 11, 12,
					16, 17, 18
				}
			}
		}
	}
}