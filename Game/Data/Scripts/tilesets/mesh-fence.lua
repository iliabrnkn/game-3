return {
	{
		modelName = "mesh_fence_1",
		tilesets = {
			{
				name = "mesh_fence(small)1",
				tiles = {
					1, 5,
					1 + 14, 5 + 14
				}
			}
		}
	},
	{
		modelName = "mesh_fence_high_1",
		tilesets = {
			{
				name = "mesh_fence1",
				tiles = {
					1, 5,
					1 + 14, 5 + 14
				}
			}
		}
	},
	{
		modelName = "flat_wall_offset_right",
		tilesets = {
			{
				name = "mesh_fence(small)1",
				tiles = {
					2, 3, 4,
					2 + 14, 3 + 14, 4 + 14
				}
			},
			{
				name = "mesh_fence1",
				tiles = {
					2, 3, 4,
					2 + 14, 3 + 14, 4 + 14,
					57, 58, 59
				}
			}
		}
	},
	{
		modelName = "mesh_fence_2",
		tilesets = {
			{
				name = "mesh_fence(small)1",
				tiles = {
					6,
					6 + 14
				}
			}
		}
	},
	{
		modelName = "mesh_fence_high_2",
		tilesets = {
			{
				name = "mesh_fence1",
				tiles = {
					6,
					6 + 14
				}
			}
		}
	},
	{
		modelName = "mesh_fence_3",
		tilesets = {
			{
				name = "mesh_fence(small)1",
				tiles = {
					7,
					7 + 14
				}
			}
		}
	},
	{
		modelName = "mesh_fence_high_3",
		tilesets = {
			{
				name = "mesh_fence1",
				tiles = {
					7,
					7 + 14,
					63,
					70
				}
			}
		}
	},
	{
		modelName = "mesh_fence_4",
		tilesets = {
			{
				name = "mesh_fence(small)1",
				tiles = {
					8, 12,
					8 + 14, 12 + 14
				}
			}
		}
	},
	{
		modelName = "mesh_fence_high_4",
		tilesets = {
			{
				name = "mesh_fence1",
				tiles = {
					8, 12,
					8 + 14, 12 + 14
				}
			}
		}
	},
	{
		modelName = "flat_wall_offset_left",
		tilesets = {
			{
				name = "mesh_fence(small)1",
				tiles = {
					9, 10, 11,
					9 + 14, 10 + 14, 11 + 14
				}
			},
			{
				name = "mesh_fence1",
				tiles = {
					9, 10, 11,
					9 + 14, 10 + 14, 11 + 14,
					64, 65, 66
				}
			}
		}
	},
	{
		modelName = "mesh_fence_5",
		tilesets = {
			{
				name = "mesh_fence(small)1",
				tiles = {
					13,
					13 + 14
				}
			}
		}
	},
	{
		modelName = "mesh_fence_high_5",
		tilesets = {
			{
				name = "mesh_fence1",
				tiles = {
					13,
					13 + 14
				}
			}
		}
	},
	{
		modelName = "mesh_fence_6",
		tilesets = {
			{
				name = "mesh_fence(small)1",
				tiles = {
					14,
					14 + 14
				}
			}
		}
	},
	{
		modelName = "mesh_fence_high_6",
		tilesets = {
			{
				name = "mesh_fence1",
				tiles = {
					14,
					14 + 14
				}
			}
		}
	},
	{
		modelName = "mesh_fence_barbwire_1",
		tilesets = {
			{
				name = "mesh_fence1",
				tiles = {
					29, 33,
					29 + 14, 33 + 14
				}
			}
		}
	},
	{
		modelName = "mesh_fence_barbwire_2",
		tilesets = {
			{
				name = "mesh_fence1",
				tiles = {
					30, 31, 32,
					30 + 14, 31 + 14, 32 + 14,
					60, 61, 62
				}
			}
		}
	},
	{
		modelName = "mesh_fence_barbwire_3",
		tilesets = {
			{
				name = "mesh_fence1",
				tiles = {
					34,
					34 + 14
				}
			}
		}
	},
	{
		modelName = "mesh_fence_barbwire_4",
		tilesets = {
			{
				name = "mesh_fence1",
				tiles = {
					35,
					35 + 14
				}
			}
		}
	},
	{
		modelName = "mesh_fence_barbwire_5",
		tilesets = {
			{
				name = "mesh_fence1",
				tiles = {
					36, 40,
					36 + 14, 40 + 14
				}
			}
		}
	},
	{
		modelName = "mesh_fence_barbwire_6",
		tilesets = {
			{
				name = "mesh_fence1",
				tiles = {
					37, 38, 39,
					37 + 14, 38 + 14, 39 + 14,
					67, 68, 69
				}
			}
		}
	},
	{
		modelName = "mesh_fence_barbwire_7",
		tilesets = {
			{
				name = "mesh_fence1",
				tiles = {
					41,
					41 + 14
				}
			}
		}
	},
	{
		modelName = "mesh_fence_barbwire_8",
		tilesets = {
			{
				name = "mesh_fence1",
				tiles = {
					42,
					42 + 14
				}
			}
		}
	},

	-- MESH GATES

	{
		modelName = "mesh_gate_1",
		tilesets = {
			{
				name = "mesh_gates1",
				tiles = {
					1, 2, 3,
					7, 8, 9,
					13, 14, 15,
					19, 20, 21,
					25, 26, 27
				}
			}
		}
	},
	{
		modelName = "mesh_gate_2",
		tilesets = {
			{
				name = "mesh_gates1",
				tiles = {
					4, 5, 6,
					10, 11, 12,
					16, 17, 18,
					22, 23, 24,
					28, 29, 30
				}
			}
		}
	}
}