return {
	{
		modelName = "garage_roof_right_side",
		tilesets = 
		{
			{
				name = "roof_section(garage)1",
				tiles = {
					1, 2, 3, 4, 5, 6, 7, 8,
					17, 18, 19, 20, 21, 22, 23, 24,
					33, 34, 35, 36, 37, 38, 39, 40
				}
			}
		}
	},
	{
		modelName = "garage_roof_left_side",
		tilesets = 
		{
			{
				name = "roof_section(garage)1",
				tiles = {
					9, 10, 11, 12, 13, 14, 15, 16,
					25, 26, 27, 28, 29, 30, 31, 32,
					41, 42, 43, 44, 45, 46, 47, 48
				}
			}
		}
	},

--------

	{
		modelName = "roof_section(large)_alt1_left_side",
		tilesets = 
		{
			{
				name = "roof_section(large)_alt1",
				tiles = {
					1, 2, 3, 4, 5, 6,
					9, 10, 11, 12
				}
			}
		}
	},
	{
		modelName = "roof_section(large)_alt1_left_side_chimney_1",
		tilesets = 
		{
			{
				name = "roof_section(large)_alt1",
				tiles = {
					7
				}
			}
		}
	},
	{
		modelName = "roof_section(large)_alt1_left_side_chimney_2",
		tilesets = 
		{
			{
				name = "roof_section(large)_alt1",
				tiles = {
					8
				}
			}
		}
	},
	{
		modelName = "roof_section(large)_alt1_right_side",
		tilesets = 
		{
			{
				name = "roof_section(large)_alt1",
				tiles = {
					13, 14, 15, 16,
					19, 20, 
					21, 22, 23, 24
				}
			}
		}
	},
	{
		modelName = "roof_section(large)_alt1_right_side_chimney_1",
		tilesets = 
		{
			{
				name = "roof_section(large)_alt1",
				tiles = {
					18
				}
			}
		}
	},
	{
		modelName = "roof_section(large)_alt1_right_side_chimney_2",
		tilesets = 
		{
			{
				name = "roof_section(large)_alt1",
				tiles = {
					17
				}
			}
		}
	},



--------
	{
		modelName = "roof_section(large)1_left_side",
		tilesets = {
			{
				name = "roof_section(large)1",
				tiles = {
					1, 2, 3, 4, 5, 6,
					9, 10, 11, 12
				}
			}
		}
	},
	{
		modelName = "roof_section(large)1_left_side_chimney_1",
		tilesets = {
			{
				name = "roof_section(large)1",
				tiles = {
					7
				}
			}
		}
	},
	{
		modelName = "roof_section(large)1_left_side_chimney_2",
		tilesets = {
			{
				name = "roof_section(large)1",
				tiles = {
					8
				}
			}
		}
	},
	{
		modelName = "roof_section(large)1_right_side",
		tilesets = {
			{
				name = "roof_section(large)1",
				tiles = {
					13, 14, 15, 16,
					19, 20,
					21, 22, 23, 24
				}
			}
		}
	},
	{
		modelName = "roof_section(large)1_right_side_chimney_1",
		tilesets = {
			{
				name = "roof_section(large)1",
				tiles = {
					18
				}
			}
		}
	},
	{
		modelName = "roof_section(large)1_right_side_chimney_2",
		tilesets = {
			{
				name = "roof_section(large)1",
				tiles = {
					17
				}
			}
		}
	},

	----

	{
		modelName = "roof_section(medium)1_1",
		tilesets = {
			{
				name = "roof_section(medium)1",
				tiles = {
					1, 3, 5, 7, 9, 11
				}
			}
		}
	},
	{
		modelName = "roof_section(medium)1_2",
		tilesets = {
			{
				name = "roof_section(medium)1",
				tiles = {
					2, 4, 6, 8, 10, 12
				}
			}
		}
	},

	-------

	{
		modelName = "roof_section(windows)_1",
		tilesets = {
			{
				name = "roof_section(windows)1",
				tiles = {
					1, 2,
					9, 10,
					17, 18
				}
			}
		}
	},
	{
		modelName = "roof_section(windows)_1_backward",
		tilesets = {
			{
				name = "roof_section(windows)1",
				tiles = {
					5, 6,
					13, 14,
					21, 22
				}
			}
		},
		texOffset = {x = -5, y = -5}
	},
	{
		modelName = "roof_section(windows)_2",
		tilesets = {
			{
				name = "roof_section(windows)1",
				tiles = {
					3, 4,
					11, 12,
					19, 20
				}
			}
		},
		texOffset = {x = 0, y = 0}
	},
	{
		modelName = "roof_section(windows)_2_backward",
		tilesets = {
			{
				name = "roof_section(windows)1",
				tiles = {
					7, 8,
					15, 16,
					23, 24
				}
			}
		},
		texOffset = {x = 5, y = -5}
	}
}