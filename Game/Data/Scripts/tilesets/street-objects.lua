return
{
	-- fireplugs
	{
		modelName = "fireplug_1",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					1
				},
				normalTexOffsetFactor = {x = -0.013}
			}
		}
	},
	{
		modelName = "fireplug_2",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					2
				},
				normalTexOffsetFactor = {x = -0.013}
			}
		}
	},
	{
		modelName = "fireplug_3",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					3
				},
				normalTexOffsetFactor = {x = -0.013}
			}
		}
	},
	{
		modelName = "fireplug_4",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					4
				},
				normalTexOffsetFactor = {x = -0.013}
			}
		}
	},
	{
		modelName = "fireplug_5",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					29
				},
				normalTexOffsetFactor = {x = -0.013}
			}
		}
	},
	{
		modelName = "fireplug_6",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					30
				},
				normalTexOffsetFactor = {x = -0.013}
			}
		}
	},
	{
		modelName = "fireplug_7",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					31
				},
				normalTexOffsetFactor = {x = -0.013}
			}
		}
	},
	{
		modelName = "fireplug_8",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					32
				},
				normalTexOffsetFactor = {x = -0.013}
			}
		}
	},
	-- road poles
	{
		modelName = "road_pole_1",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					5, 13
				}
			}
		}
	},
	{
		modelName = "road_pole_2",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					6, 14
				}
			}
		}
	},
	{
		modelName = "road_pole_3",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					7, 15
				}
			}
		}
	},
	{
		modelName = "road_pole_4",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					8, 16
				}
			}
		}
	},
	{
		modelName = "bent_road_pole_1",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					9
				},
				normalTexOffsetFactor = {x = -0.015}
			}
		}
	},
	{
		modelName = "bent_road_pole_2",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					10
				},
				normalTexOffsetFactor = {x = -0.015}
			}
		}
	},
	{
		modelName = "bent_road_pole_3",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					11
				},
				normalTexOffsetFactor = {x = -0.015}
			}
		}
	},
	{
		modelName = "bent_road_pole_4",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					12
				},
				normalTexOffsetFactor = {x = -0.015}
			}
		}
	},
	{
		modelName = "bent_road_pole_5",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					17
				},
				normalTexOffsetFactor = {x = -0.015}
			}
		}
	},
	{
		modelName = "bent_road_pole_6",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					18
				},
				normalTexOffsetFactor = {x = -0.015}
			}
		}
	},
	{
		modelName = "bent_road_pole_7",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					19
				},
				normalTexOffsetFactor = {x = -0.015}
			}
		}
	},
	{
		modelName = "bent_road_pole_8",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					20
				},
				normalTexOffsetFactor = {x = -0.015}
			}
		}
	},

	-- parking_meters

	{
		modelName = "parking_meter_1",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					21
				},
				normalTexOffsetFactor = {x = -0.01}
			}
		}
	},
	{
		modelName = "parking_meter_2",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					22
				},
				normalTexOffsetFactor = {x = -0.01, y = 0.01}
			}
		}
	},
	{
		modelName = "parking_meter_3",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					23
				},
				normalTexOffsetFactor = {x = -0.01, y = 0.01}
			}
		}
	},
	{
		modelName = "parking_meter_4",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					24
				},
				normalTexOffsetFactor = {x = -0.01, y = 0.01}
			}
		}
	},
	{
		modelName = "bent_parking_meter_1",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					25
				},
				normalTexOffsetFactor = {x = -0.015, y = 0.015}
			}
		}
	},
	{
		modelName = "bent_parking_meter_2",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					26
				},
				normalTexOffsetFactor = {x = -0.015, y = 0.015}
			}
		}
	},
	{
		modelName = "bent_parking_meter_3",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					27
				},
				normalTexOffsetFactor = {x = -0.015, y = 0.015}
			}
		}
	},
	{
		modelName = "bent_parking_meter_4",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					28
				},
				normalTexOffsetFactor = {x = -0.015, y = 0.015}
			}
		}
	},

	-- phone booths

	{
		modelName = "phone_booth_1",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					33, 37
				}
			}
		}
	},
	{
		modelName = "phone_booth_2",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					34, 38
				}
			}
		}
	},
	{
		modelName = "phone_booth_3",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					35, 39
				}
			}
		}
	},
	{
		modelName = "phone_booth_4",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					36, 40
				}
			}
		}
	},
	{
		modelName = "phone_booth_5",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					41, 43
				}
			}
		}
	},
	{
		modelName = "phone_booth_6",
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					42, 44
				}
			}
		}
	},

	-- newspaper vending machines

	{
		modelName = 'newspaper_vending_machine_1',
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					45, 51, 59, 65
				}
			}
		}
	},
	{
		modelName = 'newspaper_vending_machine_2',
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					46, 52, 60, 66
				}
			}
		}
	},
	{
		modelName = 'newspaper_vending_machine_3',
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					47, 53, 61, 67
				}
			}
		}
	},
	{
		modelName = 'newspaper_vending_machine_4',
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					48, 54, 62, 68
				}
			}
		}
	},
	{
		modelName = 'newspaper_vending_machine_5',
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					49, 55, 63, 69
				}
			}
		}
	},
	{
		modelName = 'newspaper_vending_machine_6',
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					50, 56, 64, 70
				}
			}
		}
	},
	{
		modelName = 'newspaper_vending_machine_7',
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					57, 71
				}
			}
		}
	},
	{
		modelName = 'newspaper_vending_machine_8',
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					58, 72
				}
			}
		}
	},

	-- gas bottles

	{
		modelName = 'gas_bottle_1',
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					73
				}
			}
		}
	},
	{
		modelName = 'gas_bottle_2',
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					74
				}
			}
		}
	},
	{
		modelName = 'gas_bottle_3',
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					75
				}
			}
		}
	},
	{
		modelName = 'gas_bottle_4',
		tilesets = {
			{
				name = "street_objects1",
				tiles = {
					76
				}
			}
		}
	}

}