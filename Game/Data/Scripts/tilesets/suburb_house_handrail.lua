return
{
	{
		modelName = "suburb_house_handrail_3_1",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					1
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					1
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					1
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_2",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					2
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					2
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					2
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_3",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					3
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					3
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					3
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_4",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					4
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					4
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					4
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_headed_1",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					33
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					33
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					33
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_headed_2",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					34
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					34
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					34
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_headed_3",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					35
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					35
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					35
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_headed_4",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					36
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					36
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					36
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_headed_5",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					37
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					37
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					37
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_headed_6",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					38
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					38
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					38
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_headed_7",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					39
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					39
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					39
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_headed_8",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					40
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					40
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					40
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_headed_9",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					41, 42
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					41, 42
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					41, 42
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_headed_10",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					43, 44
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					43, 44
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					43, 44
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_middle_headed_1",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					45
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					45
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					45
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_middle_headed_2",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					46
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					46
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					46
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_middle_headed_3",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					47
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					47
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					47
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_middle_headed_4",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					48
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					48
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					48
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_headed_single_1",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					49, 50
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					49, 50
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					49, 50
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_headed_single_2",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					51
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					51
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					51
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_headed_single_3",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					52
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					52
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					52
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_5",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					5, 15
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					5, 15
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					5, 15
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_6",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					6, 16
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					6, 16
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					6, 16
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_7",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					7, 17
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					7, 17
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					7, 17
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_8",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					8, 18
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					8, 18
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					8, 18
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_9",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					9, 19
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					9, 19
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					9, 19
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_10",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					10, 20
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					10, 20
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					10, 20
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_11",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					11, 21
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					11, 21
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					11, 21
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_12",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					12, 22
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					12, 22
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					12, 22
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_13",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					13, 23
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					13, 23
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					13, 23
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_14",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					14, 24
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					14, 24
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					14, 24
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_middle_1",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					25, 29
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					25, 29
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					25, 29
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_middle_2",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					26, 30
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					26, 30
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					26, 30
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_middle_3",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					27, 31
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					27, 31
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					27, 31
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_3_middle_4",
		tilesets = {
			{
				name = "suburb_house_handrail1",
				tiles = {
					28, 32
				}
			},
			{
				name = "suburb_house_handrail2",
				tiles = {
					28, 32
				}
			},
			{
				name = "suburb_house_handrail3",
				tiles = {
					28, 32
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_4_1",
		tilesets = {
			{
				name = "suburb_house_handrail4",
				tiles = {
					1, 13
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_4_2",
		tilesets = {
			{
				name = "suburb_house_handrail4",
				tiles = {
					2, 14
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_4_3",
		tilesets = {
			{
				name = "suburb_house_handrail4",
				tiles = {
					3, 15
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_4_4",
		tilesets = {
			{
				name = "suburb_house_handrail4",
				tiles = {
					4, 16
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_4_5",
		tilesets = {
			{
				name = "suburb_house_handrail4",
				tiles = {
					5, 17
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_4_6",
		tilesets = {
			{
				name = "suburb_house_handrail4",
				tiles = {
					6, 18
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_4_7",
		tilesets = {
			{
				name = "suburb_house_handrail4",
				tiles = {
					7, 19
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_4_8",
		tilesets = {
			{
				name = "suburb_house_handrail4",
				tiles = {
					8, 20
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_4_9",
		tilesets = {
			{
				name = "suburb_house_handrail4",
				tiles = {
					9
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_4_10",
		tilesets = {
			{
				name = "suburb_house_handrail4",
				tiles = {
					10, 12
				}
			}
		}
	},
	{
		modelName = "suburb_house_handrail_4_11",
		tilesets = {
			{
				name = "suburb_house_handrail4",
				tiles = {
					11
				}
			}
		}
	}
}