return {
	{
		modelName = "wooden_pole_1",
		tilesets =
		{
			{
				name = "wooden_poles1",
				tiles = {
					1
				}
			}
		}
	},
	{
		modelName = "wooden_pole_2",
		tilesets =
		{
			{
				name = "wooden_poles1",
				tiles = {
					2
				}
			}
		}
	},
	{
		modelName = "wooden_pole_3",
		tilesets =
		{
			{
				name = "wooden_poles1",
				tiles = {
					3
				}
			}
		}
	},
	{
		modelName = "wooden_pole_4",
		tilesets =
		{
			{
				name = "wooden_poles1",
				tiles = {
					4
				}
			}
		}
	},
	{
		modelName = "electric_pole_1",
		tilesets =
		{
			{
				name = "wooden_poles1",
				tiles = {
					5
				}
			}
		}
	},
	{
		modelName = "electric_pole_2",
		tilesets =
		{
			{
				name = "wooden_poles1",
				tiles = {
					6
				}
			}
		}
	},
	{
		modelName = "electric_pole_3",
		tilesets =
		{
			{
				name = "wooden_poles1",
				tiles = {
					7
				}
			}
		}
	},
	{
		modelName = "electric_pole_4",
		tilesets =
		{
			{
				name = "wooden_poles1",
				tiles = {
					8
				}
			}
		}
	},
	{
		modelName = "combined_pole_1",
		tilesets =
		{
			{
				name = "wooden_poles1",
				tiles = {
					9
				}
			}
		}
	},
	{
		modelName = "combined_pole_2",
		tilesets =
		{
			{
				name = "wooden_poles1",
				tiles = {
					10
				}
			}
		}
	},
	{
		modelName = "combined_pole_3",
		tilesets =
		{
			{
				name = "wooden_poles1",
				tiles = {
					11
				}
			}
		}
	},
	{
		modelName = "combined_pole_4",
		tilesets =
		{
			{
				name = "wooden_poles1",
				tiles = {
					12
				}
			}
		}
	},
	{
		modelName = "combined_pole_large_1",
		tilesets =
		{
			{
				name = "wooden_poles1",
				tiles = {
					13
				}
			}
		}
	},
	{
		modelName = "combined_pole_large_2",
		tilesets =
		{
			{
				name = "wooden_poles1",
				tiles = {
					14
				}
			}
		}
	},
	{
		modelName = "combined_pole_large_3",
		tilesets =
		{
			{
				name = "wooden_poles1",
				tiles = {
					15
				}
			}
		}
	},
	{
		modelName = "combined_pole_large_4",
		tilesets =
		{
			{
				name = "wooden_poles1",
				tiles = {
					16
				}
			}
		}
	}
}