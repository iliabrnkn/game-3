return 
{
	{
		modelName = "boards",
		tilesets = {
			{
				name = "wooden_fence1",
				tiles = {
					53, 54, 55, 56
				}
			}
		}
	},
	{
		modelName = "wooden_fence_balk_right",
		tilesets = {
			{
				name = "wooden_fence1",
				tiles = {
					49,
					51
				}
			}
		}
	},
	{
		modelName = "wooden_fence_balk_left",
		tilesets = {
			{
				name = "wooden_fence1",
				tiles = {
					50,
					52
				}
			}
		}
	}
}