return
{
	{
		modelName = "dumbster_1",
		tilesets = {
			{
				name = "dumbster(small)1",
				tiles = {
					1, 7
				}
			}
		}
	},
	{
		modelName = "dumbster_2",
		tilesets = {
			{
				name = "dumbster(small)1",
				tiles = {
					2, 8
				}
			}
		}
	},
	{
		modelName = "dumbster_3",
		tilesets = {
			{
				name = "dumbster(small)1",
				tiles = {
					3, 9
				}
			}
		}
	},
	{
		modelName = "dumbster_4",
		tilesets = {
			{
				name = "dumbster(small)1",
				tiles = {
					4, 10
				}
			}
		}
	},
	{
		modelName = "dumbster_5",
		tilesets = {
			{
				name = "dumbster(small)1",
				tiles = {
					5, 11
				}
			}
		}
	},
	{
		modelName = "dumbster_6",
		tilesets = {
			{
				name = "dumbster(small)1",
				tiles = {
					6, 12
				}
			}
		}
	}
}