return 
{
	{
		modelName = "tire_1",
		tilesets = {
			{
				name = "tires1",
				tiles = {
					1, 2, 5
				}
			}
		}
	},
	{
		modelName = "tire_2",
		tilesets = {
			{
				name = "tires1",
				tiles = {
					3
				}
			}
		}
	},
	{
		modelName = "tire_3",
		tilesets = {
			{
				name = "tires1",
				tiles = {
					4
				}
			}
		}
	},
	{
		modelName = "tire_4",
		tilesets = {
			{
				name = "tires1",
				tiles = {
					6
				}
			}
		}
	},
	{
		modelName = "tire_5",
		tilesets = {
			{
				name = "tires1",
				tiles = {
					7
				}
			}
		}
	},
	{
		modelName = "tire_6",
		tilesets = {
			{
				name = "tires1",
				tiles = {
					8
				}
			}
		}
	},
	{
		modelName = "tire_7",
		tilesets = {
			{
				name = "tires1",
				tiles = {
					9
				}
			}
		}
	},
	{
		modelName = "tire_8",
		tilesets = {
			{
				name = "tires1",
				tiles = {
					10
				}
			}
		}
	},
	{
		modelName = "tire_9",
		tilesets = {
			{
				name = "tires1",
				tiles = {
					11
				}
			}
		}
	},
	{
		modelName = "tire_10",
		tilesets = {
			{
				name = "tires1",
				tiles = {
					12
				}
			}
		}
	},
	{
		modelName = "tire_11",
		tilesets = {
			{
				name = "tires1",
				tiles = {
					13
				}
			}
		}
	},
	{
		modelName = "tire_12",
		tilesets = {
			{
				name = "tires1",
				tiles = {
					14
				}
			}
		}
	},
	{
		modelName = "tire_13",
		tilesets = {
			{
				name = "tires1",
				tiles = {
					15
				}
			}
		}
	},
	{
		modelName = "tire_14",
		tilesets = {
			{
				name = "tires1",
				tiles = {
					16
				}
			}
		}
	},
	{
		modelName = "tire_15",
		tilesets = {
			{
				name = "tires1",
				tiles = {
					17
				}
			}
		}
	},
	{
		modelName = "tire_16",
		tilesets = {
			{
				name = "tires1",
				tiles = {
					18
				}
			}
		}
	},
	{
		modelName = "tire_17",
		tilesets = {
			{
				name = "tires1",
				tiles = {
					19
				}
			}
		}
	},
	{
		modelName = "tire_18",
		tilesets = {
			{
				name = "tires1",
				tiles = {
					20
				}
			}
		}
	},
	{
		modelName = "tire_19",
		tilesets = {
			{
				name = "tires1",
				tiles = {
					21
				}
			}
		}
	},
	{
		modelName = "tire_20",
		tilesets = {
			{
				name = "tires1",
				tiles = {
					22
				}
			}
		}
	}
}