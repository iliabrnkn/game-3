return
{
	{
		modelName = "barrel",
		tilesets = {
			{
				name = "barrels1",
				tiles = {
					1, 2,
					21 + 1, 21 + 2, 
					42 + 1, 42 + 2
				}
			}
		}
	},
	{
		modelName = "barrel_2",
		tilesets = {
			{
				name = "barrels1",
				tiles = {
					3, 4, 5,
					21 + 3, 21 + 4, 21 + 5,
					42 + 3, 42 + 4, 42 + 5
				}
			}
		}
	},
	{
		modelName = "crushed_barrel_1",
		tilesets = {
			{
				name = "barrels1",
				tiles = {
					6,
					21 + 6, 
					42 + 6
				}
			}
		}
	},
	{
		modelName = "crushed_barrel_1_open",
		tilesets = {
			{
				name = "barrels1",
				tiles = {
					7,
					21 + 7,
					42 + 7
				}
			}
		}
	},
	{
		modelName = "crushed_barrel_2",
		tilesets = {
			{
				name = "barrels1",
				tiles = {
					8,
					21 + 8,
					42 + 8
				}
			}
		}
	},
	{
		modelName = "crushed_barrel_3",
		tilesets = {
			{
				name = "barrels1",
				tiles = {
					9
				}
			}
		}
	},
	{
		modelName = "barrel_lying",
		tilesets = {
			{
				name = "barrels1",
				tiles = {
					10,
					21 + 10,
					42 + 10
				}
			}
		}
	},
	{
		modelName = "barrel_lying_2",
		tilesets = {
			{
				name = "barrels1",
				tiles = {
					11, 15,
					21 + 11, 21 + 15,
					42 + 11, 42 + 15
				}
			}
		}
	},
	{
		modelName = "barrel_lying_2_open",
		tilesets = {
			{
				name = "barrels1",
				tiles = {
					12, 
					21 + 12,
					42 + 12
				}
			}
		}
	},
	{
		modelName = "barrel_lying_3",
		tilesets = {
			{
				name = "barrels1",
				tiles = {
					13, 
					21 + 13, 
					42 + 13
				}
			}
		}
	},
	{
		modelName = "barrel_lying_3_open",
		tilesets = {
			{
				name = "barrels1",
				tiles = {
					14,
					21 + 14,
					42 + 14
				}
			}
		}
	},
	{
		modelName = "barrel_lying_4",
		tilesets = {
			{
				name = "barrels1",
				tiles = {
					16,
					21 + 16,
					42 + 16
				}
			}
		}
	},
	{
		modelName = "barrel_lying_5_open",
		tilesets = {
			{
				name = "barrels1",
				tiles = {
					17, 
					21 + 17, 
					42 + 17
				}
			}
		}
	},
	{
		modelName = "barrel_lying_5",
		tilesets = {
			{
				name = "barrels1",
				tiles = {
					18, 21,
					21 + 18, 21 + 21,
					42 + 18, 42 + 21
				}
			}
		}
	},
	{
		modelName = "barrel_lying_6_open",
		tilesets = {
			{
				name = "barrels1",
				tiles = {
					19, 
					21 + 19, 
					42 + 19
				}
			}
		}
	},
	{
		modelName = "barrel_lying_6",
		tilesets = {
			{
				name = "barrels1",
				tiles = {
					19, 20,
					21 + 19, 21 + 20,
					42 + 19, 42 + 20
				}
			}
		}
	},
	{
		modelName = "barrel_bonfire",
		tilesets = {
			{
				name = "barrels2",
				tiles = {
					1
				}
			}
		}
	}
}