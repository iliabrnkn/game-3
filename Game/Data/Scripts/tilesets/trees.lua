return 
{
	{
		modelName = "naked_tree_1",
		tilesets = {
			{
				name = "trees1",
				tiles = {
					1
				},
				normalTexOffsetFactor = {x = -0.015}
			}
		},
	},
	{
		modelName = "leafy_tree_1",
		tilesets = {
			{
				name = "trees1",
				tiles = {
					2
				}
			}
		}
	},
	{
		modelName = "leafy_tree_2",
		tilesets = {
			{
				name = "trees1",
				tiles = {
					3
				}
			}
		}
	},
	{
		modelName = "dead_tree_1",
		tilesets = {
			{
				name = "trees2",
				tiles = {
					1
				}
			}
		}
	},
	{
		modelName = "dead_tree_2",
		tilesets = {
			{
				name = "trees2",
				tiles = {
					2
				}
			}
		}
	},
	{
		modelName = "dead_tree_3",
		tilesets = {
			{
				name = "trees2",
				tiles = {
					3
				}
			}
		}
	}
}