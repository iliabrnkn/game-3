return {
	{
		modelName = "wall_left",
		tilesets = {
			{
				name = "fence(small)1",
				tiles = {
					1, 9, 11, 13
				}
			},
			{
				name = "apartments_interior_walls1",
				tiles = {
					1, 2, 3, 4
				}
			},
			{
				name = "wooden_fence1",
				tiles = {
					5, 6, 7,
					11, 12, 13, 14,
					15, 16, 17,
					30, 31, 32, 34, 35,
					36, 37, 38, 39, 40
				}
			},
			{
				name = "suburb_fence(large)1",
				tiles = {
					8, 9, 10, 11, 12, 13, 16, 18
				}
			}
		},
		
		texOffset = {x = -1}
	},
	{
		modelName = "wall_right",
		tilesets = {
			{
				name = "fence(small)1",
				tiles = {
					2, 5, 10, 12, 14
				}
			},
			{
				name = "apartments_interior_walls1",
				tiles = {
					7
				}
			},
			{
				name = "wooden_fence1",
				tiles = {
					1, 2, 3,
					19, 20, 21,
					22, 23, 24, 25, 26, 27, 28,
					42,
					43, 44, 45, 46, 47, 48
				}
			},
			{
				name = "suburb_fence(large)1",
				tiles = {
					1, 2, 3, 4, 5, 6, 15, 17
				},
				texOffset = {x = 3}
			}
		},

		texOffset = {x = -1}
	},
	{
		modelName = "wall_corner",
		tilesets = {
			{
				name = "fence(small)1",
				tiles = {
					3
				}
			},
			{
				name = "apartments_interior_walls1",
				tiles = {
					6
				}
			},
			{
				name = "wooden_fence1",
				tiles = {
					4,
					18,
					29,
					41
				}
			},
			{
				name = "suburb_fence(large)1",
				tiles = {
					7
				},
				texOffset = {x = 3, y = -3}
			}
		},
		texOffset = {x = -1}
	},
	{
		modelName = "wall_edge",
		tilesets = {
			{
				name = "wooden_fence1",
				tiles = {
					8, 9, 10, 
					33
				}
			},
			{
				name = "suburb_fence(large)1",
				tiles = {
					14
				}
			}
		},
		texOffset = {x = -1}
	},
	--[[{
		modelName = "foregroundLeftSideWall",
		tilesets = {
			{
				name = "fence(small)1",
				tiles = {
					8, 16, 18, 20
				}
			}
		}
	},
	{
		modelName = "foregroundRightSideWall",
		tilesets = {
			{
				name = "fence(small)1",
				tiles = {
					6, 7, 15, 17, 19
				}
			}
		}
	},
	{
		modelName = "foregroundCornerWalls",
		tilesets = {
			{
				name = "fence(small)1",
				tiles = {
					4
				}
			}
		}
	}]]
	{
		modelName = "flat_wall_left",
		tilesets =
		{ 
			{
				name = "suburb_house_walls1(gray)",
				tiles = {
					1, 2, 3, 4, 5, 6, 7, 8, 9,
					48, 49, 50, 51, 52, 53
				}
			},
			{
				name = "suburb_house_walls2(gray)",
				tiles = {
					1, 2, 3, 4, 5, 6, 7, 8, 9
				}
			},
			{
				name = "suburb_house_walls1(green)",
				tiles = {
					1, 2, 3, 4, 5, 6, 7, 8, 9,
					48, 49, 50, 51, 52, 53
				}
			},
			{
				name = "suburb_house_walls2(green)",
				tiles = {
					1, 2, 3, 4, 5, 6, 7, 8, 9
				}
			},
			{
				name = "suburb_house_walls1(yellow)",
				tiles = {
					1, 2, 3, 4, 5, 6, 7, 8, 9,
					48, 49, 50, 51, 52, 53
				}
			},
			{
				name = "suburb_house_walls2(yellow)",
				tiles = {
					1, 2, 3, 4, 5, 6, 7, 8, 9
				}
			}
		}
	},
	{
		modelName = "flat_wall_right",
		tilesets =
		{ 
			{
				name = "suburb_house_walls1(gray)",
				tiles = {
					23, 24, 25, 26, 27, 28, 29, 30, 31,
					54, 55, 56, 57, 58, 59,
					74, 75, 76, 77
				}
			},
			{
				name = "suburb_house_walls2(gray)",
				tiles = {
					23, 24, 25, 26, 27, 28, 29, 30, 31
				}
			},
			{
				name = "suburb_house_walls1(green)",
				tiles = {
					23, 24, 25, 26, 27, 28, 29, 30, 31,
					54, 55, 56, 57, 58, 59,
					74, 75, 76, 77
				}
			},
			{
				name = "suburb_house_walls2(green)",
				tiles = {
					23, 24, 25, 26, 27, 28, 29, 30, 31
				}
			},
			{
				name = "suburb_house_walls1(yellow)",
				tiles = {
					23, 24, 25, 26, 27, 28, 29, 30, 31,
					54, 55, 56, 57, 58, 59,
					74, 75, 76, 77
				}
			},
			{
				name = "suburb_house_walls2(yellow)",
				tiles = {
					23, 24, 25, 26, 27, 28, 29, 30, 31
				}
			}
		}
	},
	{
		modelName = "flat_wall_large_left",
		tilesets =
		{ 
			{
				name = "suburb_house_walls1(gray)",
				tiles = {
					10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22,
					60, 61, 62, 63, 64,
					70, 71, 72, 73,
					78, 79, 80, 81
				}
			},
			{
				name = "suburb_house_walls2(gray)",
				tiles = {
					10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22
				}
			},
			{
				name = "suburb_house_walls1(green)",
				tiles = {
					10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22,
					60, 61, 62, 63, 64,
					70, 71, 72, 73,
					78, 79, 80, 81
				}
			},
			{
				name = "suburb_house_walls2(green)",
				tiles = {
					10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22
				}
			},
			{
				name = "suburb_house_walls1(yellow)",
				tiles = {
					10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22,
					60, 61, 62, 63, 64,
					70, 71, 72, 73,
					78, 79, 80, 81
				}
			},
			{
				name = "suburb_house_walls2(yellow)",
				tiles = {
					10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22
				}
			}
		}
	},
	{
		modelName = "flat_wall_large_right",
		tilesets =
		{ 
			{
				name = "suburb_house_walls1(gray)",
				tiles = {
					32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44,
					65, 66, 67, 68, 69
				}
			},
			{
				name = "suburb_house_walls2(gray)",
				tiles = {
					32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44
				}
			},
			{
				name = "suburb_house_walls1(green)",
				tiles = {
					32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44,
					65, 66, 67, 68, 69
				}
			},
			{
				name = "suburb_house_walls2(green)",
				tiles = {
					32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44
				}
			},
			{
				name = "suburb_house_walls1(yellow)",
				tiles = {
					32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44,
					65, 66, 67, 68, 69
				}
			},
			{
				name = "suburb_house_walls2(yellow)",
				tiles = {
					32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44
				}
			}
		}
	},
	{
		modelName = "flat_wall_corner",
		tilesets =
		{ 
			{
				name = "suburb_house_walls1(gray)",
				tiles = {
					45, 46, 47
				}
			},
			{
				name = "suburb_house_walls2(gray)",
				tiles = {
					45, 46, 47
				}
			},
			{
				name = "suburb_house_walls1(green)",
				tiles = {
					45, 46, 47
				}
			},
			{
				name = "suburb_house_walls2(green)",
				tiles = {
					45, 46, 47
				}
			},
			{
				name = "suburb_house_walls1(yellow)",
				tiles = {
					45, 46, 47
				}
			},
			{
				name = "suburb_house_walls2(yellow)",
				tiles = {
					45, 46, 47
				}
			}
		}
	}
}