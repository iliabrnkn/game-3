-- туман

prototypes["fogEmitter"] = prototypes["base"]:derive{
	name = "fogEmitter",
	initialComponents = {
		particleEmitter = {
			maxParticlesCount = 800,
			maxLifeTime = 2.5,
			startColor = {x = 1.0, y = 0.0, z = 1.0 },
			endColor = { x = 0.5, y = 0.0, z = 0.5 },
			startParticleSize = 80.0,
			endParticleSize = 80.0,
			startVelocityFactor = 1.0,
			endVelocityFactor = 1.0, 
			minGid = 1,
			maxGid = 4,
			particleRespawnFunc = function(emitter, particleCount) 
				local result = {}

				c_consoleLog(tostring(c_random(100)))

				for i = 1, particleCount do
					local emitterPos = c_getPos(emitter)
					local currentParticle = { position = {}, startVelocity = {} }
					
					--[[currentParticle.position = {
						x = math.fmod(math.random(), 2) == 1 and math.random() or -math.random(), 
						y = math.fmod(math.random(), 2) == 1 and math.random() or -math.random()
					}]]

					if (math.fmod(c_random(100), 2) == 1) then
						currentParticle.position.x = emitterPos.x + emitter.coreRadius * 1.0 / c_random(20)
					else
						currentParticle.position.x = emitterPos.x - emitter.coreRadius * 1.0 / c_random(20)
					end

					if (math.fmod(c_random(100), 2) == 1) then
						currentParticle.position.y = emitterPos.y + emitter.coreRadius * 1.0 / c_random(20)
					else
						currentParticle.position.y = emitterPos.y - emitter.coreRadius * 1.0 / c_random(20)
					end

					currentParticle.position.z = 2.0
					
					--[[currentParticle.velocity = 
						c_normalizeVec({ x = currentParticle.position.x - emitterPos.x, y = currentParticle.position.y - emitterPos.y, z = 0.0 })]]

					currentParticle.startVelocity = c_normalizeVec({ 
						x = currentParticle.position.x - emitterPos.x,
						y = currentParticle.position.y - emitterPos.y
					})

					currentParticle.endVelocity = c_normalizeVec({ 
						x = currentParticle.position.x - emitterPos.x,
						y = currentParticle.position.y - emitterPos.y
					})

					table.insert(result, currentParticle)
				end

				return result
			end,
			blur = true,
			original = true,
			active = true
		},
		position = {x = 4, y = 4, z = 2.0}
	},
	coreRadius = 1.0
}

-- огонь

prototypes["fire"] = prototypes["base"]:derive{
	name = "fire",
	coreRadius = 0.5,
	initialComponents = {
		position = {x = 7.0, y = 4.0, z = 0.0},
		particleEmitter = {
			maxParticlesCount = 400,
			maxLifeTime = 2.0,
			startParticleSize = 3.0,
			endParticleSize = 0.0,
			startVelocityFactor = 1.0,
			endVelocityFactor = 1.0,
			startColor = { x = 1.0, y = 1.0, z = 0.5},
			endColor = { x = 0.1, y = 0.0, z = 0.0, a = 0.0},
			coreRadius = 1.0,
			blur = true,
			original = true,
			particleRespawnFunc = function(emitter, particleCount) 
			
				local result = {}
				
				for i = 1, particleCount do

					local emitterPos = c_getPos(emitter)
					local currentParticle = { 
						position = {x = emitterPos.x, y = emitterPos.y, z = emitterPos.z}, 
						startVelocity = {x = 0.0, y = 0.0, z = 0.0},
						endVelocity = {x = 0.0, y = 0.0, z = 0.0} 
					}

					if (math.fmod(c_random(100), 2) == 1) then
						currentParticle.position.x = emitterPos.x + emitter.coreRadius * 1.0 / (c_random(50) + 1)
					else
						currentParticle.position.x = emitterPos.x - emitter.coreRadius * 1.0 / (c_random(50) + 1)
					end

					if (math.fmod(c_random(100), 2) == 1) then
						currentParticle.position.y = emitterPos.y + emitter.coreRadius * 1.0 / (c_random(50) + 1)
					else
						currentParticle.position.y = emitterPos.y - emitter.coreRadius * 1.0 / (c_random(50) + 1)
					end

					currentParticle.startVelocity.z = 2.0
					currentParticle.endVelocity.z = 2.0

					table.insert(result, currentParticle)
				end

				return result
			end
		}
	},
	onInit = function(self) end
}

-- выстрел

prototypes["gunshot"] = prototypes["base"]:derive{
	name = "gunshot",
	gunshotProperties = {
		length = 1.0,
		offset = 1.0,
		widestPoint = 0.1, -- самая толстая точка на главной оси
		radius = 1.0,
		mainAxis = createVector(1.0, 0.0, 0.0),
		upAxis = createVector(0.0, 1.0, 0.0)
	},
	initialComponents = {
		position = {x = 7.0, y = 4.0, z = 2.0},
		particleEmitter = {
			maxParticlesCount = 800,
			maxLifeTime = 0.07,
			startParticleSize = 2.0,
			endParticleSize = 0.0,
			startVelocityFactor = 1.0,
			endVelocityFactor = 1.0,
			startColor = { r = 1.0, g = 1.0, b = 0.8},
			endColor = { r = 0.18, g = 0.0, b = 0.0},
			blur = true,
			original = true,
			particleRespawnFunc = function(emitter, particleCount) 
			
				local rightAxis = c_crossProduct(emitter.gunshotProperties.mainAxis, emitter.gunshotProperties.upAxis)
				local emitterPos = c_getPos(emitter)
				local result = {}
				
				for i = 1, particleCount do
					local currentParticle =  { 
						position = emitterPos + emitter.gunshotProperties.mainAxis * emitter.gunshotProperties.offset, 
						startVelocity = {x = 0.0, y = 0.0, z = 0.0} 
					}

					local randNumLen = emitter.gunshotProperties.length * c_random(100) / 100.0
					local mainAxisPos = emitter.gunshotProperties.mainAxis * randNumLen

					local randNumAngle = math.pi * 2.0 * c_random(100) / 100.0

					local sin = math.sin(randNumAngle)
					local cos = math.cos(randNumAngle)
					local currRadius = emitter.gunshotProperties.radius * c_random(10) * c_random(10) * c_random(10) / 1000.0

					if (randNumLen <= emitter.gunshotProperties.widestPoint * emitter.gunshotProperties.length) then
						--currRadius = currRadius * (1.0 - 1.0 / math.exp(randNumLen/(emitter.widestPoint * emitter.length)))
						currRadius = currRadius * 
							math.sin(math.pi / 2.0 * randNumLen / (emitter.gunshotProperties.length * emitter.gunshotProperties.widestPoint))
					else
						currRadius = currRadius * math.sin(
							math.pi / 2.0 
							+ math.pi / 2.0 * 
								(randNumLen - emitter.gunshotProperties.length * emitter.gunshotProperties.widestPoint) / 
								(emitter.gunshotProperties.length * (1.0 - emitter.gunshotProperties.widestPoint))
						)
					end

					local rightHandCoord = currRadius * cos --/ c_random(100);
					local rightAxisPos = rightAxis * rightHandCoord
					local upCoord = currRadius * sin --/ c_random(100);
					local upAxisPos = upCoord * emitter.gunshotProperties.upAxis

					currentParticle.startVelocity = (mainAxisPos + rightAxisPos + upAxisPos)
						* emitter.gunshotProperties.length / emitter.initialComponents.particleEmitter.maxLifeTime

					currentParticle.endVelocity = currentParticle.startVelocity

					table.insert(result, currentParticle)
				end

				return result
			end
		}
	},
	onInit = function(self) end
}

-- брызги

prototypes["spray"] = prototypes["base"]:derive{
	name = "spray",
	sprayProperties = {
		length = 2.0,
		offset = 0.0,
		radius = 0.5,
		mainAxis = { x = 0.0, y = 0.0, z = 1.0 },
		upAxis = { x = 1.0, y = 0.0, z = 0.0 }
	},
	initialComponents = {
		position = {x = 7.0, y = 4.0, z = 0.0},
		particleEmitter = {
			maxParticlesCount = 200,
			maxLifeTime = 0.4,
			startParticleSize = 2.0,
			endParticleSize = 1.0,
			startVelocityFactor = 20.0,
			endVelocityFactor = 0.0,
			startColor = { r = 0.2, g = 0.2, b = 0.2, a = 1.0},
			endColor = { r = 0.2, g = 0.2, b = 0.2, a = 0.5},
			blur = false,
			original = true,
			particleRespawnFunc = function(emitter, particleCount) 
			
				local rightAxis = c_crossProduct(emitter.sprayProperties.mainAxis, emitter.sprayProperties.upAxis)
				local emitterPos = c_getPos(emitter)
				local result = {}
				
				for i = 1, particleCount do
					local currentParticle = { 
						position = {
							x = emitterPos.x + emitter.sprayProperties.mainAxis.x * emitter.sprayProperties.offset, 
							y = emitterPos.y + emitter.sprayProperties.mainAxis.y * emitter.sprayProperties.offset, 
							z = emitterPos.z + emitter.sprayProperties.mainAxis.z * emitter.sprayProperties.offset
						}, 
						startVelocity = {x = 0.0, y = 0.0, z = 0.0},
						endVelocity = {x = 0.0, y = 0.0, z = 0.0}
					}

					local randNumLen = emitter.sprayProperties.length * c_random(100) / 100.0
					local mainAxisPos = { 
						x = emitter.sprayProperties.mainAxis.x * randNumLen, 
						y = emitter.sprayProperties.mainAxis.y * randNumLen, 
						z = emitter.sprayProperties.mainAxis.z * randNumLen 
					}

					local randNumAngle = math.pi * 2.0 * c_random(100) / 100.0

					local sin = math.sin(randNumAngle)
					local cos = math.cos(randNumAngle)
					local currRadius = (emitter.sprayProperties.radius * c_random(100) / 100.0)
					 					* math.exp((randNumLen)/emitter.sprayProperties.length - 1.0)

					local rightHandCoord = currRadius * cos --/ c_random(100);
					local rightAxisPos = { x = rightHandCoord * rightAxis.x, y = rightHandCoord * rightAxis.y, z = rightHandCoord * rightAxis.z}
					local upCoord = currRadius * sin --/ c_random(100);
					local upAxisPos = { 
						x = upCoord * emitter.sprayProperties.upAxis.x, 
						y = upCoord * emitter.sprayProperties.upAxis.y, 
						z = upCoord * emitter.sprayProperties.upAxis.z 
					}

					currentParticle.startVelocity.x = (mainAxisPos.x + rightAxisPos.x + upAxisPos.x) * 
						emitter.sprayProperties.length / emitter.initialComponents.particleEmitter.maxLifeTime

					currentParticle.startVelocity.y = (mainAxisPos.y + rightAxisPos.y + upAxisPos.y) * 
						emitter.sprayProperties.length / emitter.initialComponents.particleEmitter.maxLifeTime

					currentParticle.startVelocity.z = (mainAxisPos.z + rightAxisPos.z + upAxisPos.z) * 
						emitter.sprayProperties.length / emitter.initialComponents.particleEmitter.maxLifeTime

					--[[currentParticle.endVelocity = createVector{
						x = currentParticle.startVelocity.x,
						y = currentParticle.startVelocity.y,
						z = -currentParticle.startVelocity.z
					}]]
					currentParticle.endVelocity = createVector{
						x = 0.0,
						y = 0.0,
						z = -1.0
					}

					table.insert(result, currentParticle)
				end

				return result
			end
		}
	},
	onInit = function(self) end
}