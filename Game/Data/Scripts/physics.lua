prototypes["physicalObject"] = prototypes["base"]:derive{
	name = "physicalObject",
    initialComponents = {
    	position = {},
    	collideable = {
    		mass = 1.0,
    		shape = {
    			type = "box",
    			length = 1.0,
    			height = 1.0,
    			width = 1.0
    		}
    	}
    }
}

function createBox(xInitial, yInitial, zInitial, initialLen, initialWidth, initialHeight, initialMass)
	local box = c_createEntity(prototypes["physicalObject"]:derive{
		name = "box",
		initialComponents = {
			position = {x = xInitial, y = yInitial, z = zInitial},
			collideable = {
				shape = {
					type = "box", 
					length = initialLen, 
					width = initialWidth, 
					height = initialHeight
				},
				mass = initialMass,
				debug = true
			}
		}
	})

	return box
end