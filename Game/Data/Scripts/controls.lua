onMouseMove = function(args)
	-- если к курсору привязан источник освещения
	if (mouseTorch) then
		c_modifyComponent(mouseTorch, "position", {x = args.mapCoords.x, y = args.mapCoords.y})
	end
end

onMouseClick = function(args)

	-- если зажата клавиша R, движемся в направлении курсора
	if (args.originalPass and player.runByDir) then
		c_findPath(dummy, args.mapCoords)		
	elseif (args.originalPass and player.currentWeapon ~= nil and player.aiming) then
		if (args.leftButtonPressed) then
			player.currentWeapon:attack(player.orientPoint)
		elseif (args.middleButtonPressed) then
			local entityPointed = c_getEntityByScreenCoords({x = args.xScreen, y = args.yScreen})

			if (entityPointed ~= nil and entityPointed ~= player and c_hasSkeleton(entityPointed)) then
				player.entityLocked = entityPointed
			else
				player.entityLocked = nil
			end
		end
	elseif (args.originalPass and player.currentWeapon ~= nil and not player.aiming) then
		player:enableAiming(true)
	end

end

debugToggleAlignAngles = false
debugToggleAlignOrigin = false
debugToggleLooseBodies = false
debugOffsetWeapon = false
zVary = 0.0
playerToggleRunNextVal = false
mainConstraintsEnabled = false

sloMo = { nextValue = false, currentValue = false }

controls = {
	["F1"] = function(repeated, originalPass) if (originalPass and not repeated) then c_debugEnableGravity() end end,
	["F2"] = function(repeated, originalPass) if (originalPass and not repeated) then c_debugShowLevelGeometry() end end,
	["F3"] = function(repeated, originalPass) if (originalPass and not repeated) then c_debugShowAABBs() end end,
	["F4"] = function(repeated, originalPass) if (originalPass and not repeated) then c_debugShowCoords() end end,
	["F5"] = function(repeated, originalPass) if (originalPass and not repeated) then c_debugDrawMeshes() end end,
	["F6"] = function(repeated, originalPass) if (originalPass and not repeated) then c_debugEnableConstraints() end end,
	["F7"] = function(repeated, originalPass) if (originalPass and not repeated) then 
		debugToggleAlignAngles = not debugToggleAlignAngles 
		
		c_consoleLog("debugToggleAlignAngles = " .. tostring(debugToggleAlignAngles))
	end end,
	["F8"] = function(repeated, originalPass) if (originalPass and not repeated) then 
		debugToggleAlignOrigin = not debugToggleAlignOrigin 
		
		c_consoleLog("debugToggleAlignOrigin = " .. tostring(debugToggleAlignOrigin))
	end end,
	--[[["F9"] = function(repeated, originalPass) if (originalPass and not repeated) then
		debugToggleLooseBodies = not debugToggleLooseBodies
		c_looseBoneBodies(player, debugToggleLooseBodies)
		c_consoleLog("debugToggleLooseBodies = " .. tostring(debugToggleLooseBodies))
	end end,]]
	["F10"] = function(repeated, originalPass) if (originalPass and not repeated) then
		player:enableAiming(not player.aiming)

		c_enableLightPoint(mouseTorch, not c_isLightPointEnabled(mouseTorch))
	end end,
	["Num1"] = function(repeated, originalPass) 
		if (originalPass and not repeated) then
			
			if (player.currentWeapon ~= nil) then
				player.currentWeapon:unassign()
			end
			bat:assign(player)
		end
	end,
	["Num2"] = function(repeated, originalPass) 
		if (originalPass and not repeated) then
			if (player.currentWeapon ~= nil) then
				player.currentWeapon:unassign()
			end
			pistol:assign(player)
		end
	end,
	["Num3"] = function(repeated, originalPass) 
		if (originalPass and not repeated) then
			if (player.currentWeapon ~= nil) then
				player.currentWeapon:unassign()
			end
			shotty:assign(player)
		end
	end,
	["P"] = function(repeated, originalPass)
		if (originalPass and not repeated) then
			if (player.currentWeapon ~= nil) then
				player.currentWeapon:unassign()
			end
		end
	end,

	["Numpad7"] = function(repeated, originalPass)
		if (alignableEntity ~= nil and originalPass and not repeated and debugToggleAlignAngles) then
			local newFrame = c_debugTransformEntity(alignableEntity, {x = 0.0, y = 0.0, z = -0.1}, {x = 0.0, y = 0.0, z = 0.0})

			c_consoleLog("angles z = " .. newFrame.eulerAngles.z .. " y = " .. newFrame.eulerAngles.y .. " x = " .. newFrame.eulerAngles.x)
		elseif (alignableEntity ~= nil and originalPass and not repeated and debugToggleAlignOrigin) then
			local newFrame = c_debugTransformEntity(alignableEntity, {x = 0.0, y = 0.0, z = 0.0}, {x = -0.03, y = 0.0, z = 0.0})

			c_consoleLog("origin z = " .. newFrame.origin.z .. " y = " .. newFrame.origin.y .. " x = " .. newFrame.origin.x)
		end

		if (debugOffsetWeapon) then
			zVary = zVary + 0.01
		end
	end,
	["Numpad8"] = function(repeated, originalPass)
		if (alignableEntity ~= nil and originalPass and not repeated and debugToggleAlignAngles) then
			local newFrame = c_debugTransformEntity(alignableEntity, {x = 0.0, y = 0.0, z = 0.1}, {x = 0.0, y = 0.0, z = 0.0})
			c_consoleLog("angles z = " .. newFrame.eulerAngles.z .. " y = " .. newFrame.eulerAngles.y .. " x = " .. newFrame.eulerAngles.x)
		elseif (alignableEntity ~= nil and originalPass and not repeated and debugToggleAlignOrigin) then
			local newFrame = c_debugTransformEntity(alignableEntity, {x = 0.0, y = 0.0, z = 0.0}, {x = 0.03, y = 0.0, z = 0.0})

			c_consoleLog("origin z = " .. newFrame.origin.z .. " y = " .. newFrame.origin.y .. " x = " .. newFrame.origin.x)
		end

		if (debugOffsetWeapon) then
			zVary = zVary - 0.01
		end
	end,
	["Numpad4"] = function(repeated, originalPass) 
		if (alignableEntity ~= nil and originalPass and not repeated and debugToggleAlignAngles) then
			local newFrame = c_debugTransformEntity(alignableEntity, {x = 0.0, y = -0.1, z = 0.0}, {x = 0.0, y = 0.0, z = 0.0})
			c_consoleLog("angles z = " .. newFrame.eulerAngles.z .. " y = " .. newFrame.eulerAngles.y .. " x = " .. newFrame.eulerAngles.x)
		elseif (alignableEntity ~= nil and originalPass and not repeated and debugToggleAlignOrigin) then
			local newFrame = c_debugTransformEntity(alignableEntity, {x = 0.0, y = 0.0, z = 0.0}, {x = 0.0, y = -0.03, z = 0.0})

			c_consoleLog("origin z = " .. newFrame.origin.z .. " y = " .. newFrame.origin.y .. " x = " .. newFrame.origin.x)
		end
	end,
	["Numpad5"] = function(repeated, originalPass) 
		if (alignableEntity ~= nil and originalPass and not repeated and debugToggleAlignAngles) then
			local newFrame = c_debugTransformEntity(alignableEntity, {x = 0.0, y = 0.1, z = 0.0}, {x = 0.0, y = 0.0, z = 0.0})
			c_consoleLog("angles z = " .. newFrame.eulerAngles.z .. " y = " .. newFrame.eulerAngles.y .. " x = " .. newFrame.eulerAngles.x)
		elseif (alignableEntity ~= nil and originalPass and not repeated and debugToggleAlignOrigin) then
			local newFrame = c_debugTransformEntity(alignableEntity, {x = 0.0, y = 0.0, z = 0.0}, {x = 0.0, y = 0.03, z = 0.0})

			c_consoleLog("origin z = " .. newFrame.origin.z .. " y = " .. newFrame.origin.y .. " x = " .. newFrame.origin.x)
		end
	end,
	["Numpad1"] = function(repeated, originalPass) 
		if (alignableEntity ~= nil and originalPass and not repeated and debugToggleAlignAngles) then
			local newFrame = c_debugTransformEntity(alignableEntity, {x = -0.1, y = 0.0, z = 0.0}, {x = 0.0, y = 0.0, z = 0.0})
			c_consoleLog("angles z = " .. newFrame.eulerAngles.z .. " y = " .. newFrame.eulerAngles.y .. " x = " .. newFrame.eulerAngles.x)
		elseif (alignableEntity ~= nil and originalPass and not repeated and debugToggleAlignOrigin) then
			local newFrame = c_debugTransformEntity(alignableEntity, {x = 0.0, y = 0.0, z = 0.0}, {x = 0.0, y = 0.0, z = -0.03})

			c_consoleLog("origin z = " .. newFrame.origin.z .. " y = " .. newFrame.origin.y .. " x = " .. newFrame.origin.x)
		end
	end,
	["Numpad2"] = function(repeated, originalPass) 
		if (alignableEntity ~= nil and originalPass and not repeated and debugToggleAlignAngles) then
			local newFrame = c_debugTransformEntity(alignableEntity, {x = 0.1, y = 0.0, z = 0.0}, {x = 0.0, y = 0.0, z = 0.0})
			c_consoleLog("angles z = " .. newFrame.eulerAngles.z .. " y = " .. newFrame.eulerAngles.y .. " x = " .. newFrame.eulerAngles.x)
		elseif (alignableEntity ~= nil and originalPass and not repeated and debugToggleAlignOrigin) then
			local newFrame = c_debugTransformEntity(alignableEntity, {x = 0.0, y = 0.0, z = 0.0}, {x = 0.0, y = 0.0, z = 0.03})

			c_consoleLog("origin z = " .. newFrame.origin.z .. " y = " .. newFrame.origin.y .. " x = " .. newFrame.origin.x)
		end
	end,

	["W"] = function() player.movementDir.x = player.movementDir.x - 0.5 player.movementDir.y = player.movementDir.y - 0.5 end,
	["S"] = function() player.movementDir.x = player.movementDir.x + 0.5 player.movementDir.y = player.movementDir.y + 0.5 end,
	["A"] = function() player.movementDir.x = player.movementDir.x - 0.5 player.movementDir.y = player.movementDir.y + 0.5 end,
	["D"] = function() player.movementDir.x = player.movementDir.x + 0.5 player.movementDir.y = player.movementDir.y - 0.5 end,
	["I"] = function(repeated, originalPass) 
		if originalPass and not repeated then
			c_showWidget("Inventory", not state.inventoryShown) 
			state.inventoryShown = not state.inventoryShown
			player.numb = state.inventoryShown
			c_playAnimation(player, "idle", 1.0, true, {"Armature"})
		end
	end,
	["U"] = function() 
		c_modifyComponent(mouseTorch, "position", {z = c_getComponent(mouseTorch, "position").z + 0.1})
	end,
	["J"] = function() 
		c_modifyComponent(mouseTorch, "position", {z = c_getComponent(mouseTorch, "position").z - 0.1})
	end,
	["F"] = function(repeated, originalPass)

		if (originalPass) then
			state.fighting = not state.fighting

			-- toggle fightmode
			if (state.fighting) then
				player.animations.idle = "idleBat"
				player.animations.moveForward = "moveForwardWithBat"
				player.animations.moveBackwards = "moveBackwardsWithBat"
			else
				player.animations.idle = "idle"
				player.animations.moveForward = "moveForward"
				player.animations.moveBackwards = "moveBackwards"
			end
		end
	end,
	["LShift"] = function(repeated, originalPass)
		playerToggleRunNextVal = true
	end,
	["R"] = function(repeated, originalPass)
		player.runByDir = true
	end,
	["Space"] = function(repeated, originalPass)
		if (not repeated and originalPass and not player.busy and not player.unmoveable and lengthVec(c_getTargetVelocity(player)) > 0.0) then
			player:dodge(nil)
		end
	end,
	["Q"] = function(repeated, originalPass) -- замедление времени
		if (not repeated and originalPass) then
			sloMo.nextValue = not sloMo.currentValue
		end
	end
	--[[["Num1"] = function(repeated, originalPass)
		if (originalPass) then
			c_consoleLog("weapon assigning")
			c_addComponent(player, "weapon", weaponPrototypes.pistol)
		end
	end]]
}

onKeyPressed = function(key, repeated, originalPass)
	if controls[key] ~= nil then
		controls[key](repeated, originalPass)
	end

	if ((player.movementDir.x ~= 0.0 or player.movementDir.y ~= 0.0) and not player.unmoveable) then
		c_move(player, player.movementDir)
	end
end

-- до того как системы обновятся
onSceneUpdate = function(delta)
	-- бег

	if (player.toggleRun ~= playerToggleRunNextVal) then
		player:enableRun(playerToggleRunNextVal)
		
		local vel = createVector(c_getTargetVelocity(player))
		if (lengthVec(vel) > 0.0) then
			player:onStartMove()
		end
	end

	playerToggleRunNextVal = false

	local mousePos = c_getMousePos();

	-- передвигаем камеру в соответствии с координатами мыши, если инвентарь не показан на экране
	if (not state.inventoryShown) then
		local playerPos = c_getComponent(player, "position")
		c_centerCameraAt(playerPos.x + (mousePos.mapCoords.x - playerPos.x) / 2.0, playerPos.y + (mousePos.mapCoords.y - playerPos.y) / 2.0)
	end

	-- замедление времени
	if (sloMo.nextValue ~= sloMo.currentValue) then
		sloMo.currentValue = sloMo.nextValue

		c_setTimeFactor(sloMo.currentValue and 0.25 or 0.5)
	end

end

-- после того как системы обновятся
onSceneUpdated = function(delta)

	local mousePos = c_getMousePos();

	if (not player.unmoveable) then
		player.movementDir = createVector({x = 0.0, y = 0.0})
		c_stop(player)
	end

	player.runByDir = false

	if (player.entityLocked == nil) then
		c_setCursorInvisible(false)
		-- меняем курсор на прицел, если надо
		local entityPointed = c_getEntityByScreenCoords({x = mousePos.xScreen, y = mousePos.yScreen})
		if (entityPointed ~= nil) then
			if (player.currentWeapon ~= nil and player.aiming and c_hasSkeleton(entityPointed) and player ~= entityPointed) then
				c_setAimCursor(1.0)
				player.orientPoint = c_getBoneBodyPos(entityPointed, "chest")
			else
				c_setDefaultCursor(true)
			end
		else
			c_setDefaultCursor(false)

			-- смотреть в сторону курсора
			player.orientPoint = c_getSceneCoordsByScreenCoords({x = mousePos.xScreen, y = mousePos.yScreen})
		end
	else
		player.orientPoint = c_getBoneBodyPos(player.entityLocked, "chest")

		c_setCursorInvisible(true)
		c_lockAimCursor(player.orientPoint)
	end

	player:orientTo(player.orientPoint)

end