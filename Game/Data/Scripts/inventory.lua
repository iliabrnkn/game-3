--inventory

print("inventory.lua included")
activeItemHandlers = {
	onWeapon0Changed = function(itemTable) 
		c_removeComponent(player, "weapon")
		c_addComponent(player, "weapon", itemTable)

		c_consoleLog("weapon0 changed")
	end,

	onWeapon1Changed = function(itemTable) 
		c_removeComponent(player, "weapon")
		c_addComponent(player, "weapon", itemTable)

		c_consoleLog("weapon1 changed")
	end
}