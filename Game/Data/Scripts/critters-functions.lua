-- здесь будут хайлевельные функции спавна персонажей, в том числе и гг

function createPlayer(x, y, z)

	local player = c_createEntity(prototypes["actor"]:derive{
		name = "player",
		initialComponents = {
			position = {x = x, y = y, z = z},
			direction = {y = 1.0, x = 0.0}
		},
		movementDir = {x = 0.0, y = 0.0},
		orientPoint = createVector(),
		onCollide = function(self, entityTable)
			--[[c_modifyComponent(self, "moveable", {
				rawMovementDir = {x = 0.0, y = 0.0}, 
				speedScalarIncrement = 0.0,
				speedScalar = 0.0
			})]]
		end
	})

	return player
end

function createDummy(x, y, z)
	local dummy = c_createEntity(prototypes["actor"]:derive{
		name = "dummy",
		initialComponents = {
			position = {x = x, y = y, z = z},
			direction = {y = 1.0, x = 0.0}
		},
		onCollide = function(self, entityTable)
			--[[c_modifyComponent(self, "moveable", {
				rawMovementDir = {x = 0.0, y = 0.0}, 
				speedScalarIncrement = 0.0,
				speedScalar = 0.0
			})]]--
		end
	})

	return dummy
end