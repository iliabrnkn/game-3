prototypes["meleeWeapon"] = prototypes["base"]:derive{
  name = "meleeWeapon",
  attacking = false,
  weaponConstraint = {
    weapon_l = { 
      linearLowerLimits = {x = 0.0, y = 0.0, z = 0.0},
      linearUpperLimits = {x = 0.0, y = 0.0, z = 0.0},
      angularLowerLimits= {x = 0.0, y = 0.0, z = 0.0},
      angularUpperLimits= {x = 0.0, y = 0.0, z = 0.0}
    },
    weapon_r = { 
      linearLowerLimits = {x = 0.0, y = 0.0, z = 0.0},
      linearUpperLimits = {x = 0.0, y = 0.0, z = 0.0},
      angularLowerLimits= {x = 0.0, y = 0.0, z = 0.0},
      angularUpperLimits= {x = 0.0, y = 0.0, z = 0.0}
    }
  },
  initialComponents = {
    position = {},
    strikeable = {}
  },
  assign = function(self, ownerEntity)
    self.owner = ownerEntity
    ownerEntity.currentWeapon = self
  end,
  unassign = function(self)
    self.owner.aiming = false
    self.owner.currentWeapon = nil 
    self.owner:playIdleAnimation()
    self.owner = nil
  end,
  orientTo = function(self, mapCoords)
    c_orientTo(self.owner, mapCoords)
  end,
  dodge = function(self, dirInitial)
    self.owner.unmoveable = true
    
    local dir = normalizeVec((dirInitial == nil) and c_getTargetVelocity(self.owner) or dirInitial)
    local angle = c_findAngle(dir, c_getComponent(self.owner, "direction"))
    c_debugDrawVector(c_getPos(self.owner), dir, {r = 1.0})
    c_consoleLog("angle = " .. tostring(angle))
    
    if (angle <= math.pi * 3.0 / 4.0) then
      c_playAnimationOnce(self.owner, "dodge_forward", 1.0, {"Armature"}, {"zero_joint_arm_l", "zero_joint_arm_r"})
    else
      c_playAnimationOnce(self.owner, "dodge_backward", 1.0, {"Armature"}, {"zero_joint_arm_l", "zero_joint_arm_r"})
    end

    c_move(self.owner, dir * 3.0)

    c_setTimer(self.owner, 0.45, function(t)  
      c_stop(t)

      c_setTimer(t, 0.05, function(ent) 
        self.owner.unmoveable = false 
      end)

    end)
  end,

  -- EVENTS
  onHit = function(self, args)
    local dir = normalizeVec(c_getPos(args.targetEntity) - c_getPos(args.atacker))
    c_looseBoneBodies(args.targetEntity, 2.5)
    local pushDir = normalizeVec(createVector(args.hitNormalWorld):xy())
    local finalDir = slerpVec(dir, pushDir, 0.5)
    c_applyCentralImpulse(c_getBoneBodyWorldArrayIndex(args.targetEntity, "head"), finalDir * 500.0)
    c_applyCentralImpulse(c_getBoneBodyWorldArrayIndex(args.targetEntity, "chest"), finalDir * 500.0)
    c_setActiveStrikeable(self, false)
  end,

  onMiss = function(self, args)
    c_setActiveStrikeable(self, false)
  end
}

dofile(c_getScriptsFolder() .. "/weapons/baseball-bat.lua")