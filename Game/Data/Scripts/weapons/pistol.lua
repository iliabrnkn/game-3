prototypes["pistol"] = prototypes["gun"]:derive{
  name = "pistol",
  weaponProperties = {
    spreadAngle = math.pi * 0.01,
    maxDistance = 20.0
  },
  initialComponents = {
    position = {},
    meshBatch = {
      meshes = {
        "pistol"
      }
    },
  },

  -- METHODS
  playWalkAnimation = function(self)

    local restrictedNodes = {}
    local allowedNodes = {}

    if (self.owner.aiming) then
      c_playAnimation(self.owner, "pistol_aim", 1.0, true, {"spine_01"})
      table.insert(restrictedNodes, "spine_01")
      table.insert(allowedNodes, "pelvis")
    else
      c_playAnimation(self.owner, "pistol_idle", 1.0, true, {"spine_01"})
      table.insert(allowedNodes, "Armature")
      table.insert(restrictedNodes, "spine_01")
    end 

    if (c_getComponent(self.owner, "moveable").walkForward) then --ходьба вперед
      c_playAnimation(self.owner, self.owner.toggleRun and "run_forward" or "walk_forward",
        1.0, false, allowedNodes, restrictedNodes)
    else -- ходьба назад
      c_playAnimation(self.owner, self.owner.toggleRun and "run_backward" or "walk_backward", 
        1.0, false, allowedNodes, restrictedNodes)
    end
  
  end,

  playIdleAnimation = function(self)
    if (not self.owner.aiming) then
      c_playAnimation(self.owner, "pistol_idle", 1.0 , true, {"Armature"})
      c_playAnimation(self.owner, "idle", 1.0 , false, {"spine_01"}, {"zero_joint_arm_l", "zero_joint_arm_r"})
    else
      c_playAnimation(self.owner, "pistol_aim", 1.0 , true, {"Armature"})
    end
  end,

  playRecoilAnimation = function(self)
    --c_playAnimationOnce(self.owner, "pistol_shot", 0.5)
  end,

  attack = function(self, mapPoint)
    if (not self.owner.busy) then
      local shotDir = normalizeVec(createVector(mapPoint) - c_getPos(self))
      local randomRotVert = 
      {
        axis = {z = 1.0},
        angle = (c_random(100) % 2 == 0 and 1 or -1) * self.weaponProperties.spreadAngle / 2.0 * c_random(100) / 100.0
      }
      local randomRotHor = 
      {
        axis = crossProduct(normalizeVec(shotDir), createVector({z = 1.0})),
        angle = (c_random(100) % 2 == 0 and 1 or -1) * self.weaponProperties.spreadAngle / 2.0 * c_random(100) / 100.0
      }
      shotDir = c_rotateVec(c_rotateVec(shotDir, randomRotVert), randomRotHor)
      c_drawTrace(self, shotDir * self.weaponProperties.maxDistance) -- TODO: переименовать функцию

      self:fire()
    end
  end,

  orientTo = function(self, mapCoords)
    c_orientTo(self.owner, mapCoords)

    -- поворачиваем части тела в направлении указанной точки
    
    if (self.owner.aiming) then
      -- поворачиваем части тела в направлении указанной точки
      c_orientBoneBodyToPoint(self.owner, "head", 1, mapCoords)
      --c_orientBoneBodyToPoint(owner, "neck", 1, mapCoords)
      --c_orientBoneBodyToPoint(owner, "spine_02", 1, {x = mapCoords.x, y = mapCoords.y, z = c_getBoneBodyPos(owner, "spine_02").z})
      c_orientBoneBodyToPoint(self.owner, "weapon_l", 1, mapCoords)
      --c_orientBoneBodyToPoint(self.owner, "weapon_r", 1, mapCoords)

      local headPos = createVector(c_getBoneBodyPos(self.owner, "head"))
      local headToGroundNormalized = normalizeVec(createVector(mapCoords) - headPos)
      local pistolNewPos = headPos + headToGroundNormalized * self.headToPistolDistance
      
      c_setBoneBodyPos(self.owner, "weapon_l", pistolNewPos)
      c_setBoneBodyPos(self.owner, "weapon_r", pistolNewPos)

      -- поворачиваем вспышку в нужное место
      local pistolCurrPos = createVector(c_getPos(self))

      self.gunshotProperties.mainAxis = normalizeVec(createVector(mapCoords) - pistolCurrPos)
      self.gunshotProperties.upAxis = normalizeVec(crossProduct(self.gunshotProperties.mainAxis, createVector(0.0, 0.0, 1.0)))

    --
    end
  end,

  assign = function(self, owner)
    c_consoleLog("weapon_l constraints num" .. c_getNumConstraints(owner, "weapon_l"))
    c_consoleLog("hand_l constraints num" .. c_getNumConstraints(owner, "weapon_l"))

    owner.currentWeapon = self
    self.owner = owner
    c_setWeaponBody(self.owner, self, "weapon_l")
    c_setBoneOrientationRatio(self.owner, 1.0, 0.0)

    -- задаем телам второй набор констрэйнтов для прицеливания
    
    -- вырубаем у рук дефолтные констрэйнты тишки
    c_enableBoneBodyConstraint(self.owner, "weapon_l", 0, false)
    c_enableBoneBodyConstraint(self.owner, "weapon_r", 0, false)
    c_enableBoneBodyConstraint(self.owner, "hand_r", 0, false)
    c_enableBoneBodyConstraint(self.owner, "hand_l", 0, false)
    c_enableBoneBodyConstraint(self.owner, "forearm_l", 0, false)
    c_enableBoneBodyConstraint(self.owner, "forearm_r", 0, false)
    c_enableBoneBodyConstraint(self.owner, "shoulder_l", 0, false)
    c_enableBoneBodyConstraint(self.owner, "shoulder_r", 0, false)

    c_addBoneBodyConstraint(self.owner, "hand_l", "weapon_l", {
      frameBBasis = {
        0.316403, 0.035913, 0.947945, 
        0.750914, 0.601144, -0.273413, 
        -0.579670, 0.798334, 0.163236
      }
    })

    c_addBoneBodyConstraint(self.owner, "hand_r", "weapon_r", {
      frameBBasis = {
        0.047808, 0.997569, 0.050708, 
        0.205389, -0.059499, 0.976870, 
        0.977512, -0.036288, -0.207734
      }
    })

    c_addBoneBodyConstraint(self.owner, "weapon_r", "weapon_l", {
      frameBBasis = {
        -0.164184, 0.978329, -0.126155, 
        -0.167855, -0.153734, -0.973751, 
        -0.972043, -0.138699, 0.189458
      }
    })
    c_addBoneBodyConstraint(self.owner, "forearm_l", "hand_l", {
      frameBBasis = {
        0.935353, -0.172880, 0.308590, 
        -0.021812, 0.842571, 0.538143, 
        -0.353044, -0.510085, 0.784330
      }
    })
    c_addBoneBodyConstraint(self.owner, "forearm_r", "hand_r", {
      frameBBasis = {
        0.892040, 0.259968, 0.369704, 
        -0.292971, 0.955480, 0.035022, 
        -0.344140, -0.139554, 0.928489
      }
    })
    c_addBoneBodyConstraint(self.owner, "shoulder_l", "forearm_l", {
      frameBBasis = {
        0.319988, -0.941589, 0.104969, 
        0.941849, 0.304149, -0.142873, 
        0.102601, 0.144583, 0.984159
      }
    })
    c_addBoneBodyConstraint(self.owner, "shoulder_r", "forearm_r", {
      frameBBasis = {
        0.765795, -0.642000, 0.037331, 
        0.642179, 0.760345, -0.097375, 
        0.034131, 0.098542, 0.994547
      }
    })

    c_addBoneBodyConstraint(self.owner, "clavicle_l", "shoulder_l", {
      frameBBasis = {
        0.672256, 0.326852, 0.664259, 
        0.541149, 0.395350, -0.742197, 
        -0.505203, 0.858409, 0.088901
      }
    })

    c_addBoneBodyConstraint(self.owner, "clavicle_r", "shoulder_r", {
      frameBBasis = {
        0.202642, -0.191777, 0.960290, 
        0.862049, 0.500142, -0.082029, 
        -0.464551, 0.844440, 0.266671
      }
    })

    --[[c_addBoneBodyConstraint(self.owner, "zero_joint_arm_l", "clavicle_l", {
      writeToFile = true
    })
    c_addBoneBodyConstraint(self.owner, "zero_joint_arm_r", "clavicle_r", {
      writeToFile = true
    })]]

    c_setTimer(
      self.owner, 0.3,
      function(t)

        local headPos = c_getBoneBodyPos(player, "head")
        local pistolPos = c_getBoneBodyPos(player, "weapon_l")
        local lHandPos = c_getBoneBodyPos(player, "hand_l")
        local rHandPos = c_getBoneBodyPos(player, "hand_r")
        pistol.headToPistolDistance = 0.85 * c_getLength({x = pistolPos.x - headPos.x, y = pistolPos.y - headPos.y, z = pistolPos.z - headPos.z})

      end
    )

    self.owner:playIdleAnimation()
    --self.owner:enableAiming(false)

  end,
  unassign = function(self)

    c_setBoneBodyPos(self.owner, "weapon_r", createVector())
    c_setBoneBodyPos(self.owner, "weapon_l", createVector())
    c_orientBoneBodyToPoint(self.owner, "weapon_l", 1, false)

    c_removeBoneBodyConstraint(self.owner, "weapon_l", 0)
    c_removeBoneBodyConstraint(self.owner, "weapon_l", 1)
    c_removeBoneBodyConstraint(self.owner, "weapon_r", 1)
    c_removeBoneBodyConstraint(self.owner, "hand_r", 1)
    c_removeBoneBodyConstraint(self.owner, "hand_l", 1)
    c_removeBoneBodyConstraint(self.owner, "forearm_l", 1)
    c_removeBoneBodyConstraint(self.owner, "forearm_r", 1)
    c_removeBoneBodyConstraint(self.owner, "shoulder_l", 1)
    c_removeBoneBodyConstraint(self.owner, "shoulder_r", 1)

    c_enableBoneBodyConstraint(self.owner, "weapon_l", 0, true)
    c_enableBoneBodyConstraint(self.owner, "weapon_r", 0, true)
    c_enableBoneBodyConstraint(self.owner, "hand_r", 0, true)
    c_enableBoneBodyConstraint(self.owner, "hand_l", 0, true)
    c_enableBoneBodyConstraint(self.owner, "forearm_l", 0, true)
    c_enableBoneBodyConstraint(self.owner, "forearm_r", 0, true)
    c_enableBoneBodyConstraint(self.owner, "shoulder_l", 0, true)
    c_enableBoneBodyConstraint(self.owner, "shoulder_r", 0, true)

    self.owner.aiming = false
    self.owner.currentWeapon = nil 
    self.owner:playIdleAnimation()
    self.owner = nil
  end

}