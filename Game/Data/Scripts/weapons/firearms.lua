-- прототип дальнобойного оружия
prototypes["gun"] = prototypes["gunshot"]:derive{
  name = "gun",

  -- игровая логика

  minDamage = 10,
  maxDamage = 10,

  ----

  recoilFactor = 100,
  gunshotProperties = {
    offset = 0.2
  },
  weaponProperties = {
    spreadAngle = math.pi * 0.01,
    maxDistance = 20.0
  },
  weaponConstraint = {
    weapon_l = { 
      linearLowerLimits = {x = 0.0, y = 0.0, z = 0.0},
      linearUpperLimits = {x = 0.0, y = 0.0, z = 0.0},
      angularLowerLimits= {x = 0.0, y = 0.0, z = 0.0},
      angularUpperLimits= {x = 0.0, y = 0.0, z = 0.0}
    },
    weapon_r = { 
      linearLowerLimits = {x = 0.0, y = 0.0, z = 0.0},
      linearUpperLimits = {x = 0.0, y = 0.0, z = 0.0},
      angularLowerLimits= {x = 0.0, y = 0.0, z = 0.0},
      angularUpperLimits= {x = 0.0, y = 0.0, z = 0.0}
    }
  },
  initialComponents = {
    position = {},
    collideable = {
      mass = 10.0,
      shape = {
        type = "box",
        width = 0.2,
        height = 0.2,
        length = 0.2
      }
    },
    lightPoint = {color = {r = 1.0, g = 1.0, b = 1.0}, radius = 10.0, dynamic = true, active = false},
    meshBatch = {
      material = {r = 0.1, g = 0.1, b = 0.1},
      scale = 0.023,
      initialTransform = {
        basis = {
          1.0, 0.0, 0.0,
          0.0, 0.0, 1.0,
          0.0, 1.0, 0.0
        },
        origin = {x = 0.0, y = 0.0, z = 0.0}
      }
    },
    particleEmitter = {active = false},
    tracer = {}
  },
  owner = nil,
  -- методы

  generateRecoil = function(self) 
    local recoilRadius = 1.0
    local recoilAngle = math.pi * 2.0 * c_random(100) / 100.0
    local sin = recoilRadius * math.sin(recoilAngle)
    local cos = recoilRadius * math.cos(recoilAngle)
    local rightAxis = c_crossProduct(self.gunshotProperties.upAxis, self.gunshotProperties.mainAxis)
    local recoilOffset = self.gunshotProperties.upAxis * sin + rightAxis * cos
    local recoilAxis = c_normalizeVec(recoilOffset - self.gunshotProperties.mainAxis)

    return recoilAxis * self.recoilFactor
  end,

  assign = function(self, ownerEntity)
    self.owner = ownerEntity
    ownerEntity.currentWeapon = self
  end,

  playRecoilAnimation = function(self, owner)
  end,

  fire = function(self)
    c_enableParticleEmitter(self, true)
    c_setLightPointOffset(self, c_normalizeVec(self.gunshotProperties.mainAxis))
    c_enableLightPoint(self, true)
    c_setTimer(self, 0.05, function(t) 
      c_enableParticleEmitter(t, false) 
      c_enableLightPoint(self, false)
      self:playRecoilAnimation()
      
      -- рандомно генерим отдачу
      local recoil = self:generateRecoil()

      c_applyImpulse(c_getBoneBodyWorldArrayIndex(player, "weapon_l"), recoil * 0.5, createVector(0.0, 0.4, 0.0))
      c_applyCentralImpulse(c_getBoneBodyWorldArrayIndex(player, "weapon_r"), recoil)

      -- пыль 
      --[[local spray = c_createEntity(prototypes["spray"]:derive{
        name = "spray-" .. tostring(mapPoint.x) .. "-" .. tostring(mapPoint.y),
        initialComponents = 
        {
          position = mapPoint,
          particleEmitter = {
            startParticleSize = 2.5,
            endParticleSize = 0.0
          }
        }
      })

      -- через полсекунды вырубаем эмиттер, еще через полсекунды удаляем его
      c_setTimer(self, 0.15, function(t)
        c_enableParticleEmitter(spray, false)
        c_setTimer(self, 0.25, function(t)
          c_removeEntity(spray)
        end)
      end)]]
    end)
  end,

  enableAiming = function(self, enabled)
    if (not enabled) then -- отменяем прицеливание
      c_setBoneBodyPos(self.owner, "weapon_r", createVector())
      c_setBoneBodyPos(self.owner, "weapon_l", createVector())

      c_orientBoneBodyToPoint(self.owner, "head", 1, false)
      c_orientBoneBodyToPoint(self.owner, "weapon_l", 1, false)
    end


    c_enableBoneBodyAnimation(self.owner, 
      {"hand_l", "forearm_l", "shoulder_l", "clavicle_l", "weapon_l", "zero_joint_arm_l", -- левая рука
       "hand_r", "forearm_r", "shoulder_r", "clavicle_r", "weapon_r", "zero_joint_arm_r",-- правая рука
       "neck", "head" -- торс
      }, 
      not enabled
    )

    self.owner.aiming = enabled
    self.owner:playIdleAnimation()

  end,
  unassign = function(self)
    self.owner.aiming = false
    self.owner.currentWeapon = nil 
    self.owner:playIdleAnimation()
    self.owner = nil
  end,
  dodge = function(self, dirInitial)
    self.owner.busy = true
    
    local dir = normalizeVec((dirInitial == nil) and c_getTargetVelocity(self.owner) or dirInitial)
    local angle = c_findAngle(dir, c_getComponent(self.owner, "direction"))
    
    if (angle <= math.pi * 3.0 / 4.0) then
      c_playAnimationOnce(self.owner, "dodge_forward", 1.0, {"Armature"}, {"zero_joint_arm_l", "zero_joint_arm_r"})
    else
      c_playAnimationOnce(self.owner, "dodge_backwards", 1.0, {"Armature"}, {"zero_joint_arm_l", "zero_joint_arm_r"})
    end

    c_move(self.owner, dir * 3.0)

    c_setTimer(self.owner, 0.45, function(t)  
      c_stop(self.owner)
      c_setTimer(self.owner, 0.05, function(t) self.owner.busy = false end)
    end)
  end,
  onMiss = function(self, args)
    local spray = c_createEntity(prototypes["spray"]:derive{
      name = "spray-" .. tostring(args.hitCoords.x) .. "-" .. tostring(args.hitCoords.y),
      sprayProperties = {
        mainAxis = args.hitNormalWorld
      },
      initialComponents = 
      {
        position = args.hitCoords,
        particleEmitter = {
          startParticleSize = 2.5,
          endParticleSize = 1.0,
        }
      }
    })

    -- через полсекунды вырубаем эмиттер, еще через полсекунды удаляем его
    c_setTimer(self, 0.15, function(t)
      c_enableParticleEmitter(spray, false)
      c_setTimer(self, 0.25, function(t)
        c_removeEntity(spray)
      end)
    end)
  end,
  onHit = function(self, args)
    --[[c_debugDrawVector(args.hitCoords, args.traceDir, {r = 1.0})]]
    c_applyCentralImpulse(args.targetBodyWorldIndex, args.traceDir * 100.0)

    
    if (args.targetEntity.hitPoints > 0) then
      local damage = self.minDamage + c_random(self.maxDamage - self.minDamage)
      args.targetEntity.hitPoints = args.targetEntity.hitPoints - damage
      c_looseBoneBodies(args.targetEntity, 0.5)

      if (args.targetEntity.hitPoints <= 0) then
        if (math.abs(angle(args.traceDir, c_getDir(args.targetEntity))) > math.pi / 2.0) then
          c_playAnimationOnce(args.targetEntity, "death_backward", 2.0, {"Armature"})  
        else
          c_playAnimationOnce(args.targetEntity, "death_forward", 2.0, {"Armature"})
        end

        c_setTimer(args.targetEntity, 1.0, function()
          c_setDead(args.targetEntity, true)
        end)
      end
    end

  end
}

dofile(c_getScriptsFolder() .. "/weapons/shotgun.lua")
dofile(c_getScriptsFolder() .. "/weapons/pistol.lua")

function createSpacer(entity, bone1, bone2)

  local dist = c_getBoneBodyPos(entity, bone2) - c_getBoneBodyPos(entity, bone1)

  c_addBoneBodyConstraint(player, bone1, bone2, 
    {
      linearLowerLimits = dist,
      linearUpperLimits = dist,
      angularLowerLimits = {x = 0.0, y = 0.0, z = 0.0},
      angularUpperLimits = {x = 0.0, y = 0.0, z = 0.0}
    }
  )
end