prototypes["shotgun"] = prototypes["gun"]:derive{
  name = "shotgun",
  recoilFactor = 100,
  weaponProperties = {
    spreadAngle = math.pi * 0.03,
    maxDistance = 20.0
  },
  gunshotProperties = {
    offset = 0.55,
    length = 1.4
  },
  shoulderToRWeaponDistance = 1.1 * 0.4892697930336, -- расстояние от рук до левого плеча, выяснено опытным путем
  shoulderToLWeaponDistance = 0.25455778241158,
  initialComponents = {
    position = {},
    collideable = {
      shape = {
        type = "shiftedCylinder",
        radius = 0.06,
        length = 0.6,
        localTransform = 
        {
          basis = {
            1.0, 0.0, 0.0,
            0.0, 0.0, 1.0,
            0.0, 1.0, 0.0
          },
          origin = createVector(0.0, 0.0, 0.3)
        }
      }
    },
    lightPoint = {
      radius = 15.0
    },
    meshBatch = {
      meshes = {
        "shotty"
      },
      scale = 0.027,
      initialTransform = {
        basis = {
          1.0, 0.0, 0.0,
          0.0, 1.0, 0.0,
          0.0, 0.0, 1.0
        },
        origin = {x = 0.0, y = 0.05, z = 0.3}
      }
    },
    particleEmitter = {
      maxParticlesCount = 1200
    }
  },

  -- METHODS
  playWalkAnimation = function(self)

    local restrictedNodes = {}
    local allowedNodes = {}

    if (self.owner.aiming) then
      c_playAnimation(self.owner, "rifle_aim", 1.0, true, {"spine_01"})
      table.insert(restrictedNodes, "spine_01")
      table.insert(allowedNodes, "pelvis")
    else
      c_playAnimation(self.owner, "rifle_idle", 1.0, true, {"spine_01"})
      table.insert(allowedNodes, "Armature")
      table.insert(restrictedNodes, "spine_01")
    end 

    if (c_getComponent(self.owner, "moveable").walkForward) then --ходьба вперед
      --c_consoleLog("here2")
      c_playAnimation(self.owner, self.owner.toggleRun and "run_forward" or "walk_forward",
        1.0, false, allowedNodes, restrictedNodes)
    else -- ходьба назад
      --c_consoleLog("here3")
      c_playAnimation(self.owner, self.owner.toggleRun and "run_backward" or "walk_backward", 
        1.0, false, allowedNodes, restrictedNodes)
    end
  
  end,

  playIdleAnimation = function(self)
    if (not self.owner.aiming) then
      c_playAnimation(self.owner, "rifle_idle", 1.0 , true, {"Armature"})
      c_playAnimation(self.owner, "idle", 1.0 , false, {"spine_01"}, {"zero_joint_arm_l", "zero_joint_arm_r"})
    else
      c_playAnimation(self.owner, "rifle_aim", 1.0 , true, {"Armature"})
    end
  end,

  playRecoilAnimation = function(self)
  end,

  attack = function(self, mapPoint)
    if (not self.owner.busy) then

      for i = 1, 4 do
        local shotDir = normalizeVec(createVector(mapPoint) - c_getPos(self))
        local randomRotVert = 
        {
          axis = {z = 1.0},
          angle = (c_random(100) % 2 == 0 and 1 or -1) * self.weaponProperties.spreadAngle / 2.0 * c_random(100) / 100.0
        }
        local randomRotHor = 
        {
          axis = crossProduct(normalizeVec(shotDir), createVector({z = 1.0})),
          angle = (c_random(100) % 2 == 0 and 1 or -1) * self.weaponProperties.spreadAngle / 2.0 * c_random(100) / 100.0
        }
        shotDir = c_rotateVec(c_rotateVec(shotDir, randomRotVert), randomRotHor)
        c_drawTrace(self, shotDir * self.weaponProperties.maxDistance) -- TODO: переименовать функцию
      end

      self:fire()
    end
  end,

  orientTo = function(self, mapCoords)
    c_orientTo(self.owner, mapCoords)

    -- поворачиваем части тела в направлении указанной точки

    if (self.owner.aiming) then

      c_orientBoneBodyToPoint(self.owner, "head", 1, mapCoords)

      local shoulderPos = createVector(c_getBoneBodyPos(self.owner, "clavicle_l"))
      local shoulderToGroundNormalized = normalizeVec(createVector(mapCoords) - shoulderPos)
      local weaponRNewPos = shoulderPos + shoulderToGroundNormalized * self.shoulderToRWeaponDistance
      local weaponLNewPos = shoulderPos + shoulderToGroundNormalized * self.shoulderToLWeaponDistance
      
      c_setBoneBodyPos(self.owner, "weapon_r", weaponRNewPos)
      c_setBoneBodyPos(self.owner, "weapon_l", weaponLNewPos)

      c_orientBoneBodyToPoint(self.owner, "weapon_l", 1, mapCoords)

      --c_debugDrawVector(weaponLNewPos, shoulderToGroundNormalized, {r = 1.0})
      --c_debugDrawVector(weaponRNewPos, shoulderToGroundNormalized, {g = 1.0})

      -- поворачиваем вспышку в нужное место
      local shotgunCurrPos = c_getPos(self)

      self.gunshotProperties.mainAxis = normalizeVec(createVector(createVector(mapCoords) - createVector(c_getPos(self))))
      self.gunshotProperties.upAxis = normalizeVec(crossProduct(self.gunshotProperties.mainAxis, createVector(0.0, 0.0, 1.0)))

    --
    else
      c_orientBoneBodyToPoint(self.owner, "weapon_l", 1, 
        c_getBoneBodyPos(self.owner, "weapon_r") + (c_getBoneBodyPos(self.owner, "weapon_r") - c_getBoneBodyPos(self.owner, "hand_r")) +
        createVector(0.0, 0.0, 0.05))
    end

  end,
  assign = function(self, owner)

    owner.currentWeapon = self
    self.owner = owner
    c_setWeaponBody(self.owner, self, "weapon_l")
    c_setBoneOrientationRatio(player, 1.0, 0.0)

    c_enableBoneBodyConstraint(self.owner, "weapon_l", 0, false)
    c_enableBoneBodyConstraint(self.owner, "weapon_r", 0, false)
    c_enableBoneBodyConstraint(self.owner, "hand_r", 0, false)
    c_enableBoneBodyConstraint(self.owner, "hand_l", 0, false)
    c_enableBoneBodyConstraint(self.owner, "forearm_l", 0, false)
    c_enableBoneBodyConstraint(self.owner, "forearm_r", 0, false)
    c_enableBoneBodyConstraint(self.owner, "shoulder_r", 0, false)

    c_addBoneBodyConstraint(self.owner, "hand_l", "weapon_l",
      {
        frameBBasis = {
          -0.351999, -0.279916, 0.893165, 
          0.718935, 0.530178, 0.449492, 
          -0.599357, 0.800349, 0.014619
        }
      }
    )
    
    c_addBoneBodyConstraint(self.owner, "hand_r", "weapon_r", 
      {
        frameBBasis = {
          0.047808, 0.997569, 0.050707, 
          0.205389, -0.059498, 0.976870, 
          0.977512, -0.036288, -0.207734
        }
      }
    )

    c_addBoneBodyConstraint(self.owner, "forearm_l", "hand_l", 
      {
        frameBBasis = {
          0.882795, -0.136942, -0.449355, 
          0.123000, 0.990577, -0.060236, 
          0.453370, -0.002094, 0.891320
        }
      }
    )
    
    c_addBoneBodyConstraint(self.owner, "forearm_r", "hand_r", 
      {
        frameBBasis = {
          0.117935, -0.058449, 0.991300, 
          -0.201277, 0.976138, 0.081501, 
          -0.972409, -0.209138, 0.103357
        }
      }
    )

    c_addBoneBodyConstraint(self.owner, "shoulder_l", "forearm_l", 
      {
        frameBBasis = {
          -0.417714, -0.882316, 0.216873, 
          0.882560, -0.450737, -0.133879, 
          0.215877, 0.135481, 0.966976
        }
      }
    )
    
    c_addBoneBodyConstraint(self.owner, "shoulder_r", "forearm_r", 
      {
        frameBBasis = {
          0.431414, -0.897849, 0.088028, 
          0.898099, 0.418183, -0.136167, 
          0.085446, 0.137802, 0.986767
        }
      }
    )
    
    c_addBoneBodyConstraint(self.owner, "clavicle_r", "shoulder_r", 
      {
        frameBBasis = {
          0.113074, -0.033934, 0.993007, 
          0.254587, 0.967041, 0.004056, 
          -0.960416, 0.252348, 0.117986
        }
      }
    )

    self.owner:enableAiming(false)
  end,
  unassign = function(self)

    c_setBoneBodyPos(self.owner, "weapon_r", createVector())
    c_setBoneBodyPos(self.owner, "weapon_l", createVector())
    c_orientBoneBodyToPoint(self.owner, "weapon_l", 1, false)

    c_removeBoneBodyConstraint(self.owner, "weapon_l", 0)
    c_removeBoneBodyConstraint(self.owner, "weapon_l", 1)
    c_removeBoneBodyConstraint(self.owner, "weapon_r", 1)
    c_removeBoneBodyConstraint(self.owner, "hand_r", 1)
    c_removeBoneBodyConstraint(self.owner, "hand_l", 1)
    c_removeBoneBodyConstraint(self.owner, "forearm_l", 1)
    c_removeBoneBodyConstraint(self.owner, "forearm_r", 1)
    c_removeBoneBodyConstraint(self.owner, "shoulder_r", 1)

    --c_enableBoneBodyConstraint(self.owner, "weapon_l", 0, true)
    c_enableBoneBodyConstraint(self.owner, "weapon_r", 0, true)
    c_enableBoneBodyConstraint(self.owner, "hand_r", 0, true)
    c_enableBoneBodyConstraint(self.owner, "hand_l", 0, true)
    c_enableBoneBodyConstraint(self.owner, "forearm_l", 0, true)
    c_enableBoneBodyConstraint(self.owner, "forearm_r", 0, true)
    c_enableBoneBodyConstraint(self.owner, "shoulder_r", 0, true)

    self.owner.aiming = false
    self.owner.currentWeapon = nil
    self.owner:playIdleAnimation() 
    self.owner = nil
  end
}