prototypes["baseballBat"] = prototypes["meleeWeapon"]:derive{
  name = "baseballBat",
  initialComponents = {
    position = {},
    meshBatch = {
      meshes = {
        "baseball-bat"
      },
      material = {r = 0.54, g = 0.38, b = 0.17},
      scale = 0.002,
      initialTransform = {
        basis = {
          1.0, 0.0, 0.0,
          0.0, 0.0, 1.0,
          0.0, -1.0, 0.0
        },
        origin = {x = -0.05, y = -0.05, z = 0.0}
      }
    },
    collideable = {
      mass = 10.0,
      shape = {
        type = "shiftedCylinder",
        radius = 0.06,
        length = 0.6,
        localTransform = 
        {
          basis = {
            1.0, 0.0, 0.0,
            0.0, 0.0, 1.0,
            0.0, 1.0, 0.0
          },
          origin = createVector(-0.05, -0.05, 0.3)
        }
      }
    }
  },

  -- METHODS

  enableAiming = function(self, enabled)
    c_enableBoneBodyAnimation(self.owner, 
      {"hand_l", "forearm_l", "shoulder_l", "clavicle_l", "weapon_l", "zero_joint_arm_l", -- левая рука
       "hand_r", "forearm_r", "shoulder_r", "clavicle_r", "weapon_r", "zero_joint_arm_r",-- правая рука
       "neck", "head" -- торс
      }, 
      enabled
    )

    self.owner.aiming = enabled
    self.owner:playIdleAnimation()

  end,

  playWalkAnimation = function(self, velocity)

    local velocity = c_getComponent(self.owner, "moveable").targetLinearVelocity

    local movAnimNamePart = "walk"

    if (self.owner.toggleRun) then movAnimNamePart = "run" end

    if (not self.owner.aiming) then
      if (c_getComponent(self.owner, "moveable").walkForward) then --ходьба вперед
        c_playAnimation(self.owner, "bat_walk_forward", 1.0, true, {"spine_01"})
        c_playAnimation(self.owner, movAnimNamePart .. "_forward", 1.0, false, {"Armature"}, {"spine_01"})
      else -- ходьба назад
        c_playAnimation(self.owner, "bat_walk_forward", 1.0, true, {"spine_01"})
        c_playAnimation(self.owner, movAnimNamePart .. "_backward", 1.0, false, {"Armature"}, {"spine_01"})
      end
    else
      if (c_getComponent(self.owner, "moveable").walkForward) then --ходьба вперед
        c_playAnimation(self.owner, "bat_aim_walk_forward", 1.0, true, {"spine_01"})
        c_playAnimation(self.owner, movAnimNamePart .. "_forward", 1.0, false, {"Armature"}, {"spine_01"})
      else -- ходьба назад
        c_playAnimation(self.owner, "bat_aim_walk_forward", 1.0, true, {"spine_01"})
        c_playAnimation(self.owner, movAnimNamePart .. "_backward", 1.0, false, {"Armature"}, {"spine_01"})
      end

      if (self.attacking) then c_playAnimationOnce(self.owner, "bat_attack_01", 1.2, {"spine_01"}) end
    end
  
  end,

  playIdleAnimation = function(self)
    if (not self.owner.aiming) then
      c_playAnimation(self.owner, "bat_idle", 1.0 , true, {"Armature"})
    else
      c_playAnimation(self.owner, "bat_aim", 1.0, true, {"Armature"})
      if (self.attacking) then -- если персонаж замахнулся при ходьбе, доигрываем анимацию
        c_playAnimationOnce(self.owner, "bat_attack_01", 1.2, {"spine_01"})
      end
    end
  end,

  playRecoilAnimation = function(self)
  end,

  attack = function(self, mapPoint)

    if (not self.owner.busy) then

      --[[if (lengthVec(c_getTargetVelocity(self.owner)) > 0.0) then
        c_playAnimationOnce(self.owner, "bat_attack_01", 1.2, {"spine_01"})
        c_setTimer(self.owner, 0.7, function(t) 
          t.busy = false
          self.attacking = false
          c_setActiveStrikeable(self, false)
        end)
      else
        self.owner.unmoveable = true
        c_orientBodyTo(self.owner, mapPoint)
        c_playAnimationOnce(self.owner, "bat_attack_01", 1.2, {"Armature"})
        
        c_setTimer(self.owner, 0.7, function(t) 
          t.busy = false
          self.attacking = false
          self.owner.unmoveable = false
          c_setActiveStrikeable(self, false)
        end)
      end]]

      self.attackID = self.attackID == nil and 0 or math.fmod(self.attackID + 1, 3)
      local attackName = "bat_attack_0" .. tostring(self.attackID + 1)

      local attackedEntity = c_getEntityByMapCoords(mapPoint)
      
      if (attackedEntity ~= nil) then
        c_orientBodyTo(self.owner, c_getPos(attackedEntity))
      end

      c_playAnimationOnce(self.owner, attackName, 1.2, {"Armature"})

      self.owner.busy = true
      self.attacking = true
      c_setActiveStrikeable(self, true)

      self.owner.unmoveable = true      

      c_setTimer(self.owner, 0.7, function(t) 
          t.busy = false
          self.attacking = false
          self.owner.unmoveable = false
          c_setActiveStrikeable(self, false)
        end)
    end
  end,

  assign = function(self, ownerEntity)

    self.owner = ownerEntity
    self.owner.currentWeapon = self
    c_setWeaponBody(self.owner, self, "weapon_l")
    self.owner:playIdleAnimation()
  end
}