-- COMMON PART

const = {

  PI = 3.14159265359,
  HALF_PI = 1.57079632679,
  QUARTER_PI = 0.78539816339,
  THREE_PI_OVER_FOUR = 2.35619449019,
  TWO_PI_OVER_THREE = 2.09439510239,
  TILESTEP = 32,
  FLOOR_SELECTION = 0,
  WALL_SELECTION = 1,
  SPEED_UP_COEFFICIENT = 0.01,
  IMG_FOLDER = [[Images\]],
  GUI_FOLDER = [[Images\GUI\]]
}

function setMetaTables(original, derived)
  for key, val in pairs(derived) do
    if type(val) == "table" and original[key] ~= nil and type(original[key]) == "table" then
      setMetaTables(original[key], val)
    end
  end

  setmetatable(derived, original)
  original.__index = original
end

prototypes = {
  
  base = {
    name = "base",
    initialComponents = {},
    
    ---------- methods
    
    onInit = function(self) end,

    derive = function (self, t, preInitFunc)

      t["prototype"] = self.name

      --[[if t.initialComponents ~= nil then
        for key, val in pairs(t.initialComponents) do
          if self.initialComponents[key] ~= nil then
            setmetatable(t.initialComponents[key], self.initialComponents[key])
            self.initialComponents[key].__index = self.initialComponents[key]
          end
        end

        setmetatable(t.initialComponents, self.initialComponents)
        self.initialComponents.__index = self.initialComponents
      end

      self.__index = self

      -- если задана функция, обрабатывающая созданную таблицу t перед тем, как она отдается на инициализацию
      if (preInitFunc ~= nil) then preInitFunc(t) end

      return t]]

      setMetaTables(self, t)

      -- если задана функция, обрабатывающая созданную таблицу t перед тем, как она отдается на инициализацию
      if (preInitFunc ~= nil) then preInitFunc(t) end

      return t
    end
  }
}

prototypes["object"] = prototypes["base"]:derive{
  name = "object",

  initialComponents = {
    position = {}
  }
}