-- плейсхолдеры для измененных функций
function c_playAnimation(entity, animName, velocity, resetAnimationBatch, allowedRootNodes, restrictedRootNodes)
	c_playAnim(entity, {
		name = animName,
		allowedRootNodes = allowedRootNodes,
		restrictedRootNodes = restrictedRootNodes ~= nil and restrictedRootNodes or nil,
		resetAnimationBatch = resetAnimationBatch,
		speed = velocity,
		looped = true
	})
end

function c_playAnimationOnce(entity, animName, exactTime, allowedRootNodes, restrictedRootNodes)
	c_playAnim(entity, {
		name = animName,
		allowedRootNodes = allowedRootNodes == nil and {"Armature"} or allowedRootNodes,
		restrictedRootNodes = restrictedRootNodes == nil and nil or restrictedRootNodes,
		resetAnimationBatch = false,
		exactTime = exactTime,
		looped = false
	})
end