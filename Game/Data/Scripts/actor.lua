--dofile("Data/Scripts/weapons.lua")

MODEL_SCALE = 0.037
minUpperLimit = 0.0

prototypes["actor"] = prototypes["base"]:derive({
  
  ------ INIT ------
  name = "actor",
  attackStack = 2,
  toggleRun = false,
  runByDir = false,

  -- игровая логика
  hitPoints = 100,

  assignWeapon = function(self, weapon)
    
    if (self.currentWeapon ~= nil) then
      self.currentWeapon:unassign()
    end

    weapon:assign(self)
  end,

  ----
  
  initialComponents = {
    position = {},
    moveable = {maxLinearVelocity = 3.0, maxAngularVelocity = 6.28},
    pathable = {},
    direction = {x = 1.0, y = 0.0},
    inverseKinematicsController = {},
    autoWalkController = {},
    animations = {
      idle = "idle",
      walk_forward = "pistol_aim_walk_forward",
      walk_backwards = "walk_forward",
      weapon_attack_01 = "bat_attack_01",
      weapon_attack_02 = "bat_attack_02",
      weapon_attack_03 = "bat_attack_03",
      weapon_aim = "bat_aim"
    },
    meshBatch = {
      meshes = {
        "guy"
      }
    },
    skeleton = {
      name = "main",
      
      -- здесь все в координатах сцены
      boneBindingPoints = {
        Armature = {
          head = {x = 0.0, y = 24.0, z = 0.0},
          tail = {x = 0.0, y = 24.0156, z = 0.0}
        },
        pelvis = {
          head = {x = -0.008463, y = 24.0149, z = -0.174333},
          tail = {x = -0.008463, y = 24.0249, z = -0.174333}
        },
        zero_joint_thigh_l = {
          head = {x = -0.008463, y = 24.0149, z = -0.174333},
          tail = {x = 1.796, y = 22.3433, z = -0.406778}
        },
        zero_joint_thigh_r = {
          head = {x = -0.008463, y = 24.0149, z = -0.174333},
          tail = {x = 1.796, y = 25.6865, z = 0.058111}
        },
        thigh_l = {
          head = {x = 1.796, y = 22.3433, z = -0.406778},
          tail = {x = 2.3591, y = 12.2044, z = 0.377722}
        },
        thigh_r = {
          head = {x = -1.81293, y = 22.3433, z = -0.406778},
          tail = {x = -1.24983, y = 32.4822, z = -1.19128}
        },
        knee_l = {
          head = {x = 2.3591, y = 12.2044, z = 0.377722},
          tail = {x = 2.62228, y = 1.64644, z = -0.715631}
        },
        knee_r = {
          head = {x = -2.37602, y = 12.2044, z = 0.377722},
          tail = {x = -2.11447, y = 22.7614, z = 1.48049}
        },
        foot_l = {
          head = {x = 2.62228, y = 1.64644, z = -0.715631},
          tail = {x = 3.02227, y = 0.53448, z = 2.87344}
        },
        foot_r = {
          head = {x = -2.63921, y = 1.64644, z = -0.715631},
          tail = {x = -2.23923, y = 2.7584, z = -4.3047}
        },
        foot_tip_l = {
          head = {x = 3.02227, y = 0.53448, z = 2.87344},
          tail = {x = 3.02227, y = 4.31308, z = 2.87344}
        },
        foot_tip_r = {
          head = {x = -3.03919, y = 0.534476, z = 2.87344},
          tail = {x = -3.03919, y = -3.24413, z = 2.87344}
        },
        spine_01 = {
          head = {x = -0.008463, y = 24.0149, z = -0.174333},
          tail = {x = -0.008463, y = 27.3854, z = -0.174333}
        },
        spine_02 = {
          head = {x = -0.008463, y = 27.3854, z = -0.174333},
          tail = {x = -0.008463, y = 30.7849, z = -0.2615}
        },
        chest = {
          head = {x = -0.008463, y = 30.7849, z = -0.2615},
          tail = {x = -0.008463, y = 32.5621, z = -0.373621}
        },
        neck = {
          head = {x = -0.008463, y = 36.087, z = -0.914188},
          tail = {x = -0.008463, y = 38.6697, z = -0.246071}
        },
        head = {
          head = {x = -0.008463, y = 38.6697, z = -0.24607},
          tail = {x = -0.008463, y = 43.4849, z = -0.215715}
        },
        head_tip = {
          head = {x = -0.008463, y = 43.4849, z = -0.215715},
          tail = {x = -0.008463, y = 48.3001, z = -0.215714}
        },
        zero_joint_arm_l = {
          head = {x = -0.008463, y = 30.7849, z = -0.2615},
          tail = {x = -0.008463, y = 35.7426, z = -0.574276}
        },
        zero_joint_arm_r = {
          head = {x = -0.008463, y = 30.7849, z = -0.2615},
          tail = {x = -0.008463, y = 25.8272, z = 0.051274}
        },
        clavicle_l = {
          head = {x = 1.25153, y = 35.5789, z = -0.587093},
          tail = {x = 4.34852, y = 36.0147, z = -0.607406}
        },
        clavicle_r = {
          head = {x = -1.26846, y = 35.5789, z = -0.587093},
          tail = {x = 1.69218, y = 36.58, z = -0.70508}
        },
        shoulder_l = {
          head = {x = 4.22171, y = 34.6039, z = -0.492634},
          tail = {x = 9.0661, y = 34.7342, z = -1.41855}
        },
        shoulder_r = {
          head = {x = -4.23863, y = 34.6039, z = -0.492634},
          tail = {x = 0.605758, y = 34.4735, z = 0.433281}
        },
        forearm_l = {
          head = {x = 9.0661, y = 34.7342, z = -1.41855},
          tail = {x = 15.5723, y = 34.656, z = -1.01709}
        },
        forearm_r = {
          head = {x = -9.08302, y = 34.7342, z = -1.41855},
          tail = {x = -2.57684, y = 34.8125, z = -1.82001}
        },
        zero_joint_hand_l = {
          head = {x = 15.5723, y = 34.656, z = -1.01709},
          tail = {x = 15.5822, y = 34.6558, z = -1.01648}
        },
        zero_joint_hand_r = {
          head = {x = -15.5892, y = 34.656, z = -1.01709}, 
          tail = {x = -15.5792, y = 34.6561, z = -1.0177}
        },
        hand_l = {
          head = {x = 15.5723, y = 34.656, z = -1.01709},
          tail = {x = 17.5885, y = 34.2543, z = 0.206502}
        },
        hand_r = {
          head = {x = -15.5892, y = 34.656, z = -1.01709},
          tail = {x = -13.2026, y = 34.7983, z = -0.930273}
        },
        weapon_l = {
          head = {x = 17.3914, y = 33.8466, z = -1.10845},
          tail = {x = 17.3914, y = 36.2391, z = -1.10845}
        },
        weapon_r = {
          head = {x = -17.4083, y = 33.8466, z = -1.10845},
          tail = {x = -17.4083, y = 31.4541, z = -1.10845}
        },
        middle_tip_l = {
          head = {x = 20.7521, y = 34.1118, z = -1.43808},
          tail = {x = 20.7521, y = 34.753, z = -1.43808}
        },
        middle_tip_r = {
          head = {x = -20.769, y = 34.1118, z = -1.43808},
          tail = {x = -20.769, y = 33.4706, z = -1.43808}
        },
        thumb_02_r = {
          head = {x = -17.5697, y = 33.817, z = 0.23868},
          tail = {x = -17.001, y = 34.2374, z = -0.140975}
        },
        thumb_02_l = {
          head = {x = 17.5528, y = 33.817, z = 0.23868},
          tail = {x = 18.1215, y = 33.3966, z = 0.618313}
        }
      },
      boneRigidBodies = {
        Armature = {
          firstJoint = "foot_tip_r",
          secondJoint = "head_tip",
          shape = {
            type = "cylinder",
            radius = 7.0
          },
          mass = 0.1,
          ignoreCollisionWithAllBones = true,
          collisionMask = "character_boxes",
          collisionGroup = "character_boxes"
        },
        weapon_l = {
          firstJoint = "weapon_l",
          secondJoint = "thumb_02_l",
          shape = {
            type = "cylinder",
            radius = 0.01 / MODEL_SCALE
          },
          mass = 10.0,
          ignoreCollisionWithAllBones = true,
          debug = true
        },
        weapon_r = {
          firstJoint = "weapon_r",
          secondJoint = "thumb_02_r",
          shape = {
            type = "cylinder",
            radius = 0.01 / MODEL_SCALE  
          },
          mass = 10.0,
          ignoreCollisionWithAllBones = true,
          invertedAxis = true,
          debug = true
        },

        -- торс

        pelvis = {
          firstJoint = "pelvis",
          shape = {
            radius = 2.243,
            type = "sphere",
          },
          mass = 10.0,
          ignoreCollisionWithAllBones = true
          
        },
        head = {
          firstJoint = "head",
          secondJoint = "head_tip",
          shape = {
            type = "capsule",
            radius = 0.073 / MODEL_SCALE,
          },
          mass = 10.0,
          affectable = true,
          ignoreCollisionWithAllBones = true
        },
        neck = {
          firstJoint = "neck",
          secondJoint = "head",
          shape = {
            type = "capsule",
            radius = 0.04 / MODEL_SCALE,
          },
          mass = 10.0,
          affectable = true,
          ignoreCollisionWithAllBones = true
        },
        chest = {
          firstJoint = "chest",
          secondJoint = "neck",
          shape = {
            type = "cylinder",
            radius = 0.13 / MODEL_SCALE
          },
          mass = 10.0,
          affectable = true
        },
        spine_02 = {
          firstJoint = "spine_02",
          secondJoint = "chest",
          shape = {
            type = "cylinder",
            radius = 0.12 / MODEL_SCALE
          },
          mass = 10.0,
          ignoreCollisionWithAllBones = true
        },
        spine_01 = {
          firstJoint = "spine_01",
          secondJoint = "spine_02",
          shape = {
            type = "cylinder",
            radius = 0.11 / MODEL_SCALE
          },
          mass = 10.0
        },

        -- руки

        zero_joint_arm_l = {
          firstJoint = "zero_joint_arm_l",
          secondJoint = "clavicle_l",
          shape = {
            type = "capsule",
            radius = 0.03 / MODEL_SCALE
          },
          mass = 10.0,
          ignoreCollisionWithAllBones = true,
          affectable = true
        },
        zero_joint_arm_r = {
          firstJoint = "zero_joint_arm_r",
          secondJoint = "clavicle_r",
          shape = {
            type = "capsule",
            radius = 0.03 / MODEL_SCALE
          },
          mass = 10.0,
          ignoreCollisionWithAllBones = true,
          invertedAxis = true,
          affectable = true
        },
        clavicle_l = {
          firstJoint = "clavicle_l",
          secondJoint = "shoulder_l",
          shape = {
            type = "capsule",
            radius = 0.05 / MODEL_SCALE
          },
          mass = 10.0,
          ignoreCollisionWithAllBones = true,
          affectable = true
        },
        clavicle_r = {
          firstJoint = "clavicle_r",
          secondJoint = "shoulder_r",
          shape = {
            type = "capsule",
            radius = 0.05 / MODEL_SCALE
          },
          mass = 10.0,
          invertedAxis = true,
          ignoreCollisionWithAllBones = true,
          affectable = true
        },
        shoulder_l = {
          firstJoint = "shoulder_l",
          secondJoint = "forearm_l",
          shape = {
            type = "capsule",
            radius = 0.05 / MODEL_SCALE
          },
          mass = 10.0,
          affectable = true,
          ignoreCollisionWithAllBones = true
        },
        shoulder_r = {
          firstJoint = "shoulder_r",
          secondJoint = "forearm_r",
          shape = {
            type = "capsule",
            radius = 0.05 / MODEL_SCALE
          },
          mass = 10.0,
          invertedAxis = true,
          affectable = true,
          ignoreCollisionWithAllBones = true
        },
        forearm_l = {
          firstJoint = "forearm_l",
          secondJoint = "hand_l",
          shape = {
            type = "capsule",
            radius = 0.05 / MODEL_SCALE
          },
          mass = 10.0,
          affectable = true
        },
        forearm_r = {
          firstJoint = "forearm_r",
          secondJoint = "hand_r",
          shape = {
            type = "capsule",
            radius = 0.05 / MODEL_SCALE
          },
          mass = 10.0,
          invertedAxis = true,
          affectable = true
        },
        hand_l = {
          firstJoint = "hand_l",
          secondJoint = "weapon_l",
          shape = {
            type = "capsule",
            radius = 0.03 / MODEL_SCALE
          },
          mass = 10.0,
          affectable = true,
          ignoreCollisionWithAllBones = true
        },
        hand_r = {
          firstJoint = "hand_r",
          secondJoint = "weapon_r",
          shape = {
            type = "capsule",
            radius = 0.03 / MODEL_SCALE
          },
          mass = 10.0,
          invertedAxis = true,
          affectable = true,
          ignoreCollisionWithAllBones = true,
        },

        -- ноги

        zero_joint_thigh_l = {
          firstJoint = "zero_joint_thigh_l",
          secondJoint = "thigh_l",
          shape = {
            type = "capsule",
            radius = 0.03 / MODEL_SCALE,
          },
          mass = 10.0,
          ignoreCollisionWithAllBones = true
        },
        zero_joint_thigh_r = {
          firstJoint = "zero_joint_thigh_r",
          secondJoint = "thigh_r",
          shape = {
            type = "capsule",
            radius = 0.03 / MODEL_SCALE
          },
          mass = 10.0,
          ignoreCollisionWithAllBones = true,
          invertedAxis = true
        },
        thigh_l = {
          firstJoint = "thigh_l",
          secondJoint = "knee_l",
          shape = {
            type = "capsule",
            radius = 0.06 / MODEL_SCALE
          },
          mass = 10.0,
          ignoreCollisionWithAllBones = true
        },
        thigh_r = {
          firstJoint = "thigh_r",
          secondJoint = "knee_r",
          shape = {
            type = "capsule",
            radius = 0.06 / MODEL_SCALE
          },
          mass = 10.0,
          invertedAxis = true,
          ignoreCollisionWithAllBones = true
        },
        knee_l = {
          firstJoint = "knee_l",
          secondJoint = "foot_l",
          shape = {
            type = "capsule",
            radius = 0.05 / MODEL_SCALE
          },
          mass = 10.0,
          ignoreCollisionWithAllBones = true
        },
        knee_r = {
          firstJoint = "knee_r",
          secondJoint = "foot_r",
          shape = {
            type = "capsule",
            radius = 0.05 / MODEL_SCALE
          },
          mass = 10.0,
          invertedAxis = true,
          ignoreCollisionWithAllBones = true
        },
        foot_l = {
          firstJoint = "foot_l",
          secondJoint = "foot_tip_l",
          shape = {
            type = "capsule",
            radius = 0.03 / MODEL_SCALE
          },
          mass = 10.0,
          ignoreCollisionWithAllBones = true
        },
        foot_r = {
          firstJoint = "foot_r",
          secondJoint = "foot_tip_r",
          shape = {
            type = "capsule",
            radius = 0.03 / MODEL_SCALE
          },
          mass = 10.0,
          invertedAxis = true,
          ignoreCollisionWithAllBones = true
        }
      },
      constraints = {
        -- торс

        {
          firstBody = "neck",
          secondBody = "head",
          angularLowLimit = {x = -math.pi / 8.0, y = -math.pi / 4.0, z = -math.pi / 8.0},
          angularUpperLimit = {x = math.pi / 8.0, y = math.pi / 4.0, z = math.pi / 8.0},
          linearLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = -minUpperLimit},
          linearUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit}
        },
        {
          firstBody = "chest",
          secondBody = "neck",
          angularLowLimit = {x = -math.pi / 8.0, y = -math.pi / 4.0, z = -math.pi / 8.0},
          angularUpperLimit = {x = math.pi / 8.0, y = math.pi / 4.0, z = math.pi / 8.0},
          linearLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = -minUpperLimit},
          linearUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit}
        },
        {
          firstBody = "spine_02",
          secondBody = "chest",
          angularLowLimit = {x = -math.pi/32.0, y = -math.pi/32.0, z = -math.pi/32.0},
          angularUpperLimit = {x = math.pi/32.0, y = math.pi/32.0, z = math.pi/32.0},
          linearLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = -minUpperLimit},
          linearUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit}
        },
        {
          firstBody = "spine_01",
          secondBody = "spine_02",
          angularLowLimit = {x = -math.pi/16.0, y = -math.pi/6.0, z = -math.pi/8.0},
          angularUpperLimit = {x = math.pi/16.0, y = math.pi/6.0, z = math.pi/8.0},
          linearLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = -minUpperLimit},
          linearUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit}
        },
        {
          firstBody = "pelvis",
          secondBody = "spine_01",
          angularLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = -minUpperLimit},
          angularUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit},
          linearLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = -minUpperLimit},
          linearUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit}
        },

        -- руки

        {
          firstBody = "spine_02",
          secondBody = "zero_joint_arm_l",
          angularLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = -minUpperLimit},
          angularUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit},
          linearLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = -minUpperLimit},
          linearUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit},
          ignoreCollisionWithAllBones = true
        },
        {
          firstBody = "spine_02",
          secondBody = "zero_joint_arm_r",
          angularLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = -minUpperLimit},
          angularUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit},
          linearLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = -minUpperLimit},
          linearUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit},
          ignoreCollisionWithAllBones = true
        },
        {
          firstBody = "zero_joint_arm_l",
          secondBody = "clavicle_l",
          angularLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = -minUpperLimit},
          angularUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit},
          linearLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = -minUpperLimit},
          linearUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit},
          ignoreCollisionWithAllBones = true
        },
        {
          firstBody = "zero_joint_arm_r",
          secondBody = "clavicle_r",
          angularLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = -minUpperLimit},
          angularUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit},
          linearLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = -minUpperLimit},
          linearUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit},
          ignoreCollisionWithAllBones = true
        },
        {
          firstBody = "clavicle_l",
          secondBody = "shoulder_l",
          
          --[[angularLowLimit = {x = -math.pi/2.5, y = -math.pi / 4.1, z = -math.pi / 6.0},
          angularUpperLimit = {x = math.pi/6.0, y = math.pi / 4.1, z = math.pi / 2.3},]]
          angularLowLimit = {x = -math.pi / 4.0, y = -math.pi / 2, z = -math.pi / 4.0},
          angularUpperLimit = {x = math.pi / 4.0, y = math.pi / 2, z = math.pi / 4.0},
          linearLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = -minUpperLimit},
          linearUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit},
          axisShift = true
        },
        {
          firstBody = "clavicle_r",
          secondBody = "shoulder_r",
          --[[angularLowLimit = {x = -math.pi/2.5, y = -math.pi / 4.1, z = -math.pi / 2.3},
          angularUpperLimit = {x = math.pi/6.0, y = math.pi / 4.1, z = math.pi / 6.0},]]
          -- TODO: дрожание джоинтов происходит изза констрэйнтов
          angularLowLimit = {x = -math.pi / 3.0, y = -math.pi / 2, z = -math.pi / 3.0},
          angularUpperLimit = {x = math.pi / 3.0, y = math.pi / 2, z = math.pi / 3.0},
          linearLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = -minUpperLimit},
          linearUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit},
          axisShift = true
        },
        {
          firstBody = "shoulder_l",
          secondBody = "forearm_l",
          angularLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = math.pi * 0.05},
          angularUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = math.pi * 0.7},
          linearLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = -minUpperLimit},
          linearUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit},
          axisShift = true
        },
        {
          firstBody = "shoulder_r",
          secondBody = "forearm_r",
          angularLowLimit = {x = -minUpperLimit, y = -math.pi, z = -math.pi * 0.7},
          angularUpperLimit = {x = minUpperLimit, y = math.pi, z = -math.pi * 0.05},
          linearLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = -minUpperLimit},
          linearUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit},
          axisShift = true
        },
        {
          firstBody = "forearm_l",
          secondBody = "hand_l",
          angularLowLimit = {x = -math.pi / 3.0, y = -math.pi, z = -math.pi / 16.0},
          angularUpperLimit = {x = math.pi / 2.5, y = math.pi / 3.0, z = math.pi / 8.0},
          linearLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = -minUpperLimit},
          linearUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit},
          axisShift = true
        },
        {
          firstBody = "forearm_r",
          secondBody = "hand_r",
          angularLowLimit = {x = -math.pi / 2.5, y = -math.pi / 3.0, z = -math.pi / 8.0} ,
          angularUpperLimit = {x = math.pi / 3.0, y = minUpperLimit, z = math.pi / 16.0},
          linearLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = -minUpperLimit},
          linearUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit},
          axisShift = true
        },

        -- оружие

        {
          firstBody = "hand_r",
          secondBody = "weapon_r",
          angularLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = -minUpperLimit} ,
          angularUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit},
          linearLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = -minUpperLimit},
          linearUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit},
          axisShift = true
        },
        {
          firstBody = "hand_l",
          secondBody = "weapon_l",
          angularLowLimit = {x = -math.pi, y = -math.pi / 2.0, z = -math.pi} ,
          angularUpperLimit = {x = math.pi, y = math.pi / 2.0, z = math.pi},
          linearLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = -minUpperLimit},
          linearUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit},
          axisShift = true
        },        

        -- ноги

        {
          firstBody = "pelvis",
          secondBody = "zero_joint_thigh_l",
          angularLowLimit = {x = 0.0, y = -math.pi / 16.0, z = -math.pi / 12.0},
          angularUpperLimit = {x = minUpperLimit, y = math.pi / 16.0, z = minUpperLimit},
          linearLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = -minUpperLimit},
          linearUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit}
        },
        {
          firstBody = "pelvis",
          secondBody = "zero_joint_thigh_r",
          angularLowLimit = {x = 0.0, y = -math.pi / 16.0, z = 0.0},
          angularUpperLimit = {x = minUpperLimit, y = math.pi / 16.0, z = math.pi / 12.0},
          linearLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = -minUpperLimit},
          linearUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit}
        },
        {
          firstBody = "zero_joint_thigh_l",
          secondBody = "thigh_l",
          angularLowLimit = {x = -math.pi / 4.0, y = 0.0, z = -math.pi / 4.0},
          angularUpperLimit = {x = math.pi / 3.0, y = minUpperLimit, z = minUpperLimit},
          linearLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = -minUpperLimit},
          linearUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit}
        },
        {
          firstBody = "zero_joint_thigh_r",
          secondBody = "thigh_r",
          angularLowLimit = {x = -math.pi / 3.0, y = -minUpperLimit, z = -minUpperLimit},
          angularUpperLimit = {x = math.pi / 4.0, y = minUpperLimit, z = math.pi / 4.0},
          linearLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = -minUpperLimit},
          linearUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit}
        },
        {
          firstBody = "thigh_l",
          secondBody = "knee_l",
          angularLowLimit = {x = -math.pi / 1.5, y = -minUpperLimit, z = -minUpperLimit},
          angularUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit},
          linearLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = -minUpperLimit},
          linearUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit}
        },
        {
          firstBody = "thigh_r",
          secondBody = "knee_r",
          angularLowLimit = {x = -math.pi / 1.5, y = 0, z = -minUpperLimit},
          angularUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit},
          linearLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = -minUpperLimit},
          linearUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit}
        },
        {
          firstBody = "knee_l",
          secondBody = "foot_l",
          angularLowLimit = {x = -minUpperLimit, y = -math.pi/6.0, z = -minUpperLimit},
          angularUpperLimit = {x = minUpperLimit, y = math.pi/6.0, z = minUpperLimit},
          linearLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = -minUpperLimit},
          linearUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit}
        },
        {
          firstBody = "knee_r",
          secondBody = "foot_r",
          angularLowLimit = {x = -minUpperLimit, y = math.pi/6.0, z = -minUpperLimit},
          angularUpperLimit = {x = minUpperLimit, y = -math.pi/6.0, z = minUpperLimit},
          linearLowLimit = {x = -minUpperLimit, y = -minUpperLimit, z = -minUpperLimit},
          linearUpperLimit = {x = minUpperLimit, y = minUpperLimit, z = minUpperLimit}
        }
      }
    }
  },

  busy = false,
  unmoveable = false,
  currentWeapon = nil,
  aiming = false,

  ----- METHODS -----
  attack = function(self, coords)
    c_getComponent(self, "weapon").attack(self, coords)
  end,

  -- ориентация в общем случае зависит от используемого оружия
  orientTo = function(self, mapCoords)
    if (self.currentWeapon == nil or self.currentWeapon.orientTo == nil) then
      c_orientTo(self, mapCoords)
    else
      self.currentWeapon:orientTo(mapCoords)
    end
  end,

  playIdleAnimation = function(self)
    if (self.currentWeapon == nil) then
      c_playAnimation(self, "idle", 1.0, true, {"Armature"})
    else
      self.currentWeapon:playIdleAnimation()
    end
  end,

  enableAiming = function(self, enabled)
    if (self.currentWeapon ~= nil) then
      self.currentWeapon:enableAiming(enabled)
    end
  end,

  enableRun = function(self, enabled)
    self.toggleRun = enabled

    if (enabled) then
      c_setMaxLinVelocity(self, 9.2)
    else
      c_setMaxLinVelocity(self, 3.4)
    end
  end,

  dodge = function(self, dirInitial)
    if (self.currentWeapon ~= nil) then
      self.currentWeapon:dodge(dirInitial)
    end
  end,

  ----- EVENTS -----

  onInit = function(self)
    c_addComponent(self, "collideable", { 
      worldArrayIndex = c_getBoneBodyWorldArrayIndex(self, "Armature")
    })

    self.animations = self.initialComponents.animations
    self:playIdleAnimation()
    c_setBoneOrientationRatio(self, 1.0, 0.0)
    
    return true
  end,

  onStartMove = function(self)
    local mov = c_getComponent(self, "moveable")

    if (self.currentWeapon == nil) then

      if (mov.walkForward) then
        c_playAnimation(self, self.toggleRun and "run_forward" or "walk_forward", 
        1.0, true, {"Armature"})
      else
        c_playAnimation(self, self.toggleRun and "run_backward" or "walk_backward", 
        1.0, true, {"Armature"})
      end
    else
      self.currentWeapon:playWalkAnimation()
    end

    return not self.numb
  end,

  currentMoveAnimation = nil,

  onStopMove = function(self)
    self:playIdleAnimation()
  end,

  onStartAiming = function(self)
    return true
  end,

  onGetHit = function(self, args)
    -- кровь

    --c_move(self, normalizeVec(createVector(args.traceDir.x, args.traceDir.y, 0.0)))

    local spray = c_createEntity(prototypes["spray"]:derive{
      name = "bloodspray-" .. tostring(args.hitCoords.x) .. "-" .. tostring(args.hitCoords.y),
      sprayProperties = {
        mainAxis = args.hitNormalWorld * (-1.0)
      },
      initialComponents = 
      {
        position = args.hitCoords - args.hitNormalWorld * 0.1,
        particleEmitter = {
          startParticleSize = 3.5,
          endParticleSize = 1.0,
          startColor = { r = 0.5, g = 0.0, b = 0.0, a = 1.0 },
          endColor = { r = 0.5, g = 0.0, b = 0.0, a = 1.0 }
        }
      }
    })

    -- через полсекунды вырубаем эмиттер, еще через полсекунды удаляем его
    c_setTimer(self, 0.15, function(t)
      c_enableParticleEmitter(spray, false)
      c_setTimer(self, 0.25, function(t)
        c_removeEntity(spray)
      end)
    end)

    c_setTimer(self, 0.1, function(t) 
      c_stop(self)
    end)

    return true
  end,

  onWeaponCollidedWithWall = function(self)
  end,

  onChangeOrientation = function(self)
  end,

  onStrikedBy = function(args)
  end
},
function(entity)
  -- проставляет height у капсул и цилиндров
  for kb, vb in pairs(entity.initialComponents.skeleton.boneRigidBodies) do
    if (vb.shape.type == "capsule" or vb.shape.type == "cylinder") then
      local firstJointHeadPos, secondJointHeadPos

      for kbp, vbp in pairs(entity.initialComponents.skeleton.boneBindingPoints) do
        if (kbp == vb.firstJoint) then firstJointHeadPos = vbp.head end
        if (kbp == vb.secondJoint) then secondJointHeadPos = vbp.head end
      end

      if (firstJointHeadPos == nil) then print("Cant find first joint (".. kb ..")") end
      if (secondJointHeadPos == nil) then print("Cant find second joint (".. kb ..")") end

      vb.shape.height = c_getLength({
        x = secondJointHeadPos.x - firstJointHeadPos.x, 
        y = secondJointHeadPos.y - firstJointHeadPos.y
      })

      if (vb.axisOffset == nil) then vb.axisOffset = {} end
      
      vb.axisOffset.x = 0.0
      vb.axisOffset.y = vb.shape.height / 2.0
      vb.axisOffset.z = 0.0

      if (vb.shape.type == "capsule") then
        vb.shape.height = vb.shape.height - vb.shape.radius * 2.0
        if (vb.shape.height) then c_consoleLog("Negative capsule height (" .. kb .. ")") end
      end
    end
  end
end
)