testAnim = function(animName)
	c_playAnimation(player, animName, 1.0, true, {"Armature"})
end

testAnimOnce = function(animName, speed)
	c_playAnimationOnce(player, animName, speed)
end