prototypes["enemy"] = prototypes["actor"]:derive{
  name = "enemy",
  viewAngle = 3.0 * const.PI / 4.0,
  enemyLastSightedPoint = nil,
  
  -- функция просто забирает из компонента viewable таблицы всех видимых сущностей
  checkIfEntityVisible = function(self, entityName)

    entities = c_getComponent(self, "viewable").entitiesInSight

    for k, v in pairs(entities) do
      if (v.name == entityName) then 
        return v
      end
    end

    return nil
  end,

  calmRoutine = function(self) 
    
    goAggressive = false
    player = self:checkIfEntityVisible("player") -- проверка, видит ли перс игрока

    if player ~= nil then -- если заметил игрока
      --goAggressive = true
      enemyLastSightedPoint = c_getComponent(player, "position")
      c_goTo(self, enemyLastSightedPoint.x, enemyLastSightedPoint.y)
      pos = c_getComponent(self, "position")
      c_modifyComponent(self, "orientation", {x = enemyLastSightedPoint.x - pos.x, y = enemyLastSightedPoint.y - pos.y})
    end

    if (not goAggressive) then
      c_setTimer(self, 0.5, function(t) t:calmRoutine() end)
    else
      --self:aggressiveRoutine()
    end

  end,

  aggressiveRoutine = function(self)

    --[[goCalm = false
    player = self:checkIfEntityVisible("player") -- проверка, видит ли перс игрока

    if player ~= nil then -- если заметил игрока
      goAggressive = true
      enemyLastSightedPoint = c_getComponent(player, "position")
    end

    if (enemyLastSightedPoint ~= nil) then
      c_goTo(enemyLastSightedPoint)
    end

    if (not goCalm) then
      --c_setTimer(self, 0.5, self:aggressiveRoutine())
    else
      self:calmRoutine()
    end]]--
  end,

  onInit = function(self)
    print("init")
    c_playAnimation(self, self.animations.idle, 1.0, true, {"Armature"})
    c_setTimer(self, 1.0, function(t) t:calmRoutine() end)

  end
}
