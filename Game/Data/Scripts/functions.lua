-- COMMON

function getTableLen(table)
	local result = 0

	for k,v in pairs(table) do
		result = result + 1
	end

	return result;
end

function getStaticObjects(layerGroup, result, layerOffset)

  	if (layerOffset == nil) then layerOffset = 0 end
  
  	for lk, lv in pairs(layerGroup.layers) do -- парсим каждый слой в группе

		if lv.type == "group" then -- если это группа слоев, передаем её в новый виток рекурсии
			getStaticObjects(lv, result, layerOffset)
		end

		if lv.type == "objectgroup" then
			for k,v in pairs(lv.objects) do
				v.layerOffset = layerOffset
				table.insert(result, v)
			end
		end

		if lv.type ~= "group" then 
			layerOffset = layerOffset + 1 
		end
	end
end