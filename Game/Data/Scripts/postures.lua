-- позы тела, каждая задает определенные параметры для инверсной кинематики
postures = 
{
	-- направление руки в нужную сторону
	
	rightArmPointing = 
	{
		finalEndEffectorPos = 
		endEffector = "hand_r",
		startEffector = "pelvis" -- это первый НЕПОДВИЖНЫЙ джоинт
	}
}