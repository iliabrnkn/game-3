return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "1.0.2",
  orientation = "isometric",
  renderorder = "right-down",
  width = 30,
  height = 30,
  tilewidth = 64,
  tileheight = 32,
  nextobjectid = 153,
  properties = {},
  tilesets = {
    {
      name = "concrete1",
      firstgid = 1,
      filename = "../../../../../game-assets-master/tilesets/concrete1.tsx",
      tilewidth = 64,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/concrete1.png",
      imagewidth = 384,
      imageheight = 192,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 32
      },
      properties = {},
      terrains = {},
      tilecount = 36,
      tiles = {}
    },
    {
      name = "roof_section(garage)1",
      firstgid = 37,
      filename = "../../../../../game-assets-master/tilesets/roof_section(garage)1.tsx",
      tilewidth = 240,
      tileheight = 160,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/roof_section(garage)1.png",
      imagewidth = 960,
      imageheight = 1920,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 240,
        height = 160
      },
      properties = {},
      terrains = {},
      tilecount = 48,
      tiles = {}
    },
    {
      name = "roof_section(large)_alt1",
      firstgid = 85,
      filename = "../../../../../game-assets-master/tilesets/roof_section(large)_alt1.tsx",
      tilewidth = 320,
      tileheight = 320,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/roof_section(large)_alt1.png",
      imagewidth = 1280,
      imageheight = 1920,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 320,
        height = 320
      },
      properties = {},
      terrains = {},
      tilecount = 24,
      tiles = {}
    },
    {
      name = "roof_section(large)1",
      firstgid = 109,
      filename = "../../../../../game-assets-master/tilesets/roof_section(large)1.tsx",
      tilewidth = 320,
      tileheight = 230,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/roof_section(large)1.png",
      imagewidth = 1280,
      imageheight = 1380,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 320,
        height = 230
      },
      properties = {},
      terrains = {},
      tilecount = 24,
      tiles = {}
    },
    {
      name = "suburb_house_walls1(gray)",
      firstgid = 133,
      filename = "../../../../../game-assets-master/tilesets/suburb_house_walls1(gray).tsx",
      tilewidth = 64,
      tileheight = 162,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/suburb_house_walls1(gray).png",
      imagewidth = 576,
      imageheight = 1458,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 162
      },
      properties = {},
      terrains = {},
      tilecount = 81,
      tiles = {}
    },
    {
      name = "barrels1",
      firstgid = 214,
      filename = "../../../../../game-assets-master/tilesets/barrels1.tsx",
      tilewidth = 64,
      tileheight = 60,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/barrels1.png",
      imagewidth = 448,
      imageheight = 540,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 60
      },
      properties = {},
      terrains = {},
      tilecount = 63,
      tiles = {}
    },
    {
      name = "trees1",
      firstgid = 277,
      filename = "../../../../../game-assets-master/tilesets/trees1.tsx",
      tilewidth = 260,
      tileheight = 300,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/trees1.png",
      imagewidth = 780,
      imageheight = 300,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 260,
        height = 300
      },
      properties = {},
      terrains = {},
      tilecount = 3,
      tiles = {}
    },
    {
      name = "land_and_grass1",
      firstgid = 280,
      filename = "../../../../../game-assets-master/tilesets/land_and_grass1.tsx",
      tilewidth = 64,
      tileheight = 40,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/land_and_grass1.png",
      imagewidth = 256,
      imageheight = 400,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 40
      },
      properties = {},
      terrains = {},
      tilecount = 40,
      tiles = {}
    },
    {
      name = "land_and_grass2",
      firstgid = 320,
      filename = "../../../../../game-assets-master/tilesets/land_and_grass2.tsx",
      tilewidth = 64,
      tileheight = 40,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/land_and_grass2.png",
      imagewidth = 256,
      imageheight = 640,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 40
      },
      properties = {},
      terrains = {},
      tilecount = 64,
      tiles = {}
    },
    {
      name = "bushes1",
      firstgid = 384,
      filename = "../../../../../game-assets-master/tilesets/bushes1.tsx",
      tilewidth = 80,
      tileheight = 70,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/bushes1.png",
      imagewidth = 400,
      imageheight = 140,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 80,
        height = 70
      },
      properties = {},
      terrains = {},
      tilecount = 10,
      tiles = {}
    },
    {
      name = "road_and_grass1",
      firstgid = 394,
      filename = "../../../../../game-assets-master/tilesets/road_and_grass1.tsx",
      tilewidth = 256,
      tileheight = 148,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/road_and_grass1.png",
      imagewidth = 768,
      imageheight = 1184,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 256,
        height = 148
      },
      properties = {},
      terrains = {},
      tilecount = 24,
      tiles = {}
    },
    {
      name = "road1",
      firstgid = 418,
      filename = "../../../../../game-assets-master/tilesets/road1.tsx",
      tilewidth = 256,
      tileheight = 148,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/road1.png",
      imagewidth = 768,
      imageheight = 2664,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 256,
        height = 148
      },
      properties = {},
      terrains = {},
      tilecount = 54,
      tiles = {}
    },
    {
      name = "road_and_grass2",
      firstgid = 472,
      filename = "../../../../../game-assets-master/tilesets/road_and_grass2.tsx",
      tilewidth = 256,
      tileheight = 160,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/road_and_grass2.png",
      imagewidth = 768,
      imageheight = 2560,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 256,
        height = 160
      },
      properties = {},
      terrains = {},
      tilecount = 48,
      tiles = {}
    },
    {
      name = "road_and_grass3",
      firstgid = 520,
      filename = "../../../../../game-assets-master/tilesets/road_and_grass3.tsx",
      tilewidth = 256,
      tileheight = 148,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/road_and_grass3.png",
      imagewidth = 768,
      imageheight = 1776,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 256,
        height = 148
      },
      properties = {},
      terrains = {},
      tilecount = 36,
      tiles = {}
    },
    {
      name = "billboard1",
      firstgid = 556,
      filename = "../../../../../game-assets-master/tilesets/billboard1.tsx",
      tilewidth = 360,
      tileheight = 460,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/billboard1.png",
      imagewidth = 720,
      imageheight = 460,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 360,
        height = 460
      },
      properties = {},
      terrains = {},
      tilecount = 2,
      tiles = {}
    },
    {
      name = "trash_can1",
      firstgid = 558,
      filename = "../../../../../game-assets-master/tilesets/trash_can1.tsx",
      tilewidth = 64,
      tileheight = 80,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/trash_can1.png",
      imagewidth = 448,
      imageheight = 160,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 80
      },
      properties = {},
      terrains = {},
      tilecount = 14,
      tiles = {}
    },
    {
      name = "chevrolet caprice default",
      firstgid = 572,
      filename = "../../../../../game-assets-master/tilesets/chevrolet caprice default.tsx",
      tilewidth = 210,
      tileheight = 130,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/chevrolet caprice default.png",
      imagewidth = 420,
      imageheight = 260,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 210,
        height = 130
      },
      properties = {},
      terrains = {},
      tilecount = 4,
      tiles = {}
    },
    {
      name = "dumbster(small)1",
      firstgid = 576,
      filename = "../../../../../game-assets-master/tilesets/dumbster(small)1.tsx",
      tilewidth = 100,
      tileheight = 120,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/dumbster(small)1.png",
      imagewidth = 500,
      imageheight = 360,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 100,
        height = 120
      },
      properties = {},
      terrains = {},
      tilecount = 15,
      tiles = {}
    },
    {
      name = "block_stairs1",
      firstgid = 591,
      filename = "../../../../../game-assets-master/tilesets/block_stairs1.tsx",
      tilewidth = 64,
      tileheight = 70,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/block_stairs1.png",
      imagewidth = 384,
      imageheight = 280,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 70
      },
      properties = {},
      terrains = {},
      tilecount = 24,
      tiles = {}
    },
    {
      name = "mesh_fence(small)1",
      firstgid = 615,
      filename = "../../../../../game-assets-master/tilesets/mesh_fence(small)1.tsx",
      tilewidth = 64,
      tileheight = 100,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/mesh_fence(small)1.png",
      imagewidth = 448,
      imageheight = 400,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 100
      },
      properties = {},
      terrains = {},
      tilecount = 28,
      tiles = {}
    },
    {
      name = "barrels2",
      firstgid = 643,
      filename = "../../../../../game-assets-master/tilesets/barrels2.tsx",
      tilewidth = 64,
      tileheight = 60,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/barrels2.png",
      imagewidth = 64,
      imageheight = 60,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 60
      },
      properties = {},
      terrains = {},
      tilecount = 1,
      tiles = {}
    },
    {
      name = "wooden_poles1",
      firstgid = 644,
      filename = "../../../../../game-assets-master/tilesets/wooden_poles1.tsx",
      tilewidth = 180,
      tileheight = 350,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/wooden_poles1.png",
      imagewidth = 720,
      imageheight = 1400,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 180,
        height = 350
      },
      properties = {},
      terrains = {},
      tilecount = 16,
      tiles = {}
    },
    {
      name = "suburb_house_handrail4",
      firstgid = 660,
      filename = "../../../../../game-assets-master/tilesets/suburb_house_handrail4.tsx",
      tilewidth = 64,
      tileheight = 160,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/suburb_house_handrail4.png",
      imagewidth = 384,
      imageheight = 640,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 160
      },
      properties = {},
      terrains = {},
      tilecount = 24,
      tiles = {}
    },
    {
      name = "suburb_house_handrail3",
      firstgid = 684,
      filename = "../../../../../game-assets-master/tilesets/suburb_house_handrail.tsx",
      tilewidth = 64,
      tileheight = 160,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/suburb_house_handrail3.png",
      imagewidth = 384,
      imageheight = 1440,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 160
      },
      properties = {},
      terrains = {},
      tilecount = 54,
      tiles = {}
    },
    {
      name = "suburb_house_handrail2",
      firstgid = 738,
      filename = "../../../../../game-assets-master/tilesets/suburb_house_handrail2.tsx",
      tilewidth = 64,
      tileheight = 160,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/suburb_house_handrail2.png",
      imagewidth = 384,
      imageheight = 1440,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 160
      },
      properties = {},
      terrains = {},
      tilecount = 54,
      tiles = {}
    },
    {
      name = "mesh_fence1",
      firstgid = 792,
      filename = "../../../../../game-assets-master/tilesets/mesh_fence1.tsx",
      tilewidth = 64,
      tileheight = 150,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/mesh_fence1.png",
      imagewidth = 448,
      imageheight = 1650,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 150
      },
      properties = {},
      terrains = {},
      tilecount = 77,
      tiles = {}
    },
    {
      name = "roof_section(medium)1",
      firstgid = 869,
      filename = "../../../../../game-assets-master/tilesets/roof_section(medium)1.tsx",
      tilewidth = 360,
      tileheight = 220,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/roof_section(medium)1.png",
      imagewidth = 1440,
      imageheight = 1320,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 360,
        height = 220
      },
      properties = {},
      terrains = {},
      tilecount = 24,
      tiles = {}
    },
    {
      name = "blocks3",
      firstgid = 893,
      filename = "../../../../../game-assets-master/tilesets/blocks3.tsx",
      tilewidth = 64,
      tileheight = 95,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/blocks3.png",
      imagewidth = 384,
      imageheight = 1140,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 95
      },
      properties = {},
      terrains = {},
      tilecount = 72,
      tiles = {}
    },
    {
      name = "blocks1",
      firstgid = 965,
      filename = "../../../../../game-assets-master/tilesets/blocks1.tsx",
      tilewidth = 64,
      tileheight = 95,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/blocks1.png",
      imagewidth = 384,
      imageheight = 855,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 95
      },
      properties = {},
      terrains = {},
      tilecount = 54,
      tiles = {}
    },
    {
      name = "tires1",
      firstgid = 1019,
      filename = "../../../../../game-assets-master/tilesets/tires1.tsx",
      tilewidth = 80,
      tileheight = 90,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/tires1.png",
      imagewidth = 480,
      imageheight = 360,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 80,
        height = 90
      },
      properties = {},
      terrains = {},
      tilecount = 24,
      tiles = {}
    },
    {
      name = "mesh_gates1",
      firstgid = 1043,
      filename = "../../../../../game-assets-master/tilesets/mesh_gates1.tsx",
      tilewidth = 94,
      tileheight = 150,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/mesh_gates1.png",
      imagewidth = 564,
      imageheight = 750,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 94,
        height = 150
      },
      properties = {},
      terrains = {},
      tilecount = 30,
      tiles = {}
    },
    {
      name = "suburb_garage_door1",
      firstgid = 1073,
      filename = "../../../../../game-assets-master/tilesets/suburb_garage_door1.tsx",
      tilewidth = 220,
      tileheight = 220,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/suburb_garage_door1.png",
      imagewidth = 660,
      imageheight = 1320,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 220,
        height = 220
      },
      properties = {},
      terrains = {},
      tilecount = 18,
      tiles = {}
    },
    {
      name = "wooden_fence1",
      firstgid = 1091,
      filename = "../../../../../game-assets-master/tilesets/wooden_fence1.tsx",
      tilewidth = 64,
      tileheight = 140,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/wooden_fence1.png",
      imagewidth = 448,
      imageheight = 1120,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 140
      },
      properties = {},
      terrains = {},
      tilecount = 56,
      tiles = {}
    },
    {
      name = "apartments_interior_walls1",
      firstgid = 1147,
      filename = "../../../../../game-assets-master/tilesets/apartments_interior_walls1.tsx",
      tilewidth = 64,
      tileheight = 129,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/apartments_interior_walls1.png",
      imagewidth = 512,
      imageheight = 2064,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 129
      },
      properties = {},
      terrains = {},
      tilecount = 128,
      tiles = {}
    },
    {
      name = "trash(medium)1",
      firstgid = 1275,
      filename = "../../../../../game-assets-master/tilesets/trash(medium)1.tsx",
      tilewidth = 100,
      tileheight = 80,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/trash(medium)1.png",
      imagewidth = 500,
      imageheight = 240,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 100,
        height = 80
      },
      properties = {},
      terrains = {},
      tilecount = 15,
      tiles = {}
    },
    {
      name = "trees2",
      firstgid = 1290,
      filename = "../../../../../game-assets-master/tilesets/trees2.tsx",
      tilewidth = 260,
      tileheight = 320,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/trees2.png",
      imagewidth = 780,
      imageheight = 960,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 260,
        height = 320
      },
      properties = {},
      terrains = {},
      tilecount = 9,
      tiles = {}
    },
    {
      name = "back_walls(suburb_house)1",
      firstgid = 1299,
      filename = "../../../../../game-assets-master/tilesets/back_walls(suburb_house)1.tsx",
      tilewidth = 64,
      tileheight = 162,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/back_walls(suburb_house)1.png",
      imagewidth = 384,
      imageheight = 162,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 162
      },
      properties = {},
      terrains = {},
      tilecount = 6,
      tiles = {}
    },
    {
      name = "roof_section(large)_alt4",
      firstgid = 1305,
      filename = "../../../../../game-assets-master/tilesets/roof_section(large)_alt4.tsx",
      tilewidth = 480,
      tileheight = 360,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/roof_section(large)_alt4.png",
      imagewidth = 1920,
      imageheight = 1800,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 480,
        height = 360
      },
      properties = {},
      terrains = {},
      tilecount = 20,
      tiles = {}
    },
    {
      name = "roof_section(windows)1",
      firstgid = 1325,
      filename = "../../../../../game-assets-master/tilesets/roof_section(windows)1.tsx",
      tilewidth = 200,
      tileheight = 240,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/roof_section(windows)1.png",
      imagewidth = 800,
      imageheight = 1440,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 200,
        height = 240
      },
      properties = {},
      terrains = {},
      tilecount = 24,
      tiles = {}
    },
    {
      name = "suburb_fence(large)1",
      firstgid = 1349,
      filename = "../../../../../game-assets-master/tilesets/suburb_fence(large)1.tsx",
      tilewidth = 64,
      tileheight = 150,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/suburb_fence(large)1.png",
      imagewidth = 448,
      imageheight = 450,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 150
      },
      properties = {},
      terrains = {},
      tilecount = 21,
      tiles = {}
    },
    {
      name = "street_objects1",
      firstgid = 1370,
      filename = "../../../../../game-assets/tilesets/street_objects1.tsx",
      tilewidth = 64,
      tileheight = 90,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets/tilesets/street_objects1.png",
      imagewidth = 512,
      imageheight = 1080,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 90
      },
      properties = {},
      terrains = {},
      tilecount = 96,
      tiles = {}
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "floor",
      x = 0,
      y = 0,
      width = 30,
      height = 30,
      visible = true,
      opacity = 1,
      disposition = "floor",
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "lua",
      data = {
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
      }
    },
    {
      type = "tilelayer",
      name = "walls",
      x = 0,
      y = 0,
      width = 30,
      height = 30,
      visible = true,
      opacity = 1,
      disposition = "floor",
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1433, 0, 1439, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1432, 0, 1438, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 1427, 0, 0, 1442, 1443, 1444, 1445, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 1426, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1431, 0, 1437, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1370, 1371, 1372, 0, 1373, 0, 0, 1430, 0, 1436, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 1419, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1441, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 1418, 0, 0, 0, 0, 0, 1374, 1375, 1376, 0, 1377, 0, 0, 1429, 0, 1435, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1378, 1379, 0, 1380, 1381, 0, 0, 1428, 0, 1434, 0, 1440, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 1417, 0, 0, 1382, 1383, 0, 1384, 0, 1385, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 1416, 0, 0, 0, 1386, 1387, 0, 1388, 0, 1389, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 1415, 0, 0, 0, 1390, 1391, 0, 1392, 0, 1393, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 1414, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1394, 1395, 0, 1396, 0, 1397, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1398, 1399, 0, 1400, 0, 1401, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1402, 1403, 1404, 0, 1405, 0, 1406, 1407, 1408, 0, 1409, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1410, 1411, 0, 1412, 1413, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "objectgroup",
      name = "navmesh",
      visible = false,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 101,
          name = "rect2",
          type = "",
          shape = "rectangle",
          x = 0,
          y = 264,
          width = 248,
          height = 376,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 106,
          name = "rect5",
          type = "",
          shape = "rectangle",
          x = 248,
          y = 392,
          width = 184,
          height = 248,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 108,
          name = "rect4",
          type = "",
          shape = "rectangle",
          x = 432,
          y = 336,
          width = 240,
          height = 304,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 115,
          name = "rect3",
          type = "",
          shape = "rectangle",
          x = 432,
          y = 264,
          width = 240,
          height = 72,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 117,
          name = "rect0",
          type = "",
          shape = "rectangle",
          x = 0,
          y = 0,
          width = 392,
          height = 248,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 120,
          name = "",
          type = "",
          shape = "rectangle",
          x = 264,
          y = 264,
          width = 80,
          height = 112,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 124,
          name = "",
          type = "",
          shape = "rectangle",
          x = 208,
          y = 336,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 125,
          name = "",
          type = "",
          shape = "rectangle",
          x = 232,
          y = 384,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 126,
          name = "",
          type = "",
          shape = "rectangle",
          x = 240,
          y = 376,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 127,
          name = "",
          type = "",
          shape = "rectangle",
          x = 248,
          y = 384,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 128,
          name = "",
          type = "",
          shape = "rectangle",
          x = 392,
          y = 0,
          width = 280,
          height = 240,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 129,
          name = "",
          type = "",
          shape = "rectangle",
          x = 496,
          y = 232,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 130,
          name = "",
          type = "",
          shape = "rectangle",
          x = 392,
          y = 240,
          width = 280,
          height = 24,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 131,
          name = "",
          type = "",
          shape = "rectangle",
          x = 328,
          y = 376,
          width = 104,
          height = 16,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 133,
          name = "",
          type = "",
          shape = "rectangle",
          x = 344,
          y = 360,
          width = 88,
          height = 16,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 134,
          name = "",
          type = "",
          shape = "rectangle",
          x = 360,
          y = 264,
          width = 72,
          height = 96,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 135,
          name = "",
          type = "",
          shape = "rectangle",
          x = 344,
          y = 264,
          width = 16,
          height = 16,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 136,
          name = "",
          type = "",
          shape = "rectangle",
          x = 248,
          y = 264,
          width = 16,
          height = 16,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 141,
          name = "",
          type = "",
          shape = "rectangle",
          x = 248,
          y = 360,
          width = 16,
          height = 16,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 142,
          name = "",
          type = "",
          shape = "rectangle",
          x = 248,
          y = 360,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 143,
          name = "",
          type = "",
          shape = "rectangle",
          x = 256,
          y = 360,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 144,
          name = "",
          type = "",
          shape = "rectangle",
          x = 256,
          y = 360,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 145,
          name = "",
          type = "",
          shape = "rectangle",
          x = 256,
          y = 368,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 146,
          name = "",
          type = "",
          shape = "rectangle",
          x = 248,
          y = 368,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 151,
          name = "",
          type = "",
          shape = "rectangle",
          x = 328,
          y = 248,
          width = 64,
          height = 16,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 152,
          name = "",
          type = "",
          shape = "rectangle",
          x = 0,
          y = 248,
          width = 248,
          height = 16,
          rotation = 0,
          visible = true,
          properties = {}
        }
      }
    }
  }
}
