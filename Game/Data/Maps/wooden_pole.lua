return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "1.0.2",
  orientation = "isometric",
  renderorder = "right-down",
  width = 11,
  height = 11,
  tilewidth = 64,
  tileheight = 32,
  nextobjectid = 1,
  properties = {},
  tilesets = {
    {
      name = "wooden_poles1",
      firstgid = 1,
      filename = "../../../../../game-assets-master/tilesets/wooden_poles1.tsx",
      tilewidth = 180,
      tileheight = 350,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/wooden_poles1.png",
      imagewidth = 720,
      imageheight = 1400,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 180,
        height = 350
      },
      properties = {},
      terrains = {},
      tilecount = 16,
      tiles = {}
    },
    {
      name = "trees1",
      firstgid = 17,
      filename = "../../../../../game-assets-master/tilesets/trees1.tsx",
      tilewidth = 260,
      tileheight = 300,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/trees1.png",
      imagewidth = 780,
      imageheight = 300,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 260,
        height = 300
      },
      properties = {},
      terrains = {},
      tilecount = 3,
      tiles = {}
    },
    {
      name = "suburb_house_handrail4",
      firstgid = 20,
      filename = "../../../../../game-assets-master/tilesets/suburb_house_handrail4.tsx",
      tilewidth = 64,
      tileheight = 160,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/suburb_house_handrail4.png",
      imagewidth = 384,
      imageheight = 640,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 160
      },
      properties = {},
      terrains = {},
      tilecount = 24,
      tiles = {}
    },
    {
      name = "suburb_house_handrail3",
      firstgid = 44,
      filename = "../../../../../game-assets-master/tilesets/suburb_house_handrail.tsx",
      tilewidth = 64,
      tileheight = 160,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/suburb_house_handrail3.png",
      imagewidth = 384,
      imageheight = 1440,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 160
      },
      properties = {},
      terrains = {},
      tilecount = 54,
      tiles = {}
    },
    {
      name = "mesh_fence(small)1",
      firstgid = 98,
      filename = "../../../../../game-assets-master/tilesets/mesh_fence(small)1.tsx",
      tilewidth = 64,
      tileheight = 100,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/mesh_fence(small)1.png",
      imagewidth = 448,
      imageheight = 400,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 100
      },
      properties = {},
      terrains = {},
      tilecount = 28,
      tiles = {}
    },
    {
      name = "mesh_fence1",
      firstgid = 126,
      filename = "../../../../../game-assets-master/tilesets/mesh_fence1.tsx",
      tilewidth = 64,
      tileheight = 150,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/mesh_fence1.png",
      imagewidth = 448,
      imageheight = 1650,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 150
      },
      properties = {},
      terrains = {},
      tilecount = 77,
      tiles = {}
    },
    {
      name = "blocks3",
      firstgid = 203,
      filename = "../../../../../game-assets-master/tilesets/blocks3.tsx",
      tilewidth = 64,
      tileheight = 95,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/blocks3.png",
      imagewidth = 384,
      imageheight = 1140,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 95
      },
      properties = {},
      terrains = {},
      tilecount = 72,
      tiles = {}
    },
    {
      name = "bushes1",
      firstgid = 275,
      filename = "../../../../../game-assets-master/tilesets/bushes1.tsx",
      tilewidth = 80,
      tileheight = 70,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/bushes1.png",
      imagewidth = 400,
      imageheight = 140,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 80,
        height = 70
      },
      properties = {},
      terrains = {},
      tilecount = 10,
      tiles = {}
    },
    {
      name = "tires1",
      firstgid = 285,
      filename = "../../../../../game-assets-master/tilesets/tires1.tsx",
      tilewidth = 80,
      tileheight = 90,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/tires1.png",
      imagewidth = 480,
      imageheight = 360,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 80,
        height = 90
      },
      properties = {},
      terrains = {},
      tilecount = 24,
      tiles = {}
    },
    {
      name = "suburb_garage_door1",
      firstgid = 309,
      filename = "../../../../../game-assets-master/tilesets/suburb_garage_door1.tsx",
      tilewidth = 220,
      tileheight = 220,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/suburb_garage_door1.png",
      imagewidth = 660,
      imageheight = 1320,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 220,
        height = 220
      },
      properties = {},
      terrains = {},
      tilecount = 18,
      tiles = {}
    },
    {
      name = "wooden_fence1",
      firstgid = 327,
      filename = "../../../../../game-assets-master/tilesets/wooden_fence1.tsx",
      tilewidth = 64,
      tileheight = 140,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/wooden_fence1.png",
      imagewidth = 448,
      imageheight = 1120,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 140
      },
      properties = {},
      terrains = {},
      tilecount = 56,
      tiles = {}
    },
    {
      name = "trash(medium)1",
      firstgid = 383,
      filename = "../../../../../game-assets-master/tilesets/trash(medium)1.tsx",
      tilewidth = 100,
      tileheight = 80,
      spacing = 0,
      margin = 0,
      image = "../../../../../game-assets-master/tilesets/trash(medium)1.png",
      imagewidth = 500,
      imageheight = 240,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 100,
        height = 80
      },
      properties = {},
      terrains = {},
      tilecount = 15,
      tiles = {}
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "Слой тайлов 1",
      x = 0,
      y = 0,
      width = 11,
      height = 11,
      visible = true,
      opacity = 1,
      disposition = "floor",
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 383, 0, 0
      }
    }
  }
}
