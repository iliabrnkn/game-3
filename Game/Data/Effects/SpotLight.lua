return {
	name = "SpotLight",
	vertexshader = 
		[[
		#version 420
		#extension GL_ARB_shading_language_420pack: enable
		#extension ARB_shader_viewport_layer_array: enable

		const int MAX_MESHES = 100;
		const int MAX_DIRECTIONAL_LIGHTS = 64;
		
		in vec3 position; // в случае биллборда здесь хранятся вертексы в локальной системе координат биллборда
		in vec3 normal; // в случае биллборда здесь хранится смещение
		in vec2 texCoord;
		in vec4 boneID;
		in vec4 weights; 
		in int meshID;

		out vec4 fragPos;
		out vec2 fragTexCoord;
		out float fragSinAlpha;
		out float fragCosAlpha;
		out vec3 fragPosToCam;

		flat out int fragLightPointNum;
		flat out int fragMeshID;

		mat4 getMat4FromSamplerBuffer(samplerBuffer buf, int i)
		{
			return mat4(
				texelFetch(buf, i * 4),
				texelFetch(buf, i * 4 + 1),
				texelFetch(buf, i * 4 + 2),
				texelFetch(buf, i * 4 + 3) 
			);
		}

		// ----

		uniform samplerBuffer boneMxBuffer;
		uniform samplerBuffer mvpMXs;

		uniform int lightPointNum;


		// ----

		struct LightProps {
			vec4 pos;
			vec3 color;
			float radius;
			int dynamicIndex;
			int staticIndex;
    	};

    	struct MeshProps {
			mat4 transformMx;
			vec4 material;
			int boneMxOffset;
			float padding1;
			float padding2;
			float padding3;
    	};
		
		layout(std140, binding = 0) uniform lightsBlock
		{
			LightProps lightProps[MAX_DIRECTIONAL_LIGHTS];
		};

		layout(std140, binding = 1) uniform meshBlock
		{
			MeshProps meshProps[MAX_MESHES];
		};

		// ----

		void main()
		{
			vec4 vertPos = vec4(position, 1.f);
			gl_Layer = lightPointNum;

			if (meshID >= 0) // если это не геометрия уровня
			{
				if (meshProps[meshID].boneMxOffset != -1) // если это персонаж
				{
					mat4 boneTransform = mat4(0.f);

					for (int i = 0; i < 3; i++)
					{
						if (boneID[i] != -1.f)
						{
							boneTransform += getMat4FromSamplerBuffer(
								boneMxBuffer, 
								meshProps[meshID].boneMxOffset + int(boneID[i])
							) * weights[i];
						}
					}

					vertPos = boneTransform * vertPos;
				}

				vertPos = meshProps[meshID].transformMx * vertPos;
			}

			gl_Position = getMat4FromSamplerBuffer(mvpMXs, lightPointNum) * vertPos;
			fragPos = vertPos;
			fragTexCoord = texCoord;
			fragMeshID = meshID;

			fragLightPointNum = lightPointNum;

		}]],
	fragmentshader = 
		[[
		#version 330
		#extension GL_ARB_shading_language_420pack: enable

		const int MAX_DIRECTIONAL_LIGHTS = 64;

		in vec4 fragPos;
		in vec2 fragTexCoord;
		in vec3 fragPosToCam;
		in float fragSinAlpha;
		in float fragCosAlpha;

		flat in int fragLightPointNum;
		flat in int fragMeshID;

		struct LightProps {
			vec4 pos;
			vec3 color;
			float radius;
			int dynamicIndex;
			int staticIndex;
    	};
		
		layout(std140, binding = 0) uniform lightsBlock
		{
			LightProps lightProps[MAX_DIRECTIONAL_LIGHTS];
		};

		uniform sampler2D shadowMask;

		void main(){
			gl_FragDepth = length(fragPos.xyz - lightProps[fragLightPointNum].pos.xyz) / lightProps[fragLightPointNum].radius;
		}]],
	attriblocations = {
		{
			name = "position",
			location = 0
		},
		{
			name = "normal",
			location = 1
		},
		{
			name = "texCoord",
			location = 2
		},
		{
			name = "boneID",
			location = 3
		},
		{
			name = "weights",
			location = 4
		},
		{
			name = "meshID",
			location = 5
		}
	}
}