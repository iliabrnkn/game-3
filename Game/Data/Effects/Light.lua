--[[
ПАМЯТКА
текстурные координаты биллбордов -3
текстурные координаты заборчиков -5
]]--

return {
	name = "Light",
	vertexshader = 
		[[
		#version 420
		#extension GL_ARB_shading_language_420pack: enable
		#extension ARB_shader_viewport_layer_array: enable

		const int MAX_MESHES = 100;
		const int MAX_BONES_PER_MODEL = 100;

		const int LIGHT_MAX_STATIC_ONMILIGHTS = 32;
		const int LIGHT_MAX_DYNAMIC_ONMILIGHTS = 32;
		const int LIGHT_POINTS_COUNT_PER_INSTANCE = 5;
		const vec3 viewerNormal = vec3(sqrt(2.f) / 4.f, sqrt(3.f) / 2.f, sqrt(2.f) / 4.f);
		
		in vec3 position; // в случае биллборда здесь хранятся вертексы в локальной системе координат биллборда
		in vec3 normal; // в случае биллборда здесь хранится смещение
		in vec2 texCoord;
		in vec4 boneID;
		in vec4 weights; 
		in int meshID;

		out vec4 fragPos;
		out vec2 fragTexCoord;
		out vec3 fragNormal;
		out vec3 fragViewerNormal;

		flat out int fragLightPointNum;
		flat out int fragMeshID;

		mat4 getMat4FromSamplerBuffer(samplerBuffer buf, int i)
		{
			return mat4(
				texelFetch(buf, i * 4),
				texelFetch(buf, i * 4 + 1),
				texelFetch(buf, i * 4 + 2),
				texelFetch(buf, i * 4 + 3) 
			);
		}

		// ----

		uniform samplerBuffer boneMxBuffer;
		uniform samplerBuffer dynamicShadowMXs;
		uniform int lightPointNum;

		// ----

		struct LightProps {
			vec4 pos;
			vec3 color;
			float radius;
			int dynamicIndex;
			int staticIndex;
    	};

    	struct MeshProps {
			mat4 transformMx;
			vec4 material;
			int boneMxOffset;
			float padding1;
			float padding2;
			float padding3;
    	};
		
		layout(std140, binding = 0) uniform lightsBlock
		{
			LightProps lightProps[LIGHT_MAX_STATIC_ONMILIGHTS + LIGHT_MAX_DYNAMIC_ONMILIGHTS];
		};

		layout(std140, binding = 1) uniform meshBlock
		{
			MeshProps meshProps[MAX_MESHES];
		};

		// ----

		void main()
		{	
			fragLightPointNum = lightPointNum;
			gl_Layer = lightPointNum * 6 + gl_InstanceID % 6;

			vec4 vertPos = vec4(position, 1.f);

			if (meshID >= 0) // если это не геометрия уровня
			{
				if (meshProps[meshID].boneMxOffset != -1) // если это персонаж
				{
					mat4 boneTransform = mat4(0.f);

					for (int i = 0; i < 3; i++)
					{
						if (boneID[i] != -1.f)
						{
							boneTransform += getMat4FromSamplerBuffer(
								boneMxBuffer, 
								meshProps[meshID].boneMxOffset + int(boneID[i])
							) * weights[i];
						}
					}

					vertPos = boneTransform * vertPos;
				}

				vertPos = meshProps[meshID].transformMx * vertPos;
			}

			mat4 mvp = getMat4FromSamplerBuffer(dynamicShadowMXs, gl_Layer);

			gl_Position = mvp * vertPos;

			fragPos = vertPos;
			fragTexCoord = texCoord;
			fragMeshID = meshID;
			fragNormal = normal.xyz;
			fragViewerNormal = vec3(mvp * vec4(viewerNormal, 0.f));

		}]],
	fragmentshader = 
		[[
		#version 420
		#extension GL_ARB_shading_language_420pack: enable

    	const int LIGHT_MAX_STATIC_ONMILIGHTS = 32;
		const int LIGHT_MAX_DYNAMIC_ONMILIGHTS = 32;
		const float COLOR_VOLUME_STEP = 0.1;

		const int REGULAR_TILE_OBJECT_ID = -1;
		const int BILLBOARD_VERTEX_OBJECT_ID = -2;
		const int FENCE_VERTEX_OBJECT_ID = -3;
		const int FENCE_BACKWARDS_VERTEX_OBJECT_ID = -4;

		const int MAX_GID_COUNT = 4096;

		uniform sampler2DArray spriteAtlasA;
		uniform sampler2DArray spriteAtlasB;
		uniform sampler2DArray spriteAtlasC;
		uniform sampler2DArray spriteAtlasD;

		in vec4 fragPos;
		in vec2 fragTexCoord;
		in vec3 fragNormal;
		in vec3 fragViewerNormal;
		
		flat in int fragLightPointNum;
		flat in int fragMeshID;

		struct LightProps {
			vec4 pos;
			vec3 color;
			float radius;
			int dynamicIndex;
			int staticIndex;
    	};
		
		layout(std140, binding = 0) uniform lightsBlock
		{
			LightProps lightProps[LIGHT_MAX_STATIC_ONMILIGHTS + LIGHT_MAX_DYNAMIC_ONMILIGHTS];
		};

		struct GidProps
		{
			float layerNum;
			float atlasNum;
			vec2 spriteSizeTextureOffsetFactor;
		};

		vec4 sampleAtlas(GidProps gidProps, vec2 texCoord)
		{
			vec4 res = vec4(0.0);

			if (gidProps.atlasNum == 0.f)
			{
				res = texture(spriteAtlasA, vec3(texCoord, gidProps.layerNum));
			}
			else if (gidProps.atlasNum == 1.0)
			{
				res = texture(spriteAtlasB, vec3(texCoord, gidProps.layerNum));
			}
			else if (gidProps.atlasNum == 2.0)
			{
				res = texture(spriteAtlasC, vec3(texCoord, gidProps.layerNum));
			}
			else if (gidProps.atlasNum == 3.0)
			{
				res = texture(spriteAtlasD, vec3(texCoord, gidProps.layerNum));
			}

			return res;
		}

		layout(std140, binding = 2) uniform gidBlock
		{
			GidProps gidProps[MAX_GID_COUNT];
		};

		void main(){

			// смещение текстуры для того, чтобы избавиться от просветов по краям
			vec2 texOffsetFactor = vec2(
				mix(0.f, gidProps[-fragMeshID].spriteSizeTextureOffsetFactor.x, dot(fragNormal, normalize(vec3(1.f, 0.f, -1.f)))),
				mix(0.f, gidProps[-fragMeshID].spriteSizeTextureOffsetFactor.y, dot(fragNormal, normalize(vec3(0.f, 1.f, 0.f))))
			);

			if (fragMeshID < 0 &&
				( 
					sampleAtlas(gidProps[-fragMeshID], fragTexCoord + texOffsetFactor).a == 0.f
				)
			)
			{
				discard;
			}

			gl_FragDepth = length(fragPos.xyz - lightProps[fragLightPointNum].pos.xyz) / (lightProps[fragLightPointNum].radius);
		}]],
	attriblocations = {
		{
			name = "position",
			location = 0
		},
		{
			name = "normal",
			location = 1
		},
		{
			name = "texCoord",
			location = 2
		},
		{
			name = "boneID",
			location = 3
		},
		{
			name = "weights",
			location = 4
		},
		{
			name = "meshID",
			location = 5
		}
	}
}