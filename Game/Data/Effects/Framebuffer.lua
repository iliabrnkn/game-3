return {
	name = "Framebuffer",
	vertexshader = 
		[[
		#version 420
		
		in vec3 position;
		in vec2 texCoord;

		uniform mat4 projectionMatrix;
		out vec2 texCoordFrag;

		void main()
		{
			texCoordFrag = texCoord;

			gl_Position = projectionMatrix * vec4(position, 1.0);
		}]],
	fragmentshader = 
		[[
		#version 420
		#extension GL_ARB_texture_cube_map_array: enable
		
		const int MILD_BORDER_SPAN = 100;
		const int LIGHT_MAX_DYNAMIC_OMNILIGHTS = 32;
		const int LIGHT_MAX_STATIC_OMNILIGHTS = 32;
		const int LIGHT_MAX_DYNAMIC_SPOTLIGHTS = 32;
		const int LIGHT_MAX_STATIC_SPOTLIGHTS = 32;
		const float PI = 3.14159265;
		const float PI_OVER_2 = 1.57079633;
		const float LIGHT_AMBIENT_FADE_COEF = 3.0;
		const float LIGHT_NORMAL_OFFSET = 0.1;

		const bool SHADOW_PCF = false;
		const float SHADOW_SAMPLES = 4.0;
		const float SHADOW_OFFSET = 0.02;
		const float SHADOW_BIAS = 0.01;

		const vec2 FRAME_OFFSET = vec2(1.0 / 640.0, 1.0 / 400.0);
		const float MIX_OFFSET = 0.1;
		const vec3 FLOOR_POS_OFFSET = vec3(0.04, 0.04, 0.0); // съезд координат пола, нужен, чтобы освещение было pixel perfect

		const float OFFSET_WALL_BY_NORMAL = 0.04f;

		const ivec3 GUI_COLOR_BRIGHTEST = ivec3(49, 138, 189);
		const ivec3 GUI_COLOR_BRIGHT = ivec3(35, 99, 135);
		const ivec3 GUI_COLOR_MEDIUM = ivec3(24, 67, 91);
		const ivec3 GUI_COLOR_DIM = ivec3(16, 45, 61);
		const ivec3 GUI_COLOR_BACKGROUND = ivec3(23, 23, 23);

		const float BLUR_STEP = 5.0;
		const float DIFFUSE_BRIGHTNESS = 0.2;
		const vec3 eyeDir = vec3(sqrt(2.f) / 4.f, sqrt(2.f) / 4.f, sqrt(3.f) / 2.f);
		const float SHININESS = 1.5;

		const int LIGHT_TYPE_SPOT = 2;
		const int LIGHT_TYPE_OMNI = 1;

		const int PCF_DEPTH = 1;

		uniform sampler2D colorTex;
		uniform sampler2D posTex;
		uniform sampler2D normalTex;
		uniform sampler2D normalTex3D;
		uniform sampler2D guiTex;

		uniform samplerCubeArrayShadow dynamicShadowMap;
		uniform samplerCubeArrayShadow staticShadowMap;
		uniform sampler2DArrayShadow  spotLightDynamicShadowMap;
		uniform sampler2DArrayShadow  spotLightStaticShadowMap;
		uniform sampler2DShadow directionalShadowMap;

		uniform samplerBuffer spotLightMVPMXs;
		
		uniform sampler2D bloomTex;
		uniform sampler2D particleTex;
		uniform sampler2D cloudMaskTex;
		
		uniform vec2 offsetFromTop;
		uniform vec2 cameraOffset;
		uniform vec3 ambienceLight;
		uniform int omniLightsCount;
		uniform int spotLightsCount;
		uniform int blooming;

		uniform vec3 directionalLightColor;
		uniform mat4 directionalLightMVP;

		uniform vec3 lightPos;
		uniform float farPlane;
		uniform float gamma;

		in vec2 texCoordFrag;

		out vec4 fragColor;

		struct LightProps {
			vec4 pos;
			vec3 color;
			float radius;
			int dynamicIndex;
			int staticIndex;
    	};

    	mat4 getMat4FromSamplerBuffer(samplerBuffer buf, int i)
		{
			return mat4(
				texelFetch(buf, i * 4),
				texelFetch(buf, i * 4 + 1),
				texelFetch(buf, i * 4 + 2),
				texelFetch(buf, i * 4 + 3) 
			);
		}
		
		layout(std140, binding = 0) uniform omniLightBlock
		{
			LightProps omniLightProps[LIGHT_MAX_DYNAMIC_OMNILIGHTS + LIGHT_MAX_STATIC_OMNILIGHTS];
		};

		layout(std140, binding = 1) uniform spotLightBlock
		{
			LightProps spotLightProps[LIGHT_MAX_DYNAMIC_SPOTLIGHTS + LIGHT_MAX_STATIC_SPOTLIGHTS];
		};

		float getDirectionalLightShadowDepth(vec3 fragPos, vec3 normal)
		{	
			for (int i = -PCF_DEPTH / 2; i < PCF_DEPTH / 2 + 1; ++i)
			{
				for (int j = -PCF_DEPTH / 2; j < PCF_DEPTH / 2 + 1; ++j)
				{
					for (int k = -PCF_DEPTH / 2; k < PCF_DEPTH / 2 + 1; ++k)
					{
						vec3 bias = vec3(float(i) * SHADOW_BIAS, float(j) * SHADOW_BIAS, float(k) * SHADOW_BIAS);
						vec4 fragPosLightSpace = directionalLightMVP * vec4(fragPos + bias, 1.f);

						if (fragPosLightSpace.z >= -1.f && fragPosLightSpace.z <= 1.f &&
							fragPosLightSpace.x >= -1.f && fragPosLightSpace.x <= 1.f &&
							fragPosLightSpace.y >= -1.f && fragPosLightSpace.y <= 1.f)
						{
							return texture(directionalShadowMap, vec3(
								0.5f + fragPosLightSpace.x / 2.f, 
								0.5f + fragPosLightSpace.y / 2.f,
								0.5f + fragPosLightSpace.z / 2.f - SHADOW_BIAS * 0.1));
						}
						else
							return 0.f;
					}
				}
			}
		}

		float getShadowDepth(vec3 fragPos, vec3 normal, int lightPointNum, int lightType)
		{
			float shadow = 0.f;

			// глубина прохода динамического освещения
			if (lightType == LIGHT_TYPE_SPOT)
			{
				vec3 lightPos = spotLightProps[lightPointNum].pos.xyz;
				float lightRadius = spotLightProps[lightPointNum].radius;
				int lightIndex = spotLightProps[lightPointNum].dynamicIndex;

				vec3 fragToLight = fragPos - lightPos;

				float ref = length(fragToLight) / lightRadius;

				mat4 lightMVP = getMat4FromSamplerBuffer(spotLightMVPMXs, lightPointNum);
				vec4 fragPosLightSpace = lightMVP * vec4(fragPos, 1.f);
				fragPosLightSpace = fragPosLightSpace / fragPosLightSpace.w;

				if (fragPosLightSpace.z >= 0.f && fragPosLightSpace.z <= 1.f &&
					fragPosLightSpace.x >= -1.f && fragPosLightSpace.x <= 1.f &&
					fragPosLightSpace.y >= -1.f && fragPosLightSpace.y <= 1.f &&
					sqrt(pow(fragPosLightSpace.x, 2) + pow(fragPosLightSpace.y, 2)) <= 1.f)
				{
					shadow = (1.f - length(fragPosLightSpace.xy)) *
						texture(spotLightDynamicShadowMap, vec4((vec2(1.f) + fragPosLightSpace.xy) / 2.f, lightIndex, ref));
				}
			}
			else
			{
				vec3 lightPos = omniLightProps[lightPointNum].pos.xyz;
				float lightRadius = omniLightProps[lightPointNum].radius;
				int lightIndex = omniLightProps[lightPointNum].dynamicIndex;

				vec3 fragToLight = fragPos - lightPos;

				float ref = length(fragToLight) / lightRadius;

				for (int i = -PCF_DEPTH / 2; i < PCF_DEPTH / 2 + 1; ++i)
				{
					for (int j = -PCF_DEPTH / 2; j < PCF_DEPTH / 2 + 1; ++j)
					{
						for (int k = -PCF_DEPTH / 2; k < PCF_DEPTH / 2 + 1; ++k)
						{
							vec3 bias = vec3(float(i) * SHADOW_BIAS, float(j) * SHADOW_BIAS, float(k) * SHADOW_BIAS);
							bias += (normal - normalize(fragToLight)) * mix(SHADOW_BIAS, 11.f * SHADOW_BIAS, length(fragToLight)/length(lightRadius));
							ref = length(fragToLight + bias) / lightRadius;

							float shadowIncrement = texture(dynamicShadowMap, vec4(fragToLight + bias, lightIndex), ref);

							if (omniLightProps[lightPointNum].staticIndex != -1)
							{
								shadowIncrement = min(shadowIncrement, 
									texture(staticShadowMap, vec4(fragToLight + bias, omniLightProps[lightPointNum].staticIndex), ref));
							}

							shadow += shadowIncrement;
						}
					}
				}

				shadow /=  float(pow(PCF_DEPTH + 1, 3));
			}

			return shadow;
		}

		float GetBrightness(vec3 fragPos, vec3 normalVal3D, int lightPointNum, int lightType)
		{
			float brightness = 0.0;
			vec3 lightPos = omniLightProps[lightPointNum].pos.xyz;
			vec3 tangent = fragPos - lightPos;
			float lightRadius = omniLightProps[lightPointNum].radius;

			if (length(tangent) <= lightRadius) //- ВАЖНО
			{
				float lightTangentCos = max(dot(normalize(-tangent), normalize(normalVal3D)), 0.0);
				//float eyeNormalCos = dot(normalVal3D, eyeDir);
				float lightBrightness = 0.1 * pow(length(lightRadius), 2) / pow(length(tangent), 2);

				//float lightBrightness = exp(-pow(length(tangent), 2) / pow(lightRadius, 2));
				//float lightBrightness = exp(-length(tangent) / lightRadius);
				
				brightness += 
					(lightBrightness * lightTangentCos) * getShadowDepth(fragPos, normalVal3D, lightPointNum, lightType);
			}

			return brightness;
		}

		float GetBrightnessGodRays(vec3 fragPos, vec3 normalVal3D, int lightPointNum, float stepLen, int lightType)
		{
			float brightness = 0.0;
			vec3 step = vec3(stepLen / (2.f * sqrt(2.f)), stepLen * sqrt(3.f) / 2.f, stepLen / (2.f * sqrt(2.f)));
			
			// для объемных лучей начинаем с пройденного первого шага 
			vec3 currFragPos = fragPos + step;
			vec3 lightPos = spotLightProps[lightPointNum].pos.xyz;
			vec3 tangent = currFragPos - lightPos;
			float lightRadius = spotLightProps[lightPointNum].radius; 

			while (length(tangent) <= lightRadius)
			/*for (int i = 0; i < 100; ++i)*/
			{
				float posBrightness =  0.1 * pow(length(lightRadius), 2) / pow(length(tangent), 2);

				brightness += posBrightness * getShadowDepth(currFragPos, normalVal3D, lightPointNum, lightType);
				currFragPos += step;
				tangent = currFragPos - lightPos;
			}

			return brightness;
		}

		vec4 vhsEffect(sampler2D tex, vec2 texCoord, float offset)
		{
			vec4 colOriginal = texture(tex, texCoord);
			vec4 colRight = texture(tex, texCoord + vec2(FRAME_OFFSET.x * offset, 0.f));
			vec4 colLeft = texture(tex, texCoord + vec2(FRAME_OFFSET.x * offset, 0.f));

			return vec4(colOriginal.r, colRight.g, colLeft.b, colOriginal.w);
		}

		void main() {
		
			vec4 colorVal = texture(colorTex, texCoordFrag);
			/*colorVal.r = pow(colorVal.r, gamma);
			colorVal.g = pow(colorVal.g, gamma);
			colorVal.b = pow(colorVal.b, gamma);*/

			vec4 shadowColor = vec4(colorVal.x * ambienceLight.x, colorVal.y * ambienceLight.y, colorVal.z * ambienceLight.z, colorVal.w);
			vec4 posVal = vec4(texture(posTex, texCoordFrag));
			vec3 normalVal3D = vec3(texture(normalTex3D, texCoordFrag));
			vec3 fragPos = posVal.xyz;
			vec3 resultColor = colorVal.xyz; // сначала это просто значение буфера цвета
			vec3 lightColor = vec3(0.0);

			// освещение
			// пока оставил для направленных источников только боголучи, а для точечных - обычное освещение
			for (int i = 0; i < omniLightsCount; ++i)
			{
				float brightness = GetBrightness(fragPos, normalVal3D, i, LIGHT_TYPE_OMNI);
				//float godRaysBrightness = GetBrightnessGodRays(fragPos, normalVal3D, i, 0.05, LIGHT_TYPE_OMNI);
				lightColor += min(brightness, 1.0) * omniLightProps[i].color;
			}

			for (int i = 0; i < spotLightsCount; ++i)
			{
				//float brightness = GetBrightness(fragPos, normalVal3D, i, LIGHT_TYPE_SPOT);
				float godRaysBrightness = GetBrightnessGodRays(fragPos, normalVal3D, i, 0.1, LIGHT_TYPE_SPOT);
				lightColor += godRaysBrightness * spotLightProps[i].color;
			}

			lightColor += getDirectionalLightShadowDepth(fragPos, normalVal3D) * directionalLightColor *
				(1.f - texture(cloudMaskTex, texCoordFrag).a);

			resultColor = vec3(max(shadowColor.x, lightColor.x) * resultColor.x,
							   max(shadowColor.y, lightColor.y) * resultColor.y, 
							   max(shadowColor.z, lightColor.z) * resultColor.z);

			vec4 particleComponent = texture(particleTex, texCoordFrag);
			vec4 bloomComponent = texture(bloomTex, texCoordFrag);
			vec4 bloom = mix(particleComponent, bloomComponent, max(particleComponent.a, bloomComponent.a));
			
			fragColor = vec4(resultColor + particleComponent.xyz * particleComponent.a + bloomComponent.xyz * bloomComponent.a, 1.f);
			
			//vec4 fragPosLightSpace = directionalLightMVP * vec4(fragPos, 1.f);
			//fragColor = vec4(vec3(0.5 + fragPosLightSpace.z / 2.f), 1.f);
			//fragColor = vec4(vec3(getDirectionalLightShadowDepth(fragPos, normalVal3D)), 1.f);
			//fragColor = colorVal;
			//fragColor = texture(cloudMaskTex, texCoordFrag);
		}]],
	attriblocations = {
		{
			name = "position",
			location = 0
		},
		{
			name = "texCoord",
			location = 1
		}
	}
}