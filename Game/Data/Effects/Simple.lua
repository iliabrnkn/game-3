return {
	name = "Simple",
	vertexshader = 
		[[
		#version 420
		
		uniform mat4 mvp;

		const float TINY_Z_OFFSET = -0.001f;

		in vec3 position;
		in vec3 color; // сюда засунул цвет
		in float boneID;

		out vec4 fragColor;
		
		void main()
		{
			gl_Position = mvp * vec4(position.x, position.y, position.z, 1.f);
			gl_Position.z += TINY_Z_OFFSET;

			if (color == vec3(0.f))
				fragColor = vec4(0.f);
			else
				fragColor = vec4(color.x, color.y, color.z, 1.f);

			//fragColor = vec4(-vec3(gl_Position.z - 1.f) / 2.f, 1.f);

		}]],
	fragmentshader = 
		[[
		#version 420
		
		in vec4 fragColor;
		
		layout(location = 0) out vec4 outColor;
		
		void main(){
			outColor = fragColor;
		}]],
	attriblocations = {
		{
			name = "position",
			location = 0
		},
		{
			name = "color",
			location = 1
		},
		{
			name = "boneID",
			location = 2
		}
	}
}