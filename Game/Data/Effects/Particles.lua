return {
	name = "Particles",
	vertexshader = 
		[[
		#version 420

		const int MAX_EMITTERS = 200;

		in vec3 position;
		in float lifeTime;
		in vec2 startTexCoord; // (-1.f, -1.f) - без текстур
		in vec3 startVelocity;
		in vec3 endVelocity;
		in int emitterNum;
		
		out vec4 fragColor;
		out vec2 fragStartTexCoords;
		out float fragTemperature;
		out vec2 fragSingleParticleTexRatio;
		flat out int fragBlurred;
		flat out int fragOriginal;

		uniform mat4 mvp;

		struct EmitterProps {
			vec4 startColor;
			vec4 endColor;
			vec2 singleParticleTexRatio; // размер тайла текстуры частицы пропорционально размеру текстуры
			float startParticleSize;
			float endParticleSize;
			float maxLifeTime;
			int blurred;
			int original;
			float padding;
    	};

		layout(std140, binding = 0) uniform emitterBlock
		{
			EmitterProps emitterProps[MAX_EMITTERS];
		};

		void main()
		{
			EmitterProps currentEmitter = emitterProps[emitterNum];

			fragTemperature = lifeTime / currentEmitter.maxLifeTime;
			fragStartTexCoords = startTexCoord;
			fragColor = mix(currentEmitter.startColor, currentEmitter.endColor, fragTemperature);
			fragSingleParticleTexRatio = currentEmitter.singleParticleTexRatio; 
			gl_PointSize = mix(currentEmitter.startParticleSize, currentEmitter.endParticleSize, fragTemperature);
			gl_Position = mvp * vec4(position, 1.f);
			fragBlurred = currentEmitter.blurred;
			fragOriginal = currentEmitter.original;
		}]],
	fragmentshader = 
		[[
		#version 420

		const float FOG_PARTICLE_TYPES = 4.f;

		in vec4 fragColor;
		in vec2 fragStartTexCoords;
		in float fragTemperature;
		in vec2 fragSingleParticleTexRatio;
		flat in int fragBlurred;
		flat in int fragOriginal;


		uniform sampler2D particleTex;

		layout(location = 0) out vec4 outParticles;
		layout(location = 1) out vec4 outBlurred;
		
		void main(){
			
			vec4 saturation = vec4(1.f);

			if (fragStartTexCoords != vec2(-1.f, -1.f))
			{
				saturation = texture(particleTex, 
					vec2(
						fragStartTexCoords.x + gl_PointCoord.x * fragSingleParticleTexRatio.x, 
						fragStartTexCoords.y + gl_PointCoord.y * fragSingleParticleTexRatio.y
					)
				);
			}

			if (saturation.a == 0.f)
				discard;

			vec4 resCol;

			if (fragStartTexCoords != vec2(-1.f, -1.f))
			{
				resCol = vec4(
					saturation.xyz * fragColor.xyz, 
					0.05 * exp(-gl_PointCoord.y) * max(fragColor.a * saturation.a / (max(pow((fragTemperature - 0.5), 2), 0.1f)), 0.4f)
				);
			}
			else
			{
				resCol = fragColor;
			}

			if (fragBlurred == 1)
				outBlurred = resCol;
			if (fragOriginal == 1)
				outParticles = resCol;
		}]],
	attriblocations = {
		{
			name = "position",
			location = 0
		},
		{
			name = "lifeTime",
			location = 1
		},
		{
			name = "startTexCoord",
			location = 2
		},
		{
			name = "startVelocity",
			location = 3
		},
		{
			name = "endVelocity",
			location = 4
		},
		{
			name = "emitterNum",
			location = 5
		}
	}
}