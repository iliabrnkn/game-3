return {
	name = "StaticLight",
	vertexshader = 
		[[
		#version 420
		#extension GL_ARB_shading_language_420pack: enable

		const int MAX_MESHES = 100;
		const int MAX_BONES_PER_MODEL = 100;
		
		in vec3 position;
		in vec3 normal;
		in vec2 texCoord;
		in vec4 bonesIdx;
		in vec4 weights; 
		in int meshID;

		out vec4 geoVec;
		out vec2 geoTexCoord;
		out vec3 geoNormal;
		flat out int geoMeshID;

		void main()
		{
			mat4 boneTransform = mat4(0.0);
			
			if (meshID == -1)
			{
				gl_Position = vec4(position, 1.0);
			}

			geoMeshID = meshID;
			geoVec = gl_Position;
			geoTexCoord = texCoord;
			geoNormal = normal; 

		}]],
	geometryshader = 
		[[
		#version 420
		#extension GL_ARB_shading_language_420pack: enable
		#extension GL_ARB_gpu_shader5: enable

		const int LIGHT_MAX_STATIC_ONMILIGHTS = 32;
		const int LIGHT_MAX_DYNAMIC_ONMILIGHTS = 32;
		const int LIGHT_POINTS_COUNT_PER_INSTANCE = 10;

		struct LightProps {
			vec4 pos;
			vec3 color;
			float radius;
			int dynamicIndex;
			int staticIndex;
    	};

		uniform int lightsCount;

		layout (triangles) in;
		layout (triangle_strip, max_vertices = 180) out;
		layout (invocations = 32) in;

		in vec2 geoTexCoord[3];
		in vec4 geoVec[3];
		in vec3 geoNormal[3];
		flat in int geoMeshID[3];

		flat out int lightPointNum;
		out vec4 fragPos;
		out vec2 fragTexCoord;

		layout(std140, binding = 0) uniform lightsBlock
		{
			LightProps lightProps[LIGHT_MAX_STATIC_ONMILIGHTS + LIGHT_MAX_DYNAMIC_ONMILIGHTS];
		};

		uniform samplerBuffer staticShadowMXs;
		uniform samplerBuffer projectionMXs;

		mat4 getMat4FromSamplerBuffer(samplerBuffer buf, int i)
		{
			return mat4(
				texelFetch(buf, i * 4),
				texelFetch(buf, i * 4 + 1),
				texelFetch(buf, i * 4 + 2),
				texelFetch(buf, i * 4 + 3) 
			);
		}

		void main()
		{
			// за одно инстанцирование проходимся по LIGHT_POINTS_COUNT_PER_INSTANCE статических источников света
			int currInstanceStartLightNum = gl_InvocationID * LIGHT_POINTS_COUNT_PER_INSTANCE;

			if (currInstanceStartLightNum < lightsCount)
			{
				if (geoMeshID[0] == -1 && currInstanceStartLightNum < lightsCount)
				{
					// проходимся от источника света номер currInstanceStartLightNum до источника света номер currInstanceLastLightNum
					int currInstanceLastLightNum = min(lightsCount, (gl_InvocationID + 1) * LIGHT_POINTS_COUNT_PER_INSTANCE) - 1;

					for (int i = currInstanceStartLightNum; i <= currInstanceLastLightNum; ++i)
					{
						if (lightProps[i].staticIndex != -1)
						{
							lightPointNum = i;

							for (int face = 0; face < 6; ++face)
							{
								gl_Layer = face + lightProps[i].staticIndex * 6;

								for (int j = 0; j < 3; ++j)
								{
									fragPos = geoVec[j];

									mat4 modelView = getMat4FromSamplerBuffer(staticShadowMXs, face + lightProps[i].staticIndex * 6);

									// если это биллборд, то текстурные координаты у него со знаком "минус"
									if (geoTexCoord[0].x < 0 && geoTexCoord[0].y < 0)
									{
										modelView[0][0] = 1.f; 
										modelView[0][1] = 0.f; 
										modelView[0][2] = 0.f; 

										modelView[2][0] = 0.f; 
										modelView[2][1] = 0.f; 
										modelView[2][2] = 1.f; 

										// если это биллборд, то смещение в системе координат уровня у него проставлено в переменной normal
										modelView[3][0]	= geoNormal[0].x; 
										modelView[3][1]	= geoNormal[0].y; 
										modelView[3][2]	= geoNormal[0].z;
									}

									gl_Position = getMat4FromSamplerBuffer(projectionMXs, lightPointNum) * modelView * geoVec[j];
									fragTexCoord = geoTexCoord[j];

									EmitVertex();
								}

								EndPrimitive();
							}
						}
					}
				}
			}
		}

		]],
	fragmentshader = 
		[[
		#version 330
		#extension GL_ARB_shading_language_420pack: enable

		// debug

		uniform float farPlane;
		uniform vec3 lightPos;
		uniform sampler2D shadowMask;

		const int LIGHT_MAX_STATIC_ONMILIGHTS = 32;
		const int LIGHT_MAX_DYNAMIC_ONMILIGHTS = 32;

		struct LightProps {
			vec4 pos;
			vec3 color;
			float radius;
			int dynamicIndex;
			int staticIndex;
    	};

		in vec4 fragPos;
		in vec2 fragTexCoord;
		flat in int lightPointNum;
		
		layout(std140, binding = 0) uniform lightsBlock
		{
			LightProps lightProps[LIGHT_MAX_STATIC_ONMILIGHTS + LIGHT_MAX_DYNAMIC_ONMILIGHTS];
		};

		void main(){
			float lightDistance = length(fragPos.xyz - lightProps[lightPointNum].pos.xyz) / lightProps[lightPointNum].radius;
			
			if (fragTexCoord.x < -1.f && fragTexCoord.y < -1.f) // если это биллборд с альфа-каналом
			{
				float maskAlpha = texture(shadowMask, fragTexCoord + 3.f).a;

				if (maskAlpha == 1.f) // если пиксель непрозрачный, пишем в буфер расстояние до биллборда
					gl_FragDepth = lightDistance;
				
				// иначе не пишем ничего
				else
					discard;
			}
			else
			{
				gl_FragDepth = lightDistance;
			}
		}]],
	attriblocations = {
		{
			name = "position",
			location = 0
		},
		{
			name = "normal",
			location = 1
		},
		{
			name = "texCoord",
			location = 2
		},
		{
			name = "bonesIdx",
			location = 3
		},
		{
			name = "weights",
			location = 4
		},
		{
			name = "meshID",
			location = 5
		}
	}
}