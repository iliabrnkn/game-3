return {
	name = "DefaultModel",
	vertexshader = 
		[[
		#version 420
		#extension GL_ARB_shading_language_420pack: enable

		const int MAX_MESHES = 100;
		const int MAX_BONES_PER_MODEL = 100;
		
		// ---- INPUTS

		in vec3 position;
		in vec3 normal;
		in vec2 texCoord;
		in vec4 boneID;
		in vec4 weights; 
		in int meshID;

		// ---- OUTPUTS

		out vec3 fragPos;
		out vec3 fragNormal;
		out vec2 fragTexCoord;
		flat out int fragMeshID;
		flat out vec4 fragMaterialColor;
		out vec3 fragNDC;

		// ---- UNIFORMS

		uniform mat4 mvp;
		uniform samplerBuffer boneMxBuffer;

		// ----

		struct MeshProps {
			mat4 transformMx;
			vec4 material;
			int boneMxOffset;
			float padding1;
			float padding2;
			float padding3;
    	};

		layout(std140, binding = 0) uniform meshBlock
		{
			MeshProps meshProps[MAX_MESHES];
		};

		// ----

		mat4 getMat4FromSamplerBuffer(samplerBuffer buf, int i)
		{
			return mat4(
				texelFetch(buf, i * 4),
				texelFetch(buf, i * 4 + 1),
				texelFetch(buf, i * 4 + 2),
				texelFetch(buf, i * 4 + 3) 
			);
		}

		// ----

		void main()
		{
			vec4 vertPos = vec4(position, 1.f);
			vec4 vertNormal = vec4(normal, 0.f);

			if (meshID >= 0) // если это не геометрия уровня
			{
				if (meshProps[meshID].boneMxOffset != -1) // если это персонаж
				{
					mat4 boneTransform = mat4(0.f);

					for (int i = 0; i < 3; i++)
					{
						if (boneID[i] != -1.f)
						{
							boneTransform += getMat4FromSamplerBuffer(
								boneMxBuffer, 
								meshProps[meshID].boneMxOffset + int(boneID[i])
							) * weights[i];
						}
					}

					vertPos = boneTransform * vertPos;
					vertNormal = boneTransform * vertNormal;
				}

				vertPos = meshProps[meshID].transformMx * vertPos;
				vertNormal = meshProps[meshID].transformMx * vertNormal;
			}
			else // если в meshID лежит gid 
			{
			}

			//---- OUTPUTS

			fragPos = vec3(vertPos);
			fragNormal = normalize(vec3(vertNormal)); 
			fragMeshID = meshID;
			fragTexCoord = texCoord.xy;

			gl_Position = mvp * vertPos;
			fragNDC = gl_Position.xyz / gl_Position.w;

			if (meshID >= 0)
				fragMaterialColor = meshProps[meshID].material;

		}]],

	fragmentshader = 
		[[
		#version 420
		
		const int MAX_GID_COUNT = 4096;

		// ---- INPUTS

		in vec3 fragPos;
		in vec3 fragNormal;
		in vec2 fragTexCoord;
		flat in int fragMeshID;
		flat in vec4 fragMaterialColor;
		in vec3 fragNDC;

		// ---- OUTPUTS
		
		layout(location = 0) out vec4 outColor;
		layout(location = 1) out vec4 outPos;
		layout(location = 2) out vec4 outNormal;
		layout(location = 3) out vec4 outNormal3D;

		// ---- UNIFORMS

		uniform sampler2DArray atlas;
		uniform sampler2DArray spriteAtlasA;
		uniform sampler2DArray spriteAtlasB;
		uniform sampler2DArray spriteAtlasC;
		uniform sampler2DArray spriteAtlasD;
		uniform int display;

		struct GidProps
		{
			float layerNum;
			float atlasNum;
			vec2 spriteSizeTextureOffsetFactor;
		};

		vec4 sampleAtlas(GidProps gidProps, vec2 texCoord)
		{
			vec4 res = vec4(0.0);

			if (gidProps.atlasNum == 0.f)
			{
				res = texture(spriteAtlasA, vec3(texCoord, gidProps.layerNum));
			}
			else if (gidProps.atlasNum == 1.0)
			{
				res = texture(spriteAtlasB, vec3(texCoord, gidProps.layerNum));
			}
			else if (gidProps.atlasNum == 2.0)
			{
				res = texture(spriteAtlasC, vec3(texCoord, gidProps.layerNum));
			}
			else if (gidProps.atlasNum == 3.0)
			{
				res = texture(spriteAtlasD, vec3(texCoord, gidProps.layerNum));
			}

			return res;
		}

		layout(std140, binding = 1) uniform gidBlock
		{
			GidProps gidProps[MAX_GID_COUNT];
		};

		// ----
		
		void main(){

			if (display == 0)
				discard;

			if (fragNormal != vec3(-1.f))
			{
				outNormal3D = vec4(fragNormal, 1.0);
			}

			outPos = vec4(fragPos, 1.0);

			if (fragMeshID < 0) // геометрия
			{
				outColor = sampleAtlas(gidProps[-fragMeshID], fragTexCoord);
			}
			else if (fragMaterialColor != vec4(-1.f)) // предметы
			{
				outColor = fragMaterialColor;
			}
			else // персонажи
			{
				outColor = texture(atlas, vec3(fragTexCoord.xy, 0.f));
			}

			if (outColor.a == 0.0)
				discard;
		}]],
	attriblocations = {
		{
			name = "position",
			location = 0
		},
		{
			name = "normal",
			location = 1
		},
		{
			name = "texCoord",
			location = 2
		},
		{
			name = "boneID",
			location = 3
		},
		{
			name = "weights",
			location = 4
		},
		{
			name = "meshID",
			location = 5
		}
	}
}