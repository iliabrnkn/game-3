return {
	name = "Bloom",
	vertexshader = 
		[[
		#version 420
		
		in vec3 position;
		in vec2 texCoord;

		uniform mat4 projectionMatrix;
		out vec2 texCoordFrag;

		void main()
		{
			texCoordFrag = texCoord;

			gl_Position = projectionMatrix * vec4(position, 1.0);
		}]],
	fragmentshader = 
		[[
		#version 420

		in vec2 texCoordFrag;

		const int BLOOM_DEPTH = 5;

		uniform int horizontal;
		uniform sampler2D bloomTex;
		uniform float weight[5] = float[] (0.227027, 0.1945946, 0.1216216, 0.054054, 0.016216);

		layout(location = 0) out vec4 outBloomPingPong;
		
		void main(){

			ivec2 texSize = ivec2(640, 360);
			vec3 resColor = vec3(0.f);
			
			if (horizontal == 1)
			{
				float texOffset = 1.f / float(texSize.x);
				
				for (int i = -BLOOM_DEPTH; i < BLOOM_DEPTH; ++i)
				{
					float xOffset = texCoordFrag.x + float(i) * texOffset;

					if (xOffset >= 0.f && xOffset <= 1.f)
						resColor += weight[abs(i)] * texture(bloomTex, vec2(xOffset, texCoordFrag.y)).rgb;
				}
			}
			else
			{
				float texOffset = 1.f / float(texSize.y);

				for (int i = -BLOOM_DEPTH; i < BLOOM_DEPTH; ++i)
				{
					float yOffset = texCoordFrag.y + float(i) * texOffset;

					if (yOffset >= 0.f && yOffset <= 1.f)
						resColor += weight[abs(i)] * texture(bloomTex, vec2(texCoordFrag.x, yOffset)).rgb;
				}
			}

			outBloomPingPong = vec4(resColor.rgb, 1.f);

			//outBloomPingPong = texture(bloomTex, texCoordFrag);		
		}]],
	attriblocations = {
		{
			name = "position",
			location = 0
		},
		{
			name = "texCoord",
			location = 1
		}
	}
}