return {
	name = "Clouds",
	vertexshader = 
		[[
		#version 420
		#extension GL_ARB_shading_language_420pack: enable
		// ---- INPUTS

		in vec3 position;
		in vec3 normal;
		in vec2 texCoord;
		in vec4 boneID;
		in vec4 weights; 
		in int meshID;

		// ---- OUTPUTS

		out vec2 fragTexCoord;
		
		// ---- UNIFORMS

		uniform mat4 mvp;
		
		void main()
		{
			//---- OUTPUTS

			fragTexCoord = texCoord;
			gl_Position = mvp * vec4(position, 1.f);

		}]],

	fragmentshader = 
		[[
		#version 420

		// UNIFORMS

		uniform sampler2DArray cloudTex;
		uniform vec2 cloudTexOffset;

		// ---- INPUTS

		in vec2 fragTexCoord;
		
		// ---- OUTPUTS
		
		layout(location = 0) out vec4 outClouds;
		
		void main(){

			//outClouds = vec4(textureOffset(cloudTex, vec3(fragTexCoord, 0), cloudTexOffset).a * vec3(1.f), 1.f);
			//outClouds = vec4(1.f);
			outClouds = texture(cloudTex, vec3(fragTexCoord + cloudTexOffset, 0));
		}]],
	attriblocations = {
		{
			name = "position",
			location = 0
		},
		{
			name = "normal",
			location = 1
		},
		{
			name = "texCoord",
			location = 2
		},
		{
			name = "boneID",
			location = 3
		},
		{
			name = "weights",
			location = 4
		},
		{
			name = "meshID",
			location = 5
		}
	}
}