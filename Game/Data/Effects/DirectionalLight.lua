return {
	name = "DirectionalLight",
	vertexshader = 
		[[
		#version 420
		#extension GL_ARB_shading_language_420pack: enable

		const int MAX_MESHES = 100;
		
		in vec3 position;
		in vec3 normal;
		in vec2 texCoord;
		in vec4 boneID;
		in vec4 weights; 
		in int meshID;

		out vec3 fragDeviceCoords;
		out vec2 fragTexCoord;
		out vec3 fragScenePos;

		flat out int fragMeshID;

		mat4 getMat4FromSamplerBuffer(samplerBuffer buf, int i)
		{
			return mat4(
				texelFetch(buf, i * 4),
				texelFetch(buf, i * 4 + 1),
				texelFetch(buf, i * 4 + 2),
				texelFetch(buf, i * 4 + 3) 
			);
		}

		// ----

		uniform samplerBuffer boneMxBuffer;
		uniform mat4 mvp;

		// ----

    	struct MeshProps {
			mat4 transformMx;
			vec4 material;
			int boneMxOffset;
			float padding1;
			float padding2;
			float padding3;
    	};

		layout(std140, binding = 0) uniform meshBlock
		{
			MeshProps meshProps[MAX_MESHES];
		};

		// ----

		void main()
		{
			vec4 vertPos = vec4(position, 1.f);

			if (meshID >= 0) // если это не геометрия уровня
			{
				if (meshProps[meshID].boneMxOffset != -1) // если это персонаж
				{
					mat4 boneTransform = mat4(0.f);

					for (int i = 0; i < 3; i++)
					{
						if (boneID[i] != -1.f)
						{
							boneTransform += getMat4FromSamplerBuffer(
								boneMxBuffer, 
								meshProps[meshID].boneMxOffset + int(boneID[i])
							) * weights[i];
						}
					}

					vertPos = boneTransform * vertPos;
				}

				vertPos = meshProps[meshID].transformMx * vertPos;
			}

			gl_Position = mvp * vertPos;
			fragDeviceCoords = gl_Position.xyz;
			fragTexCoord = texCoord;
			fragMeshID = meshID;
			fragScenePos = vertPos.xyz;
		}]],
	fragmentshader = 
		[[
		#version 330
		#extension GL_ARB_shading_language_420pack: enable

		in vec2 fragTexCoord;
		flat in int fragMeshID;

		in vec3 fragDeviceCoords;
		in vec3 fragScenePos;

		uniform sampler2D shadowMask;
		uniform sampler2DArray spriteAtlasA;
		uniform sampler2DArray spriteAtlasB;
		uniform sampler2DArray spriteAtlasC;
		uniform sampler2DArray spriteAtlasD;

		const int MAX_GID_COUNT = 4096;

		struct GidProps
		{
			float layerNum;
			float atlasNum;
			vec2 spriteSizeTextureOffsetFactor;
		};

		vec4 sampleAtlas(GidProps gidProps, vec2 texCoord)
		{
			vec4 res = vec4(0.0);

			if (gidProps.atlasNum == 0.f)
			{
				res = texture(spriteAtlasA, vec3(texCoord, gidProps.layerNum));
			}
			else if (gidProps.atlasNum == 1.0)
			{
				res = texture(spriteAtlasB, vec3(texCoord, gidProps.layerNum));
			}
			else if (gidProps.atlasNum == 2.0)
			{
				res = texture(spriteAtlasC, vec3(texCoord, gidProps.layerNum));
			}
			else if (gidProps.atlasNum == 3.0)
			{
				res = texture(spriteAtlasD, vec3(texCoord, gidProps.layerNum));
			}

			return res;
		}

		layout(std140, binding = 1) uniform gidBlock
		{
			GidProps gidProps[MAX_GID_COUNT];
		};

		void main(){
			if (fragMeshID < 0 &&
				( 
					sampleAtlas(gidProps[-fragMeshID], fragTexCoord).a == 0.f
				)
			)
			{
				discard;
			}
			
			gl_FragDepth = fragDeviceCoords.z / 2.f + 0.5f;
		}]],
	attriblocations = {
		{
			name = "position",
			location = 0
		},
		{
			name = "normal",
			location = 1
		},
		{
			name = "texCoord",
			location = 2
		},
		{
			name = "boneID",
			location = 3
		},
		{
			name = "weights",
			location = 4
		},
		{
			name = "meshID",
			location = 5
		}
	}
}