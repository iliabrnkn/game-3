return {
	name = "Scaling",
	vertexshader = 
		[[
		#version 420
		
		in vec3 position;
		in vec2 texCoord;

		uniform mat4 projectionMatrix;
		out vec2 texCoordFrag;

		void main()
		{
			texCoordFrag = texCoord;

			gl_Position = projectionMatrix * vec4(position, 1.0);
		}]],
	fragmentshader = 
		[[
		#version 420
		#extension GL_ARB_texture_cube_map_array: enable

		uniform sampler2D colorTex;
		uniform float gamma;

		in vec2 texCoordFrag;
		out vec4 fragColor;

		/*vec4 vhsEffect(sampler2D tex, vec2 texCoord, float offset)
		{
			vec4 colOriginal = texture(tex, texCoord);
			vec4 colRight = texture(tex, texCoord + vec2(FRAME_OFFSET.x * offset, 0.f));
			vec4 colLeft = texture(tex, texCoord + vec2(FRAME_OFFSET.x * offset, 0.f));

			return vec4(colOriginal.r, colRight.g, colLeft.b, colOriginal.w);
		}*/

		void main() {
		
			vec4 colorVal = texture(colorTex, texCoordFrag);
			colorVal.r = pow(colorVal.r, gamma);
			colorVal.g = pow(colorVal.g, gamma);
			colorVal.b = pow(colorVal.b, gamma);

			fragColor = colorVal;
		}]],
	attriblocations = {
		{
			name = "position",
			location = 0
		},
		{
			name = "texCoord",
			location = 1
		}
	}
}