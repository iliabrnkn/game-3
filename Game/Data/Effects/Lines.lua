return {
	name = "Lines",
	vertexshader = 
		[[
		#version 420

		in vec3 point;
		in vec4 color;
		
		out vec4 fragColor;
		
		uniform mat4 mvp;

		void main()
		{
			fragColor = vec4(color);
			gl_Position = mvp * vec4(point, 1.f);

		}]],
	fragmentshader = 
		[[
		#version 420

		in vec4 fragColor;

		layout(location = 0) out vec4 outBlurred;
		//layout(location = 1) out vec4 outBlurred;
		
		void main(){
			//outParticles = fragColor;
			outBlurred = fragColor;
		}]],
	attriblocations = {
		{
			name = "point",
			location = 0
		},
		{
			name = "color",
			location = 1
		}
	}
}