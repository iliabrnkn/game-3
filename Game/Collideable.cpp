#include "stdafx.h"
#include <exception>

#include "Collideable.h"
#include "GameplayState.h"
#include "Position.h"
#include "Direction.h"
#include "LuaHelper.h"

namespace mg
{
	//CollideableComponentManager
	void CollideableComponentManager::AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		RigidBody* body;

		// �����
		float mass = LuaHelper::GetFloat(table, "mass", false, 0.f);
		auto shapeTable = LuaHelper::GetTable(table, "shape", false);

		// ���� ��������� ����� ����
		if (LuaHelper::GetInteger(table, "worldArrayIndex", false) == -1)
		{
			// ������
			btCollisionShape* collisionShape = LuaHelper::GetCollisionShape(shapeTable);

			btTransform startTransform;

			// �������� ����������� ������ ����� ��������� Position, 
			// ���� ��� ��� - ����������� ���
			if (entity.has_component<Position>())
			{
				// �������� � ������������ � ��������
				startTransform.setOrigin(*(entity.component<Position>()));
			}
			else
			{
				entity.assign<Position>(startTransform.getOrigin());
			}

			// ���� � �������� ����� ���� ��������� Direction, �� ����������� ���� ��������������
			if (entity.has_component<Direction>())
			{
				startTransform.setBasis(entity.component<Direction>()->GetBasis());
			}
			else
			{
				startTransform.setBasis(btMatrix3x3::getIdentity());
			}

			// ������ � �����
			std::string collisionGroup = LuaHelper::GetString(table, "collisionGroup", false, "everything");
			std::string collisionMask = LuaHelper::GetString(table, "collisionMask", false, "everything");

			body = gps->GetPhysics().CreateRigidBody(mass, startTransform, collisionShape, SceneVector(0), 
				mass > 0 ? ~(btCollisionObject::CF_STATIC_OBJECT | btCollisionObject::CF_KINEMATIC_OBJECT) : btCollisionObject::CF_STATIC_OBJECT,
				COLLISION_GROUPS.at(collisionGroup), COLLISION_MASKS.at(collisionMask));
			body->setWorldTransform(startTransform);
			body->getMotionState()->setWorldTransform(startTransform);
			body->setUserIndex(entity.id().index());
			//body->setSleepingThresholds(0.0, 0.0);

			// damping
			auto dampingTable = LuaHelper::GetTable(table, "damping", false);

			if (!dampingTable.IsNil())
			{
				body->setDamping(
					LuaHelper::GetFloat(dampingTable, "linear", false, 0.05),
					LuaHelper::GetFloat(dampingTable, "angular", false, 1.05)
				);
			}
			else
			{
				body->setDamping(0.05f, 1.05f);
			}

			// �������� ����
			if (LuaHelper::GetBoolean(table, "debug", false))
			{
				auto debugColorTable = LuaHelper::GetTable(table, "debugColor", false);

				if (!debugColorTable.IsNil())
				{
					body->setCustomDebugColor(
						btVector3(
							LuaHelper::GetFloat(debugColorTable, "r", false, 0.f),
							LuaHelper::GetFloat(debugColorTable, "g", false, 0.f),
							LuaHelper::GetFloat(debugColorTable, "b", false, 0.f)
						)
					);

					GUI::GetInstance().ConsoleLog("custom color set");
				}
				else
					body->setCustomDebugColor(btVector3(0.0, 1.0, 0.0));
			}

			entity.assign<Collideable>(body);
		}
		else // ���� �� ��������� ������, ������ ������������ ������ ������������� ����
		{
			int worldArrayIndex = LuaHelper::GetInteger(table, "worldArrayIndex");

			body = reinterpret_cast<RigidBody*>(gps->GetPhysics().dynamicsWorld->getCollisionObjectArray()[worldArrayIndex]);
			// ������ ���������������� ������ ���������� ���� ��������, ������� �������� ���������
			// ����� ��� ������������� ����� ���� ���������� ��� � ��� �����������
			body->setUserIndex(entity.id().index());

			// ������� ����������
			if (entity.has_component<Skeleton>())
			{
				for (auto& bb : entity.component<Skeleton>()->boneBodies)
				{
					bb.second.body->getWorldTransform().getOrigin() = btVector3(*entity.component<Position>()) + bb.second.body->getWorldTransform().getOrigin();
					bb.second.body->getMotionState()->setWorldTransform(bb.second.body->getWorldTransform());
				}

				body->getWorldTransform().getOrigin() = btVector3(*entity.component<Position>());
			}

			if (entity.has_component<Position>() && !entity.has_component<Skeleton>())
			{
				body->getWorldTransform().getOrigin() = btVector3(*entity.component<Position>()) + body->getWorldTransform().getOrigin();
			}

			if (entity.has_component<Direction>())
			{
				auto dir = *entity.component<Direction>();
				body->getWorldTransform().setBasis(dir.GetBasis());
			}

			body->getMotionState()->setWorldTransform(body->getWorldTransform());

			// ������ �����

			if (!shapeTable.IsNil())
			{
				delete body->getCollisionShape();

				body->setCollisionShape(LuaHelper::GetCollisionShape(shapeTable));
			}
			
			entity.assign<Collideable>(body);
		}
	}

	LuaPlus::LuaObject CollideableComponentManager::GetComponentTable(entityx::Entity& entity) const
	{
		LuaPlus::LuaObject table;
		table.AssignNewTable(gps->GetLuaState());

		return LuaPlus::LuaObject();
	}

	LuaPlus::LuaObject CollideableComponentManager::ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		auto collideable = entity.component<Collideable>();

		return LuaPlus::LuaObject();
	}

	void CollideableComponentManager::ApplyCentralImpulse(const int &bodyWorldArrayIndex, const LuaPlus::LuaObject &impulseTable) const
	{
		auto *body = gps->GetPhysics().GetBodyByWorldArrayIndex(bodyWorldArrayIndex);
		body->activate();
		auto impulse = SceneVector::FromMapCoords(impulseTable);
		body->applyCentralImpulse(impulse);

		/*gps->GetScene().GetSimpleBatch().DrawVector(body->getWorldTransform().getOrigin(), impulse, glm::vec3(1.f, 0.f, 0.f));*/
	}

	void CollideableComponentManager::ApplyImpulse(const int &bodyWorldArrayIndex, const LuaPlus::LuaObject &impulseTable, 
		const LuaPlus::LuaObject &relPoint) const
	{
		auto *body = gps->GetPhysics().GetBodyByWorldArrayIndex(bodyWorldArrayIndex);
		body->activate();
		body->applyImpulse(SceneVector::FromMapCoords(impulseTable), SceneVector::FromMapCoords(relPoint));

		/*gps->GetScene().GetSimpleBatch().DrawVector(body->getWorldTransform().getOrigin() + 
			body->getWorldTransform().getBasis() * SceneVector::FromMapCoords(relPoint),
			SceneVector::FromMapCoords(impulseTable), glm::vec3(1.f, 0.f, 0.f));*/
	}

	void CollideableComponentManager::ApplyTorqueImpulse(const LuaPlus::LuaObject &entityTable, const LuaPlus::LuaObject &torqueTable) const
	{
		auto entity = gps->GetEntity(entityTable);
		auto *body = entity.component<Collideable>()->body;

		body->activate();
		body->applyTorqueImpulse(body->getInvInertiaTensorWorld().inverse() * SceneVector::FromMapCoords(torqueTable));
	}

	void CollideableComponentManager::SetAngularVelocity(const LuaPlus::LuaObject &entityTable, const LuaPlus::LuaObject &angularVelocityTable) const
	{
		auto entity = gps->GetEntity(entityTable);
		auto *body = entity.component<Collideable>()->body;

		body->activate();
		body->setAngularVelocity(SceneVector::FromMapCoords(angularVelocityTable));
	}

	/*void CollideableComponentManager::AddConstraint(const int &firstBodyIndex, const int &secBodyIndex, const LuaPlus::LuaObject &constraintTable)
	{
		gps->GetPhysics().CreateConstraint(
			gps->GetPhysics().GetBodyByWorldArrayIndex(firstBodyIndex), gps->GetPhysics().GetBodyByWorldArrayIndex(secBodyIndex),
			LuaHelper::GetFloat(constraintTable, "")
		)
	}*/

	void CollideableComponentManager::SetWeaponBody(const LuaPlus::LuaObject &characterEntityTable, const LuaPlus::LuaObject &weaponTable, 
		const char* weaponJointName)
	{
		auto characterEntity = gps->GetEntity(characterEntityTable);
		auto weaponEntity = gps->GetEntity(weaponTable);
		auto &weaponBoneBody = characterEntity.component<Skeleton>()->boneBodies.at(weaponJointName);
		auto handBoneBody = characterEntity.component<Skeleton>()->GetBoneBodyParent(weaponBoneBody);
		auto *weaponConstraint = weaponBoneBody.body->getConstraintRef(0);
		auto constraintTable = LuaHelper::GetTable(LuaHelper::GetTable(weaponTable, "weaponConstraint"), weaponJointName);

		// �������� � boneBody ������� ���� ������ �� �����
		weaponBoneBody.body->removeConstraintRef(weaponConstraint);
		auto t = weaponBoneBody.body->getWorldTransform();
		gps->GetPhysics().dynamicsWorld->removeConstraint(weaponConstraint);
		//gps->GetPhysics().dynamicsWorld->removeRigidBody(weaponBoneBody.body);
		weaponBoneBody.body = weaponEntity.component<Collideable>()->body;
		weaponBoneBody.body->setWorldTransform(/*weaponBoneBody.startTransform*/t);

		btVector3 aabbMax, aabbMin; 
		weaponBoneBody.body->getCollisionShape()->getAabb(
			btTransform::getIdentity(), 
			aabbMin, aabbMax
		);

		weaponBoneBody.axisOffset = (aabbMax - aabbMin) / 2.0;

		/*auto frameA = btTransform(handBoneBody->startTransform.getBasis().inverse(), SceneVector());
		auto frameB = btTransform(weaponBoneBody.startTransform.getBasis().inverse(), -weaponBoneBody.axisOffset / 2.0);

		if (LuaHelper::GetBoolean(constraintTable, "axisShift", false))
		{
			btMatrix3x3 frameABasis, frameBBasis;

			frameABasis[0] = frameA.getBasis().getRow(1);
			frameABasis[1] = frameA.getBasis().getRow(2);
			frameABasis[2] = frameA.getBasis().getRow(0);
			frameA.setBasis(frameABasis);

			frameB.setBasis(frameBBasis);
		}

		btGeneric6DofConstraint *constraint = new btGeneric6DofConstraint(*handBoneBody->body, *weaponBoneBody.body, 
			frameA,
			frameB, false);

		constraint->setAngularLowerLimit(LuaHelper::GetBTVec3(LuaHelper::GetTable(constraintTable, "angularLowerLimit")));
		constraint->setAngularUpperLimit(LuaHelper::GetBTVec3(LuaHelper::GetTable(constraintTable, "angularUpperLimit")));
		constraint->setLinearLowerLimit(LuaHelper::GetBTVec3(LuaHelper::GetTable(constraintTable, "linearLowerLimit")));
		constraint->setLinearUpperLimit(LuaHelper::GetBTVec3(LuaHelper::GetTable(constraintTable, "linearUpperLimit")));
		constraint->setBreakingImpulseThreshold(10000000);

		gps->GetPhysics().dynamicsWorld->addConstraint(constraint, true);*/
		weaponBoneBody.body->addConstraintRef(
			gps->GetPhysics().CreateConstraint(
				handBoneBody->body, weaponBoneBody.body, SceneVector(0.f), -weaponBoneBody.axisOffset / 2.0,
				LuaHelper::GetBTVec3(LuaHelper::GetTable(constraintTable, "angularLowerLimits")),
				LuaHelper::GetBTVec3(LuaHelper::GetTable(constraintTable, "angularUpperLimits")),
				LuaHelper::GetBTVec3(LuaHelper::GetTable(constraintTable, "linearLowerLimits")),
				LuaHelper::GetBTVec3(LuaHelper::GetTable(constraintTable, "linearUpperLimits"))
			)
		);

		// ������������� ������������ �� ����� ����������
		gps->entityManager.each<Skeleton>([&weaponBoneBody](entityx::Entity &entity, Skeleton &skeleton) {
			weaponBoneBody.body->setIgnoreCollisionCheck(skeleton.boneBodies.at("Armature").body, true);
			skeleton.boneBodies.at("Armature").body->setIgnoreCollisionCheck(weaponBoneBody.body, true);
		});

		// �������� �������� ������������ � ������� �������
		for (auto &boneBody : characterEntity.component<Skeleton>()->boneBodies)
		{
			if (weaponBoneBody.body != boneBody.second.body)
				boneBody.second.body->setIgnoreCollisionCheck(weaponBoneBody.body, true);
		}
	}

	LuaPlus::LuaObject CollideableComponentManager::DebugTransformEntity(const LuaPlus::LuaObject &entityTable, const LuaPlus::LuaObject &eulerAnglesOffsetTable, 
		const LuaPlus::LuaObject &originOffsetTable)
	{
		auto entity = gps->GetEntity(entityTable);
		auto eulerAnglesOffset = LuaHelper::GetBTVec3(eulerAnglesOffsetTable);
		auto originOffset = LuaHelper::GetBTVec3(originOffsetTable);
		auto *body = entity.component<Collideable>()->body;

		LuaPlus::LuaObject res; res.AssignNewTable(gps->GetLuaState());

		// ���� ���� �� � ���� �� ����������, ������� ���� ��� ����
		if (!body->getConstraintRef(0))
		{
			auto origin = body->getWorldTransform().getOrigin();
			btVector3 eulerAngles; body->getWorldTransform().getRotation().getEulerZYX(eulerAngles[2], eulerAngles[1], eulerAngles[0]);

			eulerAngles += eulerAnglesOffset;
			origin += originOffset;

			body->setWorldTransform(btTransform(
				btQuaternion(eulerAngles.z(), eulerAngles.y(), eulerAngles.x()),
				origin
			));

			res.SetObject("eulerAngles", LuaHelper::BTVec3ToTable(eulerAngles, gps->GetLuaState()));
			res.SetObject("origin", LuaHelper::BTVec3ToTable(origin, gps->GetLuaState()));
		}
		else // ���� ���� ��������� ������������, �������������� ����� ����� ����
		{
			btGeneric6DofConstraint* constraint = reinterpret_cast<btGeneric6DofConstraint*>(body->getConstraintRef(0));

			auto origin = constraint->getFrameOffsetB().getOrigin();
			btVector3 eulerAngles; constraint->getFrameOffsetB().getBasis().getEulerZYX(eulerAngles[2], eulerAngles[1], eulerAngles[0]);

			eulerAngles += eulerAnglesOffset;
			origin += originOffset;

			btMatrix3x3 newBasis = btMatrix3x3::getIdentity();
			newBasis.setEulerZYX(eulerAngles[0], eulerAngles[1], eulerAngles[2]);

			constraint->setFrames(constraint->getFrameOffsetA(), btTransform(newBasis, origin));

			res.SetObject("eulerAngles", LuaHelper::BTVec3ToTable(eulerAngles, gps->GetLuaState()));
			res.SetObject("origin", LuaHelper::BTVec3ToTable(origin, gps->GetLuaState()));
		}

		return res;
	}

	LuaPlus::LuaObject CollideableComponentManager::GetBodyPos(const int &bodyArrayIndex)
	{
		auto *body = gps->GetPhysics().GetBodyByWorldArrayIndex(bodyArrayIndex);

		if (body)
		{
			return LuaHelper::BTVec3ToTable(body->getWorldTransform().getOrigin(), gps->GetLuaState());
		}
	}

	int CollideableComponentManager::GetWorldArrayIndex(const LuaPlus::LuaObject &entity)
	{
		return gps->GetPhysics().GetWorldArrayIndex((gps->GetEntity(entity).component<Collideable>()->body));
	}

	void CollideableComponentManager::ApplyCentralForce(const int &bodyWorldArrayIndex, const LuaPlus::LuaObject &forceVec) const
	{
		gps->GetPhysics().GetBodyByWorldArrayIndex(bodyWorldArrayIndex)->applyCentralForce(SceneVector::FromMapCoords(forceVec));
	}
}