// Game.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <stdio.h>
#include <string>
#include <random>
#include <time.h>

#include "WindowConstants.h"
#include "Core.h"
#include "ErrorLog.h"

#include <SFML/Graphics.hpp>

#include "CustomMatrix.h"


int main(int argc, char *argv[])
{
	auto fsmodes = sf::VideoMode::getFullscreenModes();

	if (/*(argc >= 2 && std::string(argv[1]) == std::string("windowed"))*/false) // ���� ��������� � ����
	{
		mg::Core::SetVideoMode(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT, 16), sf::Style::Default);
	}
	else // ������������� �����
	{
		mg::Core::SetVideoMode(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT, 16), sf::Style::Fullscreen);
	}


	mg::Core::GetInstance()->Init();
	mg::Core::GetInstance()->Loop();

    return 0;
}

