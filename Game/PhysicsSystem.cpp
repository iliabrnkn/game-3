#include "stdafx.h"

#include <unordered_map>

#include "GameplayState.h"
#include "PhysicsSystem.h"
#include "Collideable.h" 
#include "Moveable.h"
#include "Position.h"
#include "GUI.h"
#include "Direction.h"
#include "AIPiece.h"
#include "Strikeable.h"
#include "InverseKinematicsController.h"

namespace mg
{
	PhysicsSystem::PhysicsSystem(GameplayState* gps)
		:gps(gps)
	{
	}

	void PhysicsSystem::configure(entityx::EventManager &events)
	{
		gps->eventManager.subscribe<entityx::EntityDestroyedEvent>(*this);
	}

	void PhysicsSystem::HandleCollision(RigidBody *firstBody, RigidBody *secBody, const btManifoldPoint &point, const double &dt)
	{
		entityx::Entity* firstEntity(nullptr); 
		entityx::Entity* secEntity(nullptr);
		SceneVector dir = point.getPositionWorldOnA() - point.getPositionWorldOnB();

		if (firstBody->getUserIndex() != -1)
			firstEntity = &(gps->GetEntity(firstBody->getUserIndex()));
		if (secBody->getUserIndex() != -1)
			secEntity = &(gps->GetEntity(secBody->getUserIndex()));

		// ��� ������ - ���� ���������
		if (firstBody == gps->GetScene().GetLevelBody() && secEntity && secEntity->has_component<Skeleton>())
		{
			SceneVector dirXZ = dir.XZ().Normalized();
			
			// ��������� �������� �� ����������� � ������������
			secEntity->component<Moveable>()->targetLinearVelocity = 
				secEntity->component<Moveable>()->targetLinearVelocity - 
				(SceneVector(point.m_normalWorldOnB) * secEntity->component<Moveable>()->targetLinearVelocity.Dot(point.m_normalWorldOnB)) / 4.f;
		}

		// ������������ ������� � ���-����
		BoneBody* bb = nullptr;

		/*if (firstEntity != nullptr && firstEntity->has_component<Skeleton>() &&
			(bb = firstEntity->component<Skeleton>()->FindBoneBodyByRigidBody(reinterpret_cast<const RigidBody*>(firstBody))) != nullptr
			&& secBody != gps->GetScene().GetLevelBody())
		{
			auto bodyRigid = const_cast<RigidBody*>(reinterpret_cast<const RigidBody*>(firstBody));
			dir = dir.XZ().Normalized();

			bb->additionalImpulse = true;
			bodyRigid->forcedLinearVelocity = dir * point.getDistance() / dt;
			bb->body->setCustomDebugColor(btVector3(1.0, 0.0, 0.0));
		}*/

		// ��������� ��������� �������� �������
		if (firstEntity != nullptr && firstEntity->has_component<Strikeable>() && firstEntity->component<Strikeable>()->active)
		{
			LuaPlus::LuaObject args;
			args.AssignNewTable(gps->GetLuaState());

			args.SetObject("atacker", firstEntity->component<AIPiece>()->table);
			args.SetObject("hitNormalWorld", SceneVector(point.m_normalWorldOnB).ToMapCoords(gps->GetLuaState()));
			args.SetObject("hitCoords", SceneVector(point.getPositionWorldOnB()).ToMapCoords(gps->GetLuaState()));

			if (secEntity != nullptr && secEntity->has_component<Skeleton>()) // ���� �� ����-�� ������
			{
				args.SetObject("targetEntity", secEntity->component<AIPiece>()->table);
				args.SetInteger("targetBodyWorldIndex", secBody->getWorldArrayIndex());

				firstEntity->component<AIPiece>()->Emit("onHit", args);
				secEntity->component<AIPiece>()->Emit("onGetHit", args);

				// TODO: ��������� � �������
				
				/*
					auto ownerEntity = gps->GetEntity(firstEntity->component<Strikeable>()->ownerEntityID);
					auto secRigidBody = const_cast<RigidBody*>(reinterpret_cast<const RigidBody*>(secBody));

					secBody->activate();
					secEntity->component<Skeleton>()->LooseBodies(true);
					SceneVector impulseDir = 
						SceneVector(secBody->getWorldTransform().getOrigin()).XZ() - 
						SceneVector(firstBody->getWorldTransform().getOrigin()).XZ();
					impulseDir.Normalize();

					gps->GetScene().GetSimpleBatch().DrawVector(secBody->getWorldTransform().getOrigin(), impulseDir, glm::vec3(1.0, 0.0, 0.0));
				
					secBody->applyCentralImpulse(
						40.0 * impulseDir
					);
				*/
			}
			else if (!secEntity || secEntity->has_component<Skeleton>()) // ���� ���������
			{
				firstEntity->component<AIPiece>()->Emit("onMiss", args);
			}
		}
	}

	void PhysicsSystem::update(entityx::EntityManager &es, entityx::EventManager &em, entityx::TimeDelta dt)
	{
		gps->GetPhysics().dynamicsWorld->updateSingleAabb(gps->GetScene().GetLevelBody());

		// �����������
		es.each<Collideable, Moveable, Position, Direction, AIPiece>([dt, this](entityx::Entity &entity, Collideable &collideable, Moveable &mov,
			Position &pos, Direction &dir, AIPiece &script)
		{
			if (mov.targetLinearVelocity.Length() > 0.f)
			{
				if (!dynamic_cast<MotionState*>(collideable.body->getMotionState())->prevIterationMoving)
					script.Emit("onStartMove");

				//collideable.body->setLinearVelocity(mov.targetLinearVelocity);
				collideable.body->activate(true);
			}
			else
			{
				if (dynamic_cast<MotionState*>(collideable.body->getMotionState())->prevIterationMoving)
					script.Emit("onStopMove");
			}
		});

		es.each<Collideable>([dt, this](entityx::Entity &entity, Collideable &collideable)
		{
			if (gps->GetPhysics().dynamicsWorld->getGravity().length() > 0.0)
			{
				collideable.body->activate();
				collideable.body->applyGravity();
			}

			if (entity.has_component<Skeleton>())
			{
				collideable.body->getWorldTransform().getBasis() = btMatrix3x3::getIdentity();
				btTransform wTrans;
				collideable.body->getMotionState()->getWorldTransform(wTrans);
				collideable.body->getMotionState()->setWorldTransform(
					btTransform(btMatrix3x3::getIdentity(), wTrans.getOrigin()));
			}
		});

		// ��������� ��������� ������������
		auto numManifolds = gps->GetPhysics().dynamicsWorld->getDispatcher()->getNumManifolds();

		if (numManifolds > 0)
		{
			for (int i = 0; i < numManifolds; ++i)
			{
				auto currManifold = gps->GetPhysics().dynamicsWorld->getDispatcher()->getManifoldByIndexInternal(i);

				auto *firstBody = reinterpret_cast<RigidBody*>(const_cast<btCollisionObject*>(currManifold->getBody0()));
				auto *secBody = reinterpret_cast<RigidBody*>(const_cast<btCollisionObject*>(currManifold->getBody1()));

				auto numContacts = currManifold->getNumContacts();

				for (int j = 0; j < numContacts; ++j)
				{
					auto contactPoint = currManifold->getContactPoint(j);

					HandleCollision(firstBody, secBody, contactPoint, dt);
					HandleCollision(secBody, firstBody, contactPoint, dt);
				}
			}
		}

		es.each<Skeleton, Moveable>([dt, this](entityx::Entity& entity, Skeleton &skel, Moveable &mov) {
			if (!skel.dead)
			{
				// ���������� �� ���� ��������������� ����� ������
				for (auto bbPair : skel.boneBodies)
				{
					if (bbPair.second.additionalRotation != btQuaternion::getIdentity())
					{
						auto nextRotation = (
							bbPair.second.additionalRotation *
							btQuaternion(
								SceneVector(0.f, 1.f, 0.f),
								SceneVector(0.f, 0.f, 1.f).Angle(
									mov.targetBodyOrientation.Normalized(), SceneVector(0.f, 1.f, 0.f)
								)
							)
							);

						bbPair.second.body->getWorldTransform().setRotation(nextRotation);
					}

					if (bbPair.second.straightPosition != SceneVector(0.f))
					{
						bbPair.second.body->applyCentralImpulse(
							(bbPair.second.straightPosition - SceneVector(bbPair.second.body->getWorldTransform().getOrigin())) /
							(dt * bbPair.second.body->getInvMass())
						);
					}
				}

				for (auto bbPair : skel.boneBodies)
				{
					bbPair.second.body->applyCentralImpulse(mov.targetLinearVelocity / bbPair.second.body->getInvMass());
				}

				for (auto bbPair : skel.boneBodies)
				{
					bbPair.second.body->getMotionState()->setWorldTransform(btTransform(
						bbPair.second.body->getWorldTransform().getRotation(),
						bbPair.second.body->getLinearVelocity() * dt + bbPair.second.body->getWorldTransform().getOrigin()
					));
				}
			}
		});

		// ��� ��������� ������
		//gps->GetPhysics().dynamicsWorld->stepSimulation(dt, 2, dt / 2.0);
		
		/*if (dt <= 1.0 / 30.0)
		{
			gps->GetPhysics().dynamicsWorld->stepSimulation(dt);
		}
		else
		{
			gps->GetPhysics().dynamicsWorld->stepSimulation(dt, int(dt * 60.0));
		}*/

		gps->GetPhysics().dynamicsWorld->stepSimulation(dt, int(dt * 60.0));

		es.each<Skeleton>([dt, this](entityx::Entity& entity, Skeleton &skel) {
			if (!skel.dead)
			{
				for (auto bbPair : skel.boneBodies)
				{
					// ����� �������
					if (bbPair.second.affectable)
					{
						bbPair.second.body->setLinearVelocity(
							bbPair.second.body->getLinearVelocity() * (1.f - skel.locomotorActivity)
						);
					}
					else
					{
						bbPair.second.body->setLinearVelocity(SceneVector(0.f));
					}
				}
			}
		});
		
		// �������� ���������� �������� ���� � ������������ � ���������� ����������� collideable
		es.each<Collideable, Position>([dt, this](entityx::Entity& entity, Collideable& collideable, Position& pos)
		{
			btTransform bodyTransform = collideable.body->getWorldTransform();
			pos = bodyTransform.getOrigin();
		});

		// �� �� ��� �������� � ������������
		es.each<Collideable, Direction>([dt, this](entityx::Entity& entity, Collideable& collideable, Direction& dir)
		{
			btTransform bodyTransform = collideable.body->getWorldTransform();
		});

		// ������ AABB � ���������� ���� ���
		for (int b = 0; b < gps->GetPhysics().dynamicsWorld->getNumCollisionObjects(); ++b)
		{
			auto body = gps->GetPhysics().dynamicsWorld->getCollisionObjectArray()[b];

			// ���� ����� �����-�� ��������� �������� ����, ���������� ������� ��������� ����
			btVector3 customDebugColor;
			body->getCustomDebugColor(customDebugColor);

			if (customDebugColor != btVector3(0.0, 0.0, 0.0))
			{
				if (gps->GetScene().GetSimpleBatch().ShowAABBs())
				{
					gps->GetScene().GetSimpleBatch().VisualizeAABB(body->getCollisionShape(), body->getWorldTransform(),
						glm::vec3(customDebugColor.x(), customDebugColor.y(), customDebugColor.z()));
				}

				if (gps->GetScene().GetSimpleBatch().ShowCoords())
				{
					gps->GetScene().GetSimpleBatch().VisualizeCoords(body->getCollisionShape(), body->getWorldTransform());
				}
			}
			else
			{
				if (gps->GetScene().GetSimpleBatch().ShowAABBs())
				{
					gps->GetScene().GetSimpleBatch().VisualizeAABB(body->getCollisionShape(), body->getWorldTransform());
				}
			}
		}

		// ������ ��������� ������

		if (gps->GetScene().GetSimpleBatch().ShowLevelGeometry())
		{
			gps->GetScene().GetSimpleBatch().VisualizeLevelGeometry();
		}
	}

	void PhysicsSystem::receive(const entityx::EntityDestroyedEvent& ev)
	{
		if (ev.entity.has_component<Skeleton>())
		{
			for (const auto &bb : ev.entity.component<const Skeleton>()->boneBodies)
			{
				auto* body = bb.second.body;

				if (body->getNumConstraintRefs() > 0)
					for (int i = 0; i < body->getNumConstraintRefs(); ++i)
						gps->GetPhysics().dynamicsWorld->removeConstraint(body->getConstraintRef(i));

				gps->GetPhysics().dynamicsWorld->removeCollisionObject(body);
			}
		}

		if (ev.entity.has_component<Collideable>())
		{
			auto* body = ev.entity.component<const Collideable>()->body;

			if (body->getNumConstraintRefs() > 0)
				for (int i = 0; i < body->getNumConstraintRefs(); ++i)
					gps->GetPhysics().dynamicsWorld->removeConstraint(body->getConstraintRef(i));

			gps->GetPhysics().dynamicsWorld->removeCollisionObject(body);
		}
	}
}