#pragma once

#include <map>
#include <vector>
#include <deque>
#include <string>
#include <unordered_map>
#include <algorithm>
#include <memory>
#include <glm/gtx/quaternion.hpp>

#include "Cloneable.h"
#include "VertTypes.h"

namespace mg {
	struct Props : Cloneable
	{
		std::string name;
		virtual ~Props() {};

		std::vector<AnimVertex> vertices;
		std::vector<unsigned int> indices;
		glm::vec2 texOffset;
		// �����������, �� ������� ���������� ������ �������� � ������� ������� ��������� �������
		glm::vec2 textureOffsetFactor;
		//int padding;
	};

	typedef std::shared_ptr<Props> PropsHandler;
}