#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <entityx\entityx.h>

#include <bullet/btBulletDynamicsCommon.h>

#include "LuaHelper.h"
#include "ComponentManager.h"
#include "Vector.h"

namespace mg {

	struct Position : public SceneVector
	{
		Position(const float &x, const float &y, const float &z) :
			SceneVector(x, y, z) {}

		Position(const glm::vec3 &vec) :
			SceneVector(vec) {}

		Position(const btVector3 &vec) :
			SceneVector(vec) {}

		Position(const SceneVector &vec) :
			SceneVector(vec) {}

		glm::mat4 GetTranslationMatrix() const
		{
			return glm::translate(glm::vec3(vec));
		}

		inline operator SceneVector() const
		{
			return SceneVector(vec);
		}
	};

	// Position component manager

	class PositionComponentManager : public ComponentManager
	{
	public:
		PositionComponentManager(GameplayState *gps) : ComponentManager("position", gps) {};
		~PositionComponentManager() = default;

		virtual void AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const override;
		virtual LuaPlus::LuaObject GetComponentTable(entityx::Entity& entity) const override;
		virtual LuaPlus::LuaObject ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const override;

		float GetDistance(const LuaPlus::LuaObject &firstEntityTable, const LuaPlus::LuaObject &secondEntityTable) const;
		LuaPlus::LuaObject GetPos(const LuaPlus::LuaObject &entity) const;
	};

}