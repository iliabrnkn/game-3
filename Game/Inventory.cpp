#include "stdafx.h"

#include "InventoryCell.h"
#include "InventoryItem.h"
#include "InventoryWindow.h"

namespace mg {

	//------------------- InventoryWindow.h ----------------//

	InventoryWindow::InventoryWindow(const WidgetParameters& params, const std::string& name)
		: Widget(params, name)
	{
		AddChild<InventoryGrid>(
			WidgetParameters(INVENTORY_CELL_SIDE_LENGTH * 6, INVENTORY_CELL_SIDE_LENGTH * 8),
			"ItemGrid");

		auto activeItems = AddChild<ActiveItemsWindow>(WidgetParameters(INVENTORY_CELL_SIDE_LENGTH * 3, INVENTORY_CELL_SIDE_LENGTH * 3,
			sf::Vector2i(INVENTORY_CELL_SIDE_LENGTH * 10, INVENTORY_CELL_SIDE_LENGTH * 2)),
			"ActiveItems");

		auto eventHandlers = GUI::GetInstance().GetLuaState()->GetGlobal("activeItemHandlers");
		activeItems->GetChild<ActiveItemCell>("WeaponCell0")->SetEventHandler("onWeaponChanged", eventHandlers["onWeapon0Changed"]);
		activeItems->GetChild<ActiveItemCell>("WeaponCell1")->SetEventHandler("onWeaponChanged", eventHandlers["onWeapon1Changed"]);
	}

	InventoryWindow::~InventoryWindow()
	{
	}

	// �������� �������� ����� � ����, ���� �� ����������, ���������� false

	bool InventoryWindow::AddItem(const LuaPlus::LuaObject& itemTable)
	{
		auto *itemGrid = GetChild<InventoryGrid>("ItemGrid");

		return itemGrid->AddItem(itemTable);
	}

	// ------------------------ InventoryItem.h -----------------------//

	InventoryItem::InventoryItem(const WidgetParameters& params, const std::string& name)
		: Widget(params, name),
		mouseDragOffset(0, 0)
	{
		itemTable = params.objectTable;

		if (itemTable["gridPos"].GetRef() == -1)
		{
			LuaPlus::LuaObject gridPosTable;

			gridPosTable.AssignNewTable(itemTable.GetState());

			auto gridPos = GUI::GetInstance().GetWidget<InventoryGrid>("ItemGrid")->GetGridCoords(origin);

			gridPosTable.SetInteger("x", gridPos.x);
			gridPosTable.SetInteger("y", gridPos.y);

			itemTable.SetObject("gridPos", gridPosTable);
		}

		SetBackgroundImg(params.imgPath);

		width = background.getSize().x;
		height = background.getSize().y;
	}

	void InventoryItem::OnMouseEnter()
	{
		if (!dragged)
		{
			Highlight();
		}
	}

	void InventoryItem::OnMouseLeave()
	{
		if (!dragged)
		{
			Fade();
		}
	}

	LuaPlus::LuaObject& InventoryItem::GetTable()
	{
		return itemTable;
	}

	void InventoryItem::OnClick(const sf::Vector2i& relativeCoords)
	{
		SetZOrder(GUI::GetInstance().GetWidget<InventoryGrid>("ItemGrid")->GetMaxZOrder() + 1);
		mouseDragOffset = relativeCoords;
	}

	void InventoryItem::OnDrag(const sf::Vector2i& mouseAbsCoords)
	{
		origin = mouseAbsCoords - mouseDragOffset;
	}

	void InventoryItem::OnDrop(const sf::Vector2i& mouseAbsCoords)
	{
		auto itemAbsPos = this->GetAbsPos();
		auto gridBefore = this->GetParent<Widget>()->GetParent<ItemGrid>();
		auto gridAfter = GUI::GetInstance().GetWidget<ItemGrid>(mouseAbsCoords);
		//auto gridAfter = GUI::GetInstance().GetWidget<ActiveItemsWindow>(mouseAbsCoords);
		gridBefore->FreePlace(this);

		// ������� ������ ���������, ���� ������ �������
		if (!(gridAfter != nullptr && gridAfter->ReplaceItem(this, itemAbsPos)))
		{
			gridBefore->ReplaceItem(this, this->GetLastScreenPos());
		}
	}

	sf::Vector2i InventoryItem::GetLastScreenPos() const
	{
		return lastScreenPos;
	}

	sf::Vector2i InventoryItem::GetLastGridPos() const
	{
		auto res = sf::Vector2i(itemTable["gridPos"]["x"].ToInteger(),
			itemTable["gridPos"]["y"].ToInteger());

		return res;
	}

	InventoryItem::~InventoryItem() {};

	//------------------- InventoryCell.h --------------------

	
	ItemCell::ItemCell(const mg::WidgetParameters& params, const std::string& name)
		: Widget(params, name), free(true)
	{
		DrawBox(GUI_COLOR_MEDIUM, GUI_COLOR_DIM, INVENTORY_CORNER_SIDE_LENGTH);

		SetBackgroundImg(background, sf::IntRect(0, 0, width, height));
	}

	ItemCell::~ItemCell() {};

	bool ItemCell::GetStatus()
	{
		return free;
	}

	void ItemCell::SetStatus(const bool &freeStatus)
	{
		free = freeStatus;
	}

	ActiveItemCell::ActiveItemCell(const mg::WidgetParameters &params, const std::string &name)
			: ItemCell(params, name)
	{
		DrawBox(GUI_COLOR_MEDIUM, GUI_COLOR_DIM, width / 2);
		SetBackgroundImg(background, sf::IntRect(0, 0, width, height));
	}

	/*void ActiveItemCell::SetEventHandler(const LuaPlus::LuaObject &eventHandlerInitial)
	{
		eventHandler = eventHandlerInitial;
	}*/

	/*void ActiveItemCell::OnItemChanged(const LuaPlus::LuaObject &itemTable)
	{
		static_cast<LuaPlus::LuaFunctionVoid>(eventHandler)(itemTable);
	}*/
}