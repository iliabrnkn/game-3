#include "stdafx.h"

#include "Pathable.h"
#include "GameplayState.h"
#include "Level.h"
#include "NavigationMesh.h"
#include "Position.h"
#include "Moveable.h"

namespace mg
{
	void PathableComponentManager::AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		entity.assign<Pathable>();
	}

	LuaPlus::LuaObject PathableComponentManager::GetComponentTable(entityx::Entity& entity) const
	{
		LuaPlus::LuaObject res;

		return res;
	}

	// ����� ����
	LuaPlus::LuaObject PathableComponentManager::FindPath(LuaPlus::LuaObject& entity, LuaPlus::LuaObject& endPoint)
	{
		auto ent = gps->GetEntity(entity);

		ent.component<Pathable>()->waypoints = gps->GetLevel()->GetNavMesh().FindPath(
			*ent.component<Position>(),
			SceneVector::FromMapCoords(endPoint)
		);

		LuaPlus::LuaObject res;
		res.AssignNewTable(gps->GetLuaState());

		for (auto point : ent.component<Pathable>()->waypoints)
		{
			res.Insert(point.ToMapCoords(gps->GetLuaState()));
		}

		return res;
	}

	LuaPlus::LuaObject PathableComponentManager::GetCurrentPath(LuaPlus::LuaObject &entity)
	{
		LuaPlus::LuaObject res;
		
		res.AssignNewTable(gps->GetLuaState());

		for (auto point : gps->GetEntity(entity).component<Pathable>()->waypoints)
		{
			res.Insert(point.ToMapCoords(gps->GetLuaState()));
		}

		return res;
	}

	void PathableComponentManager::ClearWayPoints(LuaPlus::LuaObject &entity) const
	{
		gps->GetEntity(entity).component<Pathable>()->waypoints.clear();
	}

	void PathableComponentManager::GoToLastWayPoint(LuaPlus::LuaObject &entity) const
	{
		auto ent = gps->GetEntity(entity);
		auto path = ent.component<Pathable>();
		
		if (path->waypoints.size() > 0)
		{
			auto mov = ent.component<Moveable>();
			SceneVector backPoint = path->waypoints.back();
			auto pos = *(ent.component<Position>());

			if ((backPoint - pos).Length() > 0.f)
			{
				mov->targetLinearVelocity = (backPoint - pos).Normalized() * mov->maxLinearVelocity;
			}
		}
	}

	bool PathableComponentManager::HasWayPoints(LuaPlus::LuaObject &entity) const
	{
		return gps->GetEntity(entity).component<Pathable>()->waypoints.size() > 0;
	}
}