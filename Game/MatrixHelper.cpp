#include "stdafx.h"

#include "Camera.h"
#include "Sizes.h"
#include "MatrixHelper.h"
#include "Renderer.h"

namespace mg {

	// ������� �� ����� � ����� ��� �� ����� � �����
	glm::mat4 MatrixHelper::mapSceneSwapMx(
		1.f, 0.f, 0.f, 0.f,
		0.f, 0.f, 1.f, 0.f,
		0.f, 1.f, 0.f, 0.f,
		0.f, 0.f, 0.f, 1.f
	);

	glm::mat4 MatrixHelper::coreViewMx =
		glm::rotate(-glm::pi<float>() / 6.f, glm::normalize(glm::vec3(1.f, 0.f, 0.f))) *
		glm::rotate(-glm::quarter_pi<float>(), glm::vec3(0.f, 1.f, 0.f)) *
		glm::scale(glm::vec3(1.f, -1.f, 1.f));

	glm::mat4 MatrixHelper::dirLightViewMX =
		glm::rotate(glm::pi<float>() / 3.0f, glm::normalize(glm::vec3(1.f, 0.f, 0.f)));
		//glm::rotate(-glm::quarter_pi<float>(), glm::vec3(0.f, 1.f, 0.f)) *
		//glm::scale(glm::vec3(1.f, -1.f, 1.f));
		//glm::mat4(1.f);

	// �� ����� � �����������
	glm::mat4 MatrixHelper::viewMx(
		coreViewMx * 
		glm::scale(glm::vec3(TILE_WIDTH_SCENE, TILE_WIDTH_SCENE, TILE_WIDTH_SCENE))
	);

	// �� ��������������� device-��������� � �������� ����������� (��� y ������ ����)
	glm::mat4 MatrixHelper::screenToNDMx(
		2.f / float(RENDERBUFFER_WIDTH), 0.f, 0.f, 0.f,
		0.f, -2.f / float(RENDERBUFFER_HEIGHT), 0.f, 0.f,
		0.f, 0.f, 1.f, 0.f,
		-1.f, 1.f, 0.f, 1.f
	);

	// �� ��������� ����������� � ��������������� device-�����������
	glm::mat4 MatrixHelper::projectionMx = glm::ortho<float>(
			0.f, float(RENDERBUFFER_WIDTH),
			float(RENDERBUFFER_HEIGHT), 0.f,
			float(ZNEAR_SCENE) * TILE_WIDTH_SCENE, float(ZFAR_SCENE) * TILE_WIDTH_SCENE
		);

	glm::mat4 MatrixHelper::dirLightProjectionMx = glm::ortho<float>(
		0.f, float(DIRECTIONAL_SHADOWMAP_WIDTH),
		0.f, float(DIRECTIONAL_SHADOWMAP_HEIGHT),
		ZNEAR_SCENE * 40.f, ZFAR_SCENE * 40.f
		);

	// �� ��������� ����� � ����������� ������
	glm::mat4 MatrixHelper::sceneToScreenMx = glm::inverse(screenToNDMx) * projectionMx * viewMx;

	// �� ��������� ������ � ����������� �����
	glm::mat4 MatrixHelper::screenToSceneMx =
		mapSceneSwapMx *
		glm::scale(glm::vec3(1.f / TILE_WIDTH_SCENE)) *
		glm::inverse(glm::rotate(glm::quarter_pi<float>(), glm::vec3(0.f, 0.f, 1.f))) *
		glm::scale(glm::vec3(1.f, 2.f, 1.f));

	glm::vec3 MatrixHelper::MapSceneSwap(const glm::vec3 &pos)
	{
		return glm::vec3(mapSceneSwapMx * glm::vec4(pos, 1.f));
	}

	// �� ��������� ����� � ����������� ������
	glm::ivec2 MatrixHelper::SceneToScreen(const glm::vec3 &scenePos, const Camera *camera)
	{
		return SceneToScreenAbs(SceneVector(scenePos) - camera->GetCameraTopLeftScenePos());
	}

	glm::ivec2 MatrixHelper::SceneToScreenAbs(const glm::vec3 &scenePos)
	{
		auto res = glm::ivec2(glm::round(glm::vec2(sceneToScreenMx * glm::vec4(scenePos, 1.f))));

		return res;
	}

	glm::vec2 MatrixHelper::SceneToScreenAbsF(const glm::vec3 &scenePos)
	{
		auto res = glm::vec2(sceneToScreenMx * glm::vec4(scenePos, 1.f));

		return res;
	}
	
	// � �������
	glm::vec3 MatrixHelper::ScreenToScene(const glm::vec2 &coords, const Camera *camera)
	{
		return ScreenToSceneAbs(coords + camera->GetCameraTopLeftScenePos().ToScreenCoords());
	}

	glm::vec3 MatrixHelper::ScreenToSceneAbs(const glm::vec2 &coords)
	{
		return glm::vec3(screenToSceneMx * glm::vec4(coords, 0.f, 1.f));
	}

	bool MatrixHelper::IsValid(const glm::mat3 &m)
	{
		if (SceneVector(m[0]).IsValid() || SceneVector(m[1]).IsValid() || SceneVector(m[2]).IsValid())
			return false;
		else
			return true;
	}

	// ������� ��������� ����� � ���������� �����������
	SceneVector MatrixHelper::SceneToViewer(const SceneVector &sceneCoords)
	{
		return coreViewMx * static_cast<glm::vec4>(sceneCoords);
	}

	// �������
	SceneVector MatrixHelper::ViewerToScene(const SceneVector &viewerCoords)
	{
		return glm::inverse(coreViewMx) * static_cast<glm::vec4>(viewerCoords);
	}
}