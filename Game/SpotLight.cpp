#include "stdafx.h"

#include "SpotLight.h"
#include "GameplayState.h"

namespace mg
{
	void SpotLightComponentManager::AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		auto colorTable = LuaPlus::LuaHelper::GetTable(table, "color");
		auto positionOffsetTable = LuaPlus::LuaHelper::GetTable(table, "positionOffset", false);

		SceneVector positionOffset(0.f);
		if (!positionOffsetTable.IsNil())
			positionOffset = SceneVector::FromMapCoords(positionOffsetTable);

		entity.assign<SpotLight>(
			LuaPlus::LuaHelper::GetFloat(table, "angle", false, glm::half_pi<float>()),
			LuaPlus::LuaHelper::GetFloat(table, "radius"),
			glm::vec3(
				LuaPlus::LuaHelper::GetFloat(colorTable, "r"),
				LuaPlus::LuaHelper::GetFloat(colorTable, "g"),
				LuaPlus::LuaHelper::GetFloat(colorTable, "b")
			),
			LuaPlus::LuaHelper::GetBoolean(table, "dynamic"),
			LuaPlus::LuaHelper::GetBoolean(table, "active", false, true),
			positionOffset
		);
	}
	
	LuaPlus::LuaObject SpotLightComponentManager::ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		/*auto dirLight = entity.component<DirectionalLight>();
		
		dirLight->angle = LuaPlus::LuaHelper::GetFloat(table, "angle", false, dirLight->angle);*/

		return GetComponentTable(entity);
	}
	
	LuaPlus::LuaObject SpotLightComponentManager::GetComponentTable(entityx::Entity &entity) const
	{
		auto dirLight = entity.component<SpotLight>();
		LuaPlus::LuaObject res;

		res.AssignNewTable(gps->GetLuaState());
		res.SetNumber("angle", dirLight->angle);

		return res;
	}
}