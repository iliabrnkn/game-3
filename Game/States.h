#pragma once

#include <string>

using namespace std;

namespace mg
{
	const double FRAME_DURATION = 1.0 / 60.0;
	const double FRAME_DURATION_GUI = 1.0 / 20.0;
	const string GAMEPLAY_STATE = "GameplayState";
}