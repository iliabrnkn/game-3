#pragma once

#include <vector>

#include "Vector.h"
#include "ComponentManager.h"

namespace mg {

	struct Trace
	{
		SceneVector startPoint;
		SceneVector endPoint;

		Trace(const SceneVector &startPoint, const SceneVector &endPoint)
			: startPoint(startPoint), endPoint(endPoint)
		{
		}
	};

	struct Tracer
	{
		Tracer()
		{
		}

		~Tracer()
		{}

		std::vector<Trace> traces;
		float maxTraceLen;
	};

	class GameplayState;

	class TracerComponentManager : public ComponentManager
	{
	public:
		TracerComponentManager(GameplayState *gps) : ComponentManager("tracer", gps) {};
		~TracerComponentManager() = default;

		virtual void AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const override;
		virtual LuaPlus::LuaObject GetComponentTable(entityx::Entity& entity) const override;
		virtual LuaPlus::LuaObject ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const override;
		void DrawTrace(const LuaPlus::LuaObject &entity, const LuaPlus::LuaObject &endPoint) const;
	};
}