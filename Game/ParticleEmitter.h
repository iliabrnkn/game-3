#pragma once

#include "stdafx.h"

#include <vector>
#include <stdlib.h>
#include <time.h>

#include <glm/glm.hpp>

#include "VertTypes.h"
#include "ComponentManager.h"

namespace mg
{
	struct ParticleEmitter
	{
		int maxParticles;
		float maxLifeTime;
		size_t lastUnusedParticleID;
		float particlesPerSec;
		float startParticleSize;
		float endParticleSize;
		float startVelocityFactor;
		float endVelocityFactor;
		glm::vec4 startParticleColor;
		glm::vec4 endParticleColor;
		/*int minGid;
		int maxGid;*/
		glm::vec2 singleParticleTexRatio;
		bool active;
		bool blur;
		bool original;
		bool depleted;
		int emitterID;

		std::vector<Particle> particles;
		LuaPlus::LuaObject particleRespawnFunc;

		ParticleEmitter(const std::vector<Particle> &particles, 
			const float &maxLifeTime,
			const float &startParticleSize, const float &endParticleSize, 
			const float &startVelocityFactor, const float &endVelocityFactor, 
			//const int &minGid, const int &maxGid, 
			const glm::vec2 &singleParticleTexRatio,
			const LuaPlus::LuaObject &particleRespawnFunc, 
			const glm::vec4 &startParticleColor, const glm::vec4 &endParticleColor,
			int emitterID,
			bool bloom = true, bool original = true, bool active = true) :
			lastUnusedParticleID(0), particles(particles), maxLifeTime(maxLifeTime),
			maxParticles(particles.size()), particlesPerSec(float(particles.size()) / maxLifeTime),
			startParticleSize(startParticleSize), endParticleSize(endParticleSize),
			startVelocityFactor(startVelocityFactor), endVelocityFactor(endVelocityFactor), 
			singleParticleTexRatio(singleParticleTexRatio),
			particleRespawnFunc(particleRespawnFunc),
			startParticleColor(startParticleColor), endParticleColor(endParticleColor),
			emitterID(emitterID),
			blur(bloom), original(original), active(active), depleted(false)
		{
		}
	};
	/*
	for (int i = 0; i < TEST_PARTICLES_COUNT; ++i)
		{
			ParticlePoint particle(1.f, 1.f);

			auto randX = 1.f / static_cast<float>(rand() % 100);
			auto randY = 1.f / static_cast<float>(rand() % 100);
			auto randZ = 1.f / static_cast<float>(rand() % 100);

			particle.pos = glm::vec3(randX, randY, randZ);
			particle.topLeftTexCoord = glm::vec2(
				static_cast<float>(rand() % 3) * 0.25f,
				static_cast<float>(rand() % 3) * 0.25f
			);

			particle.texSize = glm::vec2(0.25f, 0.25f);
			particles.push_back(particle);

			inds.push_back(i);
		}
	*/

	class ParticleSystem;

	class ParticleEmitterComponentManager : public ComponentManager
	{
		friend class ParticleSystem;

		static int emitterCount;
	
	public:
		ParticleEmitterComponentManager(GameplayState *gps) : ComponentManager("particleEmitter", gps) {};
		~ParticleEmitterComponentManager() = default;

		virtual void AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const override;
		virtual LuaPlus::LuaObject GetComponentTable(entityx::Entity& entity) const override;
		virtual LuaPlus::LuaObject ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const override;

		static inline void EmitterCountDecrement()
		{
			emitterCount--;
		}

		// �������� ������ ���/����
		void EnableParticleEmitter(const LuaPlus::LuaObject &entity, const bool &enabled);
	};
}