#pragma once

#include <unordered_map>
#include <memory.h>
#include "ApplicationState.h"
#include <entityx\entityx.h>
#include <SFML\Window.hpp>
#include <lua\LuaPlus.h>

#include "Camera.h"
#include "ModelManagers.h"
#include "AnimationManager.h"
#include "Controls.h"
#include "Scene.h"
#include "Physics.h"
#include "InventoryWindow.h"
#include "ObjectProps.h"

namespace mg {

	class Core;
	class Level;
	class Effect;

	const std::string WIDGET_INVENTORY_NAME = "Inventory";

	class GameplayState : public ApplicationState, public entityx::EntityX
	{
		friend class Core;

		std::shared_ptr<Camera> camera;
		std::unique_ptr<Level> level;
		std::unordered_map<size_t, std::unique_ptr<Factory>> componentFactories;
		entityx::Entity::Id playerID;
		unsigned int currentVersion;
		LuaPlus::LuaState* luaState;
		Scene scene;
		bool isBakingPass;
		Physics physics;
		InventoryWindow* inventory;
		//ActiveItemsWindow* activeItems;
		std::vector<GidProps> gidProps;
		double timeFactor;

	public:
		GameplayState(LuaPlus::LuaState* luaState, std::shared_ptr<Camera> cameraInit);
		~GameplayState();

		void LoadResources(const char *cPath);
		void Update(float delta, sf::Window *window) override;
		void Init() override;
		entityx::Entity GetPlayerEntity();
		LuaPlus::LuaState* GetLuaState() const;
		LuaPlus::LuaFunctionVoid OnSceneUpdated, OnSceneUpdate;

		void Free();

		// ��������� ������� �����
		inline void SetGidProperties(const int gid, const GidProps &properties)
		{
			if (gidProps.size() < gid + 1)
			{
				gidProps.resize(gid + 1);
			}

			gidProps[gid] = properties;
		}

		inline std::vector<GidProps> GetGidProperties() const
		{
			return gidProps;
		}

		// -- entities

		entityx::Entity GetEntity(const entityx::Entity::Id& id);
		entityx::Entity GetEntity(const unsigned int& id);
		entityx::Entity GetEntity(const LuaPlus::LuaObject& table);

		std::shared_ptr<Camera> GetCamera();

		const std::shared_ptr<Camera> GameplayState::GetCamera() const;

		void SetCurrentVersion(const unsigned int& versionInit);

		template<typename ElementType, typename FactoryType>
		inline void AddComponentFactory()
		{
			componentFactories[sizeof(ElementType)] = move(unique_ptr<ComponentFactory<ElementType>>(new FactoryType()));//move(make_unique<ComponentFactory<ElementType>>);
		}

		template<typename ElementType, typename FactoryType>
		inline FactoryType* GetComponentFactory()
		{
			auto factoryIter = componentFactories.find(sizeof(ElementType));
			assert(factoryIter != componentFactories.end());

			return dynamic_cast<FactoryType*>((ComponentFactory<ElementType>*)factoryIter->second.get());
		}

		template<typename ElementType>
		inline ComponentFactory<ElementType>* GetComponentFactory()
		{
			auto factoryIter = componentFactories.find(sizeof(ElementType));
			assert(factoryIter != componentFactories.end());

			ComponentFactory<ElementType> *factoryPtr = (ComponentFactory<ElementType>*)factoryIter->second.get();

			return factoryPtr;
		}

		// ���������� ��������� ������� �������
		template<typename ElementType>
		inline std::shared_ptr<ElementType> CreateElement(const std::string& elementName, const int &id = -1)
		{
			return std::static_pointer_cast<ElementType, Cloneable>((GetComponentFactory<ElementType>()->CreateElement(elementName, this, id)));
		}

		// ���������, ���������� �� ����� ������� � �����������
		template<typename ElementType>
		inline bool HasElement(const std::string& elementName)
		{
			return GetComponentFactory<ElementType>()->HasElement(elementName);
		}

		virtual void PrepareDeferredRendering(Effect& fboEffect) override;
		void AssociateGIDWProps(const int &gid, const char *cModelName, const LuaPlus::LuaObject &texOffset, 
			const LuaPlus::LuaObject &normalTexOffsetFactor);
		void LoadLevel(const LuaPlus::LuaObject& luaObject);
		Level* GetLevel();

		// ���������� ������ �� ������-�����
		Scene& GetScene()
		{
			return scene;
		}

		bool IsBakingPass() const;

		inline InventoryWindow* GetInventory() { return inventory; };

		Physics& GetPhysics();

		void SetTimeFactor(const double &newTimeFactor);
	};

}