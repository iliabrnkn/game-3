#pragma once

#include <glm\glm.hpp>

#include "Vector.h"
#include "ComponentManager.h"

namespace mg
{
	struct DirectionalLight
	{
		bool enabled;
		glm::vec3 color;
		glm::vec2 cloudTexOffset, windVelocity;

		DirectionalLight(const bool &enabled, const glm::vec3 &color, const glm::vec2 &windVelocity) :
			enabled(enabled), color(color), cloudTexOffset(0.f), windVelocity(windVelocity) {}
	};

	// DirectionalLightComponentManager

	class DirectionalLightComponentManager : public ComponentManager
	{
	public:
		DirectionalLightComponentManager(GameplayState *gps) : ComponentManager("directionalLight", gps) {};
		~DirectionalLightComponentManager() = default;

		virtual void AssignComponent(entityx::Entity &entity, const LuaPlus::LuaObject &table) const override;
		virtual LuaPlus::LuaObject GetComponentTable(entityx::Entity& entity) const override;
		virtual LuaPlus::LuaObject ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const override;

		void EnableDirectionalLight(const LuaPlus::LuaObject &entity, const bool &enabled) const;
		bool IsDirectionalLightEnabled(const LuaPlus::LuaObject &entity) const;
		void SetDirectionalLightColor(const LuaPlus::LuaObject &entity, const LuaPlus::LuaObject &color) const;
	};
}