#pragma once

#include <unordered_map>

#include <bullet/btBulletDynamicsCommon.h>
#include <bullet/BulletCollision/CollisionDispatch/btGhostObject.h>

#include "GUI.h"
#include "Masks.h"
#include "Vector.h"

namespace mg
{
	/*struct RigidBody : public btRigidBody
	{
		SceneVector forcedLinearVelocity;
		bool loose;
		bool collidesWithOtherEntities;

		RigidBody(const btRigidBodyConstructionInfo& constructionInfo);
		RigidBody(btScalar mass, btMotionState* motionState, btCollisionShape* collisionShape, const btVector3& localInertia = btVector3(0, 0, 0));

		inline const RigidBody* operator=(const btRigidBody* btRigidBody) const
		{
			return reinterpret_cast<const RigidBody*>(btRigidBody);
		}

		inline RigidBody* operator=(btRigidBody* btRigidBody)
		{
			return reinterpret_cast<RigidBody*>(btRigidBody);
		}

		inline operator btRigidBody*()
		{
			return this;
		}

		inline operator const btRigidBody*() const
		{
			return this;
		}

		virtual ~RigidBody() override;
	};*/
	typedef btRigidBody RigidBody;

	struct MotionState : public btDefaultMotionState
	{
		bool prevIterationMoving;

		MotionState(const btTransform& startTrans = btTransform::getIdentity(), const btTransform& centerOfMassOffset = btTransform::getIdentity());
		
		virtual void getWorldTransform(btTransform& centerOfMassWorldTrans) const override;
		
		virtual void setWorldTransform(const btTransform& centerOfMassWorldTrans) override;
	};

	struct Physics
	{
		const float RAY_ENLARGEMENT = 100.f;
		const size_t MAX_SERIALIZATION_BUFFER_SIZE = 1024 * 1024 * 5;

		btDefaultCollisionConfiguration* collisionConfiguration;
		btCollisionDispatcher* dispatcher;
		btDbvtBroadphase* overlappingPairCache;
		btSequentialImpulseConstraintSolver* solver;
		btDiscreteDynamicsWorld* dynamicsWorld;
		btAlignedObjectArray<btCollisionShape*> collisionShapes;

		unordered_map<btCollisionObject*, unsigned int> collideables;

		Physics();

		void Free();

		~Physics();

		void CollisionDetection();

		RigidBody* CreateRigidBody(const btScalar &mass, const btTransform& startTransform, btCollisionShape* shape,
			const btVector3 &customDebugColor = btVector3(0.0, 0.0, 0.0), const int &collisionFlags = -1,
			const int &group = COL_ALL, const int &mask = COL_ALL);

		RigidBody *GetBodyByWorldArrayIndex(const int &worldArrayIndex);
		int GetWorldArrayIndex(const RigidBody *body);

		int GetEntityIDByRay(const SceneVector &start, const SceneVector &end) const;
		SceneVector GetHitSceneCoordsByRay(const SceneVector &rayStart, const SceneVector &rayEnd) const;

		btGeneric6DofConstraint *CreateConstraint(
			btRigidBody *firstBody, btRigidBody *secBody,
			const SceneVector &frameAOrigin, const SceneVector &frameBOrigin,
			const btMatrix3x3 &frameABasisInitial, const btMatrix3x3 &frameBBasisInitial,
			const btVector3 &angularLowerLimits, const btVector3 &angularUpperLimits,
			const btVector3 &linearLowerLimits, const btVector3 &linearUpperLimits,
			const bool &axisShift = false, const bool &collision = false);

		btGeneric6DofConstraint *CreateConstraint(
			btRigidBody *firstBody, btRigidBody *secBody,
			const SceneVector &frameAOrigin, const SceneVector &frameBOrigin,
			const btVector3 &angularLowerLimits, const btVector3 &angularUpperLimits,
			const btVector3 &linearLowerLimits, const btVector3 &linearUpperLimits,
			const bool &axisShift = false, const bool &collision = false);

		//void WriteConstraintToFile(btGeneric6DofConstraint *cs, const std::string &filename);
	};

}