#pragma once

#include <entityx\System.h>
#include <memory>

#include "Effect.h"
#include "VBO.h"
#include "Position.h"
#include "Texture.h"
#include "VertTypes.h"
#include "Model.h"
#include "Batch3D.h"

namespace mg {

	class Camera;
	class GameplayState;

	// ����� ���������� ���, ��� ����������� �����, ��������� � ��� ���� ������ ���� � ���������� �� ������������

	class MeshBatchSystem : public entityx::System<MeshBatchSystem>
	{
		friend class GameplayState;

		LuaPlus::LuaState* luaState;
		bool recreateGeometryBatch;

	public:
		MeshBatchSystem(GameplayState* gps);
		~MeshBatchSystem();

		void configure(entityx::EventManager &events) override;
		void update(entityx::EntityManager &es, entityx::EventManager &em, entityx::TimeDelta dt) override;

		GameplayState* gps;
	};

}