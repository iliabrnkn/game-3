#pragma once

#include "ApplicationState.h"
#include "InventoryWindow.h"

namespace mg {

	class InventoryState : public ApplicationState
	{
		friend class Core;

		InventoryState() = default;
		~InventoryState() = default;

		virtual void Init();
		virtual void Update(float delta, sf::Window *window);
		virtual void PrepareFBO(Effect& fboEffect) {};

		InventoryWindow inventoryBackground;


	};

}