﻿#include "stdafx.h"

#include "TextureAtlas.h"
#include "ErrorLog.h"

namespace mg{

	TextureAtlas::TextureAtlas() :
		layerWidth(0), layerHeight(0), layerNum(0)
	{
		
	}

	TextureAtlas::~TextureAtlas(void)
	{
	}

	void TextureAtlas::DrawTilesetImage(sf::RenderTexture& atlasRenderTexture, TilesetImage& tilesetImage)
	{
		sf::Texture texture;
		texture.loadFromImage(tilesetImage.image);
		sf::Sprite currentSprite(texture);

		currentSprite.setPosition(0, 0);
		atlasRenderTexture.draw(currentSprite);
	}

	unsigned int TextureAtlas::FindNearestPot(unsigned int val)
	{
		for (unsigned int potToCompare = 1; potToCompare <= std::numeric_limits<unsigned int>::max();
			potToCompare = potToCompare << 1)
		{
			if (potToCompare >= val) return potToCompare;
		}
	}

	void TextureAtlas::Init()
	{
		for (auto& tilesetImg : tilesetImages)
		{
			int gidNum = tilesetImg.firstGid;
			int tileHeight = tilesetImg.tileHeight;

			for (int y = 0; y < static_cast<int>(tilesetImg.height); y += tileHeight)
			{
				for (int x = 0; x < static_cast<int>(tilesetImg.width); x += tilesetImg.tileWidth)
				{
					GidTex currentGidTex;

					currentGidTex.stUnrelatedLeftTop.x = static_cast<float>(x);
					currentGidTex.stUnrelatedLeftTop.y = static_cast<float>(y);

					currentGidTex.stUnrelatedLeftBottom.x = static_cast<float>(x);
					currentGidTex.stUnrelatedLeftBottom.y = static_cast<float>(y + tilesetImg.tileHeight);

					currentGidTex.stUnrelatedRightBottom.x = static_cast<float>(x + tilesetImg.tileWidth);
					currentGidTex.stUnrelatedRightBottom.y = static_cast<float>(y + tilesetImg.tileHeight);

					currentGidTex.stUnrelatedRightTop.x = static_cast<float>(x + tilesetImg.tileWidth);
					currentGidTex.stUnrelatedRightTop.y = static_cast<float>(y);

					currentGidTex.height = tilesetImg.tileHeight;
					currentGidTex.width = tilesetImg.tileWidth;

					currentGidTex.gidNum = gidNum;
					currentGidTex.texLayerNum = layerNum;

					//swap(gidTexes[gidNum], currentGidTex);
					gidTexes.insert(std::make_pair(gidNum++, currentGidTex));
				}
			}

			if (layerWidth < tilesetImg.width) layerWidth = tilesetImg.width;
			if (layerHeight < tilesetImg.height) layerHeight = tilesetImg.height;

			layerNum++;
		}

		for (auto &gidTex : gidTexes)
		{
			gidTex.second.layerHeight = layerHeight;
			gidTex.second.layerWidth = layerWidth;
		}
	}

	/*инициализирует двумерный массив текстур*/
	void TextureAtlas::InitTexture()
	{
		int offsetZ = 0;
		
		texture.Init2DArray(layerWidth, layerHeight, layerNum);
		
		for (auto& tilesetImg : tilesetImages)
		{
			texture.InitSubImage(0, 0, offsetZ++, tilesetImg.image.getSize().x, tilesetImg.image.getSize().y, 1, tilesetImg.image.getPixelsPtr());
		}
	}

	void TextureAtlas::AddTexture(
		const int& firstGidInit,
		const int& singleYOffsetInit,
		const int& tileCountInit,
		const string& tilesetFilename,
		const unsigned int& tileWidth,
		const unsigned int& tileHeight)
	{
		TilesetImage tilesetImage(tilesetFilename);
		tilesetImage.firstGid = firstGidInit;
		tilesetImage.singleTileYOffset = singleYOffsetInit;
		
		if (tileWidth != 0)
			tilesetImage.tileWidth = tileWidth;
		else
			tilesetImage.tileWidth = tilesetImage.width;

		if (tileHeight != 0)
			tilesetImage.tileHeight = tileHeight;
		else
			tilesetImage.tileHeight = tilesetImage.height;

		tilesetImage.tileCount = tileCountInit;

		tilesetImages.push_back(tilesetImage);

		//currentAtlasSize += tilesetImage.width * tilesetImage.height * 4;
	}

	Texture* TextureAtlas::GetTexture()
	{
		return &texture;
	}

	/*sf::Image* TextureAtlas::GetAtlasImage()
	{
		return &image;
	}*/

	void TextureAtlas::SetGidDrawingOffset(int gidNum, glm::vec2 offset)
	{
		drawingOffsetsByGidNums.insert(make_pair(gidNum, offset));
	}
	
	GidTex* TextureAtlas::GetGidTex(int gidNum)
	{
		auto iter = gidTexes.find(gidNum);
		if (iter != gidTexes.end())
			return &(iter->second);
		else return nullptr;
	}
}