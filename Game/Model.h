#pragma once

#include <map>
#include <vector>
#include <deque>
#include <string>
#include <unordered_map>
#include <algorithm>
#include <glm/gtx/quaternion.hpp>

#include "ModelStruct.h"
#include "Cloneable.h"
#include "ComponentManager.h"

namespace mg {

	const float MODEL_SCALE = 0.037f;

	struct Mesh : public Cloneable
	{
		float scale;
		map<std::string, Bone> bones;
		std::vector<glm::mat4> boneValues;
		std::vector<string> texNames;

		size_t vertOffset, vertsCount, indsOffset, indsCount;

		unsigned int ID;
		std::string name;

		float texOffsetX;
		float texOffsetY;
		bool removed;

		std::vector<glm::mat4>& GetBoneMatrices()
		{
			return boneValues;
		}
	};

	struct MeshBatch
	{
		std::vector<Mesh> meshes;
		float scale;
		glm::vec4 material;
		glm::mat4 initialTransform;
	};

	class MeshBatchComponentManager : public ComponentManager
	{
	public:
		MeshBatchComponentManager(GameplayState *gps) : ComponentManager("meshBatch", gps) {};
		~MeshBatchComponentManager() = default;

		virtual void AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const override;
		virtual LuaPlus::LuaObject GetComponentTable(entityx::Entity& entity) const override;
		virtual LuaPlus::LuaObject ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const override;
	};

}