#pragma once

#include <vector>

#include "AStar.h"
#include "Vector.h"
#include "GameplayState.h"

namespace mg {

	struct NavigationRect
	{
		SceneVector boundingMin;
		SceneVector boundingMax;
		std::vector<AStarNodeHandler> nodes;

		NavigationRect(SceneVector boundingMin, SceneVector boundingMax) :
			boundingMin(boundingMin), boundingMax(boundingMax)
		{}

		void inline AddNode(AStarNodeHandler node)
		{
			// ���� � �������������� ��� ���� ���� � ����� �� �����������, ����������
			if (std::find_if(nodes.begin(), nodes.end(),
				[node](auto x) { return node->pos == x->pos; }) != nodes.end())
				return;

			for (auto &currNode : nodes)
			{
				node->GetNeighbours().push_back(currNode);
				currNode->GetNeighbours().push_back(node);
			}

			nodes.push_back(node);
		}

		void inline RemoveNode(AStarNodeHandler node)
		{
			auto found = std::remove_if(nodes.begin(), nodes.end(),
				[node](auto x) { return x == node; });

			if (found != nodes.end())
			{
				for (auto neighbour : node->neighbours)
				{
					neighbour->neighbours.erase(std::remove_if(
						neighbour->neighbours.begin(), neighbour->neighbours.end(), [node](auto x) { return node == x; })
					);
				}

				nodes.erase(found);
			}
		}
	};

	class NavigationMesh
	{
		friend class Level;

		std::vector<NavigationRect> navRects;

	public:
		void Init(GameplayState* gps, const LuaPlus::LuaObject &table);
		std::vector<SceneVector> FindPath(const SceneVector& startPoint, const SceneVector& endPoint);
		NavigationRect *GetNavRectContainingPoint(const SceneVector &point);

		std::vector<SceneVector> ReconstructPath(AStarNodeHandler node);
		AStarNodeHandler FindClosestNode(const SceneVector &point);
	};
}