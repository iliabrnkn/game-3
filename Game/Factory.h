#pragma once

#include <memory>
#include <string>

#include "Cloneable.h"

namespace mg
{
	class GameplayState;

	class Factory
	{
	public:
		virtual ~Factory() = 0 {}
	};

	//-----------BinaryFileLoader------------

	class BinaryFileLoader
	{
	protected:
		inline std::string ReadStringFromFile(std::ifstream &file) const
		{
			// string length without terminator character
			size_t length;

			file.read((char*)&length, sizeof(size_t));

			char* cStr = new char[length];
			file.read(cStr, sizeof(char) * (length));
			std::string res(cStr);

			delete cStr;

			return res;
		}

	public:
		virtual void LoadFromFile(const std::string& filename) = 0;
		virtual ~BinaryFileLoader() = 0 {}
	};

	// ------------------------------------

	template <typename T>
	class ComponentFactory : public Factory, public BinaryFileLoader
	{
		static_assert(std::is_base_of<Cloneable, T>::value, "T should inherit from Cloneable");

	protected:
		std::unordered_map<std::string, std::shared_ptr<Cloneable>> elements;

	public:

		virtual std::shared_ptr<Cloneable> CreateElement(const std::string& componentName, GameplayState *gps, const int &id = -1) = 0;

		inline bool HasElement(const std::string& elementName) const
		{
			if (elements.find(elementName) != elements.end())
				return true;
			else
				return false;
		}

		inline std::shared_ptr<Cloneable> &GetPrototype(const std::string &elementName)
		{
			return elements[elementName];
		}

		virtual ~ComponentFactory() = 0 {}
	};
}