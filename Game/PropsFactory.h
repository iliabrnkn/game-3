#pragma once

#include <string>
#include <unordered_map>
#include <memory>
#include <fstream>

#include "Props.h"
#include "Factory.h"

namespace mg
{
	class PropsFactory : public ComponentFactory<Props>
	{
	public:
		PropsFactory() : 
			ComponentFactory<Props>() {}

		virtual ~PropsFactory() {}

		virtual std::shared_ptr<Cloneable> CreateElement(const std::string& componentName, GameplayState *gps, const int &id = -1) override;
		virtual void LoadFromFile(const std::string& filename) override;
	};
}