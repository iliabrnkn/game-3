#include "stdafx.h"

#include "DirectionalLightSystem.h"
#include "DirectionalLight.h"
#include "Direction.h"
#include "GameplayState.h"
#include "Sizes.h"
#include "Renderer.h"
#include "Core.h"
#include "Level.h"
#include "MathHelper.h"

namespace mg {
	DirectionalLightSystem::DirectionalLightSystem(GameplayState* gameplayState)
		:gps(gameplayState)
	{}

	void DirectionalLightSystem::configure(entityx::EventManager &events)
	{}

	void DirectionalLightSystem::update(entityx::EntityManager &es, entityx::EventManager &em, entityx::TimeDelta dt)
	{
		//auto mvp = gps->GetCamera()->GetDirLightMVP();
		/*auto mvp = glm::infinitePerspective(glm::pi<float>() * 2.f / 3.f, 1.f, 0.01f) * glm::lookAt(
			glm::vec3(1.f, 10.f, 0.f), glm::vec3(1.f, 0.f, 0.f), glm::vec3(0.f, 0.f, -1.f));*/

		/*glm::vec4 eye = gps->GetCamera()->GetDirLightWorldViewMatrix() * glm::vec4(1.f, 1.f, 1.f, 1.f);
		glm::vec4 ndc = MatrixHelper::dirLightProjectionMx * eye;*/

		es.each<DirectionalLight, Direction>([dt, this](entityx::Entity& entity, DirectionalLight &light, Direction &dir)
		{
			glm::mat4 mvp =
				MatrixHelper::dirLightProjectionMx *
				glm::scale(glm::vec3(TILE_WIDTH_SCENE, TILE_WIDTH_SCENE, TILE_WIDTH_SCENE)) *
				glm::translate(glm::vec3(DIRECTIONAL_SHADOWMAP_WIDTH / (2.f * TILE_WIDTH_SCENE), DIRECTIONAL_SHADOWMAP_HEIGHT / (2.f * TILE_WIDTH_SCENE), 0.f)) *
				glm::rotate(dir.YZ().Normalized().Angle(SceneVector(0.f, 0.f, 1.f), SceneVector(1.f, 0.f, 0.f)), glm::vec3(1.f, 0.f, 0.f)) * 
				glm::rotate(dir.XZ().Normalized().Angle(SceneVector(0.f, 0.f, 1.f), SceneVector(0.f, 1.f, 0.f)), glm::vec3(0.f, 1.f, 0.f)) *
				glm::translate(
					-glm::vec3(gps->GetCamera()->GetCameraCenterScenePos())
				);

			gps->GetScene().GetDirectionalLightEffect().SetUniformMatrix4fDeferred("mvp", mvp);
			Core::GetInstance()->GetRenderer()->GetDeferredRenderingEffect().SetUniformMatrix4fDeferred("directionalLightMVP", mvp);
			Core::GetInstance()->GetRenderer()->GetDeferredRenderingEffect().SetUniform3fDeferred("directionalLightColor", light.color);
			
			char postfix = 'A';
			for (auto &atlas : gps->GetLevel()->GetSpriteAtlases())
			{
				string varname = "spriteAtlas";
				varname.push_back(postfix++);
				gps->GetScene().GetDirectionalLightEffect().SetUniform1iDeferred(varname,
					atlas.GetTexture()->GetTextureUnit());
			}
			
			gps->GetScene().GetCloudEffect().SetUniform1iDeferred("cloudTex", gps->GetLevel()->GetCloudAtlas().GetTexture()->GetTextureUnit());
			gps->GetScene().GetCloudEffect().SetUniformMatrix4fDeferred("mvp", gps->GetCamera()->GetSceneMVP());
			
			// �������� �������
			light.cloudTexOffset += light.windVelocity * static_cast<float>(dt);

			glm::vec2 cloudsCoordsMax = gps->GetLevel()->GetCloudTexCoordSize();

			if (light.cloudTexOffset.x > cloudsCoordsMax.x)
				light.cloudTexOffset.x -= cloudsCoordsMax.x;

			if (light.cloudTexOffset.y > cloudsCoordsMax.y)
				light.cloudTexOffset.y -= cloudsCoordsMax.y;

			gps->GetScene().GetCloudEffect().SetUniform2fDeferred("cloudTexOffset", light.cloudTexOffset);

			// ����������, ����� �������� ����� ����� �������������� ��� ������������� ����� (��, ��� ����� ������������ � ����� ������ ������)
			
			gps->GetScene().GetMainSceneDrawAdditionalInfo().sceneSegmentsToDraw.clear();
		});
	}
}