#pragma once

#include <SFML\Graphics.hpp>
#include <lua\LuaPlus.h>

#include "Widget.h"
#include "Paths.h"
#include "ItemGrid.h"
#include "InventoryItem.h"

namespace mg
{
	class InventoryWindow : public Widget
	{
	public:
		InventoryWindow(const WidgetParameters& params, const std::string& name);

		~InventoryWindow();

		// �������� �������� ����� � ����, ���� �� ����������, ���������� false

		bool AddItem(const LuaPlus::LuaObject& itemTable);
	};
}