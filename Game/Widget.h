#pragma once

#include <string>
#include <vector>
#include <deque>
#include <memory>
#include <unordered_map>

#include <SFML\Graphics.hpp>
#include <lua\LuaPlus.h>

namespace mg
{
	const sf::Color GUI_COLOR_BRIGHTEST = sf::Color(49, 138, 189);
	const sf::Color GUI_COLOR_BRIGHT = sf::Color(35, 99, 135);
	const sf::Color GUI_COLOR_MEDIUM = sf::Color(24, 67, 91);
	const sf::Color GUI_COLOR_DIM = sf::Color(16, 45, 61);

	const sf::Color GUI_COLOR_BACKGROUND = sf::Color(23, 23, 23, 200);
	const sf::Color GUI_COLOR_TRANSPARENT = sf::Color(0, 0, 0, 0);
	const int CORNER_OFFSET = 2;
	const sf::Vector2i CONTENT_OFFSET(10, 5);
	const int DEFAULT_CORNER_SIDE_LENGTH = 5;
	const int INVENTORY_CELL_SIDE_LENGTH = 31;

	struct WidgetParameters
	{
		int width;
		int height;
		sf::Vector2i origin;
		std::string text;
		std::string imgPath;
		bool visible;
		bool displayable;
		LuaPlus::LuaObject objectTable;

		WidgetParameters() {}
		
		~WidgetParameters() {}

		/*������ �����������*/
		WidgetParameters(const int& width,
			const int& height,
			const sf::Vector2i& origin,
			const std::string& text,
			const std::string& imgPath,
			const LuaPlus::LuaObject& objectTable,
			const bool& visible,
			const bool& displayable):
				width(width),
				height(height),
				origin(origin),
				text(text),
				imgPath(imgPath),
				visible(visible),
				displayable(displayable),
				objectTable(objectTable)
		{}

		/*�������� �����������*/
		WidgetParameters(const int& width, const int& height, const sf::Vector2i& origin = sf::Vector2i(0,0), const bool& displayable = true, const bool& visible = true) :
			width(width), height(height), origin(origin), displayable(displayable), visible(visible)
		{}

		/*��� ����������� ��������� � ���������*/
		WidgetParameters(const int& width, const int& height, const sf::Vector2i& origin,
			const std::string& imgPath, const LuaPlus::LuaObject& objectTable, const bool& displayable = true, const bool& visible = true) :
			width(width),
			height(height),
			origin(origin),
			imgPath(imgPath),
			objectTable(objectTable),
			displayable(displayable),
			visible(visible)
		{}

		/*��� �������� �������� ���������*/
		WidgetParameters(const LuaPlus::LuaObject params) :
			width(params["width"].ToNumber()),
			height(params["height"].ToNumber()),
			origin(params["origin"]["x"].ToNumber(), params["origin"]["y"].ToNumber()),
			text(params["text"].ToString()),
			imgPath(params["imgPath"].ToString()),
			objectTable(params["objectTable"]),
			visible("visible"),
			displayable("displayable")
		{}
	};

	class Widget
	{
		friend class GUI;
	
	protected:

		// ���-�����������, ���� ����� �����������

		LuaPlus::LuaObject onClickHandler;
		int zOrder; // ������� ���������, ��� �� ������ - ��� �������������� ����
		bool visibility;
		bool displayability;
		int id;

		bool removed;

		WidgetParameters startParams;

		std::unordered_map<std::string, LuaPlus::LuaObject> eventHandlers;

	public:
		Widget(const WidgetParameters& params, const std::string& name, const int &zOrderInitial = 100);
		~Widget();
		
		sf::Sprite& GetSprite();

		void ClearRemoved();
	
		void SetOrigin(const sf::Vector2f& origin);
		void SetSize(const int& width, const int& height);
		virtual void Draw(sf::RenderWindow& window, sf::Shader &shader, sf::Vector2i &relativeCoords = sf::Vector2i(0, 0));
		void SetBackgroundImg(const std::string& backgroundImagePath, const sf::IntRect& rect = sf::IntRect());
		void Init();
		
		virtual bool Widget::HandleMouse(const sf::Vector2i& absCoords); // �������� ������ �����������
		virtual void OnClick(const sf::Vector2i& relativeCoords) {}; // ����������� ���� ��� ��� ������, ����� ��������� ����������� OnDrag
		sf::Vector2i GetAbsPos();
		
		// ����������� ���� ��� ����� ���� �������� ��� ���������
		virtual void OnMouseEnter() {};
		// ����������� ���� ��� ����� ���� ������ � ��������
		virtual void OnMouseLeave() {};
		// ����������� ������ ��� ��� ����� ��������� ��� ���������
		virtual void OnMouseOver(const sf::Vector2i& relativeCoords) {};
		// ����
		virtual void OnDrag(const sf::Vector2i& mouseAbsCoords) {};
		// � ����
		virtual void OnDrop(const sf::Vector2i& mouseAbsCoords) {};

		template <typename T>
		T* GetChild(const std::string& name)
		{
			auto iter = std::find_if(children.begin(), children.end(), [&name](auto& x) { return x->name == name; });

			if (iter != children.end())
			{
				return dynamic_cast<T*>((*iter).get());
			}
			else
			{
				for (auto& child : children)
				{
					return child->GetChild<T>(name);
				}
			}

			return nullptr;
		}

		template <typename T>
		T* GetChild(const int& widgetID)
		{
			auto iter = std::find_if(children.begin(), children.end(), [&widgetID](auto& x) { return x->ID == widgetID; });

			if (iter != children.end())
			{
				return dynamic_cast<T*>((*iter).get());
			}
			else
			{
				for (auto& child : children)
				{
					return child->GetChild<T>(widgetID);
				}
			}

			return nullptr;
		}

		template <typename T>
		T* GetChildByCoords(const sf::Vector2i &searchCoords, const sf::Vector2i &currWidgetAbsCoords)
		{
			T* res = nullptr;

			for (auto &child : children)
			{
				auto nextAbsCoords = child->origin + currWidgetAbsCoords;

				// ���� ����� ����������� ������� ������ ��������� ����, ������� �������� � ���� ������ �����

				if (sf::IntRect(nextAbsCoords.x, nextAbsCoords.y, child->width, child->height).contains(searchCoords))
				{
					res = dynamic_cast<T*>(child.get());

					if (!res)
					{
						res = child->GetChildByCoords<T>(searchCoords, nextAbsCoords);
					}

					if (res) break;
				}
			}

			return res;
		}

		template <typename T>
		T* AddChild(const WidgetParameters& params, const std::string& name)
		{
			auto ptr = std::unique_ptr<Widget>(new T(params, name));
			auto* res = ptr.get();

			res->SetZOrder(zOrder + 1);
			res->SetParent(this);

			children.push_back(std::move(ptr));

			return dynamic_cast<T*>(res);
		}

		template <typename T>
		T* AddChild(std::unique_ptr<T> &childWidget)
		{
			auto child = static_cast<std::unique_ptr<Widget>>(std::move(childWidget));

			child->SetZOrder(zOrder + 1);
			child->SetParent(this);

			children.push_back(std::move(child));

			return dynamic_cast<T*>(children[children.size() - 1].get());
		}

		template <typename T>
		std::unique_ptr<T> Clone(const std::string &name)
		{
			return std::unique_ptr<T>(new T(startParams, name));
		}

		void SetOnTop();

		void Highlight();
		void Fade();
		std::vector<std::unique_ptr<Widget>>::iterator RemoveChild(const int &widgetID);

		sf::Vector2i GetOrigin()  const;
		sf::Vector2i GetSize()  const;

		void SetZOrder(const int& z);
		int GetMaxZOrder() const;
		void DoReSort();
		void SetVisibility(const bool& visible);
		bool IsVisible() const;
		void SetDisplayability(const bool& display);
		bool IsDisplayable() const;
		std::string GetName() const;

		inline int GetID() const
		{
			return id;
		}

		inline bool IsRemoved() const
		{
			return removed;
		}

		inline void Remove(const bool &isRemoved = true)
		{
			removed = isRemoved;
		}

		template<typename T>
		T* GetParent()
		{
			return dynamic_cast<T*>(parent);
		}

		void SetEventHandler(const std::string& eventHandlerName, const LuaPlus::LuaObject &eventHandler);
		void FireEvent(const std::string &eventHandlerName, LuaPlus::LuaObject &arg);

	protected:
		void SetBackgroundImg(const sf::Image& img, const sf::IntRect& rect);
		void DrawCorner(const sf::Vector2i& startPoint, const int& dx, const int& dy, const int& lineWidth, sf::Image& img, const sf::Color& color);
		void DrawBox(const sf::Color& bordersColor, const sf::Color& cornersColor, const int& cornerSideLength = DEFAULT_CORNER_SIDE_LENGTH, int boxWidth = -1, int boxHeight = -1, const sf::Vector2i& origin = sf::Vector2i(0, 0));
		
		inline void SetParent(Widget *parentWidget)
		{
			parent = parentWidget;
		}

		Widget* parent;
		std::string name;
		std::vector<std::string> childrenNames;

		sf::Image background;
		int width;
		int height;
		sf::Vector2i origin;
		bool mouseHover;
		bool dragged;
		bool doReSort;

		sf::Sprite sprite;
		sf::Texture texture;

		std::vector<std::unique_ptr<Widget>> children;
	};
}