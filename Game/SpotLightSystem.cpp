#include "stdafx.h"

#include "SpotLightSystem.h"

#include "Sizes.h"
#include "LightPoint.h"
#include "SpotLight.h"
#include "Direction.h"
#include "Position.h"
#include "Collideable.h"
#include "Core.h"
#include "Paths.h"
#include "GameplayState.h"
#include "Level.h"
#include "MathHelper.h"
#include "Scene.h"

namespace mg {

	SpotLightSystem::SpotLightSystem(GameplayState* gameplayState)
		:gps(gameplayState)
	{}

	void SpotLightSystem::configure(entityx::EventManager &events)
	{
		// static lights
		staticShadowMXsBuffer.Init((LIGHT_MAX_DYNAMIC_DIRECT_LIGHTS + LIGHT_MAX_STATIC_DIRECT_LIGHTS) * sizeof(glm::mat4), GL_UNIFORM_BUFFER);
		staticLightsTransformsBufferTex.InitBuffer();
		staticShadowMXsBuffer.Bind();
		staticLightsTransformsBufferTex.BindBuffer(staticShadowMXsBuffer.GetID(), GL_RGBA32F);

		// mvp matrices
		dynamicShadowMXsBuffer.Init((LIGHT_MAX_DYNAMIC_DIRECT_LIGHTS + LIGHT_MAX_STATIC_DIRECT_LIGHTS) * sizeof(glm::mat4), GL_UNIFORM_BUFFER);
		dynamicShadowMXsBufferTex.InitBuffer();
		dynamicShadowMXsBuffer.Bind();
		dynamicShadowMXsBufferTex.BindBuffer(dynamicShadowMXsBuffer.GetID(), GL_RGBA32F);

		// light props
		dirLightProps.Init((LIGHT_MAX_DYNAMIC_DIRECT_LIGHTS + LIGHT_MAX_STATIC_DIRECT_LIGHTS) * sizeof(LightProps), GL_UNIFORM_BUFFER);
	}

	void SpotLightSystem::update(entityx::EntityManager &es, entityx::EventManager &em, entityx::TimeDelta dt)
	{
		std::vector<glm::mat4> lightMVPs;

		int staticDirLightsCount = 0, dirLightsCount = 0;

		std::vector<LightProps> dirLightProperties;
		dirLightProperties.resize(LIGHT_MAX_DYNAMIC_DIRECT_LIGHTS + LIGHT_MAX_STATIC_DIRECT_LIGHTS);
		//staticShadowTransforms.clear();
		lightMVPs.clear();

		// spotlights
		es.each<SpotLight, Direction, Position>(
			[&dt, &es, this, &lightMVPs, &staticDirLightsCount, &dirLightsCount, &dirLightProperties]
			(entityx::Entity& entity, SpotLight &spotLight, Direction &dir, Position& lightPosition)
		{
			// TODO: ��������� ���������� ����, �������� �� ��������� �� �����
			if (gps->GetCamera()->ViewportIntersectsWithCircle(lightPosition, spotLight.radius))
			{
				SceneVector finalLightPosition = lightPosition + spotLight.positionOffset;

				lightMVPs.push_back(glm::perspective<float>(spotLight.angle, 1.f, 0.01f, spotLight.radius) *
					glm::lookAt((glm::vec3)finalLightPosition, (glm::vec3)(finalLightPosition + dir.Normalized()), glm::vec3(0.f, 0.f, 1.f))
				);

				auto &currDirLightProps = dirLightProperties[dirLightsCount];

				currDirLightProps.color = spotLight.color;
				currDirLightProps.pos = finalLightPosition;
				currDirLightProps.radius = spotLight.radius;

				// ������ �������� ����� - ������������, �� ��������� ���������� �� ������ ������� � �������� ����� ������������
				currDirLightProps.staticIndex = spotLight.dynamic ? -1 : staticDirLightsCount++;
				currDirLightProps.dynamicIndex = dirLightsCount++;

				if (!gps->IsBakingPass())
				{
					if (spotLight.dynamic)
					{
						SceneDrawAdditionalInfo lightAdditionalInfo(true, true);

						SceneVector lightPointMinBoundingPoint = finalLightPosition - SceneVector(spotLight.radius);
						SceneVector lightPointMaxBoundingPoint = finalLightPosition + SceneVector(spotLight.radius);

						// ����������, ����� �������� ����� ����� �������������� ��� ������� ���������
						for (auto &sceneSegment : gps->GetScene().GetSceneSegments())
						{

							if (
								MathHelper::IntersectsBoxes(
									sceneSegment.second.minBoundingPoint,
									sceneSegment.second.maxBoundingPoint,
									lightPointMinBoundingPoint,
									lightPointMaxBoundingPoint
								) ||
								MathHelper::IntersectsBoxes(
									lightPointMinBoundingPoint,
									lightPointMaxBoundingPoint,
									sceneSegment.second.minBoundingPoint,
									sceneSegment.second.maxBoundingPoint
								)
								)
							{
								lightAdditionalInfo.sceneSegmentsToDraw.push_back(sceneSegment.first);
							}
						}

						gps->GetScene().RegisterDirLightInQueue(currDirLightProps.dynamicIndex, lightAdditionalInfo);
					}
					else
					{
						gps->GetScene().RegisterDirLightInQueue(currDirLightProps.dynamicIndex, SceneDrawAdditionalInfo(false, true));
					}
				}
				else
				{
					if (!spotLight.dynamic)
					{
						gps->GetScene().RegisterDirLightInQueue(currDirLightProps.dynamicIndex, SceneDrawAdditionalInfo(true, false));
					}
				}
			}
		});

		dirLightProps.Bind();
		dirLightProps.Clear();
		dirLightProps.Fill(dirLightProperties);
		dirLightProps.Unbind();

		dynamicShadowMXsBuffer.Bind();
		dynamicShadowMXsBuffer.Clear();
		dynamicShadowMXsBuffer.Fill(lightMVPs);
		dynamicShadowMXsBuffer.Unbind();

		// ��������� ��� ��������� ������������ �����
		gps->GetScene().GetSpotLightEffect().SetUniformBlockDeferred<LightProps>("lightsBlock", 0, dirLightProps);
		gps->GetScene().GetSpotLightEffect().SetUniform1iDeferred("mvpMXs", dynamicShadowMXsBufferTex.GetTextureUnit());
		
		Core::GetInstance()->GetRenderer()->SetSpotLightCount(dirLightsCount);
		Core::GetInstance()->GetRenderer()->GetDeferredRenderingEffect().SetUniform1iDeferred("spotLightsCount", dirLightsCount);
		Core::GetInstance()->GetRenderer()->GetDeferredRenderingEffect().SetUniformBlockDeferred<LightProps>("spotLightBlock", 1, dirLightProps);
		Core::GetInstance()->GetRenderer()->GetDeferredRenderingEffect().SetUniform1iDeferred("spotLightMVPMXs", dynamicShadowMXsBufferTex.GetTextureUnit());
	}
}