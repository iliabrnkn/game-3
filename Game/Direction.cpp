#include "stdafx.h"

#include <entityx/entityx.h>

#include "GameplayState.h"
#include "Direction.h"
#include "Collideable.h"
#include "LuaHelper.h"
#include "Position.h"

namespace mg
{
	void DirectionComponentManager::AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		entity.assign<Direction>(SceneVector::FromMapCoords(table).Normalized());
	}

	LuaPlus::LuaObject DirectionComponentManager::GetComponentTable(entityx::Entity& entity) const
	{
		if (entity.has_component<Direction>())
		{
			return entity.component<Direction>()->Normalize().ToMapCoords(gps->GetLuaState());
		}
		else
		{
			return (LuaPlus::LuaObject()).AssignNil();
		}
	}

	LuaPlus::LuaObject DirectionComponentManager::ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		if (!entity.has_component<Direction>())
			return LuaPlus::LuaObject().AssignNil();

		auto component = entity.component<Direction>();

		*component = SceneVector::FromMapCoords(table).Normalize();

		// ���� � �������� ���� ��������� collideable, �������� ��� ����������
		if (entity.has_component<Collideable>())
		{
			auto *body = entity.component<Collideable>()->body;
			btTransform currTransform; body->getMotionState()->getWorldTransform(currTransform);
			auto newTransform = btTransform(component->GetBasis(), currTransform.getOrigin());

			body->setWorldTransform(newTransform);
			body->getMotionState()->setWorldTransform(newTransform);
		}

		return GetComponentTable(entity);
	}

	void DirectionComponentManager::OrientTo(const LuaPlus::LuaObject &entityTable, const LuaPlus::LuaObject &mapPointTable) const
	{
		auto entity = gps->GetEntity(entityTable);
		
		assert(entity.has_component<Direction>());

		*entity.component<Direction>() = (SceneVector::FromMapCoords(mapPointTable) - *entity.component<Position>()).Normalized();
	}

	LuaPlus::LuaObject DirectionComponentManager::GetDir(const LuaPlus::LuaObject &entity) const
	{
		return gps->GetEntity(entity).component<Direction>()->ToMapCoords(gps->GetLuaState());
	}
}