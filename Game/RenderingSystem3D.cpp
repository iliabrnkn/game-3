#include "stdafx.h"

#include <map>
#include <algorithm>
#include <glm/gtx/quaternion.hpp>

#include "RenderingSystem3D.h"
#include "Paths.h"
#include "OGLConstants.h"
#include "Camera.h"
#include "Direction.h"
#include "Selectable.h"
#include "math.h"
#include "Sizes.h"
#include "GUI.h"
#include "GameplayState.h"
#include "LightPoint.h"
#include "Core.h"
#include "Level.h"
#include "Collideable.h"
#include "MathHelper.h"

namespace mg
{
	RenderingSystem3D::RenderingSystem3D(GameplayState* gps) : gps(gps), displayMeshes(true)
	{}

	RenderingSystem3D::~RenderingSystem3D()
	{}

	void RenderingSystem3D::configure(entityx::EventManager &events)
	{
		boneMxBuffer.Init(
			MAX_MESHES_COUNT * MAX_BONES_PER_MODEL * sizeof(glm::mat4) // bone matrices
			, GL_UNIFORM_BUFFER
		);

		boneMxBufferTexture.InitBuffer();
		boneMxBuffer.Bind();
		boneMxBufferTexture.BindBuffer(boneMxBuffer.GetID(), GL_RGBA32F);

		meshPropsBuffer.Init(MAX_MESHES_COUNT * sizeof(MeshProps), GL_UNIFORM_BUFFER);
		gidPropsBuffer.Init(MAX_GID_COUNT * sizeof(GidProps), GL_UNIFORM_BUFFER);
	}

	void RenderingSystem3D::update(entityx::EntityManager &es, entityx::EventManager &em, entityx::TimeDelta dt)
	{
		std::vector<MeshProps> meshProps(MAX_MESHES_COUNT);
		int lastBoneOffset = 0;

		boneMxBuffer.Bind();
		boneMxBuffer.Clear();
		
		es.each<MeshBatch, Collideable>([&dt, this, &meshProps](entityx::Entity& entity,
			MeshBatch& meshBatch, Collideable &collideable) 
		{
			for (auto& mesh : meshBatch.meshes)
			{
				if (mesh.ID != -1)
				{
					meshProps[mesh.ID].transformMx = meshBatch.initialTransform * glm::scale(glm::vec3(meshBatch.scale));
					meshProps[mesh.ID].material = meshBatch.material;

					if (entity.has_component<Skeleton>()) // ���� ��� ��������
					{
						meshProps[mesh.ID].boneMxOffset = boneMxBuffer.GetCurrentOffset();
						boneMxBuffer.Fill(mesh.boneValues);
					}
					else
					{
						meshProps[mesh.ID].transformMx = static_cast<glm::mat4>(MatrixHelper::ToGLMMatrix(collideable.body->getWorldTransform())) *
							meshProps[mesh.ID].transformMx;
						meshProps[mesh.ID].boneMxOffset = -1;
					}
				}
			}
		});

		// --------- buffer texture
		gps->GetScene().GetGeomertyEffect().SetUniform1iDeferred("boneMxBuffer", boneMxBufferTexture.GetTextureUnit());
		gps->GetScene().GetLightEffect().SetUniform1iDeferred("boneMxBuffer", boneMxBufferTexture.GetTextureUnit());
		gps->GetScene().GetSpotLightEffect().SetUniform1iDeferred("boneMxBuffer", boneMxBufferTexture.GetTextureUnit());
		gps->GetScene().GetDirectionalLightEffect().SetUniform1iDeferred("boneMxBuffer", boneMxBufferTexture.GetTextureUnit());

		// --------- uniform blocks
		meshPropsBuffer.Bind();
		meshPropsBuffer.Clear();
		meshPropsBuffer.Fill(meshProps);

		gps->GetScene().GetGeomertyEffect().SetUniformBlockDeferred<MeshProps>("meshBlock", 0, meshPropsBuffer);
		gps->GetScene().GetLightEffect().SetUniformBlockDeferred<MeshProps>("meshBlock", 1, meshPropsBuffer);
		gps->GetScene().GetSpotLightEffect().SetUniformBlockDeferred<MeshProps>("meshBlock", 1, meshPropsBuffer);
		gps->GetScene().GetDirectionalLightEffect().SetUniformBlockDeferred<MeshProps>("meshBlock", 0, meshPropsBuffer);

		gidPropsBuffer.Bind();
		gidPropsBuffer.Clear();
		gidPropsBuffer.Fill(gps->GetGidProperties());

		gps->GetScene().GetGeomertyEffect().SetUniformBlockDeferred<GidProps>("gidBlock", 1, gidPropsBuffer);
		gps->GetScene().GetLightEffect().SetUniformBlockDeferred<GidProps>("gidBlock", 2, gidPropsBuffer);
		gps->GetScene().GetDirectionalLightEffect().SetUniformBlockDeferred<GidProps>("gidBlock", 1, gidPropsBuffer);

		// --------- uniforms
		gps->GetScene().GetGeomertyEffect().SetUniform1iDeferred("atlas", gps->GetLevel()->GetSkinAtlas().GetTexture()->GetTextureUnit());
		gps->GetScene().GetGeomertyEffect().SetUniform1iDeferred("debugPropsGid", gps->GetLevel()->GetDebugGid());

		char postfix = 'A';

		for (auto &atlas : gps->GetLevel()->GetSpriteAtlases())
		{
			string varname = "spriteAtlas";
			varname.push_back(postfix++);
			gps->GetScene().GetGeomertyEffect().SetUniform1iDeferred(varname,
				atlas.GetTexture()->GetTextureUnit());
		}

		gps->GetScene().GetGeomertyEffect().SetUniformMatrix4fDeferred("mvp", gps->GetCamera()->GetSceneMVP());
		gps->GetScene().GetGeomertyEffect().SetUniform1iDeferred("display", displayMeshes ? 1 : 0);
		
		// ��������� ����� �������� ����� ����� �������������� ��� ������ ������ � ������������� �����
		
		gps->GetScene().GetMainSceneDrawAdditionalInfo().sceneSegmentsToDraw.clear();
		
		for (auto &sceneSegment : gps->GetScene().GetSceneSegments())
		{
			if (
					MathHelper::IntersectsBoxes(
						sceneSegment.second.minBoundingPoint,
						sceneSegment.second.maxBoundingPoint,
						gps->GetCamera()->GetViewportBoundingBoxMin(),
						gps->GetCamera()->GetViewportBoundingBoxMax()
					) ||
					MathHelper::IntersectsBoxes(
						gps->GetCamera()->GetViewportBoundingBoxMin(),
						gps->GetCamera()->GetViewportBoundingBoxMax(),
						sceneSegment.second.minBoundingPoint,
						sceneSegment.second.maxBoundingPoint
					)
				)
			{
				gps->GetScene().GetMainSceneDrawAdditionalInfo().sceneSegmentsToDraw.push_back(sceneSegment.first);
			}
		}

		/*auto minBP = gps->GetCamera()->GetViewportBoundingBoxMin();
		auto maxBP = gps->GetCamera()->GetViewportBoundingBoxMax();

		gps->GetScene().GetSimpleBatch().DrawVector(
			gps->GetCamera()->GetCameraTopLeftScenePos(),
			gps->GetCamera()->GetCameraCenterScenePos() - gps->GetCamera()->GetCameraTopLeftScenePos(),
			glm::vec3(1.f, 0.f, 0.f)
		);*/

		/*gps->GetScene().GetSimpleBatch().DrawVector(
			SceneVector(minBP.X(), 0, minBP.Z()),
			SceneVector(minBP.X(), 0, maxBP.Z() - minBP.Z()),
			glm::vec3(1.f, 0.f, 0.f)
		);*/
	}
}