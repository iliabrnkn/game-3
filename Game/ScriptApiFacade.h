#pragma once

#include <entityx/entityx.h>
#include <memory>
#include <string>

#include <lua/LuaPlus.h>

#include "ComponentManagersRepo.h"

namespace mg {

	class ScriptApiFacade
	{
		friend class GameplayState;

	private:
		ScriptApiFacade() = default;
		~ScriptApiFacade() = default;

		GameplayState* gps;

		std::unique_ptr<ComponentManagersRepo> repo;

	public:
		static ScriptApiFacade& Get();

		void AssignComponentsToEntity(entityx::EntityX *entityx, entityx::Entity& entity, const LuaPlus::LuaObject& componentsTable);
		
		/*float GetDistance(const int &firstId, const int &secondId) const;
		glm::vec2 GetEntityPos(const int &id) const;*/
		void WatchFloat(const char* name, const float& val) const;

		LuaPlus::LuaObject CreateEntity(LuaPlus::LuaObject& table);
		void RemoveEntity(LuaPlus::LuaObject& table);

		// TODO : ����������� ����� const

		LuaPlus::LuaObject GetComponent(LuaPlus::LuaObject& table, const char* componentName);
		LuaPlus::LuaObject ModifyComponent(LuaPlus::LuaObject& table, const char* componentName, LuaPlus::LuaObject& componentTable);
		void RemoveComponent(LuaPlus::LuaObject &table, const char *componentName);
		void AddComponent(LuaPlus::LuaObject &table, const char *componentName, LuaPlus::LuaObject& componentTable);
		double GetTimeMS();
		void SetTimer(LuaPlus::LuaObject& table, const float& time, const LuaPlus::LuaObject &callback);
		void ConsoleLog(const const char* msg);
		bool CheckIfTilesetExists(const char* filename);
		LuaPlus::LuaObject GetEntityByID(const unsigned int& id);
		void CenterCameraAt(const float& x, const float& y);
		LuaPlus::LuaObject NormalizeVec(const LuaPlus::LuaObject& table);
		void SetRepeater(const LuaPlus::LuaObject &table, const float& period, const LuaPlus::LuaObject &callback,
			const LuaPlus::LuaObject &predicate, const LuaPlus::LuaObject &finalization, const char *name);
		LuaPlus::LuaObject MixVec(const LuaPlus::LuaObject first, const LuaPlus::LuaObject second, const float& a);
		LuaPlus::LuaObject SlerpVec(const LuaPlus::LuaObject first, const LuaPlus::LuaObject second, const float& a);
		float FindAngle(const LuaPlus::LuaObject& vec1, const LuaPlus::LuaObject& vec2) const;
		void ShowWidget(const char* widgetName, const bool& visible);
		bool AddToInventory(const int &x, const int& y, const LuaPlus::LuaObject &entityTable);
		float GetLength(const LuaPlus::LuaObject &vec);
		LuaPlus::LuaObject GetEntityByScreenCoords(const LuaPlus::LuaObject &screenCoords);
		LuaPlus::LuaObject GetEntityByMapCoords(const LuaPlus::LuaObject &mapCoords);
		LuaPlus::LuaObject GetSceneCoordsByScreenCoords(const LuaPlus::LuaObject &screenCoords);
		int Random(const int &maxNum);
		LuaPlus::LuaObject CrossProduct(const LuaPlus::LuaObject &vecA, const LuaPlus::LuaObject &vecB);
		float DotProduct(const LuaPlus::LuaObject &vecA, const LuaPlus::LuaObject &vecB);
		LuaPlus::LuaObject RotateVec(const LuaPlus::LuaObject &vec, const LuaPlus::LuaObject &quat);
		const char* GetScriptsFolder();
		const char* GetMapsFolder();
		
		void SetDefaultCursor(const bool &highlighted);
		void SetAimCursor(const float &factor);
		void SetCursorInvisible(const bool &invisible);
		void LockAimCursor(const LuaPlus::LuaObject &coords);

		void DebugShowLevelGeometry();
		void DebugShowAABBs();
		void DebugShowCoords();
		void DebugEnableGravity();
		void DebugDrawMeshes();
		void DebugEnableConstraints();
		void DebugDrawVector(const LuaPlus::LuaObject &begin, const LuaPlus::LuaObject &dir, const LuaPlus::LuaObject &color);
		bool DebugLooseBodies();

		void Init();
		void LoadLevel(const LuaPlus::LuaObject& luaObject);

		inline void CheckIfNumber(const LuaPlus::LuaObject& table, const std::string& fieldName1, const std::string& fieldName2 = "", const std::string& fieldName3 = "", const std::string& fieldName4 = "") const
		{
			assert(table.GetByName(fieldName1.c_str()).GetRef() != -1 && table.GetByName(fieldName1.c_str()).IsNumber());
			
			if (fieldName2 != "")
				assert(table.GetByName(fieldName2.c_str()).GetRef() != -1 && table.GetByName(fieldName2.c_str()).IsNumber());
			if (fieldName3 != "")
				assert(table.GetByName(fieldName3.c_str()).GetRef() != -1 && table.GetByName(fieldName3.c_str()).IsNumber());
			if (fieldName4 != "")
				assert(table.GetByName(fieldName4.c_str()).GetRef() != -1 && table.GetByName(fieldName4.c_str()).IsNumber());
		}

		LuaPlus::LuaObject RayTest(const float &xStart, const float &yStart, const float &zStart,
			const float &xDir, const float &yDir, const float &zDir) const;
	};
}