#include "stdafx.h"

#include "Viewable.h"
#include "GameplayState.h"

namespace mg
{
	void ViewableComponentManager::AssignComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		entity.assign<Viewable>(
			(table.GetByName("angle").GetRef() != -1 && table.GetByName("angle").IsNumber()) ? table.GetByName("angle").ToNumber() : 0.f,
			(table.GetByName("radius").GetRef() != -1 && table.GetByName("radius").IsNumber()) ? table.GetByName("radius").ToNumber() : 0.f);
	}

	LuaPlus::LuaObject ViewableComponentManager::GetComponentTable(entityx::Entity& entity) const
	{
		LuaPlus::LuaObject res;

		if (entity.has_component<Viewable>())
		{
			auto componentHandler = entity.component<Viewable>();

			res.AssignNewTable(gps->GetLuaState());

			res.SetNumber("angle", componentHandler->angle);
			res.SetNumber("radius", componentHandler->radius);

			// ���������� ��� ������� ��������

			LuaPlus::LuaObject entitiesInSightTable;
			entitiesInSightTable.AssignNewTable(gps->GetLuaState());
			auto i = 0;

			for (auto& inSightTable : componentHandler->entitiesInSight)
			{
				entitiesInSightTable.SetObject(++i, inSightTable);

				// -- test
				auto name = inSightTable.GetByName("name");
				bool isNotNil = name.GetRef() != -1;
				bool isStr = name.IsString();
				string val = name.ToString();
			}

			res.SetObject("entitiesInSight", entitiesInSightTable);
		}
		else
		{
			res.AssignNil();
		}

		return res;
	};

	LuaPlus::LuaObject ViewableComponentManager::ModifyComponent(entityx::Entity& entity, const LuaPlus::LuaObject& table) const
	{
		if (!entity.has_component<Viewable>())
			return LuaPlus::LuaObject().AssignNil();

		entity.component<Viewable>()->angle = table.GetByName("angle").GetRef() != -1 ? table.GetByName("angle").ToNumber() : entity.component<Viewable>()->angle;
		entity.component<Viewable>()->radius = table.GetByName("radius").GetRef() != -1 ? table.GetByName("radius").ToNumber() : entity.component<Viewable>()->radius;

		return GetComponentTable(entity);
	};
}