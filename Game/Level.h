#pragma once

#include <vector>
#include <unordered_map>
#include <functional>
#include <mutex>
#include <entityx/entityx.h>
#include <set>

#include <lua/LuaPlus.h>

#include "Texture.h"
#include "AStar.h"
#include "Position.h"
#include "IVec2Hasher.h"
#include "VertTypes.h"
#include "Props.h"
#include "TextureAtlas.h"
#include "NavigationMesh.h"

namespace mg {

	const int MAX_GID_COUNT = 4096 ;
	const float TILES_PER_CLOUD_UNIT = 30.f;
	const int SPRITE_ATLASES_COUNT = 4;

	struct TileGeometry
	{
		std::vector<unsigned int> indices;
		std::vector<SimpleVertex> vertices;
	};

	class GameplayState;

	class Level
	{
		friend class GameplayState;

	public:
		Level();
		Level(const Level&) = delete;
		~Level() = default;
		void Load(const LuaPlus::LuaObject &mapObject, GameplayState *gps);

		int GetWidth() const;
		int GetHeight() const;

		std::vector<TextureAtlas>& GetSpriteAtlases();
		TextureAtlas& GetParticleAtlas();
		TextureAtlas& GetSkinAtlas();
		TextureAtlas& GetCloudAtlas();

		std::map<unsigned int, PropsHandler> tileProps;

		void AssociateGidWGeometry(const int &gid, PropsHandler props, GameplayState *gps);

		inline void SetDebugGid(const int &gid)
		{
			debugGid = gid;
		}

		inline int GetDebugGid()
		{
			return debugGid;
		}

		void TestPathFinding(GameplayState *gps);

		inline NavigationMesh &GetNavMesh()
		{
			return navigationMesh;
		}

		inline glm::vec2 GetCloudTexCoordSize()
		{
			return cloudTexCoordSize;
		}

	private:
		int debugGid;
		int FindTilesetNumByGid(const LuaPlus::LuaObject &tilesets, const int &gid);
		void InitAtlases(GameplayState* gps, const LuaPlus::LuaObject &mapTable);

		//---- entity creation functions
		entityx::Entity CreateTileLayer(GameplayState* gps, const LuaPlus::LuaObject &table);
		void CreateSceneSegment(GameplayState* gps, const std::vector<std::vector<int>> &layerTiles,
			const int &startX, const int &startY, const float &layerHeight);

		void ParseLayers(LuaPlus::LuaObject& layersInitial, LuaPlus::LuaObject& result);

		TextureAtlas particleAtlas, skinAtlas, cloudAtlas;
		std::vector<TextureAtlas> spriteAtlases;

		std::unordered_map<glm::ivec2, AStarNode, IVec2Hasher> aStarNodes;

		int width, height;
		// ���������� ����� �� �������� ������ (������ ���� ������������ ��������� �� ��� y � ���������)
		unordered_map<float, int> layerCountsByOffsets;
		mutex layerParsingMutex;
		NavigationMesh navigationMesh;

		glm::vec3 ambienceLight; // ���������� ����
		glm::vec2 cloudTexCoordSize;

		//���������� ������ GidTex �� ������� �������
		inline GidTex *GetGidTex(const int &gid)
		{
			for (auto &atlas : spriteAtlases)
			{
				if (atlas.HasGid(gid))
					return atlas.GetGidTex(gid);
			}

			return nullptr;
		}

		// ��������� �������� �� �������
		inline std::deque<LuaPlus::LuaObject> GetTilesetsSortedBySize(LuaPlus::LuaObject& tilesets)
		{
			std::deque<LuaPlus::LuaObject> result;

			for (LuaPlus::LuaTableIterator iter(tilesets); iter; iter.Next())
			{
				auto currTileset = iter.GetValue();
				auto currSize = LuaHelper::GetInteger(iter.GetValue(), "imagewidth") * LuaHelper::GetInteger(iter.GetValue(), "imageheight");

				bool smallerTsFound = false;

				for (int i = 0; i < result.size(); ++i)
				{
					auto compareSize = LuaHelper::GetInteger(result[i], "imagewidth") * LuaHelper::GetInteger(result[i], "imageheight");

					if (compareSize <= currSize)
					{
						result.insert(result.begin() + i, currTileset);
						smallerTsFound = true;
						break;
					}
				}

				if (!smallerTsFound)
					result.push_back(iter.GetValue());
			}

			return result;
		}
	};
}