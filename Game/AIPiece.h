#pragma once

#include <string>
#include <map>
#include <vector>
#include <lua/LuaPlus.h>

#include "GUI.h"

namespace mg {

	struct LuaRepeater
	{
		std::string name;
		double deltaTime;
		double elapsedTime;
		double period;
		LuaPlus::LuaObject predicate;

		LuaPlus::LuaObject callback;
		LuaPlus::LuaObject finalization;

		LuaRepeater() = default;

		LuaRepeater(const double& period, const LuaPlus::LuaObject& predicate, const LuaPlus::LuaObject& callback, const LuaPlus::LuaObject& finalization, 
			const char* name) :
			deltaTime(0.0), period(period), callback(callback), finalization(finalization), predicate(predicate), elapsedTime(0.0), name(name)
		{
			assert(predicate.IsFunction() && callback.IsFunction());
		}

		// ������� ������������ ���������� �� ������� � ��������� �� ���������� predicate
		// ���� ��������� ����� ������ period, � �������� �����, ����������� callback

		void Check(double delta, LuaPlus::LuaObject& tableArg)
		{
			deltaTime += delta;
			elapsedTime += delta;

			if (deltaTime >= period)
			{
				if (callback.GetRef() == -1)
				{
					int here = 0;
				}

				static_cast<LuaPlus::LuaFunctionVoid>(callback)(tableArg, elapsedTime);
				deltaTime = 0.0;
			}
		}

		~LuaRepeater() = default;
	};

	struct LuaTimer
	{
		double currentTime;
		double endTime;
		bool staled;
		
		LuaPlus::LuaObject callback;

		LuaTimer() = default;

		LuaTimer(const double& endTime, const LuaPlus::LuaObject& callback) :
			endTime(endTime), currentTime(0.0f), staled(false), callback(callback)
		{
			assert(callback.IsFunction());
		}

		void Fire(LuaPlus::LuaObject& tableArg)
		{
			static_cast<LuaPlus::LuaFunctionVoid>(callback)(tableArg);
		}

		~LuaTimer() = default;
	};

	struct AIPiece
	{
		std::vector<LuaTimer> timerRequests;
		std::vector<LuaTimer> timers;
		std::vector<LuaRepeater> repeaterRequests;
		std::unordered_map<std::string, LuaRepeater> repeaters;

		LuaPlus::LuaObject table;

		AIPiece()
		{}

		AIPiece(LuaPlus::LuaObject tableInit) :
			table(tableInit)
		{
			name = tableInit["name"].GetString();
		}

		std::string name;

		bool HasKey(const std::string& key)
		{
			return table.GetByName(key.c_str()).GetRef() == -1 ? false : true;
		}

		std::string GetString(const std::string& key)
		{
			assert(table.GetByName(key.c_str()).IsString());

			return table.GetByName(key.c_str()).ToString();
		}

		LuaPlus::LuaObject operator[](const std::string& key)
		{
			return table.GetByName(key.	c_str());
		}

		// ����������
		void Emit(const std::string& functionName)
		{
			auto funcMember = table[functionName.c_str()];

			if (funcMember.GetRef() != -1 && funcMember.IsFunction())
			{
				static_cast<LuaPlus::LuaFunctionVoid>(funcMember)(table);
			}
		}

		// ����������, ����������� ��������� (�������)
		void Emit(const std::string& functionName, LuaPlus::LuaObject argsObject)
		{
			auto funcMember = table[functionName.c_str()];

			if (funcMember.GetRef() != -1 && funcMember.IsFunction()) // ����� assert, ���� ��� ����������� - ������ ������ �� ������
			{
				static_cast<LuaPlus::LuaFunctionVoid>(funcMember)(table, argsObject);
			}
		}
	};
}