#pragma once

#include "Widget.h"
#include "lua/LuaPlus.h"

namespace mg
{
	const int INVENTORY_CORNER_SIDE_LENGTH = 4;

	class ItemCell : public Widget
	{
		friend class InventoryWindow;
		friend class InventoryGrid;

		int entityID;
		bool free;

	public:
		ItemCell(const mg::WidgetParameters& params, const std::string& name);

		~ItemCell();

		bool GetStatus();

		void SetStatus(const bool &freeStatus);
	};

	class ActiveItemCell : public ItemCell
	{
		//LuaPlus::LuaObject eventHandler;

	public:
		ActiveItemCell(const mg::WidgetParameters &params, const std::string &name);

		/*void SetEventHandler(const LuaPlus::LuaObject &eventHandlerInitial);*/

		/*void OnItemChanged(const LuaPlus::LuaObject &itemTable);*/
	};
}