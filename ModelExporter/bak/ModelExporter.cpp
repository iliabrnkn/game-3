// ModelExporter.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <functional>
#include <fstream>
#include <string>
#include <deque>
#include <set>
#include <map>
#include <unordered_map>
#include <direct.h>
#include <memory>
#include <limits.h>
//#include <glm/detail/func_matrix.inl>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <iostream>
#include <algorithm>

#include "Importer.hpp"
#include "postprocess.h"
#include "scene.h"
#include "DefaultLogger.hpp"
#include "LogStream.hpp"
#include "Channel.h"

using namespace std;
using namespace Assimp;

unordered_map<string, aiNode*> nodes;

const unsigned int MAX_WEIGHTS_PER_VERTEX = 4;

void writeStringIntoFile(ofstream& file, const char* cStr, const size_t& size)
{
	file.write((char*)&size, sizeof(size_t));
	file.write(cStr, size);
}

void countNodes(aiNode* node)
{
	nodes[node->mName.C_Str()] = node;

	for (int i = 0; i < node->mNumChildren; i++)
	{
		countNodes(node->mChildren[i]);
	}
}

std::vector<std::string> splitpath(
	const std::string& str
	, const std::set<char> delimiters)
{
	std::vector<std::string> result;

	char const* pch = str.c_str();
	char const* start = pch;
	for (; *pch; ++pch)
	{
		if (delimiters.find(*pch) != delimiters.end())
		{

			if (start != pch)
			{
				std::string str(start, pch);
				result.push_back(str);
			}
			else
			{
				result.push_back("");
			}
			start = pch + 1;
		}
	}
	result.push_back(start);

	return result;
}

//std::string getBoneName(const aiString &str)
//{
//	auto name = std::string(str.C_Str());
//	auto delPos = name.find("$");
//	auto boneName = name.substr(0, delPos - 1);
//
//	return boneName;
//}
//
//void getNecesaryNodes(aiNode* node, std::unordered_map<std::string, aiNode*> &m)
//{
//	auto boneName = getBoneName(node->mName);
//
//	if (m.find(boneName) == m.end())
//	{
//		m.insert(std::make_pair(node->mName.C_Str(), new aiNode()));
//	}
//
//	if (boneName == std::string(node->mName.C_Str))
//	{
//		m[boneName]-> = node
//	}
//}

void writeVec3ToFile(const glm::vec3 &vec, std::ofstream &outfile)
{
	outfile.write((char*)&(vec.x), sizeof(float));
	outfile.write((char*)&(vec.y), sizeof(float));
	outfile.write((char*)&(vec.z), sizeof(float));
}

void writeVec3ToFile(const aiVector3D &vec, std::ofstream &outfile)
{
	writeVec3ToFile(glm::vec3(vec.x, vec.y, vec.z), outfile);
}

void writeQuatToFile(const glm::quat &q, std::ofstream &outfile)
{
	outfile.write((char*)&(q.x), sizeof(float));
	outfile.write((char*)&(q.y), sizeof(float));
	outfile.write((char*)&(q.z), sizeof(float));
	outfile.write((char*)&(q.w), sizeof(float));
}

void writeQuatToFile(const aiQuaternion &q, std::ofstream &outfile)
{
	writeQuatToFile(glm::quat(q.w, q.x, q.y, q.z), outfile);
}

int exportSkeleton(ofstream& outfile, const aiScene* scene)
{
	// ��������� ������ ��������������� ������ ����

	//for ()

	//---

	countNodes(scene->mRootNode);

	// scene nodes
	unsigned int nodesCount = nodes.size();

	outfile.write((char*)&nodesCount, sizeof(unsigned int)); //scene nodes count

	for (auto nameNodePair : nodes)
	{
		//outfile.write(nameNodePair.first.c_str(), nameNodePair.first.length() + 1); // node name
		writeStringIntoFile(outfile, nameNodePair.first.c_str(), nameNodePair.first.length() + 1);
	
		std::string nodeName(nameNodePair.first.c_str());
	}

	for (auto nameNodePair : nodes)
	{
		//outfile.write(nameNodePair.first.c_str(), nameNodePair.first.length() + 1); // node name
		writeStringIntoFile(outfile, nameNodePair.first.c_str(), nameNodePair.first.length() + 1);

		// transformation matrix
		outfile.write((char*)&(nameNodePair.second->mTransformation.a1), sizeof(float));
		outfile.write((char*)&(nameNodePair.second->mTransformation.a2), sizeof(float));
		outfile.write((char*)&(nameNodePair.second->mTransformation.a3), sizeof(float));
		outfile.write((char*)&(nameNodePair.second->mTransformation.a4), sizeof(float));

		outfile.write((char*)&(nameNodePair.second->mTransformation.b1), sizeof(float));
		outfile.write((char*)&(nameNodePair.second->mTransformation.b2), sizeof(float));
		outfile.write((char*)&(nameNodePair.second->mTransformation.b3), sizeof(float));
		outfile.write((char*)&(nameNodePair.second->mTransformation.b4), sizeof(float));

		outfile.write((char*)&(nameNodePair.second->mTransformation.c1), sizeof(float));
		outfile.write((char*)&(nameNodePair.second->mTransformation.c2), sizeof(float));
		outfile.write((char*)&(nameNodePair.second->mTransformation.c3), sizeof(float));
		outfile.write((char*)&(nameNodePair.second->mTransformation.c4), sizeof(float));

		outfile.write((char*)&(nameNodePair.second->mTransformation.d1), sizeof(float));
		outfile.write((char*)&(nameNodePair.second->mTransformation.d2), sizeof(float));
		outfile.write((char*)&(nameNodePair.second->mTransformation.d3), sizeof(float));
		outfile.write((char*)&(nameNodePair.second->mTransformation.d4), sizeof(float));

		// parent name

		string noparentString = "NOPARENT";

		if (nameNodePair.second->mParent != NULL)
			writeStringIntoFile(outfile, nameNodePair.second->mParent->mName.C_Str(), nameNodePair.second->mParent->mName.length + 1);
		else
			writeStringIntoFile(outfile, noparentString.c_str(), noparentString.length() + 1);

		// ancestors

		unsigned int numChildren = nameNodePair.second->mNumChildren;
		outfile.write((char*)&numChildren, sizeof(unsigned int));

		for (unsigned int i = 0; i < nameNodePair.second->mNumChildren; i++)
		{
			//outfile.write(nameNodePair.second->mChildren[i]->mName.C_Str(), nameNodePair.second->mChildren[i]->mName.length + 1);
			writeStringIntoFile(outfile, nameNodePair.second->mChildren[i]->mName.C_Str(), nameNodePair.second->mChildren[i]->mName.length + 1);
		}
	}

	// inversed root node transform

	glm::mat4 inversedRootNodeTransform = /*glm::rotate(glm::half_pi<float>(), glm::vec3(0.0, 0.0, 1.0)) * glm::rotate(glm::half_pi<float>(), glm::vec3(1.0, 0.0, 0.0)) **/ glm::mat4(
		glm::vec4(scene->mRootNode->mTransformation.a1, scene->mRootNode->mTransformation.b1, scene->mRootNode->mTransformation.c1, scene->mRootNode->mTransformation.d1),
		glm::vec4(scene->mRootNode->mTransformation.a2, scene->mRootNode->mTransformation.b2, scene->mRootNode->mTransformation.c2, scene->mRootNode->mTransformation.d2),
		glm::vec4(scene->mRootNode->mTransformation.a3, scene->mRootNode->mTransformation.b3, scene->mRootNode->mTransformation.c3, scene->mRootNode->mTransformation.d3),
		glm::vec4(scene->mRootNode->mTransformation.a4, scene->mRootNode->mTransformation.b4, scene->mRootNode->mTransformation.c4, scene->mRootNode->mTransformation.d4));

	outfile.write((char*)&(inversedRootNodeTransform[0][0]), sizeof(float));
	outfile.write((char*)&(inversedRootNodeTransform[1][0]), sizeof(float));
	outfile.write((char*)&(inversedRootNodeTransform[2][0]), sizeof(float));
	outfile.write((char*)&(inversedRootNodeTransform[3][0]), sizeof(float));

	outfile.write((char*)&(inversedRootNodeTransform[0][1]), sizeof(float));
	outfile.write((char*)&(inversedRootNodeTransform[1][1]), sizeof(float));
	outfile.write((char*)&(inversedRootNodeTransform[2][1]), sizeof(float));
	outfile.write((char*)&(inversedRootNodeTransform[3][1]), sizeof(float));

	outfile.write((char*)&(inversedRootNodeTransform[0][2]), sizeof(float));
	outfile.write((char*)&(inversedRootNodeTransform[1][2]), sizeof(float));
	outfile.write((char*)&(inversedRootNodeTransform[2][2]), sizeof(float));
	outfile.write((char*)&(inversedRootNodeTransform[3][2]), sizeof(float));

	outfile.write((char*)&(inversedRootNodeTransform[0][3]), sizeof(float));
	outfile.write((char*)&(inversedRootNodeTransform[1][3]), sizeof(float));
	outfile.write((char*)&(inversedRootNodeTransform[2][3]), sizeof(float));
	outfile.write((char*)&(inversedRootNodeTransform[3][3]), sizeof(float));

	return 0;
}

struct Bone
{
	Bone(const string& nameInit) : name(nameInit) {}

	string name;
	vector<aiVertexWeight> weights;
	aiMatrix4x4 offsetMatrix;
};

void analyzeMesh(const aiMesh* currMesh, std::unordered_map<int, std::set<int>>& vertBoneIds)
{
	// -------------- ������� ���-�� �������������, ������� ����� �� ������ �������� �� ��� ����� ����� -------------------

	unsigned int maxAdjustFaces = 0; // ������������ ���-�� adjust faces ��� ������ ������������
	int minAdjustedFaces = 100; // �����������
	std::unordered_map<int, int> adjustTypesCount; // ���-�� �������������, ������� ���������� ���-�� ��������
	int maxFaceOffset = 0; // ������������ ����� � ������� ����� ��������� ��������������
	int maxIndOffset = 0; // ������������ ����� �������� ����� �� ������������ � ������������
	unsigned int sameBonesFaces = 0; // ���-�� �������������, ������� ���������� �������� �������� ��� �� �� �������, ��� � �� ��� ������� �������� �������������
	std::unordered_map<int, int> diffBonesAdjustFacesCount; // ��������� �� ���-�� �������� �������������, ������� ������������ �� ��� ������� ������ �� �������
	std::unordered_map<int, unsigned int> facesByCommonPoints; // ��������� ��� ������������� (������ �� ������� ������ �� ��� ����) �� ���-�� ����� ����� � ����������� �������, �� ����������� ������������
	std::unordered_map<int, int> adjustFacesOffsets;

	
	for (auto i = 0; i < currMesh->mNumFaces; ++i)
	{
		auto& currFace = currMesh->mFaces[i];
		unsigned int adjustFacesCount = 0;

		// �������, ���� � �� �� �� ������� ������ �� ������� � ������� ������������

		bool sameBonesWithinCurrFace = true;

		for (int indF = 0; indF < currFace.mNumIndices; ++indF)
		{
			auto& currVertNum = currFace.mIndices[indF];

			for (int indS = 0; indS < currFace.mNumIndices; ++indS)
			{
				if (indF == indS) continue;
				auto& otherVertNum = currFace.mIndices[indS];

				// ��������� ���-�� ����������� �����

				if (vertBoneIds[currVertNum].size() != vertBoneIds[otherVertNum].size())
					sameBonesWithinCurrFace = false;

				if (sameBonesWithinCurrFace)
				{
					for (auto& currBoneId : vertBoneIds[currVertNum])
					{
						for (auto& otherBoneId : vertBoneIds[otherVertNum])
						{
							if (currBoneId != otherBoneId)
							{
								sameBonesWithinCurrFace = false;
								break;
							}
						}

						if (!sameBonesWithinCurrFace) break;
					}
				}
			}
		}

		bool sameBonesWithOtherFaces = sameBonesWithinCurrFace;
		unsigned int allCommonPointsCount = 0;

		for (auto j = 0; j < currMesh->mNumFaces; ++j)
		{
			auto& otherFace = currMesh->mFaces[j];
			unsigned int commonPointsCount = 0;
			int diffBonesCounter = 0;
			// ����� ���� � ������������� ��������
			bool sameBonesWithNewVert = true;

			if (currFace == otherFace) continue;

			// ���������� ������� � ������� ����������
			for (int currIndNum = 0; currIndNum < currFace.mNumIndices; ++currIndNum)
			{
				auto& currVertNum = currFace.mIndices[currIndNum];

				for (int otherIndNum = 0; otherIndNum < otherFace.mNumIndices; ++otherIndNum)
				{
					auto& otherVertNum = otherFace.mIndices[otherIndNum];

					if (currVertNum == otherVertNum)
						++commonPointsCount; // ����� ����� �����
					else // ���� ��� ����� �� �����, �������, �� �� �� ����� ������� ������ �� ��� �����, ��� � ��� ����� ����������� ������������
					{
						if (sameBonesWithNewVert)
						{
							// ��������� ���-�� ����������� �����

							if (vertBoneIds[currVertNum].size() != vertBoneIds[otherVertNum].size())
							{
								sameBonesWithNewVert = false;
								++diffBonesCounter;
							}

							if (sameBonesWithNewVert)
							{
								for (auto& currBoneId : vertBoneIds[currVertNum])
								{
									for (auto& otherBoneId : vertBoneIds[otherVertNum])
									{
										if (currBoneId != otherBoneId)
										{
											sameBonesWithNewVert = false;
											sameBonesWithOtherFaces = false;
											++diffBonesCounter;
											break;
										}
									}

									if (!sameBonesWithNewVert) break;
								}
							}
						}
					}

					if (maxIndOffset < glm::abs<int>(currVertNum - otherVertNum))
						maxIndOffset = glm::abs<int>(currVertNum - otherVertNum);
				}
			}

			// ����
			if (commonPointsCount > 0)
			{
				int here = 0;
			}

			// ������ ��� ��� �������������, � ������� � �������� ������������� �� ��� ����� �����
			if (!sameBonesWithNewVert && commonPointsCount == 2)
			{
				if (diffBonesAdjustFacesCount.find(diffBonesCounter) == diffBonesAdjustFacesCount.end())
					diffBonesAdjustFacesCount[diffBonesCounter] = 0;

				++diffBonesAdjustFacesCount[diffBonesCounter];
			}

			if (sameBonesWithOtherFaces)
				sameBonesFaces++;

			// ����� ���� ���� ��� ����� �����, ���� ����, ���� �� �����, ����� ������ ������������
			assert(commonPointsCount <= 2);

			// ����������� ����� ��� ����� ����� � �������
			if (commonPointsCount == 2)
			{
				if (maxFaceOffset < glm::abs<int>(j - i))
					maxFaceOffset = glm::abs<int>(j - i);

				if (adjustFacesOffsets.find(i - j) == adjustFacesOffsets.end())
					adjustFacesOffsets[i - j] = 0;
				adjustFacesOffsets[i-j]++;

				++adjustFacesCount;
			}

			allCommonPointsCount += commonPointsCount;
		}

		if (maxAdjustFaces < adjustFacesCount)
			maxAdjustFaces = adjustFacesCount;
		else if (minAdjustedFaces > adjustFacesCount)
			minAdjustedFaces = adjustFacesCount;

		if (adjustTypesCount.find(adjustFacesCount) == adjustTypesCount.end())
			adjustTypesCount[adjustFacesCount] = 0;
		++adjustTypesCount[adjustFacesCount];

		if (facesByCommonPoints.find(allCommonPointsCount) == facesByCommonPoints.end())
			facesByCommonPoints[allCommonPointsCount] = 0;
		++facesByCommonPoints[allCommonPointsCount];
	}

	int here = 0;
	
}

struct pairHasher
{
	size_t operator() (const std::pair<unsigned int, unsigned int>& key) const
	{
		return key.first ^ key.second;
	}
};

struct AdjacencyGroup
{
	aiFace* centerFace; // ������ ������������ �����
	std::vector<aiFace*> faces; // "����"
	int centerFaceNum; // ����� ������������ �����
	std::set<int> faceNums; // ������ ����� ������, �.�. ��������� ��� ���������������, �� ������ ������������� �� � ������� ����������� ��� ������������ ��������
	std::vector<unsigned int> indices;
};

#define FAKE_INDEX_PASSED INT_MAX // ��� ������, ������� ��� ���� ���� ��� ��������
#define FAKE_INDEX_DUMMY INT_MAX - 1 // ��� ������, � ������� ��� ������� �������������

// ���������� ������� � ������� ��������� (adjacency) �������������
std::vector<unsigned int> ReorderIndices(const aiMesh* currMesh, const int& minNeighbourCount = 3, const bool& enclosed = false)
{
	std::set<int> adjacencyCenterFaces; // ������ �������������� ������
	std::vector<std::pair<int,aiFace*>> zeroFaces;
	std::vector<AdjacencyGroup> adjacencyGroups;
	
	std::set<int> faceIndsToPass;
	std::set<int> zeroesToRemove;

	int dupls = 0;
	
	// ���������� �� �������, � ������� ��������� ����������

	/*for (int maxDist = 0; maxDist < 1; ++maxDist)
	{
		std::set<unsigned int> filteredIndices;

		for (unsigned int i = 0; i < currMesh->mNumVertices - 1; ++i)
		{
			glm::vec3 v1(currMesh->mVertices[i].x, currMesh->mVertices[i].y, currMesh->mVertices[i].z);

			for (unsigned int j = i + 1; j < currMesh->mNumVertices; ++j)
			{
				if (filteredIndices.find(j) != filteredIndices.end())
					continue;

				glm::vec3 v2(currMesh->mVertices[j].x, currMesh->mVertices[j].y, currMesh->mVertices[j].z);

				if (glm::length(v2 - v1) <= static_cast<float>(maxDist) * 0.1f)
				{
					filteredIndices.insert(j);

					for (unsigned int f = 0; f < currMesh->mNumFaces; ++f)
					{
						for (int p = 0; p < 3; ++p)
						{
							if (currMesh->mFaces[f].mIndices[p] == j)
								currMesh->mFaces[f].mIndices[p] = i;
						}
					}
				}
			}
		}

		printf("%d vertices was filtered out\n", filteredIndices.size());
	}*/

	for (unsigned int i = 0; i < currMesh->mNumFaces; ++i)
	{
		faceIndsToPass.insert(i);
	}

	printf("%d duplicates found\n", dupls);

	std::unordered_map<std::pair<unsigned int, unsigned int>, unsigned int, pairHasher> sideTable;

	// �������� ���������� � ������

	for (auto& faceInd : faceIndsToPass)
	{
		auto* currFace = &currMesh->mFaces[faceInd];

		assert(sideTable.find(make_pair(currFace->mIndices[0], currFace->mIndices[1])) == sideTable.end() &&
			sideTable.find(make_pair(currFace->mIndices[1], currFace->mIndices[2])) == sideTable.end() &&
			sideTable.find(make_pair(currFace->mIndices[2], currFace->mIndices[0])) == sideTable.end());

		sideTable[make_pair(currFace->mIndices[0], currFace->mIndices[1])] = currFace->mIndices[2];
		sideTable[make_pair(currFace->mIndices[1], currFace->mIndices[2])] = currFace->mIndices[0];
		sideTable[make_pair(currFace->mIndices[2], currFace->mIndices[0])] = currFace->mIndices[1];
	}

	// ������� ������� ������ �������������
	int requiredCommonPointCount = 2; int adjTrianglesInGroup = 3;
	size_t previousPassZeroesCount = 0;
	int totalDegenFaces = 0;

	////////// ����������� ������� � ����� �������, ����� ��� ��������� ������� ������������� ��������� ������ ����� ������ �� ������ ����

	std::vector<unsigned int> res;

	for (unsigned int faceInd = 0; faceInd < currMesh->mNumFaces; ++faceInd)
	{
		auto& currFace = currMesh->mFaces[faceInd];

		res.push_back(currFace.mIndices[0]);

		for (int i = 0; i < 3; ++i)
		{
			unsigned int firstVertInd = currFace.mIndices[i];
			unsigned int secVertInd = currFace.mIndices[i + 1 > 2 ? 0 : i + 1];

			// ���������, ���� �� ������� ����������� ��� ���� �����
			if (sideTable.find(std::make_pair(secVertInd, firstVertInd)) != sideTable.end())
			{
				// ���� ����, ���������� ��� ��������� �������
				res.push_back(sideTable[std::make_pair(secVertInd, firstVertInd)]);
			}
			else // ���� �������� ������������ ���, ����������� ������-��������, ����� �� ��������� ���� ������ ������ ���
			{
				res.push_back(FAKE_INDEX_DUMMY);
			}

			if (i + 1 <= 2)
				res.push_back(secVertInd);

			// ��������� �������� �� ����� ��������������� ������� ������� �����
			sideTable[std::make_pair(firstVertInd, secVertInd)] = FAKE_INDEX_PASSED;
		}
	}

 	return res;
}

struct HitBoxConstructionInfo
{
	float radius;
	std::string firstJointName;
	std::string secJointName;

	HitBoxConstructionInfo(const float& radius, const std::string& firstJointName, const std::string& secJointName) :
		radius(radius), firstJointName(firstJointName), secJointName(secJointName) {}
};

glm::mat4 convertToGLMMat(const aiMatrix4x4& mat)
{
	glm::mat4 res;

	res[0][0] = mat.a1;
	res[1][0] = mat.a2;
	res[2][0] = mat.a3;
	res[3][0] = mat.a4;

	res[0][1] = mat.b1;
	res[1][1] = mat.b2;
	res[2][1] = mat.b3;
	res[3][1] = mat.b4;

	res[0][2] = mat.c1;
	res[1][2] = mat.c2;
	res[2][2] = mat.c3;
	res[3][2] = mat.c4;

	res[0][3] = mat.d1;
	res[1][3] = mat.d2;
	res[2][3] = mat.d3;
	res[3][3] = mat.d4;

	return res;
}

int exportMeshes(ofstream& outfile, const aiScene* scene, vector<string> meshNames, bool isWeapon = false)
{
	unsigned int vertCount = 0;
	vector<aiMesh*> meshes;
	vector<Bone> bones;

	if (scene->mNumMeshes == 1)
	{
		meshes.push_back(scene->mMeshes[0]);
	}
	else
	{
		for (auto& meshName : meshNames)
		{
			auto foundMeshIter = find_if(scene->mMeshes, scene->mMeshes + scene->mNumMeshes,
				[&meshName](auto x) {return strcmp(x->mName.C_Str(), meshName.c_str()) == 0; });

			// check if all requested meshes are exists in the scene
			if (foundMeshIter == scene->mMeshes + scene->mNumMeshes)
			{
				printf("Couldn't find %s mesh\r\n", meshName);

				return 1;
			}
			else
			{
				meshes.push_back(*foundMeshIter);
				vertCount += (*foundMeshIter)->mNumVertices;
			}
		}
	}

	unsigned int bonesOffset = 0;
	unsigned int vertOffset = 0;

	// ���������� ������� ����������� �������� �� ������ �������

	std::unordered_map<int, std::set<int>> vertBoneIds;

	// bones counting

	for (auto currMesh : meshes)
	{
		for (int i = 0; i < currMesh->mNumBones; i++)
		{
			string currBoneName = currMesh->mBones[i]->mName.C_Str();

			auto foundBoneIter = find_if(bones.begin(), bones.end(),
				[&currBoneName](auto& x) { return x.name == currBoneName; });

			if (foundBoneIter == bones.end())
			{
				Bone bone(currBoneName);

				for (int j = 0; j < currMesh->mBones[i]->mNumWeights; j++)
				{
					aiVertexWeight currWeight = currMesh->mBones[i]->mWeights[j];
					currWeight.mVertexId += vertOffset;
					bone.weights.push_back(currWeight);
					
					vertBoneIds[currWeight.mVertexId].insert(i);
				}

				bone.offsetMatrix = currMesh->mBones[i]->mOffsetMatrix;

				bones.push_back(bone);
			}
			else
			{
				for (int j = 0; j < currMesh->mBones[i]->mNumWeights; j++)
				{
					aiVertexWeight currWeight = currMesh->mBones[i]->mWeights[j];
					currWeight.mVertexId += vertOffset;
					foundBoneIter->weights.push_back(currWeight);
				}
			}
		}

		vertOffset += currMesh->mNumVertices;
	}
	
	vertOffset = 0;

	// -------------- writing vertices -------------------
	unsigned int meshesCount = meshes.size();
	outfile.write((char*)&meshesCount, sizeof(unsigned int)); // mesh count, unsigned int

	for (auto currMesh : meshes)
	{
		//vertices count, unsigned int
		outfile.write((char*)&(currMesh->mNumVertices), sizeof(unsigned int));

		for (unsigned int vertNum = 0; vertNum < currMesh->mNumVertices; vertNum++)
		{
			// vertex coords
			outfile.write((char*)&(currMesh->mVertices[vertNum].x), sizeof(float));
			outfile.write((char*)&(currMesh->mVertices[vertNum].y), sizeof(float));
			outfile.write((char*)&(currMesh->mVertices[vertNum].z), sizeof(float));

			// normal
			outfile.write((char*)&(currMesh->mNormals[vertNum].x), sizeof(float));
			outfile.write((char*)&(currMesh->mNormals[vertNum].y), sizeof(float));
			outfile.write((char*)&(currMesh->mNormals[vertNum].z), sizeof(float));

			// texture coords
			if (currMesh->mTextureCoords[0]/* != nullptr*/)
			{
				outfile.write((char*)&(currMesh->mTextureCoords[0][vertNum].x), sizeof(float));
				outfile.write((char*)&(currMesh->mTextureCoords[0][vertNum].y), sizeof(float));
			}
			else
			{
				float zero = 0.f;

				outfile.write((char*)&(zero), sizeof(float));
				outfile.write((char*)&(zero), sizeof(float));
			}
		}

		// ������� � ��� ������������������, � ������� ��� ����, ��� ������� �������������

		unsigned int numIndices = 0;

		for (unsigned int i = 0; i < currMesh->mNumFaces; i++)
		{
			if (currMesh->mFaces[i].mNumIndices != 3)
			{
				printf("Not all faces are triangular in %s mesh\r\n", currMesh->mName.C_Str());
				
				return 1;
			}

			numIndices += currMesh->mFaces[i].mNumIndices;
		}

		outfile.write((char*)&numIndices, sizeof(unsigned int));

		for (unsigned int i = 0; i < currMesh->mNumFaces; i++)
			for (int j = 0; j < currMesh->mFaces[i].mNumIndices; j++)
			{
				unsigned int currIndex = vertOffset + currMesh->mFaces[i].mIndices[j];
				outfile.write((char*)&(currIndex), sizeof(unsigned int)); // unsigned int
			}

		// ������� ��� adjacency

		//auto reorderedIndices = ReorderIndices(currMesh, 3, true);
		//unsigned int reorderedIndicesNum = reorderedIndices.size();

		//outfile.write((char*)&(reorderedIndicesNum), sizeof(unsigned int));

		//for (auto& index : reorderedIndices)
		//{
		//	outfile.write((char*)&(index), sizeof(unsigned int)); // unsigned int
		//}

		vertOffset += currMesh->mNumVertices; // ��� ���� �� ������������, �.�. � ������ 1 ���
	}

	//-------------- writing bones ----------------

	unsigned int boneNum = bones.size();
	outfile.write((char*)&(boneNum), sizeof(unsigned int));

	for (auto& bone : bones)
	{
		writeStringIntoFile(outfile, bone.name.c_str(), bone.name.length() + 1);

		unsigned int weightsNum = bone.weights.size();
		outfile.write((char*)&weightsNum, sizeof(unsigned int)); // weights count

		for (auto& weight : bone.weights)
		{
			outfile.write((char*)&(weight.mVertexId), sizeof(unsigned int));
			outfile.write((char*)&(weight.mWeight), sizeof(float));
		}

		// offset matrix

		outfile.write((char*)&(bone.offsetMatrix.a1), sizeof(float));
		outfile.write((char*)&(bone.offsetMatrix.a2), sizeof(float));
		outfile.write((char*)&(bone.offsetMatrix.a3), sizeof(float));
		outfile.write((char*)&(bone.offsetMatrix.a4), sizeof(float));

		outfile.write((char*)&(bone.offsetMatrix.b1), sizeof(float));
		outfile.write((char*)&(bone.offsetMatrix.b2), sizeof(float));
		outfile.write((char*)&(bone.offsetMatrix.b3), sizeof(float));
		outfile.write((char*)&(bone.offsetMatrix.b4), sizeof(float));

		outfile.write((char*)&(bone.offsetMatrix.c1), sizeof(float));
		outfile.write((char*)&(bone.offsetMatrix.c2), sizeof(float));
		outfile.write((char*)&(bone.offsetMatrix.c3), sizeof(float));
		outfile.write((char*)&(bone.offsetMatrix.c4), sizeof(float));

		outfile.write((char*)&(bone.offsetMatrix.d1), sizeof(float));
		outfile.write((char*)&(bone.offsetMatrix.d2), sizeof(float));
		outfile.write((char*)&(bone.offsetMatrix.d3), sizeof(float));
		outfile.write((char*)&(bone.offsetMatrix.d4), sizeof(float));
	}

	return 0;
}

//std::vector<aiVectorKey> getMergedPosKeys(const aiNodeAnim *shorter, const aiNodeAnim *larger)
//{
//	std::vector<aiVectorKey> positionKeys;
//
//	if (shorter->mNumPositionKeys < larger->mNumPositionKeys)
//	{
//		auto mergedKeysPerCurrChannelKey = float(float(shorter->mNumPositionKeys / larger->mNumPositionKeys));
//
//		for (int i = 0; i < larger->mNumPositionKeys; ++i)
//		{
//			int mergedKeyPreNum = int(mergedKeysPerCurrChannelKey * i);
//			int mergedKeyPostNum = int(mergedKeysPerCurrChannelKey * i) + 1 >= shorter->mNumPositionKeys ?
//				shorter->mNumPositionKeys - 1 : int(mergedKeysPerCurrChannelKey * i);
//
//			float interpolant = float(mergedKeysPerCurrChannelKey) * float(i) - float(mergedKeyPreNum);
//
//			auto &mergedKeyPre = shorter->mPositionKeys[mergedKeyPreNum];
//			auto &mergedKeyPost = shorter->mPositionKeys[mergedKeyPostNum];
//
//			auto mergedComponent = glm::mix(glm::vec3(mergedKeyPre.mValue.x, mergedKeyPre.mValue.y, mergedKeyPre.mValue.z),
//				glm::vec3(mergedKeyPost.mValue.x, mergedKeyPost.mValue.y, mergedKeyPost.mValue.z), interpolant);
//
//			mergedComponent += glm::vec3(larger->mPositionKeys[i].mValue.x,
//				larger->mPositionKeys[i].mValue.y, larger->mPositionKeys[i].mValue.z);
//
//			aiVectorKey Key = aiVectorKey(larger->mPositionKeys[i].mTime,
//				aiVector3D(mergedComponent.x, mergedComponent.y, mergedComponent.z));
//
//			positionKeys.push_back(Key);
//		}
//	}
//
//	return positionKeys;
//}

// ������� ��������� ���������� ������/�������, ���� ���������, ����� �������� �������
void mergeChannels(aiAnimation *currentAnim)
{
	aiAnimation mergedAnim;

	mergedAnim.mDuration = currentAnim->mDuration;
	mergedAnim.mTicksPerSecond = currentAnim->mTicksPerSecond;
	std::unordered_map<std::string, aiNodeAnim*> mergedChannels;

	for (int i = 0; i < currentAnim->mNumChannels; ++i)
	{
		auto *currChannel = currentAnim->mChannels[i];

		auto name = std::string(currChannel->mNodeName.C_Str());
		auto delPos = name.find("$");
		auto boneName = name.substr(0, delPos - 1);

		if (delPos != std::string::npos)
		{
			if (mergedChannels.find(boneName) == mergedChannels.end())
			{
				mergedChannels[boneName] = new aiNodeAnim();
			}

			auto *mergedChannel = mergedChannels[boneName];

			if (!mergedChannels[boneName]->mPositionKeys)
				mergedChannels[boneName]->mPositionKeys = new aiVectorKey[glm::max(mergedChannel->mNumPositionKeys, currChannel->mNumPositionKeys)];
			if (!mergedChannels[boneName]->mRotationKeys)
				mergedChannels[boneName]->mRotationKeys = new aiQuatKey[glm::max(mergedChannel->mNumRotationKeys, currChannel->mNumRotationKeys)];
			if (!mergedChannels[boneName]->mScalingKeys)
				mergedChannels[boneName]->mScalingKeys = new aiVectorKey[glm::max(mergedChannel->mNumScalingKeys, currChannel->mNumScalingKeys)];

			mergeChannels<aiVectorKey>(mergedChannel, currChannel, mergedChannel->mNumPositionKeys, currChannel->mNumPositionKeys,
				mergedChannel->mPositionKeys, currChannel->mPositionKeys);
			mergeChannels<aiQuatKey>(mergedChannel, currChannel, mergedChannel->mNumRotationKeys, currChannel->mNumRotationKeys,
				mergedChannel->mRotationKeys, currChannel->mRotationKeys);
			mergeChannels<aiVectorKey>(mergedChannel, currChannel, mergedChannel->mNumScalingKeys, currChannel->mNumScalingKeys,
				mergedChannel->mScalingKeys, currChannel->mScalingKeys);

			mergedChannel->mNodeName = boneName;
		}
	}

	aiNodeAnim armatureChannel;

	for (int i = 0; i < currentAnim->mNumChannels; ++i)
	{
		auto currChannel = currentAnim->mChannels[i];

		if (std::string(currChannel->mNodeName.C_Str()) == "Armature") // ��������� ����������� ����
		{
			mergedChannels["Armature"] = new aiNodeAnim();

			mergedChannels["Armature"]->mPositionKeys = new aiVectorKey[currChannel->mNumPositionKeys];
			mergedChannels["Armature"]->mNumPositionKeys = currChannel->mNumPositionKeys;
			std::copy(currChannel->mPositionKeys, currChannel->mPositionKeys + currChannel->mNumPositionKeys, mergedChannels["Armature"]->mPositionKeys);

			mergedChannels["Armature"]->mRotationKeys = new aiQuatKey[currChannel->mNumRotationKeys];
			mergedChannels["Armature"]->mNumRotationKeys = currChannel->mNumRotationKeys;
			std::copy(currChannel->mRotationKeys, currChannel->mRotationKeys + currChannel->mNumRotationKeys, mergedChannels["Armature"]->mRotationKeys);

			mergedChannels["Armature"]->mScalingKeys = new aiVectorKey[currChannel->mNumScalingKeys];
			mergedChannels["Armature"]->mNumScalingKeys = currChannel->mNumScalingKeys;
			std::copy(currChannel->mScalingKeys, currChannel->mScalingKeys + currChannel->mNumRotationKeys, mergedChannels["Armature"]->mScalingKeys);

			mergedChannels["Armature"]->mNodeName = currChannel->mNodeName;
			mergedChannels["Armature"]->mPreState = currChannel->mPreState;
			mergedChannels["Armature"]->mPostState = currChannel->mPostState;
		}
	}

	for (int i = 0; i < currentAnim->mNumChannels; ++i)
	{
		delete currentAnim->mChannels[i];
	}

	delete[] currentAnim->mChannels;

	// �������� � �������� ��� ��������� ������

	currentAnim->mChannels = new aiNodeAnim*[mergedChannels.size()];
	int channelNum = 0;

	for (auto p : mergedChannels)
	{
		auto *currChannel = currentAnim->mChannels[channelNum++] = new aiNodeAnim();

		currChannel->mPositionKeys = new aiVectorKey[p.second->mNumPositionKeys];
		currChannel->mNumPositionKeys = p.second->mNumPositionKeys;
		std::copy(p.second->mPositionKeys, p.second->mPositionKeys + p.second->mNumPositionKeys, currChannel->mPositionKeys);

		currChannel->mRotationKeys = new aiQuatKey[p.second->mNumRotationKeys];
		currChannel->mNumRotationKeys = p.second->mNumRotationKeys;
		std::copy(p.second->mRotationKeys, p.second->mRotationKeys + p.second->mNumRotationKeys, currChannel->mRotationKeys);

		currChannel->mScalingKeys = new aiVectorKey[p.second->mNumScalingKeys];
		currChannel->mNumScalingKeys = p.second->mNumScalingKeys;
		std::copy(p.second->mScalingKeys, p.second->mScalingKeys + p.second->mNumScalingKeys, currChannel->mScalingKeys);

		currChannel->mNodeName = p.second->mNodeName;
		currChannel->mPreState = p.second->mPreState;
		currChannel->mPostState = p.second->mPostState;
	}

	currentAnim->mNumChannels = mergedChannels.size();
}

int exportAnimations(const aiScene* scene, const string& sourceFileName, const string& outFileName, const glm::quat& rotationInit = glm::quat(), const double& durationModificator = 1.0)
{
	for (int i = 0; i < scene->mNumAnimations; i++)
	{
		auto* currentAnim = scene->mAnimations[i];
		std::string animOutName = outFileName + "-" + currentAnim->mName.C_Str() + ".anim";

		for (auto iter = animOutName.begin(); iter != animOutName.end(); iter++)
		{
			if (*iter == '|')
				*iter = '_';
		}

		ofstream animOutFile(animOutName, ios::out | ios::binary);

		// -----

	 	auto duration = currentAnim->mDuration * durationModificator;
		auto ticksPerSecond = currentAnim->mTicksPerSecond / durationModificator;

		// animation duration in ticks
		animOutFile.write((char*)&(duration), sizeof(double)); // DOUBLE
		animOutFile.write((char*)&(ticksPerSecond), sizeof(double));

		//channels number, unsigned int
		animOutFile.write((char*)&(currentAnim->mNumChannels), sizeof(unsigned int));

		vector<string> channelNames;

		// ������� ������������ ���������� ������ ��������
		unsigned int keyCountMax = 0;
		for (int channelNum = 0; channelNum < currentAnim->mNumChannels; channelNum++)
		{
			channelNames.push_back(currentAnim->mChannels[channelNum]->mNodeName.C_Str());
			unsigned int currMax = glm::max(glm::max(currentAnim->mChannels[channelNum]->mNumPositionKeys,
				currentAnim->mChannels[channelNum]->mNumRotationKeys), currentAnim->mChannels[channelNum]->mNumScalingKeys);
			
			if (currMax > keyCountMax)
				keyCountMax = currMax;
		}

		animOutFile.write((char*)&(keyCountMax), sizeof(unsigned int));

		bool rotated = false;

		std::set<std::string> nodeNames;

		for (int channelNum = 0; channelNum < currentAnim->mNumChannels; channelNum++)
		{
			auto* channel = currentAnim->mChannels[channelNum];
			glm::vec3 maxAngles, minAngles; // ������������ � ����������� ����, ������������ ���������� ������ ���� x, y, z

			// channel node name
			writeStringIntoFile(animOutFile, channel->mNodeName.C_Str(), channel->mNodeName.length + 1);

			nodeNames.insert(channel->mNodeName.C_Str());

			for (int keyNum = 0; keyNum < keyCountMax; keyNum++)
			{
				// ����� ����� �������� �����

				double currFrameTime = currentAnim->mDuration * (static_cast<double>(keyNum)) / static_cast<double>(keyCountMax);

				animOutFile.write((char*)&(currFrameTime), sizeof(double));

				// �����

				bool keyWritten = false;

				if (channel->mNumPositionKeys == 1)
				{
					writeVec3ToFile(channel->mPositionKeys[0].mValue, animOutFile);					
					keyWritten = true;
				}
				else if (channel->mNumPositionKeys == keyCountMax)
				{
					writeVec3ToFile(channel->mPositionKeys[keyNum].mValue, animOutFile);
					keyWritten = true;
				}
				else
				{
					// ���� ���������� � ��������� �������� ����� ������
					for (unsigned int i = 0; i < channel->mNumPositionKeys - 1; ++i)
					{
						auto& prevAnimKey = channel->mPositionKeys[i];
						auto& nextAnimKey = channel->mPositionKeys[i + 1];

						// ����� ��� �������� �����
						if (prevAnimKey.mTime <= currFrameTime && currFrameTime < nextAnimKey.mTime)
						{
							glm::vec3 currKeyPos = glm::mix(
								glm::vec3(
									prevAnimKey.mValue.x,
									prevAnimKey.mValue.y,
									prevAnimKey.mValue.z
								),
								glm::vec3(
									nextAnimKey.mValue.x,
									nextAnimKey.mValue.y,
									nextAnimKey.mValue.z
								),
								(currFrameTime - prevAnimKey.mTime) / (nextAnimKey.mTime - prevAnimKey.mTime)
							);
							
							writeVec3ToFile(currKeyPos, animOutFile);
							keyWritten = true;

							break;
						}
					}
				}

				// ���� ���� ��� ��� �� �������, ����� ��������� ����
				if (!keyWritten)
				{
					writeVec3ToFile(channel->mPositionKeys[channel->mNumPositionKeys - 1].mValue, animOutFile);
				}

				keyWritten = false;

				// ��������
				if (channel->mNumRotationKeys == 1)
				{
					writeQuatToFile(channel->mRotationKeys[0].mValue, animOutFile);

					keyWritten = true;
				}
				else if (channel->mNumRotationKeys == keyCountMax)
				{
					writeQuatToFile(channel->mRotationKeys[keyNum].mValue, animOutFile);
					keyWritten = true;
				}
				else 
				{
					// ���� ���������� � ��������� �������� ����� ������
					for (unsigned int i = 0; i < channel->mNumRotationKeys - 1; ++i)
					{
						auto& prevAnimKey = channel->mRotationKeys[i];
						auto& nextAnimKey = channel->mRotationKeys[i + 1];

						// ����� ��� �������� �����
						if (prevAnimKey.mTime <= currFrameTime && currFrameTime < nextAnimKey.mTime)
						{
							glm::quat currRot = glm::slerp(
								glm::quat(
									prevAnimKey.mValue.x,
									prevAnimKey.mValue.y,
									prevAnimKey.mValue.z,
									prevAnimKey.mValue.w
								),
								glm::quat(
									nextAnimKey.mValue.x,
									nextAnimKey.mValue.y,
									nextAnimKey.mValue.z,
									nextAnimKey.mValue.w
								),
								static_cast<float>((currFrameTime - prevAnimKey.mTime) / (nextAnimKey.mTime - prevAnimKey.mTime))
							);

							writeQuatToFile(currRot, animOutFile);
							keyWritten = true;

							break;
						}
					}
				}

				if (!keyWritten) // ����� ��������� ����
				{
					writeQuatToFile(channel->mRotationKeys[channel->mNumRotationKeys].mValue, animOutFile);

				}

				keyWritten = false;

				// �������
				if (channel->mNumScalingKeys == 1)
				{
					writeVec3ToFile(channel->mScalingKeys[0].mValue, animOutFile);
					keyWritten = true;
				}
				else if (channel->mNumScalingKeys == keyCountMax)
				{
					writeVec3ToFile(channel->mScalingKeys[keyNum].mValue, animOutFile);
					keyWritten = true;
				}
				else
				{
					for (unsigned int i = 0; i < channel->mNumScalingKeys - 1; ++i)
					{
						auto& prevAnimKey = channel->mScalingKeys[i];
						auto& nextAnimKey = channel->mScalingKeys[i + 1];

						// ����� ��� �������� �����
						if (prevAnimKey.mTime <= currFrameTime && currFrameTime < nextAnimKey.mTime)
						{
							glm::vec3 currScale = glm::mix(
								glm::vec3(
									prevAnimKey.mValue.x,
									prevAnimKey.mValue.y,
									prevAnimKey.mValue.z
								),
								glm::vec3(
									nextAnimKey.mValue.x,
									nextAnimKey.mValue.y,
									nextAnimKey.mValue.z
								),
								static_cast<float>((currFrameTime - prevAnimKey.mTime) / (nextAnimKey.mTime - prevAnimKey.mTime))
							);

							writeVec3ToFile(currScale, animOutFile);
							keyWritten = true;

							break;
						}
					}
				}

				if (!keyWritten)
				{
					writeVec3ToFile(channel->mScalingKeys[channel->mNumScalingKeys].mValue, animOutFile);
				}
			}

			rotated = true;
		}

		animOutFile.close();
	}

	return 0;
}

void exportMapProp(const aiScene* scene, const string& outFileName, const std::string &invertedAxis = "")
{
	std::ofstream currTilesetVerticesFile(outFileName + ".tsv", std::ios::out | std::ios::binary);

	for (int i = 0; i < scene->mNumMeshes; ++i)
	{
		auto transform = scene->mRootNode->mTransformation * scene->mRootNode->mChildren[0]->mTransformation;

		auto *mesh = scene->mMeshes[i];
		
		// ����� � ���� ������������ ������
		// �������
		size_t resVertsSize = mesh->mNumVertices;

		currTilesetVerticesFile.write((const char*)&resVertsSize, sizeof(size_t)); // ���-�� ������

		for (int i = 0; i < mesh->mNumVertices; ++i)
		{
			// �������
			auto &vert = transform * mesh->mVertices[i];

			//glm::vec3 vertRound = glm::round(glm::vec3(vert.x * 100.f, vert.y * 100.f, vert.z * 100.f)) / 100.f;
			
			if (invertedAxis == "-y")
				vert.y = -vert.y;
			if (invertedAxis == "-x")
				vert.x = -vert.x;
			if (invertedAxis == "-z")
				vert.z = -vert.z;

			currTilesetVerticesFile.write((const char*)&(vert.x), sizeof(float)); // x
			currTilesetVerticesFile.write((const char*)&(vert.y), sizeof(float)); // y
			currTilesetVerticesFile.write((const char*)&(vert.z), sizeof(float)); // z

			// �������
			auto &normal = (aiMatrix3x3(transform) * mesh->mNormals[i]).Normalize();
			//normal.x = -normal.x;

			currTilesetVerticesFile.write((const char*)&(normal.x), sizeof(float)); // x
			currTilesetVerticesFile.write((const char*)&(normal.y), sizeof(float)); // y
			currTilesetVerticesFile.write((const char*)&(normal.z), sizeof(float)); // z
		}

		// �������
		size_t resIndsSize = mesh->mNumFaces * 3;
		currTilesetVerticesFile.write((const char*)&resIndsSize, sizeof(size_t)); // ���-�� ��������

		for (int i = 0; i < resIndsSize; ++i)
		{
			unsigned int currIndex = mesh->mFaces[i / 3].mIndices[i % 3];
			currTilesetVerticesFile.write((const char*)&(currIndex), sizeof(unsigned int)); // �������
		}
	}

	currTilesetVerticesFile.close();
}

int main(int argc, char *argv[])
{
	if (argc < 2) return 1;

	// argc[1] : source filename

	// argc[2] : output dir

	// argc[3] :
	// -onlymesh <meshname> - exports only one mesh
	// -withoutmesh <meshname> - exports all mesh merged together except one mesh
	// -showmeshnames - shows mesh names

	string source = argv[1];

	string outputDir = argv[2];

	string outputFile = argv[2] + splitpath(source, std::set<char>{ '\\' }).back();

	string flag = "";
	string flagArg = "";
	string flagSecArg;
	if (argc >= 4)
	{
		flag = argv[3];
		if (argc >= 5)
			flagArg = argv[4];
		if (argc >= 6)
			flagSecArg = argv[5];
	}

	Importer importer;
	importer.SetPropertyInteger(AI_CONFIG_PP_FD_REMOVE, 1);

	DefaultLogger::create("", Logger::VERBOSE);


	// Now I am ready for logging my stuff
	DefaultLogger::get()->info("import");

	const auto* scene = importer.ReadFile(source,
		//aiProcessPreset_TargetRealtime_Quality
		(\
		aiProcess_CalcTangentSpace | \
		aiProcess_GenNormals | \
		aiProcess_JoinIdenticalVertices | \
		aiProcess_LimitBoneWeights | \
		aiProcess_Triangulate | \
		aiProcess_GenUVCoords | \
		aiProcess_SortByPType | \
		aiProcess_ImproveCacheLocality | \
		aiProcess_FindDegenerates | \
		aiProcess_FindInvalidData | \
		/*aiProcess_FixInfacingNormals | \*/
		aiProcess_ValidateDataStructure | \
		aiProcess_OptimizeGraph | \
		0)
	);

	const char* error = importer.GetErrorString();

	if (strcmp((error), "") != 0)
	{
		printf(error);
	}

	// ������� �������
	if (flag == "-props")
	{
		exportMapProp(scene, std::string(outputFile, 0, outputFile.length() - 4), flagArg);
		return 0;
	}

	if (scene == nullptr)
	{
		printf("Can't open source file");
		cin.ignore();

		return 1;
	}

	if (flag == "-showmeshnames")
	{
		for (int i = 0; i < scene->mNumMeshes; i++)
		{
			printf("%s \r\n", scene->mMeshes[i]->mName.C_Str());
		}
		cin.ignore();

		return 0;
	}

	// animations

	if (scene->HasAnimations())
	{
		exportAnimations(scene, source, outputFile, glm::quat(glm::cos(-glm::quarter_pi<float>()), glm::sin(-glm::quarter_pi<float>()) * glm::vec3(0.0, 1.0, .0)));
	}

	if (flag == "-onlyanim")
		return 0;

	vector<string> meshNamesToExport;

	if (flag == "-onlymesh")
		meshNamesToExport.push_back(flagArg);
	else
		for (int i = 0; i < scene->mNumMeshes; i++)
		{
			if (flag == "-withoutmesh" /*&& strcmp(scene->mMeshes[i]->mName.C_Str(), flagArg.c_str()) == 0*/)
				continue;

			meshNamesToExport.push_back(scene->mMeshes[i]->mName.C_Str());
		}
	
	if (scene == nullptr)
	{
		printf("Can't parse scene file.\r\n");
		cin.ignore();
		return 1;
	}

	// skeleton
	ofstream skeletOutFile(outputFile + ".sklt", ios::out | ios::binary);

	exportSkeleton(skeletOutFile, scene);
	skeletOutFile.close();

	// meshes

	if (flag != "-nomesh")
	{

		std::string outFileName = outputFile + ".msh";
		ofstream meshOutFile(outputFile + ".msh", ios::out | ios::binary);

		exportMeshes(meshOutFile, scene, meshNamesToExport);
		meshOutFile.close();
	}

	DefaultLogger::kill();

    return 0;
}