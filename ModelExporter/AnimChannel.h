#include <string>
#include <vector>

#pragma once

struct AnimKey
{
	double time;
	glm::vec3 pos;
	glm::quat rot;
	glm::vec3 scale;

	AnimKey() :
		time(0.0), pos(0.f), rot(glm::mat3(1.f)), scale(glm::vec3(1.f))
	{}
};

struct ChannelNode
{
	std::string name;
	std::vector<ChannelNode> childNodes;
	std::vector<AnimKey> keys;
	bool isFinal;
	std::string boneName;

	ChannelNode(const std::string &name, const int &numKeys, const std::vector<std::string> allowedBones, const glm::mat4 &baseTransform) :
		name(name), keys(numKeys), boneName("")
	{
		for (auto &allowedBoneName : allowedBones)
		{
			if (name.find(allowedBoneName) != std::string::npos)
			{
				if (boneName.length() < allowedBoneName.length())
					boneName = allowedBoneName;
			}
		}

		auto pos = glm::vec3(baseTransform[3]);
		auto rot = glm::toQuat(glm::mat3(baseTransform));

		for (auto &key : keys)
		{
			key.pos = pos;
			key.rot = rot;
			key.scale = glm::vec3(1.f);
		}
	}
	
	~ChannelNode() {}

	inline void FindNode(const std::string &nodeName, ChannelNode *&res)
	{
		if (nodeName == name)
			res = this;
		else
		{
			for (auto &childNode : childNodes)
			{
				childNode.FindNode(nodeName, res);
				if (res)
					break;
			}
		}
	}
};