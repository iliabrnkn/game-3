#pragma once

#include <functional>
#include <string>
#include <set>
#include <unordered_map>
#include <memory>
//#include <glm/detail/func_matrix.inl>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <algorithm>

#include "Importer.hpp"
#include "postprocess.h"
#include "scene.h"
#include "DefaultLogger.hpp"
#include "LogStream.hpp"

template<typename T>

// ���������� ��� ������������� � ����

T mergeKeyValues(T &mergedVal , T &currVal)
{
	T* funcRes = new T();

	if (sizeof(T) == sizeof(aiVector3D))
	{
		aiVector3D *mergedChannelKeyVal = reinterpret_cast<aiVector3D*>(&mergedVal);
		aiVector3D *currChannelKeyVal = reinterpret_cast<aiVector3D*>(&currVal);

		/*glm::vec3 res(mergedChannelKeyVal->x + currChannelKeyVal->x,
			mergedChannelKeyVal->y + currChannelKeyVal->y,
			mergedChannelKeyVal->z + currChannelKeyVal->z);*/
		glm::vec3 res = glm::vec3(currChannelKeyVal->x, currChannelKeyVal->y, currChannelKeyVal->z);

		*mergedChannelKeyVal = aiVector3D(res.x, res.y, res.z);

		//memcpy((void*)funcRes, (void*)(&mergedChannelKeyVal), sizeof(aiVector3D));
		*(reinterpret_cast<aiVector3D*>(funcRes)) = *mergedChannelKeyVal;
	}
	else if (sizeof(T) == sizeof(aiQuaternion))
	{
		aiQuaternion *mergedChannelKeyVal = reinterpret_cast<aiQuaternion*>(&mergedVal);

		glm::quat mergedQuat(mergedChannelKeyVal->w, mergedChannelKeyVal->x, mergedChannelKeyVal->y, mergedChannelKeyVal->z);

		aiQuaternion *currChannelKeyVal = reinterpret_cast<aiQuaternion*>(&currVal);

		glm::quat currQuat(currChannelKeyVal->w, currChannelKeyVal->x, currChannelKeyVal->y, currChannelKeyVal->z);

		/*glm::quat res = mergedQuat * currQuat;*/
		glm::quat res = currQuat;

		*mergedChannelKeyVal = aiQuaternion(res.w, res.x, res.y, res.z);

		//memcpy((void*)funcRes, (void*)(&mergedChannelKeyVal), sizeof(aiQuaternion));
		*(reinterpret_cast<aiQuaternion*>(funcRes)) = *mergedChannelKeyVal;
	}

	return *funcRes;
}

// ���������� ��� ����� �������� � ����

template<typename T>
void getMergedKeys(const aiNodeAnim *shorter, const aiNodeAnim *larger, const unsigned int &shorterNumKeys, const unsigned int &largerNumKeys,
	T* shorterKeys, T* mLargerKeys, std::vector<T> &resKeys)
{
	if (shorterNumKeys < largerNumKeys)
	{
		float shorterKeysPerCurrChannelKey = float(shorterNumKeys) / float(largerNumKeys);

		for (int i = 0; i < largerNumKeys; ++i)
		{
			int shorterKeyPreNum = int(shorterKeysPerCurrChannelKey * i);
			int shorterKeyPostNum = int(shorterKeysPerCurrChannelKey * i) + 1 >= shorterNumKeys ?
				shorterNumKeys - 1 : int(shorterKeysPerCurrChannelKey * i);

			float interpolant = shorterKeysPerCurrChannelKey * float(i) - float(shorterKeyPreNum);

			if (sizeof(T) == sizeof(aiVectorKey))
			{
				aiVectorKey* largerVecKeys = reinterpret_cast<aiVectorKey*>(mLargerKeys);
				aiVectorKey* shorterVecKeys = reinterpret_cast<aiVectorKey*>(shorterKeys);
				//std::vector<aiVectorKey> &resVecKeys = dynamic_cast<std::vector<aiVectorKey>&>(resKeys);
				std::vector<aiVectorKey> *resVecKeys = reinterpret_cast<std::vector<aiVectorKey>*>(&resKeys);

				auto &shorterKeyPre = shorterVecKeys[shorterKeyPreNum];
				auto &shorterKeyPost = shorterVecKeys[shorterKeyPostNum];

				glm::vec3 shorterComponent = glm::mix(glm::vec3(shorterKeyPre.mValue.x, shorterKeyPre.mValue.y, shorterKeyPre.mValue.z),
					glm::vec3(shorterKeyPost.mValue.x, shorterKeyPost.mValue.y, shorterKeyPost.mValue.z), interpolant);

				aiVectorKey resKey = aiVectorKey(largerVecKeys[i].mTime,
					mergeKeyValues<aiVector3D>(aiVector3D(shorterComponent.x, shorterComponent.y, shorterComponent.z), largerVecKeys[i].mValue));

				resVecKeys->push_back(resKey);
			}
			else if (sizeof(T) == sizeof(aiQuatKey))
			{
				aiQuatKey* largerQuatKeys = reinterpret_cast<aiQuatKey*>(mLargerKeys);
				aiQuatKey* shorterQuatKeys = reinterpret_cast<aiQuatKey*>(shorterKeys);
				std::vector<aiQuatKey> *resQuatKeys = reinterpret_cast<std::vector<aiQuatKey>*>(&resKeys);

				auto &shorterKeyPre = glm::quat(shorterQuatKeys[shorterKeyPreNum].mValue.w, 
					shorterQuatKeys[shorterKeyPreNum].mValue.x, shorterQuatKeys[shorterKeyPreNum].mValue.y, shorterQuatKeys[shorterKeyPreNum].mValue.z);
				auto &shorterKeyPost = glm::quat(shorterQuatKeys[shorterKeyPostNum].mValue.w,
					shorterQuatKeys[shorterKeyPostNum].mValue.x, shorterQuatKeys[shorterKeyPostNum].mValue.y, shorterQuatKeys[shorterKeyPostNum].mValue.z);

				auto shorterComponent = glm::slerp(shorterKeyPre, shorterKeyPost, interpolant);
				
				aiQuatKey resKey = aiQuatKey(largerQuatKeys[i].mTime,
					mergeKeyValues<aiQuaternion>(aiQuaternion(shorterComponent.w, shorterComponent.x, shorterComponent.y, shorterComponent.z), 
						largerQuatKeys[i].mValue));

				resQuatKeys->push_back(resKey);
			}
		}
	}
}

// ���������� ��� ������� � ���� ����� ��������

template<typename T>
void mergeChannels(aiNodeAnim *mergedChannel, aiNodeAnim *currChannel, unsigned int &numKeysMergedChannel, unsigned int &numKeysCurrChannel,
	T *mergedChannelKeys, T* currChannelKeys)
{
	if (numKeysMergedChannel == 0) // ���� ������ ������
	{
		std::copy(currChannelKeys, currChannelKeys + numKeysCurrChannel,
			mergedChannelKeys);
		numKeysMergedChannel = numKeysCurrChannel;
	}
	else if (/*numKeysCurrChannel > numKeysMergedChannel ||*/ numKeysCurrChannel < numKeysMergedChannel) // ���� � ����������� ������ ������ ������
	{
		std::vector<T> mergedKeys;

		if (numKeysMergedChannel < numKeysCurrChannel)
			 getMergedKeys<T>(mergedChannel, currChannel, numKeysMergedChannel, numKeysCurrChannel,
				mergedChannelKeys, currChannelKeys, mergedKeys);
		else if (numKeysMergedChannel > numKeysCurrChannel)
			getMergedKeys<T>(currChannel, mergedChannel, numKeysCurrChannel, numKeysMergedChannel,
				currChannelKeys, mergedChannelKeys, mergedKeys);

		if (numKeysMergedChannel < mergedKeys.size())
		{
			if ((void*)(mergedChannel->mPositionKeys) == (void*)(mergedChannelKeys))
			{
				delete[] mergedChannel->mPositionKeys;
				unsigned int s = mergedKeys.size();
				mergedChannel->mPositionKeys = new aiVectorKey[s];
				std::copy(mergedKeys.begin(), mergedKeys.end(), reinterpret_cast<T*>(mergedChannel->mPositionKeys));
			}
			else if ((void*)(mergedChannel->mRotationKeys) == (void*)(mergedChannelKeys))
			{
				delete[] mergedChannel->mRotationKeys;
				unsigned int s = mergedKeys.size();
				mergedChannel->mRotationKeys = new aiQuatKey[s];
				std::copy(mergedKeys.begin(), mergedKeys.end(), reinterpret_cast<T*>(mergedChannel->mRotationKeys));
			}
			else if ((void*)(mergedChannel->mScalingKeys) == (void*)(mergedChannelKeys))
			{
				delete[] mergedChannel->mScalingKeys;
				unsigned int s = mergedKeys.size();
				mergedChannel->mScalingKeys = new aiVectorKey[s];
				std::copy(mergedKeys.begin(), mergedKeys.end(), reinterpret_cast<T*>(mergedChannel->mScalingKeys));
			}
		}

		numKeysMergedChannel = mergedKeys.size();
	}
	else if (numKeysCurrChannel == numKeysMergedChannel) // ���� ������� �� ������
	{
		for (int i = 0; i < numKeysCurrChannel; ++i)
		{
			if (sizeof(T) == sizeof(aiVectorKey))
			{
				aiVectorKey *mergedChannelKey = reinterpret_cast<aiVectorKey*>(&(mergedChannelKeys[i]));
				aiVectorKey *currChannelKey = reinterpret_cast<aiVectorKey*>(&(currChannelKeys[i]));

				mergedChannelKey = &(aiVectorKey(currChannelKey->mTime, mergeKeyValues<aiVector3D>(mergedChannelKey->mValue, currChannelKey->mValue)));
			}
			else if (sizeof(T) == sizeof(aiQuatKey))
			{
				aiQuatKey *mergedChannelKey = reinterpret_cast<aiQuatKey*>(&(mergedChannelKeys[i]));
				aiQuatKey *currChannelKey = reinterpret_cast<aiQuatKey*>(&(currChannelKeys[i]));

				mergedChannelKey = &(aiQuatKey(currChannelKey->mTime, mergeKeyValues<aiQuaternion>(mergedChannelKey->mValue, currChannelKey->mValue)));
			}
		}
	}
}