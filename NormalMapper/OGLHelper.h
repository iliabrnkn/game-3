#pragma once

#include <map>
#include <unordered_map>
#include <deque>
#include <string>

#include <windows.h>
#include <GL/glew/glew.h>
#include <GL/GL.h>

#include <SFML/Window.hpp>
#include <SFML\Graphics.hpp>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

#include "Structs.h"

GLuint normalsShader, depthShader;

std::string vertexShader =
"#version 330 \r\n"
"uniform mat4 viewMatrix; \r\n"
"uniform mat4 projectionMatrix; \r\n"
"uniform mat2 inversed2DMat;"
"\r\n"
"in vec3 position;\r\n"
"in vec3 normal;\r\n"
"in vec3 screenPos;\r\n"
"\r\n"
"out vec3 fragNormal;\r\n"
"out vec3 fragPos;\r\n"
"\r\n"
"vec3 ScreenToScene(vec2 screenCoord){\r\n"
"	return vec3(inversed2DMat * screenCoord, 0.f);\r\n"
"}\r\n"
"\r\n"
"void main()\r\n"
"{	\r\n"
//"	gl_Position = projectionMatrix * viewMatrix * vec4(position, 1.f);\r\n"
"	vec4 transformedVert = viewMatrix * vec4(position, 1.f);\r\n"
"	gl_Position = projectionMatrix * transformedVert;\r\n"
//"	gl_Position = projectionMatrix * vec4(screenPos.x, screenPos.y, transformedVert.z, transformedVert.w);"
"	fragPos = position;"
"	fragNormal = normal;\r\n"
"}";

std::string normalsFragShader =
"#version 330\r\n"
"\r\n"
"in vec3 fragNormal;\r\n"
"in vec3 fragPos;\r\n"
"\r\n"
"layout(location = 0) out vec4 outColor;\r\n"
"\r\n"
"float getZLen(vec2 point)\r\n"
"{\r\n"
"	if (point != vec2(0.0))\r\n"
"		return length(point) * dot(normalize(point), normalize(vec2(1.0, 1.0)));\r\n"
"	else \r\n"
"		return 0.0;\r\n"
"}\r\n"
"\r\n"
"void main(){\r\n"
"\r\n"
"	outColor = vec4(fragNormal, 1.0);\r\n"
"}\r\n";

std::string depthFragShader =
"#version 330\r\n"
"\r\n"
"const float TILESIDE = 45.254833995939;"
"\r\n"
"uniform mat2 inversed2DMat;"
"uniform mat4 rawMatrix;"
"uniform vec2 tileSize;"
"\r\n"
"in vec3 fragNormal;\r\n"
"in vec3 fragPos;\r\n"
"\r\n"
"layout(location = 0) out vec4 outColor;\r\n"
"\r\n"
"vec3 ScreenToScene(vec2 screenCoord){\r\n"
"	return vec3(inversed2DMat * screenCoord, 0.f);\r\n"
"}\r\n"
"vec2 SceneToScreen(vec3 scenePos)\r\n"
"{\r\n"
"	return vec2(rawMatrix * vec4(scenePos.x * TILESIDE, scenePos.y * TILESIDE, scenePos.z, 1));\r\n"
"}\r\n"
"\r\n"
"void main(){\r\n"
"	vec2 fragCoord = vec2(float(gl_FragCoord.x), 855.f - float(gl_FragCoord.y));"
"	vec2 offset = vec2(float(int(fragCoord.x / tileSize.x)) * tileSize.x,"
"		float(int(fragCoord.y / tileSize.y)) * tileSize.y) + vec2(tileSize.x / 2.f, tileSize.y - 32.f);"
//"	outColor = vec4(abs(fragPos - ScreenToScene(offset))/10.f, 1.0);\r\n"
"	outColor = vec4(abs(vec2(ScreenToScene(fragCoord + vec2(0, fragPos.z * 39) - offset))/10.f), fragPos.z / 10.f, 1.0);\r\n"
"}\r\n";

class OGLHelper
{
public:
	
	void InitOpengl();
	GLuint InitVAO(std::vector<SimpleVert> vect, std::vector<unsigned int> indices, GLuint bufVert, GLuint indVert);
	GLuint InitShader(const std::string& vsSource, const std::string& fsSource);
	
	inline void checkIfGLError()
	{
		auto err = glGetError();

		assert(err == GL_NO_ERROR);
	}

	std::pair<sf::Image, sf::Image> DrawGeometries(std::unordered_map<int, NMTileGeometry> &geometries, const int &tsWidth, const int &tsHeight,
		const int &tileWidth, const int &tileHeight, const int &tileCount);

	int AllocateUniformVar(const std::string &varName, GLuint shaderName);

	inline void SetStates()
	{
		glDepthMask(GL_TRUE); checkIfGLError();
		glClearDepth(1.f); checkIfGLError();
		glClearColor(0.0f, 0.0f, 0.0f, 0.f); checkIfGLError();
		glClearStencil(0); checkIfGLError();
		glEnable(GL_DEPTH_TEST); checkIfGLError();
		glEnable(GL_ALPHA_TEST); checkIfGLError();
		glEnable(GL_BLEND); checkIfGLError();
		glEnable(GL_MULTISAMPLE); checkIfGLError();
		glEnable(GL_TEXTURE_2D); checkIfGLError();
		glEnable(GL_TEXTURE_CUBE_MAP); checkIfGLError();
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); checkIfGLError();
	}
};