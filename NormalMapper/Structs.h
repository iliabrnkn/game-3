#pragma once

#include <map>
#include <unordered_map>
#include <deque>
#include <glm/glm.hpp>
#include <string>


#include "../Game/Sizes.h"
#include "../Game/StringHelper.h"
#include "../Game/Matrices.h"

const glm::mat4 rawMatrix = glm::scale(glm::vec3(1.0, 0.5, 1.0)) * glm::rotate(glm::half_pi<float>() / 2.0f, glm::vec3(0.0f, 0.0f, 1.0f));
const glm::mat2 inversed2DMatrix = glm::mat2(glm::scale(glm::vec3(1.0f / mg::TILE_WIDTH_SCENE)) * glm::inverse(rawMatrix));
const glm::vec3 upVec = glm::vec3(0.0, 0.0, 1.0);
const float MAP_TILE_STEP = 32.f;
const int CIRCLE_APPROXIMATION_POINT_COUNT = 10;
const float TILESIDE = 45.254833995939f;

struct TileScale
{
	glm::vec2 translate;
	float scaleZ;
	glm::vec2 scale;
	float translateZ;
	glm::vec3 scaleVol;
	bool roundVertValuesX;
	bool roundVertValuesY;
	bool roundVertValuesZ;
	bool roundVertValuesZOnlyZeroes;

	TileScale():
		scaleVol(1.f), roundVertValuesX(false), roundVertValuesY(false), roundVertValuesZ(false)
	{}
};

struct SimpleVert
{
	SimpleVert() {}

	SimpleVert(const glm::vec3 &vert, const glm::vec3 &normal, const glm::vec2 &screenPos) :
		vert(vert), normal(normal), screenPos(screenPos)
	{}

	~SimpleVert() {};

	glm::vec3 vert;
	glm::vec3 normal;
	glm::vec2 screenPos;
};

// ��� ������ � �������
enum FlatShapeType
{
	PolyLineShape = 0,
	EllipseShape = 1,
	RectangleShape = 2
};

struct ShapeLayer
{
	std::vector<glm::vec3> points;
	std::vector<glm::vec2> screenPoints;
	float height;
	FlatShapeType shapeType;
	glm::vec3 center;
	unsigned int offset;
};

struct NMTileGeometry
{
	NMTileGeometry() {};

	NMTileGeometry(const std::vector<SimpleVert> &vertices, const std::vector<unsigned int> &indices) :
		vertices(vertices), indices(indices) {}

	std::vector<SimpleVert> vertices;
	std::vector<unsigned int> indices;
	glm::vec2 origin;
};

struct Quad
{
	Quad(const glm::vec3& v1, const glm::vec3& v2, const glm::vec3& v3, const glm::vec3& v4,
		const glm::vec2 &screenPos1, const glm::vec2 &screenPos2, const glm::vec2 &screenPos3, const glm::vec2 &screenPos4) :
		vec{ v1, v2, v3, v4 }, screenPos{screenPos1, screenPos2, screenPos3, screenPos4} 
	{

		normal = -glm::normalize(glm::cross(glm::normalize(v4 - v1), glm::normalize(v2 - v1)));
	}

	~Quad() {};

	std::vector<glm::vec3> vec;
	std::vector<glm::vec2> screenPos;
	glm::vec3 normal;
};