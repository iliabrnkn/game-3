#include "stdafx.h"

#include "OGLHelper.h"

void OGLHelper::InitOpengl()
{
	glewInit();
	assert(glewInit() == GLEW_OK);
}

GLuint OGLHelper::InitVAO(std::vector<SimpleVert> vect, std::vector<unsigned int> indices, GLuint bufVert, GLuint indVert)
{
	GLuint buf, ind;

	glGenBuffers(1, &buf); checkIfGLError();
	glGenBuffers(1, &ind); checkIfGLError();

	glBindBuffer(GL_ARRAY_BUFFER, buf); checkIfGLError();

	/*for (auto &v : vect)
	{
	v.vert = glm::round(v.vert);
	}*/

	glBufferData(GL_ARRAY_BUFFER, sizeof(SimpleVert) * vect.size(), vect.data(), GL_STATIC_DRAW); checkIfGLError();
	glBindBuffer(GL_ARRAY_BUFFER, NULL); checkIfGLError();

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ind); checkIfGLError();
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * indices.size(), indices.data(), GL_STATIC_DRAW); checkIfGLError();
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, NULL); checkIfGLError();

	GLuint vao;

	glGenVertexArrays(1, &vao); checkIfGLError();

	glBindVertexArray(vao); checkIfGLError();

	glBindBuffer(GL_ARRAY_BUFFER, buf); checkIfGLError();
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ind); checkIfGLError();

	glEnableVertexAttribArray(0); checkIfGLError();
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(SimpleVert), (void*)(0)); checkIfGLError();
	glEnableVertexAttribArray(1); checkIfGLError();
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(SimpleVert), (void*)(12)); checkIfGLError();
	glEnableVertexAttribArray(2); checkIfGLError();
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(SimpleVert), (void*)(24)); checkIfGLError();

	glBindVertexArray(NULL); checkIfGLError();

	bufVert = buf;
	indVert = ind;

	return vao;
}

GLuint OGLHelper::InitShader(const std::string& vsSource, const std::string& fsSource)
{
	GLuint program = glCreateProgram(); checkIfGLError();

	// ------ VERTEX SHADER

	GLuint vs = glCreateShader(GL_VERTEX_SHADER); checkIfGLError();

	const char* vsc = vsSource.c_str();

	glShaderSource(vs, 1, &vsc, 0); checkIfGLError();
	glCompileShader(vs); checkIfGLError();

	GLint length, result;

	glGetShaderiv(vs, GL_INFO_LOG_LENGTH, &length); checkIfGLError();

	char* log = new char[length];

	glGetShaderInfoLog(vs, length, &result, log); checkIfGLError();
	glAttachShader(program, vs); checkIfGLError();

	printf(log);
	printf("\r\n");

	delete[length] log;

	// ----- FRAGMENT SHADER

	GLuint fs = glCreateShader(GL_FRAGMENT_SHADER); checkIfGLError();

	const char* fsc = fsSource.c_str();

	glShaderSource(fs, 1, &fsc, 0); checkIfGLError();
	glCompileShader(fs); checkIfGLError();

	glGetShaderiv(fs, GL_INFO_LOG_LENGTH, &length); checkIfGLError();
	log = new char[length];
	glGetShaderInfoLog(fs, length, &result, log); checkIfGLError();

	glAttachShader(program, fs); checkIfGLError();

	printf(log);
	printf("\r\n");

	delete[length] log;

	// ----- LINKING

	glBindAttribLocation(program, 0, "vert"); checkIfGLError();
	glBindAttribLocation(program, 1, "normal"); checkIfGLError();

	glLinkProgram(program);

	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length); checkIfGLError();
	log = new char[length];
	glGetProgramInfoLog(program, length, &result, log); checkIfGLError();

	printf(log);
	printf("\r\n");

	delete[length] log;

	return program;
}

std::pair<sf::Image, sf::Image> OGLHelper::DrawGeometries(std::unordered_map<int, NMTileGeometry> &geometries, const int &tsWidth, const int &tsHeight,
	const int &tileWidth, const int &tileHeight, const int &tileCount)
{
	// �������
	glm::mat4 projectionMatrix = glm::ortho<float>(0.0, tsWidth, tsHeight, 0.0f, -1000.f, 1000.f);
	glm::mat4 viewMatrixProtosUnscalled = glm::rotate(glm::pi<float>() / 4.f, glm::vec3(0.0f, 0.0f, 1.0f))
		* glm::rotate(glm::pi<float>() / 3.f, glm::normalize(glm::vec3(1.0f, -1.0f, .0f)));
	glm::mat4 viewMatrix = viewMatrixProtosUnscalled * glm::scale(glm::vec3(TILESIDE, TILESIDE, TILESIDE));

	std::pair<sf::Image, sf::Image> res;

	sf::RenderTexture rtNormals;
	rtNormals.setSmooth(false);
	assert(rtNormals.create(tsWidth, tsHeight, true));

	sf::RenderTexture rtDepth;
	rtDepth.setSmooth(false);
	assert(rtDepth.create(tsWidth, tsHeight, true));

	//glm::vec3 offsetStep = mg::MatrixHelper::ScreenToScene(glm::vec2(tileWidth / 2, tileHeight - mg::TILESTEP));
	glm::vec3 offset;
	int tsRowNum = 0;

	NMTileGeometry resultGeometry;

	for (int idLocal = 0; idLocal < tileCount; ++idLocal)
	{
		if (geometries.find(idLocal) == geometries.end())
			continue;

		NMTileGeometry &currTileGeometry = geometries[idLocal];

		if (idLocal > 0 && idLocal * tileWidth % tsWidth == 0)
			++tsRowNum;

		offset = mg::MatrixHelper::ScreenToScene(glm::vec2((float)tileWidth / 2.f + (float)tileWidth * idLocal - (idLocal / (tsWidth / tileWidth)) * tsWidth,
			tileHeight * static_cast<float>(tsRowNum) + tileHeight - mg::TILE_HEIGHT_SCREEN));

		// �������

		// �������
		for (auto &ind : currTileGeometry.indices)
		{
			resultGeometry.indices.push_back(ind + resultGeometry.vertices.size());
		}

		for (int i = 0; i < currTileGeometry.vertices.size(); ++i)
		{
			resultGeometry.vertices.push_back(SimpleVert(currTileGeometry.vertices[i].vert + offset, 
				currTileGeometry.vertices[i].normal, currTileGeometry.vertices[i].screenPos));
		}
	}


	//----------------- ���������

	// ��������� ��������

	rtNormals.setActive();

	SetStates();

	glViewport(0, 0, tsWidth, tsHeight);

	GLuint bufVert = 0, indVert = 0;

	normalsShader = InitShader(vertexShader, normalsFragShader);

	GLuint vao = InitVAO(resultGeometry.vertices, resultGeometry.indices, bufVert, indVert);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); checkIfGLError();

	glUseProgram(normalsShader); checkIfGLError();

	int viewMatrixLocation = AllocateUniformVar("viewMatrix", normalsShader); checkIfGLError();
	int projMatrixLocation = AllocateUniformVar("projectionMatrix", normalsShader); checkIfGLError();
	int inversed2DMatrixLocation = AllocateUniformVar("inversed2DMat", normalsShader); checkIfGLError();

	glUniformMatrix4fv(viewMatrixLocation, 1, GL_FALSE, (GLfloat*)glm::value_ptr(viewMatrix)); checkIfGLError(); // view matrix
	glUniformMatrix4fv(projMatrixLocation, 1, GL_FALSE, (GLfloat*)glm::value_ptr(projectionMatrix)); checkIfGLError();// projection matrix
	glUniformMatrix2fv(inversed2DMatrixLocation, 1, GL_FALSE, (GLfloat*)glm::value_ptr(inversed2DMatrix)); checkIfGLError();

	glBindVertexArray(vao); checkIfGLError();

	glDrawElements(GL_TRIANGLES, resultGeometry.indices.size(), GL_UNSIGNED_INT, 0); checkIfGLError();

	glBindVertexArray(NULL); checkIfGLError();

	glDeleteBuffers(1, &bufVert); checkIfGLError();
	glDeleteBuffers(1, &indVert); checkIfGLError();

	rtNormals.display();
	res.second = rtNormals.getTexture().copyToImage();
	rtNormals.setActive(false);

	// ��������� �������

	rtDepth.setActive();
	glViewport(0, 0, tsWidth, tsHeight);

	SetStates();

	depthShader = InitShader(vertexShader, depthFragShader);

	vao = InitVAO(resultGeometry.vertices, resultGeometry.indices, bufVert, indVert);

	glClearColor(0.f, 0.f, 0.f, 1.f);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); checkIfGLError();

	glUseProgram(depthShader); checkIfGLError();

	viewMatrixLocation = AllocateUniformVar("viewMatrix", depthShader); checkIfGLError();
	projMatrixLocation = AllocateUniformVar("projectionMatrix", depthShader); checkIfGLError();
	inversed2DMatrixLocation = AllocateUniformVar("inversed2DMat", depthShader); checkIfGLError();
	int tileSizeLocation = AllocateUniformVar("tileSize", depthShader); checkIfGLError();
	int rawMatLocation = AllocateUniformVar("rawMatrix", depthShader); checkIfGLError();

	glUniformMatrix4fv(viewMatrixLocation, 1, GL_FALSE, (GLfloat*)glm::value_ptr(viewMatrix)); checkIfGLError(); // view matrix
	glUniformMatrix4fv(projMatrixLocation, 1, GL_FALSE, (GLfloat*)glm::value_ptr(projectionMatrix)); checkIfGLError();// projection matrix
	glUniformMatrix2fv(inversed2DMatrixLocation, 1, GL_FALSE, (GLfloat*)glm::value_ptr(inversed2DMatrix)); checkIfGLError();
	glUniform2f(tileSizeLocation, tileWidth, tileHeight); checkIfGLError();
	glUniformMatrix4fv(rawMatLocation, 1, GL_FALSE, (GLfloat*)glm::value_ptr(rawMatrix)); checkIfGLError();

	glBindVertexArray(vao); checkIfGLError();

	glDrawElements(GL_TRIANGLES, resultGeometry.indices.size(), GL_UNSIGNED_INT, 0); checkIfGLError();

	glBindVertexArray(NULL); checkIfGLError();

	rtDepth.display();
	res.first = rtDepth.getTexture().copyToImage();
	rtDepth.setActive(false);

	return res;

	// cleanup

	glUseProgram(0); checkIfGLError();
	glDeleteProgram(depthShader); checkIfGLError();
	glDeleteProgram(normalsShader); checkIfGLError();

	glDeleteVertexArrays(1, &vao); checkIfGLError();
	glDeleteBuffers(1, &bufVert); checkIfGLError();
	glDeleteBuffers(1, &indVert); checkIfGLError();
}

int OGLHelper::AllocateUniformVar(const std::string &varName, GLuint shaderName)
{
	glUseProgram(shaderName);
	GLint location = glGetUniformLocation(shaderName, varName.c_str());

	return location;
}
