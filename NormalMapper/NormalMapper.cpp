// NormalMapper.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <map>
#include <unordered_map>
#include <deque>
#include <string>
#include <iostream>
#include <fstream>

#include <windows.h>
#include <GL/glew/glew.h>
#include <GL/GL.h>

#include <SFML/Window.hpp>
#include <SFML\Graphics.hpp>
#include <lua\LuaPlus.h>
#include <fstream>
#include <glm/glm.hpp>
#include <glm/gtx/intersect.hpp>
#include <glm/gtx/transform.hpp>

#include "../Game/Sizes.h"

LuaPlus::LuaState* state;

// ������� �� ����� � ����� ��� �� ����� � �����
glm::mat4 mapSceneSwapMx(
	1.f, 0.f, 0.f, 0.f,
	0.f, 0.f, 1.f, 0.f,
	0.f, 1.f, 0.f, 0.f,
	0.f, 0.f, 0.f, 1.f
);

// �� ����� � �����������
glm::mat4 viewMx(
	glm::rotate(-glm::pi<float>() / 6.f, glm::normalize(glm::vec3(1.f, 0.f, 0.f))) *
	glm::rotate(-glm::quarter_pi<float>(), glm::vec3(0.f, 1.f, 0.f)) *
	glm::scale(glm::vec3(mg::TILE_WIDTH_SCENE, -mg::TILE_WIDTH_SCENE, mg::TILE_WIDTH_SCENE))
);

// �� ��������������� device-��������� � �������� ����������� (��� y ������ ����)
glm::mat4 screenToNDMx(
	2.f / float(mg::RENDERBUFFER_WIDTH), 0.f, 0.f, 0.f,
	0.f, -2.f / float(mg::RENDERBUFFER_HEIGHT), 0.f, 0.f,
	0.f, 0.f, 1.f, 0.f,
	-1.f, 1.f, 0.f, 1.f
);

// �� ��������� ����������� � ��������������� device-�����������
glm::mat4 projectionMx = glm::ortho<float>(
	0.f, float(mg::RENDERBUFFER_WIDTH),
	float(mg::RENDERBUFFER_HEIGHT), 0.f,
	float(mg::ZNEAR_SCENE) * mg::TILE_WIDTH_SCENE, float(mg::ZFAR_SCENE) * mg::TILE_WIDTH_SCENE
	);

// �� ��������� ����� � ����������� ������
glm::mat4 sceneToScreenMx = glm::inverse(screenToNDMx) * projectionMx * viewMx;

// �� ��������� ������ � ����������� �����
glm::mat4 screenToSceneMx =
	mapSceneSwapMx *
	glm::scale(glm::vec3(1.f / mg::TILE_WIDTH_SCENE)) *
	glm::inverse(glm::rotate(glm::quarter_pi<float>(), glm::vec3(0.f, 0.f, 1.f))) *
	glm::scale(glm::vec3(1.f, 2.f, 1.f));

glm::vec2 ScreenToScene(const glm::vec3 &vec)
{
	return glm::vec2(sceneToScreenMx * glm::vec4(vec, 1.f));
}

glm::vec3 ScreenToScene(const glm::vec2 &vec)
{
	return glm::vec3(screenToSceneMx * glm::vec4(vec, 0.f, 1.f));
}

glm::vec2 FindNextLinePoint(const sf::Image &cv, const glm::vec2 &vecInitial)
{
	glm::vec2 vec = vecInitial;
	// �������� �� ����� ����� �����
	const sf::Color blueCol = sf::Color(0, 0, 255);
	sf::Color currCol;
	sf::Color redCol = cv.getPixel(int(vec.x), int(vec.y));

	if (int(vec.x) - 1 >= 0 &&
		(currCol = cv.getPixel(int(vec.x) - 1, int(vec.y))) == blueCol)
	{
		return glm::vec2(vec.x -1.f, vec.y);
	}
	if (int(vec.x) - 1 >= 0 && int(vec.y) - 1 >= 0 &&
		(currCol = cv.getPixel(int(vec.x) - 1, int(vec.y) - 1)) == blueCol)
	{
		return glm::vec2(vec.x - 1.f, vec.y - 1.f);
	}
	if (int(vec.y) - 1 >= 0 &&
		(currCol = cv.getPixel(int(vec.x), int(vec.y) - 1)) == blueCol)
	{
		return glm::vec2(vec.x, vec.y - 1.f);
	}
	if (int(vec.x) + 1 < cv.getSize().x && int(vec.y) - 1 >= 0 &&
		(currCol = cv.getPixel(int(vec.x + 1), int(vec.y) - 1)) == blueCol)
	{
		return glm::vec2(vec.x + 1.f, vec.y - 1.f);
	}
	if (int(vec.x) + 1 < cv.getSize().x &&
		(currCol = cv.getPixel(int(vec.x + 1), int(vec.y))) == blueCol)
	{
		return glm::vec2(vec.x + 1.f, vec.y);
	}

	// ���� ����� ��� ����� ��������, �������, ���� �� �������

	if (int(vec.x) - 1 >= 0 && 
		(currCol = cv.getPixel(int(vec.x) - 1, int(vec.y))).r > 0 && 
		currCol.g == 0 && currCol.b == 0 && currCol != redCol)
	{
		return glm::vec2(vec.x - 1.f, vec.y);
	}
	if (int(vec.x) - 1 >= 0 && int(vec.y) - 1 >= 0 &&
		(currCol = cv.getPixel(int(vec.x) - 1, int(vec.y) - 1)).r > 0 && 
		currCol.g == 0 && currCol.b == 0 && currCol != redCol)
	{
		return glm::vec2(vec.x - 1.f, vec.y - 1.f);
	}
	if (int(vec.y) - 1 >= 0 &&
		(currCol = cv.getPixel(int(vec.x), int(vec.y) - 1)).r > 0 && 
		currCol.g == 0 && currCol.b == 0 && currCol != redCol)
	{
		return glm::vec2(vec.x, vec.y - 1.f);
	}
	if (int(vec.y) - 1 >= 0 && int(vec.x + 1) < cv.getSize().x &&
		(currCol = cv.getPixel(int(vec.x + 1), int(vec.y) - 1)).r > 0 && 
		currCol.g == 0 && currCol.b == 0 && currCol != redCol)
	{
		return glm::vec2(vec.x + 1.f, vec.y - 1.f);
	}
	if (int(vec.x + 1) < cv.getSize().x &&
		(currCol = cv.getPixel(int(vec.x + 1), int(vec.y))).r > 0 && 
		currCol.g == 0 && currCol.b == 0 && currCol != redCol)
	{
		return glm::vec2(vec.x + 1.f, vec.y);
	}

	return vec;
}

sf::Font font;

void DrawTextOnImage(sf::RenderTexture &rt, const std::string &text, const glm::vec2 &coords)
{
	sf::Text sfText;
	sfText.setFont(font);
	sfText.setString(text);
	sfText.setCharacterSize(9);
	sfText.setFillColor(sf::Color(255, 255, 255, 255));

	sfText.setPosition(coords.x, coords.y);

	rt.draw(sfText);
}

std::string ProcessColoredTile(sf::Image &cv, sf::RenderTexture &rt, const int &xStart, const int &yStart, const int &tWidth, const int &tHeight)
{
	glm::vec2 originScreen = glm::vec2(xStart + 32.f, yStart + float(tHeight) - 32.f);

	std::unordered_map<int, std::vector<glm::vec2>> redDots; // ������� �� ��������� ��������
	std::unordered_map<int, int> screenHeights; // ���������� � ������� ����� ������ ������� �����

	//���� ������� �����
	for (int y = tHeight - 1; y >= 0; --y)
	{
		for (int x = 0; x < tWidth; ++x)
		{
			if (x + xStart < 0 || y + yStart < 0 ||
				x + xStart >= cv.getSize().x || y + yStart >= cv.getSize().y)
				continue;

			sf::Color col = cv.getPixel(x + xStart, y + yStart);

			if (col.r > 0 && col.g == 0 && col.b == 0) 
			{
				redDots[col.r].push_back(glm::vec2(xStart + x, yStart + y));

				// ����� � ������� �������� � ������� ����� ����� �����
				glm::vec2 nextLinePoint = glm::vec2(x + xStart, y + yStart); glm::vec2 prevPoint = glm::vec2(x + xStart, y + yStart);

				while ((nextLinePoint = FindNextLinePoint(cv, nextLinePoint)) != prevPoint) {
					// ���������, �� ��������� �� � ������� ��������� ������� �����
					sf::Color colorCheck = cv.getPixel(nextLinePoint.x, nextLinePoint.y);
					
					if (colorCheck.r > 0 && colorCheck.g == 0 && colorCheck.b == 0)
					{
						screenHeights.insert(std::make_pair(colorCheck.r, y + yStart - nextLinePoint.y));
						break;
					}

					prevPoint = nextLinePoint;
				}
			}
		}
	}

	int dotNum = 0;
	std::string res = "";

	// ���������� ������ �� ������� �����

	for (auto pair : redDots)
	{
		// ������ ����
		auto iterDown = std::find_if(screenHeights.begin(), screenHeights.end(),
			[&pair](auto &x) { return x.first == pair.first; });

		auto zLevel = 0.f;

		if (iterDown != screenHeights.end())
		{
			zLevel = iterDown->second;
		}
		else // ���� �� ����� ����� ����, �� ������� ���� ����� � ����� ����
		{
			zLevel = 0;
		}

		// �����
		for (auto dot : pair.second)
		{
			glm::vec3 scenePoint = ScreenToScene(dot + glm::vec2(0, zLevel) - originScreen);
			
			scenePoint.y = zLevel / 39;

			std::string coordsText = "{" + std::to_string(scenePoint.x) + ";"
				+ std::to_string(-scenePoint.z) + ";" + std::to_string(scenePoint.y) + "}";

			glm::ivec2 dotOffset(0, 0);

			// �������, ���� � ��������� 343 �������� ��� ������� �����, ������ ����� �����, ����� - ������
			for (int x = -8; x < 9; ++x)
			{
				for (int y = -8; y < 9; ++y)
				{
					if (x == 0 && y == 0) continue;

					auto currPoint = glm::ivec2(dot.x + x, dot.y + y);
					
					if (currPoint.x < 0 || currPoint.y < 0
						|| currPoint.x >= cv.getSize().x || currPoint.y >= cv.getSize().y)
						continue;

					auto currPointColor = cv.getPixel(currPoint.x, currPoint.y);

					if (currPointColor.r > 0 && currPointColor.g == 0 && currPointColor.b == 0)
					{
						dotOffset = -glm::ivec2(x, y) * 4;
						break;
					}
				}

				if (dotOffset != glm::ivec2(0, 0)) break;
			}

			if (dotOffset != glm::ivec2(0, 0))
			{
				sf::Vertex line[] =
				{
					sf::Vertex(sf::Vector2f(dot.x, dot.y)),
					sf::Vertex(sf::Vector2f(dot.x + dotOffset.x, dot.y + dotOffset.y))
				};

				rt.draw(line, 2, sf::Lines);
			}

			DrawTextOnImage(rt, std::to_string(dotNum), 
				dot + (
					dotOffset == glm::ivec2(0) ? 
					glm::vec2(1.f, 1.f) :
					(dotOffset + glm::ivec2(
						glm::sign(dotOffset.x) < 0 ? -9 : 1, 
						glm::sign(dotOffset.y) < 0 ? -9 : 0
					))
				)
			);
			res += std::to_string(dotNum) + ": " + coordsText + "\r\n";

			++dotNum;
		}
	}

	return res;
}

// ������������ �������� - ���� � ���-�����, ��� ������� ��� �����
int main(int argc, char *argv[])
{
	font.loadFromFile("arial.ttf");
	if (argc == 1) return 0;
	state = LuaPlus::LuaState::Create(true);
	state->DoFile(argv[1]); // ������ ���������� ���������� ��� �����

	auto tilesets = state->GetGlobal("tilesets");
	if (tilesets.GetRef() == -1 || !tilesets.IsTable())
	{
		printf("there is no tilesets lua-file");
		return 1;
	}

	for (LuaPlus::LuaTableIterator iter(tilesets); iter; iter.Next())
	{
		auto singleTileset = iter.GetValue();

		if (singleTileset.IsTable()) // ������� � ���������
		{		
			int startX = singleTileset["startX"].ToInteger(); int startY = singleTileset["startY"].ToInteger();
			int imgWidth = singleTileset["imagewidth"].ToInteger();
			int imgHeight = singleTileset["imageheight"].ToInteger();
			int tileWidth = singleTileset["tilewidth"].ToInteger();
			int tileHeight = singleTileset["tileheight"].ToInteger();
			int firstGid = singleTileset.GetByName("firstgid").ToInteger();
			int tileCount = singleTileset.GetByName("tilecount").ToInteger();
			std::string name = singleTileset.GetByName("name").ToString();
			std::string imgFilename = singleTileset.GetByName("image").ToString();

			sf::Image tsImg; 
			tsImg.loadFromFile(argc >= 3 ? argv[2] : "" + imgFilename);
			
			sf::RenderTexture rt;
			rt.setActive(true);
			rt.create(tsImg.getSize().x, tsImg.getSize().y);
			rt.clear();

			sf::Texture tex; tex.loadFromImage(tsImg);
			sf::Sprite sp(tex); sp.setPosition(0, 0);
			rt.draw(sp);
			rt.display();

			auto coords = ProcessColoredTile(tsImg, rt, startX, startY, tileWidth, tileHeight);
			rt.display();
			rt.getTexture().copyToImage().saveToFile("Output/" + name + ".png");

			printf("%s processed\r\n", imgFilename.c_str());

			// ���������� �����
			auto str = std::ofstream("Output/" + name + "_coords.txt");
			str.clear();
			str << coords;
			str << "\r\n---\r\n\r\n";
			str.close();
		}
	}



	printf("done");
	char a;
	std::cin >> a;

    return 0;
}