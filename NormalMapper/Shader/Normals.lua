return {
	name = "NormalShader",
	vertexshader = 
		[[
		#version 330
		
		uniform mat4 viewMatrix;
		uniform mat4 projectionMatrix;

		in vec3 position;
		in vec3 normal;
		in float boneID;

		out vec3 fragNormal;
		out vec3 fragPos;

		void main()
		{	
			gl_Position = projectionMatrix * viewMatrix * vec4(position, 1.f);
			
			fragPos = position;
			fragNormal = normal;
		}]],
	fragmentshader = 
		[[
		#version 330
		
		uniform vec2 cameraSceneOffset;
		uniform vec2 offsetFromTop;
		uniform float maxZCoordScreen;

		in vec3 fragNormal;
		in vec3 fragPos;

		layout(location = 0) out vec4 outColor;

		float getZLen(vec2 point)
		{
			if (point != vec2(0.0))
				return length(point) * dot(normalize(point), normalize(vec2(1.0, 1.0)));
			else 
				return 0.0;
		}
		
		void main(){

			vec2 depthVecRel = fragPos.xy - cameraSceneOffset;
			float fragDepth = getZLen(depthVecRel + offsetFromTop) / maxZCoordScreen;
			
			gl_FragDepth = 1.0 - fragDepth/2.0;

			outColor = vec4(fragNormal);
		}]],
	attriblocations = {
		{
			name = "position",
			location = 0
		},
		{
			name = "normal",
			location = 1
		},
		{
			name = "boneID",
			location = 2
		}
	}
}